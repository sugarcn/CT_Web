package org.ctonline.dao.goods.impl;


import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.goods.F_GoodsDAO;
import org.ctonline.po.basic.CtAd;
import org.ctonline.po.basic.CtCoupon;
import org.ctonline.po.basic.CtCouponDetail;
import org.ctonline.po.basic.CtNotice;
import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.basic.CtReplace;
import org.ctonline.po.basic.CtResource;
import org.ctonline.po.basic.CtSearchKeyword;
import org.ctonline.po.basic.CtXwStock;
import org.ctonline.po.goods.CtBom;
import org.ctonline.po.goods.CtBomGoods;
import org.ctonline.po.goods.CtCollect;
import org.ctonline.po.goods.CtCollectGoods;
import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.goods.CtGoodsAttribute;
import org.ctonline.po.goods.CtGoodsAttributeRelation;
import org.ctonline.po.goods.CtGoodsBrand;
import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.po.goods.CtGoodsDetail;
import org.ctonline.po.goods.CtGoodsImg;
import org.ctonline.po.goods.CtGoodsResource;
import org.ctonline.po.goods.CtGoodsType;
import org.ctonline.po.help.CtHelp;
import org.ctonline.po.order.CtCart;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.views.ViewCateNum;
import org.ctonline.po.views.ViewGoodsList;
import org.ctonline.po.views.ViewSubcateNum;
import org.ctonline.util.Page;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;

import com.opensymphony.xwork2.ActionContext;
import com.sun.net.httpserver.Authenticator.Success;

public class F_GoodsImpl  extends BaseHibernateDAOImpl implements F_GoodsDAO {


	@SuppressWarnings("unchecked")
	@Override
	public List<CtGoodsCategory> queryAllCate() {
		// TODO Auto-generated method stub
		List<CtGoodsCategory> cateList = null;
		 
		Query query= getSession().createQuery("from CtGoodsCategory as cate where cate.parentId = 0 order by cate.parentId,cate.sortOrder");
		cateList = query.list();
		return cateList;
	}

	public List<CtGoodsCategory> queryAllCateToType(String type) {
		// TODO Auto-generated method stub
		List<CtGoodsCategory> cateList = null;
		String hql = null;
		if(false){
			hql = "from CtGoodsCategory as cate,CtGoodsCategory as cate1  where cate.CId and cate1.sortOrder = cate.CName='"+type+"' order by cate.parentId,cate.sortOrder";
		} else {
			hql = "from CtGoodsCategory as cate,CtGoodsCategory as cate1 order by cate.parentId,cate.sortOrder";
		}
		Query query= getSession().createQuery(hql);
		cateList = query.list();
		return cateList;
	}
	
	@Override
	public List<ViewCateNum> cateCount() {
		// TODO Auto-generated method stub
		List<ViewCateNum> catecount = null;
		 
		Query query= getSession().createQuery("from ViewCateNum");
		catecount = query.list();
		 
		return catecount;
	}

	@Override
	public List<ViewSubcateNum> subcateCount() {
		// TODO Auto-generated method stub
		List<ViewSubcateNum> subcatecount = null;
		 
		Query query= getSession().createQuery("from ViewSubcateNum");
		subcatecount = query.list();
		 
		return subcatecount;
	}

	@Override
	public List<CtGoodsBrand> queryBrand() {
		// TODO Auto-generated method stub
		List<CtGoodsBrand> brandList = null;
		 
		Query query= getSession().createQuery("from CtGoodsBrand as brand order by brand.sortOrder");
		brandList = query.list();
		 
		return brandList;
	}

	@Override
	public List<ViewGoodsList> getGoodsList(Page page) {
		// TODO Auto-generated method stub
		List<ViewGoodsList> goodsList = null;
		 
		Query query= getSession().createQuery("from ViewGoodsList as list");
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		goodsList = query.list();
		 
		return goodsList;
	}
	
	@Override
	public List<?> searchGoodsByCId(Long cid, Page page) {
		// TODO Auto-generated method stub
		int countForBian = 0;
		List<?> goodsList = null;
		List<CtGoodsCategory> goodsList1 = new ArrayList<CtGoodsCategory>();
		List goodsListAll = new ArrayList();
		List goodsListForThree = new ArrayList();
		List goodsListAll1 = new ArrayList();
		String hql = " from CtGoodsCategory cc where cc.CId = " + cid;
		Query query1 = getSession().createQuery(hql);
		goodsList1 = query1.list();
		if (goodsList1 != null && goodsList1.size() > 0) {
			if(goodsList1.get(0).getParentId().equals(0L)){
				hql = " from CtGoodsCategory cc where cc.parentId = " + cid;
				StringBuffer sqlFirst = new StringBuffer();
				sqlFirst.append("SELECT  ");
				sqlFirst.append("CG.G_ID AS G_ID,NVL(CG.G_NAME, 'N/A') AS G_NAME,NVL(CG.G_SN, 'N/A') AS G_SN,NVL(CG.MARKET_PRICE, 9999) AS MARKET_PRICE,  ");
				sqlFirst.append("NVL(CG.PROMOTE_PRICE, 9999) AS PROMOTE_PRICE,NVL(CG.SHOP_PRICE, 9999) AS SHOP_PRICE,  ");
				sqlFirst.append("NVL(CGB.B_NAME, 'N/A') AS B_NAME,NVL(CG.G_UNIT, 'N/A') AS G_UNIT,  ");
				sqlFirst.append("NVL(CG.PACK1, 'N/A') AS PACK1,NVL(CG.PACK1_NUM, 0) AS PACK1_NUM,  ");
				sqlFirst.append("NVL(CG.PACK2, 'N/A') AS PACK2,NVL(CG.PACK2_NUM, 0) AS PACK2_NUM,  ");
				sqlFirst.append("NVL(CG.PACK3, 'N/A') AS PACK3,NVL(CG.PACK3_NUM, 0) AS PACK3_NUM,  ");
				sqlFirst.append("NVL(CG.PACK4, 'N/A') AS PACK4,NVL(CG.PACK4_NUM, 0) AS PACK4_NUM,  ");
				sqlFirst.append("NVL(CGI.P_URL, 'nourl') AS P_URL ");
				sqlFirst.append("FROM  ");
				sqlFirst.append("CT_GOODS cg  ");
				sqlFirst.append("LEFT JOIN  ");
				sqlFirst.append("CT_GOODS_BRAND cgb  ");
				sqlFirst.append("ON  ");
				sqlFirst.append("CG.B_ID = CGB.B_ID  ");
				sqlFirst.append("LEFT JOIN  ");
				sqlFirst.append("CT_GOODS_IMG cgi  ");
				sqlFirst.append("ON  ");
				sqlFirst.append("CGI.G_ID = CG.G_ID  ");
				sqlFirst.append("WHERE  ");
				sqlFirst.append("CG.C_ID = "+cid);
				sqlFirst.append(" ORDER BY  ");
				sqlFirst.append("CG.G_ID  ");
				SQLQuery queryFirst= getSession().createSQLQuery(sqlFirst.toString());
//				if (page.getCurrentPage()!=0){
//					page.setTotalCount(page.getTotalCount()==null?0L:page.getTotalCount() + this.countByCId(ctGoodsCategory.getCId()));
//					query.setFirstResult(page.getFirstIndex());
//					query.setMaxResults(page.getPageSize());
//				}
				List goodsListForFirst = queryFirst.list();
				for (int i = 0; i < goodsListForFirst.size(); i++) {
					goodsListAll.add(goodsListForFirst.get(i));
				}
				query1 = getSession().createQuery(hql);
				List<CtGoodsCategory> findList = query1.list();
				for (CtGoodsCategory ctGoodsCategory : findList) {
					hql = " from CtGoodsCategory cc where cc.parentId = " + ctGoodsCategory.getCId();
					query1 = getSession().createQuery(hql);
					List<CtGoodsCategory> findListForThree = query1.list();
					for (CtGoodsCategory ctGoodsCategory2 : findListForThree) {
						StringBuffer sql = new StringBuffer();
						sql.append("SELECT  ");
						sql.append("CG.G_ID AS G_ID,NVL(CG.G_NAME, 'N/A') AS G_NAME,NVL(CG.G_SN, 'N/A') AS G_SN,NVL(CG.MARKET_PRICE, 9999) AS MARKET_PRICE,  ");
						sql.append("NVL(CG.PROMOTE_PRICE, 9999) AS PROMOTE_PRICE,NVL(CG.SHOP_PRICE, 9999) AS SHOP_PRICE,  ");
						sql.append("NVL(CGB.B_NAME, 'N/A') AS B_NAME,NVL(CG.G_UNIT, 'N/A') AS G_UNIT,  ");
						sql.append("NVL(CG.PACK1, 'N/A') AS PACK1,NVL(CG.PACK1_NUM, 0) AS PACK1_NUM,  ");
						sql.append("NVL(CG.PACK2, 'N/A') AS PACK2,NVL(CG.PACK2_NUM, 0) AS PACK2_NUM,  ");
						sql.append("NVL(CG.PACK3, 'N/A') AS PACK3,NVL(CG.PACK3_NUM, 0) AS PACK3_NUM,  ");
						sql.append("NVL(CG.PACK4, 'N/A') AS PACK4,NVL(CG.PACK4_NUM, 0) AS PACK4_NUM,  ");
						sql.append("NVL(CGI.P_URL, 'nourl') AS P_URL ");
						sql.append("FROM  ");
						sql.append("CT_GOODS cg  ");
						sql.append("LEFT JOIN  ");
						sql.append("CT_GOODS_BRAND cgb  ");
						sql.append("ON  ");
						sql.append("CG.B_ID = CGB.B_ID  ");
						sql.append("LEFT JOIN  ");
						sql.append("CT_GOODS_IMG cgi  ");
						sql.append("ON  ");
						sql.append("CGI.G_ID = CG.G_ID  ");
						sql.append("WHERE  ");
						sql.append("CG.C_ID = "+ctGoodsCategory2.getCId());
						sql.append(" ORDER BY  ");
						sql.append("CG.G_ID  ");
						SQLQuery query= getSession().createSQLQuery(sql.toString());
//						if (page.getCurrentPage()!=0){
//							page.setTotalCount(page.getTotalCount()==null?0L:page.getTotalCount() + this.countByCId(ctGoodsCategory.getCId()));
//							query.setFirstResult(page.getFirstIndex());
//							query.setMaxResults(page.getPageSize());
//						}
						goodsListForThree = query.list();
						for (int i = 0; i < goodsListForThree.size(); i++) {
							goodsListAll.add(goodsListForThree.get(i));
						}
					}
					StringBuffer sql = new StringBuffer();
					sql.append("SELECT  ");
					sql.append("CG.G_ID AS G_ID,NVL(CG.G_NAME, 'N/A') AS G_NAME,NVL(CG.G_SN, 'N/A') AS G_SN,NVL(CG.MARKET_PRICE, 9999) AS MARKET_PRICE,  ");
					sql.append("NVL(CG.PROMOTE_PRICE, 9999) AS PROMOTE_PRICE,NVL(CG.SHOP_PRICE, 9999) AS SHOP_PRICE,  ");
					sql.append("NVL(CGB.B_NAME, 'N/A') AS B_NAME,NVL(CG.G_UNIT, 'N/A') AS G_UNIT,  ");
					sql.append("NVL(CG.PACK1, 'N/A') AS PACK1,NVL(CG.PACK1_NUM, 0) AS PACK1_NUM,  ");
					sql.append("NVL(CG.PACK2, 'N/A') AS PACK2,NVL(CG.PACK2_NUM, 0) AS PACK2_NUM,  ");
					sql.append("NVL(CG.PACK3, 'N/A') AS PACK3,NVL(CG.PACK3_NUM, 0) AS PACK3_NUM,  ");
					sql.append("NVL(CG.PACK4, 'N/A') AS PACK4,NVL(CG.PACK4_NUM, 0) AS PACK4_NUM,  ");
					sql.append("NVL(CGI.P_URL, 'nourl') AS P_URL ");
					sql.append("FROM  ");
					sql.append("CT_GOODS cg  ");
					sql.append("LEFT JOIN  ");
					sql.append("CT_GOODS_BRAND cgb  ");
					sql.append("ON  ");
					sql.append("CG.B_ID = CGB.B_ID  ");
					sql.append("LEFT JOIN  ");
					sql.append("CT_GOODS_IMG cgi  ");
					sql.append("ON  ");
					sql.append("CGI.G_ID = CG.G_ID  ");
					sql.append("WHERE  ");
					sql.append("CG.C_ID = "+ctGoodsCategory.getCId());
					sql.append(" ORDER BY  ");
					sql.append("CG.G_ID  ");
					SQLQuery query= getSession().createSQLQuery(sql.toString());
					List goodsListErjiAll = query.list();
					for (int i = 0; i < goodsListErjiAll.size(); i++) {
						goodsListAll.add(goodsListErjiAll.get(i));
					}
				}
				if (page.getCurrentPage()!=0){
					page.setTotalCount(Long.valueOf(goodsListAll.size()));
				}
				int index = page.getFirstIndex();
				if(goodsListAll != null && goodsListAll.size() > 0){
					for (Object ctGoodsCategory2 : goodsListAll) {
						if(countForBian >= index && countForBian < (index+12)){
							goodsListAll1.add(ctGoodsCategory2);
						}
						countForBian++;
					}
				}
				countForBian = 0;
				goodsList = goodsListAll1;
			} else {
				hql = " from CtGoodsCategory cc where cc.parentId = " + cid;
				List<CtGoodsCategory> goodsListClickErji = getSession().createQuery(hql).list();
				for (CtGoodsCategory ctGoodsCategory2 : goodsListClickErji) {
					StringBuffer sql = new StringBuffer();
					sql.append("SELECT  ");
					sql.append("CG.G_ID AS G_ID,NVL(CG.G_NAME, 'N/A') AS G_NAME,NVL(CG.G_SN, 'N/A') AS G_SN,NVL(CG.MARKET_PRICE, 9999) AS MARKET_PRICE,  ");
					sql.append("NVL(CG.PROMOTE_PRICE, 9999) AS PROMOTE_PRICE,NVL(CG.SHOP_PRICE, 9999) AS SHOP_PRICE,  ");
					sql.append("NVL(CGB.B_NAME, 'N/A') AS B_NAME,NVL(CG.G_UNIT, 'N/A') AS G_UNIT,  ");
					sql.append("NVL(CG.PACK1, 'N/A') AS PACK1,NVL(CG.PACK1_NUM, 0) AS PACK1_NUM,  ");
					sql.append("NVL(CG.PACK2, 'N/A') AS PACK2,NVL(CG.PACK2_NUM, 0) AS PACK2_NUM,  ");
					sql.append("NVL(CG.PACK3, 'N/A') AS PACK3,NVL(CG.PACK3_NUM, 0) AS PACK3_NUM,  ");
					sql.append("NVL(CG.PACK4, 'N/A') AS PACK4,NVL(CG.PACK4_NUM, 0) AS PACK4_NUM,  ");
					sql.append("NVL(CGI.P_URL, 'nourl') AS P_URL ");
					sql.append("FROM  ");
					sql.append("CT_GOODS cg  ");
					sql.append("LEFT JOIN  ");
					sql.append("CT_GOODS_BRAND cgb  ");
					sql.append("ON  ");
					sql.append("CG.B_ID = CGB.B_ID  ");
					sql.append("LEFT JOIN  ");
					sql.append("CT_GOODS_IMG cgi  ");
					sql.append("ON  ");
					sql.append("CGI.G_ID = CG.G_ID  ");
					sql.append("WHERE  ");
					sql.append("CG.C_ID = "+ctGoodsCategory2.getCId());
					sql.append(" ORDER BY  ");
					sql.append("CG.G_ID  ");
					SQLQuery query= getSession().createSQLQuery(sql.toString());
					List goodsListClickSec = query.list();
					for (int i = 0; i < goodsListClickSec.size(); i++) {
						goodsListAll.add(goodsListClickSec.get(i));
					}
				}
				StringBuffer sql = new StringBuffer();
				sql.append("SELECT  ");
				sql.append("CG.G_ID AS G_ID,NVL(CG.G_NAME, 'N/A') AS G_NAME,NVL(CG.G_SN, 'N/A') AS G_SN,NVL(CG.MARKET_PRICE, 9999) AS MARKET_PRICE,  ");
				sql.append("NVL(CG.PROMOTE_PRICE, 9999) AS PROMOTE_PRICE,NVL(CG.SHOP_PRICE, 9999) AS SHOP_PRICE,  ");
				sql.append("NVL(CGB.B_NAME, 'N/A') AS B_NAME,NVL(CG.G_UNIT, 'N/A') AS G_UNIT,  ");
				sql.append("NVL(CG.PACK1, 'N/A') AS PACK1,NVL(CG.PACK1_NUM, 0) AS PACK1_NUM,  ");
				sql.append("NVL(CG.PACK2, 'N/A') AS PACK2,NVL(CG.PACK2_NUM, 0) AS PACK2_NUM,  ");
				sql.append("NVL(CG.PACK3, 'N/A') AS PACK3,NVL(CG.PACK3_NUM, 0) AS PACK3_NUM,  ");
				sql.append("NVL(CG.PACK4, 'N/A') AS PACK4,NVL(CG.PACK4_NUM, 0) AS PACK4_NUM,  ");
				sql.append("NVL(CGI.P_URL, 'nourl') AS P_URL ");
				sql.append("FROM  ");
				sql.append("CT_GOODS cg  ");
				sql.append("LEFT JOIN  ");
				sql.append("CT_GOODS_BRAND cgb  ");
				sql.append("ON  ");
				sql.append("CG.B_ID = CGB.B_ID  ");
				sql.append("LEFT JOIN  ");
				sql.append("CT_GOODS_IMG cgi  ");
				sql.append("ON  ");
				sql.append("CGI.G_ID = CG.G_ID  ");
				sql.append("WHERE  ");
				sql.append("CG.C_ID = "+cid);
				sql.append(" ORDER BY  ");
				sql.append("CG.G_ID  ");
				SQLQuery query= getSession().createSQLQuery(sql.toString());
//				if (page.getCurrentPage()!=0){
//					page.setTotalCount(this.countByCId(cid));
//					query.setFirstResult(page.getFirstIndex());
//					query.setMaxResults(page.getPageSize());
//				}
				goodsList = query.list();
				for (int i = 0; i < goodsList.size(); i++) {
					goodsListAll.add(goodsList.get(i));
				}
				if (page.getCurrentPage()!=0){
					page.setTotalCount(Long.valueOf(goodsListAll.size()));
				}
				int index = page.getFirstIndex();
				if(goodsListAll != null && goodsListAll.size() > 0){
					for (Object ctGoodsCategory2 : goodsListAll) {
						if(countForBian >= index && countForBian < (index+12)){
							goodsListAll1.add(ctGoodsCategory2);
						}
						countForBian++;
					}
				}
				countForBian = 0;
				goodsList = goodsListAll1;
			}
		}
		return goodsList;
	}
	@Override
	public List<?> searchGoodsList(Long pid,String kword,Page page,String price1, String price2) {
		// TODO Auto-generated method stub
 		List<?> goodsList = null;
		StringBuffer sql = new StringBuffer();
		if (pid != null) {
			sql.append("SELECT  ");
			sql.append("CG.G_ID AS G_ID,NVL(CG.G_NAME, 'N/A') AS G_NAME,NVL(CG.G_SN, 'N/A') AS G_SN,NVL(CG.MARKET_PRICE, 9999) AS MARKET_PRICE,  ");
			sql.append("NVL(CG.PROMOTE_PRICE, 9999) AS PROMOTE_PRICE,NVL(CG.SHOP_PRICE, 9999) AS SHOP_PRICE,  ");
			sql.append("NVL(CGB.B_NAME, 'N/A') AS B_NAME,NVL(CG.G_UNIT, 'N/A') AS G_UNIT,  ");
			sql.append("NVL(CG.PACK1, 'N/A') AS PACK1,NVL(CG.PACK1_NUM, 0) AS PACK1_NUM,  ");
			sql.append("NVL(CG.PACK2, 'N/A') AS PACK2,NVL(CG.PACK2_NUM, 0) AS PACK2_NUM,  ");
			sql.append("NVL(CG.PACK3, 'N/A') AS PACK3,NVL(CG.PACK3_NUM, 0) AS PACK3_NUM,  ");
			sql.append("NVL(CG.PACK4, 'N/A') AS PACK4,NVL(CG.PACK4_NUM, 0) AS PACK4_NUM,  ");
			sql.append("NVL(CGI.P_URL, 'nourl') AS P_URL ");
			sql.append("FROM  ");
			sql.append("CT_GOODS cg  ");
			sql.append("LEFT JOIN  ");
			sql.append("CT_GOODS_BRAND cgb  ");
			sql.append("ON  ");
			sql.append("CG.B_ID = CGB.B_ID  ");
			sql.append("LEFT JOIN  ");
			sql.append("CT_GOODS_IMG cgi  ");
			sql.append("ON  ");
			sql.append("CGI.G_ID = CG.G_ID  ");
			sql.append("WHERE  ");
			sql.append("EXISTS(  ");
			sql.append("SELECT  ");
			sql.append("C_ID  ");
			sql.append("FROM  ");
			sql.append("CT_GOODS_CATEGORY  ");
			sql.append("WHERE  ");
			sql.append("PARENT_ID="+pid+"  ");
			sql.append("AND  ");
			sql.append("C_ID = CG.C_ID  ");
			sql.append(")  ");
			sql.append("AND  ");
			sql.append("(CG.G_NAME LIKE '%"+kword+"%'  ");
			sql.append("OR  ");
			sql.append("CG.G_SN LIKE '%"+kword+"%')  ");
			sql.append("ORDER BY  ");
			sql.append("CG.G_ID  ");
		} else {
			if ((price1 != null && price2 != null) && (price1.length() > 0 && price2.length() > 0)) {
				sql.append("SELECT  ");
				sql.append("CG.G_ID AS G_ID,NVL(CG.G_NAME, 'N/A') AS G_NAME,NVL(CG.G_SN, 'N/A') AS G_SN,NVL(CG.MARKET_PRICE, 9999) AS MARKET_PRICE,  ");
				sql.append("NVL(CG.PROMOTE_PRICE, 9999) AS PROMOTE_PRICE,NVL(CG.SHOP_PRICE, 9999) AS SHOP_PRICE,  ");
				sql.append("NVL(CGB.B_NAME, 'N/A') AS B_NAME,NVL(CG.G_UNIT, 'N/A') AS G_UNIT,  ");
				sql.append("NVL(CG.PACK1, 'N/A') AS PACK1,NVL(CG.PACK1_NUM, 0) AS PACK1_NUM,  ");
				sql.append("NVL(CG.PACK2, 'N/A') AS PACK2,NVL(CG.PACK2_NUM, 0) AS PACK2_NUM,  ");
				sql.append("NVL(CG.PACK3, 'N/A') AS PACK3,NVL(CG.PACK3_NUM, 0) AS PACK3_NUM,  ");
				sql.append("NVL(CG.PACK4, 'N/A') AS PACK4,NVL(CG.PACK4_NUM, 0) AS PACK4_NUM,  ");
				sql.append("NVL(CGI.P_URL, 'nourl') AS P_URL ");
				sql.append("FROM  ");
				sql.append("CT_GOODS cg  ");
				sql.append("LEFT JOIN  ");
				sql.append("CT_GOODS_BRAND cgb  ");
				sql.append("ON  ");
				sql.append("CG.B_ID = CGB.B_ID  ");
				sql.append("LEFT JOIN  ");
				sql.append("CT_GOODS_IMG cgi  ");
				sql.append("ON  ");
				sql.append("CGI.G_ID = CG.G_ID  ");
				sql.append("WHERE  ");
				sql.append("EXISTS(  ");
				sql.append("SELECT  ");
				sql.append("C_ID  ");
				sql.append("FROM  ");
				sql.append("CT_GOODS_CATEGORY  ");
				sql.append("WHERE  ");
				sql.append("C_ID = CG.C_ID  ");
				sql.append(")  ");
				sql.append("AND ");
				sql.append("CG.SHOP_PRICE between " + price1 + " and " + price2);
				sql.append("ORDER BY  ");
				sql.append("CG.G_ID  ");
			} else {
				sql.append("SELECT  ");
				sql.append("CG.G_ID AS G_ID,NVL(CG.G_NAME, 'N/A') AS G_NAME,NVL(CG.G_SN, 'N/A') AS G_SN,NVL(CG.MARKET_PRICE, 9999) AS MARKET_PRICE,  ");
				sql.append("NVL(CG.PROMOTE_PRICE, 9999) AS PROMOTE_PRICE,NVL(CG.SHOP_PRICE, 9999) AS SHOP_PRICE,  ");
				sql.append("NVL(CGB.B_NAME, 'N/A') AS B_NAME,NVL(CG.G_UNIT, 'N/A') AS G_UNIT,  ");
				sql.append("NVL(CG.PACK1, 'N/A') AS PACK1,NVL(CG.PACK1_NUM, 0) AS PACK1_NUM,  ");
				sql.append("NVL(CG.PACK2, 'N/A') AS PACK2,NVL(CG.PACK2_NUM, 0) AS PACK2_NUM,  ");
				sql.append("NVL(CG.PACK3, 'N/A') AS PACK3,NVL(CG.PACK3_NUM, 0) AS PACK3_NUM,  ");
				sql.append("NVL(CG.PACK4, 'N/A') AS PACK4,NVL(CG.PACK4_NUM, 0) AS PACK4_NUM,  ");
				sql.append("NVL(CGI.P_URL, 'nourl') AS P_URL ");
				sql.append("FROM  ");
				sql.append("CT_GOODS cg  ");
				sql.append("LEFT JOIN  ");
				sql.append("CT_GOODS_BRAND cgb  ");
				sql.append("ON  ");
				sql.append("CG.B_ID = CGB.B_ID  ");
				sql.append("LEFT JOIN  ");
				sql.append("CT_GOODS_IMG cgi  ");
				sql.append("ON  ");
				sql.append("CGI.G_ID = CG.G_ID  ");
				sql.append("WHERE  ");
				sql.append("EXISTS(  ");
				sql.append("SELECT  ");
				sql.append("C_ID  ");
				sql.append("FROM  ");
				sql.append("CT_GOODS_CATEGORY  ");
				sql.append("WHERE  ");
				sql.append("C_ID = CG.C_ID  ");
				sql.append(")  ");
				if(kword!= null){
					sql.append("AND  ");
					sql.append("(CG.G_NAME LIKE '%"+kword+"%'  ");
					sql.append("OR  ");
					sql.append("CG.G_SN LIKE '%"+kword+"%')  ");
				}
				sql.append("ORDER BY  ");
				sql.append("CG.G_ID  ");
			}

		}
		SQLQuery query= getSession().createSQLQuery(sql.toString());
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.searchCount(pid,kword));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		goodsList = query.list();
		 
		return goodsList;
	}
	
	public Long searchCount(Long pid,String kword){
		StringBuffer sql = new StringBuffer();
		if(pid != null){
			sql.append("SELECT  ");
			sql.append("count(*)  ");
			sql.append("FROM  ");
			sql.append("CT_GOODS cg  ");
			sql.append("WHERE  ");
			sql.append("EXISTS(  ");
			sql.append("SELECT  ");
			sql.append("C_ID  ");
			sql.append("FROM  ");
			sql.append("CT_GOODS_CATEGORY  ");
			sql.append("WHERE  ");
			sql.append("PARENT_ID="+pid+"  ");
			sql.append("AND  ");
			sql.append("C_ID = CG.C_ID  ");
			sql.append(")  ");
			sql.append("AND  ");
			sql.append("(CG.G_NAME LIKE '%"+kword+"%'  ");
			sql.append("OR  ");
			sql.append("CG.G_SN LIKE '%"+kword+"%')  ");
		} else {
			sql.append("SELECT  ");
			sql.append("count(*)  ");
			sql.append("FROM  ");
			sql.append("CT_GOODS cg  ");
			sql.append("WHERE  ");
			sql.append("EXISTS(  ");
			sql.append("SELECT  ");
			sql.append("C_ID  ");
			sql.append("FROM  ");
			sql.append("CT_GOODS_CATEGORY  ");
			sql.append("WHERE  ");
			sql.append("C_ID = CG.C_ID  ");
			sql.append(")  ");
			sql.append("AND  ");
			sql.append("(CG.G_NAME LIKE '%"+kword+"%'  ");
			sql.append("OR  ");
			sql.append("CG.G_SN LIKE '%"+kword+"%')  ");
		}
		BigDecimal ct = (BigDecimal) getSession().createSQLQuery(sql.toString()).uniqueResult();
		Long count = ct.longValue();
		return count;
	}
	
	public Long countByCId(Long cid) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtGoods as cg where 1=1 ";
		if(cid != null){
			hql+=" and cg.CId=" + cid;
		}
		Long count= (Long)getSession().createQuery(hql).uniqueResult();
		 
		return count;
	}
	
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from ViewGoodsList";
		if (str !=null)
			hql+=str;
		 
		Long count= (Long)getSession().createQuery(hql).uniqueResult();
		 
		return count;
	}

	@Override
	public List<?> callProBrand(Long pid, String kword,String price1, String price2) throws SQLException {
		List<?> brandList = null;
		StringBuffer sql = new StringBuffer();
		if (pid != null){
			sql.append("SELECT ");
			sql.append("DISTINCT(CG.B_ID),CGB.B_NAME ");
			sql.append("FROM ");
			sql.append("CT_GOODS cg ");
			sql.append("LEFT JOIN ");
			sql.append("CT_GOODS_BRAND cgb ");
			sql.append("ON ");
			sql.append("CG.B_ID = CGB.B_ID ");
			sql.append("WHERE ");
			sql.append("EXISTS( ");
			sql.append("SELECT ");
			sql.append("C_ID ");
			sql.append("FROM ");
			sql.append("CT_GOODS_CATEGORY ");
			sql.append("WHERE ");
			sql.append("PARENT_ID="+pid+" ");
			sql.append("AND ");
			sql.append("C_ID = CG.C_ID ");
			sql.append(") ");
			sql.append("AND ");
			sql.append("CG.B_ID IS NOT NULL ");
			sql.append("AND ");
			sql.append("(CG.G_NAME LIKE '%"+kword+"%' ");
			sql.append("OR ");
			sql.append("CG.G_SN LIKE '%"+kword+"%') ");
			sql.append("ORDER BY ");
			sql.append("CG.B_ID ");
		} else {
			if(kword != null && kword.equals("price")){
				sql.append("SELECT ");
				sql.append("DISTINCT(CG.B_ID),CGB.B_NAME ");
				sql.append("FROM ");
				sql.append("CT_GOODS cg ");
				sql.append("LEFT JOIN ");
				sql.append("CT_GOODS_BRAND cgb ");
				sql.append("ON ");
				sql.append("CG.B_ID = CGB.B_ID ");
				sql.append("WHERE ");
				sql.append("EXISTS( ");
				sql.append("SELECT ");
				sql.append("C_ID ");
				sql.append("FROM ");
				sql.append("CT_GOODS_CATEGORY ");
				sql.append("WHERE ");
				sql.append("C_ID = CG.C_ID ");
				sql.append(") ");
				sql.append("AND ");
				sql.append("CG.B_ID IS NOT NULL ");
				sql.append("ORDER BY ");
				sql.append("CG.B_ID ");
			} else {
				if(price1 != null && price2 != null){
					sql.append("SELECT ");
					sql.append("DISTINCT(CG.B_ID),CGB.B_NAME ");
					sql.append("FROM ");
					sql.append("CT_GOODS cg ");
					sql.append("LEFT JOIN ");
					sql.append("CT_GOODS_BRAND cgb ");
					sql.append("ON ");
					sql.append("CG.B_ID = CGB.B_ID ");
					sql.append("WHERE ");
					sql.append("EXISTS( ");
					sql.append("SELECT ");
					sql.append("C_ID ");
					sql.append("FROM ");
					sql.append("CT_GOODS_CATEGORY ");
					sql.append("WHERE ");
					sql.append("C_ID = CG.C_ID ");
					sql.append(") ");
					sql.append("AND ");
					sql.append("CG.B_ID IS NOT NULL ");
					sql.append("AND ");
					sql.append("CG.SHOP_PRICE between " + price1 + " and " + price2);
					sql.append("ORDER BY ");
					sql.append("CG.B_ID ");
				} else {
					sql.append("SELECT ");
					sql.append("DISTINCT(CG.B_ID),CGB.B_NAME ");
					sql.append("FROM ");
					sql.append("CT_GOODS cg ");
					sql.append("LEFT JOIN ");
					sql.append("CT_GOODS_BRAND cgb ");
					sql.append("ON ");
					sql.append("CG.B_ID = CGB.B_ID ");
					sql.append("WHERE ");
					sql.append("EXISTS( ");
					sql.append("SELECT ");
					sql.append("C_ID ");
					sql.append("FROM ");
					sql.append("CT_GOODS_CATEGORY ");
					sql.append("WHERE ");
					sql.append("C_ID = CG.C_ID ");
					sql.append(") ");
					sql.append("AND ");
					sql.append("CG.B_ID IS NOT NULL ");
					sql.append("AND ");
					sql.append("(CG.G_NAME LIKE '%"+kword+"%' ");
					sql.append("OR ");
					sql.append("CG.G_SN LIKE '%"+kword+"%') ");
					sql.append("ORDER BY ");
					sql.append("CG.B_ID ");
				}
			}

		}
		SQLQuery query = getSession().createSQLQuery(sql.toString());
		brandList = query.list();
		return brandList;
	}

	@Override
	public List<CtGoodsCategory> getSubCate(Long pid) {
		// TODO Auto-generated method stub
		List<CtGoodsCategory> cateList = null;
		String hql = "from CtGoodsCategory as cate ";
		if (pid != null){
			hql = "from CtGoodsCategory as cate where cate.parentId = "+pid;
		}
		Query query= getSession().createQuery(hql);
		cateList = query.list();
		 
		return cateList;
	}

	@Override
	public List<?> callProType(Long pid, String kword,String price1,String price2) {
		// TODO Auto-generated method stub
		List<?> typeList = null;
		StringBuffer sql = new StringBuffer();
		if (pid != null){
			sql.append("SELECT ");
			sql.append("DISTINCT(cgt.G_T_ID),cgt.TYPE_NAME ");
			sql.append("FROM ");
			sql.append("CT_GOODS cg ");
			sql.append("LEFT JOIN ");
			sql.append("CT_GOODS_ATTRIBUTE_RELATION cgar ");
			sql.append("ON ");
			sql.append("CG.G_ID = CGAR.G_ID ");
			sql.append("LEFT JOIN ");
			sql.append("CT_GOODS_ATTRIBUTE cga  ");
			sql.append("ON ");
			sql.append("cga.ATTR_ID = cgar.ATTR_ID ");
			sql.append("LEFT JOIN ");
			sql.append("CT_GOODS_TYPE cgt ");
			sql.append("ON ");
			sql.append("cgt.G_T_ID = cga.G_T_ID ");
			sql.append("WHERE ");
			sql.append("EXISTS( ");
			sql.append("SELECT ");
			sql.append("C_ID ");
			sql.append("FROM ");
			sql.append("CT_GOODS_CATEGORY ");
			sql.append("WHERE ");
			sql.append("PARENT_ID="+pid+" ");
			sql.append("AND ");
			sql.append("C_ID = CG.C_ID ");
			sql.append(") ");
			sql.append("AND ");
			sql.append("cgt.G_T_ID IS NOT NULL ");
			sql.append("AND ");
			sql.append("(CG.G_NAME LIKE '%"+kword+"%' ");
			sql.append("OR ");
			sql.append("CG.G_SN LIKE '%"+kword+"%') ");
			sql.append("ORDER BY ");
			sql.append("cgt.G_T_ID ");
		} else {
			if(kword != null && kword.equals("price")){
				sql.append("SELECT ");
				sql.append("DISTINCT(CG.B_ID),CGB.B_NAME ");
				sql.append("FROM ");
				sql.append("CT_GOODS cg ");
				sql.append("LEFT JOIN ");
				sql.append("CT_GOODS_BRAND cgb ");
				sql.append("ON ");
				sql.append("CG.B_ID = CGB.B_ID ");
				sql.append("WHERE ");
				sql.append("EXISTS( ");
				sql.append("SELECT ");
				sql.append("C_ID ");
				sql.append("FROM ");
				sql.append("CT_GOODS_CATEGORY ");
				sql.append("WHERE ");
				sql.append("C_ID = CG.C_ID ");
				sql.append(") ");
				sql.append("AND ");
				sql.append("CG.B_ID IS NOT NULL ");
				sql.append("ORDER BY ");
				sql.append("CG.B_ID ");
			} else {
				if(price1 != null && price2 != null){
					sql.append("SELECT ");
					sql.append("DISTINCT(CG.B_ID),CGB.B_NAME ");
					sql.append("FROM ");
					sql.append("CT_GOODS cg ");
					sql.append("LEFT JOIN ");
					sql.append("CT_GOODS_BRAND cgb ");
					sql.append("ON ");
					sql.append("CG.B_ID = CGB.B_ID ");
					sql.append("WHERE ");
					sql.append("EXISTS( ");
					sql.append("SELECT ");
					sql.append("C_ID ");
					sql.append("FROM ");
					sql.append("CT_GOODS_CATEGORY ");
					sql.append("WHERE ");
					sql.append("C_ID = CG.C_ID ");
					sql.append(") ");
					sql.append("AND ");
					sql.append("CG.B_ID IS NOT NULL ");
					sql.append("AND ");
					sql.append("CG.SHOP_PRICE between " + price1 + " and " + price2);
					sql.append("ORDER BY ");
					sql.append("CG.B_ID ");
				} else {
					sql.append("SELECT ");
					sql.append("DISTINCT(CG.B_ID),CGB.B_NAME ");
					sql.append("FROM ");
					sql.append("CT_GOODS cg ");
					sql.append("LEFT JOIN ");
					sql.append("CT_GOODS_BRAND cgb ");
					sql.append("ON ");
					sql.append("CG.B_ID = CGB.B_ID ");
					sql.append("WHERE ");
					sql.append("EXISTS( ");
					sql.append("SELECT ");
					sql.append("C_ID ");
					sql.append("FROM ");
					sql.append("CT_GOODS_CATEGORY ");
					sql.append("WHERE ");
					sql.append("C_ID = CG.C_ID ");
					sql.append(") ");
					sql.append("AND ");
					sql.append("CG.B_ID IS NOT NULL ");
					sql.append("AND ");
					sql.append("(CG.G_NAME LIKE '%"+kword+"%' ");
					sql.append("OR ");
					sql.append("CG.G_SN LIKE '%"+kword+"%') ");
					sql.append("ORDER BY ");
					sql.append("CG.B_ID ");
				}
			}
		}
		SQLQuery query = getSession().createSQLQuery(sql.toString());
		typeList = query.list();
		return typeList;
	}

	@Override
	public List<?> callProAttr(Long pid, String kword,String price1,String price2) {
		// TODO Auto-generated method stub
		List<?> attrList = null;
		StringBuffer sql = new StringBuffer();
		if(pid != null){
			sql.append("SELECT ");
			sql.append("DISTINCT(CGAR.ATTR_ID),cga.ATTR_NAME ");
			sql.append("FROM ");
			sql.append("CT_GOODS cg ");
			sql.append("LEFT JOIN ");
			sql.append("CT_GOODS_ATTRIBUTE_RELATION cgar ");
			sql.append("ON ");
			sql.append("CG.G_ID = CGAR.G_ID ");
			sql.append("LEFT JOIN ");
			sql.append("CT_GOODS_ATTRIBUTE cga  ");
			sql.append("ON ");
			sql.append("cga.ATTR_ID = cgar.ATTR_ID ");
			sql.append("LEFT JOIN ");
			sql.append("CT_GOODS_TYPE cgt ");
			sql.append("ON ");
			sql.append("cgt.G_T_ID = cga.G_T_ID ");
			sql.append("WHERE ");
			sql.append("EXISTS( ");
			sql.append("SELECT ");
			sql.append("C_ID ");
			sql.append("FROM ");
			sql.append("CT_GOODS_CATEGORY ");
			sql.append("WHERE ");
			sql.append("PARENT_ID="+pid+" ");
			sql.append("AND ");
			sql.append("C_ID = CG.C_ID ");
			sql.append(") ");
			sql.append("AND ");
			sql.append("cgt.G_T_ID IS NOT NULL ");
			sql.append("AND ");
			sql.append("(CG.G_NAME LIKE '%"+kword+"%' ");
			sql.append("OR ");
			sql.append("CG.G_SN LIKE '%"+kword+"%') ");
			sql.append("ORDER BY ");
			sql.append("CGAR.ATTR_ID ");
		} else {
			if(kword != null && kword.equals("price")){
				sql.append("SELECT ");
				sql.append("DISTINCT(CG.B_ID),CGB.B_NAME ");
				sql.append("FROM ");
				sql.append("CT_GOODS cg ");
				sql.append("LEFT JOIN ");
				sql.append("CT_GOODS_BRAND cgb ");
				sql.append("ON ");
				sql.append("CG.B_ID = CGB.B_ID ");
				sql.append("WHERE ");
				sql.append("EXISTS( ");
				sql.append("SELECT ");
				sql.append("C_ID ");
				sql.append("FROM ");
				sql.append("CT_GOODS_CATEGORY ");
				sql.append("WHERE ");
				sql.append("C_ID = CG.C_ID ");
				sql.append(") ");
				sql.append("AND ");
				sql.append("CG.B_ID IS NOT NULL ");
				sql.append("ORDER BY ");
				sql.append("CG.B_ID ");
			} else {
				if(price1 != null && price2 != null){
					sql.append("SELECT ");
					sql.append("DISTINCT(CG.B_ID),CGB.B_NAME ");
					sql.append("FROM ");
					sql.append("CT_GOODS cg ");
					sql.append("LEFT JOIN ");
					sql.append("CT_GOODS_BRAND cgb ");
					sql.append("ON ");
					sql.append("CG.B_ID = CGB.B_ID ");
					sql.append("WHERE ");
					sql.append("EXISTS( ");
					sql.append("SELECT ");
					sql.append("C_ID ");
					sql.append("FROM ");
					sql.append("CT_GOODS_CATEGORY ");
					sql.append("WHERE ");
					sql.append("C_ID = CG.C_ID ");
					sql.append(") ");
					sql.append("AND ");
					sql.append("CG.B_ID IS NOT NULL ");
					sql.append("AND ");
					sql.append("CG.SHOP_PRICE between " + price1 + " and " + price2);
					sql.append("ORDER BY ");
					sql.append("CG.B_ID ");
				} else {
					sql.append("SELECT ");
					sql.append("DISTINCT(CG.B_ID),CGB.B_NAME ");
					sql.append("FROM ");
					sql.append("CT_GOODS cg ");
					sql.append("LEFT JOIN ");
					sql.append("CT_GOODS_BRAND cgb ");
					sql.append("ON ");
					sql.append("CG.B_ID = CGB.B_ID ");
					sql.append("WHERE ");
					sql.append("EXISTS( ");
					sql.append("SELECT ");
					sql.append("C_ID ");
					sql.append("FROM ");
					sql.append("CT_GOODS_CATEGORY ");
					sql.append("WHERE ");
					sql.append("C_ID = CG.C_ID ");
					sql.append(") ");
					sql.append("AND ");
					sql.append("CG.B_ID IS NOT NULL ");
					sql.append("AND ");
					sql.append("(CG.G_NAME LIKE '%"+kword+"%' ");
					sql.append("OR ");
					sql.append("CG.G_SN LIKE '%"+kword+"%') ");
					sql.append("ORDER BY ");
					sql.append("CG.B_ID ");
				}
			}
		}
		SQLQuery query = getSession().createSQLQuery(sql.toString());
		attrList = query.list();
		return attrList;
	}

	@Override
	public List<?> callProAttrVal(Long pid, String kword,String price1,String price2) {
		// TODO Auto-generated method stub
		List<?> attrvalList = null;
		StringBuffer sql = new StringBuffer();
		if(pid!=null){
			sql.append("SELECT ");
			sql.append("CGAR.ATTR_ID,cga.ATTR_NAME,CGAR.ATTR_VALUE ");
			sql.append("FROM ");
			sql.append("CT_GOODS cg ");
			sql.append("LEFT JOIN ");
			sql.append("CT_GOODS_ATTRIBUTE_RELATION cgar ");
			sql.append("ON ");
			sql.append("CG.G_ID = CGAR.G_ID ");
			sql.append("LEFT JOIN ");
			sql.append("CT_GOODS_ATTRIBUTE cga  ");
			sql.append("ON ");
			sql.append("cga.ATTR_ID = cgar.ATTR_ID ");
			sql.append("LEFT JOIN ");
			sql.append("CT_GOODS_TYPE cgt ");
			sql.append("ON ");
			sql.append("cgt.G_T_ID = cga.G_T_ID ");
			sql.append("WHERE ");
			sql.append("EXISTS( ");
			sql.append("SELECT ");
			sql.append("C_ID ");
			sql.append("FROM ");
			sql.append("CT_GOODS_CATEGORY ");
			sql.append("WHERE ");
			sql.append("PARENT_ID="+pid+" ");
			sql.append("AND ");
			sql.append("C_ID = CG.C_ID ");
			sql.append(") ");
			sql.append("AND ");
			sql.append("cgt.G_T_ID IS NOT NULL ");
			sql.append("AND ");
			sql.append("(CG.G_NAME LIKE '%"+kword+"%' ");
			sql.append("OR ");
			sql.append("CG.G_SN LIKE '%"+kword+"%') ");
			sql.append("ORDER BY ");
			sql.append("CGAR.ATTR_ID ");
		} else{
			if(kword != null && kword.equals("price")){
				sql.append("SELECT ");
				sql.append("DISTINCT(CG.B_ID),CGB.B_NAME ");
				sql.append("FROM ");
				sql.append("CT_GOODS cg ");
				sql.append("LEFT JOIN ");
				sql.append("CT_GOODS_BRAND cgb ");
				sql.append("ON ");
				sql.append("CG.B_ID = CGB.B_ID ");
				sql.append("WHERE ");
				sql.append("EXISTS( ");
				sql.append("SELECT ");
				sql.append("C_ID ");
				sql.append("FROM ");
				sql.append("CT_GOODS_CATEGORY ");
				sql.append("WHERE ");
				sql.append("C_ID = CG.C_ID ");
				sql.append(") ");
				sql.append("AND ");
				sql.append("CG.B_ID IS NOT NULL ");
				sql.append("ORDER BY ");
				sql.append("CG.B_ID ");
			} else {
				if(price1 != null && price2 != null){
					sql.append("SELECT ");
					sql.append("DISTINCT(CG.B_ID),CGB.B_NAME ");
					sql.append("FROM ");
					sql.append("CT_GOODS cg ");
					sql.append("LEFT JOIN ");
					sql.append("CT_GOODS_BRAND cgb ");
					sql.append("ON ");
					sql.append("CG.B_ID = CGB.B_ID ");
					sql.append("WHERE ");
					sql.append("EXISTS( ");
					sql.append("SELECT ");
					sql.append("C_ID ");
					sql.append("FROM ");
					sql.append("CT_GOODS_CATEGORY ");
					sql.append("WHERE ");
					sql.append("C_ID = CG.C_ID ");
					sql.append(") ");
					sql.append("AND ");
					sql.append("CG.B_ID IS NOT NULL ");
					sql.append("AND ");
					sql.append("CG.SHOP_PRICE between " + price1 + " and " + price2);
					sql.append("ORDER BY ");
					sql.append("CG.B_ID ");
				} else {
					sql.append("SELECT ");
					sql.append("DISTINCT(CG.B_ID),CGB.B_NAME ");
					sql.append("FROM ");
					sql.append("CT_GOODS cg ");
					sql.append("LEFT JOIN ");
					sql.append("CT_GOODS_BRAND cgb ");
					sql.append("ON ");
					sql.append("CG.B_ID = CGB.B_ID ");
					sql.append("WHERE ");
					sql.append("EXISTS( ");
					sql.append("SELECT ");
					sql.append("C_ID ");
					sql.append("FROM ");
					sql.append("CT_GOODS_CATEGORY ");
					sql.append("WHERE ");
					sql.append("C_ID = CG.C_ID ");
					sql.append(") ");
					sql.append("AND ");
					sql.append("CG.B_ID IS NOT NULL ");
					sql.append("AND ");
					sql.append("(CG.G_NAME LIKE '%"+kword+"%' ");
					sql.append("OR ");
					sql.append("CG.G_SN LIKE '%"+kword+"%') ");
					sql.append("ORDER BY ");
					sql.append("CG.B_ID ");
				}
			}
		}
		SQLQuery query = getSession().createSQLQuery(sql.toString());
		attrvalList = query.list();
		return attrvalList;
	}

	public List<CtGoods> getGoodsByGname(String Gname) {
		// TODO Auto-generated method stub
		 
		String hql = "select G_ID,G_NAME from CT_GOODS cg where G_NAME like '%"+Gname+"%' and rownum <=10";
		Query query = getSession().createSQLQuery(hql);
		List<CtGoods> list;
		list = query.list();
		 
		return list;
	}

	@Override
	public Object getOneGoodsByGname(String Gname) {
		// TODO Auto-generated method stub
		 
		String hql = "select G_ID,G_NAME from CT_GOODS cg where G_NAME ='"+Gname+"' or G_SN='"+Gname+"' and rownum <=10";
		Query query = getSession().createSQLQuery(hql);
		List<CtGoods> list;
		Object obj;
		CtGoods cg;
		list = query.list();
		 
		if(list.size() > 0){
			obj = list.get(0);
		}else {
			obj = null;
		}
		return obj;
	}

	@Override
	public List<?> getGoods(Long gid) {
		// TODO Auto-generated method stub
		List<?> goods = null;
		String hql = "from ViewGoodsList as goods where goods.GId="+gid;
		 
		Query query= getSession().createQuery(hql);
		goods = query.list();
		 
		return goods;
	}
	
	@Override
	public CtGoods getGood(Long gid) {
		// TODO Auto-generated method stub
		CtGoods goods = (CtGoods)getSession().get(CtGoods.class, gid);
		
		return goods;
		
		
	}

	

	@Override
	public List<CtGoodsImg> getImg(Long gid) {
		// TODO Auto-generated method stub
		List<CtGoodsImg> imgList = null;
		String hql = "from CtGoodsImg as img where img.GId="+ gid + " order by img.coverImg";
		 
		Query query= getSession().createQuery(hql);
		imgList = query.list();
		 
		return imgList;
	}

	@Override
	public Integer getGNum(Long gid) {
		// TODO Auto-generated method stub
		Integer GNum = null;
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT ");
		sql.append("NVL(SUM(CGN.G_NUM), 0) ");
		sql.append("FROM ");
		sql.append("CT_GOODS_NUM cgn ");
		sql.append("WHERE ");
		sql.append("CGN.G_ID = "+gid);
		 
		SQLQuery query = getSession().createSQLQuery(sql.toString());
		BigDecimal db = (BigDecimal) query.uniqueResult();
		GNum = db.intValue();
		return GNum;
	}

	@Override
	public List<CtGoodsDetail> getGoodsIntroduce(Long id) {
		// TODO Auto-generated method stub
		List<CtGoodsDetail> detailList = null;
		String hql = "from CtGoodsDetail as det where det.GId =" + id;
		 
		Query query = getSession().createQuery(hql);
		detailList = query.list();
		 
		return detailList;
	}

	@Override
	public List<?> getGoodsReplaces(Long gid) {
		// TODO Auto-generated method stub
		List<?> replaceList = null;
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT  ");
		sql.append("CG.G_ID AS G_ID,NVL(CG.G_NAME, 'N/A') AS G_NAME,NVL(CG.G_SN, 'N/A') AS G_SN,NVL(CG.MARKET_PRICE, 9999) AS MARKET_PRICE,  ");
		sql.append("NVL(CG.PROMOTE_PRICE, 9999) AS PROMOTE_PRICE,NVL(CG.SHOP_PRICE, 9999) AS SHOP_PRICE,  ");
		sql.append("NVL(CGB.B_NAME, 'N/A') AS B_NAME,NVL(CG.G_UNIT, 'N/A') AS G_UNIT,  ");
		sql.append("NVL(CG.PACK1, 'N/A') AS PACK1,NVL(CG.PACK1_NUM, 0) AS PACK1_NUM,  ");
		sql.append("NVL(CG.PACK2, 'N/A') AS PACK2,NVL(CG.PACK2_NUM, 0) AS PACK2_NUM,  ");
		sql.append("NVL(CG.PACK3, 'N/A') AS PACK3,NVL(CG.PACK3_NUM, 0) AS PACK3_NUM,  ");
		sql.append("NVL(CG.PACK4, 'N/A') AS PACK4,NVL(CG.PACK4_NUM, 0) AS PACK4_NUM,  ");
		sql.append("NVL(CGI.P_URL, 'nourl') AS P_URL ");
		sql.append("FROM  ");
		sql.append("CT_GOODS cg  ");
		sql.append("LEFT JOIN  ");
		sql.append("CT_GOODS_BRAND cgb  ");
		sql.append("ON  ");
		sql.append("CG.B_ID = CGB.B_ID  ");
		sql.append("LEFT JOIN  ");
		sql.append("CT_GOODS_IMG cgi  ");
		sql.append("ON  ");
		sql.append("CGI.G_ID = CG.G_ID  ");
		sql.append("WHERE ");
		sql.append("EXISTS(  ");
		sql.append("SELECT  ");
		sql.append("CGR.REPLACE_G_ID  ");
		sql.append("FROM  ");
		sql.append("CT_GOODS_REPLACE cgr  ");
		sql.append("WHERE  ");
		sql.append("CGR.G_ID = "+gid);
		sql.append(" AND  ");
		sql.append("CGR.REPLACE_G_ID = CG.G_ID)  ");
		 
		SQLQuery query = getSession().createSQLQuery(sql.toString());
		replaceList = query.list();
		 
		return replaceList;
	}

	@Override
	public List<?> getGoodsLink(Long gid) {
		// TODO Auto-generated method stub
		List<?> linkList = null;
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT  ");
		sql.append("CG.G_ID AS G_ID,NVL(CG.G_NAME, 'N/A') AS G_NAME,NVL(CG.G_SN, 'N/A') AS G_SN,NVL(CG.MARKET_PRICE, 9999) AS MARKET_PRICE,  ");
		sql.append("NVL(CG.PROMOTE_PRICE, 9999) AS PROMOTE_PRICE,NVL(CG.SHOP_PRICE, 9999) AS SHOP_PRICE,  ");
		sql.append("NVL(CGB.B_NAME, 'N/A') AS B_NAME,NVL(CG.G_UNIT, 'N/A') AS G_UNIT,  ");
		sql.append("NVL(CG.PACK1, 'N/A') AS PACK1,NVL(CG.PACK1_NUM, 0) AS PACK1_NUM,  ");
		sql.append("NVL(CG.PACK2, 'N/A') AS PACK2,NVL(CG.PACK2_NUM, 0) AS PACK2_NUM,  ");
		sql.append("NVL(CG.PACK3, 'N/A') AS PACK3,NVL(CG.PACK3_NUM, 0) AS PACK3_NUM,  ");
		sql.append("NVL(CG.PACK4, 'N/A') AS PACK4,NVL(CG.PACK4_NUM, 0) AS PACK4_NUM,  ");
		sql.append("NVL(CGI.P_URL, 'nourl') AS P_URL ");
		sql.append("FROM  ");
		sql.append("CT_GOODS cg  ");
		sql.append("LEFT JOIN  ");
		sql.append("CT_GOODS_BRAND cgb  ");
		sql.append("ON  ");
		sql.append("CG.B_ID = CGB.B_ID  ");
		sql.append("LEFT JOIN  ");
		sql.append("CT_GOODS_IMG cgi  ");
		sql.append("ON  ");
		sql.append("CGI.G_ID = CG.G_ID  ");
		sql.append("WHERE ");
		sql.append("EXISTS(  ");
		sql.append("SELECT  ");
		sql.append("CGL.LINK_G_ID  ");
		sql.append("FROM  ");
		sql.append("CT_GOODS_LINK cgl  ");
		sql.append("WHERE  ");
		sql.append("CGL.G_ID = "+gid);
		sql.append(" AND  ");
		sql.append("CGL.LINK_G_ID = CG.G_ID)  ");
		 
		SQLQuery query = getSession().createSQLQuery(sql.toString());
		linkList = query.list();
		 
		return linkList;
	}

	@Override
	public List<?> getBrandByCId(Long cid) {
		// TODO Auto-generated method stub
		List<?> brandList = null;
					StringBuffer sql = new StringBuffer();
					sql.append("SELECT ");
					sql.append("DISTINCT(CG.B_ID),CGB.B_NAME ");
					sql.append("FROM ");
					sql.append("CT_GOODS cg ");
					sql.append("LEFT JOIN ");
					sql.append("CT_GOODS_BRAND cgb ");
					sql.append("ON ");
					sql.append("CG.B_ID = CGB.B_ID ");
					sql.append("WHERE ");
					sql.append("CG.C_ID = "+cid);
					sql.append(" AND ");
					sql.append("CG.B_ID IS NOT NULL ");
					sql.append("ORDER BY ");
					sql.append("CG.B_ID ");
					SQLQuery query = getSession().createSQLQuery(sql.toString());
						brandList = query.list();
		return brandList;
	}

	@Override
	public List<?> getTypeByCId(Long cid) {
		// TODO Auto-generated method stub
		List<?> typeList = null;
		 
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT ");
		sql.append("DISTINCT(cgt.G_T_ID),cgt.TYPE_NAME ");
		sql.append("FROM ");
		sql.append("CT_GOODS cg ");
		sql.append("LEFT JOIN ");
		sql.append("CT_GOODS_ATTRIBUTE_RELATION cgar ");
		sql.append("ON ");
		sql.append("CG.G_ID = CGAR.G_ID ");
		sql.append("LEFT JOIN ");
		sql.append("CT_GOODS_ATTRIBUTE cga  ");
		sql.append("ON ");
		sql.append("cga.ATTR_ID = cgar.ATTR_ID ");
		sql.append("LEFT JOIN ");
		sql.append("CT_GOODS_TYPE cgt ");
		sql.append("ON ");
		sql.append("cgt.G_T_ID = cga.G_T_ID ");
		sql.append("WHERE ");
		sql.append("CG.C_ID = "+cid);
		sql.append(" AND ");
		sql.append("cgt.G_T_ID IS NOT NULL ");
		sql.append("ORDER BY ");
		sql.append("cgt.G_T_ID ");
		SQLQuery query = getSession().createSQLQuery(sql.toString());
		typeList = query.list();
		 
		return typeList;
	}

	@Override
	public List<?> getAttrByCId(Long cid) {
		// TODO Auto-generated method stub
		List<?> attrList = null;
		List<CtGoodsCategory> goodsList1 = new ArrayList<CtGoodsCategory>();
		List goodsListAll = new ArrayList();
		List goodsListAll1 = new ArrayList();
		String hql = " from CtGoodsCategory cc where cc.CId = " + cid;
		Query query1 = getSession().createQuery(hql);
		goodsList1 = query1.list();
		if (goodsList1 != null && goodsList1.size() > 0) {
			if(goodsList1.get(0).getParentId().equals(0L)){
				hql = " from CtGoodsCategory cc where cc.parentId = " + cid;
				query1 = getSession().createQuery(hql);
				List<CtGoodsCategory> findList = query1.list();
				for (CtGoodsCategory ctGoodsCategory : findList) {
					StringBuffer sql = new StringBuffer();
					sql.append("SELECT ");
					sql.append("DISTINCT(CGAR.ATTR_ID),cga.ATTR_NAME ");
					sql.append("FROM ");
					sql.append("CT_GOODS cg ");
					sql.append("LEFT JOIN ");
					sql.append("CT_GOODS_ATTRIBUTE_RELATION cgar ");
					sql.append("ON ");
					sql.append("CG.G_ID = CGAR.G_ID ");
					sql.append("LEFT JOIN ");
					sql.append("CT_GOODS_ATTRIBUTE cga  ");
					sql.append("ON ");
					sql.append("cga.ATTR_ID = cgar.ATTR_ID ");
					sql.append("LEFT JOIN ");
					sql.append("CT_GOODS_TYPE cgt ");
					sql.append("ON ");
					sql.append("cgt.G_T_ID = cga.G_T_ID ");
					sql.append("WHERE ");
					sql.append("CG.C_ID = "+ctGoodsCategory.getCId());
					sql.append(" AND ");
					sql.append("cgt.G_T_ID IS NOT NULL ");
					sql.append("ORDER BY ");
					sql.append("CGAR.ATTR_ID ");
					SQLQuery query = getSession().createSQLQuery(sql.toString());
					attrList = query.list();
					goodsListAll = query.list();
					if(goodsListAll != null && goodsListAll.size() > 0){
						for (Object ctGoodsCategory2 : goodsListAll) {
							goodsListAll1.add(ctGoodsCategory2);
						}
					}
				}
			} else {
				StringBuffer sql = new StringBuffer();
				sql.append("SELECT ");
				sql.append("DISTINCT(CGAR.ATTR_ID),cga.ATTR_NAME ");
				sql.append("FROM ");
				sql.append("CT_GOODS cg ");
				sql.append("LEFT JOIN ");
				sql.append("CT_GOODS_ATTRIBUTE_RELATION cgar ");
				sql.append("ON ");
				sql.append("CG.G_ID = CGAR.G_ID ");
				sql.append("LEFT JOIN ");
				sql.append("CT_GOODS_ATTRIBUTE cga  ");
				sql.append("ON ");
				sql.append("cga.ATTR_ID = cgar.ATTR_ID ");
				sql.append("LEFT JOIN ");
				sql.append("CT_GOODS_TYPE cgt ");
				sql.append("ON ");
				sql.append("cgt.G_T_ID = cga.G_T_ID ");
				sql.append("WHERE ");
				sql.append("CG.C_ID = "+cid);
				sql.append(" AND ");
				sql.append("cgt.G_T_ID IS NOT NULL ");
				sql.append("ORDER BY ");
				sql.append("CGAR.ATTR_ID ");
				SQLQuery query = getSession().createSQLQuery(sql.toString());
				attrList = query.list();
			}
		}
		 
		return attrList;
	}

	@Override
	public List<?> getAttrValByCId(Long cid) {
		// TODO Auto-generated method stub
		List<?> attrvalList = null;
		List<CtGoodsCategory> goodsList1 = new ArrayList<CtGoodsCategory>();
		List goodsListAll = new ArrayList();
		List goodsListAll1 = new ArrayList();
		String hql = " from CtGoodsCategory cc where cc.CId = " + cid;
		Query query1 = getSession().createQuery(hql);
		goodsList1 = query1.list();
		if (goodsList1 != null && goodsList1.size() > 0) {
			if(goodsList1.get(0).getParentId().equals(0L)){
				hql = " from CtGoodsCategory cc where cc.parentId = " + cid;
				query1 = getSession().createQuery(hql);
				List<CtGoodsCategory> findList = query1.list();
				for (CtGoodsCategory ctGoodsCategory : findList) {
					StringBuffer sql = new StringBuffer();
					sql.append("SELECT ");
					sql.append("CGAR.ATTR_ID,cga.ATTR_NAME,CGAR.ATTR_VALUE ");
					sql.append("FROM ");
					sql.append("CT_GOODS cg ");
					sql.append("LEFT JOIN ");
					sql.append("CT_GOODS_ATTRIBUTE_RELATION cgar ");
					sql.append("ON ");
					sql.append("CG.G_ID = CGAR.G_ID ");
					sql.append("LEFT JOIN ");
					sql.append("CT_GOODS_ATTRIBUTE cga  ");
					sql.append("ON ");
					sql.append("cga.ATTR_ID = cgar.ATTR_ID ");
					sql.append("LEFT JOIN ");
					sql.append("CT_GOODS_TYPE cgt ");
					sql.append("ON ");
					sql.append("cgt.G_T_ID = cga.G_T_ID ");
					sql.append("WHERE ");
					sql.append("CG.C_ID = "+ctGoodsCategory.getCId());
					sql.append(" AND ");
					sql.append("cgt.G_T_ID IS NOT NULL ");
					sql.append("ORDER BY ");
					sql.append("CGAR.ATTR_ID ");
					SQLQuery query = getSession().createSQLQuery(sql.toString());
					goodsListAll = query.list();
					if(goodsListAll != null && goodsListAll.size() > 0){
						for (Object ctGoodsCategory2 : goodsListAll) {
							goodsListAll1.add(ctGoodsCategory2);
						}
					}
				}
				attrvalList = goodsListAll1;
			} else {
				StringBuffer sql = new StringBuffer();
				sql.append("SELECT ");
				sql.append("CGAR.ATTR_ID,cga.ATTR_NAME,CGAR.ATTR_VALUE ");
				sql.append("FROM ");
				sql.append("CT_GOODS cg ");
				sql.append("LEFT JOIN ");
				sql.append("CT_GOODS_ATTRIBUTE_RELATION cgar ");
				sql.append("ON ");
				sql.append("CG.G_ID = CGAR.G_ID ");
				sql.append("LEFT JOIN ");
				sql.append("CT_GOODS_ATTRIBUTE cga  ");
				sql.append("ON ");
				sql.append("cga.ATTR_ID = cgar.ATTR_ID ");
				sql.append("LEFT JOIN ");
				sql.append("CT_GOODS_TYPE cgt ");
				sql.append("ON ");
				sql.append("cgt.G_T_ID = cga.G_T_ID ");
				sql.append("WHERE ");
				sql.append("CG.C_ID = "+cid);
				sql.append(" AND ");
				sql.append("cgt.G_T_ID IS NOT NULL ");
				sql.append("ORDER BY ");
				sql.append("CGAR.ATTR_ID ");
				SQLQuery query = getSession().createSQLQuery(sql.toString());
				attrvalList = query.list();
			}
		}
		return attrvalList;
	}

	@Override
	public List<?> searchGoodsByBrand(Long bid, Page page) {
		// TODO Auto-generated method stub
		List<?> goodsList = null;
		 
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT  ");
		sql.append("CG.G_ID AS G_ID,NVL(CG.G_NAME, 'N/A') AS G_NAME,NVL(CG.G_SN, 'N/A') AS G_SN,NVL(CG.MARKET_PRICE, 9999) AS MARKET_PRICE,  ");
		sql.append("NVL(CG.PROMOTE_PRICE, 9999) AS PROMOTE_PRICE,NVL(CG.SHOP_PRICE, 9999) AS SHOP_PRICE,  ");
		sql.append("NVL(CGB.B_NAME, 'N/A') AS B_NAME,NVL(CG.G_UNIT, 'N/A') AS G_UNIT,  ");
		sql.append("NVL(CG.PACK1, 'N/A') AS PACK1,NVL(CG.PACK1_NUM, 0) AS PACK1_NUM,  ");
		sql.append("NVL(CG.PACK2, 'N/A') AS PACK2,NVL(CG.PACK2_NUM, 0) AS PACK2_NUM,  ");
		sql.append("NVL(CG.PACK3, 'N/A') AS PACK3,NVL(CG.PACK3_NUM, 0) AS PACK3_NUM,  ");
		sql.append("NVL(CG.PACK4, 'N/A') AS PACK4,NVL(CG.PACK4_NUM, 0) AS PACK4_NUM,  ");
		sql.append("NVL(CGI.P_URL, 'nourl') AS P_URL ");
		sql.append("FROM  ");
		sql.append("CT_GOODS cg  ");
		sql.append("LEFT JOIN  ");
		sql.append("CT_GOODS_BRAND cgb  ");
		sql.append("ON  ");
		sql.append("CG.B_ID = CGB.B_ID  ");
		sql.append("LEFT JOIN  ");
		sql.append("CT_GOODS_IMG cgi  ");
		sql.append("ON  ");
		sql.append("CGI.G_ID = CG.G_ID  ");
		sql.append("WHERE  ");
		sql.append("CG.B_ID = "+bid);
		sql.append(" ORDER BY  ");
		sql.append("CG.G_ID  ");
		SQLQuery query = getSession().createSQLQuery(sql.toString());
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.searchCountByBId(bid));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		goodsList = query.list();
		 
		return goodsList;
	}
	
	public Long searchCountByBId(Long bid){
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT  ");
		sql.append("count(*)  ");
		sql.append("FROM  ");
		sql.append("CT_GOODS cg  ");
		sql.append("WHERE  ");
		sql.append("CG.B_ID = "+bid);
		 
		BigDecimal ct = (BigDecimal) getSession().createSQLQuery(sql.toString()).uniqueResult();
		Long count = ct.longValue();
		return count;
	}

	@Override
	public List<?> getTypeByBId(Long bid) {
		// TODO Auto-generated method stub
		List<?> typeList = null;
		 
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT ");
		sql.append("DISTINCT(cgt.G_T_ID),cgt.TYPE_NAME ");
		sql.append("FROM ");
		sql.append("CT_GOODS cg ");
		sql.append("LEFT JOIN ");
		sql.append("CT_GOODS_ATTRIBUTE_RELATION cgar ");
		sql.append("ON ");
		sql.append("CG.G_ID = CGAR.G_ID ");
		sql.append("LEFT JOIN ");
		sql.append("CT_GOODS_ATTRIBUTE cga  ");
		sql.append("ON ");
		sql.append("cga.ATTR_ID = cgar.ATTR_ID ");
		sql.append("LEFT JOIN ");
		sql.append("CT_GOODS_TYPE cgt ");
		sql.append("ON ");
		sql.append("cgt.G_T_ID = cga.G_T_ID ");
		sql.append("WHERE ");
		sql.append("CG.B_ID = "+bid);
		sql.append(" AND ");
		sql.append("cgt.G_T_ID IS NOT NULL ");
		sql.append("ORDER BY ");
		sql.append("cgt.G_T_ID ");
		SQLQuery query = getSession().createSQLQuery(sql.toString());
		typeList = query.list();
		 
		return typeList;
	}

	@Override
	public List<?> getAttrByBId(Long bid) {
		// TODO Auto-generated method stub
		List<?> attrList = null;
		 
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT ");
		sql.append("DISTINCT(CGAR.ATTR_ID),cga.ATTR_NAME ");
		sql.append("FROM ");
		sql.append("CT_GOODS cg ");
		sql.append("LEFT JOIN ");
		sql.append("CT_GOODS_ATTRIBUTE_RELATION cgar ");
		sql.append("ON ");
		sql.append("CG.G_ID = CGAR.G_ID ");
		sql.append("LEFT JOIN ");
		sql.append("CT_GOODS_ATTRIBUTE cga  ");
		sql.append("ON ");
		sql.append("cga.ATTR_ID = cgar.ATTR_ID ");
		sql.append("LEFT JOIN ");
		sql.append("CT_GOODS_TYPE cgt ");
		sql.append("ON ");
		sql.append("cgt.G_T_ID = cga.G_T_ID ");
		sql.append("WHERE ");
		sql.append("CG.B_ID = "+bid);
		sql.append(" AND ");
		sql.append("cgt.G_T_ID IS NOT NULL ");
		sql.append("ORDER BY ");
		sql.append("CGAR.ATTR_ID ");
		SQLQuery query = getSession().createSQLQuery(sql.toString());
		attrList = query.list();
		 
		return attrList;
	}

	@Override
	public List<?> getAttrValByBId(Long bid) {
		// TODO Auto-generated method stub
		List<?> attrvalList = null;
		 
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT ");
		sql.append("CGAR.ATTR_ID,cga.ATTR_NAME,CGAR.ATTR_VALUE ");
		sql.append("FROM ");
		sql.append("CT_GOODS cg ");
		sql.append("LEFT JOIN ");
		sql.append("CT_GOODS_ATTRIBUTE_RELATION cgar ");
		sql.append("ON ");
		sql.append("CG.G_ID = CGAR.G_ID ");
		sql.append("LEFT JOIN ");
		sql.append("CT_GOODS_ATTRIBUTE cga  ");
		sql.append("ON ");
		sql.append("cga.ATTR_ID = cgar.ATTR_ID ");
		sql.append("LEFT JOIN ");
		sql.append("CT_GOODS_TYPE cgt ");
		sql.append("ON ");
		sql.append("cgt.G_T_ID = cga.G_T_ID ");
		sql.append("WHERE ");
		sql.append("CG.B_ID = "+bid);
		sql.append(" AND ");
		sql.append("cgt.G_T_ID IS NOT NULL ");
		sql.append("ORDER BY ");
		sql.append("CGAR.ATTR_ID ");
		SQLQuery query = getSession().createSQLQuery(sql.toString());
		attrvalList = query.list();
		 
		return attrvalList;
	}

	@Override
	public List<?> choseGoodsmod1(Long pid, String kword, String mod,String cont, Page page) {
		// TODO Auto-generated method stub
		List<?> searchList = null;
		 
		StringBuffer sql = new StringBuffer();
		
		if(mod.equals("1")){
			sql.append("SELECT  ");
			sql.append("CG.G_ID AS G_ID,NVL(CG.G_NAME, 'N/A') AS G_NAME,NVL(CG.G_SN, 'N/A') AS G_SN,NVL(CG.MARKET_PRICE, 9999) AS MARKET_PRICE,  ");
			sql.append("NVL(CG.PROMOTE_PRICE, 9999) AS PROMOTE_PRICE,NVL(CG.SHOP_PRICE, 9999) AS SHOP_PRICE,  ");
			sql.append("NVL(CGB.B_NAME, 'N/A') AS B_NAME,NVL(CG.G_UNIT, 'N/A') AS G_UNIT,  ");
			sql.append("NVL(CG.PACK1, 'N/A') AS PACK1,NVL(CG.PACK1_NUM, 0) AS PACK1_NUM,  ");
			sql.append("NVL(CG.PACK2, 'N/A') AS PACK2,NVL(CG.PACK2_NUM, 0) AS PACK2_NUM,  ");
			sql.append("NVL(CG.PACK3, 'N/A') AS PACK3,NVL(CG.PACK3_NUM, 0) AS PACK3_NUM,  ");
			sql.append("NVL(CG.PACK4, 'N/A') AS PACK4,NVL(CG.PACK4_NUM, 0) AS PACK4_NUM,  ");
			sql.append("NVL(CGI.P_URL, 'nourl') AS P_URL ");
			sql.append("FROM  ");
			sql.append("CT_GOODS cg  ");
			sql.append("LEFT JOIN  ");
			sql.append("CT_GOODS_BRAND cgb  ");
			sql.append("ON  ");
			sql.append("CG.B_ID = CGB.B_ID  ");
			sql.append("LEFT JOIN  ");
			sql.append("CT_GOODS_IMG cgi  ");
			sql.append("ON  ");
			sql.append("CGI.G_ID = CG.G_ID  ");
			sql.append("WHERE  ");
			sql.append("EXISTS(  ");
			sql.append("SELECT  ");
			sql.append("C_ID  ");
			sql.append("FROM  ");
			sql.append("CT_GOODS_CATEGORY  ");
			sql.append("WHERE  ");
			sql.append("PARENT_ID="+pid+"  ");
			sql.append("AND  ");
			sql.append("C_ID = CG.C_ID  ");
			sql.append(")  ");
			sql.append("AND  ");
			sql.append("(CG.G_NAME LIKE '%"+kword+"%'  ");
			sql.append("OR  ");
			sql.append("CG.G_SN LIKE '%"+kword+"%')  ");
			sql.append("AND  ");
			sql.append("CG.B_ID="+cont+" ");
			sql.append("ORDER BY  ");
			sql.append("CG.G_ID  ");
			SQLQuery query = getSession().createSQLQuery(sql.toString());
			if (page.getCurrentPage()!=0){
				page.setTotalCount(this.getCount1_1(pid,kword,cont));
				query.setFirstResult(page.getFirstIndex());
				query.setMaxResults(page.getPageSize());
			}
			searchList = query.list();
		}
		if(mod.equals("2")){
			sql.append("SELECT  ");
			sql.append("CG.G_ID AS G_ID,NVL(CG.G_NAME, 'N/A') AS G_NAME,NVL(CG.G_SN, 'N/A') AS G_SN,NVL(CG.MARKET_PRICE, 9999) AS MARKET_PRICE,  ");
			sql.append("NVL(CG.PROMOTE_PRICE, 9999) AS PROMOTE_PRICE,NVL(CG.SHOP_PRICE, 9999) AS SHOP_PRICE,  ");
			sql.append("NVL(CGB.B_NAME, 'N/A') AS B_NAME,NVL(CG.G_UNIT, 'N/A') AS G_UNIT,  ");
			sql.append("NVL(CG.PACK1, 'N/A') AS PACK1,NVL(CG.PACK1_NUM, 0) AS PACK1_NUM,  ");
			sql.append("NVL(CG.PACK2, 'N/A') AS PACK2,NVL(CG.PACK2_NUM, 0) AS PACK2_NUM,  ");
			sql.append("NVL(CG.PACK3, 'N/A') AS PACK3,NVL(CG.PACK3_NUM, 0) AS PACK3_NUM,  ");
			sql.append("NVL(CG.PACK4, 'N/A') AS PACK4,NVL(CG.PACK4_NUM, 0) AS PACK4_NUM,  ");
			sql.append("NVL(CGI.P_URL, 'nourl') AS P_URL ");
			sql.append("FROM  ");
			sql.append("CT_GOODS cg  ");
			sql.append("LEFT JOIN  ");
			sql.append("CT_GOODS_BRAND cgb  ");
			sql.append("ON  ");
			sql.append("CG.B_ID = CGB.B_ID  ");
			sql.append("LEFT JOIN  ");
			sql.append("CT_GOODS_IMG cgi  ");
			sql.append("ON  ");
			sql.append("CGI.G_ID = CG.G_ID  ");
			sql.append("WHERE  ");
			sql.append("EXISTS(  ");
			sql.append("SELECT  ");
			sql.append("C_ID  ");
			sql.append("FROM  ");
			sql.append("CT_GOODS_CATEGORY  ");
			sql.append("WHERE  ");
			sql.append("PARENT_ID="+pid+"  ");
			sql.append("AND  ");
			sql.append("C_ID = CG.C_ID  ");
			sql.append(")  ");
			sql.append("AND  ");
			sql.append("(CG.G_NAME LIKE '%"+kword+"%'  ");
			sql.append("OR  ");
			sql.append("CG.G_SN LIKE '%"+kword+"%')  ");
			sql.append("AND  ");
			sql.append("CG.C_ID="+cont+" ");
			sql.append("ORDER BY  ");
			sql.append("CG.G_ID  ");
			SQLQuery query = getSession().createSQLQuery(sql.toString());
			if (page.getCurrentPage()!=0){
				page.setTotalCount(this.getCount1_2(pid,kword,cont));
				query.setFirstResult(page.getFirstIndex());
				query.setMaxResults(page.getPageSize());
			}
			searchList = query.list();
		}
		if(mod.equals("3")){
			sql.append("SELECT  ");
			sql.append("CG.G_ID AS G_ID,NVL(CG.G_NAME, 'N/A') AS G_NAME,NVL(CG.G_SN, 'N/A') AS G_SN,NVL(CG.MARKET_PRICE, 9999) AS MARKET_PRICE,  ");
			sql.append("NVL(CG.PROMOTE_PRICE, 9999) AS PROMOTE_PRICE,NVL(CG.SHOP_PRICE, 9999) AS SHOP_PRICE,  ");
			sql.append("NVL(CGB.B_NAME, 'N/A') AS B_NAME,NVL(CG.G_UNIT, 'N/A') AS G_UNIT,  ");
			sql.append("NVL(CG.PACK1, 'N/A') AS PACK1,NVL(CG.PACK1_NUM, 0) AS PACK1_NUM,  ");
			sql.append("NVL(CG.PACK2, 'N/A') AS PACK2,NVL(CG.PACK2_NUM, 0) AS PACK2_NUM,  ");
			sql.append("NVL(CG.PACK3, 'N/A') AS PACK3,NVL(CG.PACK3_NUM, 0) AS PACK3_NUM,  ");
			sql.append("NVL(CG.PACK4, 'N/A') AS PACK4,NVL(CG.PACK4_NUM, 0) AS PACK4_NUM,  ");
			sql.append("NVL(CGI.P_URL, 'nourl') AS P_URL ");
			sql.append("FROM  ");
			sql.append("CT_GOODS cg  ");
			sql.append("LEFT JOIN  ");
			sql.append("CT_GOODS_BRAND cgb  ");
			sql.append("ON  ");
			sql.append("CG.B_ID = CGB.B_ID  ");
			sql.append("LEFT JOIN  ");
			sql.append("CT_GOODS_IMG cgi  ");
			sql.append("ON  ");
			sql.append("CGI.G_ID = CG.G_ID  ");
			sql.append("WHERE  ");
			sql.append("EXISTS(  ");
			sql.append("SELECT  ");
			sql.append("C_ID  ");
			sql.append("FROM  ");
			sql.append("CT_GOODS_CATEGORY  ");
			sql.append("WHERE  ");
			sql.append("PARENT_ID="+pid+"  ");
			sql.append("AND  ");
			sql.append("C_ID = CG.C_ID  ");
			sql.append(")  ");
			sql.append("AND  ");
			sql.append("(CG.G_NAME LIKE '%"+kword+"%'  ");
			sql.append("OR  ");
			sql.append("CG.G_SN LIKE '%"+kword+"%')  ");
			sql.append("AND ");
			sql.append("EXISTS( ");
			sql.append("SELECT ");
			sql.append("GAR.G_ID ");
			sql.append("FROM ");
			sql.append("CT_GOODS_ATTRIBUTE ga,CT_GOODS_ATTRIBUTE_RELATION gar ");
			sql.append("WHERE ");
			sql.append("gar.ATTR_ID = ga.ATTR_ID ");
			sql.append("AND ");
			sql.append("ga.G_T_ID = "+cont+" ");
			sql.append("AND ");
			sql.append("GAR.G_ID = CG.G_ID ");
			sql.append(") ");
			sql.append("ORDER BY  ");
			sql.append("CG.G_ID  ");
			SQLQuery query = getSession().createSQLQuery(sql.toString());
			if (page.getCurrentPage()!=0){
				page.setTotalCount(this.getCount1_3(pid,kword,cont));
				query.setFirstResult(page.getFirstIndex());
				query.setMaxResults(page.getPageSize());
			}
			searchList = query.list();
		}
		if(mod.equals("4")){
			String[] clist = cont.split("\\|");
			String attr = clist[0];
			String attrval = clist[1];
			sql.append("SELECT  ");
			sql.append("CG.G_ID AS G_ID,NVL(CG.G_NAME, 'N/A') AS G_NAME,NVL(CG.G_SN, 'N/A') AS G_SN,NVL(CG.MARKET_PRICE, 9999) AS MARKET_PRICE,  ");
			sql.append("NVL(CG.PROMOTE_PRICE, 9999) AS PROMOTE_PRICE,NVL(CG.SHOP_PRICE, 9999) AS SHOP_PRICE,  ");
			sql.append("NVL(CGB.B_NAME, 'N/A') AS B_NAME,NVL(CG.G_UNIT, 'N/A') AS G_UNIT,  ");
			sql.append("NVL(CG.PACK1, 'N/A') AS PACK1,NVL(CG.PACK1_NUM, 0) AS PACK1_NUM,  ");
			sql.append("NVL(CG.PACK2, 'N/A') AS PACK2,NVL(CG.PACK2_NUM, 0) AS PACK2_NUM,  ");
			sql.append("NVL(CG.PACK3, 'N/A') AS PACK3,NVL(CG.PACK3_NUM, 0) AS PACK3_NUM,  ");
			sql.append("NVL(CG.PACK4, 'N/A') AS PACK4,NVL(CG.PACK4_NUM, 0) AS PACK4_NUM,  ");
			sql.append("NVL(CGI.P_URL, 'nourl') AS P_URL ");
			sql.append("FROM  ");
			sql.append("CT_GOODS cg  ");
			sql.append("LEFT JOIN  ");
			sql.append("CT_GOODS_BRAND cgb  ");
			sql.append("ON  ");
			sql.append("CG.B_ID = CGB.B_ID  ");
			sql.append("LEFT JOIN  ");
			sql.append("CT_GOODS_IMG cgi  ");
			sql.append("ON  ");
			sql.append("CGI.G_ID = CG.G_ID  ");
			sql.append("WHERE  ");
			sql.append("EXISTS(  ");
			sql.append("SELECT  ");
			sql.append("C_ID  ");
			sql.append("FROM  ");
			sql.append("CT_GOODS_CATEGORY  ");
			sql.append("WHERE  ");
			sql.append("PARENT_ID="+pid+"  ");
			sql.append("AND  ");
			sql.append("C_ID = CG.C_ID  ");
			sql.append(")  ");
			sql.append("AND  ");
			sql.append("(CG.G_NAME LIKE '%"+kword+"%'  ");
			sql.append("OR  ");
			sql.append("CG.G_SN LIKE '%"+kword+"%')  ");
			sql.append("AND ");
			sql.append("EXISTS( ");
			sql.append("SELECT ");
			sql.append("GAR.G_ID ");
			sql.append("FROM ");
			sql.append("CT_GOODS_ATTRIBUTE_RELATION gar ");
			sql.append("WHERE ");
			sql.append("GAR.ATTR_ID = "+attr+" ");
			sql.append("AND ");
			sql.append("GAR.ATTR_VALUE = "+attrval+" ");
			sql.append("AND ");
			sql.append("GAR.G_ID = CG.G_ID ");
			sql.append(") ");
			sql.append("ORDER BY  ");
			sql.append("CG.G_ID  ");
			SQLQuery query = getSession().createSQLQuery(sql.toString());
			if (page.getCurrentPage()!=0){
				page.setTotalCount(this.getCount1_4(pid,kword,attr,attrval));
				query.setFirstResult(page.getFirstIndex());
				query.setMaxResults(page.getPageSize());
			}
			searchList = query.list();
		}
		
		
		 
		return searchList;
	}
	
	public Long getCount1_1(Long pid,String kword,String cont){
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT  ");
		sql.append("count(*)  ");
		sql.append("FROM  ");
		sql.append("CT_GOODS cg  ");
		sql.append("WHERE  ");
		sql.append("EXISTS(  ");
		sql.append("SELECT  ");
		sql.append("C_ID  ");
		sql.append("FROM  ");
		sql.append("CT_GOODS_CATEGORY  ");
		sql.append("WHERE  ");
		sql.append("PARENT_ID="+pid+"  ");
		sql.append("AND  ");
		sql.append("C_ID = CG.C_ID  ");
		sql.append(")  ");
		sql.append("AND  ");
		sql.append("(CG.G_NAME LIKE '%"+kword+"%'  ");
		sql.append("OR  ");
		sql.append("CG.G_SN LIKE '%"+kword+"%')  ");
		sql.append("AND  ");
		sql.append("CG.B_ID="+cont+" ");
		 
		BigDecimal ct = (BigDecimal) getSession().createSQLQuery(sql.toString()).uniqueResult();
		Long count = ct.longValue();
		return count;
	} 
	public Long getCount1_3(Long pid,String kword,String cont){
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT  ");
		sql.append("count(*)  ");
		sql.append("FROM  ");
		sql.append("CT_GOODS cg  ");
		sql.append("WHERE  ");
		sql.append("EXISTS(  ");
		sql.append("SELECT  ");
		sql.append("C_ID  ");
		sql.append("FROM  ");
		sql.append("CT_GOODS_CATEGORY  ");
		sql.append("WHERE  ");
		sql.append("PARENT_ID="+pid+"  ");
		sql.append("AND  ");
		sql.append("C_ID = CG.C_ID  ");
		sql.append(")  ");
		sql.append("AND  ");
		sql.append("(CG.G_NAME LIKE '%"+kword+"%'  ");
		sql.append("OR  ");
		sql.append("CG.G_SN LIKE '%"+kword+"%')  ");
		sql.append("AND  ");
		sql.append("EXISTS( ");
		sql.append("SELECT ");
		sql.append("GAR.G_ID ");
		sql.append("FROM ");
		sql.append("CT_GOODS_ATTRIBUTE ga,CT_GOODS_ATTRIBUTE_RELATION gar ");
		sql.append("WHERE ");
		sql.append("gar.ATTR_ID = ga.ATTR_ID ");
		sql.append("AND ");
		sql.append("ga.G_T_ID = "+cont+" ");
		sql.append("AND ");
		sql.append("GAR.G_ID = CG.G_ID ");
		sql.append(") ");
		 
		BigDecimal ct = (BigDecimal) getSession().createSQLQuery(sql.toString()).uniqueResult();
		Long count = ct.longValue();
		return count;
	}
	public Long getCount1_4(Long pid,String kword,String attr,String attrval){
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT  ");
		sql.append("count(*)  ");
		sql.append("FROM  ");
		sql.append("CT_GOODS cg  ");
		sql.append("WHERE  ");
		sql.append("EXISTS(  ");
		sql.append("SELECT  ");
		sql.append("C_ID  ");
		sql.append("FROM  ");
		sql.append("CT_GOODS_CATEGORY  ");
		sql.append("WHERE  ");
		sql.append("PARENT_ID="+pid+"  ");
		sql.append("AND  ");
		sql.append("C_ID = CG.C_ID  ");
		sql.append(")  ");
		sql.append("AND  ");
		sql.append("(CG.G_NAME LIKE '%"+kword+"%'  ");
		sql.append("OR  ");
		sql.append("CG.G_SN LIKE '%"+kword+"%')  ");
		sql.append("AND ");
		sql.append("EXISTS( ");
		sql.append("SELECT ");
		sql.append("GAR.G_ID ");
		sql.append("FROM ");
		sql.append("CT_GOODS_ATTRIBUTE_RELATION gar ");
		sql.append("WHERE ");
		sql.append("GAR.ATTR_ID = "+attr+" ");
		sql.append("AND ");
		sql.append("GAR.ATTR_VALUE = "+attrval+" ");
		sql.append("AND ");
		sql.append("GAR.G_ID = CG.G_ID ");
		sql.append(") ");
		 
		BigDecimal ct = (BigDecimal) getSession().createSQLQuery(sql.toString()).uniqueResult();
		Long count = ct.longValue();
		return count;
	} 
	public Long getCount1_2(Long pid,String kword,String cont){
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT  ");
		sql.append("count(*)  ");
		sql.append("FROM  ");
		sql.append("CT_GOODS cg  ");
		sql.append("WHERE  ");
		sql.append("EXISTS(  ");
		sql.append("SELECT  ");
		sql.append("C_ID  ");
		sql.append("FROM  ");
		sql.append("CT_GOODS_CATEGORY  ");
		sql.append("WHERE  ");
		sql.append("PARENT_ID="+pid+"  ");
		sql.append("AND  ");
		sql.append("C_ID = CG.C_ID  ");
		sql.append(")  ");
		sql.append("AND  ");
		sql.append("(CG.G_NAME LIKE '%"+kword+"%'  ");
		sql.append("OR  ");
		sql.append("CG.G_SN LIKE '%"+kword+"%')  ");
		sql.append("AND  ");
		sql.append("CG.C_ID="+cont+" ");
		 
		BigDecimal ct = (BigDecimal) getSession().createSQLQuery(sql.toString()).uniqueResult();
		Long count = ct.longValue();
		return count;
	} 

	@Override
	public List<?> choseGoodsmod2(Long cid, String mod,String cont, Page page,String price1,String price2) {
		// TODO Auto-generated method stub
		List<?> goodsList = null;
		 
		StringBuffer sql = new StringBuffer();
		if(mod.equals("priceFind")){
			if(price1 != null && price2 != null){
				sql.append("SELECT  ");
				sql.append("CG.G_ID AS G_ID,NVL(CG.G_NAME, 'N/A') AS G_NAME,NVL(CG.G_SN, 'N/A') AS G_SN,NVL(CG.MARKET_PRICE, 9999) AS MARKET_PRICE,  ");
				sql.append("NVL(CG.PROMOTE_PRICE, 9999) AS PROMOTE_PRICE,NVL(CG.SHOP_PRICE, 9999) AS SHOP_PRICE,  ");
				sql.append("NVL(CGB.B_NAME, 'N/A') AS B_NAME,NVL(CG.G_UNIT, 'N/A') AS G_UNIT,  ");
				sql.append("NVL(CG.PACK1, 'N/A') AS PACK1,NVL(CG.PACK1_NUM, 0) AS PACK1_NUM,  ");
				sql.append("NVL(CG.PACK2, 'N/A') AS PACK2,NVL(CG.PACK2_NUM, 0) AS PACK2_NUM,  ");
				sql.append("NVL(CG.PACK3, 'N/A') AS PACK3,NVL(CG.PACK3_NUM, 0) AS PACK3_NUM,  ");
				sql.append("NVL(CG.PACK4, 'N/A') AS PACK4,NVL(CG.PACK4_NUM, 0) AS PACK4_NUM,  ");
				sql.append("NVL(CGI.P_URL, 'nourl') AS P_URL ");
				sql.append("FROM  ");
				sql.append("CT_GOODS cg  ");
				sql.append("LEFT JOIN  ");
				sql.append("CT_GOODS_BRAND cgb  ");
				sql.append("ON  ");
				sql.append("CG.B_ID = CGB.B_ID  ");
				sql.append("LEFT JOIN  ");
				sql.append("CT_GOODS_IMG cgi  ");
				sql.append("ON  ");
				sql.append("CGI.G_ID = CG.G_ID  ");
				sql.append("LEFT JOIN  ");
				sql.append("ct_goods_category cgc  ");
				sql.append("ON  ");
				sql.append("cg.c_id=cgc.c_id ");
				sql.append("WHERE  ");
				if(cont!=null && cont.length() > 0){
					if(cont.indexOf("testStr")==-1){
						sql.append("cgc.c_id="+ cont);
						sql.append(" AND ");
					} else {
						sql.append("(CG.G_NAME LIKE '%"+cont.substring(0, cont.indexOf("testStr"))+"%'  ");
						sql.append("OR  ");
						sql.append("CG.G_SN LIKE '%"+cont.substring(0, cont.indexOf("testStr"))+"%')  ");
						sql.append("AND  ");
					}
				}
				sql.append("CG.SHOP_PRICE between " + price1 + " and " + price2);
				sql.append(" ORDER BY  ");
				sql.append("CG.G_ID  ");
			} else {
				sql.append("SELECT  ");
				sql.append("CG.G_ID AS G_ID,NVL(CG.G_NAME, 'N/A') AS G_NAME,NVL(CG.G_SN, 'N/A') AS G_SN,NVL(CG.MARKET_PRICE, 9999) AS MARKET_PRICE,  ");
				sql.append("NVL(CG.PROMOTE_PRICE, 9999) AS PROMOTE_PRICE,NVL(CG.SHOP_PRICE, 9999) AS SHOP_PRICE,  ");
				sql.append("NVL(CGB.B_NAME, 'N/A') AS B_NAME,NVL(CG.G_UNIT, 'N/A') AS G_UNIT,  ");
				sql.append("NVL(CG.PACK1, 'N/A') AS PACK1,NVL(CG.PACK1_NUM, 0) AS PACK1_NUM,  ");
				sql.append("NVL(CG.PACK2, 'N/A') AS PACK2,NVL(CG.PACK2_NUM, 0) AS PACK2_NUM,  ");
				sql.append("NVL(CG.PACK3, 'N/A') AS PACK3,NVL(CG.PACK3_NUM, 0) AS PACK3_NUM,  ");
				sql.append("NVL(CG.PACK4, 'N/A') AS PACK4,NVL(CG.PACK4_NUM, 0) AS PACK4_NUM,  ");
				sql.append("NVL(CGI.P_URL, 'nourl') AS P_URL ");
				sql.append("FROM  ");
				sql.append("CT_GOODS cg  ");
				sql.append("LEFT JOIN  ");
				sql.append("CT_GOODS_BRAND cgb  ");
				sql.append("ON  ");
				sql.append("CG.B_ID = CGB.B_ID  ");
				sql.append("LEFT JOIN  ");
				sql.append("CT_GOODS_IMG cgi  ");
				sql.append("ON  ");
				sql.append("CGI.G_ID = CG.G_ID  ");
				sql.append(" ORDER BY  ");
				sql.append("CG.G_ID  ");
			}
			SQLQuery query= getSession().createSQLQuery(sql.toString());
			if (page.getCurrentPage()!=0){
				if(query.list().size()>1){
					page.setTotalCount(Long.valueOf(query.list().size()-1));
					query.setFirstResult(1);
					query.setMaxResults(12);
				} else if(query.list().size() == 0) {
					page.setTotalCount(0L);
				} else {
					page.setTotalCount(1L);
				}
			}
			goodsList = query.list();
		}
		if(mod.equals("1")){
			String[] clist = cont.split("\\|");
			String attr = clist[0];
			String attrval = clist[1];
			sql.append("SELECT  ");
			sql.append("CG.G_ID AS G_ID,NVL(CG.G_NAME, 'N/A') AS G_NAME,NVL(CG.G_SN, 'N/A') AS G_SN,NVL(CG.MARKET_PRICE, 9999) AS MARKET_PRICE,  ");
			sql.append("NVL(CG.PROMOTE_PRICE, 9999) AS PROMOTE_PRICE,NVL(CG.SHOP_PRICE, 9999) AS SHOP_PRICE,  ");
			sql.append("NVL(CGB.B_NAME, 'N/A') AS B_NAME,NVL(CG.G_UNIT, 'N/A') AS G_UNIT,  ");
			sql.append("NVL(CG.PACK1, 'N/A') AS PACK1,NVL(CG.PACK1_NUM, 0) AS PACK1_NUM,  ");
			sql.append("NVL(CG.PACK2, 'N/A') AS PACK2,NVL(CG.PACK2_NUM, 0) AS PACK2_NUM,  ");
			sql.append("NVL(CG.PACK3, 'N/A') AS PACK3,NVL(CG.PACK3_NUM, 0) AS PACK3_NUM,  ");
			sql.append("NVL(CG.PACK4, 'N/A') AS PACK4,NVL(CG.PACK4_NUM, 0) AS PACK4_NUM,  ");
			sql.append("NVL(CGI.P_URL, 'nourl') AS P_URL ");
			sql.append("FROM  ");
			sql.append("CT_GOODS cg  ");
			sql.append("LEFT JOIN  ");
			sql.append("CT_GOODS_BRAND cgb  ");
			sql.append("ON  ");
			sql.append("CG.B_ID = CGB.B_ID  ");
			sql.append("LEFT JOIN  ");
			sql.append("CT_GOODS_IMG cgi  ");
			sql.append("ON  ");
			sql.append("CGI.G_ID = CG.G_ID  ");
			sql.append("WHERE  ");
			sql.append("CG.C_ID = "+cid);
			sql.append("AND  ");
			sql.append("CG.B_ID="+attr+" ");
			sql.append(" ORDER BY  ");
			sql.append("CG.G_ID  ");
			SQLQuery query= getSession().createSQLQuery(sql.toString());
			if (page.getCurrentPage()!=0){
				page.setTotalCount(this.getCount2_1(cid,attr));
				query.setFirstResult(page.getFirstIndex());
				query.setMaxResults(page.getPageSize());
			}
			goodsList = query.list();
		}
		if(mod.equals("2")){
			return null;
		}
		if(mod.equals("3")){
			String[] clist = cont.split("\\|");
			String attr = clist[0];
			String attrval = clist[1];
			sql.append("SELECT  ");
			sql.append("CG.G_ID AS G_ID,NVL(CG.G_NAME, 'N/A') AS G_NAME,NVL(CG.G_SN, 'N/A') AS G_SN,NVL(CG.MARKET_PRICE, 9999) AS MARKET_PRICE,  ");
			sql.append("NVL(CG.PROMOTE_PRICE, 9999) AS PROMOTE_PRICE,NVL(CG.SHOP_PRICE, 9999) AS SHOP_PRICE,  ");
			sql.append("NVL(CGB.B_NAME, 'N/A') AS B_NAME,NVL(CG.G_UNIT, 'N/A') AS G_UNIT,  ");
			sql.append("NVL(CG.PACK1, 'N/A') AS PACK1,NVL(CG.PACK1_NUM, 0) AS PACK1_NUM,  ");
			sql.append("NVL(CG.PACK2, 'N/A') AS PACK2,NVL(CG.PACK2_NUM, 0) AS PACK2_NUM,  ");
			sql.append("NVL(CG.PACK3, 'N/A') AS PACK3,NVL(CG.PACK3_NUM, 0) AS PACK3_NUM,  ");
			sql.append("NVL(CG.PACK4, 'N/A') AS PACK4,NVL(CG.PACK4_NUM, 0) AS PACK4_NUM,  ");
			sql.append("NVL(CGI.P_URL, 'nourl') AS P_URL ");
			sql.append("FROM  ");
			sql.append("CT_GOODS cg  ");
			sql.append("LEFT JOIN  ");
			sql.append("CT_GOODS_BRAND cgb  ");
			sql.append("ON  ");
			sql.append("CG.B_ID = CGB.B_ID  ");
			sql.append("LEFT JOIN  ");
			sql.append("CT_GOODS_IMG cgi  ");
			sql.append("ON  ");
			sql.append("CGI.G_ID = CG.G_ID  ");
			sql.append("WHERE  ");
			sql.append("CG.C_ID = "+cid);
			sql.append("AND ");
			sql.append("EXISTS( ");
			sql.append("SELECT ");
			sql.append("GAR.G_ID ");
			sql.append("FROM ");
			sql.append("CT_GOODS_ATTRIBUTE ga,CT_GOODS_ATTRIBUTE_RELATION gar ");
			sql.append("WHERE ");
			sql.append("gar.ATTR_ID = ga.ATTR_ID ");
			sql.append("AND ");
			sql.append("ga.G_T_ID = "+attr+" ");
			sql.append("AND ");
			sql.append("ga.attr_name = '"+attrval+"' ");
			sql.append("AND ");
			sql.append("GAR.G_ID = CG.G_ID ");
			sql.append(") ");
			sql.append(" ORDER BY  ");
			sql.append("CG.G_ID  ");
			SQLQuery query= getSession().createSQLQuery(sql.toString());
			if (page.getCurrentPage()!=0){
				page.setTotalCount(this.getCount2_3(cid,cont));
				query.setFirstResult(page.getFirstIndex());
				query.setMaxResults(page.getPageSize());
			}
			goodsList = query.list();
		}
		if(mod.equals("4")){
			String[] clist = cont.split("\\|");
			String attr = clist[0];
			String attrval = clist[1];
			sql.append("SELECT  ");
			sql.append("CG.G_ID AS G_ID,NVL(CG.G_NAME, 'N/A') AS G_NAME,NVL(CG.G_SN, 'N/A') AS G_SN,NVL(CG.MARKET_PRICE, 9999) AS MARKET_PRICE,  ");
			sql.append("NVL(CG.PROMOTE_PRICE, 9999) AS PROMOTE_PRICE,NVL(CG.SHOP_PRICE, 9999) AS SHOP_PRICE,  ");
			sql.append("NVL(CGB.B_NAME, 'N/A') AS B_NAME,NVL(CG.G_UNIT, 'N/A') AS G_UNIT,  ");
			sql.append("NVL(CG.PACK1, 'N/A') AS PACK1,NVL(CG.PACK1_NUM, 0) AS PACK1_NUM,  ");
			sql.append("NVL(CG.PACK2, 'N/A') AS PACK2,NVL(CG.PACK2_NUM, 0) AS PACK2_NUM,  ");
			sql.append("NVL(CG.PACK3, 'N/A') AS PACK3,NVL(CG.PACK3_NUM, 0) AS PACK3_NUM,  ");
			sql.append("NVL(CG.PACK4, 'N/A') AS PACK4,NVL(CG.PACK4_NUM, 0) AS PACK4_NUM,  ");
			sql.append("NVL(CGI.P_URL, 'nourl') AS P_URL ");
			sql.append("FROM  ");
			sql.append("CT_GOODS cg  ");
			sql.append("LEFT JOIN  ");
			sql.append("CT_GOODS_BRAND cgb  ");
			sql.append("ON  ");
			sql.append("CG.B_ID = CGB.B_ID  ");
			sql.append("LEFT JOIN  ");
			sql.append("CT_GOODS_IMG cgi  ");
			sql.append("ON  ");
			sql.append("CGI.G_ID = CG.G_ID  ");
			sql.append("WHERE  ");
			sql.append("CG.C_ID = "+cid);
			sql.append("AND ");
			sql.append("EXISTS( ");
			sql.append("SELECT ");
			sql.append("GAR.G_ID ");
			sql.append("FROM ");
			sql.append("CT_GOODS_ATTRIBUTE_RELATION gar ");
			sql.append("WHERE ");
			sql.append("GAR.ATTR_ID = "+attr+" ");
			sql.append("AND ");
			sql.append("GAR.ATTR_VALUE = "+attrval+" ");
			sql.append("AND ");
			sql.append("GAR.G_ID = CG.G_ID ");
			sql.append(") ");
			sql.append(" ORDER BY  ");
			sql.append("CG.G_ID  ");
			SQLQuery query= getSession().createSQLQuery(sql.toString());
			if (page.getCurrentPage()!=0){
				page.setTotalCount(this.getCount2_4(cid,attr,attrval));
				query.setFirstResult(page.getFirstIndex());
				query.setMaxResults(page.getPageSize());
			}
			goodsList = query.list();
		}
		
		 
		return goodsList;
	}
	public Long getCount2_1(Long cid,String cont){
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT  ");
		sql.append("count(*)  ");
		sql.append("FROM  ");
		sql.append("CT_GOODS cg  ");
		sql.append("WHERE  ");
		sql.append("CG.C_ID = "+cid+" ");
		sql.append("AND  ");
		sql.append("CG.B_ID="+cont+" ");
		 
		BigDecimal ct = (BigDecimal) getSession().createSQLQuery(sql.toString()).uniqueResult();
		Long count = ct.longValue();
		return count;
	}
	public Long getCount2_3(Long cid,String cont){
		String[] clist = cont.split("\\|");
		String attr = clist[0];
		String attrval = clist[1];
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT  ");
		sql.append("count(*)  ");
		sql.append("FROM  ");
		sql.append("CT_GOODS cg  ");
		sql.append("WHERE  ");
		sql.append("CG.C_ID = "+cid+" ");
		sql.append("AND ");
		sql.append("EXISTS( ");
		sql.append("SELECT ");
		sql.append("GAR.G_ID ");
		sql.append("FROM ");
		sql.append("CT_GOODS_ATTRIBUTE ga,CT_GOODS_ATTRIBUTE_RELATION gar ");
		sql.append("WHERE ");
		sql.append("GAR.G_ID = CG.G_ID ");
		sql.append("AND ");
		sql.append("gar.ATTR_ID = ga.ATTR_ID ");
		sql.append("AND ");
		sql.append("GAR.ATTR_ID = "+attr+" ");
		sql.append("AND ");
		sql.append("GAR.ATTR_VALUE = '"+attrval+"' ");
		sql.append(") ");
		 
		BigDecimal ct = (BigDecimal) getSession().createSQLQuery(sql.toString()).uniqueResult();
		Long count = ct.longValue();
		return count;
	}
	public Long getCount2_4(Long cid,String attr,String attrval){
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT  ");
		sql.append("count(*)  ");
		sql.append("FROM  ");
		sql.append("CT_GOODS cg  ");
		sql.append("WHERE  ");
		sql.append("CG.C_ID = "+cid+" ");
		sql.append("AND ");
		sql.append("EXISTS( ");
		sql.append("SELECT ");
		sql.append("GAR.G_ID ");
		sql.append("FROM ");
		sql.append("CT_GOODS_ATTRIBUTE_RELATION gar ");
		sql.append("WHERE ");
		sql.append("GAR.ATTR_ID = "+attr+" ");
		sql.append("AND ");
		sql.append("GAR.ATTR_VALUE = "+attrval+" ");
		sql.append("AND ");
		sql.append("GAR.G_ID = CG.G_ID ");
		sql.append(") ");
		 
		BigDecimal ct = (BigDecimal) getSession().createSQLQuery(sql.toString()).uniqueResult();
		Long count = ct.longValue();
		return count;
	}

	@Override
	public List<?> choseGoodsmod3(Long bid, String mod,String cont, Page page) {
		// TODO Auto-generated method stub
		List<?> goodsList = null;
		 
		StringBuffer sql = new StringBuffer();
		if(mod.equals("1")){
			return null;
		}
		if(mod.equals("2")){
			return null;	
		}
		if(mod.equals("3")){
			sql.append("SELECT  ");
			sql.append("CG.G_ID AS G_ID,NVL(CG.G_NAME, 'N/A') AS G_NAME,NVL(CG.G_SN, 'N/A') AS G_SN,NVL(CG.MARKET_PRICE, 9999) AS MARKET_PRICE,  ");
			sql.append("NVL(CG.PROMOTE_PRICE, 9999) AS PROMOTE_PRICE,NVL(CG.SHOP_PRICE, 9999) AS SHOP_PRICE,  ");
			sql.append("NVL(CGB.B_NAME, 'N/A') AS B_NAME,NVL(CG.G_UNIT, 'N/A') AS G_UNIT,  ");
			sql.append("NVL(CG.PACK1, 'N/A') AS PACK1,NVL(CG.PACK1_NUM, 0) AS PACK1_NUM,  ");
			sql.append("NVL(CG.PACK2, 'N/A') AS PACK2,NVL(CG.PACK2_NUM, 0) AS PACK2_NUM,  ");
			sql.append("NVL(CG.PACK3, 'N/A') AS PACK3,NVL(CG.PACK3_NUM, 0) AS PACK3_NUM,  ");
			sql.append("NVL(CG.PACK4, 'N/A') AS PACK4,NVL(CG.PACK4_NUM, 0) AS PACK4_NUM,  ");
			sql.append("NVL(CGI.P_URL, 'nourl') AS P_URL ");
			sql.append("FROM  ");
			sql.append("CT_GOODS cg  ");
			sql.append("LEFT JOIN  ");
			sql.append("CT_GOODS_BRAND cgb  ");
			sql.append("ON  ");
			sql.append("CG.B_ID = CGB.B_ID  ");
			sql.append("LEFT JOIN  ");
			sql.append("CT_GOODS_IMG cgi  ");
			sql.append("ON  ");
			sql.append("CGI.G_ID = CG.G_ID  ");
			sql.append("WHERE  ");
			sql.append("CG.B_ID = "+bid);
			sql.append("AND ");
			sql.append("EXISTS( ");
			sql.append("SELECT ");
			sql.append("GAR.G_ID ");
			sql.append("FROM ");
			sql.append("CT_GOODS_ATTRIBUTE ga,CT_GOODS_ATTRIBUTE_RELATION gar ");
			sql.append("WHERE ");
			sql.append("gar.ATTR_ID = ga.ATTR_ID ");
			sql.append("AND ");
			sql.append("ga.G_T_ID = "+cont+" ");
			sql.append("AND ");
			sql.append("GAR.G_ID = CG.G_ID ");
			sql.append(") ");
			sql.append(" ORDER BY  ");
			sql.append("CG.G_ID  ");
			SQLQuery query = getSession().createSQLQuery(sql.toString());
			if (page.getCurrentPage()!=0){
				page.setTotalCount(this.getCount3_3(bid,cont));
				query.setFirstResult(page.getFirstIndex());
				query.setMaxResults(page.getPageSize());
			}
			goodsList = query.list();
		}
		if(mod.equals("4")){
			String[] clist = cont.split("\\|");
			String attr = clist[0];
			String attrval = clist[1];
			sql.append("SELECT  ");
			sql.append("CG.G_ID AS G_ID,NVL(CG.G_NAME, 'N/A') AS G_NAME,NVL(CG.G_SN, 'N/A') AS G_SN,NVL(CG.MARKET_PRICE, 9999) AS MARKET_PRICE,  ");
			sql.append("NVL(CG.PROMOTE_PRICE, 9999) AS PROMOTE_PRICE,NVL(CG.SHOP_PRICE, 9999) AS SHOP_PRICE,  ");
			sql.append("NVL(CGB.B_NAME, 'N/A') AS B_NAME,NVL(CG.G_UNIT, 'N/A') AS G_UNIT,  ");
			sql.append("NVL(CG.PACK1, 'N/A') AS PACK1,NVL(CG.PACK1_NUM, 0) AS PACK1_NUM,  ");
			sql.append("NVL(CG.PACK2, 'N/A') AS PACK2,NVL(CG.PACK2_NUM, 0) AS PACK2_NUM,  ");
			sql.append("NVL(CG.PACK3, 'N/A') AS PACK3,NVL(CG.PACK3_NUM, 0) AS PACK3_NUM,  ");
			sql.append("NVL(CG.PACK4, 'N/A') AS PACK4,NVL(CG.PACK4_NUM, 0) AS PACK4_NUM,  ");
			sql.append("NVL(CGI.P_URL, 'nourl') AS P_URL ");
			sql.append("FROM  ");
			sql.append("CT_GOODS cg  ");
			sql.append("LEFT JOIN  ");
			sql.append("CT_GOODS_BRAND cgb  ");
			sql.append("ON  ");
			sql.append("CG.B_ID = CGB.B_ID  ");
			sql.append("LEFT JOIN  ");
			sql.append("CT_GOODS_IMG cgi  ");
			sql.append("ON  ");
			sql.append("CGI.G_ID = CG.G_ID  ");
			sql.append("WHERE  ");
			sql.append("CG.B_ID = "+bid);
			sql.append("AND ");
			sql.append("EXISTS( ");
			sql.append("SELECT ");
			sql.append("GAR.G_ID ");
			sql.append("FROM ");
			sql.append("CT_GOODS_ATTRIBUTE_RELATION gar ");
			sql.append("WHERE ");
			sql.append("GAR.ATTR_ID = "+attr+" ");
			sql.append("AND ");
			sql.append("GAR.ATTR_VALUE = "+attrval+" ");
			sql.append("AND ");
			sql.append("GAR.G_ID = CG.G_ID ");
			sql.append(") ");
			sql.append(" ORDER BY  ");
			sql.append("CG.G_ID  ");
			SQLQuery query = getSession().createSQLQuery(sql.toString());
			if (page.getCurrentPage()!=0){
				page.setTotalCount(this.getCount3_4(bid,attr,attrval));
				query.setFirstResult(page.getFirstIndex());
				query.setMaxResults(page.getPageSize());
			}
			goodsList = query.list();
		}
		
		
		 
		return goodsList;
	}
	public Long getCount3_3(Long bid,String cont){
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT  ");
		sql.append("count(*)  ");
		sql.append("FROM  ");
		sql.append("CT_GOODS cg  ");
		sql.append("WHERE  ");
		sql.append("CG.B_ID = "+bid);
		sql.append("AND ");
		sql.append("EXISTS( ");
		sql.append("SELECT ");
		sql.append("GAR.G_ID ");
		sql.append("FROM ");
		sql.append("CT_GOODS_ATTRIBUTE ga,CT_GOODS_ATTRIBUTE_RELATION gar ");
		sql.append("WHERE ");
		sql.append("gar.ATTR_ID = ga.ATTR_ID ");
		sql.append("AND ");
		sql.append("ga.G_T_ID = "+cont+" ");
		sql.append("AND ");
		sql.append("GAR.G_ID = CG.G_ID ");
		sql.append(") ");
		 
		BigDecimal ct = (BigDecimal) getSession().createSQLQuery(sql.toString()).uniqueResult();
		Long count = ct.longValue();
		return count;
	}
	public Long getCount3_4(Long bid,String attr,String attrval){
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT  ");
		sql.append("count(*)  ");
		sql.append("FROM  ");
		sql.append("CT_GOODS cg  ");
		sql.append("WHERE  ");
		sql.append("CG.B_ID = "+bid);
		sql.append("AND ");
		sql.append("EXISTS( ");
		sql.append("SELECT ");
		sql.append("GAR.G_ID ");
		sql.append("FROM ");
		sql.append("CT_GOODS_ATTRIBUTE_RELATION gar ");
		sql.append("WHERE ");
		sql.append("GAR.ATTR_ID = "+attr+" ");
		sql.append("AND ");
		sql.append("GAR.ATTR_VALUE = "+attrval+" ");
		sql.append("AND ");
		sql.append("GAR.G_ID = CG.G_ID ");
		sql.append(") ");
		 
		BigDecimal ct = (BigDecimal) getSession().createSQLQuery(sql.toString()).uniqueResult();
		Long count = ct.longValue();
		return count;
	}

	@Override
	public String save(CtCollectGoods ccg) {
		// TODO Auto-generated method stub
		String sql = "insert into Ct_Collect_Goods(COID,U_ID,G_ID,ADD_TIME,COLL_ID) values(SEQ_COLLECT_GOODS.nextval,'"+ccg.getUId()+"','"+ccg.getGId()+"','"+ccg.getAddTime()+"','"+ccg.getCollid()+"')";
		Query query = getSession().createSQLQuery(sql);
		Long id = (long)query.executeUpdate();
		String res=id.toString();
		return res;
	}

	@Override
	public CtCollectGoods getCtCollectGoodsByUIdAndGid(Long UId, Long GId) {
		// TODO Auto-generated method stub
		CtCollectGoods ccg;
		String hql = "from CtCollectGoods ccg where ccg.UId="+UId+" and ccg.GId="+GId;
		Query query = this.getSession().createQuery(hql);
		List<CtCollectGoods> collectGoods = query.list();
		if(collectGoods.size() >0 && collectGoods != null){
			ccg = collectGoods.get(0);
		}else {
			ccg = null;
		}
		return ccg;
	}

	@Override
	public List<?> collectList(Long UId,Page page,Long collid) {
		// TODO Auto-generated method stub
		List<?> collectList = null;
		StringBuffer sql = new StringBuffer();
		sql.append("select CG.G_ID AS G_ID,NVL(CG.G_NAME, 'N/A') AS G_NAME,NVL(CG.G_SN, 'N/A') AS G_SN,NVL(CG.MARKET_PRICE, 9999) AS MARKET_PRICE,  NVL(CG.PROMOTE_PRICE, 9999) AS PROMOTE_PRICE,NVL(CG.SHOP_PRICE, 9999) AS SHOP_PRICE,  NVL(CGB.B_NAME, 'N/A') AS B_NAME,NVL(CG.G_UNIT, 'N/A') AS G_UNIT,  NVL(CGI.P_URL, 'nourl') AS P_URL, NVL(CG.PACK1, 'N/A') AS PACK1,NVL(CG.PACK1_NUM, 0) AS PACK1_NUM,  NVL(CG.PACK2, 'N/A') AS PACK2,NVL(CG.PACK2_NUM, 0) AS PACK2_NUM,  NVL(CG.PACK3, 'N/A') AS PACK3,NVL(CG.PACK3_NUM, 0) AS PACK3_NUM,  NVL(CG.PACK4, 'N/A') AS PACK4,NVL(CG.PACK4_NUM, 0) AS PACK4_NUM,  ccg.ADD_TIME,ccg.COLL_ID   from ct_collect_goods ccg");
		sql.append("  left join ct_goods_img cgi on ccg.g_id=cgi.g_id");
		sql.append("  left join ct_goods cg on cg.g_id=ccg.g_id ");
		sql.append("  left join ct_goods_brand cgb on cg.b_id=cgb.b_id");
		sql.append("  where U_id="+UId+" and ccg.coll_id="+collid+" and cg.g_id is not null  order by ccg.add_time desc");
//		sql.append("SELECT  ");
//		sql.append("CG.G_ID AS G_ID,NVL(CG.G_NAME, 'N/A') AS G_NAME,NVL(CG.G_SN, 'N/A') AS G_SN,NVL(CG.MARKET_PRICE, 9999) AS MARKET_PRICE,  ");
//		sql.append("NVL(CG.PROMOTE_PRICE, 9999) AS PROMOTE_PRICE,NVL(CG.SHOP_PRICE, 9999) AS SHOP_PRICE,  ");
//		sql.append("NVL(CGB.B_NAME, 'N/A') AS B_NAME,NVL(CG.G_UNIT, 'N/A') AS G_UNIT,  ");
//		sql.append("NVL(CGI.P_URL, 'nourl') AS P_URL, ");
//		sql.append("NVL(CG.PACK1, 'N/A') AS PACK1,NVL(CG.PACK1_NUM, 0) AS PACK1_NUM,  ");
//		sql.append("NVL(CG.PACK2, 'N/A') AS PACK2,NVL(CG.PACK2_NUM, 0) AS PACK2_NUM,  ");
//		sql.append("NVL(CG.PACK3, 'N/A') AS PACK3,NVL(CG.PACK3_NUM, 0) AS PACK3_NUM,  ");
//		sql.append("NVL(CG.PACK4, 'N/A') AS PACK4,NVL(CG.PACK4_NUM, 0) AS PACK4_NUM,  ");
//		sql.append("ccg.ADD_TIME ");
//		sql.append("FROM  ");
//		sql.append("CT_GOODS cg  ");
//		sql.append("LEFT JOIN  ");
//		sql.append("CT_GOODS_BRAND cgb  ");
//		sql.append("ON  ");
//		sql.append("CG.B_ID = CGB.B_ID  ");
//		sql.append("LEFT JOIN  ");
//		sql.append("CT_GOODS_IMG cgi  ");
//		sql.append("ON  ");
//		sql.append("CGI.G_ID = CG.G_ID  ");
//		sql.append("LEFT JOIN  CT_COLLECT_GOODS ccg ON CG.G_ID = ccg.G_ID ");
//		sql.append("WHERE  ");
//		sql.append("EXISTS(  ");
//		sql.append("SELECT  ");
//		sql.append("G_ID  ");
//		sql.append("FROM  ");
//		sql.append("CT_COLLECT_GOODS  ");
//		sql.append("WHERE  ");
//		sql.append("G_ID = CG.G_ID  ");
//		sql.append(" AND U_ID = "+UId);
//		sql.append(") ORDER BY ADD_TIME DESC  ");
		SQLQuery query = getSession().createSQLQuery(sql.toString());
		
		collectList = query.list();
		
		for (int k = 0; k < collectList.size(); k++)  //外循环是循环的次数
		{
		    for (int j = collectList.size() - 1 ; j > k; j--)  //内循环是 外循环一次比较的次数
		    {
		    	Object[] obj = (Object[]) collectList.get(k);
		    	Object[] obj1 = (Object[]) collectList.get(j);
		        if (obj[0].equals(obj1[0]))
		        {
		        	collectList.remove(j);
		        }

		    }
		}
		List<?> collectList1 = null;
		List<Object> obj = new ArrayList<Object>();
		if (page.getCurrentPage()!=0){
			page.setTotalCount(Long.valueOf(collectList.size()));
			int start = ((page.getCurrentPage()-1)*page.getPageSize())+1;
			int end = start+11 > collectList.size() ? collectList.size() : start+11;
			int k = 0;
			for (int i = start; i <= end; i++) {
				Object a = collectList.get(i-1);
				obj.add(a);
				k++;
			}
			collectList1 = obj;
			if(collectList.size() > 12){
				for (int i = 12; i < collectList.size(); i++) {
					collectList.remove(i);
				}
			}
		}
		return collectList1;
	}
	
	public Long collectCount(Long UId){
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT  ");
		sql.append("count(*) FROM ");
		sql.append("CT_GOODS cg  ");
		sql.append("LEFT JOIN  ");
		sql.append("CT_GOODS_BRAND cgb  ");
		sql.append("ON  ");
		sql.append("CG.B_ID = CGB.B_ID  ");
		sql.append("LEFT JOIN  ");
		sql.append("CT_GOODS_IMG cgi  ");
		sql.append("ON  ");
		sql.append("CGI.G_ID = CG.G_ID  ");
		sql.append("WHERE  ");
		sql.append("EXISTS(  ");
		sql.append("SELECT  ");
		sql.append("G_ID  ");
		sql.append("FROM  ");
		sql.append("CT_COLLECT_GOODS  ");
		sql.append("WHERE  ");
		sql.append("G_ID = CG.G_ID  ");
		sql.append(" AND U_ID = "+UId);
		sql.append(")  ");
		BigDecimal ct = (BigDecimal) getSession().createSQLQuery(sql.toString()).uniqueResult();
		Long count = ct.longValue();
		return count;
	}
	public Long collectCount(Long UId,Long collid){
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT  ");
		sql.append("count(*) FROM ");
		sql.append("CT_GOODS cg  ");
		sql.append("LEFT JOIN  ");
		sql.append("CT_GOODS_BRAND cgb  ");
		sql.append("ON  ");
		sql.append("CG.B_ID = CGB.B_ID  ");
		sql.append("LEFT JOIN  ");
		sql.append("CT_GOODS_IMG cgi  ");
		sql.append("ON  ");
		sql.append("CGI.G_ID = CG.G_ID  ");
		sql.append("WHERE  ");
		sql.append("EXISTS(  ");
		sql.append("SELECT  ");
		sql.append("G_ID  ");
		sql.append("FROM  ");
		sql.append("CT_COLLECT_GOODS  ");
		sql.append("WHERE  ");
		sql.append("G_ID = CG.G_ID  ");
		sql.append(" AND U_ID = "+UId);
		sql.append(" AND COID = "+collid);
		sql.append(")  ");
		BigDecimal ct = (BigDecimal) getSession().createSQLQuery(sql.toString()).uniqueResult();
		Long count = ct.longValue();
		return count;
	}

	@Override
	public Integer delGoodsByByUIdAndGid(Long UId, Long GId,Long collid) {
		// TODO Auto-generated method stub
		String hql = "delete from CtCollectGoods ccg where ccg.UId="+UId+" and ccg.GId="+GId+" and ccg.collid="+collid;
		Query query= getSession().createQuery(hql);
		Integer id = query.executeUpdate();
		return id;
	}

	@Override
	public List<CtCouponDetail> getCouponByGid(Long gid) {
		Date date = new Date();
		SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time = siFormat.format(date);
		String hql = " from CtCouponDetail ccd where ccd.ctCoupon.GId = " + gid + " and ccd.ctCoupon.etime>='"+time+"' " +
				" and (ccd.isUse is null or ccd.isUse = 0) and ccd.status = 1 and ccd.orderId is null";
		
		//String sql = " from CtCoupon cc where cc.GId = '" + gid + "' and cc.etime >= '" + time+ "'";
		Query query= getSession().createQuery(hql);
		return query.list();
	}

	@Override
	public List<CtCouponDetail> getCouponByCouponId(Integer couponId) {
		List<CtCouponDetail> coupons;
		String hql = "from CtCouponDetail ccd where ccd.ctCoupon.couponId="+couponId +" and ccd.UId is null order by CDetailId";
		Query query = getSession().createQuery(hql);
		coupons = query.list();
		return coupons;
	}

	@Override
	public List<ViewGoodsList> callGoodsByPrice(String price1, String price2) {
		String hql = "from ViewGoodsList cg where cg.shopPrice between " + price1 + " and " + price2;
		Query query = getSession().createQuery(hql);
		return query.list();
	}

	@Override
	public String getGoodsTypeByGid(Long gid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long getGoodsExTypeByTypeId(String typeId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CtGoodsCategory> getCateByEx(Long exType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CtCoupon> getCouponByGidNew(Long gid) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		
		String hql = " from CtCoupon cc where cc.etime >= '"+sdf.format(new Date())+"' and cc.GId = " + gid;
		Query query = getSession().createQuery(hql);
		List<CtCoupon> list = query.list();
		return list;
	}

	@Override
	public List<CtCollectGoods> loadAll(Long UId) {
		// TODO Auto-generated method stub
		List<CtCollectGoods> collect = null;
		String hql = " from CtCollectGoods where UId="+UId;
		Query query = getSession().createQuery(hql);
		collect = query.list();
		return collect;
	}

	@Override
	public List<CtCollect> findAll(Page page) {
		List<CtCollect> collect = null;
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		Long UId = cu.getUId();
		String hql = "from CtCollect cc where cc.UId="+UId+" order by cc.colltime desc";
		Query query = getSession().createQuery(hql);
		if(page != null){
			if (page.getCurrentPage() != 0) {
				page.setTotalCount(this.orderCount(UId));
				query.setFirstResult(page.getFirstIndex());
				query.setMaxResults(page.getPageSize());
			}
			Long count = page.getTotalCount() / page.getPageSize();
			if(count.equals(0L)){
				count = 1L;
			}
			page.setTotalPage(count);
			
		}
		collect = query.list();
		return collect;
	}

	private Long orderCount(Long uId) {
		String hql = "select count(*) from CtCollect cc where cc.UId="+uId+" order by cc.colltime desc";
		Long count = (Long) getSession().createQuery(hql).uniqueResult();
		return count;
	}

	@Override
	public String save(CtCollect collect) {
		String hql = "insert into Ct_Collect(COLL_ID,COLL_TITLE,COLL_DESC,COLL_TIME,U_ID) values(seq_collect.nextval,'"+collect.getColltitle()+"','"+collect.getColldesc()+"','"+collect.getColltime()+"','"+collect.getUId()+"')";
		Query query = getSession().createSQLQuery(hql);
		Long id = (long) query.executeUpdate();
		String res=id.toString();
		return res;
	}

	@Override
	public CtCollect getCtCollByCollTitle(String collTitle,String collid) {
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		Long UId = cu.getUId();
		
		String sql = "select * from Ct_Collect cc where cc.coll_title='"+collTitle+"' and cc.U_Id="+UId;
		if (collid != null && !collid.equals("")){
			sql += " and cc.COLL_ID<>"+ Long.valueOf(collid);
		}
		
		Query query = getSession().createSQLQuery(sql).addEntity(CtCollect.class);
		List<CtCollect> colls = query.list();		
		CtCollect coll;
		if(colls.size()>0){
			coll = colls.get(0);
		}else {
			coll = null;
		}
		return coll;
	}

	@Override
	public void delCollGoodsByCollId(Long collid) {
		// TODO Auto-generated method stub
		String hql="delete CtCollectGoods as ccg where ccg.collid=?";
		Query query= getSession().createQuery(hql);
		query.setParameter(0, collid);
		query.executeUpdate();
	}

	@Override
	public void delete(Long collMid) {
		// TODO Auto-generated method stub
		String hql="delete CtCollect as cc where cc.collid=?";		
		Query query= getSession().createQuery(hql);
		query.setParameter(0, collMid);
		query.executeUpdate();
	}

	@Override
	public CtCollect getCtCollByCollId(Long CollId) {
		// TODO Auto-generated method stub
		CtCollect collect;
		String hql = "from CtCollect cc where cc.collid=?";
		Query query = getSession().createQuery(hql);
		query.setParameter(0, CollId);
		collect = (CtCollect) query.uniqueResult();
		return collect;
	}

	@Override
	public String updateColl(CtCollect collect) {
		// TODO Auto-generated method stub
		getSession().merge(collect);
		return "success";
	}
	
	@Override
	public boolean isSmall(Long gId,Integer sn) {
		// TODO Auto-generated method stub
		String hql="select simSNum from CtRangePrice where GId="+gId+" and rownum<2 order by RPid ";

		Object count= getSession().createQuery(hql).uniqueResult();
		if (count != null && count.equals(sn)){
			return true;
		}else
			return false;
		
	}

	@Override
	public List<CtCollect> search(String keyword, Page page) {
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		Long UId = cu.getUId();
		List<CtCollect> collect=null;
		String hql="from CtCollect cc where (cc.colltitle like '%"+keyword+"%' or cc.colldesc like '%"+keyword+"%') and cc.UId="+UId+" order by cc.colltime desc";
//		String str="";
//		if (!keyword.equals("")){
//			str+=" ";
//		}
//		//str+=" order by regionID desc";
//		hql+=str;
		Query query= getSession().createQuery(hql);
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		collect=query.list();
		return collect;
	}

	@Override
	public List<CtCollectGoods> loadCollDetailAll(Page page, Long collid) {
		List<CtCollectGoods> CollectList;
		
		String hql = "from CtCollectGoods ccg where ccg.collid=?";
		Query query = getSession().createQuery(hql);
		query.setParameter(0, collid);
//		if (page.getCurrentPage()!=0){
//			page.setTotalCount(this.totalCountCtBomGoods(collid));
//			query.setFirstResult(page.getFirstIndex());
//			query.setMaxResults(page.getPageSize());
//		}
		CollectList = query.list();
		
		return CollectList;
	}

	@Override
	public CtCollectGoods getCtCollGoodsByGId(Long GId, Long collid) {
		String hql = "from CtCollectGoods ccg where ccg.GId=? and ccg.collid=?";
		Query query = getSession().createQuery(hql);
		query.setParameter(0, GId);
		query.setParameter(1, collid);
		List<CtCollectGoods> lists = query.list();	
		CtCollectGoods collGoods=null;
		if(lists.size() >0){
			collGoods = lists.get(0);
		}else {
			collGoods = null;
		}
		return collGoods;
	}

	@Override
	public List<CtCollect> getCtCollByUId(Long UId) {
		List<CtCollect> collects = null;	
		String hql = "from CtCollect cc where cc.UId="+UId+" order by cc.colltime desc";
		String str = "where cc.UId="+UId+" order by cc.colltime desc";
		Query query = getSession().createQuery(hql);
		collects = query.list();
		return collects;
	}

	@Override
	public List<CtGoodsCategory> queryAll() {
		List<CtGoodsCategory> cate = null;
		Query query= getSession().createQuery("from CtGoodsCategory as cate where cate.parentId <> 0 order by cate.parentId,cate.sortOrder");
		cate = query.list();
		return cate;
	}

	@Override
	public List<CtGoodsCategory> queryAllByPid(Long pid) {
		List<CtGoodsCategory> ccg;
		String hql = "from CtGoodsCategory ccg where ccg.parentId="+pid;
		Query query = getSession().createQuery(hql);
		ccg= query.list();	
		return ccg;
	}

	@Override
	public CtGoodsCategory queryAllByCName(String cname) {
		CtGoodsCategory ccg;
		String hql = "from CtGoodsCategory ccg where ccg.parentId= 0 and ccg.CName='"+cname+"'";
		Query query = getSession().createQuery(hql);
		ccg = (CtGoodsCategory) query.uniqueResult();
		return ccg;
	}

	@Override
	public CtGoodsCategory queryByCName(String cname) {
		CtGoodsCategory ccg;
		String hql = "from CtGoodsCategory ccg where ccg.parentId<>0 and ccg.CName='"+cname+"'";
		Query query = getSession().createQuery(hql);
		ccg = (CtGoodsCategory) query.uniqueResult();
		return ccg;
	}

	@Override
	public CtGoodsCategory getCNameByCid(Long cid) {
		CtGoodsCategory ccg;
		String hql = "from CtGoodsCategory ccg where ccg.CId='"+cid+"'";
		Query query = getSession().createQuery(hql);
		ccg = (CtGoodsCategory) query.uniqueResult();
		return ccg;
	}

	@Override
	public List<CtNotice> findAllNotices() {
		String hql = " from CtNotice cn order by cn.noTime desc";
		Query query = getSession().createQuery(hql);
		List<CtNotice> list = query.list();
		List<CtNotice> list1 = new ArrayList<CtNotice>();
		if(list.size() > 5){
			for (int i = 0; i < 5; i++) {
				list1.add(list.get(i));
			}
		} else {
			list1 = list;
		}
		return list1;
	}

	@Override
	public CtGoodsBrand findById(Long id) {
		// TODO Auto-generated method stub
		CtGoodsBrand brank = (CtGoodsBrand)getSession().get(CtGoodsBrand.class, id);
		return brank;
	}

	@Override
	public List<CtGoodsCategory> getSanjiFenLei() {
		String hql = " from CtGoodsCategory cgc where cgc.parentId = 2";
		Query query = getSession().createQuery(hql);
		List<CtGoodsCategory> list = query.list();
		List<CtGoodsCategory> list1 = new ArrayList<CtGoodsCategory>();
		List<CtGoodsCategory> list2 = new ArrayList<CtGoodsCategory>();
		List<CtGoodsCategory> list3 = new ArrayList<CtGoodsCategory>();
		for (int i = 0; i < list.size(); i++) {
			hql = " from CtGoodsCategory cgc where cgc.parentId = " + list.get(i).getCId();
			query = getSession().createQuery(hql);
			list1 = query.list();
			for (int j = 0; j < list1.size(); j++) {
				list2.add(list1.get(j));
			}
		}
		if(list2.size() > 11){
			for (int i = 0; i <11; i++) {
				list3.add(list2.get(i));
			}
		} else {
			list3 = list2;
		}
		return list3;
	}

	@Override
	public List<CtGoodsCategory> getSanjiFenLeiDianRong() {
		String hql = " from CtGoodsCategory cgc where cgc.parentId = 3";
		Query query = getSession().createQuery(hql);
		List<CtGoodsCategory> list = query.list();
		List<CtGoodsCategory> list1 = new ArrayList<CtGoodsCategory>();
		List<CtGoodsCategory> list2 = new ArrayList<CtGoodsCategory>();
		List<CtGoodsCategory> list3 = new ArrayList<CtGoodsCategory>();
		for (int i = 0; i < list.size(); i++) {
			hql = " from CtGoodsCategory cgc where cgc.parentId = " + list.get(i).getCId();
			query = getSession().createQuery(hql);
			list1 = query.list();
			for (int j = 0; j < list1.size(); j++) {
				list2.add(list1.get(j));
			}
		}
		if(list2.size() > 6){
			for (int i = 0; i < 6; i++) {
				list3.add(list2.get(i));
			}
		} else {
			list3 = list2;
		}
		return list3;
	}

	@Override
	public Map<Integer, String> getCateFenLeiAll(Long gId) {
		Map<Integer, String> map = new HashMap<Integer, String>();
		String hql = " from CtGoods cg where cg.GId = " + gId;
		Query query = getSession().createQuery(hql);
		CtGoods goods = (CtGoods) query.list().get(0);
		hql = " from CtGoodsCategory cgc where cgc.CId = " + goods.getCId();
		query = getSession().createQuery(hql);
		CtGoodsCategory goodsCategory = (CtGoodsCategory) query.list().get(0);
		if(!goodsCategory.getParentId().equals(0L)){
			map.put(0, goodsCategory.getCName()+"_"+goodsCategory.getCId());
			hql = " from CtGoodsCategory cgc where cgc.CId = " + goodsCategory.getParentId();
			query = getSession().createQuery(hql);
			CtGoodsCategory goodsCategory1 = (CtGoodsCategory) query.list().get(0);
			if(!goodsCategory1.getParentId().equals(0L)){
				map.put(1, goodsCategory1.getCName()+"_"+goodsCategory1.getCId());
				hql = " from CtGoodsCategory cgc where cgc.CId = " + goodsCategory1.getParentId();
				query = getSession().createQuery(hql);
				CtGoodsCategory goodsCategory2 = (CtGoodsCategory) query.list().get(0);
				map.put(2, goodsCategory2.getCName()+"_"+goodsCategory2.getCId());
			} else {
				map.put(1, goodsCategory1.getCName()+"_"+goodsCategory1.getCId());
			}
		} else {
			map.put(0, goodsCategory.getCName()+"_"+goodsCategory.getCId());
		}
		
		return map;
	}

	@Override
	public CtCart getcartBycartid(Long cartId) {
		CtCart cart;
		String hql = "from CtCart cc where cc.cartId="+cartId;
		Query query = getSession().createQuery(hql);
		cart = (CtCart) query.uniqueResult();
		return cart;
	}

	@Override
	public List<CtGoods> getSubHade() {

		String hql = " from CtGoods cg where cg.isSubhead = 1";
		Query query = getSession().createQuery(hql);
		return query.list();
	}

	@Override
	public List<CtGoodsResource> getResultIdByGid(Long gid) {
		String hql = " from CtGoodsResource cgr where cgr.GId = " + gid;
		Query query = getSession().createQuery(hql);
		List<CtGoodsResource> list = query.list();
		if(list != null && list.size() > 0){
			return list;
		}
		return null;
	}

	@Override
	public CtResource getResByResId(Long resourceId) {
		String hql = " from CtResource cr where cr.RId = " + resourceId;
		Query query = getSession().createQuery(hql);
		List<CtResource> list = query.list();
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}

	@Override
	public List<CtGoodsCategory> getCateForFirst() {
		String hql = " from CtGoodsCategory cgc where cgc.parentId = 0 order by cgc.sortOrder asc";
		Query query = getSession().createQuery(hql);
		query.setCacheable(true);
		return query.list();
	}

	@Override
	public List<CtGoodsCategory> getCateSer(List<CtGoodsCategory> cateList) {
		String hql = "";
		Query query = null;
		List<CtGoodsCategory> cateListSerSing = new ArrayList<CtGoodsCategory>();
		List<CtGoodsCategory> cateListSerAll = new ArrayList<CtGoodsCategory>();
		for (int i = 0; i < cateList.size(); i++) {
			hql = " from CtGoodsCategory cgc where cgc.parentId = " + cateList.get(i).getCId();
			query = getSession().createQuery(hql);
			query.setCacheable(true);
			cateListSerSing = query.list();
			for (int j = 0; j < cateListSerSing.size(); j++) {
				cateListSerAll.add(cateListSerSing.get(j));
			}
		}
		return cateListSerAll;
	}

	public List<CtGoodsCategory> getCateThree(List<CtGoodsCategory> cateListSer) {
		String hql = "";
		Query query = null;
		List<CtGoodsCategory> cateListSerSing = new ArrayList<CtGoodsCategory>();
		List<CtGoodsCategory> cateListSerAll = new ArrayList<CtGoodsCategory>();
		for (int i = 0; i < cateListSer.size(); i++) {
			hql = " from CtGoodsCategory cgc where cgc.parentId = " + cateListSer.get(i).getCId();
			query = getSession().createQuery(hql);
			query.setCacheable(true);
			cateListSerSing = query.list();
			for (int j = 0; j < cateListSerSing.size(); j++) {
				cateListSerAll.add(cateListSerSing.get(j));
			}
		}
		return cateListSerAll;
	}

	@Override
	public List<CtGoodsAttributeRelation> getAttrRelByGid(Long gid) {
		String hql = " from CtGoodsAttributeRelation cgar where cgar.GId = " + gid;
		Query query = getSession().createQuery(hql);
		List<CtGoodsAttributeRelation> list = query.list();
		if(list != null && list.size() > 0){
			return list;
		}
		return null;
	}

	@Override
	public CtGoodsAttribute getattrByAttrId(Long attrId) {
		String hql = " from CtGoodsAttribute cgar where (cgar.attrId <> 14 and cgar.attrId <> 27) and cgar.attrId = " + attrId;
		Query query = getSession().createQuery(hql);
		List<CtGoodsAttribute> list = query.list();
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}

	@Override
	public List<CtGoodsAttributeRelation> getAttrRelByAttrId(Long attrId, Long gid) {
		String hql = " from CtGoodsAttributeRelation cgar where (cgar.attrId <> 14 and cgar.attrId <> 27) and cgar.attrId = " + attrId +" and cgar.GId=" + gid;
		Query query = getSession().createQuery(hql);
		return query.list();
	}

	@Override
	public List<CtGoodsAttribute> getAttrByGTIdOrder(Long gtId) {
		String hql = " from CtGoodsAttribute cgar where cgar.GTId = " + gtId + " order by cgar.sortOrder desc";
		Query query = getSession().createQuery(hql);
		return query.list();
	}

	@Override
	public List<CtGoods> getGoodsByGid(Long gid) {
		String hql = " from CtGoods cc where cc.GId="+gid;
		Query query = getSession().createQuery(hql);
		List<CtGoods> list = query.list();
		if(list != null && list.size() > 0){
			return list;
		}
		return null;
	}

	@Override
	public List<CtGoods> loadGoods() {
		List<CtGoods> ctgoods = null;
		String hql=" from CtGoods ";
		Query query= getSession().createQuery(hql);
		ctgoods = query.list();
		return ctgoods;
	}

	@Override
	public List<CtGoods> getGoodsByBrandId(Long bId) {
		String hql = " from CtGoods cc where cc.BId="+bId;
		Query query= getSession().createQuery(hql);
		query.setFirstResult(0);
		query.setMaxResults(6);
		List<CtGoods> list = query.list();
		if(list != null && list.size() > 0){
			return list;
		}
		return null;
	}

	@Override
	public String getCateNameByCateId(Long cId) {
		String hql = " from CtGoodsCategory cc where cc.CId="+cId;
		Query query= getSession().createQuery(hql);
		List<CtGoodsCategory> list = query.list();
		if(list != null && list.size() > 0){
			return list.get(0).getCName();
		}
		return null;
	}

	@Override
	public void addGoodsKey(CtSearchKeyword searchKeyWord) {
		getSession().merge(searchKeyWord);
	}

	@Override
	public CtSearchKeyword findByKeyWord(String kword) {
		String hql = " from CtSearchKeyword cs where cs.SKeyword='"+kword+"'";
		Query query= getSession().createQuery(hql);
		List<CtSearchKeyword> list = query.list();
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}

	@Override
	public List<CtSearchKeyword> findKeyWordOrderTop4() {
		String hql = " from CtSearchKeyword cs order by cs.SNum desc,cs.SGoodsnum desc";
		Query query= getSession().createQuery(hql);
		query.setMaxResults(4);
		query.setFetchSize(0);
		List<CtSearchKeyword> list = query.list();
		if(list != null && list.size() > 0){
			return list;
		}
		return null;
	}

	@Override
	public void GoodsKeyNew(CtSearchKeyword searchKeyWord) {
		String sql = " insert into ct_search_keyword values(seq_search.nextval,'"+searchKeyWord.getSKeyword()+"', "+searchKeyWord.getSNum()+", "+searchKeyWord.getSGoodsnum()+")";
		Query query= getSession().createSQLQuery(sql);
		int count = query.executeUpdate();
		System.out.println(count);
	}

	@Override
	public Object merge(Object ctGoods) {
		return getSession().merge(ctGoods);
	}

	@Override
	public List<CtGoods> findByGSn(String seller_outer_no) {
		String hql = " from CtGoods cg where cg.GSn='"+seller_outer_no+"'";
		Query query= getSession().createQuery(hql);
		return query.list();
		
	}

	@Override
	public Object findByTypeName(String string, String string2,
			String string3, int i) {
		String[] duoge = string2.split(",");
		String[] duogevalue = string.split(",");
		String str = "";
		for (int j = 0; j < duoge.length; j++) {
			str += duoge[j]+"= '"+duogevalue[j]+"' and ss.";
		}
		str = str.substring(0, str.length()-7);
		String hql = " from "+string3+" ss where ss."+str+" ";
		System.out.println(hql);
		Query query = getSession().createQuery(hql);
		if(i==1){
			return query.list();
		}
		if(i==0){
			List list = query.list();
			if(list != null && list.size() > 0){
				return list.get(0);
			} else {
				return null;
			}
		}
		return null;
	}

	@Override
	public CtXwStock findByXWStock() {
		try {
			String hql = " from CtXwStock cws";
			Query query = getSession().createQuery(hql);
			List<CtXwStock> list = query.list();
			if(list != null && list.size() > 0){
				return list.get(0);
			}
			return null;
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void deleteRanPriceByGid(Long gId) {
		String hql = "delete from CtRangePrice crp where crp.GId="+gId;
		Query query = getSession().createQuery(hql);
		query.executeUpdate();
	}

	@Override
	public List<CtResource> findResouresByTopSex() {
		String hql = " from CtResource cr where cr.RTId <> 20 and 1=(select count(*) from CtResource c where cr.RName = c.RName) order by cr.RSort asc, cr.RId desc";
		Query query = getSession().createQuery(hql);
		query.setMaxResults(6);
		return query.list();
	}

	@Override
	public List<CtResource> findResoursByPage(Page page) {
		String hql  = " from CtResource cr where cr.RTId <> 20 and 1=(select count(*) from CtResource c where cr.RName = c.RName) order by cr.RSort asc, cr.RId desc";
		Query query = getSession().createQuery(hql);
		if (page.getCurrentPage()!=0){
			page.setTotalCount(totalCoutResour());
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		List<CtResource> list = query.list();
		if(list != null && list.size() > 0){
			return list;
		}
		return null;
	}

	private Long totalCoutResour() {
		String hql = "select count(*) from CtResource cr where RTId <> 20 and 1=(select count(*) from CtResource c where cr.RName = c.RName)";
		Query query = getSession().createQuery(hql);
		return (Long) query.uniqueResult();
	}

	@Override
	public CtReplace findReplaceByKey(String key) {
		key = key.toUpperCase();
		String hql = " from CtReplace cr where cr.re1='"+key+"' or cr.re2='"+key+"' or cr.re3='"+key+"' or cr.re4='"+key+"' or cr.re5='"+key+"' ";
		Query query = getSession().createQuery(hql);
		List<CtReplace> list = query.list();
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}

	@Override
	public List<CtAd> findAdByIsUpAndType(int i, String str) {
		String hql = " from CtAd ca where ca.adIsUp = "+i+" and ca.ctAdType.adTypeName='"+str+"'  order by ca.adSort asc";
		Query query = getSession().createQuery(hql);
		List<CtAd> list = query.list();
		if(list != null && list.size() > 0){
			return list;
		}
		return null;
	}
}

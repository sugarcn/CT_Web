package org.ctonline.dao.goods.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.goods.CtBomDao;
import org.ctonline.po.goods.CtBom;
import org.ctonline.po.goods.CtBomGoods;
import org.ctonline.po.goods.CtCollectGoods;
import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.goods.CtRangePrice;
import org.ctonline.po.order.CtCart;
import org.ctonline.po.user.CtUser;
import org.ctonline.util.Page;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.opensymphony.xwork2.ActionContext;

public class CtBomDaoImpl extends BaseHibernateDAOImpl implements CtBomDao {

	@Override
	public List<CtBom> loadAll(Page page) {
		// TODO Auto-generated method stub
		List<CtBom> boms = null;
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		Long UId = cu.getUId();
		
		String hql = "from CtBom cb where cb.UId="+UId+" order by cb.bomTime desc";
		String str = "where cb.UId="+UId+" order by cb.bomTime desc";
		Query query = getSession().createQuery(hql);
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(str));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		Long count = page.getTotalCount() / page.getPageSize();
		
		if(count.equals(0L)){
			count = 1L;
		}
		page.setTotalPage(count);
		boms = query.list();
		
		return boms;
	}
	@Override
	public Integer countBom(Integer id){
		
		String hql = "select count(bomId) FROM CtBomGoods where bomId ="+id;
		Long count = (Long)getSession().createQuery(hql).uniqueResult();
		Integer cou = count.hashCode();
		return cou;
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtBom cb ";
		if (str !=null)
			hql+=str;
		 
		Long count= (Long)getSession().createQuery(hql).uniqueResult();
		
		return count;
	}

	@Override
	public String save(CtBom ctBom) {
		// TODO Auto-generated method stub
		Integer id;
		 
		id =  (Integer) getSession().save(ctBom);
		
		String res=id.toString();
		return res;
	}

	@Override
	public CtBom getCtBomByBomId(Integer BomId, Long uid) {
		// TODO Auto-generated method stub
		CtBom bom;
		 
		String hql = "from CtBom cb where cb.bomId=? and cb.UId="+uid;
		Query query = getSession().createQuery(hql);
		query.setParameter(0, BomId);
		bom = (CtBom) query.uniqueResult();
		
		return bom;
	}

	@Override
	public String update(CtBom ctBom) {
		// TODO Auto-generated method stub
		 
		getSession().update(ctBom);
		return "success";
		
	}

	@Override
	public void delete(Integer bomMid) {
		// TODO Auto-generated method stub
		String hql="delete CtBom as cb where cb.bomId=?";
		
		Query query= getSession().createQuery(hql);
		query.setParameter(0, bomMid);
		query.executeUpdate();
		
		
	}

	@Override
	public List<CtBom> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		Long UId = cu.getUId();
		List< CtBom> boms=null;
		String sql="from CtBom cb";
		String str="";
		if (!keyword.equals("")){
			str+=" where ( cb.bomTitle like '%"+keyword+"%' or cb.bomDesc like '%"+keyword+"%' ) and cb.UId="+UId+"  order by cb.bomTime desc";
		}
		//str+=" order by regionID desc";
		sql+=str;
		
		Query query= getSession().createQuery(sql);
		page.setTotalCount(this.totalCount(str));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		boms=query.list();
		
		return boms;
	}

	@Override
	public List<CtBomGoods> loadBomDetailAll(Page page,Integer bomId) {
		// TODO Auto-generated method stub
		List<CtBomGoods> bomGoodsList;		
		String hql = "from CtBomGoods cbg where cbg.bomId="+bomId;
		Query query = getSession().createQuery(hql);
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCountCtBomGoods(bomId));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		bomGoodsList = query.list();		
		return bomGoodsList;
	}
	
	public Long totalCountCtBomGoods(Integer bomId) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtBomGoods cbg where cbg.bomId="+bomId;
		 
		Long count= (Long)getSession().createQuery(hql).uniqueResult();
		
		return count;
	}

	@Override
	public void delGoodsOfBomDetail(Long bomGoodsId) {
		// TODO Auto-generated method stub
		String hql="delete CtBomGoods as cbg where cbg.bomGoodsId=?";
		
		Query query= getSession().createQuery(hql);
		query.setParameter(0, bomGoodsId);
		query.executeUpdate();
		
		
	}

	@Override
	public List<CtBomGoods> findBomDetailAll(String keyword, Page page,Integer bomId) {
		// TODO Auto-generated method stub
		List< CtBomGoods> bomGoodsList=null;
		String hql="from CtBomGoods cbg where ( cbg.goods.GName like '%"+keyword+"%' or cbg.goods.goodsBrand.BName like '%"+keyword+"%' or cbg.goods.goodsCategory.CName like '%"+keyword+"%' or cbg.goods.shopPrice like '%"+keyword+"%') and cbg.bomId="+bomId;
		
		Query query= getSession().createQuery(hql);
		page.setTotalCount(this.totalCountOfBomDetailAll(hql));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		bomGoodsList=query.list();
		
		return bomGoodsList;
	}
	
	public Long totalCountOfBomDetailAll(String hql) {
		// TODO Auto-generated method stub
		String str = "select count (*) ";
		str = str + hql;
		 
		Long count= (Long)getSession().createQuery(str).uniqueResult();
		
		return count;
	}

	@Override
	public CtBomGoods getCtBomGoodsBybomGoodsId(Long bomGoodsId) {
		// TODO Auto-generated method stub
		 
		String hql = "from CtBomGoods cbg where cbg.bomGoodsId=?";
		Query query = getSession().createQuery(hql);
		query.setParameter(0, bomGoodsId);
		List<CtBomGoods> lists = query.list();
		
		CtBomGoods bomGoods=null;
		if(lists.size() >0){
			 bomGoods = lists.get(0);
		}else {
			bomGoods = null;
		}
		return bomGoods;
	}

	@Override
	public String saveCtBomGoods(CtBomGoods cbg) {
		// TODO Auto-generated method stub
		 
		Long id = (Long) getSession().save(cbg);
		
		String res=id.toString();
		return res;
	}

	@Override
	public String updateCtBomGoods(CtBomGoods cbg) {
		// TODO Auto-generated method stub
		 
		//getSession().update(cbg);
		
		getSession().merge(cbg);
		String res = "success";
		return res;
	}

	@Override
	public CtBomGoods getCtBomGoodsByGId(Long GId,Integer bomId) {
		// TODO Auto-generated method stub
		 
		String hql = "from CtBomGoods cbg where cbg.bomGoodsId="+GId+" and cbg.bomId="+bomId+"";
		Query query = getSession().createQuery(hql);
		List<CtBomGoods> lists = query.list();
		
		CtBomGoods bomGoods=null;
		if(lists.size() >0){
			 bomGoods = lists.get(0);
		}else {
			bomGoods = null;
		}
		return bomGoods;
	}
	
	public CtBomGoods getCtBomGoodsByGIdandPack(Long GId,Integer bomId,String pack) {
		// TODO Auto-generated method stub
		 
		String hql = "from CtBomGoods cbg where cbg.GId=? and cbg.bomId=? and pack=?";
		Query query = getSession().createQuery(hql);
		query.setParameter(0, GId);
		query.setParameter(1, bomId);
		query.setParameter(2, pack);
		List<CtBomGoods> lists = query.list();
		
		CtBomGoods bomGoods=null;
		if(lists.size() >0){
			 bomGoods = lists.get(0);
		}else {
			bomGoods = null;
		}
		return bomGoods;
	}

	@Override
	public CtBom getCtBomByBomTitle(String bomTitle,String bomId) {
		// TODO Auto-generated method stub
		
		 
		String hql = "from CtBom cb where cb.bomTitle='"+bomTitle+"'";
		
		if (bomId != null && !bomId.equals("")){
			hql += " and cb.bomId<>"+ Long.valueOf(bomId);
		}
		
		Query query = getSession().createQuery(hql);
		List<CtBom> boms = query.list();
		
		CtBom cb;
		if(boms.size()>0){
			cb = boms.get(0);
		}else {
			cb = null;
		}
		return cb;
	}

	@Override
	public List<CtBomGoods> getCtBomGoodsByBomId(Integer bomId) {
		// TODO Auto-generated method stub
		 
		String hql = "from CtBomGoods cbg where cbg.bomId="+bomId + " and cbg.goodsNum <> -1";
		Query query = getSession().createQuery(hql);
		List<CtBomGoods> bomGoods = query.list();
		
		return bomGoods;
	}

	@Override
	public List<CtBom> loadAllHot(Page page) {
		// TODO Auto-generated method stub
		List<CtBom> boms = null;
		Map usersession = ActionContext.getContext().getSession();
//		CtUser cu = (CtUser) usergetSession().get("userinfosession");
//		Long UId = cu.getUId();
		
		String hql = " from CtBom cb where cb.isHot= 1  order by cb.num desc";
		String str = "where cb.isHot= 1";
		Query query = getSession().createQuery(hql);
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(str));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		boms = query.list();
		
		return boms;
	}

	@Override
	public List<CtBom> loadAllHotLogin(Page page) {
		// TODO Auto-generated method stub
		List<CtBom> boms = null;
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		Long UId = cu.getUId();
		
		String hql = "from CtBom cb where cb.isHot= 1 and cb.UId <> "+UId+" order by cb.num desc";
		String str = "where cb.isHot= 1 and cb.UId <> "+UId;
		Query query = getSession().createQuery(hql);
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(str));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		boms = query.list();
		
		return boms;
	}

	@Override
	public List<CtBom> getCtBomsByUId(Long UId) {
		// TODO Auto-generated method stub
		List<CtBom> boms = null;
		
		String hql = "from CtBom cb where cb.UId="+UId+" order by cb.bomTime desc";
		String str = "where cb.UId="+UId+" order by cb.bomTime desc";
		Query query = getSession().createQuery(hql);
		boms = query.list();
		
		return boms;
	}

	@Override
	public List<CtBom> findByKeyWord(String keyword, Page page) {
		// TODO Auto-generated method stub
		List< CtBom> boms=null;
		String sql=" from CtBom cb ";
		String str="";
		if (!keyword.equals("")){
			str+=" where ( cb.bomTitle like '%"+keyword+"%' or cb.bomDesc like '%"+keyword+"%' )  and cb.isHot=1 order by cb.bomTime desc";
		}
		//str+=" order by regionID desc";
		sql+=str;
		
		Query query= getSession().createQuery(sql);
		page.setTotalCount(this.totalCount(str));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		boms=query.list();
		
		return boms;
	}

	@Override
	public List<CtBom> findByKeyWordLogin(String keyword, Page page) {
		// TODO Auto-generated method stub
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		Long UId = cu.getUId();
		List< CtBom> boms=null;
		String sql=" from CtBom cb ";
		String str="";
		if (!keyword.equals("")){
			str+=" where ( cb.bomTitle like '%"+keyword+"%' or cb.bomDesc like '%"+keyword+"%' ) and cb.UId<>"+UId+" and cb.isHot=1 order by cb.bomTime desc";
		}
		//str+=" order by regionID desc";
		sql+=str;
		
		Query query= getSession().createQuery(sql);
		page.setTotalCount(this.totalCount(str));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		boms=query.list();
		
		return boms;
	}

	@Override
	public void delBomGoodsByBomId(Integer bomId) {
		// TODO Auto-generated method stub
		String hql="delete CtBomGoods as cbg where cbg.bomId=?";
		
		Query query= getSession().createQuery(hql);
		query.setParameter(0, bomId);
		query.executeUpdate();
		
		
	}

	@Override
	public List<CtBom> loadAll() {
		// TODO Auto-generated method stub
		List<CtBom> boms = null;
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		Long UId = cu.getUId();
		String hql = "from CtBom cb where cb.UId="+UId+" order by cb.bomTime desc";
		String str = "where cb.UId="+UId+" order by cb.bomTime desc";
		Query query = getSession().createQuery(hql);
		boms = query.list();
		return boms;
	}

	@Override
	public List<CtBomGoods> getCtBomGoodsByGIdAndBomId(Long GId, Integer bomId) {
		// TODO Auto-generated method stub
		String hql = "from CtBomGoods cbg where cbg.GId=? and cbg.bomId=?";
		Query query = getSession().createQuery(hql);
		query.setParameter(0, GId);
		query.setParameter(1, bomId);
		List<CtBomGoods> lists = query.list();
		
		return lists;
	}
	
	@Override
	public CtCart getcartBycartid(Long cartId) {
		CtCart cart;
		String hql = "from CtCart cc where cc.cartId="+cartId;
		Query query = getSession().createQuery(hql);
		cart = (CtCart) query.uniqueResult();
		return cart;
	}
	@Override
	public CtBomGoods getCtBomGoodsByGId(Long GId, Long bomId) {
		String hql = "from CtBomGoods cb where cb.GId=? and cb.bomId=?";
		Query query = getSession().createQuery(hql);
		query.setParameter(0, GId);
		query.setParameter(1, bomId);
		List<CtBomGoods> lists = query.list();	
		CtBomGoods ctbom=null;
		if(lists.size() >0){
			ctbom = lists.get(0);
		}else {
			ctbom = null;
		}
		return ctbom;
	}
	@Override
	public List<CtBom> findSome() {
		List<CtBom> boms = null;
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		Long UId = cu.getUId();	
		String hql = "from CtBom cb where cb.UId="+UId+" order by cb.bomTime desc";
		Query query = getSession().createQuery(hql);
		boms = query.list();	
		return boms;
	}
	
	@Override
	public String updateCtBomNum(Long bomId,Long bomNum) {
		String hql = null;
		String res = "success";
		try{
			if(bomId != null && bomNum != null){
				hql = "update CtBom c set c.num = " + bomNum + " where c.bomId=" + bomId;
			}
			Query query = getSession().createQuery(hql);
			query.executeUpdate();

		}catch(Exception e){
			res = "error";
			//e.printStackTrace();
		}
		return res;

	}
	@Override
	public List<CtRangePrice> findRanByBGid(String gid) {
		String hql = "from CtRangePrice cr where cr.GId = " + gid + " order by cr.simSNum desc";
		Query query = getSession().createQuery(hql);
		List<CtRangePrice> lists = query.list();	
		if(lists.size() >0){
			return lists;
		}
		return null;
	}
}

package org.ctonline.dao.goods;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder.In;

import org.ctonline.dao.BaseHibernateDAO;
import org.ctonline.po.goods.CtBom;
import org.ctonline.po.goods.CtBomGoods;
import org.ctonline.po.goods.CtCollectGoods;
import org.ctonline.po.goods.CtRangePrice;
import org.ctonline.po.order.CtCart;
import org.ctonline.util.Page;

public interface CtBomDao extends BaseHibernateDAO{

	//加载所有Bom
	public List<CtBom> loadAll(Page page);
	//在个人中心加载前4条数据
	public List<CtBom> findSome();
	//加载所有Bom
	public List<CtBom> loadAll();
	public Long totalCount(String str);
	//保存Bom
	public String save(CtBom ctBom);
	//根据BomId查询Bom
	public CtBom getCtBomByBomId(Integer BomId, Long uid);
	//修改Bom
	public String update(CtBom ctBom);
	//批量删除Bom
	public void delete(Integer bomMid);
	//搜索
	public List<CtBom> findAll(String keyword, Page page);
	//加载所有Bom详情
	public List<CtBomGoods> loadBomDetailAll(Page page,Integer bomId);
	//删除Bom详情中的某个商品
	public void delGoodsOfBomDetail(Long bomGoodsId);
	//搜索Bom详情中的商品
	public List<CtBomGoods> findBomDetailAll(String keyword, Page page,Integer bomId);
	//根据bomGoodsId查询CtBomGoods
	public CtBomGoods getCtBomGoodsBybomGoodsId(Long bomGoodsId);
	//保存CtBomGoods
	public String saveCtBomGoods(CtBomGoods cbg);
	//修改CtBomGoods
	public String updateCtBomGoods(CtBomGoods cbg);
	//根据GId查询CtBomGoods
	public CtBomGoods getCtBomGoodsByGId(Long GId,Integer bomId);
	public CtBomGoods getCtBomGoodsByGIdandPack(Long GId,Integer bomId,String pack);
	//根据bomTitle查询Bom
	public CtBom getCtBomByBomTitle(String bomTitle,String bomId);
	//更加bomId查询bom详情
	public List<CtBomGoods> getCtBomGoodsByBomId(Integer bomId);
	//加载所有热点Bom未登录
	public List<CtBom> loadAllHot(Page page);
	//加载所有热点Bom用户登陆后
	public List<CtBom> loadAllHotLogin(Page page);
	//根据UId查询Bom
	public List<CtBom> getCtBomsByUId(Long UId);
	//bom中心未登录搜索
	public List<CtBom> findByKeyWord(String keyword, Page page);
	//bom中心已经登录搜索
	public List<CtBom> findByKeyWordLogin(String keyword, Page page);
	//根据bomId删除bom详情中的商品
	public void delBomGoodsByBomId(Integer bomId);
	//新增bom详情时根据GId查询CtBomGoods
	public List<CtBomGoods> getCtBomGoodsByGIdAndBomId(Long GId,Integer bomId);
	//获取Bom数量
	public Integer countBom(Integer id);
	public CtCart getcartBycartid(Long cartId);
	//根据GId查询BomGoods
	public CtBomGoods getCtBomGoodsByGId(Long GId,Long bomId);
	//更新bom数量
	public String updateCtBomNum(Long bomId,Long bomNum);
	public List<CtRangePrice> findRanByBGid(String gid);
}

package org.ctonline.dao.order.impl;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.HibernateSessionFactory;
import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.order.OrderDAO;
import org.ctonline.dto.order.OrderDTO;
import org.ctonline.po.basic.CtCashCoupon;
import org.ctonline.po.basic.CtCouponDetail;
import org.ctonline.po.basic.CtPaymentWx;
import org.ctonline.po.basic.CtTransaction;
import org.ctonline.po.goods.CtBomGoods;
import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.goods.CtGoodsBrand;
import org.ctonline.po.order.CtCart;
import org.ctonline.po.order.CtComplain;
import org.ctonline.po.order.CtEvaluation;
import org.ctonline.po.order.CtExpress;
import org.ctonline.po.order.CtOrderGoods;
import org.ctonline.po.order.CtOrderInfo;
import org.ctonline.po.order.CtRefund;
import org.ctonline.po.order.CtShop;
import org.ctonline.po.pay.CtPayInterface;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.user.CtUserAddress;
import org.ctonline.po.views.ViewCartList;
import org.ctonline.po.views.ViewOrderBom;
import org.ctonline.po.views.ViewOrderCheck;
import org.ctonline.po.views.ViewUserAddress;
import org.ctonline.util.Page;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.Service;
import org.hibernate.service.ServiceRegistry;

import com.opensymphony.xwork2.ActionContext;

public class OrderImpl extends BaseHibernateDAOImpl implements OrderDAO {

	@Override
	public List<ViewCartList> getCartByUId(Long UId) {
		// TODO Auto-generated method stub
		List<ViewCartList> cartList = null;
		String hql = "from ViewCartList as cart where cart.UId = " + UId +" and cart.carttype is not null order by cart.carttime desc";

		Query query = getSession().createQuery(hql);
		cartList = query.list();

		return cartList;
	}
	@Override
	public String addCart(CtCart cart) {
		// TODO Auto-generated method stub
		cart.setCartAddTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		getSession().merge(cart);
		return "success";
	}

	@Override
	public List<CtGoods> getGoodsByGId(Long gid) {
		// TODO Auto-generated method stub
		List<CtGoods> goodsList = null;
		String hql = "from CtGoods as cg where cg.GId = " + gid;

		Query query = getSession().createQuery(hql);
		goodsList = query.list();

		return goodsList;
	}

@Override
public List<ViewUserAddress> getUserAddress(Long uid) {
		// TODO Auto-generated method st
		List<ViewUserAddress> address = new ArrayList<ViewUserAddress>(0);
		List<ViewUserAddress> address1 = new ArrayList<ViewUserAddress>(0);
		// String hql =
		// "select ua.A_ID,ua.U_ID,ua.A_CONSIGNEE,ua.COUNTRY,ua.PROVINCE,ua.CITY,ua.DISTRICT,ua.ADDRESS,ua.ZIPCODE,ua.TEL,ua.MB,ua.ISDEFAULT from VIEW_USER_ADDRESS ua where ua.u_id = "
		// + uid;
		String hql = " from CtUser cu where cu.UId=" + uid;
		
		Query query1 = getSession().createQuery(hql);
		CtUser user = (CtUser) query1.list().get(0);
		String str = "";
		if(user!= null && user.getUIsCustomer().equals("1")){
			str += " and v.customer is not null and v.customer=2";
		}
		
		String sql = "from ViewUserAddress v where v.UId=" + uid + str +" order by v.isdefault, v.AId desc ";
		// SQLQuery query = getSession().createSQLQuery(hql);
		Query query = getSession().createQuery(sql);
		address = query.list();
		// for (ViewUserAddress viewUserAddress : address) {
		// address1.add(viewUserAddress);
		// }
		for (int i = 0; i < address.size(); i++) {
			if (address.get(i) != null) {
				address1.add(address.get(i));
			}
		}
		return address1;
	}

	@Override
	public String updateCart(CtCart cart) {
		// TODO Auto-generated method stub
		getSession().merge(cart);
		return "success";
	}

	@Override
	public CtCart getCartByUIdGId(Long uid, Long gid) {
		// TODO Auto-generated method stub
		CtCart cart = new CtCart();
		String hql = "from CtCart as cc where cc.UId = " + uid
				+ " and cc.cartId = " + gid;

		Query query = getSession().createQuery(hql);
		List<CtCart> cclist = query.list();
		cart = cclist.get(0);

		return cart;
	}

	@Override
	public List<ViewOrderCheck> getOrderCheck(Long uid, Long gid, String queryType) {
		// TODO Auto-generated method stub
		List<ViewOrderCheck> list = null;
		String hql = "from ViewOrderCheck as oc where oc.UId = " + uid
				+ " and oc."+queryType+" = " + gid;

		Query query = getSession().createQuery(hql);
		list = query.list();

		return list;
	}

	@Override
	public CtUserAddress getAddress(Long addid) {
		// TODO Auto-generated method stub
		CtUserAddress address = new CtUserAddress();
		String hql = "from CtUserAddress as ua where ua.AId = " + addid;

		Query query = getSession().createQuery(hql);
		List<CtUserAddress> list = query.list();
		address = list.get(0);

		return address;
	}

	@Override
	public String saveOrderInfo(CtOrderInfo odinfo) {
		// TODO Auto-generated method stub

		CtOrderInfo order = (CtOrderInfo)getSession().merge(odinfo);
		Long infoID = order.getOrderId();
		return infoID.toString();
	}

	@Override
	public String saveOrderGoods(CtOrderGoods odgoods) {
		// TODO Auto-generated method stub
		getSession().save(odgoods);
		return "success";
	}

	@Override
	public String delCartGoods(Long uid, Long gid) {
		// TODO Auto-generated method stub
		try {
			String sql = "DELETE FROM CT_CART cc WHERE CC.CART_ID = " + gid
					+ " AND CC.U_ID = " + uid;

			Query query = getSession().createSQLQuery(sql);
			query.executeUpdate();
		} catch (HibernateException e) {
			e.printStackTrace();
		}

		return "success";
	}

	@Override
	public String delCart(Long uid) {
		// TODO Auto-generated method stub
		String sql = "DELETE FROM CT_CART cc WHERE CC.U_ID = " + uid;

		Query query = getSession().createSQLQuery(sql);
		query.executeUpdate();

		return "success";
	}

	@Override
	public List<CtOrderInfo> getOrderListByUId(Long uid, Page page) {
		// TODO Auto-generated method stub
		List<CtOrderInfo> orderList = null;
		String hql = "from CtOrderInfo as oi where oi.UId = " + uid
				+ "order by oi.orderId desc";
		Query query = getSession().createQuery(hql);
		if (page.getCurrentPage() != 0) {
			page.setTotalCount(this.orderCount(uid));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		page.setTotalPage(page.getTotalCount() / page.getPageSize());
		orderList = query.list();
		return orderList;
	}

	public Long orderCount(Long uid) {
		String hql = "select count(*) from CtOrderInfo as ci where ci.UId = "
				+ uid;
		Long count = (Long) getSession().createQuery(hql).uniqueResult();
		return count;
	}
	public Long orderCount(Long uid, String start) {
		String hql = "select count(*) from CtOrderInfo as ci where ci.UId = "
				+ uid + " and ci.orderStatus = " + start;
		Long count = (Long) getSession().createQuery(hql).uniqueResult();
		return count;
	}
	public Long orderCount(Long uid, String time,String start) {
		String hql = "";
		if((time != null && start != null) && (!time.equals("") && !start.equals("") && !start.equals("all"))){
			hql = "select count(*) from CtOrderInfo as ci where ci.UId = "
					+ uid + " and ci.orderStatus = " + start + " and to_date(ci.orderTime, 'yyyy-mm-dd hh24:mi:ss') BETWEEN ADD_MONTHS(SYSDATE,-"
				+ time + ") AND SYSDATE ";
		} else if(((time != null && !time.equals("")) && (start == null || start.equals("") || start.equals("all")))) {
			hql = "select count(*) from CtOrderInfo as ci where ci.UId = "
					+ uid + " and to_date(ci.orderTime, 'yyyy-mm-dd hh24:mi:ss') BETWEEN ADD_MONTHS(SYSDATE,-"
				+ time + ") AND SYSDATE ";
		} else if(((time == null || time.equals("")) &&( start != null && !start.equals("") && !start.equals("all")))) {
			hql = "select count(*) from CtOrderInfo as ci where ci.UId = "
					+ uid + " and ci.orderStatus = " + start;
		} else {
			hql = "select count(*) from CtOrderInfo as ci where ci.UId = "
					+ uid;
		}
		Long count = (Long) getSession().createQuery(hql).uniqueResult();
		return count;
	}
	public Long orderCountReg(Long uid) {
		String hql = "select count(*) from CtOrderInfo as ci where ci.UId = "
				+ uid + "";
		Long count = (Long) getSession().createQuery(hql).uniqueResult();
		return count;
	}

	@Override
	public List<?> getOrderGoods(Long uid) {
		// TODO Auto-generated method stub
		List<?> goodsList = null;
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT");
		sql.append(" COG.ORDER_GOODS_ID,COG.ORDER_ID,COG.G_ID,COG.G_PRICE,COG.G_NUMBER,COG.PACK,");
		sql.append(" NVL(COG.COUPON_ID,0) AS COUPON_ID,COG.G_SUBTOTAL,NVL(CGI.P_URL,'nourl') AS P_URL,");
		sql.append(" CG.G_NAME");
		sql.append(" FROM");
		sql.append(" CT_ORDER_GOODS cog");
		sql.append(" LEFT JOIN");
		sql.append(" CT_ORDER_INFO coi");
		sql.append(" ON");
		sql.append(" COG.ORDER_ID = COI.ORDER_ID");
		sql.append(" LEFT JOIN");
		sql.append(" CT_GOODS cg");
		sql.append(" ON");
		sql.append(" CG.G_ID = COG.G_ID");
		sql.append(" LEFT JOIN");
		sql.append(" CT_GOODS_IMG CGI");
		sql.append(" ON");
		sql.append(" COG.G_ID = CGI.G_ID");
		sql.append(" AND");
		sql.append(" CGI.COVER_IMG = 1");
		sql.append(" WHERE");
		sql.append(" COI.U_ID = " + uid);
		sql.append(" ORDER BY");
		sql.append(" COG.ORDER_GOODS_ID");
		SQLQuery query = getSession().createSQLQuery(sql.toString());
		goodsList = query.list();
		return goodsList;
	}

	@Override
	public List<CtOrderInfo> getOrderListByTime(Long uid, Page page, String time, String status) {
		// TODO Auto-generated method stub
		return findOrderListByTimeOrStatus(uid, page, time, status);
	}
	
	public List<CtOrderInfo> findOrderListByTimeOrStatus(Long uid, Page page, String time, String status){
		List<CtOrderInfo> orderList = null;
		
		String hql = "from CtOrderInfo as oi where oi.UId = "
				+ uid ;
		
		String where = "";
		if(time != null && !time.equals("")){
			where += " and to_date(oi.orderTime, 'yyyy-mm-dd hh24:mi:ss') BETWEEN ADD_MONTHS(SYSDATE,-"
				+ time + ") AND SYSDATE ";
		}
		if(status != null && !status.equals("") && !status.equals("all")){
			where += " and oi.orderStatus = " + status
				+ " ";
		}
		where += " order by oi.orderId desc";
		hql += where;
		Query query = getSession().createQuery(hql);
		if (page.getCurrentPage() != 0) {
			page.setTotalCount(this.orderCount(uid,time,status));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		Long count = page.getTotalCount() / page.getPageSize();
		if(count.equals(0L)){
			count = 1L;
		}
		page.setTotalPage(count);
		orderList = query.list();
		return orderList;
		
	}
	

	@Override
	public List<CtOrderInfo> getOrderListByStatus(Long uid, Page page, String time, String status) {
		// TODO Auto-generated method stub
		return findOrderListByTimeOrStatus(uid, page, time, status);
	}

	@Override
	public void upChangeStatus(Long orderId, String status) {
		// TODO Auto-generated method stub
		String sql = "UPDATE CT_ORDER_INFO oi SET OI.ORDER_STATUS = " + status
				+ " WHERE OI.ORDER_ID = " + orderId;
		Query query = getSession().createSQLQuery(sql);
		query.executeUpdate();
	}

	@Override
	public List<CtBomGoods> getGoodsByBom(Integer bomid) {
		// TODO Auto-generated method stub
		List<CtBomGoods> goodsList = null;
		String hql = "from CtBomGoods as cbg where cbg.bomId = " + bomid;
		Query query = getSession().createQuery(hql);
		goodsList = query.list();
		return goodsList;
	}

	@Override
	public List<ViewOrderBom> getCheckByBom(Integer bomid) {
		// TODO Auto-generated method stub
		List<ViewOrderBom> orderbom = null;
		String hql = "from ViewOrderBom as vob where vob.bomId = " + bomid + " and vob.goodsNum <> -1";
		Query query = getSession().createQuery(hql);
		orderbom = query.list();
		return orderbom;
	}

	@Override
	public List<CtCart> getCartByUIdGIdPack(Long uid, String gid, String pack) {
		// TODO Auto-generated method stub
		List<CtCart> cartist = null;
		String hql = "from CtCart c where c.UId = " + uid + " and c.GId = "
				+ gid ;
		String where =  " and c.pack = " + pack;
		if(!pack.equals("notPack")){
			hql += where;
		}
		Query query = getSession().createQuery(hql);
		cartist = query.list();
		return cartist;
	}

	@Override
	public String updateCartNum(CtCart c) {
		String hql = null;
		//System.out.println(cart.getCartId() + "===========================================================");
		String where = "";
		CtCart cart = new CtCart();
		cart.setCartAddTime(c.getCartAddTime());
		cart.setCartId(c.getCartId());
		cart.setCartType(c.getCartType());
		cart.setCupon(c.getCupon());
		cart.setGId(c.getGId());
		cart.setGNumber(c.getGNumber());
		cart.setGParNumber(c.getGParNumber());
		cart.setGParPrice(c.getGParPrice());
		cart.setGPrice(c.getGPrice());
		cart.setPack(c.getPack());
		cart.setUId(c.getUId());
		if(cart.getCartType()!= null){
			if (cart.getGParPrice() != null) {
				where +=  " , c.GParPrice = "+cart.getGParPrice();
			}
		}
		if(cart.getCartType()!= null){
			if(cart.getGPrice() != null){
				where +=  " , c.GPrice = "+cart.getGPrice();
			}
		}
		if(cart.getGParNumber() == null){
			cart.setGParNumber(0L);
		} else {
			
		}
		if (cart != null) {
			if(cart.getGNumber() != null && (cart.getGParNumber() == null || cart.getGParNumber().equals("0"))){
				hql = "update CtCart c set c.GNumber = " + cart.getGNumber()
						+", c.GPrice="+cart.getGPrice()+ " where c.cartId=" + cart.getCartId();
			}
			if(cart.getGParNumber() != null && (cart.getGNumber() == null || cart.getGNumber() == 0)){
				hql = "update CtCart c set c.GParNumber = " + cart.getGParNumber()
						+", c.GParPrice="+cart.getGParPrice()+ " where c.cartId=" + cart.getCartId();
			}
			if(cart.getGParNumber() != null && cart.getGNumber() != null ){
				hql = "update CtCart c set c.GParNumber = " + cart.getGParNumber()
						+", c.GParPrice="+cart.getGParPrice() + ", c.GNumber = "+cart.getGNumber()+", c.GPrice = "+cart.getGPrice()+" where c.cartId=" + cart.getCartId();
			}
		}
		try {
			Query query = getSession().createQuery(hql);
			query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		getSession().flush();
		getSession().clear();
		return "success";
	}

	@Override
	public String updateCoupon(CtCouponDetail ccd) {
		String hql = null;
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String da = sdf.format(date);
		if (ccd != null && ccd.getCouponId() != null) {
			hql = " update CtCouponDetail ccd set ccd.useTime ='" + da
					+ "', ccd.orderId = " + ccd.getOrderId()
					+ ", ccd.isUse = 1 ,ccd.prePrice = " + ccd.getPrePrice()
					+ " where ccd.CDetailId = " + ccd.getCouponId()
					+ " and ccd.UId=" + ccd.getUId();
		}
		Query query = getSession().createQuery(hql);
		query.executeUpdate();
		return "success";
	}

	@Override
	public CtCart getCartByCartId(Long cartId) {
		// TODO Auto-generated method stub
		CtCart cart = new CtCart();
		String hql = "from CtCart as cc where cc.cartId = " + cartId;
		Query query = getSession().createQuery(hql);
		List<CtCart> cclist = query.list();
		if(cclist != null && cclist.size() > 0){
			return cclist.get(0);
		}
		return null;
	}

	public CtCart getCartByCartIdandpack(Long cartId, Long gid, String pack) {
		// TODO Auto-generated method stub
		CtCart cart = new CtCart();
		String hql = "from CtCart as cc where cc.cartId = " + cartId
				+ " and cc.GId = " + gid + " and cc.pack = '" + pack + "'";
		Query query = getSession().createQuery(hql);
		List<CtCart> cclist = query.list();
		cart = cclist.get(0);
		return cart;
	}

	@Override
	public List<CtGoods> getGoodsBycartId(String string) {
		String hql = "from CtCart cc where cc.cartId =  " + string;
		Query query = getSession().createQuery(hql);
		List<CtCart> carts = query.list();
		List<CtGoods> ctGoods = new ArrayList<CtGoods>();
		List<CtGoods> ctGoodss = new ArrayList<CtGoods>();

		for (int i = 0; i < carts.size(); i++) {
			String hqlGoods = "from CtGoods cg where cg.GId = "
					+ carts.get(i).getGId();
			Query queryGoods = getSession().createQuery(hqlGoods);
			ctGoods = queryGoods.list();
			for (int j = 0; j < ctGoods.size(); j++) {
				ctGoodss.add(ctGoods.get(j));
			}
		}
		return ctGoodss;
	}

	@Override
	public CtOrderInfo getOrderByOrderSn(String orderSn,Long uid) {
		if(orderSn.equals("goods")){
			String hql="from CtOrderInfo co where co.orderStatus=5 and co.evaId is not null";
			Query query = getSession().createQuery(hql);
			List<CtOrderInfo> list = query.list();
			if (list.size() > 0) {
				CtOrderInfo ct=new CtOrderInfo();
				ct.setTempListOrder(list);
				return ct;
			}
		}else{
		String hql = "from CtOrderInfo co where co.orderSn = '" + orderSn + "' and co.UId = " + uid;
		Query query = getSession().createQuery(hql);
		List<CtOrderInfo> list = query.list();
		if (list.size() > 0) {
			return list.get(0);
		}
		}
		return null;
	}

	@Override
	public void updateOrderStartBySn(String orderSn) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String dateFmt = sdf.format(date);
		String hql = " update from CtOrderInfo co set co.orderStatus = 5, co.isOkTime = '"
				+ dateFmt + "' where co.orderSn='" + orderSn + "'";
		Query query = getSession().createQuery(hql);
		query.executeUpdate();
	}

	@Override
	public CtOrderInfo getOrderByOrderId(Long infoID) {
		String hql = " from CtOrderInfo coi where coi.orderId = " + infoID;
		Query query = getSession().createQuery(hql);
		List<CtOrderInfo> list = query.list();
		if (list != null && list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	@Override
	public List<CtOrderInfo> getIsDis() {
		String hql = " from CtOrderInfo coi where coi.orderStatus = 4";
		Query query = getSession().createQuery(hql);
		return query.list();
	}

	@Override
	public int updateStartByOrderId(String orderId) {
		String hql = " update from CtOrderInfo coi set coi.orderStatus = 5 where coi.orderId = "
				+ orderId;
		Query query = getSession().createQuery(hql);
		return query.executeUpdate();
	}

	@Override
	public CtExpress getExpByExId(Byte exId) {
		String hql = " from CtExpress ce where ce.exId = " + exId;
		Query query = getSession().createQuery(hql);
		return (CtExpress) query.list().get(0);
	}

	@Override
	public List<CtOrderGoods> getOrderGoodsByOrderId(Long orderId) {
		String hql = " from CtOrderGoods cog where cog.orderId = " + orderId;
		Query query = getSession().createQuery(hql);
		return query.list();
	}

	@Override
	public CtEvaluation saveEvaluation(CtEvaluation ctEvaluation) {
		CtEvaluation ct = (CtEvaluation) getSession().merge(ctEvaluation);
		return ct;
	}

	@Override
	public void updateEvaIdByOrder(Long orderId, Long evaId) {
		String hql = " update from CtOrderInfo coi set coi.evaId = " + evaId
				+ " where coi.orderId = " + orderId;
		Query query = getSession().createQuery(hql);
		query.executeUpdate();
	}

	@Override
	public CtEvaluation getEvaByOrderId(Long orderId) {
		String hql = " from CtEvaluation eva where eva.orderId = " + orderId;
		Query query = getSession().createQuery(hql);
		if (query.list() != null && query.list().size() > 0) {
			return (CtEvaluation) query.list().get(0);
		}
		return null;
	}

	@Override
	public String updateRetOrderByOrderSn(CtOrderInfo orderInfo) {
		String hql = " update from CtOrderInfo coi set coi.retNumber="+orderInfo.getRetNumber()+",coi.retTime='"+orderInfo.getRetTime()+"',coi.retStatus='"+orderInfo.getRetStatus()+"' where coi.orderSn='"+orderInfo.getOrderSn()+"'";
		Query query = getSession().createQuery(hql);
		query.executeUpdate();
		return "success";
	}

	@Override
	public List<CtComplain> getOrderComplainByUid(Long uId,Page page) {
		String hql = " from CtComplain cl where cl.UId = " + uId +" order by cl.comStatus";
		Query query = getSession().createQuery(hql);
		if(page != null){
			if (page.getCurrentPage()!=0){
				page.setTotalCount(this.compCount(uId));
				query.setFirstResult(page.getFirstIndex());
				query.setMaxResults(page.getPageSize());
			}
			Long count = page.getTotalCount() / page.getPageSize();
			if(count.equals(0L)){
				count = 1L;
			}
			page.setTotalPage(count);
		}
		
		List<CtComplain> list = query.list();
		return list;
	}

	private Long compCount(Long uId) {
		String hql = "select count(*) from CtComplain cl where cl.UId = " + uId +" order by cl.comStatus";
		Long count = (Long) getSession().createQuery(hql).uniqueResult();
		return count;
	}

	@Override
	public String updateComplain(CtComplain complain) {
		getSession().merge(complain);
		return "succcess";
	}

	@Override
	public CtComplain getComplainByComId(Integer comId) {
		String hql = " from CtComplain cl where cl.comId=" + comId;
		Query query = getSession().createQuery(hql);
		return (CtComplain) query.list().get(0);
	}

	@Override
	public List<CtOrderInfo> getOrderReturnGoods(Long uId, Page page) {
		String hql = " from CtOrderInfo coi where coi.UId = " + uId +" and coi.retStatus is not null ";
		Query query = getSession().createQuery(hql);
		if (page.getCurrentPage() != 0) {
			page.setTotalCount(this.orderCountReg(uId));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		Long count = page.getTotalCount() / page.getPageSize();
		if(count.equals(0L)){
			count = 1L;
		}
		page.setTotalPage(count);
		return query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CtOrderInfo> getFindOrderBy(Long uid, Page page, OrderDTO orderDTO,Boolean isGoods) {
		
		try {
			String str = "";
			str += " and c.UId="+uid+" and c.orderTime >= '"+orderDTO.getStartTime()+" 00:00:00 ' and c.orderTime <= '"+orderDTO.getEndTime()+" 23:59:59' ";
//		
//			String hql = " from CtOrderInfo c,CtOrderGoods g  where c.orderId=g.orderInfo.orderId and 1=1 ";
			String hql = " from CtOrderInfo c where 1=1 ";
			if(orderDTO.getStatus() != null){
				str += " and c.orderStatus = " + orderDTO.getStatus();
			}
			if(!orderDTO.getFindStr().equals("")){
				if(isGoods){
//					str += " or g.ctGoods.GName like '%"+orderDTO.getFindStr()+"%' ";
				}
				str += "and c.orderSn like '%"+orderDTO.getFindStr()+"%' or c.consignee like '%"+orderDTO.getFindStr()+"%' ";
			}
			if(orderDTO.getPay() != null && orderDTO.getPay().equals("ok")){
				str += " and c.pay >= 5 and c.pay <= 8";
			}
			str += " order by c.orderId desc ";
			
			hql += str;
			Query query = getSession().createQuery(hql);
			if (page.getCurrentPage() != 0) {
				page.setTotalCount(this.orderCountBySerKey(str));
				query.setFirstResult(page.getFirstIndex());
				query.setMaxResults(page.getPageSize());
			}
			List<CtOrderInfo> list = query.list();
//			List<CtOrderInfo> list1 = new ArrayList<CtOrderInfo>();
//			for (int i = 0; i < list.size(); i++) {
//				Object[] o = list.get(i);
//				list1.add((CtOrderInfo)o[0]);
//			}
			if(list!= null && list.size() > 0){
				return list;
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		
//		String statusType = "";
//		String hql = "";
//		String hqlForOrderSn = "";
//		List<CtOrderGoods> list = new ArrayList<CtOrderGoods>();
//		List<CtOrderInfo> listTemp = new ArrayList<CtOrderInfo>();
//		String whereStr = "";
//		if(orderDTO.getEndTime().equals("")){
//			whereStr = " and coi.orderTime >= '"+orderDTO.getStartTime()+"' ";
//		} else {
//			if(orderDTO.getStartTime().equals(orderDTO.getEndTime())){
//				whereStr = "and coi.orderTime like '"+orderDTO.getStartTime()+"%' ";
//			} else {
//				whereStr = " and coi.orderTime >= '"+orderDTO.getStartTime()+" 00:00:00' and coi.orderTime <= '"+orderDTO.getEndTime()+" 23:59:59' ";
//			}
//		}
//		if(orderDTO.getStatus() != null){
//			whereStr += " and coi.orderStatus = " + orderDTO.getStatus();
//		}
//		if(!orderDTO.getFindStr().equals("")){
//			String st = orderDTO.getStartTime();
//			if(isGoods){
//				hql = " from CtOrderGoods cog where 1=1 and cog.orderInfo.UId="+uid+" and cog.GId like '%"+orderDTO.getFindStr()+"%' order by cog.orderId desc";
//				hqlForOrderSn = " from CtOrderInfo coi where coi.UId = "+uid+" and  coi.orderSn like '%"+orderDTO.getFindStr()+"%' "+whereStr+" order by coi.orderStatus desc";
//			} else {
//				String str = "";
//				if(orderDTO.getFindStr().length() >= 2){
//					str = orderDTO.getFindStr().substring(0, 2);
//				}
//				if(!str.equals("")&& str.toLowerCase().equals("ct")){
//					hqlForOrderSn = " from CtOrderInfo coi where 1=1 and coi.UId = "+uid+" and coi.orderSn like '%"+orderDTO.getFindStr()+"%' "+whereStr+" order by coi.orderStatus desc";
//				} else {
//					hql = " from CtOrderGoods cog where 1=1 and cog.ctGoods.GName like '%"+orderDTO.getFindStr()+"%' order by cog.orderId desc";
//					hqlForOrderSn = " from CtOrderInfo coi where coi.UId = "+uid+" and  1=1 and coi.consignee like '%"+orderDTO.getFindStr()+"%' "+whereStr+"  order by coi.orderStatus desc";
//				}
//			}
//			Query queryOrder = getSession().createQuery(hqlForOrderSn);
//			if (page.getCurrentPage() != 0) {
//				page.setTotalCount(this.orderCountReg(uid));
//				queryOrder.setFirstResult(page.getFirstIndex());
//				queryOrder.setMaxResults(page.getPageSize());
//			}
//			try {
//				if (!hql.equals("")) {
//					Query queryGoods = getSession().createQuery(hql);
//					list = queryGoods.list();
//				}
//			} catch (HibernateException e) {
//				e.printStackTrace();
//			}
//			if(list != null && list.size() >0){
//				for (int i = 0; i < list.size()-1; i++) {
//					for (int j = list.size()-1; j > i; j--) {
//						if(list.get(i).getOrderId().equals(list.get(j).getOrderId())){
//							//System.out.println(list.get(i).getOrderId());
//							list.remove(i);
//						}
//					}
//				}
//				for (int i = 0; i < list.size(); i++) {
//					hql = " from CtOrderInfo coi where coi.UId = "+uid+" and  coi.orderId = '" + list.get(i).getOrderId() + "' "+whereStr+"  order by coi.orderStatus desc";
//					Query queryGoods1 = getSession().createQuery(hql);
//					
//					if(queryGoods1.list() != null && queryGoods1.list().size() > 0){
//						listTemp.add((CtOrderInfo) queryGoods1.list().get(0));
//					}
//					
//				}
//			}
//			List<CtOrderInfo> listTemp1 = queryOrder.list();
//			for (int i = 0; i < listTemp1.size(); i++) {
//				listTemp.add(listTemp1.get(i));
//			}
//		} else {
//			String hqll = " from CtOrderInfo coi where coi.UId = "+uid + " "+whereStr+" order by  coi.orderStatus desc";
//			Query query2 = getSession().createQuery(hqll);
//			listTemp = query2.list();
//		}
//		if (page.getCurrentPage()!=0){
//			page.setTotalCount(Long.valueOf(listTemp.size()));
//		}
//		List<CtOrderInfo> newList = new ArrayList<CtOrderInfo>();
//		int index = page.getFirstIndex();
//		for (int i = 0; i < listTemp.size(); i++) {
//			if(i >= index && i < (index+12)){
//				newList.add(listTemp.get(i));
//			}
//		}
		return null;
	}

	private Long orderCountBySerKey(String str) {
		String hql = "select count(*) from CtOrderInfo c where 1=1 " + str;
		long count= (Long)getSession().createQuery(hql).uniqueResult();
		return count;
	}
	@Override
	public CtCart getCartByFCartId(Long cartId) {
		String hql = "from CtCart as cc where cc.cartId = " + cartId;
		Query query = getSession().createQuery(hql);
		List<CtCart> cclist = query.list();
		CtCart cart =null;
		if(cclist.size() >0){
			cart = cclist.get(0);
		}else {
			cart = null;
		}
		return cart;
	}

	@Override
	public List<CtShop> getAllShop() {
		String hql = " from CtShop cs order by cs.SId desc";
		Query query = getSession().createQuery(hql);
		return query.list();
	}

	@Override
	public String updateCartUtil(String uid) {
		String[] str = uid.split(",");
		String hql = " update from CtCart cc set cc.pack='"+str[0]+"' where cc.cartType = 0 and cc.UId = " + str[1];
		Query query = getSession().createQuery(hql);
		query.executeUpdate();
		return "success";
	}

	@Override
	public String getIsPanOrK(Long uid) {
		String hql = " from CtCart cc where cc.UId = " + uid + " and cc.cartType = 0";
		Query query = getSession().createQuery(hql);
		List<CtCart> list = query.list();
		String mess = null;
		for (int i = 0; i < list.size(); i++) {
			if(list.get(i).getPack().equals("K")){
				mess = "K";
			} else if (list.get(i).getPack().equals("Pan")){
				mess = "Pan";
			} else {
				mess = null;
			}
		}
		return mess;
	}

	@Override
	public List<CtOrderInfo> getSomeOrderListByUId(Long uid) {
		List<CtOrderInfo> orderList = null;
		String hql = "from CtOrderInfo as oi where oi.UId = " + uid
				+ " order by oi.orderTime desc";
		Query query = getSession().createQuery(hql);
		orderList = query.list();
		return orderList;
	}
	@Override
	public List<CtOrderInfo> getSomeOrderListByUId1(Long uid,Page page) {
		List<CtOrderInfo> orderList = null;
		String hql = "from CtOrderInfo as oi where oi.UId = " + uid
				+ " order by oi.orderTime desc";
		Query query = getSession().createQuery(hql);
		if (page.getCurrentPage() != 0) {
			page.setTotalCount(this.orderCount(uid));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		page.setTotalPage(page.getTotalCount() / page.getPageSize());
		orderList = query.list();
		return orderList;
	}

	@Override
	public Long dfCount() {
	Map usersession = ActionContext.getContext().getSession();
	CtUser cu = (CtUser) usersession.get("userinfosession");
	Long UId=0l;
	if (cu!=null){
		UId = cu.getUId();
	}
	
	Long count=0l;
	try{
		String str=" t where t.UId="+UId+" and t.orderStatus=0";
		 count = this.totalCount(str);
	}catch(Exception e){
		//System.out.println("......"+e.getMessage());
	}
	return count;
	}

	@Override
	public Long totalCount(String str) {
		String hql="select count(*) from CtOrderInfo";
		Long count=0L;
		try{
			if (str !=null)
			hql+=str;
		count= (Long)getSession().createQuery(hql).uniqueResult();
		
		}catch(Exception e){
			//System.out.println("..."+e.getMessage());
		}		
		return count;
	}

	@Override
	public Long dscount() {
		// TODO Auto-generated method stub
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		Long UId = cu.getUId();
		Long count=0l;
		try{
			String str=" t where t.UId="+UId+" and t.orderStatus=2";
			 count = this.totalCount(str);
		}catch(Exception e){
			//System.out.println("......"+e.getMessage());
		}
		return count;
	}

	@Override
	public Long dfhcount() {
		// TODO Auto-generated method stub
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		Long UId = cu.getUId();
		Long count=0l;
		try{
			String str=" t where t.UId="+UId+" and t.orderStatus=3";
			 count = this.totalCount(str);
		}catch(Exception e){
			//System.out.println("......"+e.getMessage());
		}
		return count;
	}

	@Override
	public Long dshcount() {
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		Long UId = cu.getUId();
		Long count=0l;
		try{
			String str=" t where t.UId="+UId+" and t.orderStatus=4";
			 count = this.totalCount(str);
		}catch(Exception e){
			//System.out.println("......"+e.getMessage());
		}
		return count;
	}

	@Override
	public Long dpjcount() {
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		Long UId = cu.getUId();
		Long count=0l;
		try{
			String str=" t where t.UId="+UId+" and t.orderStatus=5 and t.evaId is null";
			 count = this.totalCount(str);
		}catch(Exception e){
			//System.out.println("......"+e.getMessage());
		}
		return count;
	}

	@Override
	public CtEvaluation getEvaByEvaId(Long evaId) {
		String hql="from CtEvaluation ce where ce.evaId="+evaId;
		Query query = getSession().createQuery(hql);
		List<CtEvaluation> list=query.list();
		if(list!=null&&list.size()>0){
			return list.get(0);
		}
		return null;
	}	
	
	@Override
	public CtOrderInfo getOrderStatusByOrderSn(String orderSn) {

		String hql = "from CtOrderInfo co where co.orderSn = '" + orderSn + "'";
		Query query = getSession().createQuery(hql);
		List<CtOrderInfo> list = query.list();
		if (list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	@Override
	public List<CtEvaluation> getEvaAll() {
        String hql = " from CtEvaluation ce";
        Query query = getSession().createQuery(hql);
        List<CtEvaluation> list = query.list();
        return query.list();
	}

	@Override
	public CtOrderInfo getOrderInfoByOrderSn(String orderSn) {
		 String hql = " from CtOrderInfo co where co.orderSn = '" + orderSn + "'";
		 Query query = getSession().createQuery(hql);
		 List<CtOrderInfo> list = query.list();
		 if(list != null && list.size() > 0){
			 return list.get(0);
		 }
	     return null;
	}

	@Override
	public void tuPayOneOrderPayType(Long orderId) {
		String hql = "update from CtOrderInfo coi set coi.pay=1 where coi.orderId =" + orderId;
		Query query = getSession().createQuery(hql);
		query.executeUpdate();
	}

	@Override
	public void updateUserCredit(CtUser cu) {
//		getSession().update(cu);
		String hql = "update from CtUser cu set cu.UCredit = " + cu.getUCredit() + " where cu.UId = " + cu.getUId();
		Query query = getSession().createQuery(hql);
		query.executeUpdate();
	}

	@Override
	public CtCouponDetail getCcdByCcdId(String couponId) {
		String hql = " from CtCouponDetail ccd where ccd.CDetailId = " + couponId;
		Query query = getSession().createQuery(hql);
		List<CtCouponDetail> list = query.list();
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}

	@Override
	public void updateCouponRes(CtCouponDetail ccd) {
		getSession().update(ccd);
	}

	@Override
	public String getUserIdByUid(String uId) {
		
		String hql = " from CtUser cu where cu.UId = " + uId;
		Query query = getSession().createQuery(hql);
		List<CtUser> list = query.list();
		return list.get(0).getUUserid();
	}

	@Override
	public void updatePayByOrder(String out_trade_no, String type) {
		
		Query query;
		try {
			String hql = " update from CtOrderInfo co  set co.pay = " + type+ " where co.orderSn = '" + out_trade_no + "'";
			query = getSession().createQuery(hql);
			query.executeUpdate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public List<CtGoodsBrand> getGoodsBrandTopSix() {
		String hql = " from CtGoodsBrand cgb order by cgb.sortOrder";
		Query query = getSession().createQuery(hql);
		List<CtGoodsBrand> list = query.list();
		if(list != null && list.size() > 0){
			if(list.size() <= 24){
				return list;
			} else {
				List<CtGoodsBrand> list1 = new ArrayList<CtGoodsBrand>();
				for (int i = 0; i < 24; i++) {
					list1.add(list.get(i));
				}
				return list1;
			}
		}
		return list;
	}
	@Override
	public String getCateSimParNumByUserId(Long uId) {
		String hql = " from CtCart cc where cc.UId=" + uId;
		Query query = getSession().createQuery(hql);
		List<CtCart> list = query.list();
		int sim = 0;
		int par = 0;
		double price = 0d;
		if(list != null && list.size() > 0){
			for (int i = 0; i < list.size(); i++) {
				if(list.get(i).getCartType().equals("0")){
					par++;
					price += Double.valueOf(list.get(i).getGParPrice());
				} else {
					sim++;
					price += Double.valueOf(list.get(i).getGPrice());
				}
			}
			return String.valueOf(sim) + "===" + String.valueOf(par) + "===" + String.valueOf(new DecimalFormat("0.00##").format(price));
		}
		return null;
	}
	@Override
	public void saveTransaction(CtTransaction transaction) {
		getSession().merge(transaction);

	}
	@Override
	public List<CtOrderInfo> getorderToday() {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//			String dateNowMm = sdf.format(new Date());
			String hql = " from CtOrderInfo coi where coi.orderStatus<>0 and coi.orderStatus<>6 and coi.orderTime>='2016-09-01' order by coi.orderTime desc";
			Query query = getSession().createQuery(hql);
			query.setFirstResult(0); //开始记录
			query.setMaxResults(50);  //查询出来的记录数
			List<CtOrderInfo> list = query.list();
//			List<CtOrderInfo> list1 = new ArrayList<CtOrderInfo>();
//			for (int i = 0; i < list.size(); i++) {
//				String dateStr = list.get(i).getOrderTime();
//				String[] chai = dateStr.split(" ");
//				String[] dateChai = chai[1].split(":");
//				if(dateNowMm.equals(chai[0])){
//					list1.add(list.get(i));
//				}
//				if(chai[0].equals("2016-10-06")){
//					list1.add(list.get(i));
//				}
//			}
			return list;
		} catch (HibernateException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public CtTransaction getTranInfoByOrderSn(String orderSn) {
		String hql = " from CtTransaction ct where ct.orderSn = '" + orderSn + "' ";
		Query query = getSession().createQuery(hql);
		List<CtTransaction> list = query.list();
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}
	@Override
	public List<CtTransaction> findTranByTop30() {
		String hql = " from CtTransaction ct order by ct.tranId desc";
		Query query = getSession().createQuery(hql);
		query.setFirstResult(0);
		query.setMaxResults(30);
		List<CtTransaction> list = query.list();
		if(list != null && list.size() > 0){
			return list;
		}
		return null;
	}
	public static void main(String[] args) {
		 Date d=new Date();   
		   SimpleDateFormat df=new SimpleDateFormat("MM-dd");   
		   System.out.println("今天的日期："+df.format(d));   
		   System.out.println(d.getTime());
		   Long ti = (long) ((long)1000 * (long)60* (long)60* (long)24* (long)30);
		   System.out.println(ti);
//		   System.out.println(d.getTime()-1000 * 60 * 60 * 24 * 30);
		   System.out.println("三十前的日期：" + df.format(new Date(d.getTime() - ti)));  
		   System.out.println("三前的日期：" + df.format(new Date(d.getTime() - 3 * 24 * 60 * 60 * 1000)));  
//		   System.out.println("三天后的日期：" + df.format(new Date(d.getTime() + 3 * 24 * 60 * 60 * 1000)));
	}
	@Override
	public int findTramByCountList(int i) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Long ti = (long) ((long)1000 * (long)60* (long)60* (long)24* (long)i);
		Date d=new Date();  
		
		String hql = " from CtTransaction ct where ct.tranTime between '" + sdf.format(new Date(d.getTime() - ti)) +" 00:00:00' and '" + sdf.format(new Date()) + " 23:59:59' "  ;
		Query query = getSession().createQuery(hql);
		List<CtTransaction> list = query.list();
		if(list != null && list.size() > 0){
			return list.size();
		}
		return 0;
	}
	@Override
	public List<CtTransaction> findTramByIsFalseData() {
		String hql = " from CtTransaction ct where ct.tranIsZhenOrJia=0 order by ct.tranTime desc";
		Query query = getSession().createQuery(hql);
		query.setFirstResult(0);
		query.setMaxResults(30);
		return query.list();
	}
	@Override
	public void saveRefundAndUpdateOrderAndGoods(CtRefund refund,
			CtOrderInfo orderInfo) {
		try {
			getSession().save(refund);
			orderInfo.setIsRefInfo("1");
			getSession().merge(orderInfo);
			String hql = " from CtOrderGoods cog where cog.orderId=" + orderInfo.getOrderId() + " and cog.GId=" + refund.getRefGoodsid() + " ";
			if(refund.getRefGoodsType().equals("0")){
				hql += " and cog.isParOrSim=1";
			} else if (refund.getRefGoodsType().equals("1")){
				hql += " and cog.isParOrSim=0";
			}
			Query query = getSession().createQuery(hql);
			List<CtOrderGoods> list = (List<CtOrderGoods>) query.list();
			if(list != null && list.size() > 0){
				list.get(0).setIsRefund("1");
				getSession().merge(list.get(0));
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		}
	}
	@Override
	public void saveRefundCancelOrder(CtOrderInfo orderInfo) {
		String hql = " from CtRefund cr where cr.refOrderid=" + orderInfo.getOrderId();
		Query query = getSession().createQuery(hql);
		List<CtRefund> list = query.list();
		if(list != null && list.size() > 0){
			for (int i = 0; i < list.size(); i++) {
				getSession().delete(list.get(i));
			}
		}
		List<CtOrderGoods> orderGoodsList = getOrderGoodsByOrderId(orderInfo.getOrderId());
		CtRefund refund = new CtRefund();
		for (int i = 0; i < orderGoodsList.size(); i++) {
			refund = new CtRefund();
			refund.setRefGoodsid(orderGoodsList.get(i).getGId());
			refund.setRefGoodsType(orderGoodsList.get(i).getIsParOrSim().equals("0")?"1":"0");
			refund.setRefOrderid(orderGoodsList.get(i).getOrderId());
			refund.setRefPaytype(orderInfo.getPay());
			refund.setRefStatus("1");
			getSession().save(refund);
			orderGoodsList.get(i).setIsRefund("1");
			getSession().merge(orderGoodsList.get(i));
		}
		orderInfo.setIsRefInfo("1");
		getSession().merge(orderInfo);
	}
	@Override
	public void saveWxPay(CtPaymentWx wx) {
		getSession().merge(wx);
	}
	@Override
	public CtPaymentWx getWxPayByOrderSn(String string) {
		String hql = " from CtPaymentWx cpw where cpw.payOrdersn='" + string + "'";
		Query query = getSession().createQuery(hql);
		List<CtPaymentWx> list = query.list();
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}
	@Override
	public CtPaymentWx findPayWxByOpenId(String openid) {
		try {
			String hql = " from CtPaymentWx cpw where cpw.payOpenId='" + openid + "'";
			Query query = getSession().createQuery(hql);
			List<CtPaymentWx> list = query.list();
			if(list != null && list.size() > 0){
				return list.get(0);
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public CtShop getShopType4ById(Short sId) {
		String hql = " from CtShop cs where cs.SId = " + sId;
		Query query = getSession().createQuery(hql);
		List<CtShop> list = query.list();
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}
	@Override
	public List<CtOrderInfo> findOrderListByOrderStart(String string) {
		
		try {
			SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
			Date beginDate = new Date();
			Calendar date = Calendar.getInstance();
			date.setTime(beginDate);
			date.set(Calendar.DATE, date.get(Calendar.DATE) - 7);
			Date endDate = dft.parse(dft.format(date.getTime()));
			
			String hql = " from CtOrderInfo co where (co.orderStatus='"+string+"' and co.deliverTime <= '"+dft.format(endDate)+"')";
			Query query = getSession().createQuery(hql);
			List<CtOrderInfo> list = query.list();
			if(list != null && list.size() > 0){
				return list;
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public void updateOrderByOrderInfo(CtOrderInfo orderInfo) {
		String hql = "update CtOrderInfo co set co.orderStatus = "+orderInfo.getOrderStatus()+",co.isOkTime='"+orderInfo.getIsOkTime()+"' where co.orderId="+orderInfo.getOrderId();
		Query query = getSession().createQuery(hql);
		query.executeUpdate();
		//getSession().merge(orderInfo);
	}
	@Override
	public CtCart getCartByGidAndTypeId(String gid, int i, Long uid) {
		String hql = " from CtCart cc where cc.GId="+gid+" and cc.cartType="+i+" and cc.UId="+uid;
		Query query = getSession().createQuery(hql);
		List<CtCart> list = query.list();
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}
	@Override
	public void delCartGoodsByUidAndGid(Long uid, Long ctid) {
		String hql = " delete from CtCart cc where cc.GId="+ctid+" and cc.UId="+uid;
		getSession().createQuery(hql).executeUpdate();
	}
	@Override
	public String findPayTotalByOrderSn(String orderSn) {
		String hql = " from CtPayInterface cpi where cpi.orderSn='"+orderSn+"'";
		Query query = getSession().createQuery(hql);
		List<CtPayInterface> list = query.list();
		if(list != null && list.size() > 0){
			return list.get(0).getTotalFee();
		}
		return null;
	}
	@Override
	public String getOrderTotalByTime(String time, CtUser cu) {
		String hql = "select sum(t.total) from CtOrderInfo t where t.orderTime >='"+time+"' and (t.orderStatus=1 or t.orderStatus=3 or t.orderStatus=4 or t.orderStatus=5 or t.orderStatus=8) and t.UId="+cu.getUId()+"";
		Query query = getSession().createQuery(hql);
		String to = (String) query.uniqueResult();
		return to;
	}
	@Override
	public List<CtCashCoupon> findCashCouponByOrderSn(String orderSn) {
		String hql = " from CtCashCoupon ccc where ccc.orderSn='"+orderSn+"' order by ccc.discount desc";
		Query query = getSession().createQuery(hql);
		List<CtCashCoupon> list = query.list();
		if(list != null && list.size() > 0){
			return list;
		}
		return null;
	}
	@Override
	public CtCashCoupon getCashInfoByCashId(Integer cashId) {
		String hql = " from CtCashCoupon ccc where ccc.cashId="+cashId;
		Query query = getSession().createQuery(hql);
		List<CtCashCoupon> list = query.list();
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}
	@Override
	public void updateCashCouponById(CtCashCoupon cashCoupon) {
		getSession().merge(cashCoupon);
	}
	@Override
	public List<CtOrderInfo> getOrderListByUIdAndXingYong(Long uid, Page page,
			int i, int j) {
		// TODO Auto-generated method stub
		List<CtOrderInfo> orderList = null;
		String str = "";
		if(i != 0){
			str += " oi.pay="+i+" or";
		}
		if(j != 0){
			str += " oi.pay="+j+" or";
		}
		str = str.substring(0, str.length()-2);
		str += " or oi.pay=7 or oi.pay=8 ";
		String hql = "from CtOrderInfo as oi where oi.UId = " + uid
				+ " and ("+str+") order by oi.orderId desc";
		Query query = getSession().createQuery(hql);
		if (page.getCurrentPage() != 0) {
			page.setTotalCount(this.orderCountAndPay(uid, i, str));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		page.setTotalPage(page.getTotalCount() / page.getPageSize());
		orderList = query.list();
		return orderList;
	}
	private Long orderCountAndPay(Long uid, int i, String str) {
		String hql = "select count(*) from CtOrderInfo as oi where oi.UId = "
				+ uid + " and ("+str+") ";
		Long count = (Long) getSession().createQuery(hql).uniqueResult();
		return count;
	}
	@Override
	public CtUser getUserByIUid(Long uid) {
		String hql = " from CtUser cu where cu.UId = " + uid;
		Query query = getSession().createQuery(hql);
		List<CtUser> list = query.list();
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}
	@Override
	public void updateUser(CtUser cu) {
		getSession().merge(cu);
	}
	
}

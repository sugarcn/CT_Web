package org.ctonline.dao.order.impl;

import java.util.List;

import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.order.ICtPayDAO;
import org.ctonline.dto.order.PayDTO;
import org.ctonline.po.order.CtPay;
import org.hibernate.Query;


public class CtPayDAOImpl extends BaseHibernateDAOImpl implements ICtPayDAO {
	public List<CtPay> findAll(){
		String hql = " from CtPay";
		Query query = getSession().createQuery(hql);
		return query.list();
	}

	@Override
	public String saveOrderPay(CtPay payDTO) {
		CtPay p = (CtPay) getSession().merge(payDTO);
		return p.getPayId().toString();
	}

	@Override
	public CtPay getPayChong(Long orderId) {
		String hql = " from CtPay cp where cp.orderId="+orderId;
		Query query = getSession().createQuery(hql);
		List<CtPay> list = query.list();
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}
}
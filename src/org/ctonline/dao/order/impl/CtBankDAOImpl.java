package org.ctonline.dao.order.impl;

import java.util.List;

import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.order.ICtBankDAO;
import org.ctonline.po.order.CtBank;
import org.hibernate.Query;

public class CtBankDAOImpl extends BaseHibernateDAOImpl implements ICtBankDAO {
	public List<CtBank> findAll(){
		String hql = "from CtBank";
		Query query = getSession().createQuery(hql);
		return query.list();
	}
}
package org.ctonline.dao.order;

import java.util.List;

import org.ctonline.dto.order.PayDTO;
import org.ctonline.po.order.CtPay;

public interface ICtPayDAO {
	public abstract List findAll();

	public abstract String saveOrderPay(CtPay payDTO);

	public abstract CtPay getPayChong(Long orderId);
}
package org.ctonline.dao.order;

import java.util.List;

import org.ctonline.dao.BaseHibernateDAO;
import org.ctonline.dto.order.OrderDTO;
import org.ctonline.po.basic.CtCashCoupon;
import org.ctonline.po.basic.CtCouponDetail;
import org.ctonline.po.basic.CtPaymentWx;
import org.ctonline.po.basic.CtTransaction;
import org.ctonline.po.goods.CtBomGoods;
import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.goods.CtGoodsBrand;
import org.ctonline.po.order.CtCart;
import org.ctonline.po.order.CtComplain;
import org.ctonline.po.order.CtEvaluation;
import org.ctonline.po.order.CtExpress;
import org.ctonline.po.order.CtOrderGoods;
import org.ctonline.po.order.CtOrderInfo;
import org.ctonline.po.order.CtRefund;
import org.ctonline.po.order.CtShop;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.user.CtUserAddress;
import org.ctonline.po.views.ViewCartList;
import org.ctonline.po.views.ViewOrderBom;
import org.ctonline.po.views.ViewOrderCheck;
import org.ctonline.po.views.ViewUserAddress;
import org.ctonline.util.Page;

public interface OrderDAO extends BaseHibernateDAO {

	public List<ViewCartList> getCartByUId(Long UId);
	public String addCart(CtCart cart);
	public String updateCart(CtCart cart);
	public CtCart getCartByUIdGId(Long uid,Long gid);
	public List<CtGoods> getGoodsByGId(Long gid);
	public List<ViewUserAddress> getUserAddress(Long uid);
	public CtUserAddress getAddress(Long addid);
	public List<ViewOrderCheck> getOrderCheck(Long uid,Long gid, String queryType);
	public String saveOrderInfo(CtOrderInfo odinfo);
	public String saveOrderGoods(CtOrderGoods odgoods);
	public String delCartGoods(Long uid,Long gid);
	public String delCart(Long uid);
	public List<CtOrderInfo> getOrderListByUId(Long uid,Page page);
	//在个人中心显示部分商品订单
	public List<CtOrderInfo> getSomeOrderListByUId(Long uid);
	public List<CtOrderInfo> getSomeOrderListByUId1(Long uid,Page page);
	public List<CtOrderInfo> getOrderListByTime(Long uid,Page page,String time, String status);
	public List<CtOrderInfo> getOrderListByStatus(Long uid,Page page,String time,String status);
	public void upChangeStatus(Long orderId,String status);
	
	public List<?> getOrderGoods(Long uid);
	
	public List<CtBomGoods> getGoodsByBom(Integer bomid);
	public List<ViewOrderBom> getCheckByBom(Integer bomid);
	public List<CtCart> getCartByUIdGIdPack(Long uid, String gid, String pack);
	public String updateCartNum(CtCart cart);
	public String updateCoupon(CtCouponDetail ccd);
	
	public CtCart getCartByCartId(Long cartId);//根据CartID查出当前购物车值
	public CtCart getCartByFCartId(Long cartId);//根据CartID查出当前购物车值
	public CtCart getCartByCartIdandpack(Long cartId,Long bomId,String pack);
	public List<CtGoods> getGoodsBycartId(String string);
	public CtOrderInfo getOrderByOrderSn(String orderSn,Long uid);
	public void updateOrderStartBySn(String orderSn);
	public CtOrderInfo getOrderByOrderId(Long infoID);
	public List<CtOrderInfo> getIsDis();
	public int updateStartByOrderId(String orderId);
	public CtExpress getExpByExId(Byte exId);
	public List<CtOrderGoods> getOrderGoodsByOrderId(Long orderId);
	public CtEvaluation saveEvaluation(CtEvaluation ctEvaluation);
	public void updateEvaIdByOrder(Long orderId, Long evaId);
	public CtEvaluation getEvaByOrderId(Long orderId);
	public String updateRetOrderByOrderSn(CtOrderInfo orderInfo);
	public List<CtComplain> getOrderComplainByUid(Long uId,Page page);
	public String updateComplain(CtComplain complain);
	public CtComplain getComplainByComId(Integer comId);
	public List<CtOrderInfo> getOrderReturnGoods(Long uId,Page page);
	public List<CtOrderInfo> getFindOrderBy(Long uid, Page page, OrderDTO orderDTO,Boolean isGoods);
	public List<CtShop> getAllShop();
	public String updateCartUtil(String uid);
	public String getIsPanOrK(Long uid);
	public Long totalCount(String str);
	public Long dfCount();
	public Long dscount();
	public Long dfhcount();
	public Long dshcount();
	public Long dpjcount();
	public CtEvaluation getEvaByEvaId(Long evaId);
	public CtOrderInfo getOrderStatusByOrderSn(String orderSn);//根据订单编号查订单状态
	public List<CtEvaluation> getEvaAll();
	public CtOrderInfo getOrderInfoByOrderSn(String orderSn);
	public void tuPayOneOrderPayType(Long orderId);
	public void updateUserCredit(CtUser cu);
	public CtCouponDetail getCcdByCcdId(String couponId);
	public void updateCouponRes(CtCouponDetail ccd);
	public String getUserIdByUid(String uId);
	public void updatePayByOrder(String out_trade_no, String type);
	public List<CtGoodsBrand> getGoodsBrandTopSix();
	public String getCateSimParNumByUserId(Long uId);
	public void saveTransaction(CtTransaction transaction);
	public List<CtOrderInfo> getorderToday();
	public CtTransaction getTranInfoByOrderSn(String orderSn);
	public List<CtTransaction> findTranByTop30();
	public int findTramByCountList(int i);
	public List<CtTransaction> findTramByIsFalseData();
	public void saveRefundAndUpdateOrderAndGoods(CtRefund refund,
			CtOrderInfo orderInfo);
	public void saveRefundCancelOrder(CtOrderInfo orderInfo);
	public void saveWxPay(CtPaymentWx wx);
	public CtPaymentWx getWxPayByOrderSn(String string);
	public CtPaymentWx findPayWxByOpenId(String openid);
	public CtShop getShopType4ById(Short sId);
	public List<CtOrderInfo> findOrderListByOrderStart(String string);
	public void updateOrderByOrderInfo(CtOrderInfo orderInfo);
	public CtCart getCartByGidAndTypeId(String gid, int i, Long uid);
	public void delCartGoodsByUidAndGid(Long uid, Long ctid);
	public String findPayTotalByOrderSn(String orderSn);
	public String getOrderTotalByTime(String time, CtUser cu);
	public List<CtCashCoupon> findCashCouponByOrderSn(String orderSn);
	public CtCashCoupon getCashInfoByCashId(Integer cashId);
	public void updateCashCouponById(CtCashCoupon cashCoupon);
	public List<CtOrderInfo> getOrderListByUIdAndXingYong(Long uid, Page page,
			int i, int j);
	public CtUser getUserByIUid(Long uid);
	public void updateUser(CtUser cu);
}

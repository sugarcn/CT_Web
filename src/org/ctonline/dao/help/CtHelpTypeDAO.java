package org.ctonline.dao.help;

import java.util.List;

import org.ctonline.dao.BaseHibernateDAO;
import org.ctonline.po.basic.CtResourceType;
import org.ctonline.po.help.CtHelpType;
import org.ctonline.util.Page;

public interface CtHelpTypeDAO extends BaseHibernateDAO{
	public String save(CtHelpType adType);
	public CtHelpType findById(int id);
	public List<CtHelpType> loadAll(Page page);
	public int delete(int id);
	public Long totalCount(String str);
	public String update(CtHelpType adType);
	public List<CtHelpType> findAll(String keyword,Page page);
	public List<CtHelpType> queryType();
}
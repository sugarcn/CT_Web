package org.ctonline.dao.help.impl;

import java.util.List;

import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.help.CtHelpTypeDAO;
import org.ctonline.po.help.CtHelpType;
import org.ctonline.util.Page;
import org.hibernate.Query;

public class CtHelpTypeImpl extends BaseHibernateDAOImpl implements CtHelpTypeDAO{

	@Override
	public String save(CtHelpType helpType) {
		// TODO Auto-generated method stub
		Integer id;
		id =(Integer)getSession().save(helpType);
		return "success";
	}

	@Override
	public CtHelpType findById(int id) {
		// TODO Auto-generated method stub
		CtHelpType helpType = (CtHelpType)getSession().get(CtHelpType.class, id);
		return helpType;
	}

	@Override
	public List<CtHelpType> loadAll(Page page) {
		// TODO Auto-generated method stub
		List<CtHelpType> helpTypes = null;
		Query query = getSession().createQuery("from CtHelpType");
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		helpTypes = query.list();
		return helpTypes;
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		int d;
		String hql="delete from CtHelpType as ht where ht.id=?";
		Query query = getSession().createQuery(hql);
		query.setParameter(0, id);
		d = query.executeUpdate();
		return  d;
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtHelpType";
		if(str != null){
			hql += str;
		}
		Long count = (Long)getSession().createQuery(hql).uniqueResult();
		return count;
	}

	@Override
	public String update(CtHelpType helpType) {
		// TODO Auto-generated method stub
		getSession().update(helpType);
		return "success";
	}

	@Override
	public List<CtHelpType> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		List<CtHelpType> helpType = null;
		String sql=" from CtHelpType ";
		String str="";
		if (!keyword.equals("请输入关键字") && !keyword.equals("")){
			str+=" where H_TYPE_NAME like '%"+keyword+"%'";	
		}
		Query query= getSession().createQuery(sql+str);
		page.setTotalCount(this.totalCount(str));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		helpType=query.list();
		return helpType;
	}
	
	@Override
	public List<CtHelpType> queryType() {
		// TODO Auto-generated method stub
		List<CtHelpType> type = null;
		Query query = getSession().createQuery("from CtHelpType cht order by HTypeId asc");
		type = query.list();
		return type;
	}

}

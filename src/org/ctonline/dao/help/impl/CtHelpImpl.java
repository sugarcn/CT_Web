package org.ctonline.dao.help.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.help.CtHelpDAO;
import org.ctonline.po.basic.CtNotice;
import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.po.goods.CtGoodsImg;
import org.ctonline.po.help.CtHelp;
import org.ctonline.po.help.CtHelpType;
import org.ctonline.util.Page;
import org.hibernate.HibernateException;
import org.hibernate.Query;

public class CtHelpImpl extends BaseHibernateDAOImpl implements CtHelpDAO{

	@Override
	public CtHelp findById(int id) {
		// TODO Auto-generated method stub
		CtHelp help = (CtHelp) getSession().get(CtHelp.class, id);
		return help;
	}

	@Override
	public List<CtHelp> loadAll(Page page){
		// TODO Auto-generated method stub
		List<CtHelp> help = null;
		try {
			String hql = " from CtHelp order by HId";
			Query query = getSession().createQuery(hql);
//		if (page.getCurrentPage()!=0){
//			page.setTotalCount(this.totalCount(null));
//			query.setFirstResult(page.getFirstIndex());
//			query.setMaxResults(page.getPageSize());
//		}
			help = query.list();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return help;
	}

	@Override
	public String save(CtHelp help) {
		// TODO Auto-generated method stub
		String hql = "insert into Ct_Help(h_id,H_Type_Id,H_Title,H_Desc) values(seq_help.nextval,'"+help.getHTypeId()+"','"+help.getHTitle()+"','"+help.getHDesc()+"')";
		Query query = getSession().createSQLQuery(hql);
		int id = query.executeUpdate();
		return "success";
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtHelp ct ";
		if (str !=null)
			hql+=str;
		Long count= (Long)getSession().createQuery(hql).uniqueResult();
		return count;
	}

	@Override
	public String update(CtHelp help) {
		// TODO Auto-generated method stub
		getSession().update(help);
		return "success";
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		String hql = "delete from CtHelp ct where ct.HId="+id;
		Query query = getSession().createQuery(hql);
		int d = query.executeUpdate();
		return d;
	}

	@Override
	public CtHelp getHelpByHId(int Uid) {
		// TODO Auto-generated method stub
		String hql = "from CtHelp ct where ct.HId="+Uid;
		CtHelp cthelp = (CtHelp) getSession().createQuery(hql).uniqueResult();
		return cthelp;
	}

	@Override
	public List<CtHelp> loadOne(int id) {
		// TODO Auto-generated method stub
		List<CtHelp> ctHelps = null;
		String hql = "from CtHelp ct where ct.HId="+id;
		Query query = getSession().createQuery(hql);
		ctHelps =query.list();
		return ctHelps;
	}

	@Override
	public List<CtHelp> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		List<CtHelp> help = null;
		String sql="from CtHelp";
		String str="";
		if (!keyword.equals("请输入关键字") && !keyword.equals("")){
			str+=" where H_TITLE like '%"+keyword+"%' ";
		}
		Query query= getSession().createQuery(sql+str);
		page.setTotalCount(this.totalCount(str));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		help=query.list();
		return help;
	}

	@Override
	public List<CtHelpType> queryType() {
		List<CtHelpType> type = null;
		Query query = getSession().createQuery("from CtHelpType t order by t.HTypeId");
		type = query.list();
		return type;
	}

	@Override
	public CtHelp findByTitle(String title) {
		String hql = "from CtHelp ct where ct.HTitle='"+title+"'";
		CtHelp help = (CtHelp) getSession().createQuery(hql).uniqueResult();
		return help;
	}

	@SuppressWarnings({ "unused", "rawtypes" })
	@Override
	public List<CtNotice> findAllNotices(int typeId) {
		String hql = " from CtNotice cn where cn.noTypeId="+typeId+" order by cn.noTime desc";
		Query query = getSession().createQuery(hql);
		List<CtNotice> list = query.list();
		List<CtNotice> list1 = new ArrayList<CtNotice>();
		if(list.size() > 6){
			for (int i = 0; i < 6; i++) {
				list1.add(list.get(i));
			}
		} else {
			list1 = list;
		}
		return list1;
	}

	public static void main(String[] args){
		  String s = new String();
		  String s1 = new String();
		  try {
		   File f = new File("D:\\123.txt");
		   if(f.exists()){
		    System.out.print("文件存在");
		   }else{
		    System.out.print("文件不存在");
		    f.createNewFile();//不存在则创建
		   }
		   BufferedReader input = new BufferedReader(new FileReader(f));
		   
		   while((s = input.readLine())!=null){
		    s1 += s+"\n";
		   }
		   //System.out.println(s1);
		   input.close();
		   s1 += "添加的内容!";

		BufferedWriter output = new BufferedWriter(new FileWriter(f));
		   output.write(s1);
		   output.close();
		  } catch (Exception e) {
		   //e.printStackTrace();
		  }
		 }
		 
	@Override
	public List<CtGoods> getDianZuGoods() {
		String cids = getAllFenleiByName("电容");
		String[] cidChai = cids.split(",");
		List<CtGoods> listGoodsAll = new ArrayList<CtGoods>();
		for (int i = 0; i < cidChai.length; i++) {
			String hql = " from CtGoods cg where cg.CId = " + cidChai[i];
			Query query = getSession().createQuery(hql);
			query.setCacheable(true);
			List<CtGoods> listGoodsOne = query.list();
			for (int j = 0; j < listGoodsOne.size(); j++) {
				listGoodsAll.add(listGoodsOne.get(j));
			}
		}
		return listGoodsAll;
	}

	private String getAllFenleiByName(String findTyleName) {
		String hqlForCate = " from CtGoodsCategory cgc where cgc.parentId = 0";
		Query query = getSession().createQuery(hqlForCate);
		List<CtGoodsCategory> listForCate = query.list();
		Long cid = 0L;
		for (int i = 0; i < listForCate.size(); i++) {
			if(listForCate.get(i).getCName().equals(findTyleName)){
				//得到电阻的第一个编号
				cid = listForCate.get(i).getCId();
				break;
			}
		}
		hqlForCate = " from CtGoodsCategory cgc where cgc.parentId = "+ cid;
		query = getSession().createQuery(hqlForCate);
		listForCate = query.list();
		String cidErjiAll = "";
		for (int i = 0; i < listForCate.size(); i++) {
			cidErjiAll += listForCate.get(i).getCId().toString() + ",";
			String hqlForThree = " from CtGoodsCategory cgc where cgc.parentId = " + listForCate.get(i).getCId();
			Query queryThree = getSession().createQuery(hqlForThree);
			List<CtGoodsCategory> listForCateThree = queryThree.list();
			for (int j = 0; j < listForCateThree.size(); j++) {
				cidErjiAll += listForCateThree.get(j).getCId().toString() + ",";
			}
		}
		cidErjiAll += cid;
		return cidErjiAll;
	}

	@Override
	public List<CtGoods> getDianRongGoods() {
		String cids = getAllFenleiByName("电容");
		String[] cidChai = cids.split(",");
		List<CtGoods> listGoodsAll = new ArrayList<CtGoods>();
		for (int i = 0; i < cidChai.length; i++) {
			String hql = " from CtGoods cg where cg.CId = " + cidChai[i];
			Query query = getSession().createQuery(hql);
			List<CtGoods> listGoodsOne = query.list();
			for (int j = 0; j < listGoodsOne.size(); j++) {
				listGoodsAll.add(listGoodsOne.get(j));
			}
		}
		return listGoodsAll;
	}

	@Override
	public List<CtGoodsImg> getImg(Long gId) {
		List<CtGoodsImg> imgList = null;
		String hql = "from CtGoodsImg as img where img.GId="+gId + " order by img.coverImg desc";
		 
		Query query= getSession().createQuery(hql);
		imgList = query.list();
		 
		return imgList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Long,String> getErjiName() {
		Map<Long,String> cateNames = new HashMap<Long, String>();
		String hql = " from CtGoodsCategory cgc where cgc.parentId = 0 and rownum<3";
		Query query =  getSession().createQuery(hql);
		//获取一级分类集合
		List<CtGoodsCategory> yiJiList = query.list();
		for (int i = 0; i < yiJiList.size(); i++) {
			hql = " from  CtGoodsCategory cgc where cgc.parentId = " + yiJiList.get(i).getCId()+" and rownum<3";
			query =  getSession().createQuery(hql);
			List<CtGoodsCategory> erJiList = query.list();
			for (int j = 0; j < erJiList.size(); j++) {
				cateNames.put(erJiList.get(j).getCId() ,erJiList.get(j).getCName());
			}
		}
		return cateNames;
	}

	@Override
	public List<CtGoodsCategory> getCateListByCid(Long cid) {
		String hql = " from CtGoodsCategory cgc where cgc.parentId = " + cid;
		Query query = getSession().createQuery(hql);
		List<CtGoodsCategory> list = query.list();
		List<CtGoodsCategory> listNew = new ArrayList<CtGoodsCategory>();
		if(list != null && list.size() > 0){
			if(list.size() > 5){
				for (int i = 0; i < 2; i++) {
					listNew.add(list.get(i));
				}
			} else {
				listNew = list;
			}
			
			return listNew;
		}
		return null;
	}
}

package org.ctonline.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public interface BaseHibernateDAO {
	
	
	public SessionFactory getSessionFactory() ;

	public void setSessionFactory(SessionFactory sessionFactory) ;
	public Session begin();
	public Session getSession();
	public Session getCurrentSession();
	public void clossSession();

	public void commit();
	

}

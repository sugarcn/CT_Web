package org.ctonline.dao.basic;

import java.util.List;

import org.ctonline.dao.BaseHibernateDAO;
import org.ctonline.po.basic.CtUserRank;
import org.ctonline.util.Page;

public interface CtUserRankDAO extends BaseHibernateDAO{
	public Long save(CtUserRank userRank);//����
	public CtUserRank findById(Long id);//���id����
	public List<CtUserRank> loadAll(Page page);//��ҳ�ҳ�����
	public int delete(Long id);//ɾ��һ����¼
	public Long totalCount(String str);//ͳ������
	public void update(CtUserRank userRank);//����
	public List<CtUserRank> findAll(String keyword,Page page);//�����������
//	public Long getID();
	public String getMaxPoint();
	//获取所有积分
	public List<CtUserRank> getAll();
}

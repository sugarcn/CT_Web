package org.ctonline.dao.basic;

import org.ctonline.dao.BaseHibernateDAO;
import org.ctonline.po.basic.CtLoginLog;

/**
 * 日志类接口
 * @author ZWT
 *
 */
public interface CtLoginLogDAO extends BaseHibernateDAO {
	
	public void save(CtLoginLog ctLoginLog);//新增
	
}

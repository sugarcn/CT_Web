package org.ctonline.dao.basic;

import java.util.List;

import org.ctonline.dao.BaseHibernateDAO;
import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.po.goods.CtGroupPrice;
import org.ctonline.po.goods.CtRangePrice;
import org.ctonline.po.views.ViewCateNum;
import org.ctonline.po.views.ViewSubcateNum;

public interface BasicDAO  extends BaseHibernateDAO{
	public List<CtGoodsCategory> queryAllCate();
	public List<ViewCateNum> cateCount();
	public List<ViewSubcateNum> subcateCount();
	public String getPrice(Long uid,Long gid);
	public Long getCartGoodsNum(Long uid);
	public List<CtRangePrice> getAllRanPrice(Long gid);
	public CtGroupPrice getAllGroupPrice(Long gid);
}

package org.ctonline.dao.basic;

import java.util.List;

import org.ctonline.dao.BaseHibernateDAO;
import org.ctonline.po.basic.CtRegion;
import org.ctonline.util.Page;

public interface CtRegionDAO extends BaseHibernateDAO{
	public Long save(CtRegion region);//閿熸枻鎷烽敓鏂ゆ嫹
	public CtRegion findById(Long id);//閿熸枻鎷烽敓绲燿閿熸枻鎷烽敓鏂ゆ嫹
	public List<CtRegion> loadAll(Page page);//閿熸枻鎷烽〉閿熸彮绛规嫹閿熸枻鎷烽敓鏂ゆ嫹
	public int delete(Long id);//鍒犻敓鏂ゆ嫹涓�敓鏂ゆ嫹閿熸枻鎷峰綍
	public Long totalCount(String str);//缁熼敓鏂ゆ嫹閿熸枻鎷烽敓鏂ゆ嫹
	public void update(CtRegion region);//閿熸枻鎷烽敓鏂ゆ嫹
	public List<CtRegion> findAll(String keyword,Page page);//閿熸枻鎷烽敓鏂ゆ嫹閿熸枻鎷烽敓鏂ゆ嫹閿熸枻鎷烽敓锟�	public Long getID();
	public List<CtRegion> searchAll(Long id,Page page);
	public List<CtRegion> queryById(Long id);
	
}

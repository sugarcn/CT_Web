package org.ctonline.dao.basic;
import java.util.List;

import org.ctonline.dao.BaseHibernateDAO;
import org.ctonline.po.basic.CtCoupon;
import org.ctonline.po.basic.CtCouponDetail;
import org.ctonline.util.Page;

public interface CtCouponDetailDAO extends BaseHibernateDAO{
	public void save(CtCouponDetail coupondetail);//����
	public CtCouponDetail findById(Long id);//���id����
	public List<CtCouponDetail> loadAll(Page page,String type1,String type2);//查询所有全网优惠券
	public int delete(Long id);//ɾ��һ����¼
	public Long totalCount(String str);//ͳ������
	public String update(CtCouponDetail coupondetail);//����
	public List<CtCouponDetail> findAll(String keyword,Page page);//�����������

	public List<CtCouponDetail> findByCouponId(int id);
	public Long getID();//��ȡID
	public List<CtCouponDetail> queryAll();
	//获取限定商品
	public List<CtCouponDetail> getAdmitGoods(Page page,String type1,String type2);
	//获取样品
	public List<CtCouponDetail> getSamGoods(Page page,String type1,String type2);
	//根据优惠券码激活查询一个优惠券
	public CtCouponDetail getCtCouponDetailByCouponNum(String couponNum);
	//根据优惠券码激活优惠券
	public void activateByCouponNum(CtCouponDetail ccd);
	//获取全网优惠券商品数量
	public Long getCoutAll();
	//获取全网优惠券商品数量
	public Long getCoutOwn();
	//获取限定优惠券商品数量
	public Long getCoutAdmit();
	//所有能使用的优惠券
	public List<CtCouponDetail> getAllNotUse();
	//全网已使用优惠券
	public List<CtCouponDetail> getAllUse(Page page);
	//全网已过期优惠券
	public List<CtCouponDetail> getAllOverdue(Page page);
	//获取某个商品的优惠券列表
	public List<CtCouponDetail> getCtCouponsByGid(Long uid,Long GId);
	//判断某个类型的优惠券是否被领取
	public List<CtCouponDetail> isGetCoupom(Integer couponId);
	//随机分配一个优惠券
	public List<CtCouponDetail> getCouponDetails(Integer couponId);
	
	
	//随机对某优惠券类型，给用户分配N个优惠券
	public void udpateCouponDetails(Integer couponId,Long uid,Integer n);
	
	//获取样品券数量
	public Long getCoutSam();
	
	//判断24小时内是否有使用过样品券
		public boolean isDUse(Long uid);
		public CtCoupon findByCouponById(Integer couponId);
	
	//所有能使用的优惠券
//	public List<CtCouponDetail> getAllCanUseCoupon();
}

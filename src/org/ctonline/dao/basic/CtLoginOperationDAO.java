package org.ctonline.dao.basic;

import org.ctonline.dao.BaseHibernateDAO;
import org.ctonline.po.basic.CtLoginOperation;

public abstract interface CtLoginOperationDAO
  extends BaseHibernateDAO
{
  public abstract void insertLog(CtLoginOperation paramCtLoginOperation);

public abstract CtLoginOperation findDescEs(String opContent);
}

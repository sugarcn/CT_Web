package org.ctonline.dao.basic.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.basic.CtCouponDetailDAO;
import org.ctonline.po.basic.CtCoupon;
import org.ctonline.po.basic.CtCouponDetail;
import org.ctonline.po.basic.CtNotice;
import org.ctonline.po.user.CtUser;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.opensymphony.xwork2.ActionContext;

public class CtCouponDetailImpl extends BaseHibernateDAOImpl implements CtCouponDetailDAO {

	@Override	
	public void save(CtCouponDetail coupondetail) {
		// TODO Auto-generated method stub
			String guid="select  CREATEGUID from dual ";
			List l;
		//Long id;
		
		try{

	        SQLQuery query = getSession().createSQLQuery(guid);
	        guid = (String) query.uniqueResult();
		//l= session.createQuery(guid).list();//..uniqueResult();
		//guid=(String)l.get(0);
		coupondetail.setCouponNumber(guid);
		
		}catch(Exception e){
			//System.out.println("..."+e.getMessage());
		}		
		try{

			//id =(Long)session.save(coupon);
			getSession().save(coupondetail);
			
			
			
		}catch (Exception e) {
            //System.out.println("......"+e.getMessage());
		}
	}
	
	
	@Override
	public CtCouponDetail findById(Long id) {
		// TODO Auto-generated method stub
		
		CtCouponDetail coupon=null;
		try{
			coupon = (CtCouponDetail)getSession().createQuery(" from CtCouponDetail ccd where ccd.CDetailId="+id).list().get(0);
		    
		}catch(Exception e){
			//System.out.println("....."+e.getMessage());
		}
		return coupon;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CtCouponDetail> loadAll(Page page,String type1,String type2) {
		// TODO Auto-generated method stub
		List< CtCouponDetail> resource=null;
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		Long UId = cu.getUId();
		try{
			Date date = new Date();
			SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String time = siFormat.format(date);
			if("1".equals(type1) && "1".equals(type2)){
				Query query= getSession().createQuery("from CtCouponDetail ccd where ccd.UId="+UId +" and ccd.status='1' and  ccd.ctCoupon.couponType<>9 and ccd.isUse='0' and ccd.ctCoupon.etime>='"+time+"'");
				if (page.getCurrentPage()!=0){
					page.setTotalCount(this.totalCount(null));
					query.setFirstResult(page.getFirstIndex());
					query.setMaxResults(page.getPageSize());
				}
				resource=query.list();
			}else if ("1".equals(type1) && "2".equals(type2)) {
				Query query= getSession().createQuery("from CtCouponDetail ccd where ccd.UId="+UId +" and ccd.status='1' and ccd.isUse='1'  and  ccd.ctCoupon.couponType<>9");
				if (page.getCurrentPage()!=0){
					page.setTotalCount(this.totalCount2(null));
					query.setFirstResult(page.getFirstIndex());
					query.setMaxResults(page.getPageSize());
				}
				resource=query.list();
				
			}else if ("1".equals(type1) && "3".equals(type2)) {
				Query query= getSession().createQuery("from CtCouponDetail ccd where ccd.UId="+UId +" and ccd.ctCoupon.etime<'"+time+"' and  ccd.ctCoupon.couponType<>9");
				if (page.getCurrentPage()!=0){
					query.setFirstResult(page.getFirstIndex());
					query.setMaxResults(page.getPageSize());
				}
				resource=query.list();
				page.setTotalCount(Long.valueOf(resource.size()));
			}
		}catch(Exception e){
			//System.out.println("......"+e.getMessage());
		}
		Long totle = page.getTotalCount() / page.getPageSize();
		if(totle.equals(0L)){
			totle = 1L;
		}
		page.setTotalPage(totle);
		return resource;
	}

	
	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		int d=-1;
		String hql="delete CtCouponDetail as u where u.id=?";
		try{
			
			Query query= getSession().createQuery(hql);
			query.setParameter(0, id);
			d= query.executeUpdate();
			
			
		}catch(Exception e){
			//System.out.println("....."+e.getMessage());
		}		
		return d;
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		String hql="select count(*) from CtCouponDetail ccd where ccd.UId = " + cu.getUId() + " and ccd.isUse='0'";
		Long count=0L;
		try{
			if (str !=null)
			hql+=str;
		count= (Long)getSession().createQuery(hql).uniqueResult();
		
		}catch(Exception e){
			//System.out.println("..."+e.getMessage());
		}		
		return count;
	}
	
	public Long totalCount2(String str) {
		// TODO Auto-generated method stub
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		String hql="select count(*) from CtCouponDetail ccd where ccd.UId = " + cu.getUId() + " and ccd.isUse='1' and ccd.ctCoupon.couponType <> 9";
		Long count=0L;
		try{
			if (str !=null)
				hql+=str;
			count= (Long)getSession().createQuery(hql).uniqueResult();
			
		}catch(Exception e){
			//System.out.println("..."+e.getMessage());
		}		
		return count;
	}
	
	public Long totalCountsim(String str) {
		// TODO Auto-generated method stub
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		String hql="select count(*) from CtCouponDetail ccd where ccd.UId = " + cu.getUId() + "";
		Long count=0L;
		try{
			if (str !=null)
				hql+=str;
			count= (Long)getSession().createQuery(hql).uniqueResult();
			
		}catch(Exception e){
			//System.out.println("..."+e.getMessage());
		}		
		return count;
	}
	
	public Long totalCountAll(String str) {
		// TODO Auto-generated method stub
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		String hql="select count(*) from CtCouponDetail ccd where ccd.UId = " + cu.getUId() + " and ccd.ctCoupon.couponType != 9";
		Long count=0L;
		try{
			if (str !=null)
				hql+=str;
			count= (Long)getSession().createQuery(hql).uniqueResult();
			
		}catch(Exception e){
			//System.out.println("..."+e.getMessage());
		}		
		return count;
	}

	@Override
	public String update(CtCouponDetail coupondetail) {
		// TODO Auto-generated method stub
		try{
			
		getSession().update(coupondetail);
		
		
		}catch(Exception e){
			//System.out.println("..."+e.getMessage());
		}
		return "success";		
		
	}

	@Override
	public List<CtCouponDetail> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		List< CtCouponDetail > resource=null;
		String sql="from CtCouponDetail";
		String str="";
		try{
			if (!keyword.equals("....") && !keyword.equals("")){
				str+=" where COUPON_NAME like '%"+keyword+"%' ";
			}
			//str+=" order by regionID desc";
			sql+=str;
			
			Query query= getSession().createQuery(sql);
			page.setTotalCount(this.totalCount(str));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
			resource=query.list();
			
		}catch(Exception e){
			//System.out.println("....."+e.getMessage());
		}			
		return resource;
	}
	public List<CtCouponDetail> findByCouponId(int id) {
		// TODO Auto-generated method stub
		List< CtCouponDetail > list=null;
		
		
		try{
			
			
			Query query= getSession().createQuery("from CtCouponDetail  where COUPON_ID=?");
			query.setParameter(0, id);
			
			list=query.list();
			
		}catch(Exception e){
			//System.out.println("....."+e.getMessage());
		}			
		return list;
	}

	@Override
	public Long getID() {
		// TODO Auto-generated method stub
		Long id=-1L;
		String sql = "select SEQ_REGION .nextval as id from dual";
		try{
			
			id= (Long)getSession().createQuery(sql).uniqueResult();
			
		}catch(Exception e){
			//System.out.println("......"+e.getMessage());
		}			
		return id;
	}

	@Override
	public List<CtCouponDetail> queryAll() {
		// TODO Auto-generated method stub
		List< CtCouponDetail> resource=null;
		try{
			
			Query query= getSession().createQuery("from CtCouponDetail");
			resource=query.list();
			
		}catch(Exception e){
			//System.out.println(e.getMessage());
		}			
		return resource;
	}

	@Override
	public List<CtCouponDetail> getAdmitGoods(Page page,String type1,String type2) {
		// TODO Auto-generated method stub
		List< CtCouponDetail> resource=null;
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		Long UId = cu.getUId();
		try{
			Date date = new Date();
			SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String time = siFormat.format(date);
			if("2".equals(type1) && "1".equals(type2)){
				Query query= getSession().createQuery("from CtCouponDetail ccd where ccd.UId="+UId +" and ccd.ctCoupon.GId<>0 and ccd.status=1 and ccd.isUse=0 and ccd.ctCoupon.etime>='"+time+"'");
				if (page.getCurrentPage()!=0){
					query.setFirstResult(page.getFirstIndex());
					query.setMaxResults(page.getPageSize());
				}
				resource=query.list();
				page.setTotalCount(Long.valueOf(resource.size()));
				
			}else if ("2".equals(type1) && "2".equals(type2)) {
				Query query= getSession().createQuery("from CtCouponDetail ccd where ccd.UId="+UId +" and ccd.ctCoupon.GId<>0 and ccd.status=1 and ccd.isUse=1");
				if (page.getCurrentPage()!=0){
					query.setFirstResult(page.getFirstIndex());
					query.setMaxResults(page.getPageSize());
				}
				resource=query.list();
				page.setTotalCount(Long.valueOf(resource.size()));
				
			}else if ("2".equals(type1) && "3".equals(type2)) {
				Query query= getSession().createQuery("from CtCouponDetail ccd where ccd.UId="+UId +" and ccd.ctCoupon.GId<>0 and ccd.status=1 and ccd.isUse=0 and ccd.ctCoupon.etime<'"+time+"'");
				if (page.getCurrentPage()!=0){
					query.setFirstResult(page.getFirstIndex());
					query.setMaxResults(page.getPageSize());
				}
				resource=query.list();
				page.setTotalCount(Long.valueOf(resource.size()));
			}
			
		}catch(Exception e){
			//System.out.println("......"+e.getMessage());
		}
		Long totle = page.getTotalCount() / page.getPageSize();
		if(totle.equals(0L)){
			totle = 1L;
		}
		page.setTotalPage(totle);
		return resource;
	}
	
	
	
	@Override
	public List<CtCouponDetail> getSamGoods(Page page,String type1,String type2) {
		// TODO Auto-generated method stub
		List< CtCouponDetail> resource=null;
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		Long UId = cu.getUId();
		try{
			Date date = new Date();
			SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String time = siFormat.format(date);
			if("3".equals(type1) && "1".equals(type2)){
				Query query= getSession().createQuery("from CtCouponDetail ccd where ccd.UId="+UId +" and ccd.ctCoupon.couponType=9 and ccd.status=1 and ccd.isUse=0 and ccd.ctCoupon.etime>='"+time+"'");
				if (page.getCurrentPage()!=0){
					query.setFirstResult(page.getFirstIndex());
					query.setMaxResults(page.getPageSize());
				}
				resource=query.list();
				page.setTotalCount(Long.valueOf(resource.size()));
				
			}else if ("3".equals(type1) && "2".equals(type2)) {
				Query query= getSession().createQuery("from CtCouponDetail ccd where ccd.UId="+UId +" and ccd.ctCoupon.couponType=9 and ccd.status=1 and ccd.isUse=1");
				if (page.getCurrentPage()!=0){
					query.setFirstResult(page.getFirstIndex());
					query.setMaxResults(page.getPageSize());
				}
				resource=query.list();
				page.setTotalCount(Long.valueOf(resource.size()));
				
			}else if ("3".equals(type1) && "3".equals(type2)) {
				Query query= getSession().createQuery("from CtCouponDetail ccd where ccd.UId="+UId +" and ccd.ctCoupon.couponType=9 and ccd.status=1 and ccd.isUse=0 and ccd.ctCoupon.etime<'"+time+"'");
				if (page.getCurrentPage()!=0){
					query.setFirstResult(page.getFirstIndex());
					query.setMaxResults(page.getPageSize());
				}
				resource=query.list();
				page.setTotalCount(Long.valueOf(resource.size()));
			}
			
		}catch(Exception e){
			//System.out.println("......"+e.getMessage());
		}
		Long totle = page.getTotalCount() / page.getPageSize();
		if(totle.equals(0L)){
			totle = 1L;
		}
		page.setTotalPage(totle);
		return resource;
	}
	

	@Override
	public void activateByCouponNum(CtCouponDetail ccd) {
		// TODO Auto-generated method stub
		
		getSession().update(ccd);
		
		
	}

	@Override
	public CtCouponDetail getCtCouponDetailByCouponNum(String couponNum) {
		// TODO Auto-generated method stub
		
		String hql = "from CtCouponDetail ccd where ccd.couponNumber='"+couponNum+"'";
		Query query = getSession().createQuery(hql);
		CtCouponDetail ccd = (CtCouponDetail) query.uniqueResult();
		return ccd;
	}

	@Override
	public Long getCoutAll() {
		// TODO Auto-generated method stub
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		Long UId = cu.getUId();
		Long count=0l;
		try{
			String str = "from CtCouponDetail ccd where ccd.UId="+UId + " and ccd.isUse=0";
			 count = this.totalCount(null);
			
		}catch(Exception e){
			//System.out.println("......"+e.getMessage());
		}
		return count;
	}
	
	@Override
	public Long getCoutOwn() {//全品
		// TODO Auto-generated method stub
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		Long UId = cu.getUId();
		Long count=0l;
		try{
			String str = " and  ccd.ctCoupon.GId=0 and ccd.status=1" ;
			 count = this.totalCountAll(str);
			
		}catch(Exception e){
			//System.out.println("......"+e.getMessage());
		}
		return count;
	}

	@Override
	public Long getCoutAdmit() {
		// TODO Auto-generated method stub
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		Long UId = cu.getUId();
		Long count=0l;
		try{
			//String str = "from CtCouponDetail ccd where ccd.UId="+UId +" and ccd.ctCoupon.GId<>0 and ccd.status=1";
			String str = " and ccd.ctCoupon.GId<>0 and ccd.status=1";
			count = this.totalCount(str);
			
		}catch(Exception e){
			//System.out.println("......"+e.getMessage());
		}
		return count;
	}
	
	@Override
	public Long getCoutSam() {
		// TODO Auto-generated method stub
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		Long UId = cu.getUId();
		Long count=0l;
		try{
			//String str = "from CtCouponDetail ccd where ccd.UId="+UId +" and ccd.couponType=9";
			String str= " and ccd.ctCoupon.couponType=9 and ccd.status=1";
			count = this.totalCountsim(str);
			
		}catch(Exception e){
			//System.out.println("......"+e.getMessage());
		}
		return count;
	}
	

	@Override
	public List<CtCouponDetail> getAllNotUse() {
		// TODO Auto-generated method stub
		List< CtCouponDetail> resource=null;
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		Long UId = cu.getUId();
		try{
			Date date = new Date();
			SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String time = siFormat.format(date);
			String hql = "from CtCouponDetail ccd where ccd.UId="+UId +" and ccd.status=1  and ccd.isUse=0 and ccd.ctCoupon.etime>='"+time+"'";
			Query query= getSession().createQuery(hql);
			resource=query.list();
			
		}catch(Exception e){
			//System.out.println("......"+e.getMessage());
		}
		return resource;
	}

	@Override
	public List<CtCouponDetail> getAllUse(Page page) {
		// TODO Auto-generated method stub
		List< CtCouponDetail> resource=null;
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		Long UId = cu.getUId();
		try{
			String hql = "from CtCouponDetail ccd where ccd.UId="+UId +" and ccd.ctCoupon.GId=0 and ccd.status=1  and ccd.isUse=1";
			Query query= getSession().createQuery(hql);
			
			if (page.getCurrentPage()!=0){
				page.setTotalCount(this.totalCount(hql));
				query.setFirstResult(page.getFirstIndex());
				query.setMaxResults(page.getPageSize());
			}
			resource=query.list();
			
		}catch(Exception e){
			//System.out.println("......"+e.getMessage());
		}
		return resource;
	}

	@Override
	public List<CtCouponDetail> getAllOverdue(Page page) {
		// TODO Auto-generated method stub
		List< CtCouponDetail> resource=null;
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		Long UId = cu.getUId();
		try{
			
			String hql = "from CtCouponDetail ccd where ccd.UId="+UId +" and ccd.ctCoupon.GId=0 and ccd.status=1  and ccd.ctCoupon.etime > ";
			Query query= getSession().createQuery(hql);
			
			if (page.getCurrentPage()!=0){
				page.setTotalCount(this.totalCount(hql));
				query.setFirstResult(page.getFirstIndex());
				query.setMaxResults(page.getPageSize());
			}
			resource=query.list();
			
		}catch(Exception e){
			//System.out.println("......"+e.getMessage());
		}
		return resource;
	}

	@Override
	public List<CtCouponDetail> getCtCouponsByGid(Long uid,Long GId) {
		// TODO Auto-generated method stub
		List<CtCouponDetail> couponDetails;
		//String sql = "select COUPON_ID,COUPON_NAME,COUPON_TYPE,AMOUNT,DISCOUNT from CT_COUPON where G_ID=0 or G_ID="+GId;
		//String sql = "select G_NAME,COUPON_ID,COUPON_NAME,COUPON_TYPE,AMOUNT,DISCOUNT from CT_COUPON where G_ID="+GId+" or G_ID=0  and ETIME>='"+time+"'";
		String sql = " from CtCouponDetail ccd where 1=1 ";
		if(uid != null){
			sql += " and ccd.UId="+uid;
		}
		
		sql += " and (ccd.isUse = '0' or ccd.isUse is null) and (ccd.status = '1') and ccd.orderId is null and '"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' between ccd.ctCoupon.stime and ccd.ctCoupon.etime";
		Query query = getSession().createQuery(sql);
		couponDetails = query.list();
		
		List<CtCouponDetail> couponDetailst = new ArrayList();
		
		Integer a=0;
		for(int i=0;i<couponDetails.size();i++){
			CtCouponDetail cd = new CtCouponDetail();
			cd = couponDetails.get(i);
			if (a==cd.getCouponId()){
	
			}else{
				couponDetailst.add(cd);
				a = cd.getCouponId();
			}

			 
		}
		
		return couponDetailst;
	}
	
//	public List<CtCouponDetail> getCtCouponsByGid(Long GId) {
//		// TODO Auto-generated method stub
//		List l2 = new ArrayList();
//		List<?> l;
//		Session session = this.getSessionFactory().openSession();
//		//String hql = " select ccd.ctCoupon.*,ccd.UId from CtCouponDetail ccd where ccd.GId=0 or ccd.GId="+GId;
//		//String sql = "select DISTINCT cc.*,ccd.U_ID from CT_COUPON cc ,CT_COUPON_DETAIL ccd where cc.COUPON_ID=ccd.COUPON_ID and cc.G_ID=0 or cc.G_ID="+GId;
//		//String sql = "select COUPON_ID,COUPON_NAME,COUPON_TYPE,AMOUNT,DISCOUNT from CT_COUPON where G_ID=0 or G_ID="+GId;
//		String sql = "select cc.COUPON_ID,cc.COUPON_NAME,cc.COUPON_TYPE,cc.AMOUNT,cc.DISCOUNT from CT_COUPON cc,CT_COUPON_DETAIL ccd where cc.COUPON_ID=ccd.COUPON_ID and cc.G_ID=0 or cc.G_ID="+GId;
//		Query query = session.createSQLQuery(sql);
//		//String hql = "select DISTINCT ccd.ctCoupon.couponId,ccd.ctCoupon.couponName,ccd.ctCoupon.couponType,ccd.ctCoupon.amount,ccd.ctCoupon.discount,ccd.ctCoupon.discount,ccd.ctCoupon.GId,ccd.ctCoupon.limitNum,ccd.UId from CtCouponDetail ccd where ccd.ctCoupon.GId=0 or ccd.ctCoupon.GId="+GId;
//		//String hql = " from CtCouponDetail";
//		//Query query = session.createQuery(hql);
//		l = query.list();
//		for(int i=0;i<l.size();i++){
//			Object [] obj = (Object[]) l.get(i);
//			BigDecimal couponId = (BigDecimal) obj[0];
//			Integer couponId2 = couponId.intValue();
//			String sql2 = "select C_DETAIL_ID,COUPON_ID,U_ID from CT_COUPON_DETAIL where COUPON_ID="+couponId2;
//			Query query2 = session.createSQLQuery(sql2);
//			List<?> couponDetails = query2.list();
//			for(int j=0;j<couponDetails.size();j++){
//				Object [] obj2 = (Object[]) couponDetails.get(i);
//				String[] arr = new String[11];
//				if(obj2[2] == null){
//					obj2[1] =  0;
//				}
//				arr[0] = couponId2.toString();
//				//BigDecimal couponName = (BigDecimal) obj[1];
//				//BigDecimal couponType = (BigDecimal) obj[2];
//				BigDecimal amount = (BigDecimal) obj[3];
//				BigDecimal discount = (BigDecimal) obj[4];
//				arr[1] = (String) obj[1];
//				arr[2] = (String) obj[2];
//				arr[3] = amount.toString();
//				arr[4] = discount.toString();
//				BigDecimal uid = (BigDecimal) obj2[1];
//				arr[5] = uid.toString();
//				l2.add(arr);
//			}
//		}
//		
//		return l2;
//	}


	@Override
	public List<CtCouponDetail> isGetCoupom(Integer couponId) {
		// TODO Auto-generated method stub
		Map usersession = ActionContext.getContext().getSession();
		CtUser cu = (CtUser) usersession.get("userinfosession");
		Long UId = cu.getUId();
		List<CtCouponDetail> coupons;
		String hql = "from CtCouponDetail ccd where ccd.couponId="+couponId +" and ccd.UId ="+UId;
		Query query = getSession().createQuery(hql);
		coupons = query.list();
		
		return coupons;
	}

	@Override
	public List<CtCouponDetail> getCouponDetails(Integer couponId) {
		// TODO Auto-generated method stub
		List<CtCouponDetail> coupons;
		String hql = "from CtCouponDetail ccd where ccd.couponId="+couponId +" and ccd.UId is null order by CDetailId";
		Query query = getSession().createQuery(hql);
		coupons = query.list();
		
		return coupons;
	}
	
	
	@Override
	public void udpateCouponDetails(Integer couponId,Long uid,Integer n){
		List<CtCouponDetail> coupons;
		
		String hql = "update CtCouponDetail set UId="+uid+", status='1' where CDetailId in (select CDetailId from CtCouponDetail t where couponId="+couponId+" and UId is null and rownum<="+n+")";
		Query query= getSession().createQuery(hql);
		query.executeUpdate();
		

		
	}
	
	
	
	@Override
	public boolean isDUse(Long uid){
		List<CtCouponDetail> coupons;
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String hql = "select count(*) from CtCouponDetail where UId="+uid+" and ctCoupon.couponType=9 and useTime='"+sdf.format(new Date())+"'";
		Query query = getSession().createQuery(hql);
		Long count;
		count= (Long)getSession().createQuery(hql).uniqueResult();

		if (count>=1)
			return false;
		else
			return true;

		
	}


	@Override
	public CtCoupon findByCouponById(Integer couponId) {
		String hql = " from CtCoupon ct where ct.couponId="+couponId;
		Query query = getSession().createQuery(hql);
		List<CtCoupon> list = query.list();
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}

//	@Override
//	public List<CtCouponDetail> getAllCanUseCoupon() {
//		// TODO Auto-generated method stub
//		return null;
//	}
}

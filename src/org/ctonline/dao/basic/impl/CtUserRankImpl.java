package org.ctonline.dao.basic.impl;

import java.util.List;

import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.basic.CtRegionDAO;
import org.ctonline.dao.basic.CtUserRankDAO;
import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.basic.CtUserRank;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;

public class CtUserRankImpl extends BaseHibernateDAOImpl implements CtUserRankDAO {

	@Override
	public List<CtUserRank> loadAll(Page page) {
		// TODO Auto-generated method stub
		List<CtUserRank> userRanks = null;
		
		//getSession() getSession() =getSession()Factory.opengetSession()().getCurrentgetSession()();
		Query query = getSession().createQuery("from CtUserRank");
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		userRanks = query.list();
		
		//
		
		return userRanks;
	}
	
	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtUserRank";
		if (str !=null)
			hql+=str;
		Session session  =this.getSession();
		Long count= (Long)getSession().createQuery(hql).uniqueResult();
		
		return count;
	}

	@Override
	public Long save(CtUserRank userRank) {
		// TODO Auto-generated method stub
		Long id;
		
		id =(Long)getSession().save(userRank);
		
		
		return id;
	}

	@Override
	public String getMaxPoint() {
		// TODO Auto-generated method stub
		String hql = "select max(MAX_POINTS) from CtUserRank";
		Session session  =this.getSession();
		String maxPoint = (String)getSession().createQuery(hql).uniqueResult();
		
		return maxPoint;
	}

	@Override
	public CtUserRank findById(Long id) {
		// TODO Auto-generated method stub
		
		CtUserRank userRank = (CtUserRank) getSession().get(CtUserRank.class, id);
		
		return userRank;
	}

	@Override
	public void update(CtUserRank userRank) {
		// TODO Auto-generated method stub
		
		getSession().update(userRank);
		
		
		
	}

	//鏍规嵁id鍒犻櫎绛夌骇璁板綍
	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		int d;
		String hql="delete CtUserRank as u where u.id=?";
		
		Query query= getSession().createQuery(hql);
		query.setParameter(0, id);
		d= query.executeUpdate();
		
		
		return d;
	}

	@Override
	public List<CtUserRank> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		List<CtUserRank> userRank = null;
		String sql="from CtUserRank";
		String str="";
		if (!keyword.equals("璇疯緭鍏ヤ綘瑕佹煡璇㈢殑鍏抽敭璇�") && !keyword.equals("")){
			str+=" where R_NAME like '%"+keyword+"%' ";
			str+="or MIN_POINTS like '%"+keyword+"%' ";
			str+="or MAX_POINTS like '%"+keyword+"%' ";
		}
		sql+=str;
		
		Query query= getSession().createQuery(sql);
		page.setTotalCount(this.totalCount(str));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		userRank=query.list();
		
		return userRank;
	}

	@Override
	public List<CtUserRank> getAll() {
		// TODO Auto-generated method stub
		List<CtUserRank> userRanks = null;
		
		Query query = getSession().createQuery("from CtUserRank cur order by cur.maxPoints");
		userRanks = query.list();
		
		return userRanks;
	}

}

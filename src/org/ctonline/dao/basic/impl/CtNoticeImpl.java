package org.ctonline.dao.basic.impl;

import java.util.List;

import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.basic.CtNoticeDAO;
import org.ctonline.po.basic.CtNotice;
import org.ctonline.util.Page;
import org.hibernate.Query;

public class CtNoticeImpl extends BaseHibernateDAOImpl implements CtNoticeDAO{
	
	@Override
	public List<CtNotice> loadAll(Page page, Integer typeId) {
		// TODO Auto-generated method stub
		List<CtNotice> notice = null;
		Query query = getSession().createQuery("from CtNotice t where t.noTypeId="+typeId+" order by noTime desc");
		if(page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(" where t.noTypeId="+typeId+" "));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		notice = query.list();
		return notice;
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtNotice t";
		if(str != null){
			hql += str;
		}
		Long count = (Long)getSession().createQuery(hql).uniqueResult();
		return count;
	}

	@Override
	public CtNotice findById(int id) {
		// TODO Auto-generated method stub
		//CtAdType adType = (CtAdType)getSession().get(getClass(), id);
		CtNotice notice = (CtNotice)getSession().get(CtNotice.class, id);
		return notice;
	}
	
	@Override
	public List<CtNotice> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		List<CtNotice> notice = null;
		String sql="from CtNotice  order by noTime desc";
		String str="";
		if (!keyword.equals("请输入关键字") && !keyword.equals("")){
			str+=" where NO_TITLE like '%"+keyword+"%' ";
			
		}
		Query query= getSession().createQuery(sql+str);
		page.setTotalCount(this.totalCount(str));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		notice=query.list();
		return notice;
	}

}

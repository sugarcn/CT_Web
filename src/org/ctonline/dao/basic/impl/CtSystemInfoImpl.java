package org.ctonline.dao.basic.impl;

import java.util.List;

import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.basic.CtRegionDAO;
import org.ctonline.dao.basic.CtSystemInfoDAO;
import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.basic.CtSystemInfo;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;

public class CtSystemInfoImpl extends BaseHibernateDAOImpl implements CtSystemInfoDAO {


	@Override
	public CtSystemInfo getInfo() {
		// TODO Auto-generated method stub
		
		CtSystemInfo userRank = (CtSystemInfo) getSession().get(CtSystemInfo.class, 1);
		
		return userRank;
	}

	@Override
	public void update(CtSystemInfo userRank) {
		// TODO Auto-generated method stub
		
		getSession().update(userRank);
		
		
		
	}




}

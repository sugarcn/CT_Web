package org.ctonline.dao.basic.impl;

import java.util.List;

import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.basic.CtRegionDAO;
import org.ctonline.po.basic.CtRegion;
import org.ctonline.util.Page;
import org.hibernate.Query;

public class CtRegionImpl extends BaseHibernateDAOImpl implements CtRegionDAO {

	@Override
	public Long save(CtRegion region) {
		// TODO Auto-generated method stub
		Long id;
		
		id =(Long)getSession().save(region);
		
		
		return id;
	}

	@Override
	public CtRegion findById(Long id) {
		// TODO Auto-generated method stub
		
		CtRegion region = (CtRegion)getSession().get(CtRegion.class, id);
		
		return region;//get or load
	}

	@Override
	public List<CtRegion> loadAll(Page page) {
		// TODO Auto-generated method stub
		List< CtRegion> region=null;
		
		Query query= getSession().createQuery("from CtRegion");
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		region=query.list();
		
		return region;
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		int d;
		String hql="delete CtRegion as u where u.id=?";
		
		Query query= getSession().createQuery(hql);
		query.setParameter(0, id);
		d= query.executeUpdate();
		
		
		return d;
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtRegion";
		if (str !=null)
			hql+=str;
		Long count= (Long)getSession().createQuery(hql).uniqueResult();
		
		return count;
	}

	@Override
	public void update(CtRegion region) {
		// TODO Auto-generated method stub
		
		getSession().update(region);
		
		
	}

	@Override
	public List<CtRegion> findAll(String keyword,Page page) {
		// TODO Auto-generated method stub
		List< CtRegion> region=null;
		String sql="from CtRegion";
		String str="";
		if (!keyword.equals("������ؼ���") && !keyword.equals("")){
			str+=" where regionName like '%"+keyword+"%' ";
		}
		//str+=" order by regionID desc";
		sql+=str;
		
		Query query= getSession().createQuery(sql);
		page.setTotalCount(this.totalCount(str));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		region=query.list();
		
		return region;
	}
	
	
	@Override
	public List<CtRegion> searchAll(Long id,Page page){
		List< CtRegion> region=null;
		String sql="from CtRegion  where parentId="+id+" order by regionName ";
	
	Query query= getSession().createQuery(sql);
	if (page.getCurrentPage()!=0){
		page.setTotalCount(this.totalCount(" where parentId="+id));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
	}
	region=query.list();
	
	return region;
	}
	
	@Override
	public List<CtRegion> queryById(Long id){
		List< CtRegion> region=null;
		String sql="from CtRegion  where parentId="+id;
	
	Query query= getSession().createQuery(sql);
	region=query.list();
	
	return region;
		
	}

}

package org.ctonline.dao.basic.impl;

import java.util.List;

import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.basic.CtLoginOperationDAO;
import org.ctonline.po.basic.CtLoginOperation;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtLoginOperationImpl
  extends BaseHibernateDAOImpl
  implements CtLoginOperationDAO
{
  public void insertLog(CtLoginOperation ctLoginOperation)
  {
	  Session session=this.getSessionFactory().openSession();
	  getSession().merge(ctLoginOperation);
	  session.beginTransaction().commit();
		colseSession(session);
  }

	private void colseSession(Session session) {
		session.clear();
		session.flush();
		session.close();
	}
@Override
public CtLoginOperation findDescEs(String opContent) {
	String hql = " from CtLoginOperation co where co.logDesc='"+opContent+"' ";
	Query query = getSession().createQuery(hql);
	List<CtLoginOperation> list = query.list();
	if(list != null && list.size() > 0){
		return list.get(0);
	}
	return null;
}
}

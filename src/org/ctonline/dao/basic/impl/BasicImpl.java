package org.ctonline.dao.basic.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.basic.BasicDAO;
import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.po.goods.CtGoodsGroupRelation;
import org.ctonline.po.goods.CtGroupPrice;
import org.ctonline.po.goods.CtPrice;
import org.ctonline.po.goods.CtRangePrice;
import org.ctonline.po.user.CtUserGroupRelation;
import org.ctonline.po.views.ViewCateNum;
import org.ctonline.po.views.ViewSubcateNum;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class BasicImpl extends BaseHibernateDAOImpl implements BasicDAO{


	@Override
	public List<CtGoodsCategory> queryAllCate() {
		// TODO Auto-generated method stub
		List<CtGoodsCategory> cateList = null;
		Query query= getSession().createQuery("from CtGoodsCategory as cate  where cate.parentId=0 order by cate.parentId,cate.sortOrder");
		cateList = query.list();
		return cateList;
	}

	@Override
	public List<ViewCateNum> cateCount() {
		// TODO Auto-generated method stub
		List<ViewCateNum> catecount = null;
		Query query= getSession().createQuery("from ViewCateNum");
		catecount = query.list();
		return catecount;
	}

	@Override
	public List<ViewSubcateNum> subcateCount() {
		// TODO Auto-generated method stub
		List<ViewSubcateNum> subcatecount = null;
		Query query= getSession().createQuery("from ViewSubcateNum");
		subcatecount = query.list();
		return subcatecount;
	}

	@Override
	public String getPrice(Long uid, Long gid) {
		// TODO Auto-generated method stub
		String price = null;
		List<CtPrice> priceList = null;
		if(uid!=null){
			String uid_gid = "from CtPrice as cp where cp.UId="+uid+" and cp.UType=0 and cp.GType=0 and cp.GId="+gid;
			Query query = getSession().createQuery(uid_gid);
			priceList = query.list();
			if(priceList.size()>0){
				price = priceList.get(0).getPrice();
				return price;
			}else{
				List<CtGoodsGroupRelation> ggrList = null;
				String getGGID = "from CtGoodsGroupRelation as ggr where ggr.GId="+gid;
				Query Fggid = getSession().createQuery(getGGID);
				ggrList = Fggid.list();
				if(ggrList.size()>0){
					Long ggid = ggrList.get(0).getGGId();
					String uid_ggid = "from CtPrice as cp where cp.UId="+uid+" and cp.UType=0 and cp.GType=1 and cp.GId="+ggid;
					Query Fuid_ggid = getSession().createQuery(uid_ggid);
					priceList = Fuid_ggid.list();
					if(priceList.size()>0){
						price = priceList.get(0).getPrice();
						return price;
					}else{
						List<CtUserGroupRelation> ugrList = null;
						String getUGID = "from CtUserGroupRelation as ugr where ugr.UId="+uid;
						Query Fugid = getSession().createQuery(getUGID);
						ugrList = Fugid.list();
						if(ugrList.size()>0){
							Long ugid = ugrList.get(0).getUGId();
							String ugid_gid = "from CtPrice as cp where cp.UId="+ugid+" and cp.UType=1 and cp.GType=0 and cp.GId="+gid;
							Query Fugid_gid = getSession().createQuery(ugid_gid);
							priceList = Fugid_gid.list();
							if(priceList.size()>0){
								price = priceList.get(0).getPrice();
								return price;
							}else{
								String ugid_ggid = "from CtPrice as cp where cp.UId="+ugid+" and cp.UType=1 and cp.GType=1 and cp.GId="+ggid;
								Query Fugid_ggid = getSession().createQuery(ugid_ggid);
								priceList = Fugid_ggid.list();
								if(priceList.size()>0){
									price = priceList.get(0).getPrice();
									return price;
								}else{
									String null_ggid = "from CtPrice as cp where cp.UId is null and cp.GType=1 and cp.GId="+ggid;
									Query Fnull_ggid = getSession().createQuery(null_ggid);
									priceList = Fnull_ggid.list();
									if(priceList.size()>0){
										price = priceList.get(0).getPrice();
										return price;
									}else{
										return null;
									}
								}
							}
						}else{
							String null_ggid = "from CtPrice as cp where cp.UId is null and cp.GType=1 and cp.GId="+ggid;
							Query Fnull_ggid = getSession().createQuery(null_ggid);
							priceList = Fnull_ggid.list();
							if(priceList.size()>0){
								price = priceList.get(0).getPrice();
								return price;
							}else{
								return null;
							}
						}
					}
				}else{
					List<CtUserGroupRelation> ugrList = null;
					String getUGID = "from CtUserGroupRelation as ugr where ugr.UId="+uid;
					Query Fugid = getSession().createQuery(getUGID);
					ugrList = Fugid.list();
					if(ugrList.size()>0){
						Long ugid = ugrList.get(0).getUGId();
						String ugid_gid = "from CtPrice as cp where cp.UId="+ugid+" and cp.UType=1 and cp.GType=0 and cp.GId="+gid;
						Query Fugid_gid = getSession().createQuery(ugid_gid);
						priceList = Fugid_gid.list();
						if(priceList.size()>0){
							price = priceList.get(0).getPrice();
							return price;
						}else{
							return null;
						}
					}else{
						return null;
					}
				}
			}
		}else{
			String null_gid = "from CtPrice as cp where cp.UId is null and cp.GType=0 and cp.GId="+gid;
			Query Fnull_gid = getSession().createQuery(null_gid);
			priceList = Fnull_gid.list();
			if(priceList.size()>0){
				price = priceList.get(0).getPrice();
				return price;
			}else{
				List<CtGoodsGroupRelation> ggrList = null;
				String getGGID = "from CtGoodsGroupRelation as ggr where ggr.GId="+gid;
				Query Fggid = getSession().createQuery(getGGID);
				ggrList = Fggid.list();
				if(ggrList.size()>0){
					Long ggid = ggrList.get(0).getGGId();
					String null_ggid = "from CtPrice as cp where cp.UId is null and cp.GType=1 and cp.GId="+ggid;
					Query Fnull_ggid = getSession().createQuery(null_ggid);
					priceList = Fnull_ggid.list();
					if(priceList.size()>0){
						price = priceList.get(0).getPrice();
						return price;
					}else{
						return null;
					}
				}
			}
		}
		 
		return null;
	}

	@Override
	public Long getCartGoodsNum(Long uid) {
		// TODO Auto-generated method stub
		long count = 0;
		try {
			String hql="select distinct(cc.GId) from CtCart as cc where cc.UId=" + uid;
			count = (long) getSession().createQuery(hql).list().size();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return count;
	}

	@Override
	public List<CtRangePrice> getAllRanPrice(Long gid) {
		String hql = " from CtRangePrice crp where crp.GId = " + gid + " order by crp.simSNum ";
		Query query = getSession().createQuery(hql);
		List<CtRangePrice> list = query.list();
		List<CtRangePrice> listNew = new ArrayList<CtRangePrice>();
		if(list != null && list.size() > 5){
			for (int i = 0; i < 5; i++) {
				listNew.add(list.get(i));
			}
		} else {
					Collections.sort(list,new Comparator<CtRangePrice>(){
						@Override
						public int compare(CtRangePrice o1, CtRangePrice o2) {
							return o1.getSimSNum().compareTo(o2.getSimSNum());
						}
					});
			return list;
		}
		Collections.sort(listNew,new Comparator<CtRangePrice>(){
			@Override
			public int compare(CtRangePrice o1, CtRangePrice o2) {
				return o1.getSimSNum().compareTo(o2.getSimSNum());
			}
		});
		return listNew;
	}

	@Override
	public CtGroupPrice getAllGroupPrice(Long gid) {
		String hql = " from CtGroupPrice crp where crp.GGId = " + gid;
		Query query = getSession().createQuery(hql);
		return (CtGroupPrice) query.list().get(0);
	}

		
	
}

package org.ctonline.dao.basic.impl;

import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.basic.ICtFreeSampleRegDAO;
import org.ctonline.po.basic.CtFreeSampleReg;

public class CtFreeSampleRegDAOImpl extends BaseHibernateDAOImpl implements ICtFreeSampleRegDAO {

	/* (non-Javadoc)
	 * @see org.ctonline.dao.basic.impl.ICtFreeSampleRegDAO#insertSamInfo(org.ctonline.po.basic.CtFreeSampleReg)
	 */
	@Override
	public void saveSamInfo(CtFreeSampleReg reg){
		getSession().save(reg);
	}
}

package org.ctonline.dao.basic.impl;

import java.util.List;

import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.basic.CtRegionDAO;
import org.ctonline.dao.basic.CtTempDAO;
import org.ctonline.dao.basic.CtUserRankDAO;
import org.ctonline.po.basic.CtTemp;
import org.ctonline.po.goods.CtGoodsCategory;

import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;

public class CtTempImpl extends BaseHibernateDAOImpl implements CtTempDAO {


	@Override
	public CtTemp findBySn(String sn) {
		// TODO Auto-generated method stub
		
		//CtTemp userRank = (CtTemp) getSession().get(CtTemp.class, id);
		String hql = " from CtTemp cc where cc.t_sn="+sn;
		Query query= getSession().createQuery(hql);
		CtTemp cte = (CtTemp) query.list().get(0);
		
		return cte;
	}

}

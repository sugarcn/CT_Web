package org.ctonline.dao.basic.impl;

import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.basic.CtLoginLogDAO;
import org.ctonline.po.basic.CtLoginLog;

/**
 * 日志类Impl
 * @author ZWT
 *
 */
public class CtLoginLogImpl extends BaseHibernateDAOImpl implements CtLoginLogDAO{
	
	@Override
	public void save(CtLoginLog ctLoginLog) {
		getSession().save(ctLoginLog);
	}
	
}
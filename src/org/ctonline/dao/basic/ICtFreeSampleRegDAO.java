package org.ctonline.dao.basic;

import org.ctonline.po.basic.CtFreeSampleReg;

public interface ICtFreeSampleRegDAO {

	public abstract void saveSamInfo(CtFreeSampleReg reg);

}
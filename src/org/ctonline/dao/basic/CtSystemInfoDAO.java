package org.ctonline.dao.basic;


import org.ctonline.dao.BaseHibernateDAO;
import org.ctonline.po.basic.CtSystemInfo;


public interface CtSystemInfoDAO extends BaseHibernateDAO{
	public void update(CtSystemInfo systemInfo);//
	public CtSystemInfo getInfo();//
}

package org.ctonline.dao.basic;

import java.util.List;

import org.ctonline.dao.BaseHibernateDAO;
import org.ctonline.po.basic.CtNotice;
import org.ctonline.util.Page;

public interface CtNoticeDAO extends BaseHibernateDAO{
	public List<CtNotice> loadAll(Page page, Integer typeId);
	public Long totalCount(String str);
	public CtNotice findById(int id);
	public List<CtNotice> findAll(String keyword,Page page);

}

package org.ctonline.dao.user;

import java.util.List;

import org.ctonline.dao.BaseHibernateDAO;
import org.ctonline.po.basic.CtFilter;
import org.ctonline.po.basic.CtNotice;
import org.ctonline.po.basic.CtSuggestion;
import org.ctonline.po.basic.CtTemp;
import org.ctonline.po.user.CtContact;
import org.ctonline.po.user.CtDraw;
import org.ctonline.po.user.CtDrawDetail;
import org.ctonline.po.user.CtDrawPacket;
import org.ctonline.po.user.CtExtension;
import org.ctonline.po.user.CtHongbaoTemp;
import org.ctonline.po.user.CtLoginFailed;
import org.ctonline.po.user.CtSms;
import org.ctonline.po.user.CtUesrClient;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.user.CtHongbao;
import org.ctonline.po.user.CtUserAddress;
import org.ctonline.util.Page;

public interface CtUserDao extends BaseHibernateDAO{
	
	//��ѯһ���û�
//	public CtUser getCtUser(CtUser ctUser); 
	//����û����ѯһ���û�
	public CtUser getCtUserByUUserid(String UUsername);
	//���������֤��
	public Long saveCtSms(CtSms ctSms);
	//����ֻ��ȥ��ѯһ���û�
	public CtUser getCtUserByUMb(String UMb);
	//����ֻ�Ų�ѯһ����֤��
	public CtSms getCtSmsByUMb(String UMb);
	//ע�Ტ�����û�
	public Long save(CtUser ctUser);
	//����û�������ֻ�Ų�ѯһ���û�
	public CtUser getCtUserByUUnameOrUMb(String arg0);
	//�޸�����
	public String update(CtUser ctUser);
	//��֤��¼�û��Ƿ�Ϸ�
	public CtUser affirm(String uname,String password) throws Exception;
	//�޸��û���Ϣ
	public String editCtuser(CtUser ctUser);
	//���չؼ��ֲ�ѯ�û�
	public  List<CtUser> findAll(String keyword);
	//����û���id��ѯ���û���������û�
	public List<?>  selectAll(Long UGId);
	//查询所有用户
	public List<CtUser> loadAll(Page page);
	public Long totalCount(String str);
	//根据UEmail获取一个用户
	public CtUser getCtUserByUEmail(String UEmail);
	//根据输入的内容查找一个用户
	public CtUser getCtuserByUname(String name);
	//根据输入的密码判断
	public CtUser getCtuserByPwd(String name,String pwd) throws Exception;
	//通过UUserid和Ucode查询一个用户
	public CtUser getCtuserByUUseridAndUCode(String UUserid,String UCode);
	//通过邮件地址和密码验证登录
	public CtUser affimByEmailAndPwd(String email,String password)throws Exception;
	//绑定邮箱的验证
	public CtUser getCtUserByUcode(String UCode);
	//根据用户id或者手机号获取一个用户
	public CtUser getCtUserByUUseridOrUMb(String arg0);
	public List<CtNotice> findAllNotices();
	public String saveadvice(CtSuggestion advice);
	public String savecontact(CtContact contact);
	public int getIpcount(String IP,String date);
	public CtUser getUserByUId(Long uId);
	public CtUser findAliUserId(String user_id);
	public CtUser findQQUserId(String openID);
	public CtUser findWeiBoUid(String uid);
	public CtUser findWeiXinOpenId(String openid);
	public CtUser findExTeById(String _userId);
	public void updateExTen(CtExtension extension);
	public List<CtExtension> findExTenByUserId(Long uId);
	public void saveHongBao(CtHongbao hongbao);
	public CtHongbao getHongBaoByOpenid(String openid,String desc);//根据openid获取红包记录
	public Long getHongBaototalByDesc(String desc);//根据desc获取红包条数
	public void saveHongBaoTemp(CtHongbaoTemp hongbaot);
	
	public CtHongbaoTemp getHongBaoTempByOpenidDate(String openid,String date);//根据openid及日期获取记录
	public CtTemp getTemp(String sn,String openid,String tc,String gx);//根据openid及日期获取记录
	public String updateTemp(String tid);//根据openid及日期获取记录
	
	public List<CtTemp> getTempList();//获取所有未发红包且已关联用户
	public CtTemp getTempC(String openid,String tZ);//根据openid及日期获取记录
	public List<CtFilter> getFilter(String userName);
	public List<CtDrawPacket> findPacketsByNowHong();
	public void updateDrawDetail(CtDrawDetail drawDetail);
	public CtDrawDetail findDrawDetailByUid(Long uId);
	public void updatePacket(CtDrawPacket packet);
	public CtDraw findDrawByUid(Long uId);
	public void updateDraw(CtDraw draw);
	public List<CtDrawDetail> findDrawDetailListTop20();
	public int findAllHongCount();
	public List<CtDrawDetail> findMyDrawInfo(Long uId);
	public CtDrawDetail findDrawDetailByDsn(String content);
	public CtDrawDetail findDrawByOpenId(String fromUserName);
	public List<CtLoginFailed> findLoginFailedByMac(String macAddress);
	public void saveLoginFailed(CtLoginFailed loginFailed);
	public List<CtLoginFailed> findLoginFailedByUname(String uname);
	public List<CtUserAddress> findAllAddressByUid(Long uid, Page page);
	public void updateCusDefultByAid(Long aid, Long uid);
	public List<CtUesrClient> findCusMent(Long uId, Page page);
}

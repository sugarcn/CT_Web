package org.ctonline.dao.user.impl;

import java.util.List;

import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.user.CtAddTicketDAO;
import org.ctonline.po.user.CtAddTicket;
import org.ctonline.po.user.CtUser;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CtAddTicketImpl extends BaseHibernateDAOImpl implements CtAddTicketDAO {

	@Override
	public List<CtAddTicket> loadAll(Page page,CtUser cu) {
		// TODO Auto-generated method stub
		List<CtAddTicket> ctAddTickets = null;
		
		String hql = "from CtAddTicket c where c.UId ="+ cu.getUId() +" order by c.theDefault";
		Query query = getSession().createQuery(hql);
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		ctAddTickets = query.list();
		
		return ctAddTickets;
	}
	
	
	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtUser cu,CtAddTicket ct where cu.UId=ct.UId";
		if (str !=null)
			hql+=str;
		Session session = getSession();
		Long count= (Long)session.createQuery(hql).uniqueResult();
		
		return count;
	}

	@Override
	public CtAddTicket findById(Long id) {
		// TODO Auto-generated method stub
		Session session =this.getSession();
		CtAddTicket addTicket = (CtAddTicket) session.get(CtAddTicket.class, id);
		
		return addTicket;
	}

	@Override
	public List<CtAddTicket> loadOne(Long id) {
		// TODO Auto-generated method stub
		List<CtAddTicket> ctAddTickets = null;
		
		String hql = "from CtAddTicket ct where ct.UId="+id;
		Query query = getSession().createQuery(hql);
		ctAddTickets =query.list();
		
		return ctAddTickets;
	}

	@Override
	public String update(CtAddTicket ctAddTicket) {
		// TODO Auto-generated method stub
		getSession().update(ctAddTicket);
		
		return "success";
		
	}

	@Override
	public String save(CtAddTicket addTicket) {
		// TODO Auto-generated method stub
		Long id = (Long) getSession().save(addTicket);
		
		String res=id.toString();
		return res;
	}

	@Override
	public CtAddTicket getTicketByUId(Long UId) {
		// TODO Auto-generated method stub
		String hql = "from CtAddTicket ct where ct.UId="+UId;
		CtAddTicket ctAddTicket = (CtAddTicket) getSession().createQuery(hql).uniqueResult();
		
		return ctAddTicket;
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		String hql = "delete from CtAddTicket ct where ct.ATId="+id;
		Query query = getSession().createQuery(hql);
		int d = query.executeUpdate();
		
		
		return d;
	}


	@Override
	public CtAddTicket isExists(Long UId) {
		// TODO Auto-generated method stub
		CtAddTicket cat;
		String hql = "from CtAddTicket cat where cat.UId="+UId;
		Query query = getSession().createQuery(hql);
		if(query.list().size() > 0){
			 cat = (CtAddTicket) query.list().get(0);
		}else {
			cat = null;
		}
		return cat;
	}
	/**
	 * 修改默认增票
	 */
	public void setTehDeafult(Long Tid, CtUser cu){
		String hql1 = "UPDATE CT_ADD_TICKET SET THE_DEFAULT = '1' WHERE U_ID = "+ cu.getUId() +" AND THE_DEFAULT = '0'";
		String hql2 = "UPDATE CT_ADD_TICKET SET THE_DEFAULT = '0' WHERE A_T_ID = "+Tid;
		Query query1 = getSession().createSQLQuery(hql1);
		Query query2 = getSession().createSQLQuery(hql2);
		query1.executeUpdate();
		query2.executeUpdate();
	}
}

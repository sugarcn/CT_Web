package org.ctonline.dao.user.impl;

import java.math.BigDecimal;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.ctonline.dao.BaseHibernateDAOImpl;

import org.ctonline.dao.user.CtUserDao;
import org.ctonline.po.basic.CtFilter;
import org.ctonline.po.basic.CtNotice;
import org.ctonline.po.basic.CtSuggestion;
import org.ctonline.po.basic.CtTemp;
import org.ctonline.po.user.CtContact;
import org.ctonline.po.user.CtDraw;
import org.ctonline.po.user.CtDrawDetail;
import org.ctonline.po.user.CtDrawPacket;
import org.ctonline.po.user.CtExtension;
import org.ctonline.po.user.CtHongbaoTemp;
import org.ctonline.po.user.CtLoginFailed;
import org.ctonline.po.user.CtSms;
import org.ctonline.po.user.CtUesrClient;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.user.CtHongbao;
import org.ctonline.po.user.CtUserAddress;
import org.ctonline.util.MD5;
import org.ctonline.util.MD5Util;
import org.ctonline.util.MacUtil;
import org.ctonline.util.Page;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

public class CtUserDaoImpl extends BaseHibernateDAOImpl implements CtUserDao {
	

	/*@Override
	public CtUser getCtUser(CtUser ctUser) {
		// TODO Auto-generated method stub
		getSession() getSession() = this.getgetSession()Factory().opengetSession()();
		return null;
	}*/



	@Override
	public CtUser getCtUserByUUserid(String UUserid) {
		// TODO Auto-generated method stub
//		String hql = "from CtUser cu where cu.UUsername='"+UUsername+"'";
        String hql = "from CtUser cu where cu.UUserid=? or cu.UUsername='" + UUserid+"'";
		Query query= getSession().createQuery(hql);
		query.setParameter(0, UUserid);
		List<CtUser> ctUsers =query.list();
		CtUser ctUser = new CtUser();
		if(ctUsers.size() <= 0){
			ctUser = null;
		}else {
			ctUser = ctUsers.get(0);
		}
		return ctUser;
	}


	@Override
	public Long saveCtSms(CtSms ctSms) {
		// TODO Auto-generated method stub
		Long id;
		id = (Long) getSession().save(ctSms);
		return id;
	}


	@Override
	public CtUser getCtUserByUMb(String UMb) {
		// TODO Auto-generated method stub
		String hql = "from CtUser cu where cu.UMb=?";
		Query query= getSession().createQuery(hql);
		query.setParameter(0, UMb);
		List<CtUser> ctUsers =query.list();
		CtUser ctUser = new CtUser();
		if(ctUsers.size() <= 0){
			ctUser = null;
		}else {
			ctUser = ctUsers.get(0);
		}
		return ctUser;
	}


	@Override
	public CtSms getCtSmsByUMb(String UMb) {
		// TODO Auto-generated method stub
		String hql = "from CtSms cs where cs.UMb=? order by cs.smsTime desc";
		Query query = getSession().createQuery(hql);
		query.setParameter(0, UMb);
		CtSms cs = new CtSms();
		List<CtSms> ctSms = query.list();
		if(ctSms.size()<=0){
			cs = null;
		}else {
			cs = ctSms.get(0);
		}
		return cs;
	}


	@Override
	public Long save(CtUser ctUser) {
		// TODO Auto-generated method stub
		Long id = (Long) getSession().save(ctUser);
		return id;
	}


	@Override
	public CtUser getCtUserByUUnameOrUMb(String arg0) {
		// TODO Auto-generated method stub
		String hql = "from CtUser cu where cu.UUserid ='"+arg0+"'  or cu.UMb='"+arg0+"' or cu.UUsername = '" + arg0 + "' ";
		Query query = getSession().createQuery(hql);
//		query.setParameter(0, arg0);
//		query.setParameter(1, arg0);
		CtUser ctUser = new CtUser();
		List<CtUser> ctUsers =query.list();
		if(ctUsers != null && ctUsers.size()>0){
			ctUser = (CtUser) query.list().get(0);
		}
		return ctUser;
	}


	@Override
	public String update(CtUser ctUser) {
		// TODO Auto-generated method stub
		getSession().update(ctUser);
		return "success";
	}


	@Override
	public CtUser affirm(String uname,String password) throws Exception {
		// TODO Auto-generated method stub
		CtUser cu = this.getCtUserByUUseridOrUMb(uname);
		if(cu == null){
			return null;
		}else {
			String passwdorg = "CT"+cu.getUUserid()+password;
			String encryptPwd = MD5.sign(passwdorg, "CT", "utf-8");
			String hql = "from CtUser cu where ( cu.UUserid = '"+uname+"' or cu.UMb= '"+uname+"' or cu.UUsername = '"+uname+"' ) and cu.UPassword='"+encryptPwd+"'";
			Query query = getSession().createQuery(hql);
			CtUser ctUser = new CtUser();
			List<CtUser> ctUsers =query.list();
			if(ctUsers.size() <= 0){
				ctUser = null;
			}else {
				ctUser = ctUsers.get(0);
			}
			return ctUser;
		}
		
	}


	@Override
	public String editCtuser(CtUser ctUser) {
		// TODO Auto-generated method stub
		getSession().update(ctUser);
		return "success";
	}
	@Override
	public List<CtUser> findAll(String keyword) {
		// TODO Auto-generated method stub
		List< CtUser> user=null;
		String sql="from CtUser";
		String str="";
		if (keyword !=null && !keyword.equals("")){
			str+=" where UUsername like '%"+keyword+"%' ";
		}
		//str+=" order by regionID desc";
		sql+=str;
		Query query= getSession().createQuery(sql);
		
		user=query.list();
		return user;
	}
	
	public List<?>  selectAll(Long UGId){
		
		List<?> user=null;
		String sql="select a.UId , a.UUsername  from CtUser a , CtUserGroupRelation b where a.UId = b.UId and b.UGId="+UGId;
		//str+=" order by regionID desc";
		Query query= getSession().createQuery(sql);
		
		user=query.list();
		return user;
		
	}


	@Override
	public List<CtUser> loadAll(Page page) {
		// TODO Auto-generated method stub
		List<CtUser> users = null;
		String hql = "from CtUser";
		Query query = getSession().createQuery(hql);
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		users = query.list();
		return users;
	}


	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtUser";
		if (str !=null)
			hql+=str;
		Long count= (Long)getSession().createQuery(hql).uniqueResult();
		return count;
	}


	@Override
	public CtUser getCtUserByUEmail(String UEmail) {
		// TODO Auto-generated method stub
		String hql = "from CtUser cu where cu.UEmail=?";
		Query query= getSession().createQuery(hql);
		query.setParameter(0, UEmail);
		List<CtUser> ctUsers =query.list();
		CtUser ctUser = new CtUser();
		if(ctUsers.size() <= 0){
			ctUser = null;
		}else {
			ctUser = ctUsers.get(0);
		}
		return ctUser;
	}


	@Override
	public CtUser getCtuserByUUseridAndUCode(String UUserid, String UCode) {
		// TODO Auto-generated method stub
		String hql = "from CtUser cu where cu.UUserid=? and cu.UCode=?";
		Query query= getSession().createQuery(hql);
		query.setParameter(0, UUserid);
		query.setParameter(1, UCode);
		List<CtUser> ctUsers =query.list();
		CtUser ctUser = new CtUser();
		if(ctUsers.size() <= 0){
			ctUser = null;
		}else {
			ctUser = ctUsers.get(0);
		}
		return ctUser;
	}


	@Override
	public CtUser affimByEmailAndPwd(String email, String password) throws Exception {
		// TODO Auto-generated method stub
		CtUser cu = getCtUserByUEmail(email);
		if(cu == null){
			return null;
		}else {
			String passwdorg = "CT"+cu.getUUserid()+password;
			String encryptPwd = MD5.sign(passwdorg, "CT", "utf-8");
			//String encryptPwd = MD5Util.getEncryptPasswd(passwdorg);
			String hql = "from CtUser cu where cu.UEmail =? and cu.UPassword=?";
			Query query = getSession().createQuery(hql);
			query.setParameter(0, email);
			query.setParameter(1, encryptPwd);
			CtUser ctUser = new CtUser();
			List<CtUser> ctUsers =query.list();
			if(ctUsers.size() <= 0){
				ctUser = null;
			}else {
				ctUser = ctUsers.get(0);
			}
			return ctUser;
		}
		
	}


	@Override
	public CtUser getCtUserByUcode(String UCode) {
		// TODO Auto-generated method stub
		String hql = "from CtUser cu where cu.UCode =?";
		Query query = getSession().createQuery(hql);
		query.setParameter(0, UCode);
		CtUser ctUser = new CtUser();
		List<CtUser> ctUsers =query.list();
		if(ctUsers.size() <= 0){
			ctUser = null;
		}else {
			ctUser = ctUsers.get(0);
		}
		return ctUser;
	}


	@Override
	public CtUser getCtUserByUUseridOrUMb(String arg0) {
		// TODO Auto-generated method stub
		String hql = "from CtUser cu where cu.UUserid ='"+arg0+"' or cu.UMb='"+arg0+"' or cu.UUsername='"+arg0+"'";
		Query query = getSession().createQuery(hql);
		CtUser ctUser = new CtUser();
		List<CtUser> ctUsers =query.list();
		if(ctUsers != null && ctUsers.size()>0){
			ctUser = (CtUser) query.list().get(0);
		}
		return ctUser;
	}


	@Override
	public CtUser getCtuserByUname(String name) {
		String hql = "from CtUser cu where cu.UUsername='"+name+"' or cu.UMb='"+name+"' or cu.UEmail='"+name+"'";
		Query query= getSession().createQuery(hql);
		List<CtUser> ctUsers =query.list();
		CtUser ctUser = new CtUser();
		if(ctUsers.size() <= 0){
			ctUser = null;
		}else {
			ctUser = ctUsers.get(0);
		}
		return ctUser;
	}


	@Override
	public CtUser getCtuserByPwd(String name,String pwd) throws Exception {
		CtUser cu = getCtuserByUname(name);
		if(cu == null){
			return null;
		}else {
			String passwdorg = "CT"+cu.getUUserid()+pwd;
			String encryptPwd = MD5.sign(passwdorg, "CT", "utf-8");
		String hql = "from CtUser cu where cu.UPassword='"+encryptPwd+"'";
		Query query= getSession().createQuery(hql);
		List<CtUser> ctUsers =query.list();
		CtUser ctUser = new CtUser();
		if(ctUsers.size() <= 0){
			ctUser = null;
		}else {
			ctUser = ctUsers.get(0);
		}
		return ctUser;
	}
}


	@Override
	public List<CtNotice> findAllNotices() {
		String hql = " from CtNotice cn order by noTime desc";
		Query query = getSession().createQuery(hql);
		List<CtNotice> list = query.list();
		List<CtNotice> list1 = new ArrayList<CtNotice>();
		if(list.size() > 5){
			for (int i = 0; i < 5; i++) {
				list1.add(list.get(i));
			}
		} else {
			list1 = list;
		}
		return list1;
	}


	@Override
	public String saveadvice(CtSuggestion advice) {
		String hql = "insert into Ct_Suggestion(su_Id,g_email,g_tel,g_desc)  values(seq_suggestion.nextval,'"+advice.getGemail()+"','"+advice.getGtel()+"','"+advice.getGdesc()+"')";
		Query query = getSession().createSQLQuery(hql);
		int id = query.executeUpdate();
		return "success";
	}


	@Override
	public String savecontact(CtContact contact) {
		String hql = "insert into Ct_Contact(CON_ID,C_EMAIL,C_TEL,C_DESC,C_IP,C_TIME) values(SEQ_CONTACT.NEXTVAL,'"+contact.getCemail()+"','"+contact.getCtel()+"','"+contact.getCdesc()+"','"+contact.getCIp()+"','"+contact.getCtime()+"')";
		Query query = getSession().createSQLQuery(hql);
		int id = query.executeUpdate();
		return "success";
	}


	@Override
	public int getIpcount(String Ip,String date) {
		// TODO Auto-generated method stub
		String sql="select count('"+Ip+"') from Ct_Contact ct where ct.C_Time like '"+date+"%'";
		int count=0;
		BigDecimal result=(BigDecimal)getSession().createSQLQuery(sql).uniqueResult();
		count= result.intValue();
		return count;
	}


	@Override
	public CtUser getUserByUId(Long uId) {
		String hql = "from CtUser cu where cu.UId = "+ uId;
		Query query = getSession().createQuery(hql);
		List<CtUser> list = query.list();
		if(list!= null && list.size() > 0){
			return list.get(0);
		} 
		return null;
	}


	@Override
	public CtUser findAliUserId(String user_id) {
		String hql = " from CtUser cu where cu.UAliloginUserid = " + user_id;
		Query query = getSession().createQuery(hql);
		List<CtUser> list = query.list();
		if(list!= null && list.size() > 0){
			return list.get(0);
		} 
		return null;
	}


	@Override
	public CtUser findQQUserId(String openID) {
		String hql = " from CtUser cu where cu.UQqloginUserid = '" + openID + "'";
		Query query = getSession().createQuery(hql);
		List<CtUser> list = query.list();
		if(list!= null && list.size() > 0){
			return list.get(0);
		} 
		return null;
	}


	@Override
	public CtUser findWeiBoUid(String uid) {
		String hql = " from CtUser cu where cu.UWeibologinUserid = '" + uid + "'";
		Query query = getSession().createQuery(hql);
		List<CtUser> list = query.list();
		if(list!= null && list.size() > 0){
			return list.get(0);
		} 
		return null;
	}


	@Override
	public CtUser findWeiXinOpenId(String openid) {
		String hql = " from CtUser cu where cu.UWeixinloginUserid = '" + openid + "'";
		Query query = getSession().createQuery(hql);
		List<CtUser> list = query.list();
		if(list!= null && list.size() > 0){
			return list.get(0);
		} 
		return null;
	}


	@Override
	public CtUser findExTeById(String _userId) {
		String hql = " from CtUser cu where cu.UExtensionid = '" + _userId + "'";
		Query query = getSession().createQuery(hql);
		List<CtUser> list = query.list();
		if(list!= null && list.size() > 0){
			return list.get(0);
		} 
		return null;
	}


	@Override
	public void updateExTen(CtExtension extension) {
		getSession().merge(extension);
	}


	@Override
	public List<CtExtension> findExTenByUserId(Long uId) {
		String hql = " from CtExtension cu where cu.extensionUserid = '" + uId + "'";
		Query query = getSession().createQuery(hql);
		List<CtExtension> list = query.list();
		if(list!= null && list.size() > 0){
			return list;
		} 
		return null;  
	}


	@Override
	public void saveHongBao(CtHongbao hongbao) {
		getSession().save(hongbao);
	}

	
	@Override
	public CtHongbao getHongBaoByOpenid(String openid,String desc) {
		String hql = " from CtHongbao ch where ch.hongbaoOpenid = '" + openid+"' and ch.hongbaoDesc='"+desc+"'";
		Query query = getSession().createQuery(hql);
		List<CtHongbao> list = query.list();
		if(list!= null && list.size() > 0){
			return list.get(0);
		} 
		return null;
	}
	
	@Override
	public Long getHongBaototalByDesc(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtHongbao";
		if (str !=null)
			hql+=str;
		Long count= (Long)getSession().createQuery(hql).uniqueResult();
		return count;
	}
	
	@Override
	public void saveHongBaoTemp(CtHongbaoTemp hongbaot) {
		getSession().save(hongbaot);
	}
	
	
	@Override
	public CtHongbaoTemp getHongBaoTempByOpenidDate(String openid,String date) {
		String hql = " from CtHongbaoTemp ch where ch.htopenid = '" + openid+"' and ch.htdate='"+date+"'";
		Query query = getSession().createQuery(hql);
		List<CtHongbaoTemp> list = query.list();
		if(list!= null && list.size() > 0){
			return list.get(0);
		} 
		return null;
	}
	
	
	@Override
	public CtTemp getTemp(String sn,String openid,String tc,String gx) {
		// TODO Auto-generated method stub
		
		
		String hql =  " from CtTemp where tSN='"+sn+"' and tN is null";
		if (gx.equals("gx")){
			hql =  " from CtTemp where tSN='"+sn+"' and tN is not null";
		}
		//hql = " from CtTemp where tSN='"+sn+"'";
		Query query = getSession().createQuery(hql);

		List<CtTemp> list = query.list();
		if(list!= null && list.size() > 0){

			hql="update CtTemp set tSN='"+sn+"' where tSN='"+sn+"'";
			query = getSession().createQuery(hql);
			query.executeUpdate();
			
			CtTemp cte = new CtTemp();
			cte = list.get(0);
			if (tc.equals("NULL")){
				cte.settN(openid);
			}
			if (tc.equals("T_T")){
				cte.settT(openid);
			}
			if (tc.equals("T_Z")){
				cte.settZ(openid);
			}
			if (tc.equals("T_D")){
				cte.settD(openid);
			}
			if (tc.equals("T_ZH")){
				cte.settZH(openid);
			}
			
			
			return cte;
		}
		
		return null;
	}

	
	public CtTemp getTemp(String openid) {
		// TODO Auto-generated method stub
		
		
		String hql =  " from CtTemp where tN ='"+openid+"' and tT='Y'";
		//hql = " from CtTemp where tSN='"+sn+"'";
		Query query = getSession().createQuery(hql);

		List<CtTemp> list = query.list();
		if(list!= null && list.size() > 0){

			hql="update CtTemp set tN='"+openid+"' where tN='"+openid+"'";
			query = getSession().createQuery(hql);
			query.executeUpdate();
			
			CtTemp cte = new CtTemp();
			cte = list.get(0);
			
			cte.settN(openid);
			return cte;
		}
		
		return null;
	}
	

	@Override
	public String updateTemp(String tid) {
		// TODO Auto-generated method stub
		//int d;" from CtTemp where tSN='"+sn+"' and tN is null";
		/*
		String hql=" from CtTemp where tId="+tid;
		Query query = getSession().createQuery(hql);
		List<CtTemp> list = query.list();
		CtTemp cte = new CtTemp();
		if(list!= null && list.size() > 0){
			//hql="update CtTemp set tT='Y' where tId="+tid;
			//query = getSession().createQuery(hql);
			//query.executeUpdate();

			cte = list.get(0);
			
			cte.settT("Y");
			
		}

		return cte.gettT();
		*/
		
		String hql =  " from CtTemp where tId="+tid;
		//hql = " from CtTemp where tSN='"+sn+"'";
		Query query = getSession().createQuery(hql);

		List<CtTemp> list = query.list();
		if(list!= null && list.size() > 0){

			hql="update CtTemp set tT='Y' where tId="+tid;
			query = getSession().createQuery(hql);
			query.executeUpdate();
			
			CtTemp cte = new CtTemp();
			cte = list.get(0);
			
			cte.settT("Y");
			return cte.gettT();
		}
		
		return null;

	}
	
	@Override
	public List<CtTemp> getTempList() {
		//String hql = " from CtTemp cu where cu.extensionUserid = '" + uId + "'";
		String hql =  " from CtTemp where tN is not null and tT is not null and tNum=800";
		Query query = getSession().createQuery(hql);
		List<CtTemp> list = query.list();
		if(list!= null && list.size() > 0){
			return list;
		} 
		return null;
	}
	
	public CtTemp getTempC(String openid,String tZ) {
		// TODO Auto-generated method stub
		
		
		String hql =  " from CtTemp where tN ='"+openid+"' and "+tZ+" is null";
		//hql = " from CtTemp where tSN='"+sn+"'";
		Query query = getSession().createQuery(hql);

		List<CtTemp> list = query.list();
		if(list!= null && list.size() > 0){

			hql="update CtTemp set tN='"+openid+"' where tN='"+openid+"'";
			query = getSession().createQuery(hql);
			query.executeUpdate();
			
			CtTemp cte = new CtTemp();
			cte = list.get(0);
			
			cte.settN(openid);
			return cte;
		}
		
		return null;
	}


	@Override
	public List<CtFilter> getFilter(String userName) {
		String hql = " from CtFilter cf";
		Query query = getSession().createQuery(hql);
		return query.list();
	}


	@Override
	public List<CtDrawPacket> findPacketsByNowHong() {
		String hql = " from CtDrawPacket cd where cd.RRNum <> 0";
		Query query = getSession().createQuery(hql);
		List<CtDrawPacket> list = query.list();
		if(list != null && list.size() > 0){
			return list;
		}
		return null;
	}


	@Override
	public void updateDrawDetail(CtDrawDetail drawDetail) {
		getSession().merge(drawDetail);
	}


	@Override
	public CtDrawDetail findDrawDetailByUid(Long uId) {
		String hql = " from CtDrawDetail cd where cd.UId="+ uId + " order by cd.DNum desc";
		Query query = getSession().createQuery(hql);
		List<CtDrawDetail> list = query.list();
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}


	@Override
	public void updatePacket(CtDrawPacket packet) {
		getSession().merge(packet);
	}


	@Override
	public CtDraw findDrawByUid(Long uId) {
		String hql = " from CtDraw cd where cd.UId="+ uId;
		Query query = getSession().createQuery(hql);
		List<CtDraw> list = query.list();
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}


	@Override
	public void updateDraw(CtDraw draw) {
		getSession().merge(draw);
	}


	@Override
	public List<CtDrawDetail> findDrawDetailListTop20() {
		String hql = " from CtDrawDetail cd order by cd.DTime desc";
		Query query = getSession().createQuery(hql);
		query.setFirstResult(0);
		query.setMaxResults(20);
		List<CtDrawDetail> list = query.list();
		if(list != null && list.size() > 0){
			return list;
		}
		return null;
	}


	@Override
	public int findAllHongCount() {
		String hql = " select sum(cd.RJNum) from CtDrawPacket cd";
		Query query = getSession().createQuery(hql);
		Long count = (Long) query.uniqueResult();
		return count.intValue();
	}




	@Override
	public List<CtDrawDetail> findMyDrawInfo(Long uId) {
		String hql = " from CtDrawDetail cd where cd.UId="+uId+" order by cd.DTime desc";
		Query query = getSession().createQuery(hql);
		query.setFirstResult(0);
		query.setMaxResults(20);
		List<CtDrawDetail> list = query.list();
		if(list != null && list.size() > 0){
			return list;
		}
		return null;
	}


	@Override
	public CtDrawDetail findDrawDetailByDsn(String content) {
		String hql = " from CtDrawDetail cd where cd.DSn='"+content+"'";
		Query query = getSession().createQuery(hql);
		query.setFirstResult(0);
		query.setMaxResults(20);
		List<CtDrawDetail> list = query.list();
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}


	@Override
	public CtDrawDetail findDrawByOpenId(String fromUserName) {
		String hql = " from CtDrawDetail cd where cd.DOpenid='"+fromUserName+"'";
		Query query = getSession().createQuery(hql);
		query.setFirstResult(0);
		query.setMaxResults(20);
		List<CtDrawDetail> list = query.list();
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}


	@Override
	public List<CtLoginFailed> findLoginFailedByMac(String macAddress) {
		try {
			//获取比当前时间少五分钟的时间
			Long timeNowLong = new java.util.Date().getTime();
			timeNowLong = timeNowLong - (5 * 60 * 1000);
			java.util.Date date = new java.util.Date(timeNowLong);
			
			String hql = " from CtLoginFailed clf where clf.floginTime >= '" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date) + "' and clf.fnetworkMac='"+macAddress+"' ";
			Query query = getSession().createQuery(hql);
			List<CtLoginFailed> list = query.list();
			if(list != null && list.size() > 0){
				return list;
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public void saveLoginFailed(CtLoginFailed loginFailed) {
		getSession().merge(loginFailed);
	}


	@Override
	public List<CtLoginFailed> findLoginFailedByUname(String uname) {
		try {
			//获取比当前时间少五分钟的时间
			Long timeNowLong = new java.util.Date().getTime();
			timeNowLong = timeNowLong - (30 * 60 * 1000);
			java.util.Date date = new java.util.Date(timeNowLong);
			
			//带当前电脑网络
//			String hql = " from CtLoginFailed clf where clf.floginTime >= '" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date) + "' and clf.fnetworkMac='"+MacUtil.getMacAddress()+"' and clf.flginName='"+uname+"' ";
			//不带当前电脑网络
			String hql = " from CtLoginFailed clf where clf.floginTime >= '" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date) + "' and clf.flginName='"+uname+"'  and clf.fnameOrPass=1";
			Query query = getSession().createQuery(hql);
			List<CtLoginFailed> list = query.list();
			if(list != null && list.size() > 0){
				return list;
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public List<CtUserAddress> findAllAddressByUid(Long uid, Page page) {
		String hql = " from CtUserAddress cua where cua.UId=" + uid + " and cua.ACustomer is not null order by cua.ACustomer desc";
		Query query = getSession().createQuery(hql);
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCountCusAddress(uid));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		List<CtUserAddress> list = query.list();
		if(list != null && list.size() > 0){
			return list;
		}
		return null;
	}


	private Long totalCountCusAddress(Long uid) {
		String hql = "select count(*) from CtUserAddress cua where cua.UId=" + uid + " and cua.ACustomer is not null order by cua.ACustomer desc";
		Query query = getSession().createQuery(hql);
		return (Long) query.uniqueResult();
	}


	@Override
	public void updateCusDefultByAid(Long aid, Long uid) {
		String hql = " update CtUserAddress cua set cua.ACustomer = 1, cua.isdefault=1 where cua.UId=" + uid;
		Query query = getSession().createQuery(hql);
		query.executeUpdate();
		List<CtUserAddress> list = getSession().createQuery(" from CtUserAddress cua where cua.UId=" + uid + " and cua.cid=" + aid).list();
		if(list != null && list.size() > 0){
			hql = " update CtUserAddress cua set cua.isdefault=0 where cua.AId=" + list.get(0).getAId();
			query = getSession().createQuery(hql);
			query.executeUpdate();
		}
		hql = " update CtUserAddress cua set cua.ACustomer = 2 where cua.cid=" + aid;
		
		query = getSession().createQuery(hql);
		query.executeUpdate();
		
		hql = " update CtUesrClient cuc set cuc.cdefult = 0 where cuc.uid=" + uid;
		
		query = getSession().createQuery(hql);
		query.executeUpdate();
		
		hql = " update CtUesrClient cuc set cuc.cdefult = 1 where cuc.cid=" + aid;
		
		query = getSession().createQuery(hql);
		query.executeUpdate();
		
		
	}


	@Override
	public List<CtUesrClient> findCusMent(Long uId, Page page) {
		String hql = " from CtUesrClient cua where cua.uid=" + uId + " and cua.cname is not null order by cua.cdefult desc";
		Query query=getSession().createQuery(hql);
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCountCusClient(uId));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		List<CtUesrClient> list = query.list();
		if(list != null && list.size() > 0){
			return  list;
		}
		return null;
	}


	private Long totalCountCusClient(Long uId) {
		String hql = " select count(*) from CtUesrClient cua where cua.uid=" + uId + " and cua.cname is not null ";
		Query query=getSession().createQuery(hql);
		return (Long) query.uniqueResult();
	}

}

package org.ctonline.dao.user.impl;

import java.util.List;

import org.ctonline.dao.BaseHibernateDAOImpl;
import org.ctonline.dao.user.CtUserAddressDAO;
import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.user.CtUserAddress;
import org.ctonline.util.Page;
import org.hibernate.Query;
import org.hibernate.Session;

public class CtUserAddressImpl extends BaseHibernateDAOImpl implements CtUserAddressDAO{
	

	@Override
	public String save(CtUserAddress address) {
		// TODO Auto-generated method stub
		String hql = "insert into ct_user_address(A_ID,U_ID,A_CONSIGNEE,COUNTRY,PROVINCE,CITY,DISTRICT,ADDRESS,ZIPCODE,TEL,MB,ISDEFAULT,ADESC) values(seq_user_address.nextval,'"+address.getUId()+"','"+address.getAConsignee()+"','"+address.getCountry()+"','"+address.getProvince()+"','"+address.getCity()+"','"+address.getDistrict()+"','"+address.getAddress()+"','"+address.getZipcode()+"','"+address.getTel()+"','"+address.getMb()+"','"+address.getIsdefault()+"','"+address.getAdesc()+"')";
		Query query = getSession().createSQLQuery(hql);
		Long id = (long) query.executeUpdate();
		String res=id.toString();
		return res;
//		Long id;
//		
//		id =(Long)getSession().save(address);
//		
//		
//		return id;
	}

	@Override
	public CtUserAddress findById(Long id) {
		// TODO Auto-generated method stub
		
		CtUserAddress address = (CtUserAddress)getSession().get(CtUserAddress.class, id);
		
		return address;
	}

	@Override
	public List<CtUserAddress> loadAll(Page page ,CtUser cu) {
		// TODO Auto-generated method stub
		List< CtUserAddress> types=null;
		
		Query query= getSession().createQuery("from CtUserAddress cua where cua.UId="+cu.getUId()+" order by cua.isdefault, cua.AId desc");
		if (page.getCurrentPage()!=0){
			page.setTotalCount(this.totalCount(null));
			query.setFirstResult(page.getFirstIndex());
			query.setMaxResults(page.getPageSize());
		}
		types=query.list();
		
		return types;
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		int d;
		String hql="delete CtUserAddress as u where u.id=?";
		
		Query query= getSession().createQuery(hql);
		query.setParameter(0, id);
		d= query.executeUpdate();
		
		
		return d;
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		String hql="select count(*) from CtUserAddress";
		if (str !=null)
			hql+=str;
		Session session=this.getSession();
		Long count= (Long)session.createQuery(hql).uniqueResult();
		
		return count;
	}

	@Override
	public String update(CtUserAddress address) {
		// TODO Auto-generated method stub
		
		getSession().update(address);
		return "success";
		
		
	}

	@Override
	public List<CtUserAddress> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		List< CtUserAddress > types=null;
		String sql="from CtUserAddress";
		String str="";
		if (!keyword.equals("��������Ҫ��ѯ�Ĺؼ��") && !keyword.equals("")){
			str+=" where address like '%"+keyword+"%' ";
		}
		//str+=" order by regionID desc";
		sql+=str;
		
		Query query= getSession().createQuery(sql);
		page.setTotalCount(this.totalCount(str));
		query.setFirstResult(page.getFirstIndex());
		query.setMaxResults(page.getPageSize());
		types=query.list();
		
		return types;
	}

	@Override
	public List<CtRegion> queryById(Long id){
		List< CtRegion> region=null;
		String sql="from CtRegion  where parentId="+id;
	
	Query query= getSession().createQuery(sql);
	region=query.list();
	
	return region;
		
	}

	@Override
	public String setAsDeafult(Long Aid, CtUser cu) {
		// TODO Auto-generated method stub
		
		String hql1 = "UPDATE  CT_USER_ADDRESS SET ISDEFAULT ='1' WHERE U_ID= "+cu.getUId() +" AND ISDEFAULT ='0' ";
		String hql2 = "UPDATE  CT_USER_ADDRESS SET ISDEFAULT ='0' WHERE A_ID= "+Aid;
		Query query1 = getSession().createSQLQuery(hql1);
		Query query2 = getSession().createSQLQuery(hql2);
		query1.executeUpdate();
		query2.executeUpdate();
		return "success";
		
	}

	@Override
	public CtUserAddress findByNameandAddress(String name, String address,CtUser cu) {
		String hql = "from CtUserAddress as cc where cc.AConsignee = '" + name
				+ "' and cc.address = '" + address+"' and cc.UId="+cu.getUId();

		Query query = getSession().createQuery(hql);
		List<CtUserAddress> addlist = query.list();
		CtUserAddress add=null;
		if(addlist.size() >0){
			 add = addlist.get(0);
		}else {
			 add = null;
		}
		return add;
	}


}

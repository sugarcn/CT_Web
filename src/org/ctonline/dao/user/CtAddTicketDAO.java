package org.ctonline.dao.user;

import java.util.List;

import org.ctonline.dao.BaseHibernateDAO;
import org.ctonline.po.user.CtAddTicket;
import org.ctonline.po.user.CtUser;
import org.ctonline.util.Page;

public interface CtAddTicketDAO extends BaseHibernateDAO{
	public String save(CtAddTicket addTicket);//保存
	public CtAddTicket findById(Long id);//根据id查找
	public List<CtAddTicket> loadAll(Page page,CtUser cu);//分页找出所有
	public int delete(Long id);//删除一条记录
	public Long totalCount(String str);//统计条数
	public String update(CtAddTicket ctAddTicket);//更新
//	public List<CtUserRank> findAll(String keyword,Page page);//根据条件检索
////	public Long getID();
//	public String getMaxPoint();
	public List<CtAddTicket> loadOne(Long id);
	public CtAddTicket getTicketByUId(Long UId);
	//判断增票是否存在
	public CtAddTicket isExists(Long UId);
	
	/**
	 * 修改默认增票地址
	 * @param Tid 增票ID
	 * @param cu 用户对象
	 */
	public void setTehDeafult(Long Tid, CtUser cu);
}

package org.ctonline.dao.user;

import java.util.List;

import org.ctonline.dao.BaseHibernateDAO;
import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.user.CtUserAddress;
import org.ctonline.util.Page;

public interface CtUserAddressDAO extends BaseHibernateDAO{
	
	public String save(CtUserAddress address);//����
	public CtUserAddress findById(Long id);//���id����
	public CtUserAddress findByNameandAddress(String name,String address,CtUser cu);
	public List<CtUserAddress> loadAll(Page page,CtUser cu);//��ҳ�ҳ�����
	public int delete(Long id);//ɾ��һ����¼
	public Long totalCount(String str);//ͳ������
	public String update(CtUserAddress address);//����
	public List<CtUserAddress> findAll(String keyword,Page page);//�����������
	public List<CtRegion> queryById(Long id);
	//把地址设为默认收货地址
	public String setAsDeafult(Long Aid,CtUser cu);

}

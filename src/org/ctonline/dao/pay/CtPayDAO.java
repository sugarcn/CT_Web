package org.ctonline.dao.pay;

import java.util.List;


import org.ctonline.dao.BaseHibernateDAO;
import org.ctonline.po.pay.CtPayInterface;

public interface CtPayDAO  extends BaseHibernateDAO{
	public Long save(CtPayInterface payInterface);//save

	public String findPayByOrderSn(String orderSn);

	public String findXianXiaPayTimeByOrderSn(Long orderId);

	public CtPayInterface findPayInfoByOrderSn(String orderSn);
}

package org.ctonline.dao.pay.impl;

import java.util.List;

import org.ctonline.dao.BaseHibernateDAOImpl;

import org.ctonline.dao.pay.CtPayDAO;

import org.ctonline.po.order.CtPay;
import org.ctonline.po.pay.CtPayInterface;
import org.hibernate.Query;

public class CtPayImpl extends BaseHibernateDAOImpl implements CtPayDAO {

	@Override
	public Long save(CtPayInterface payInterface) {
		// TODO Auto-generated method stub
		Long id = 0L;
		getSession().merge(payInterface);
		
		
		return id;
	}

	@Override
	public String findPayByOrderSn(String orderSn) {
		String hql = " from CtPayInterface cpi where cpi.orderSn='" + orderSn + "' ";
		Query query = getSession().createQuery(hql);
		List<CtPayInterface> list = query.list();
		if(list != null && list.size() > 0){
			return list.get(0).getPayDate();
		}
		return null;
	}

	@Override
	public String findXianXiaPayTimeByOrderSn(Long orderId) {
		String hql = " from CtPay cpi where cpi.orderId='" + orderId + "' ";
		Query query = getSession().createQuery(hql);
		List<CtPay> list = query.list();
		if(list != null && list.size() > 0){
			return list.get(0).getPayDate();
		}
		return null;
	}

	@Override
	public CtPayInterface findPayInfoByOrderSn(String orderSn) {
		String hql = " from CtPayInterface cpi where cpi.orderSn='" + orderSn + "' ";
		Query query = getSession().createQuery(hql);
		List<CtPayInterface> list = query.list();
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}

}


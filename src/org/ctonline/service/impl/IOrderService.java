package org.ctonline.service.impl;

import java.util.List;

import org.ctonline.po.order.CtOrderInfo;

public interface IOrderService {

	//获取今天订单的详细信息
	public abstract List<CtOrderInfo> getOrderInfo();
	//	public static void main(String[] args) {
	//		List<CtOrderInfo> impl = new OrderServiceImpl().getOrderInfo();
	//		System.out.println(impl.size());
	//		for (int i = 0; i < impl.size(); i++) {
	//			for (int j = 0; j < impl.get(i).getOrderGoodsList().size(); j++) {
	//				System.out.println(impl.get(i).getOrderGoodsList().get(j).getCtGoods().getGName());
	//			}
	//		}
	//	}

}
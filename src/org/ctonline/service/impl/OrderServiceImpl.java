package org.ctonline.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.ctonline.po.order.CtOrderInfo;
import org.ctonline.test.RanInit;

public class OrderServiceImpl extends RanInit implements IOrderService {
	public List<CtOrderInfo> orderInfoList = new ArrayList<CtOrderInfo>();
	//获取今天订单的详细信息
	@Override
	public List<CtOrderInfo> getOrderInfo(){
		orderInfoList = orderManager.getorderToday();
		for (int i = 0; i < orderInfoList.size(); i++) {
			orderInfoList.get(i).setOrderGoodsList(orderManager.getOrderGoodsByOrderId(orderInfoList.get(i).getOrderId()));
		}
		return orderInfoList;
	}
//	public static void main(String[] args) {
//		List<CtOrderInfo> impl = new OrderServiceImpl().getOrderInfo();
//		System.out.println(impl.size());
//		for (int i = 0; i < impl.size(); i++) {
//			for (int j = 0; j < impl.get(i).getOrderGoodsList().size(); j++) {
//				System.out.println(impl.get(i).getOrderGoodsList().get(j).getCtGoods().getGName());
//			}
//		}
//	}
}

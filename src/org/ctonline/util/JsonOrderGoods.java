package org.ctonline.util;

public class JsonOrderGoods {

	//private Long orderGoodsId;
	//private Long orderId;
	private Long GId;
	private Long CId;
	//private String pack;
	private String GPrice;
	private Long GNumber;
	private Long couponId;
	private String GSubtotal;
	private String GParPrice;
	private Long GParNumber;
	private String isParOrSim;
	private String GSn;
	
	public String getGSn() {
		return GSn;
	}
	public void setGSn(String gSn) {
		GSn = gSn;
	}
	public Long getCId() {
		return CId;
	}
	public void setCId(Long cId) {
		CId = cId;
	}
//	public Long getOrderGoodsId() {
//		return orderGoodsId;
//	}
//	public void setOrderGoodsId(Long orderGoodsId) {
//		this.orderGoodsId = orderGoodsId;
//	}
	public Long getGId() {
		return GId;
	}
	public void setGId(Long gId) {
		GId = gId;
	}
	public String getGPrice() {
		return GPrice;
	}
	public void setGPrice(String gPrice) {
		GPrice = gPrice;
	}
	public Long getGNumber() {
		return GNumber;
	}
	public void setGNumber(Long gNumber) {
		GNumber = gNumber;
	}
	public Long getCouponId() {
		return couponId;
	}
	public void setCouponId(Long couponId) {
		this.couponId = couponId;
	}
	public String getGSubtotal() {
		return GSubtotal;
	}
	public void setGSubtotal(String gSubtotal) {
		GSubtotal = gSubtotal;
	}
	public String getGParPrice() {
		return GParPrice;
	}
	public void setGParPrice(String gParPrice) {
		GParPrice = gParPrice;
	}
	public Long getGParNumber() {
		return GParNumber;
	}
	public void setGParNumber(Long gParNumber) {
		GParNumber = gParNumber;
	}
	public String getIsParOrSim() {
		return isParOrSim;
	}
	public void setIsParOrSim(String isParOrSim) {
		this.isParOrSim = isParOrSim;
	}
	
}

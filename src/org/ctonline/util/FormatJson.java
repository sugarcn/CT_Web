package org.ctonline.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.ctonline.po.order.CtOrderGoods;
import org.ctonline.po.order.CtOrderInfo;

public class FormatJson {
	public static JsonOrderInfo formatJsonOrder(CtOrderInfo ctOrderInfo, String payTime) {
		JsonOrderInfo info = new JsonOrderInfo();
		
		info.setDelivertime(ctOrderInfo.getDeliverTime());
		String[] chai = ctOrderInfo.getRegion().getMergername().split(",");
		if(chai.length == 4){
			info.setCountryName(chai[0]);
			info.setProvinceName(chai[1]);
			info.setCityName(chai[2]);
			info.setDistrictName(chai[3]);
		}
		if(chai.length == 3){
			info.setCountryName(chai[0]);
			info.setProvinceName(chai[1]);
			info.setCityName(chai[2]);
		}
		if(chai.length == 2){
			info.setCountryName(chai[0]);
			info.setProvinceName(chai[1]);
		}
		info.setAddress(ctOrderInfo.getRegion().getMergername() + "," + ctOrderInfo.getAddress());
		info.setPayTime(payTime);
		info.setStatus(ctOrderInfo.getOrderStatus());
		
		info.setConsignee(ctOrderInfo.getConsignee());
		info.setCouponId(ctOrderInfo.getCouponId());
		//info.setDiscount(ctOrderInfo.getDiscount());
		info.setDsc(ctOrderInfo.getDsc().trim());
		//info.setEvaId(ctOrderInfo.getEvaId());
		info.setExId(ctOrderInfo.getExId());
		info.setExNo(ctOrderInfo.getExNo());
		//info.setFinance(ctOrderInfo.getFinance());
		//info.setFinanceTime(ctOrderInfo.getFinanceTime());
		info.setFreight(ctOrderInfo.getFreight());
		//info.setInvoice(ctOrderInfo.getInvoice());
		//info.setInvoiceType(ctOrderInfo.getInvoiceType());
		//info.setIsokTime(ctOrderInfo.getIsokTime());
		//info.setManage(ctOrderInfo.getManage());
		//info.setManageTime(ctOrderInfo.getManageTime());
		//info.setOrderId(ctOrderInfo.getOrderId());
		//info.setOrderIp(ctOrderInfo.getOrderIp());
		info.setOrderId(ctOrderInfo.getOrderId());
		info.setOrderSn(ctOrderInfo.getOrderSn());
		//info.setOrderStatus(ctOrderInfo.getOrderStatus());
		info.setOrderTime(ctOrderInfo.getOrderTime());
		info.setPay(ctOrderInfo.getPay());
		//info.setRetNumber(ctOrderInfo.getRetNumber());
		//info.setRetStatus(ctOrderInfo.getRetStatus());
		//info.setRetTime(ctOrderInfo.getRetTime());
		info.setShippingType(ctOrderInfo.getShippingType());
		info.setShopId(ctOrderInfo.getSId());
		info.setTel(ctOrderInfo.getTel().trim());
		info.setTotal(ctOrderInfo.getTotal());
		//info.setUId(ctOrderInfo.getUId());
		//info.setWare(ctOrderInfo.getWare());
		//info.setWareTime(ctOrderInfo.getWareTime());
		info.setZipcode(ctOrderInfo.getZipcode().trim());
		List<JsonOrderGoods>  listForJson = new ArrayList<JsonOrderGoods>();
		for (CtOrderGoods o : ctOrderInfo.getOrderGoods()) {
			JsonOrderGoods jsonGoods = new JsonOrderGoods();
			jsonGoods.setCouponId(o.getCouponId());
			jsonGoods.setGId(o.getGId());
			jsonGoods.setGNumber(o.getGNumber() == null || o.getGNumber().toString().equals("0") ? null : Long.valueOf(o.getGNumber()));
			jsonGoods.setGParNumber(o.getGParNumber() == null || o.getGParNumber().toString().equals("0") ? null : o.getGParNumber());
			jsonGoods.setGParPrice(o.getGParPrice() == null || o.getGParPrice().equals("") ? "" : sub(Double.valueOf(o.getGParPrice()),0)+"");
			jsonGoods.setGPrice(o.getGPrice());
			jsonGoods.setGSubtotal(o.getGSubtotal());
			jsonGoods.setIsParOrSim(o.getIsParOrSim());
			//jsonGoods.setOrderGoodsId(o.getOrderGoodsId());
			jsonGoods.setCId(o.getCtGoods().getCId());
			jsonGoods.setGSn(o.getCtGoods().getGSn());
			//jsonGoods.setPack(o.getPack());
			//jsonGoods.setOrderId(o.getOrderId());
			listForJson.add(jsonGoods);
		}
		info.setKfDsc(ctOrderInfo.getKfDsc());
		info.setOrderGoodsList(listForJson);
		return info;
	}
	 /** 
	  * 提供精确的减法运算。 
	  * @param v1 被减数 
	  * @param v2 减数 
	  * @return 两个参数的差 
	  */
	 public static double sub(double v1, double v2) {
	  BigDecimal b1 = new BigDecimal(Double.toString(v1));
	  BigDecimal b2 = new BigDecimal(Double.toString(v2));
	  return (b1.subtract(b2)).doubleValue();
	 }
	 /** 
	  * 提供精确的乘法运算。 
	  * @param v1 被乘数 
	  * @param v2 乘数 
	  * @return 两个参数的积 
	  */
	 public static double mul(double v1, double v2) {
	  BigDecimal b1 = new BigDecimal(Double.toString(v1));
	  BigDecimal b2 = new BigDecimal(Double.toString(v2));
	  return (b1.multiply(b2)).doubleValue();
	 }
}

package org.ctonline.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.poi.ss.formula.functions.T;
import org.apache.struts2.ServletActionContext;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Pointcut;
import org.ctonline.po.basic.CtSystemInfo;

public class OSSAroundAdvice  implements MethodInterceptor {

		/**
		 * 后置增强统一讲oss的数据读取出来
		 * @param pjp
		 * @throws Throwable
		 */
	 public void doAround(JoinPoint  pjp) throws Throwable {  
		 	CtSystemInfo systemInfo = new CtSystemInfo();
//		 	systemInfo = (CtSystemInfo) ServletActionContext.getRequest().getSession().getAttribute("systemInfo");
//		 	if(systemInfo == null){
//		 		ServletActionContext.getRequest().getSession().setAttribute("systemInfo", systemInfo);
//		 	}
	   }  
	 /** 
	     * 获得一个对象各个属性的字节流 
	     */  
	 private Boolean isOk = false;
	    public void getProperty(Object entityName) throws Exception {  
	        Class c = entityName.getClass();  
	        Field field[] = c.getDeclaredFields();  
	        for (Field f : field) {  
	            Object v = invokeMethod(entityName, f.getName(), null); 
	            String yan = null;
	            
	            if(f.getType().getName().equals("java.lang.Long")){
	            	if(v != null && !v.toString().trim().equals(null)){
	            		yan = v.toString();
	            		String regex = "^(-?[1-9]\\d*\\.?\\d*)|(-?0\\.\\d*[1-9])|(-?[0])|(-?[0]\\.\\d*)$";
	            		if(yan.matches(regex)){
	            			isOk = true;
	            		} else {
	            			isOk = false;
	            			return;
	            		}
	            	}
	            	
	            } else if(f.getType().getName().equals("java.lang.String")){
	            	if(v != null && !v.toString().trim().equals(null)){
	            		if(v.toString().indexOf("http") >= 0){
	            			isOk = true;
	            		} else {
	            			String regEx="[`~#$^&+=|{}';'<>~#￥……&——+|{}【】‘；”“’、？]";   
	            			Pattern   p   =   Pattern.compile(regEx);      
	            			Matcher   m   =   p.matcher(v.toString().trim());
	            			if(!m.find()){
	            				isOk = true;
	            			} else {
	            				isOk = false;
	            				return;
	            			}
	            		}
	            	}
	            }else if(f.getType().getName().equals("java.lang.Integer")){
	            	
	            }else if(f.getType().getName().equals("java.util.Date")){
	            	
	            }else if(f.getType().getName().equals("java.lang.Double")){
	            	
	            }else{
	            	
	            }
	            //System.out.println(f.getName() + "\t" + v + "\t" + f.getType().getName());  
	        }  
	    } 
	    /** 
	     * 获得对象属性的值 
	     */  
	    @SuppressWarnings("unchecked")  
	    private static Object invokeMethod(Object owner, String methodName,  
	            Object[] args) throws Exception {  
	        Class ownerClass = owner.getClass();  
	        methodName = methodName.substring(0, 1).toUpperCase()  
	                + methodName.substring(1);  
	        Method method = null;  
	        try {  
	            method = ownerClass.getMethod("get" + methodName);  
	        } catch (SecurityException e) {  
	        } catch (NoSuchMethodException e) {  
	            return " can't find 'get" + methodName + "' method";  
	        }  
	        return method.invoke(owner);  
	    }  
	 public Object doHuan(ProceedingJoinPoint pjp) throws Throwable {
		//System.out.println("前");
		Object[] canshu = pjp.getArgs();
		for (int i = 0; i < canshu.length; i++) {
			Object obj = canshu[i];
			getProperty(obj);
		}
		
		if(isOk){
			Object result =  pjp.proceed();
			//System.out.println(result);
			//System.out.println("后");
			return result;
		} else {
			
			return "Exerror";
		}
	 }
	 
	 /**
		 * 修改业务逻辑切方法入点
		 */
		@Pointcut("execution(* org.ctonline.dao.*.impl.*.update*(..))")
		public void updateFilmCall(){ }
		
		
	 
	 @Around("updateFilmCall()")
	public Object invoke(MethodInvocation Invocation) throws Throwable {
		//System.out.println("up");
		Invocation.proceed();
		//System.out.println("down");
		return null;
	}
	 
	 
	 
	 public Object doHuanAdd(ProceedingJoinPoint pjp) throws Throwable {
			//System.out.println("前");
			Object[] canshu = pjp.getArgs();
			for (int i = 0; i < canshu.length; i++) {
				Object obj = canshu[i];
				getProperty(obj);
			}
			
			if(isOk){
				Object result =  pjp.proceed();
				//System.out.println(result);
				//System.out.println("后");
				return result;
			} else {
				
				return "Exerror";
			}
		 }
	 
	 public Object doHuanSave(ProceedingJoinPoint pjp) throws Throwable {
			//System.out.println("前");
			Object[] canshu = pjp.getArgs();
			for (int i = 0; i < canshu.length; i++) {
				Object obj = canshu[i];
				getProperty(obj);
			}
			
			if(isOk){
				Object result =  pjp.proceed();
				//System.out.println(result);
				//System.out.println("后");
				return result;
			} else {
				
				return "Exerror";
			}
		 }
	 public Object doHuanchange(ProceedingJoinPoint pjp) throws Throwable {
			//System.out.println("前");
			Object[] canshu = pjp.getArgs();
			for (int i = 0; i < canshu.length; i++) {
				Object obj = canshu[i];
				getProperty(obj);
			}
			
			if(isOk){
				Object result =  pjp.proceed();
				//System.out.println(result);
				//System.out.println("后");
				return result;
			} else {
				
				return "Exerror";
			}
		 }
}

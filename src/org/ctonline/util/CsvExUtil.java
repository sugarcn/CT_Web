package org.ctonline.util;

public class CsvExUtil {
	
	private String goodsSn;
	private String goodsName;
	private String goodsNum;
	
	public String getGoodsSn() {
		return goodsSn;
	}
	public void setGoodsSn(String goodsSn) {
		this.goodsSn = goodsSn;
	}
	public String getGoodsName() {
		return goodsName;
	}
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	public String getGoodsNum() {
		return goodsNum;
	}
	public void setGoodsNum(String goodsNum) {
		this.goodsNum = goodsNum;
	}
	@Override
	public String toString() {
		return "CsvExUtil [goodsSn=" + goodsSn + ", goodsName=" + goodsName
				+ ", goodsNum=" + goodsNum + "]";
	}
	
}

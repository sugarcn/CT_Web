package org.ctonline.util;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

public class Util {
	
	  /**

     * URL����
	 * @throws UnsupportedEncodingException 

     */

		public static String encodeUri(String s) throws UnsupportedEncodingException
		{
			String str="";
			str=URLEncoder.encode(s,"UTF-8");
			return str;
		}

         /**

         * �µ�md5ǩ����β��secret��

         * @param secret ��������APP_SECRET

         */

         public static String md5Signature(TreeMap<String, String> params, String secret) {

                 String result = null;

                 StringBuffer orgin = getBeforeSign(params, new StringBuffer(secret));

                 if (orgin == null)

                 return result;

                 //orgin.append(secret);

                 try {

                         MessageDigest md = MessageDigest.getInstance("MD5");

                         result = byte2hex(md.digest(orgin.toString().getBytes("utf-8")));

                 } catch (Exception e) {

                         throw new java.lang.RuntimeException("sign error !");

                 }

             return result;

         }

         /**

         * ������ת�ַ�

         */

         private static String byte2hex(byte[] b) {

                 StringBuffer hs = new StringBuffer();

                 String stmp = "";

                 for (int n = 0; n < b.length; n++) {

                         stmp = (java.lang.Integer.toHexString(b[n] & 0XFF));

                         if (stmp.length() == 1)

                                 hs.append("0").append(stmp);

                         else

                                 hs.append(stmp);

                 }

                 return hs.toString().toUpperCase();

         }

         /**

         * ��Ӳ���ķ�װ����

         */

         private static StringBuffer getBeforeSign(TreeMap<String, String> params, StringBuffer orgin) {

                 if (params == null)

                         return null;

                 Map<String, String> treeMap = new TreeMap<String, String>();

                 treeMap.putAll(params);

                 Iterator<String> iter = treeMap.keySet().iterator();
                 while (iter.hasNext()) {

                         String name = (String) iter.next();

                         orgin.append(name).append(params.get(name));

                 }
                 return orgin;

         }

         /**���ӵ�TOP����������ȡ���*/

         public static String getResult(String urlStr, String content) {

                 URL url = null;

                 HttpURLConnection connection = null;
                
                 try {

                         url = new URL(urlStr);

                         connection = (HttpURLConnection) url.openConnection();

                         connection.setDoOutput(true);

                         connection.setDoInput(true);

                         connection.setRequestMethod("POST");

                         connection.setUseCaches(false);
                         
                         connection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");

                         connection.connect();

 

                         DataOutputStream out = new DataOutputStream(connection.getOutputStream());

                         out.write(content.getBytes("utf-8"));

                         out.flush();

                         out.close();

 

                         BufferedReader reader =

                         new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));

                         StringBuffer buffer = new StringBuffer();

                         String line = "";

                         while ((line = reader.readLine()) != null) {

                                 buffer.append(line);

                         }

                         reader.close();

                         return buffer.toString();

                 } catch (IOException e) {

                         e.printStackTrace();

                 } finally {

                         if (connection != null) {

                                 connection.disconnect();

                         }

                 }

                 return null;

         }
         
         public static String getRandom() {     //根据A a 0 生成6位随机唯一字符串
	      //
	         final Random r = new Random();
	         final char[] arr = new char[62];
	         for (int q = 0; q < 26; q++) {
	             arr[q] = (char) ('A' + q);
	         }
	         for (int q = 26; q < 52; q++) {
	             arr[q] = (char) ('a' + q - 26);
	         }
	         for (int q = 52; q < arr.length; q++) {
	             arr[q] = (char) ('0' + q - 52);
	         }
	         // System.out.println(Arrays.toString(arr));
	         //
	         // 
	         final char[] code = new char[6];
	         for (int i = 0; i < code.length; i++) {
	             int index;
	             do {
	                 final int rnd = r.nextInt();
	                 index = (rnd < 0 ? -rnd : rnd) % arr.length;
	                 //
	                 // 如果碰到'\0'证明取到了上次相同的元素，应当重新取，直到不重复
	             } while (arr[index] == '\0');
	             code[i] = arr[index];
	             //
	             // 使用过一次设定的'\0'，下次就不能用了
	             arr[index] = '\0';
	         }
	         return Arrays.toString(code);
         }
}

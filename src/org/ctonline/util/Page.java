package org.ctonline.util;



public class Page {
	
	private static final int DEFAULT_PAGE_SIZE = 12;  
    private static final int DEFAULT_CURRENT_PAGE = 1;  
    private int currentPage;// ��ǰҳ
    private int pageSize;// ÿҳ��ʾ����
    private Long totalCount = 0L;// ���� 
    private Long totalPage;//��ҳ��
    private int prePage;
    private int nextPage;
    private String pageStr;
    private String pageGoodsStr; // 商品分页控制器
    private String comStr;
    
    public int getPrePage() {
		return prePage;
	}
	public void setComStr(String getComStr) {
		this.comStr = getComStr;
	}
	public void setPrePage(int prePage) {
		this.prePage = prePage;
	}
	public int getNextPage() {
		return nextPage;
	}
	public void setNextPage(int nextPage) {
		this.nextPage = nextPage;
	}
	public void setTotalPage(Long totalPage) {
		this.totalPage = totalPage;
        if (this.currentPage<=1)
        	this.prePage=1;
        else
        	this.prePage=this.currentPage-1;
        if (this.currentPage>=this.totalPage)
        	this.nextPage=this.currentPage;
        else
        	this.nextPage=this.currentPage+1;
	}
	public Page(int currentPage, int pageSize) {  
        this.currentPage = currentPage;  
        this.pageSize = pageSize;  
    }  
    public Page(int currentPage) {  
        this.currentPage = currentPage;  
        this.pageSize = DEFAULT_PAGE_SIZE;  
    }  
  
    public Page() {  
        this.currentPage = DEFAULT_CURRENT_PAGE;  
        this.pageSize = DEFAULT_PAGE_SIZE;  
    } 
    //TotalPage num
    public Long getTotalPage() {  
    	  
        long remainder = totalCount % this.getPageSize();  
  
        if (0 == remainder) {  
            return totalCount / this.getPageSize();  
        }  
  
        return totalCount / this.getPageSize() + 1;  
    } 
    public int getFirstIndex() {  
        return pageSize * (currentPage - 1);  
    }  
    public boolean hasPrevious() {  
        return currentPage > 1;  
    }  
  
    public boolean hasNext() {  
        return currentPage < getTotalPage();  
    }  
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public Long getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}
	
	public String getPageStr() {
		StringBuffer sb = new StringBuffer();
		if(totalPage >= 1){

			//显示记录数,和当前是第几页
			sb.append("<div class=\"pager_d\">");
			sb.append("<ul>");
			sb.append("<input type=\"hidden\" id=\"totalpage\" value=\""+ totalPage +"\"/>");
			sb.append("<li class=\"gongjiye\">共&nbsp<span>"+ currentPage +"/"+ totalPage +"&nbsp</span>页<li>");

			//上一页跳转,当前页数等等于一就不能进行上一页操作
			sb.append("<li class=\"f_page\">");
			if(currentPage == 1){
				sb.append("<a href=\"javascript:void()\" >上一页</a>");
			}else{
				sb.append("<a href=\"javascript:upAndDownPage("+ (currentPage - 1) +")\">上一页</a>");
			}
			sb.append("</li>");
			sb.append("<li class=\"shu_page\">");
			//循环输出可选的分页页数和跳转
			int showTag = 5;//分页标签显示数量
			int startTag = 1;
			
			if (currentPage > showTag) {
				startTag = currentPage - 1;
			}
			
			int endTag = startTag + showTag - 1;
			if(currentPage >= 6){
				sb.append("<a href=\"javascript:upAndDownPage("+ 1 +")\">1</a>");
				sb.append("<a href=\"javascript:upAndDownPage("+ 2 +")\">2</a>");
				sb.append("<span class=\"kong_d\">...</span>");
			}
			for (int i = startTag; i <= totalPage && i <= endTag; i++) {
				 if (currentPage == i){
					sb.append("<a href=\"javascript:upAndDownPage("+ i +")\" class=\"hover\">"+ i +"</a>");
				}else{
					sb.append("<a href=\"javascript:upAndDownPage("+ i +")\">"+ i +"</a>");
				}
			}
			
			if(totalPage > 5 && currentPage != totalPage){
				sb.append("<span class=\"kong_d\">...</span>");
				sb.append("<a href=\"javascript:upAndDownPage("+ totalPage +")\">"+ totalPage +"</a>");
			}
			
			sb.append("</li>");
			sb.append("<li class=\"n_page\">");
			
			//下一页跳转,当前页等于等于最大页数时不能进行下一页操作
			if(currentPage == totalPage){
				sb.append("<a href=\"javascript:void()\" >下一页</a>");
			}else{
				sb.append("<a href=\"javascript:upAndDownPage("+ (currentPage + 1) +")\">下一页</a>");
			}
			sb.append("<input type=\"hidden\" id=\"totlePageHid\" value=\""+totalPage+"\" />");
			sb.append("</li>");
			//进行指定分页页面跳转
			if(totalPage > 1){
				sb.append("<li class=\"diaozhuaye\"><i>跳到第</i><input id=\"tarpage\"  onblur=\"javascript: var num=$('#tarpage').val();if(num != '' && num != null){if(num > 0 && num != 0){ var totlePage = $('#totlePageHid').val(); if(num>totlePage){alert('超出总页数');$('#tarpage').val('');}} else {alert('页码输入错误');$('#tarpage').val('');}} \" name=\"ye\" type=\"text\" /><i>页</i> <a onclick=\"upAndDownPage(this.parentNode.childNodes[1].value)\" href=\"javascript:void();\">确定</a></li>");
			} else {
				sb.append("<li class=\"diaozhuaye\"><i>跳到第</i><input  disabled=\"disabled\"  id=\"tarpage\" name=\"ye\" type=\"text\" /><i>页</i> <a href=\"javascript:void();\">确定</a></li>");
			}
			sb.append("</ul>");
			sb.append("</div>");
		}
		
		pageGoodsStr = sb.toString();
		
		return pageGoodsStr;
	}
	
	public void setPageGoodsStr(String pageGoodsStr) {
		this.pageGoodsStr = pageGoodsStr;
	}
	
	
	public String getComStr(){
		StringBuffer sb = new StringBuffer();
		if(totalPage >= 1){
			
			sb.append("<div class='page'>");
			if(currentPage == 1){
				sb.append("共<i>["+totalPage+"]</i>条记<a href=\'javascript:upAndDownPage("+ 1 +")\'>首页</a><a href=\'javascript:void();\'>上一页</a><font");
			} else {
				sb.append("共<i>["+totalPage+"]</i>条记<a href=\'javascript:upAndDownPage("+ 1 +")\'>首页</a><a href=\"javascript:upAndDownPage("+ (currentPage - 1) +")\">上一页</a><font");
			}
			sb.append("color='#ff6600'> "+currentPage +" </font><a");
			if(currentPage == totalPage){
				sb.append("href=\"javascript:void();\">下一页</a><a");
			} else {
				sb.append("href=\"javascript:upAndDownPage("+ (currentPage + 1) +");\">下一页 </a><a ");
			}
			sb.append(" href=\"javascript:upAndDownPage("+ totalPage +");\"> 尾页 </a> 跳转到<select class=\"input_out\" onchange=\"javascript:upAndDownPage(this.value)\">");
			for (int i = 0; i < totalPage; i++) {
				sb.append("<option value='"+i+"' selected>"+i+"</option>");
			}
			sb.append("</select>");
			sb.append("</div>");
		}
		comStr = sb.toString();
		return comStr;
	}
//	<div class="page">
//	共<i>[4]</i>条记<a href="/">首页</a><a href="/">上一页</a><font
//		color='#ff6600'>1</font><a
//		href="javascript:window.alert('已到尾页');">下一页</a><a
//		href="javascript:window.alert('已到尾页');">尾页</a> 跳转到<select
//		class="input_out"
//		onchange="window.location.href='/system/askList.asp?atype=2&page='+this.value"><option
//			value='1' selected>1</option>
//	</select>
//</div>
	
	/*
	 * 商品分分页控制器
	 */
	public String getPageGoodsStr() {
		StringBuffer sb = new StringBuffer();
		if(totalPage >= 1){

			//显示记录数,和当前是第几页
			sb.append("<div class=\"pager_d\">");
			sb.append("<ul>");
			sb.append("<input type=\"hidden\" id=\"totalpage\" value=\""+ totalPage +"\"/>");
			sb.append("<li class=\"gongjiye\">共&nbsp<span>"+ currentPage +"/"+ totalPage +"&nbsp</span>页<li>");

			//上一页跳转,当前页数等等于一就不能进行上一页操作
			sb.append("<li class=\"f_page\">");
			if(currentPage == 1){
				sb.append("<a href=\"javascript:void()\" >上一页</a>");
			}else{
				sb.append("<a href=\"javascript:upAndDownPage("+ (currentPage - 1) +")\">上一页</a>");
			}
			sb.append("</li>");
			sb.append("<li class=\"shu_page\">");
			//循环输出可选的分页页数和跳转
			int showTag = 5;//分页标签显示数量
			int startTag = 1;
			
			if (currentPage > showTag) {
				startTag = currentPage - 1;
			}
			
			int endTag = startTag + showTag - 1;
			if(currentPage >= 6){
				sb.append("<a href=\"javascript:upAndDownPage("+ 1 +")\">1</a>");
				sb.append("<a href=\"javascript:upAndDownPage("+ 2 +")\">2</a>");
				sb.append("<span class=\"kong_d\">...</span>");
			}
			for (int i = startTag; i <= totalPage && i <= endTag; i++) {
				 if (currentPage == i){
					sb.append("<a href=\"javascript:upAndDownPage("+ i +")\" class=\"hover\">"+ i +"</a>");
				}else{
					sb.append("<a href=\"javascript:upAndDownPage("+ i +")\">"+ i +"</a>");
				}
			}
			
			if(totalPage > 5 && currentPage != totalPage){
				sb.append("<span class=\"kong_d\">...</span>");
				sb.append("<a href=\"javascript:upAndDownPage("+ totalPage +")\">"+ totalPage +"</a>");
			}
			
			sb.append("</li>");
			sb.append("<li class=\"n_page\">");
			
			//下一页跳转,当前页等于等于最大页数时不能进行下一页操作
			if(currentPage == totalPage){
				sb.append("<a href=\"javascript:void()\" >下一页</a>");
			}else{
				sb.append("<a href=\"javascript:upAndDownPage("+ (currentPage + 1) +")\">下一页</a>");
			}
			sb.append("<input type=\"hidden\" id=\"totlePageHid\" value=\""+totalPage+"\" />");
			sb.append("</li>");
			//进行指定分页页面跳转
			if(totalPage > 1){
				sb.append("<li class=\"diaozhuaye\"><i>跳到第</i><input id=\"tarpage\"  onblur=\"javascript: var num=$('#tarpage').val();if(num != '' && num != null){if(Number(num) > 0 && Number(num) != 0){ var totlePage = $('#totlePageHid').val(); if(Number(num)>Number(totlePage)){alert('超出总页数');$('#tarpage').val('');}} else {alert('页码输入错误');$('#tarpage').val('');}} \" name=\"ye\" type=\"text\" /><i>页</i> <a onclick=\"upAndDownPage(this.parentNode.childNodes[1].value)\" href=\"javascript:void();\">确定</a></li>");
			} else {
				sb.append("<li class=\"diaozhuaye\"><i>跳到第</i><input  disabled=\"disabled\"  id=\"tarpage\" name=\"ye\" type=\"text\" /><i>页</i> <a href=\"javascript:void();\">确定</a></li>");
			}
			sb.append("</ul>");
			sb.append("</div>");
		}
		
		pageGoodsStr = sb.toString();
		
		return pageGoodsStr;
	}
	
	public void setPageStr(String pageStr) {
		this.pageStr = pageStr;
	}
	public static void main(String[] args) {
		Page page = new Page();
		page.setTotalCount(30L);
		page.setTotalPage(5L);
		//System.out.println(page.getTotalCount());
		String pa = page.getPageGoodsStr();
	}
}

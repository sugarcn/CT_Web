package org.ctonline.util;

import java.util.ArrayList;
import java.util.List;


public class JsonOrderInfo {
	private Long orderId;
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	private String orderSn;
	//private String UId;
	private String status;
	private String consignee;
	
	private String countryName;
	private String provinceName;
	private String cityName;
	private String districtName;
	private String address;
	
	private String payTime;
	
	private String kfDsc;
	
	public String getKfDsc() {
		return kfDsc;
	}
	public void setKfDsc(String kfDsc) {
		this.kfDsc = kfDsc;
	}
	public String getPayTime() {
		return payTime;
	}
	public void setPayTime(String payTime) {
		this.payTime = payTime;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getProvinceName() {
		return provinceName;
	}
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getDistrictName() {
		return districtName;
	}
	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}
	public void setConsignee(String consignee) {
		this.consignee = consignee;
	}
	private String zipcode;
	private String tel;
	private String shippingType;
	//private String invoiceType;
	//private String invoice;
	private String pay;
	private String couponId;
	private String freight;
	private String total;
	//private String discount;
	private String dsc;
	private String orderTime;
	private Byte exId;
	private String exNo;
	private String delivertime;
	//private String isokTime;
	//private Long evaId;
	//private Double retNumber;
	//private String retTime;
	//private String retStatus;
	//private String ware;
	//private String wareTime;
	//private String manage;
	//private String manageTime;
	//private String finance;
	//private String financeTime;
	private Short shopId;
	//private String orderIp;
	private Boolean shoppingTypeCollect;
	
	private List<JsonOrderGoods> orderGoodsList = new ArrayList<JsonOrderGoods>();
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<JsonOrderGoods> getOrderGoodsList() {
		return orderGoodsList;
	}
	public void setOrderGoodsList(List<JsonOrderGoods> orderGoodsList) {
		this.orderGoodsList = orderGoodsList;
	}
	public String getOrderSn() {
		return orderSn;
	}
	public void setOrderSn(String orderSn) {
		this.orderSn = orderSn;
	}
	public String getConsignee() {
		return consignee;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getShippingType() {
		return shippingType;
	}
	public void setShippingType(String shippingType) {
		this.shippingType = shippingType;
	}
	public String getPay() {
		return pay;
	}
	public void setPay(String pay) {
		this.pay = pay;
	}
	public String getCouponId() {
		return couponId;
	}
	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}
	public String getFreight() {
		return freight;
	}
	public void setFreight(String freight) {
		this.freight = freight;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getDsc() {
		return dsc;
	}
	public void setDsc(String dsc) {
		this.dsc = dsc;
	}
	public String getOrderTime() {
		return orderTime;
	}
	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}
	public Byte getExId() {
		return exId;
	}
	public void setExId(Byte exId) {
		this.exId = exId;
	}
	public String getExNo() {
		return exNo;
	}
	public void setExNo(String exNo) {
		this.exNo = exNo;
	}
	public String getDelivertime() {
		return delivertime;
	}
	public void setDelivertime(String delivertime) {
		this.delivertime = delivertime;
	}
	public Short getShopId() {
		return shopId;
	}
	public void setShopId(Short shopId) {
		this.shopId = shopId;
	}
	public Boolean getShoppingTypeCollect() {
		return shoppingTypeCollect;
	}
	public void setShoppingTypeCollect(Boolean shoppingTypeCollect) {
		this.shoppingTypeCollect = shoppingTypeCollect;
	}
	
}

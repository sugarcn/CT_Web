package org.ctonline.util;

import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.ctonline.manager.basic.CtLoginOperationManager;
import org.ctonline.manager.user.CtUserManager;
import org.ctonline.po.basic.CtLoginOperation;
import org.ctonline.po.user.CtUser;
import org.springframework.stereotype.Component;
/**
 * 增加,修改,删除日志
 * @author lxk
 *
 */
@Aspect
@Component
public class LogAspect
{
  private CtLoginOperation log;
  private CtLoginOperationManager loginOperation;
  private CtUserManager userManager;
  private static Logger logger = Logger.getLogger("CtLog");
  private static int in = 1;
  
  
  public CtUserManager getUserManager() {
	return userManager;
}


public void setUserManager(CtUserManager userManager) {
	this.userManager = userManager;
}


//@Around("execution(* org.ctonline.manager.user.impl.*.update*(..))")
  public Object aroundMethodUpdate(ProceedingJoinPoint pjd) {
	  Object result = null;
	try {
		result = null;
		  if(loginOperation != null){
			  Long userId = null;
		 		 CtUser user = (CtUser)ServletActionContext.getRequest().getSession().getAttribute("userinfosession");  
		 		 if(user == null){//没有管理员登录  
		 			userId = -1L;
		 			return null;
		 		 } else{
		 			userId = user.getUId();
		 		 }
		 		 
			  
			  //判断参数  
			  if(pjd.getArgs() == null){//没有参数  
				  return null;  
			  }  
			  
			  user = userManager.findAliUserId(userId.toString());
			  
			  result = pjd.proceed();
			  
			  for (int i = 0; i < pjd.getArgs().length; i++) {
				  System.out.println(pjd.getArgs()[i]);
			  }
			  System.out.println(pjd.getSignature().getName());
			  //获取方法名  
			  String methodName = pjd.getSignature().getName();  
			  
			  //获取操作内容  
			  String opContent = adminOptionContent(pjd.getArgs(), methodName);  
			  Boolean isRan = loginOperation.findDescEs(opContent);
			  if(isRan){
				  //创建日志对象  
				  log = new CtLoginOperation();  
				  log.setUId(userId.intValue());//设置管理员id  
				  log.setLogTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));//操作时间  
				  log.setLogModule(methodName);//操作方法 
				  log.setLogDesc(opContent);
				  log.setLogClass(pjd.getThis().toString());
				  log.setLogIp(getIpAddress(ServletActionContext.getRequest()));
				  log.setLogState("用户操作修改成功");
				  loginOperation.insertLog(log);//添加日志  
				  
				  logger.info(opContent);
			  }
			  return result;
		  }
	} catch (Exception e) {
		e.printStackTrace();
	} catch (Throwable e) {
		e.printStackTrace();
	}
     return result;
//      String methodName = pjd.getSignature().getName();
//      //执行目标方法
//      try {
//          //前置通知
//          System.out.println("The method " + methodName + " begins with " + Arrays.asList(pjd.getArgs()));
//          result = pjd.proceed();
//          //返回通知
//          System.out.println("The method " + methodName + " ends with " + Arrays.asList(pjd.getArgs()));
//      } catch (Throwable e) {
//          //异常通知
//          System.out.println("The method " + methodName + " occurs expection : " + e);
//          throw new RuntimeException(e);
//      }
//      //后置通知
//      System.out.println("The method " + methodName + " ends");
  }
  
  
  /**
   * 在方法出现异常时会执行的代码
   * 可以访问到异常对象，可以指定在出现特定异常时在执行通知代码
   */
     @AfterThrowing(value="execution(* org.ctonline.manager.*.impl.*.*(..))", throwing="ex")
     public void afterThrowing(JoinPoint joinPoint, Exception ex) throws Exception {
    	 
    	String userName = "";
    	CtUser user = (CtUser)ServletActionContext.getRequest().getSession().getAttribute("userinfosession");  
    	if(user == null){//没有管理员登录  
    	 userName = "未登录";
    	} else{
    	 userName = user.getUUsername();
    	}
    	//判断参数  
    	if(joinPoint.getArgs() == null){//没有参数  
    	 return ;  
    	} 
    	String methodName = joinPoint.getSignature().getName();
    	
    	//获取操作内容  
    	String opContent = adminOptionContent(joinPoint.getArgs(), methodName);  
    	
    	
//    	Boolean isRan = loginOperation.findDescEs(opContent);
//    	if(isRan){
//    	 //创建日志对象  
//    	 log = new CtLoginOperation();  
//    	 log.setUId(adminUserId.intValue());//设置管理员id  
//    	 log.setLogTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));//操作时间  
//    	 log.setLogModule(methodName);//操作方法 
//    	 log.setLogDesc(ex.getMessage());
//    	 log.setLogClass(joinPoint.getThis().getClass().getSimpleName().substring(0, joinPoint.getThis().getClass().getSimpleName().indexOf("$$")));
//    	 log.setLogIp(getIpAddress(ServletActionContext.getRequest()));
//    	 log.setLogState("失败");
//    	 loginOperation.addLog(log);//添加日志  
//    	}
    	
    	logger.info("异常："+ex.getMessage() +",操作数据:" + opContent + ", 操作失败, 用户：" + userName);
    	System.out.println("The method " + methodName + " occurs exception: " + ex);
     }
  
  @After("execution(* org.ctonline.manager.*.impl.*.save*(..))")
  public void afterMethodSave(JoinPoint joinpoint){
	  
	  
	  try {
		Long userId = null;
		  CtUser user = (CtUser)ServletActionContext.getRequest().getSession().getAttribute("userinfosession");  
		  if(user == null){//没有管理员登录  
		  userId = -1L;
		 user = new CtUser();
		 user.setUUsername("用户未登录");
		  } else{
		  userId = user.getUId();
		  }
		  
		   //判断参数  
		   if(joinpoint.getArgs() == null){//没有参数  
		    return ;  
		   }  
		   
		   //获取方法名  
		   String methodName = joinpoint.getSignature().getName();  
		   
		   
		 //获取日志操作详细
		   String targetName = joinpoint.getTarget().getClass().getName();  
		   Object[] arguments = joinpoint.getArgs();  
		   Class targetClass = Class.forName(targetName);  
		   Method[] methods = targetClass.getMethods();
		   String operationType = "";
		   String operationName = "";
		   try {
			for (Method method : methods) {  
			       if (method.getName().equals(methodName)) {  
			          Class[] clazzs = method.getParameterTypes();  
			           if (clazzs.length == arguments.length) {  
			               operationType = method.getAnnotation(Logg.class).operationType();
			               operationName = method.getAnnotation(Logg.class).operationName();
			               break;  
			          }  
			      }  
			  }
			} catch (Exception e) {
				e.printStackTrace();
				operationType = "";
				operationName = "";
			}
			  
			if(operationType.equals("") || operationName.equals("")){
			    return;
			}
		   
		   
			JSONArray json = JSONArray.fromObject(joinpoint.getArgs());
		    List<Map<String,Object>> mapListJson = (List)json; 
		    Map<String,Object> m = new HashMap<String, Object>();
		    for (int i = 0; i < mapListJson.size(); i++) {  
		    	try {
					Map<String,Object> obj=mapListJson.get(i);  
					  
					for(Entry<String,Object> entry : obj.entrySet()){  
					    String strkey1 = entry.getKey();  
					    Object strval1 = entry.getValue();  
//                System.out.println("KEY:"+strkey1+"  -->  Value:"+strval1+"\n");
					    if(strval1 != null && !strval1.toString().equals("") && !strval1.toString().equals("null") && !strval1.toString().equals("0")){
					    	
					    	strval1 = encryptionDate(strkey1, strval1);
			            	
					    	m.put(strkey1, strval1);
					    }
					}
					json = JSONArray.fromObject(m);
				} catch (Exception e) {
					//e.printStackTrace();
				}   
		    }  
		    json = JSONArray.fromObject(m);
			String opContent = json.toString();
			
			
		   //创建日志对象  
		   log = new CtLoginOperation();  
		   log.setUId(userId.intValue());//设置管理员id  
		   log.setLogTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));//操作时间  
		   log.setLogModule(methodName + (operationName.equals("") ? "" : "_" + operationName));//操作方法 
		   log.setLogDesc(opContent);
		   log.setLogClass(joinpoint.getThis().toString());
		   log.setLogIp(getIpAddress(ServletActionContext.getRequest()));
		   log.setLogState("用户操作-新增");
		   log.setLogType("1");
		   logger.info(fomatLogStr(user.getUUsername(), log.getLogIp(), log.getLogTime(), operationType, operationName));
		   logger.info("新增数据：" + opContent);
		   loginOperation.insertLog(log);//添加日志  
	} catch (ClassNotFoundException e) {
		e.printStackTrace();
	} catch (SecurityException e) {
		e.printStackTrace();
	}
  }


  /**
   * 敏感字段加密处理
   * @param strkey1
   * @param strval1
   * @return 加密后的值
   */
private Object encryptionDate(String strkey1, Object strval1) {
	if(strkey1.equals("UMb")){
		if(strkey1.toString().length() >= 9){
			strval1 = strval1.toString().substring(0, 3) + "***" + strval1.toString().substring(8, strval1.toString().length());
		} else {
			strval1 = strval1.toString().substring(0, 2) + "***";
		}
	}
	if(strkey1.equals("UQq")){
		if(strval1.toString().length() >= 4){
			strval1 = strval1.toString().substring(0, 3) + "***";
		} else {
			strval1 = "***";
		}
	}
	if(strkey1.equals("UWeibo")){
		if(strval1.toString().length() >= 4){
			strval1 = strval1.toString().substring(0, 3) + "***";
		} else {
			strval1 = "***";
		}
	}
	if(strkey1.equals("UTb")){
		if(strval1.toString().length() >= 4){
			strval1 = strval1.toString().substring(0, 3) + "***";
		} else {
			strval1 = "***";
		}
	}
	if(strkey1.equals("UCode")){
		strval1 = "***";
	}
	if(strkey1.equals("UWeixinloginUserid")){
		if(strval1 != null && !strval1.equals("") && strval1.toString().length() > 1){
			strval1 = strval1.toString().substring(0, 5) + "********" + strval1.toString().substring(18, strval1.toString().length());
		}
	}
	if(strkey1.equals("UPassword")){
		if(strval1 != null && !strval1.equals("") && strval1.toString().length() > 5){
			strval1 = strval1.toString().substring(0, 5) + "*********" + strval1.toString().substring(20, strval1.toString().length());
		}
	}
	return strval1;
}
  
  public String fomatLogStr(String userName, String ip, String time, String type, String typeName){
	  String str = "用户："+userName + "于" + ip + "的" + time + "进行了" + type + typeName + "的操作。";
	  return str;
  }
  
  @After("execution(* org.ctonline.manager.*.impl.*.update*(..))")
  public void afterMethodUpdate(JoinPoint joinpoint){
	  
	  
		  try {
			Long userId = null;
				 CtUser user = (CtUser)ServletActionContext.getRequest().getSession().getAttribute("userinfosession");  
				 if(user == null){//没有管理员登录  
					userId = -1L;
				 	 user = new CtUser();
				 	 user.setUUsername("用户未登录");
				 } else{
					userId = user.getUId();
				 }
				 
			  
			  //判断参数  
			  if(joinpoint.getArgs() == null){//没有参数  
				  return ;  
			  }  
			  
			  for (int i = 0; i < joinpoint.getArgs().length; i++) {
				  System.out.println(joinpoint.getArgs()[i]);
			  }
			  System.out.println(joinpoint.getSignature().getName());
			  //获取方法名  
			  String methodName = joinpoint.getSignature().getName();  
			  
			//获取日志操作详细
			   String targetName = joinpoint.getTarget().getClass().getName();  
			   Object[] arguments = joinpoint.getArgs();  
			   Class targetClass = Class.forName(targetName);  
			   Method[] methods = targetClass.getMethods();
			   String operationType = "";
			   String operationName = "";
			   try {
				for (Method method : methods) {  
				       if (method.getName().equals(methodName)) {  
				          Class[] clazzs = method.getParameterTypes();  
				           if (clazzs.length == arguments.length) {  
				               operationType = method.getAnnotation(Logg.class).operationType();
				               operationName = method.getAnnotation(Logg.class).operationName();
				               break;  
				          }  
				      }  
				  }
				} catch (Exception e) {
					e.printStackTrace();
					operationType = "";
					operationName = "";
				}
				  
				if(operationType.equals("") || operationName.equals("")){
				    return;
				}
			   
			   
				JSONArray json = JSONArray.fromObject(joinpoint.getArgs());
			    List<Map<String,Object>> mapListJson = (List)json; 
			    Map<String,Object> m = new HashMap<String, Object>();
			    for (int i = 0; i < mapListJson.size(); i++) {  
			    	try {
						Map<String,Object> obj=mapListJson.get(i);  
						  
						for(Entry<String,Object> entry : obj.entrySet()){  
						    String strkey1 = entry.getKey();  
						    Object strval1 = entry.getValue();  
//	                System.out.println("KEY:"+strkey1+"  -->  Value:"+strval1+"\n");
						    if(strval1 != null && !strval1.toString().equals("") && !strval1.toString().equals("null") && !strval1.toString().equals("0")){
						    	
						    	strval1 = encryptionDate(strkey1, strval1);
				            	
						    	m.put(strkey1, strval1);
						    }
						}
						json = JSONArray.fromObject(m);
					} catch (Exception e) {
						//e.printStackTrace();
					}   
			    }  
			    json = JSONArray.fromObject(m);
				String opContent = json.toString();
			  
			  //创建日志对象  
			  log = new CtLoginOperation();  
			  log.setUId(userId.intValue());//设置管理员id  
			  log.setLogTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));//操作时间  
			  log.setLogModule(methodName+"_"+operationName);//操作方法 
			  log.setLogDesc(opContent);
			  log.setLogClass(joinpoint.getThis().toString());
			  log.setLogIp(getIpAddress(ServletActionContext.getRequest()));
			  log.setLogState("用户操作-修改");
			  log.setLogType("1");
			  logger.info(fomatLogStr(user.getUUsername(), log.getLogIp(), log.getLogTime(), operationType, operationName));
			  logger.info("修改数据：" + opContent);
			  loginOperation.insertLog(log);//添加日志  
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
  }
  @After("execution(* org.ctonline.manager.*.impl.*.delete*(..))")
  public void afterMethodDelete(JoinPoint joinpoint) throws Exception{
	  
	  
	  if(loginOperation != null){
		  Long userId = null;
	 		 CtUser user = (CtUser)ServletActionContext.getRequest().getSession().getAttribute("userinfosession");  
	 		 if(user == null){//没有管理员登录  
	 			userId = -1L;
	 		 } else{
	 			userId = user.getUId();
	 		 }
	 		 
		  //判断参数  
		  if(joinpoint.getArgs() == null){//没有参数  
			  return ;  
		  }  
		  
		  System.out.println(joinpoint.getSignature().getName());
		  //获取方法名  
		  String methodName = joinpoint.getSignature().getName();  
		  
		//获取日志操作详细
		   String targetName = joinpoint.getTarget().getClass().getName();  
		   Object[] arguments = joinpoint.getArgs();  
		   Class targetClass = Class.forName(targetName);  
		   Method[] methods = targetClass.getMethods();
		   String operationType = "";
		   String operationName = "";
		   try {
			for (Method method : methods) {  
			       if (method.getName().equals(methodName)) {  
			          Class[] clazzs = method.getParameterTypes();  
			           if (clazzs.length == arguments.length) {  
			               operationType = method.getAnnotation(Logg.class).operationType();
			               operationName = method.getAnnotation(Logg.class).operationName();
			               break;  
			          }  
			      }  
			  }
			} catch (Exception e) {
				e.printStackTrace();
				operationType = "";
				operationName = "";
			}
			  
			if(operationType.equals("") || operationName.equals("")){
			    return;
			}
		  
		  
		  String str = methodName+" [删除编号：";
		  for (int i = 0; i < joinpoint.getArgs().length; i++) {
			  System.out.println(joinpoint.getArgs()[i]);
			  str += joinpoint.getArgs()[i] + ", ";
		  }
		  
		  str += ", time: "+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		  str += "]";
		  
		  //获取操作内容  
		  String opContent = str;  
		  Boolean isRan = loginOperation.findDescEs(opContent);
		  if(isRan){
			  //创建日志对象  
			  log = new CtLoginOperation();  
			  log.setUId(userId.intValue());//设置管理员id  
			  log.setLogTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));//操作时间  
			  log.setLogModule(methodName);//操作方法 
			  log.setLogDesc(opContent);
			  log.setLogClass(joinpoint.getThis().toString());
			  log.setLogIp(getIpAddress(ServletActionContext.getRequest()));
			  log.setLogState("用户操作-删除");
			  log.setLogType("1");
			  logger.info(fomatLogStr(user.getUUsername(), log.getLogIp(), log.getLogTime(), operationType, operationName));
			  logger.info("删除数据详细：" + opContent);
			  loginOperation.insertLog(log);//添加日志  
		  }
	  }
  }
  
  
  public String adminOptionContent(Object[] args, String mname)
    throws Exception
  {
    if (args == null) {
      return null;
    }
    StringBuffer rs = new StringBuffer();
    rs.append(mname);
    String className = null;
    int index = 1;
    Object[] arrayOfObject;
    int j = (arrayOfObject = args).length;
    for (int i = 0; i < j; i++)
    {
      Object info = arrayOfObject[i];
      
      className = info.getClass().getName();
      className = className.substring(className.lastIndexOf(".") + 1);
      rs.append("[参数:" + index + ",类型: " + className + ",值: ");
      
      Method[] methods = info.getClass().getDeclaredMethods();
      Method[] arrayOfMethod1;
      int m = (arrayOfMethod1 = methods).length;
      String methodName;
      Object rsValue;
      for (int k = 0; k < m; k++)
      {
        Method method = arrayOfMethod1[k];
        methodName = method.getName();
        if (methodName.indexOf("get") != -1)
        {
          rsValue = null;
          try
          {
            rsValue = method.invoke(info, new Object[0]);
            if (rsValue != null) {
            	//隐藏铭感字段
            	if(methodName.equals("getUMb")){
            		if(rsValue.toString() != null && rsValue.toString() != "" && rsValue.toString().length() >= 9){
            			rsValue = rsValue.toString().substring(0, 3) + "***" + rsValue.toString().substring(8, rsValue.toString().length());
            		}
            	}
            	if(methodName.equals("getUQq")){
            		if(rsValue.toString() != null && rsValue.toString() != "" && rsValue.toString().length() >= 4){
            			rsValue = rsValue.toString().substring(0, 3) + "***";
            		}
            	}
            	if(methodName.equals("getUWeibo")){
            		if(rsValue.toString() != null && rsValue.toString() != "" && rsValue.toString().length() >= 4){
            			rsValue = rsValue.toString().substring(0, 3) + "***";
            		}
            	}
            	if(methodName.equals("getUTb")){
            		if(rsValue.toString() != null && rsValue.toString() != "" && rsValue.toString().length() >= 4){
            			rsValue = rsValue.toString().substring(0, 3) + "***";
            		}
            	}
              rs.append("(" + methodName + " : " + rsValue + ")");
            }
          }
          catch (Exception e) {System.out.println(e.getMessage());}
        }
      }
      rs.append("]");
      index++;
    }
    
    return rs.toString();
  }
  
  public CtLoginOperationManager getLoginOperation()
  {
    return this.loginOperation;
  }
  
  public void setLoginOperation(CtLoginOperationManager loginOperation)
  {
    this.loginOperation = loginOperation;
  }
  /** 
   * 获取请求主机IP地址,如果通过代理进来，则透过防火墙获取真实IP地址; 
   *  
   * @param request 
   * @return 
   * @throws IOException 
   */  
  public static String getIpAddress(HttpServletRequest request) {  
      // 获取请求主机IP地址,如果通过代理进来，则透过防火墙获取真实IP地址  

      String ip = request.getHeader("X-Forwarded-For");  

      if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
          if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
              ip = request.getHeader("Proxy-Client-IP");  
          }  
          if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
              ip = request.getHeader("WL-Proxy-Client-IP");  
          }  
          if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
              ip = request.getHeader("HTTP_CLIENT_IP");  
          }  
          if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
              ip = request.getHeader("HTTP_X_FORWARDED_FOR");  
          }  
          if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
              ip = request.getRemoteAddr();  
          }  
      } else if (ip.length() > 15) {  
          String[] ips = ip.split(",");  
          for (int index = 0; index < ips.length; index++) {  
              String strIp = (String) ips[index];  
              if (!("unknown".equalsIgnoreCase(strIp))) {  
                  ip = strIp;  
                  break;  
              }  
          }  
      }  
      return ip;  
  }

}

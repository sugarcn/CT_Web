package org.ctonline.util;

import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import org.ctonline.action.user.LoginAction;
import org.ctonline.po.basic.CtSystemInfo;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import com.opensymphony.xwork2.interceptor.MethodFilterInterceptor;

public class ValidateLogin extends MethodFilterInterceptor {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String doIntercept(ActionInvocation invo) throws Exception {
		// TODO Auto-generated method stub
		Map session =  invo.getInvocationContext().getSession();
		
		CtSystemInfo systemInfo = new CtSystemInfo();
		 ServletActionContext.getRequest().getSession().setAttribute("systemInfo", systemInfo);;
		if(invo.getAction().getClass().equals(LoginAction.class)){
			return invo.invoke();
		}
		Cookie[] cookies = ServletActionContext.getRequest().getCookies();
		String [] cooks = null;
		String uname ="";
		String passwd = "";
		if(cookies !=null){
			for(Cookie coo : cookies){
				String aa = coo.getValue();
				cooks = aa.split("==");
				if(cooks.length == 2){
					uname = cooks[0];
					passwd = cooks[1];
				}
			}
			if("".equals(uname) || "".equals(passwd)){
				if(session.get("userinfosession") != null){
					//System.out.println(session.get("userinfosession"));
					//return "login_user";
					return invo.invoke();
				}else {
					
					HttpServletRequest req = ServletActionContext.getRequest();
					String path = req.getRequestURI();
					String quertString = req.getQueryString();
					String [] p  = path.split("/");
						try {
							//System.out.println("path:" + path);  
							//System.out.println("referUrl:" + p[2]);
							if(quertString!=null && !quertString.equals("null")){
								session.put("referUrl", p[p.length] + "?" + quertString);
							} else {
								if(p[p.length].equals("order_check")){
									p[p.length]="goods_cart";
								}
								session.put("referUrl", p[p.length]);
							}
						} catch (Exception e) {
							//e.printStackTrace();
							//System.out.println("异常");
						} finally {
							//System.out.println(path);
						}
					return "login";
				}
			}else {
				return "login_user";
			}
		}else{
			if(session.get("userinfosession") != null){
				return "login_user";
			}else {
				HttpServletRequest req = ServletActionContext.getRequest();
				String path = req.getRequestURI();
				String [] p  = path.split("/");
				//System.out.println("path:" + path);  
		        //System.out.println("referUrl:" + p[2]);  
				session.put("referUrl", p[p.length]);
				return "login";
			}
		}
		
	}
}


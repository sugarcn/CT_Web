package org.ctonline.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;


public class kuaidi100
{
	
	public static void main(String[] agrs)
	{
		
//		String result = kuaiDi100("3a4c64496dd5610f","yuantong","880216089442198677","2","1","desc");
		//System.out.println(result);
		 
	}
	public static String kuaiDi100New(String key, String com, String nu, String show, String muti, String order){

		String url = "http://api.kuaidi100.com/api?id="+key+"&com="+com+"&nu="+nu+"&show="+show+"&muti="+muti+"&order="+order+"";  
		  String result = null;  
		  try {  
		   HttpClient httpClient  = new HttpClient();  
		   GetMethod getMethod = new GetMethod(url);  
		   httpClient.getParams().setParameter(  
		     HttpMethodParams.HTTP_CONTENT_CHARSET, "UTF-8");  
		     
		   getMethod.getParams().setParameter("http.method.retry-handler",  
		     new DefaultHttpMethodRetryHandler());  
		   int statusCode = httpClient.executeMethod(getMethod);  
		   if (statusCode == 200) {  
		    StringBuffer temp = new StringBuffer();  
		    InputStream in = getMethod.getResponseBodyAsStream();  
		    BufferedReader buffer = new BufferedReader(  
		      new InputStreamReader(in, "UTF-8"));  
		    for (String tempstr = ""; (tempstr = buffer.readLine()) != null;)  
		     temp = temp.append(tempstr+"\n");  
		    buffer.close();  
		    in.close();  
		    result = temp.toString().trim();  
		    System.out.println(result);  
		    return result;
		   } else {  
		    System.err.println((new StringBuilder("Can't get page:"))  
		      .append(url).append("#").append(  
		        getMethod.getStatusLine()).toString());  
		   }  
		  } catch (Exception e) {  
		   e.printStackTrace();  
		  } 
		  return null;
	}
	/**
	 * 
	 * 
	 * 
	 * @param key 授权key
	 * @param com 物流公司的代码
	 * @param nu  快递单号
	 * @param show 返回类型
	 * 返回类型： 
	 *         0：返回json字符串， 
	 *         1：返回xml对象， 
	 *         2：返回html对象， 
	 *         3：返回text文本。 
	 *	如果不填，默认返回json字符串。
	 * @param muti 返回信息数量
	 * 返回信息数量： 
	 * 		   1:返回多行完整的信息， 
	 * 		   0:只返回一行信息。 
	 *          不填默认返回多行。 
	 * @param order 排序
	 * 排序： 
	 * 		   desc：按时间由新到旧排列，
	 * 		   asc：按时间由旧到新排列。 
	 * 		         不填默认返回倒序（大小写不敏感）
	 */
	public static String kuaiDi100(String key, String com, String nu, String show, String muti, String order) {
		try
		{
			URL url= new URL("http://api.kuaidi100.com/api?id="+key+"&com="+com+"&nu="+nu+"&show="+show+"&muti="+muti+"&order="+order+"");
			URLConnection con=url.openConnection();
			 con.setAllowUserInteraction(false);
			   InputStream urlStream = url.openStream();
			   String type = con.guessContentTypeFromStream(urlStream);
			   String charSet=null;
			   if (type == null)
			    type = con.getContentType();

//			   if (type == null || type.trim().length() == 0 || type.trim().indexOf("text/html") < 0)
//			    return null;

			   if(type.indexOf("charset=") > 0)
			    charSet = type.substring(type.indexOf("charset=") + 8);

			   byte b[] = new byte[10000];
			   int numRead = urlStream.read(b);
			  String content = new String(b, 0, numRead,"UTF-8");
			   while (numRead != -1) {
			    numRead = urlStream.read(b);
			    if (numRead != -1) {
			     //String newContent = new String(b, 0, numRead);
			     String newContent = new String(b, 0, numRead, charSet);
			     content += newContent;
			    }
			   }
			   ////System.out.println("content:" + content);
			   urlStream.close();
			   return content;
		} catch (MalformedURLException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}

}

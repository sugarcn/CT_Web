package org.ctonline.config;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *版本：3.3
 *日期：2012-08-10
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
	
 *提示：如何获取安全校验码和合作身份者ID
 *1.用您的签约支付宝账号登录支付宝网站(www.alipay.com)
 *2.点击“商家服务”(https://b.alipay.com/order/myOrder.htm)
 *3.点击“查询合作者身份(PID)”、“查询安全校验码(Key)”

 *安全校验码查看时，输入支付密码后，页面呈灰色的现象，怎么办？
 *解决方法：
 *1、检查浏览器配置，不让浏览器做弹框屏蔽设置
 *2、更换浏览器或电脑，重新登录查询。
 */

public class AlipayConfig {
	
	//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
	// 合作身份者ID，以2088开头由16位纯数字组成的字符串
	//public static String partner = "2088001370082973";
	public static String partner = "2088221825235989";
	
	// 收款支付宝账号
	//public static String seller_email = "ctelec@139.com";
	public static String seller_email = "s@ctego.com";
	// 商户的私钥
	//public static String key = "dke3tcq56vj743sxsqatvifywnwlb1tb";
	public static String key = "pov3jeliqdzzb7t9o7so4jjozxnli6wo";

	//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
	

	// 调试用，创建TXT日志文件夹路径
	public static String log_path = "/usr/";

	// 字符编码格式 目前支持 gbk 或 utf-8
	public static String input_charset = "utf-8";
	
	// 签名方式 不需修改
	public static String sign_type = "MD5";
	
	
	//↓↓↓↓↓↓↓↓↓↓财付通 请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
	//收款方
	public static String spnameTen = "深圳市长亭电子有限公司";  

	//商户号
	public static String partnerTen = "1252012301";

	//密钥
	public static String keyTen = "c5c02b867cc1b7d185a2ceb9dc9773b4";
	
	//支付宝返回
	public static String notify_url = "http://127.0.0.1/CT_Web/alinotify";
	public static String return_url = "http://127.0.0.1/CT_Web/alireturn";
	//public static String notify_url = "http://www.ctego.com/alinotify";
	//public static String return_url = "http://www.ctego.com/alireturn";

	//阿里登录返回
	public static String aliLogin_return_url = "http://127.0.0.1/CT_Web/ali_Login_Result";
//	public static String aliLogin_return_url = "https://www.ctego.com/CT_Web/ali_Login_Result";
	
	//目标服务地址
	public static String target_service = "user.auth.quick.login";
	
	//财付通交易完成后跳转的URL
	//public static String return_urlTen = "http://127.0.0.1:8080/CT_Web/ten_return";

	//接收财付通通知的URL
	//public static String notify_urlTen = "http://127.0.0.1:8080/CT_Web/notify_ten";
	
	public static String return_urlTen = "https://www.ctego.com/ten_return";
	public static String notify_urlTen = "https://www.ctego.com/notify_ten";
	
	
	
	//↓↓↓↓↓↓↓↓↓↓微信支付  请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
	// 微信支付商户开通后 微信会提供appid和appsecret和商户号partner
//	public static String appid = "wx20d092d7dac249d5";
	public static String appid = "wxdeeaf559835b86ac";//易购新
//	public static String appid = "wx5cc18d0bb5153333";
//	public static String appsecret = "cac931764ee679a4b00abe35095cd7ec";
	public static String appsecret = "ca0b731b25b7b1f4266dd6b503ac9986";//易购新
//	public static String appsecret = "77a90150600887ea197786f78445178f";
	public static String partner_Weixin = "1434536402";//易购新
//	public static String partner_Weixin = "1251545501";
	// 这个参数partnerkey是在商户后台配置的一个32位的key,微信商户平台-账户设置-安全设置-api安全
//	public static String partnerkey = "szctdzCTego13823631220ctegoctego";
	public static String partnerkey = "ctegooCTego13823631220ctegoctego"; //易购新
	// openId 是微信用户针对公众号的标识，授权的部分这里不解释
	public static String openId = "o7ihfs0XQxYWLybAxR6b5z1P7Mmo";
	// 微信支付成功后通知地址 必须要求80端口并且地址不能带参数
	public static String notifyurl = "https://www.ctego.com"; // Key
	
	//网页支付成功的返回
	
	public static String paySuccessUrl = "https://www.ctego.com/weixinpaysc/paySuccess";
//		public static String paySuccessUrl = "http://liuxiaokang.ngrok.cc/CT_Web/weixinpaysc/paySuccess";
		
		public static String payGetOpenId = "http://127.0.0.1/CT_Web/weixinpay/index.jsp";
		
	
	//---------------ChinaPay 返回------------
	//后台交易接收Url
//	public static String BgRetUrl = "http://localhost:8080/CT_Web/chinapayreturn";
//	//页面交易接收Url
//	public static String PageRetUrl = "http://localhost:8080/CT_Web/chinapaynotify";
//	//提交地址
//	public static String postUrl = "https://payment.chinapay.com/pay/TransGet";
//	//配置文件地址
//	public static String pubk = "D:\\PgPubk.key";
//	public static String MerPrk1 = "D:\\MerPrk1.key";
	
	public static String MerId = "808080201308522";//商户号
	public static String BgRetUrl = "https://www.ctego.com/chinapayreturn";
	//页面交易接收Url
	public static String PageRetUrl = "https://www.ctego.com/chinapaynotify";
	//提交地址
	public static String postUrl = "https://payment.chinapay.com/pay/TransGet";
	//配置文件地址
	public static String pubk = "/usr/PgPubk.key";
	public static String MerPrk1 = "/usr/MerPrk1.key";
	
	//微信登录配置文件
	public static String weixinAppId = "wxdcdffa64194cf0f7";
	public static String weixinAppSecret = "8af07e6104cdb4ff7dc76cfc1f7fd5cc";
	
	public static String weixinResUrl = "https://www.ctego.com/weixinReslogin";
	//public static String weixinResUrl = "http://www.qqtest.com/CT_Web/weixinReslogin";
	

	
	//EDB 配置
	
	//调用API地址
	public static final String testUrl = "http://vip149.edb05.com/rest/index.aspx";
	//protected static String testUrl = "http://vip802.6x86.com/edb2/rest/openapi/test.aspx";
	//申请的appkey
	public static final String appkey = "e259ceae";
	//申请的secret
	public static final String secret="abef7e082b2c4e6c9da740cd201f3133";
	//申请的token
	public static final String token   ="ea232cfb061c45ddba8d8d7c275041af";
	//主帐号
	public static final String dbhost = "edb_a83682";
	//返回格式
	public static final String format = "xml";
	
}

package org.ctonline.dto.order;

import java.util.List;

import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.goods.CtRangePrice;

/***
 * 
 * 用于传递购物车页面所需要的数据
 * @author lxk
 *
 */
public class OrderCartDTO {

	//页面隐藏属性
	private Integer cartGId;
	private Integer isSample;
	private Integer cartGoodsType; //取值有三种 1代表只有样品 2代表只有批量 3代表两种都有
	private Integer cartGoodsSelNum; // 样品价格区间选中的序号 取值 1 ， 2 ， 3。。。。。
	private CtGoods goods;
	private Integer cartId;
	
	//页面显示的属性
	private String cartGName;
	private String cartGoodsImgUrl;
	private List<CtRangePrice> prices;
//	private String gunit = "pcs";
	private String cartSimNum = "0";
	private String cartParNum = "0";
	private String cartSimPrice = "0.00";
	private String cartParPrice = "0.00";
	private String cartCountNum = "0";
	private String cartCountPrice = "0.00";
	
	public Integer getCartId() {
		return cartId;
	}
	public void setCartId(Integer cartId) {
		this.cartId = cartId;
	}
	public CtGoods getGoods() {
		return goods;
	}
	public void setGoods(CtGoods goods) {
		this.goods = goods;
	}
	public Integer getCartGId() {
		return cartGId;
	}
	public void setCartGId(Integer cartGId) {
		this.cartGId = cartGId;
	}
	public Integer getIsSample() {
		return isSample;
	}
	public void setIsSample(Integer isSample) {
		this.isSample = isSample;
	}
	public Integer getCartGoodsType() {
		return cartGoodsType;
	}
	public void setCartGoodsType(Integer cartGoodsType) {
		this.cartGoodsType = cartGoodsType;
	}
	public Integer getCartGoodsSelNum() {
		return cartGoodsSelNum;
	}
	public void setCartGoodsSelNum(Integer cartGoodsSelNum) {
		this.cartGoodsSelNum = cartGoodsSelNum;
	}
	public String getCartGName() {
		return cartGName;
	}
	public void setCartGName(String cartGName) {
		this.cartGName = cartGName;
	}
	public String getCartGoodsImgUrl() {
		return cartGoodsImgUrl;
	}
	public void setCartGoodsImgUrl(String cartGoodsImgUrl) {
		this.cartGoodsImgUrl = cartGoodsImgUrl;
	}
	public List<CtRangePrice> getPrices() {
		return prices;
	}
	public void setPrices(List<CtRangePrice> prices) {
		this.prices = prices;
	}
	public String getCartSimNum() {
		return cartSimNum;
	}
	public void setCartSimNum(String cartSimNum) {
		this.cartSimNum = cartSimNum;
	}
	public String getCartParNum() {
		return cartParNum;
	}
	public void setCartParNum(String cartParNum) {
		this.cartParNum = cartParNum;
	}
	public String getCartSimPrice() {
		return cartSimPrice;
	}
	public void setCartSimPrice(String cartSimPrice) {
		this.cartSimPrice = cartSimPrice;
	}
	public String getCartParPrice() {
		return cartParPrice;
	}
	public void setCartParPrice(String cartParPrice) {
		this.cartParPrice = cartParPrice;
	}
	public String getCartCountNum() {
		return cartCountNum;
	}
	public void setCartCountNum(String cartCountNum) {
		this.cartCountNum = cartCountNum;
	}
	public String getCartCountPrice() {
		return cartCountPrice;
	}
	public void setCartCountPrice(String cartCountPrice) {
		this.cartCountPrice = cartCountPrice;
	}
	
	
	
}

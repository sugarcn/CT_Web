package org.ctonline.dto.order;

/**
 * 用于显示订单商品数据
 * @author lxk
 *
 */
public class OrderInfoGoodsDTO {

	private Integer goodsId;
	private String goodsName;
	private String simNum = "0";
	private String parNum = "0";
	private String simPrice = "0.00";
	private String parPrice = "0.00";
	private String simPriceCount = "0.00";
	private String parPriceCount = "0.00";
	private Integer allCountNum = 0;
	private String allPrice = "0.00";
	
	public String getAllPrice() {
		return allPrice;
	}
	public void setAllPrice(String allPrice) {
		this.allPrice = allPrice;
	}
	private String isRefundSim;
	private String isRefundPar;
	
	public String getIsRefundSim() {
		return isRefundSim;
	}
	public void setIsRefundSim(String isRefundSim) {
		this.isRefundSim = isRefundSim;
	}
	public String getIsRefundPar() {
		return isRefundPar;
	}
	public void setIsRefundPar(String isRefundPar) {
		this.isRefundPar = isRefundPar;
	}
	private Integer orderGoodsType; //订单商品类型，1样品 2批量 3样品和批量
	
	public Integer getOrderGoodsType() {
		return orderGoodsType;
	}
	public void setOrderGoodsType(Integer orderGoodsType) {
		this.orderGoodsType = orderGoodsType;
	}
	public Integer getGoodsId() {
		return goodsId;
	}
	public void setGoodsId(Integer goodsId) {
		this.goodsId = goodsId;
	}
	public String getGoodsName() {
		return goodsName;
	}
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	public String getSimNum() {
		return simNum;
	}
	public void setSimNum(String simNum) {
		this.simNum = simNum;
	}
	public String getParNum() {
		return parNum;
	}
	public void setParNum(String parNum) {
		this.parNum = parNum;
	}
	public String getSimPrice() {
		return simPrice;
	}
	public void setSimPrice(String simPrice) {
		this.simPrice = simPrice;
	}
	public String getParPrice() {
		return parPrice;
	}
	public void setParPrice(String parPrice) {
		this.parPrice = parPrice;
	}
	public String getSimPriceCount() {
		return simPriceCount;
	}
	public void setSimPriceCount(String simPriceCount) {
		this.simPriceCount = simPriceCount;
	}
	public String getParPriceCount() {
		return parPriceCount;
	}
	public void setParPriceCount(String parPriceCount) {
		this.parPriceCount = parPriceCount;
	}
	public Integer getAllCountNum() {
		return allCountNum;
	}
	public void setAllCountNum(Integer allCountNum) {
		this.allCountNum = allCountNum;
	}
	
	
}

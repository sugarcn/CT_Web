package org.ctonline.dto.order;

public class OrderDTO {
	private int page;
	private Long uid;
	private Long GId;
	private Long gnum;
	private String pack;
	private Long cupon;
	private int mid;
	private String Discount;
	private String orderSn;
	private String findStr;
	private String startTime;
	private String endTime;
	private String url;
	private String sam;
	private String samTotal;
	private String sh1;
	private String isfirst;
	private Boolean shoppingTypeCollect;
	private String youhuiquanId;
	
	private String simOrPar;
	
	private String payName;
	
	
	public String getPayName() {
		return payName;
	}
	public void setPayName(String payName) {
		this.payName = payName;
	}
	public String getSimOrPar() {
		return simOrPar;
	}
	public void setSimOrPar(String simOrPar) {
		this.simOrPar = simOrPar;
	}
	public String getYouhuiquanId() {
		return youhuiquanId;
	}
	public void setYouhuiquanId(String youhuiquanId) {
		this.youhuiquanId = youhuiquanId;
	}
	public Boolean getShoppingTypeCollect() {
		return shoppingTypeCollect;
	}
	public void setShoppingTypeCollect(Boolean shoppingTypeCollect) {
		this.shoppingTypeCollect = shoppingTypeCollect;
	}
	public String getIsfirst() {
		return isfirst;
	}
	public void setIsfirst(String isfirst) {
		this.isfirst = isfirst;
	}
	public String getSh1() {
		return sh1;
	}
	public void setSh1(String sh1) {
		this.sh1 = sh1;
	}
	public String getSamTotal() {
		return samTotal;
	}
	public void setSamTotal(String samTotal) {
		this.samTotal = samTotal;
	}
	public String getSam() {
		return sam;
	}
	public void setSam(String sam) {
		this.sam = sam;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

	private String shippingType;
	private Short SId;
	
	public String getShippingType() {
		return shippingType;
	}
	public void setShippingType(String shippingType) {
		this.shippingType = shippingType;
	}
	public Short getSId() {
		return SId;
	}
	public void setSId(Short sId) {
		SId = sId;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getFindStr() {
		return findStr;
	}
	public void setFindStr(String findStr) {
		this.findStr = findStr;
	}
	public String getOrderSn() {
		return orderSn;
	}
	public void setOrderSn(String orderSn) {
		this.orderSn = orderSn;
	}
	public String getDiscount() {
		return Discount;
	}
	public void setDiscount(String discount) {
		Discount = discount;
	}
	public int getMid() {
		return mid;
	}
	public void setMid(int mid) {
		this.mid = mid;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public Long getUid() {
		return uid;
	}
	public void setUid(Long uid) {
		this.uid = uid;
	}
	public Long getGId() {
		return GId;
	}
	public void setGId(Long gId) {
		GId = gId;
	}
	public Long getGnum() {
		return gnum;
	}
	public void setGnum(Long gnum) {
		this.gnum = gnum;
	}
	public String getPack() {
		return pack;
	}
	public void setPack(String pack) {
		this.pack = pack;
	}
	public Long getCupon() {
		return cupon;
	}
	public void setCupon(Long cupon) {
		this.cupon = cupon;
	}
	
	private String data;
	private Long ctid;
	private String ctids;

	public String getCtids() {
		return ctids;
	}
	public void setCtids(String ctids) {
		this.ctids = ctids;
	}
	public Long getCtid() {
		return ctid;
	}
	public void setCtid(Long ctid) {
		this.ctid = ctid;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
	private String gids;


	public String getGids() {
		return gids;
	}
	public void setGids(String gids) {
		this.gids = gids;
	}
	
	private String addid;
	private String sptype;
	private String billtype;
	private String bill;
	private String pay;
	private String post;
	private String total;
	private String remark;

	public String getAddid() {
		return addid;
	}
	public void setAddid(String addid) {
		this.addid = addid;
	}
	public String getSptype() {
		return sptype;
	}
	public void setSptype(String sptype) {
		this.sptype = sptype;
	}
	public String getBill() {
		return bill;
	}
	public void setBill(String bill) {
		this.bill = bill;
	}
	public String getBilltype() {
		return billtype;
	}
	public void setBilltype(String billtype) {
		this.billtype = billtype;
	}
	public String getPay() {
		return pay;
	}
	public void setPay(String pay) {
		this.pay = pay;
	}
	public String getPost() {
		return post;
	}
	public void setPost(String post) {
		this.post = post;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	private String time;
	private String status;


	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	private Long orderid;


	public Long getOrderid() {
		return orderid;
	}
	public void setOrderid(Long orderid) {
		this.orderid = orderid;
	}
	private Integer bomid;


	public Integer getBomid() {
		return bomid;
	}
	public void setBomid(Integer bomid) {
		this.bomid = bomid;
	}
	
}

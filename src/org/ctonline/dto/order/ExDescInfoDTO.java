package org.ctonline.dto.order;

public class ExDescInfoDTO {
	private String time;
	private String desc;
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
}

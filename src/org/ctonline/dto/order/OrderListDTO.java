package org.ctonline.dto.order;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.ctonline.po.order.CtOrderGoods;

/**
 * 用于传递订单列表所需要的数据
 * @author lxk
 *
 */
public class OrderListDTO {

	
	private Long orderId;
	private String orderSn;
	private String goodsAllPrice = "0.00";
	private String total;
	private String trueTotal = "0";
	private String orderTime;
	private Integer orderStatus;
	private Integer goodsSize;
	private String freight;
	private String ferightDaoFu = "-1";
	
	private String retTime;
	private String retStatus;
	private Integer evaId;
	private String evaDesc;
	private String evaTime;
	
	private String exName;
	private String exNo;
	
	private String shoptype;
	
	private String pay;
	
	
	public String getPay() {
		return pay;
	}
	public void setPay(String pay) {
		this.pay = pay;
	}
	public String getShoptype() {
		return shoptype;
	}
	public void setShoptype(String shoptype) {
		this.shoptype = shoptype;
	}
	public String getExNo() {
		return exNo;
	}
	public void setExNo(String exNo) {
		this.exNo = exNo;
	}
	public String getExName() {
		return exName;
	}
	public void setExName(String exName) {
		this.exName = exName;
	}
	private List<ExDescInfoDTO> descInfoDTOsList = new ArrayList<ExDescInfoDTO>();
	
	public List<ExDescInfoDTO> getDescInfoDTOsList() {
		return descInfoDTOsList;
	}
	public void setDescInfoDTOsList(List<ExDescInfoDTO> descInfoDTOsList) {
		this.descInfoDTOsList = descInfoDTOsList;
	}
	public String getGoodsAllPrice() {
		return goodsAllPrice;
	}
	public void setGoodsAllPrice(String goodsAllPrice) {
		this.goodsAllPrice = goodsAllPrice;
	}
	private List<OrderInfoGoodsDTO> orderInfoGoodsDTOList;
	
	public String getRetTime() {
		return retTime;
	}
	public void setRetTime(String retTime) {
		this.retTime = retTime;
	}
	public String getRetStatus() {
		return retStatus;
	}
	public void setRetStatus(String retStatus) {
		this.retStatus = retStatus;
	}
	public Integer getEvaId() {
		return evaId;
	}
	public void setEvaId(Integer evaId) {
		this.evaId = evaId;
	}
	public String getEvaDesc() {
		return evaDesc;
	}
	public void setEvaDesc(String evaDesc) {
		this.evaDesc = evaDesc;
	}
	public String getEvaTime() {
		return evaTime;
	}
	public void setEvaTime(String evaTime) {
		this.evaTime = evaTime;
	}
	public String getFerightDaoFu() {
		return ferightDaoFu;
	}
	public void setFerightDaoFu(String ferightDaoFu) {
		this.ferightDaoFu = ferightDaoFu;
	}
	public String getFreight() {
		return freight;
	}
	public void setFreight(String freight) {
		this.freight = freight;
	}
	public Integer getGoodsSize() {
		return goodsSize;
	}
	public void setGoodsSize(Integer goodsSize) {
		this.goodsSize = goodsSize;
	}
	public List<OrderInfoGoodsDTO> getOrderInfoGoodsDTOList() {
		return orderInfoGoodsDTOList;
	}
	public void setOrderInfoGoodsDTOList(
			List<OrderInfoGoodsDTO> orderInfoGoodsDTOList) {
		this.orderInfoGoodsDTOList = orderInfoGoodsDTOList;
	}
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public String getOrderSn() {
		return orderSn;
	}
	public void setOrderSn(String orderSn) {
		this.orderSn = orderSn;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getTrueTotal() {
		return trueTotal;
	}
	public void setTrueTotal(String trueTotal) {
		this.trueTotal = trueTotal;
	}
	public String getOrderTime() {
		return orderTime;
	}
	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}
	public Integer getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}
}

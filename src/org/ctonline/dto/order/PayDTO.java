package org.ctonline.dto.order;

public class PayDTO {
	private String payName;
	private String payTel;
	private String costName;
	private String receiveDate;
	private String myFile;
	public String getPayName() {
		return payName;
	}
	public void setPayName(String payName) {
		this.payName = payName;
	}
	public String getPayTel() {
		return payTel;
	}
	public void setPayTel(String payTel) {
		this.payTel = payTel;
	}
	public String getCostName() {
		return costName;
	}
	public void setCostName(String costName) {
		this.costName = costName;
	}
	public String getReceiveDate() {
		return receiveDate;
	}
	public void setReceiveDate(String receiveDate) {
		this.receiveDate = receiveDate;
	}
	public String getMyFile() {
		return myFile;
	}
	public void setMyFile(String myFile) {
		this.myFile = myFile;
	}
	
}

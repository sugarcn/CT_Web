package org.ctonline.dto.wxmessage;

import java.io.Writer;
import java.util.Date;  
import java.util.Map;  

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest; 

import org.ctonline.action.user.LoginAction;
import org.ctonline.dto.wxmessage.resp.TextMessage;
import org.ctonline.manager.user.CtUserManager;
import org.ctonline.po.user.CtUser;
import org.ctonline.util.WxMessageUtil;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.core.util.QuickWriter;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;
import com.thoughtworks.xstream.io.xml.XppDriver;


public class CoreService {  
    /** 
     * 处理微信发来的请求 
     *  
     * @param request 
     * @return 
     */  
    public static String processRequest(HttpServletRequest request,CtUserManager userManager) {  
        String respMessage = null;  
        try {     
            // 默认返回的文本消息内容  
            String respContent = "请求处理异常，请稍候尝试！";  
         
           
            // xml请求解析  
            Map<String, String> requestMap = WxMessageUtil.parseXml(request);  
  
            // 发送方帐号（open_id）  
            String fromUserName = requestMap.get("FromUserName");  
            // 公众帐号  
            String toUserName = requestMap.get("ToUserName");  
            // 消息类型  
            String msgType = requestMap.get("MsgType");  
            String content = requestMap.get("Content"); 
            
            
            
            
  
            // 回复文本消息  
            TextMessage textMessage = new TextMessage();  
            textMessage.setToUserName(fromUserName);  
            textMessage.setFromUserName(toUserName);  
            textMessage.setCreateTime(new Date().getTime());  
            textMessage.setMsgType(WxMessageUtil.RESP_MESSAGE_TYPE_TEXT);  
          //  textMessage.setFuncFlag(0);  
  
            // 文本消息  
            if (msgType.equals(WxMessageUtil.REQ_MESSAGE_TYPE_TEXT)) {  
            	if (content.substring(0, 2).equals("CT")){//唯一码校验
            		//根据openid取出该用户的信息
            		CtUser user = new CtUser();
            		user = userManager.findWeiXinOpenId(fromUserName);
            		if (content.substring(2, 8).equals(user.getUExtensionid())){//如果相同，随机推送红包
            			//随机
            			LoginAction la = new LoginAction();
            			//la.wxhongbao(fromUserName,user.getUId(),"注册有礼!");
            		}
                	
            		
                }else{
                	respContent = "长亭易购感谢您的支持！";  
                }
            	
                
            }  
            // 图片消息  
            else if (msgType.equals(WxMessageUtil.REQ_MESSAGE_TYPE_IMAGE)) {  
                respContent = "长亭易购感谢您的支持！";  
            }  
            // 地理位置消息  
            else if (msgType.equals(WxMessageUtil.REQ_MESSAGE_TYPE_LOCATION)) {  
                respContent = "长亭易购感谢您的支持！";  
            }  
            // 链接消息  
            else if (msgType.equals(WxMessageUtil.REQ_MESSAGE_TYPE_LINK)) {  
                respContent = "长亭易购感谢您的支持！";  
            }  
            // 音频消息  
            else if (msgType.equals(WxMessageUtil.REQ_MESSAGE_TYPE_VOICE)) {  
                respContent = "长亭易购感谢您的支持！";  
            }  
            // 事件推送  
            else if (msgType.equals(WxMessageUtil.REQ_MESSAGE_TYPE_EVENT)) {  
                // 事件类型  
                String eventType = request.getParameter("Event");  
                // 订阅  
                if (eventType.equals(WxMessageUtil.EVENT_TYPE_SUBSCRIBE)) {  
                    respContent = "谢谢您的关注！";  
                }  
                // 取消订阅  
                else if (eventType.equals(WxMessageUtil.EVENT_TYPE_UNSUBSCRIBE)) {  
                    // TODO 取消订阅后用户再收不到公众号发送的消息，因此不需要回复消息  
                }  
                // 自定义菜单点击事件  
                else if (eventType.equals(WxMessageUtil.EVENT_TYPE_CLICK)) {  
                    // TODO 自定义菜单权没有开放，暂不处理该类消息  
                }  
            }  
  
            textMessage.setContent(respContent);  
            respMessage = WxMessageUtil.textMessageToXml(textMessage);  
           
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
  
        return respMessage;  
    }  
}  

package org.ctonline.dto.user;

public class CtAddTicketDTO {
	private Long ATId;
	private Long UId;
	private String companyName;
	private String companySn;
	private String registeAddress;
	private String registeTel;
	private String depositBank;
	private String bankAccount;
	private String lisence;
	private String taxCertifi;
	private String generalCertifi;
	private String bankLisence;
	private String isPass;
	private int page;
	private String keyword;
	private Long[] mid;
	private Long id;
	private String url;
	
	// Property accessors

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getATId() {
		return this.ATId;
	}

	public void setATId(Long ATId) {
		this.ATId = ATId;
	}

	public Long getUId() {
		return this.UId;
	}

	public void setUId(Long UId) {
		this.UId = UId;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanySn() {
		return this.companySn;
	}

	public void setCompanySn(String companySn) {
		this.companySn = companySn;
	}

	public String getRegisteAddress() {
		return this.registeAddress;
	}

	public void setRegisteAddress(String registeAddress) {
		this.registeAddress = registeAddress;
	}

	public String getRegisteTel() {
		return this.registeTel;
	}

	public void setRegisteTel(String registeTel) {
		this.registeTel = registeTel;
	}

	public String getDepositBank() {
		return this.depositBank;
	}

	public void setDepositBank(String depositBank) {
		this.depositBank = depositBank;
	}

	public String getBankAccount() {
		return this.bankAccount;
	}

	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	public String getLisence() {
		return this.lisence;
	}

	public void setLisence(String lisence) {
		this.lisence = lisence;
	}

	public String getTaxCertifi() {
		return this.taxCertifi;
	}

	public void setTaxCertifi(String taxCertifi) {
		this.taxCertifi = taxCertifi;
	}

	public String getGeneralCertifi() {
		return this.generalCertifi;
	}

	public void setGeneralCertifi(String generalCertifi) {
		this.generalCertifi = generalCertifi;
	}

	public String getBankLisence() {
		return this.bankLisence;
	}

	public void setBankLisence(String bankLisence) {
		this.bankLisence = bankLisence;
	}

	public String getIsPass() {
		return this.isPass;
	}

	public void setIsPass(String isPass) {
		this.isPass = isPass;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Long[] getMid() {
		return mid;
	}

	public void setMid(Long[] mid) {
		this.mid = mid;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}

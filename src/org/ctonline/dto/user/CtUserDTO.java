package org.ctonline.dto.user;

import java.util.List;
import java.util.Set;

/**
 * CtUser entity. @author MyEclipse Persistence Tools
 */

public class CtUserDTO{

	// Fields

	private Long UId;
	private String UUserid;
	private String UUsername;
	private String UPassword;
	private String UMb;
	private String UEmail;
	private String UQq;
	private String UWeibo;
	private String UTb;
	private String USex;
	private String UBirthday;
	private String ULastTime;
	private String URegTime;
	private String ULastIp;
	private Long RId;
	private Long UGId;
	private String UState;
	private String UCode;
	
	private int page;
	private String keyword;
	private Long[] mid;
	private Long id;
	private String url;
	private String code;
	private String UWeixinloginUserid;
	private String UWeixingztime;
	private int subscribe;
	
	private String timespace;
	private String yzmNoPass;
	

	public String getYzmNoPass() {
		return yzmNoPass;
	}

	public void setYzmNoPass(String yzmNoPass) {
		this.yzmNoPass = yzmNoPass;
	}

	private String userPhone;
	

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getTimespace() {
		return timespace;
	}

	public void setTimespace(String timespace) {
		this.timespace = timespace;
	}

	public int getSubscribe() {
		return subscribe;
	}

	public void setSubscribe(int subscribe) {
		this.subscribe = subscribe;
	}

	public String getUWeixingztime() {
		return UWeixingztime;
	}

	public void setUWeixingztime(String uWeixingztime) {
		UWeixingztime = uWeixingztime;
	}

	public String getUWeixinloginUserid() {
		return UWeixinloginUserid;
	}

	public void setUWeixinloginUserid(String uWeixinloginUserid) {
		UWeixinloginUserid = uWeixinloginUserid;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getUId() {
		return this.UId;
	}

	public void setUId(Long UId) {
		this.UId = UId;
	}

	public String getUUserid() {
		return this.UUserid;
	}

	public void setUUserid(String UUserid) {
		this.UUserid = UUserid;
	}

	public String getUUsername() {
		return this.UUsername;
	}

	public void setUUsername(String UUsername) {
		this.UUsername = UUsername;
	}

	public String getUPassword() {
		return this.UPassword;
	}

	public void setUPassword(String UPassword) {
		this.UPassword = UPassword;
	}

	public String getUMb() {
		return this.UMb;
	}

	public void setUMb(String UMb) {
		this.UMb = UMb;
	}

	public String getUEmail() {
		return this.UEmail;
	}

	public void setUEmail(String UEmail) {
		this.UEmail = UEmail;
	}

	public String getUQq() {
		return this.UQq;
	}

	public void setUQq(String UQq) {
		this.UQq = UQq;
	}

	public String getUWeibo() {
		return this.UWeibo;
	}

	public void setUWeibo(String UWeibo) {
		this.UWeibo = UWeibo;
	}

	public String getUTb() {
		return this.UTb;
	}

	public void setUTb(String UTb) {
		this.UTb = UTb;
	}

	public String getUSex() {
		return this.USex;
	}

	public void setUSex(String USex) {
		this.USex = USex;
	}

	public String getUBirthday() {
		return this.UBirthday;
	}

	public void setUBirthday(String UBirthday) {
		this.UBirthday = UBirthday;
	}

	public String getULastTime() {
		return this.ULastTime;
	}

	public void setULastTime(String ULastTime) {
		this.ULastTime = ULastTime;
	}

	public String getURegTime() {
		return this.URegTime;
	}

	public void setURegTime(String URegTime) {
		this.URegTime = URegTime;
	}

	public String getULastIp() {
		return this.ULastIp;
	}

	public void setULastIp(String ULastIp) {
		this.ULastIp = ULastIp;
	}

	public Long getRId() {
		return this.RId;
	}

	public void setRId(Long RId) {
		this.RId = RId;
	}

	public Long getUGId() {
		return this.UGId;
	}

	public void setUGId(Long UGId) {
		this.UGId = UGId;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Long[] getMid() {
		return mid;
	}

	public void setMid(Long[] mid) {
		this.mid = mid;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

//	public CtAddTicket getCtAddTicket() {
//		return ctAddTicket;
//	}
//
//	public void setCtAddTicket(CtAddTicket ctAddTicket) {
//		this.ctAddTicket = ctAddTicket;
//	}
	public String getUState() {
		return UState;
	}

	public void setUState(String uState) {
		UState = uState;
	}

	public String getUCode() {
		return UCode;
	}

	public void setUCode(String uCode) {
		UCode = uCode;
	}

}
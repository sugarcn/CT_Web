package org.ctonline.dto.basic;

import java.util.List;

import org.ctonline.po.basic.CtRegion;

public class CtRegionDTO {
	public Long getRegionId() {
		return regionId;
	}
	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}
	public String getRegionName() {
		return regionName;
	}
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public Integer getRegionType() {
		return regionType;
	}
	public void setRegionType(Integer regionType) {
		this.regionType = regionType;
	}
	private Long regionId;
	private String regionName;
	private Long parentId;
	private Integer regionType;
	private int page;
	private String keyword;
	private Long[] mid;
	private Long id;
	private List<CtRegion> plist;
	public List<CtRegion> getPlist() {
		return plist;
	}
	public void setPlist(List<CtRegion> plist) {
		this.plist = plist;
	}
	public Long[] getMid() {
		return mid;
	}
	public void setMid(Long[] mid) {
		this.mid = mid;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
}

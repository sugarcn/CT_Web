package org.ctonline.dto.basic;

import org.ctonline.po.basic.CtCoupon;
import org.ctonline.po.basic.CtCouponDetail;

public class CtCouponDetailDTO {
private CtCouponDetail coupondetail;
	
	private int page;
	private String keyword;
	private Integer CDetailId;
	private CtCoupon ctCoupon;
	private String couponNumber;
	private Long UId;
	private String useTime;
	private Long orderId;
	private Double prePrice;
	private Integer releaseUId;
	private String type1;
	private String type2;
	private Integer couponId;
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public CtCouponDetail getCouponDetail() {
		return coupondetail;
	}
	public void setCouponDetail(CtCouponDetail coupondetail) {
		this.coupondetail = coupondetail;
	}
	public CtCouponDetail getCoupondetail() {
		return coupondetail;
	}
	public void setCoupondetail(CtCouponDetail coupondetail) {
		this.coupondetail = coupondetail;
	}
	public Integer getCDetailId() {
		return CDetailId;
	}
	public void setCDetailId(Integer cDetailId) {
		CDetailId = cDetailId;
	}
	public CtCoupon getCtCoupon() {
		return ctCoupon;
	}
	public void setCtCoupon(CtCoupon ctCoupon) {
		this.ctCoupon = ctCoupon;
	}
	public String getCouponNumber() {
		return couponNumber;
	}
	public void setCouponNumber(String couponNumber) {
		this.couponNumber = couponNumber;
	}
	public Long getUId() {
		return UId;
	}
	public void setUId(Long uId) {
		UId = uId;
	}
	public String getUseTime() {
		return useTime;
	}
	public void setUseTime(String useTime) {
		this.useTime = useTime;
	}
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public Double getPrePrice() {
		return prePrice;
	}
	public void setPrePrice(Double prePrice) {
		this.prePrice = prePrice;
	}
	public Integer getReleaseUId() {
		return releaseUId;
	}
	public void setReleaseUId(Integer releaseUId) {
		this.releaseUId = releaseUId;
	}
	public String getType1() {
		return type1;
	}
	public void setType1(String type1) {
		this.type1 = type1;
	}
	public String getType2() {
		return type2;
	}
	public void setType2(String type2) {
		this.type2 = type2;
	}
	public Integer getCouponId() {
		return couponId;
	}
	public void setCouponId(Integer couponId) {
		this.couponId = couponId;
	}
}
	
	
	

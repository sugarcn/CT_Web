package org.ctonline.dto.basic;

public class CtNoticeDTO {
	private int page;
	private Integer noId;
	private String noTitle;
	private String noContent;
	private String noUrl;
	private String noTime;
	private String keyword;
	private int[] mid;
	private int id;
	
	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public Integer getNoId() {
		return noId;
	}

	public void setNoId(Integer noId) {
		this.noId = noId;
	}

	public String getNoTitle() {
		return noTitle;
	}

	public void setNoTitle(String noTitle) {
		this.noTitle = noTitle;
	}

	public String getNoContent() {
		return noContent;
	}

	public void setNoContent(String noContent) {
		this.noContent = noContent;
	}

	public String getNoUrl() {
		return noUrl;
	}

	public void setNoUrl(String noUrl) {
		this.noUrl = noUrl;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public int[] getMid() {
		return mid;
	}

	public void setMid(int[] mid) {
		this.mid = mid;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNoTime() {
		return noTime;
	}

	public void setNoTime(String noTime) {
		this.noTime = noTime;
	}

}

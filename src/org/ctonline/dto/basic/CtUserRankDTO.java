package org.ctonline.dto.basic;

public class CtUserRankDTO {
	public Long getRId() {
		return this.RId;
	}

	public void setRId(Long RId) {
		this.RId = RId;
	}

	public String getRName() {
		return this.RName;
	}

	public void setRName(String RName) {
		this.RName = RName;
	}

	public String getMinPoints() {
		return this.minPoints;
	}

	public void setMinPoints(String minPoints) {
		this.minPoints = minPoints;
	}

	public String getMaxPoints() {
		return this.maxPoints;
	}

	public void setMaxPoints(String maxPoints) {
		this.maxPoints = maxPoints;
	}
	
	private Long RId;
	private String RName;
	private String minPoints;
	private String maxPoints;
	private int page;
	private String keyword;
	private Long[] mid;
	private Long id;
	public Long[] getMid() {
		return mid;
	}
	public void setMid(Long[] mid) {
		this.mid = mid;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
}

package org.ctonline.dto.pay;

import org.ctonline.config.AlipayConfig;

public class ChinaPayDTO {

	private String chinaPayHtmlText;
	private String OrdId;//订单号
	private String subject;//订单名称
	private String TransAmt;//订单金额
	private String CuryId;//交易币种
	private String TransDate;//订单交易日期
	private String TransType;//交易类型
	private String Version;//版本号
	private String MerId;//会员ID
	private String BgRetUrl; //后台接收Url
	private String PageRetUrl; //页面接收Url
	private String GateId; //支付网关号
	private String Priv1; //商户私有域
	
	private String status;//银联支付接口返回状态

	public String getChinaPayHtmlText() {
		return chinaPayHtmlText;
	}

	public void setChinaPayHtmlText(String chinaPayHtmlText) {
		this.chinaPayHtmlText = chinaPayHtmlText;
	}

	public String getOrdId() {
		return OrdId;
	}

	public void setOrdId(String ordId) {
		OrdId = ordId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTransAmt() {
		return TransAmt;
	}

	public void setTransAmt(String transAmt) {
		TransAmt = transAmt;
	}

	public String getCuryId() {
		return CuryId;
	}

	public void setCuryId(String curyId) {
		CuryId = curyId;
	}

	public String getTransDate() {
		return TransDate;
	}

	public void setTransDate(String transDate) {
		TransDate = transDate;
	}

	public String getTransType() {
		return TransType;
	}

	public void setTransType(String transType) {
		TransType = transType;
	}

	public String getVersion() {
		return Version;
	}

	public void setVersion(String version) {
		Version = version;
	}

	public String getMerId() {
		return MerId;
	}

	public void setMerId(String merId) {
		MerId = merId;
	}

	public String getBgRetUrl() {
		return BgRetUrl;
	}

	public void setBgRetUrl(String bgRetUrl) {
		BgRetUrl = bgRetUrl;
	}

	public String getPageRetUrl() {
		return PageRetUrl;
	}

	public void setPageRetUrl(String pageRetUrl) {
		PageRetUrl = pageRetUrl;
	}

	public String getGateId() {
		return GateId;
	}

	public void setGateId(String gateId) {
		GateId = gateId;
	}

	public String getPriv1() {
		return Priv1;
	}

	public void setPriv1(String priv1) {
		Priv1 = priv1;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	


}

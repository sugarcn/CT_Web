package org.ctonline.dto.pay;

import java.util.ArrayList;
import java.util.List;

import org.ctonline.po.basic.CtCashCoupon;
import org.ctonline.po.user.CtUser;

public class CtPayDTO {

	private String sHtmlText;
	private String out_trade_no;//订单号
	private String subject;//订单名称
	public String getMem_id() {
		return mem_id;
	}

	public void setMem_id(String mem_id) {
		this.mem_id = mem_id;
	}

	private String total_fee;//订单金额
	private String orderbody;//订单描述
	private String goods_address;//商品地址
	private String trade_no;//支付宝交易号
	private String trade_status;//交易状态
	private String mem_id;//会员ID
	
	private String pay_desc; //备注信息
	
	
	private String payType;//支付方式
	
	private Integer cashId;
	
	private CtUser user;
	
	
	public CtUser getUser() {
		return user;
	}

	public void setUser(CtUser user) {
		this.user = user;
	}

	public Integer getCashId() {
		return cashId;
	}

	public void setCashId(Integer cashId) {
		this.cashId = cashId;
	}

	private List<CtCashCoupon> cashCouponList = new ArrayList<CtCashCoupon>();
	
	private CtCashCoupon coupon = new CtCashCoupon();
	
	
	
	public CtCashCoupon getCoupon() {
		return coupon;
	}

	public void setCoupon(CtCashCoupon coupon) {
		this.coupon = coupon;
	}

	public List<CtCashCoupon> getCashCouponList() {
		return cashCouponList;
	}

	public void setCashCouponList(List<CtCashCoupon> cashCouponList) {
		this.cashCouponList = cashCouponList;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	private String status;//银联支付接口返回状态
	
	

	public String getPay_desc() {
		return pay_desc;
	}

	public void setPay_desc(String pay_desc) {
		this.pay_desc = pay_desc;
	}

	public String getSubject() {
		return subject;
	}

	public String getOut_trade_no() {
		return out_trade_no;
	}

	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}

	public String getTrade_no() {
		return trade_no;
	}

	public void setTrade_no(String trade_no) {
		this.trade_no = trade_no;
	}

	public String getTrade_status() {
		return trade_status;
	}

	public void setTrade_status(String trade_status) {
		this.trade_status = trade_status;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTotal_fee() {
		return total_fee;
	}

	public void setTotal_fee(String total_fee) {
		this.total_fee = total_fee;
	}

	public String getOrderbody() {
		return orderbody;
	}

	public void setOrderbody(String orderbody) {
		this.orderbody = orderbody;
	}

	public String getGoods_address() {
		return goods_address;
	}

	public void setGoods_address(String goods_address) {
		this.goods_address = goods_address;
	}

	public String getsHtmlText() {
		return sHtmlText;
	}

	public void setsHtmlText(String sHtmlText) {
		this.sHtmlText = sHtmlText;
	}

}

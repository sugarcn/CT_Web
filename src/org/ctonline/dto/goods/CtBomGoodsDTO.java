package org.ctonline.dto.goods;

import org.ctonline.po.goods.CtBom;
import org.ctonline.po.goods.CtGoods;

/**
 * CtBomGoods entity. @author MyEclipse Persistence Tools
 */

public class CtBomGoodsDTO implements java.io.Serializable {

	// Fields

	private Long bomGoodsId;
	private CtBom ctBom;
	private Long GId;
	private String pack;
	private Long goodsNum;
	private CtGoods goods;
	private int page;
	private String keyword;
	private Long[] mid;
	private Long id;
	private Integer bomId;

	private String updateType;
	
	// Constructors

	public String getUpdateType() {
		return updateType;
	}

	public void setUpdateType(String updateType) {
		this.updateType = updateType;
	}

	/** default constructor */
	public CtBomGoodsDTO() {
	}

	/** full constructor */
	public CtBomGoodsDTO(CtBom ctBom, Long GId, String pack, Long goodsNum) {
		this.ctBom = ctBom;
		this.GId = GId;
		this.pack = pack;
		this.goodsNum = goodsNum;
	}

	// Property accessors

	public Long getBomGoodsId() {
		return this.bomGoodsId;
	}

	public void setBomGoodsId(Long bomGoodsId) {
		this.bomGoodsId = bomGoodsId;
	}

	public CtBom getCtBom() {
		return this.ctBom;
	}

	public void setCtBom(CtBom ctBom) {
		this.ctBom = ctBom;
	}

	public Long getGId() {
		return this.GId;
	}

	public void setGId(Long GId) {
		this.GId = GId;
	}

	public String getPack() {
		return this.pack;
	}

	public void setPack(String pack) {
		this.pack = pack;
	}

	public Long getGoodsNum() {
		return this.goodsNum;
	}

	public void setGoodsNum(Long goodsNum) {
		this.goodsNum = goodsNum;
	}

	public CtGoods getGoods() {
		return goods;
	}

	public void setGoods(CtGoods goods) {
		this.goods = goods;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Long[] getMid() {
		return mid;
	}

	public void setMid(Long[] mid) {
		this.mid = mid;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getBomId() {
		return bomId;
	}

	public void setBomId(Integer bomId) {
		this.bomId = bomId;
	}

}
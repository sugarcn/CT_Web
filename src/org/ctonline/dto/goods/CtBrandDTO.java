package org.ctonline.dto.goods;

import java.util.List;

import org.ctonline.po.goods.CtGoods;

public class CtBrandDTO {
	private Long bid;
	private String bname;
	private String blogn;
	private String bdesc;
	private List<CtGoods> goodsName;
	private List<CtGoods> goodsName1;
	private Long goodid;
	private String goodsCate;
	
	public String getGoodsCate() {
		return goodsCate;
	}
	public void setGoodsCate(String goodsCate) {
		this.goodsCate = goodsCate;
	}
	public String getBname() {
		return bname;
	}
	public void setBname(String bname) {
		this.bname = bname;
	}
	public String getBlogn() {
		return blogn;
	}
	public void setBlogn(String blogn) {
		this.blogn = blogn;
	}
	public Long getBid() {
		return bid;
	}
	public void setBid(Long bid) {
		this.bid = bid;
	}
	public String getBdesc() {
		return bdesc;
	}
	public void setBdesc(String bdesc) {
		this.bdesc = bdesc;
	}
	public List<CtGoods> getGoodsName() {
		return goodsName;
	}
	public void setGoodsName(List<CtGoods> goodsName) {
		this.goodsName = goodsName;
	}
	public Long getGoodid() {
		return goodid;
	}
	public void setGoodid(Long goodid) {
		this.goodid = goodid;
	}
	public List<CtGoods> getGoodsName1() {
		return goodsName1;
	}
	public void setGoodsName1(List<CtGoods> goodsName1) {
		this.goodsName1 = goodsName1;
	}
	
}

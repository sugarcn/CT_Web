package org.ctonline.dto.goods;

import org.ctonline.po.goods.CtGoodsBrand;
import org.ctonline.po.goods.CtGoodsCategory;

public class F_GoodsDTO {
	private int page;
	private String keyword;
	private Long[] mid;
	private Long id;
	private Long cartId;
	private Long GId;
	private String GName;
	private Long CId;
	private String GSn;
	private Long BId;
	private Double marketPrice;
	private Double shopPrice;
	private String price1;
	private String price2;
	private Double promotePrice;
	private String isOnSale;
	private String GKeywords;
	private Long tokenCredit;
	private Long giveCredit;
	private String GUnit;
	private String pack1;
	private Integer pack1Num;
	private String pack2;
	private Integer pack2Num;
	private String pack3;
	private Integer pack3Num;
	private String pack4;
	private Integer pack4Num;
	private String isDel;
	private Long collid;
	private String colltitle;
	private String colldesc;
	private String colltime;
	private String cname;
	private String ctids;
	private String url;
	private String bname;
	
	
	private String simCount;
	private String parCount;
	private String allCount;
	private String timestamp;
	
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getSimCount() {
		return simCount;
	}
	public void setSimCount(String simCount) {
		this.simCount = simCount;
	}
	public String getParCount() {
		return parCount;
	}
	public void setParCount(String parCount) {
		this.parCount = parCount;
	}
	public String getAllCount() {
		return allCount;
	}
	public void setAllCount(String allCount) {
		this.allCount = allCount;
	}
	public String getBname() {
		return bname;
	}
	public void setBname(String bname) {
		this.bname = bname;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getCtids() {
		return ctids;
	}
	public void setCtids(String ctids) {
		this.ctids = ctids;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public String getColltitle() {
		return colltitle;
	}
	public void setColltitle(String colltitle) {
		this.colltitle = colltitle;
	}
	public String getColldesc() {
		return colldesc;
	}
	public void setColldesc(String colldesc) {
		this.colldesc = colldesc;
	}
	public String getPrice1() {
		return price1;
	}
	public void setPrice1(String price1) {
		this.price1 = price1;
	}
	public String getPrice2() {
		return price2;
	}
	public void setPrice2(String price2) {
		this.price2 = price2;
	}

	private CtGoodsBrand goodsBrand;
	private CtGoodsCategory goodsCategory;
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public Long[] getMid() {
		return mid;
	}
	public void setMid(Long[] mid) {
		this.mid = mid;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getGId() {
		return GId;
	}
	public void setGId(Long gId) {
		GId = gId;
	}
	public String getGName() {
		return GName;
	}
	public void setGName(String gName) {
		GName = gName;
	}
	public Long getCId() {
		return CId;
	}
	public void setCId(Long cId) {
		CId = cId;
	}
	public String getGSn() {
		return GSn;
	}
	public void setGSn(String gSn) {
		GSn = gSn;
	}
	public Long getBId() {
		return BId;
	}
	public void setBId(Long bId) {
		BId = bId;
	}
	public Double getMarketPrice() {
		return marketPrice;
	}
	public void setMarketPrice(Double marketPrice) {
		this.marketPrice = marketPrice;
	}
	public Double getShopPrice() {
		return shopPrice;
	}
	public void setShopPrice(Double shopPrice) {
		this.shopPrice = shopPrice;
	}
	public Double getPromotePrice() {
		return promotePrice;
	}
	public void setPromotePrice(Double promotePrice) {
		this.promotePrice = promotePrice;
	}
	public String getIsOnSale() {
		return isOnSale;
	}
	public void setIsOnSale(String isOnSale) {
		this.isOnSale = isOnSale;
	}
	public String getGKeywords() {
		return GKeywords;
	}
	public void setGKeywords(String gKeywords) {
		GKeywords = gKeywords;
	}
	public Long getTokenCredit() {
		return tokenCredit;
	}
	public void setTokenCredit(Long tokenCredit) {
		this.tokenCredit = tokenCredit;
	}
	public Long getGiveCredit() {
		return giveCredit;
	}
	public void setGiveCredit(Long giveCredit) {
		this.giveCredit = giveCredit;
	}
	public String getGUnit() {
		return GUnit;
	}
	public void setGUnit(String gUnit) {
		GUnit = gUnit;
	}
	public String getPack1() {
		return pack1;
	}
	public void setPack1(String pack1) {
		this.pack1 = pack1;
	}
	public Integer getPack1Num() {
		return pack1Num;
	}
	public void setPack1Num(Integer pack1Num) {
		this.pack1Num = pack1Num;
	}
	public String getPack2() {
		return pack2;
	}
	public void setPack2(String pack2) {
		this.pack2 = pack2;
	}
	public Integer getPack2Num() {
		return pack2Num;
	}
	public void setPack2Num(Integer pack2Num) {
		this.pack2Num = pack2Num;
	}
	public String getPack3() {
		return pack3;
	}
	public void setPack3(String pack3) {
		this.pack3 = pack3;
	}
	public Integer getPack3Num() {
		return pack3Num;
	}
	public void setPack3Num(Integer pack3Num) {
		this.pack3Num = pack3Num;
	}
	public String getPack4() {
		return pack4;
	}
	public void setPack4(String pack4) {
		this.pack4 = pack4;
	}
	public Integer getPack4Num() {
		return pack4Num;
	}
	public void setPack4Num(Integer pack4Num) {
		this.pack4Num = pack4Num;
	}
	public String getIsDel() {
		return isDel;
	}
	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}
	public CtGoodsBrand getGoodsBrand() {
		return goodsBrand;
	}
	public void setGoodsBrand(CtGoodsBrand goodsBrand) {
		this.goodsBrand = goodsBrand;
	}
	public CtGoodsCategory getGoodsCategory() {
		return goodsCategory;
	}
	public void setGoodsCategory(CtGoodsCategory goodsCategory) {
		this.goodsCategory = goodsCategory;
	}
	
	private Long gid;
	private Long pid;
	private Long cid;
	private String kword;
	
	
	public Long getGid() {
		return gid;
	}
	public void setGid(Long gid) {
		this.gid = gid;
	}
	public Long getPid() {
		return pid;
	}
	public void setPid(Long pid) {
		this.pid = pid;
	}
	public String getKword() {
		return kword;
	}
	public void setKword(String kword) {
		this.kword = kword;
	}
	public Long getCid() {
		return cid;
	}
	public void setCid(Long cid) {
		this.cid = cid;
	}
	private Long caid;
	private Long paid;
	private String keyw;
	public Long getPaid() {
		return paid;
	}
	public void setPaid(Long paid) {
		this.paid = paid;
	}
	public String getKeyw() {
		return keyw;
	}
	public void setKeyw(String keyw) {
		this.keyw = keyw;
	}
	public Long getCaid() {
		return caid;
	}
	public void setCaid(Long caid) {
		this.caid = caid;
	}
	private Long brid;
	public Long getBrid() {
		return brid;
	}
	public void setBrid(Long brid) {
		this.brid = brid;
	}
	private int type;
	private String mod;
	private String cont;
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getMod() {
		return mod;
	}
	public void setMod(String mod) {
		this.mod = mod;
	}
	public String getCont() {
		return cont;
	}
	public void setCont(String cont) {
		this.cont = cont;
	}

	public Long getCollid() {
		return collid;
	}
	public void setCollid(Long collid) {
		this.collid = collid;
	}
	public String getColltime() {
		return colltime;
	}
	public void setColltime(String colltime) {
		this.colltime = colltime;
	}
	public Long getCartId() {
		return cartId;
	}
	public void setCartId(Long cartId) {
		this.cartId = cartId;
	}
}

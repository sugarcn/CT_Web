package org.ctonline.dto.goods;

import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.order.CtOrderInfo;

/**
 * CtOrderGoods entity. @author MyEclipse Persistence Tools
 */

public class CtOrderGoodsDTO implements java.io.Serializable {

	// Fields

	private Long orderGoodsId;
	private Long orderId;
	private Long GId;
	private String GPrice;
	private String GNumber;
	private String pack;
	private Long couponId;
	private String GSubtotal;
	private String GParPrice;
	private String GParNumber;
	private String isParOrSim;
	private CtGoods ctGoods;
	private String cartType;
	
	private String priceAllDan;
	private String priceAllDanSim;
	private String isRefund;
	
	
	public String getIsRefund() {
		return isRefund;
	}

	public void setIsRefund(String isRefund) {
		this.isRefund = isRefund;
	}

	public String getPriceAllDanSim() {
		return priceAllDanSim;
	}

	public void setPriceAllDanSim(String priceAllDanSim) {
		this.priceAllDanSim = priceAllDanSim;
	}

	public String getPriceAllDan() {
		return priceAllDan;
	}

	public void setPriceAllDan(String priceAllDan) {
		this.priceAllDan = priceAllDan;
	}

	private CtOrderInfo orderInfo;
	
	
	// Constructors

	
	public CtOrderInfo getOrderInfo() {
		return orderInfo;
	}

	public void setOrderInfo(CtOrderInfo orderInfo) {
		this.orderInfo = orderInfo;
	}

	public CtGoods getCtGoods() {
		return ctGoods;
	}

	public String getCartType() {
		return cartType;
	}

	public void setCartType(String cartType) {
		this.cartType = cartType;
	}

	public String getGParPrice() {
		return GParPrice;
	}

	public void setGParPrice(String gParPrice) {
		GParPrice = gParPrice;
	}

	public String getGParNumber() {
		return GParNumber;
	}

	public void setGParNumber(String gParNumber) {
		GParNumber = gParNumber;
	}

	public String getIsParOrSim() {
		return isParOrSim;
	}

	public void setIsParOrSim(String isParOrSim) {
		this.isParOrSim = isParOrSim;
	}

	public void setCtGoods(CtGoods ctGoods) {
		this.ctGoods = ctGoods;
	}

	/** default constructor */
	public CtOrderGoodsDTO() {
	}

	/** minimal constructor */
	public CtOrderGoodsDTO(Long orderId, Long GId, String GPrice, String GNumber,
			String pack, String GSubtotal) {
		this.orderId = orderId;
		this.GId = GId;
		this.GPrice = GPrice;
		this.GNumber = GNumber;
		this.pack = pack;
		this.GSubtotal = GSubtotal;
	}

	/** full constructor */
	public CtOrderGoodsDTO(Long orderId, Long GId, String GPrice, String GNumber,
			String pack, Long couponId, String GSubtotal) {
		this.orderId = orderId;
		this.GId = GId;
		this.GPrice = GPrice;
		this.GNumber = GNumber;
		this.pack = pack;
		this.couponId = couponId;
		this.GSubtotal = GSubtotal;
	}

	// Property accessors

	public Long getOrderGoodsId() {
		return this.orderGoodsId;
	}

	public void setOrderGoodsId(Long orderGoodsId) {
		this.orderGoodsId = orderGoodsId;
	}

	public Long getOrderId() {
		return this.orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Long getGId() {
		return this.GId;
	}

	public void setGId(Long GId) {
		this.GId = GId;
	}

	public String getGPrice() {
		return this.GPrice;
	}

	public void setGPrice(String GPrice) {
		this.GPrice = GPrice;
	}

	public String getGNumber() {
		return this.GNumber;
	}

	public void setGNumber(String GNumber) {
		this.GNumber = GNumber;
	}

	public String getPack() {
		return this.pack;
	}

	public void setPack(String pack) {
		this.pack = pack;
	}

	public Long getCouponId() {
		return this.couponId;
	}

	public void setCouponId(Long couponId) {
		this.couponId = couponId;
	}

	public String getGSubtotal() {
		return this.GSubtotal;
	}

	public void setGSubtotal(String GSubtotal) {
		this.GSubtotal = GSubtotal;
	}

}
package org.ctonline.dto.goods;

/**
 * CtRangePrice entity. @author MyEclipse Persistence Tools
 */

public class CtRangePriceDTO implements java.io.Serializable {

	// Fields

	private Long RPid;
	private Long GId;
	private Integer simSNum;
	private Integer simENum;
	private Double simRPrice;
	private Integer simIncrease;
	private String parSNum;
	private String parENum;
	private Double parRPrice;
	private String parIncrease;
	private String insTime;
	private String addStr;
	private String addStrForPar;
	
	// Constructors


	public String getAddStrForPar() {
		return addStrForPar;
	}

	public CtRangePriceDTO(Long rPid, Long gId, Integer simSNum, Integer simENum,
			Double simRPrice, Integer simIncrease, String parSNum,
			String parENum, Double parRPrice, String parIncrease,
			String insTime, String addStr, String addStrForPar) {
		super();
		RPid = rPid;
		GId = gId;
		this.simSNum = simSNum;
		this.simENum = simENum;
		this.simRPrice = simRPrice;
		this.simIncrease = simIncrease;
		this.parSNum = parSNum;
		this.parENum = parENum;
		this.parRPrice = parRPrice;
		this.parIncrease = parIncrease;
		this.insTime = insTime;
		this.addStr = addStr;
		this.addStrForPar = addStrForPar;
	}

	public Integer getSimSNum() {
		return simSNum;
	}

	public void setSimSNum(Integer simSNum) {
		this.simSNum = simSNum;
	}

	public Integer getSimENum() {
		return simENum;
	}

	public void setSimENum(Integer simENum) {
		this.simENum = simENum;
	}

	public Integer getSimIncrease() {
		return simIncrease;
	}

	public void setSimIncrease(Integer simIncrease) {
		this.simIncrease = simIncrease;
	}

	public String getParSNum() {
		return parSNum;
	}

	public void setParSNum(String parSNum) {
		this.parSNum = parSNum;
	}

	public String getParENum() {
		return parENum;
	}

	public void setParENum(String parENum) {
		this.parENum = parENum;
	}

	public String getParIncrease() {
		return parIncrease;
	}

	public void setParIncrease(String parIncrease) {
		this.parIncrease = parIncrease;
	}

	public String getInsTime() {
		return insTime;
	}

	public void setInsTime(String insTime) {
		this.insTime = insTime;
	}

	public void setAddStrForPar(String addStrForPar) {
		this.addStrForPar = addStrForPar;
	}

	public String getAddStr() {
		return addStr;
	}

	public void setAddStr(String addStr) {
		this.addStr = addStr;
	}

	/** default constructor */
	public CtRangePriceDTO() {
	}


	// Property accessors

	public Long getRPid() {
		return this.RPid;
	}

	public void setRPid(Long RPid) {
		this.RPid = RPid;
	}

	public Long getGId() {
		return this.GId;
	}

	public void setGId(Long GId) {
		this.GId = GId;
	}


	public Double getSimRPrice() {
		return this.simRPrice;
	}

	public void setSimRPrice(Double simRPrice) {
		this.simRPrice = simRPrice;
	}



	public Double getParRPrice() {
		return this.parRPrice;
	}

	public void setParRPrice(Double parRPrice) {
		this.parRPrice = parRPrice;
	}


}
package org.ctonline.dto.goods;

import java.math.BigDecimal;

/**
 * ViewCartListId entity. @author MyEclipse Persistence Tools
 */

public class ViewCartListDTO implements java.io.Serializable {

	// Fields

	private Long cartId;
	private String UId;
	private String GId;
	private String pack;
	private String GNumber;
	private String GPrice;
	private BigDecimal cupon;
	private String parprice;
	private String parnumber;
	private String carttype;
	private String sample;
	private String partial;
	private String carttime;
	private String GName;
	private String GSn;
	private String GUnit;
	private BigDecimal marketPrice;
	private BigDecimal shopPrice;
	private BigDecimal promotePrice;
	private String pack1;
	private BigDecimal pack1Num;
	private String pack2;
	private BigDecimal pack2Num;
	private String pack3;
	private BigDecimal pack3Num;
	private String pack4;
	private BigDecimal pack4Num;
	private String PUrl;

	// Constructors

	public String getParprice() {
		return parprice;
	}

	public String getSample() {
		return sample;
	}

	public String getCarttime() {
		return carttime;
	}

	public void setCarttime(String carttime) {
		this.carttime = carttime;
	}

	public void setSample(String sample) {
		this.sample = sample;
	}

	public String getPartial() {
		return partial;
	}

	public void setPartial(String partial) {
		this.partial = partial;
	}

	public void setParprice(String parprice) {
		this.parprice = parprice;
	}

	public String getParnumber() {
		return parnumber;
	}

	public void setParnumber(String parnumber) {
		this.parnumber = parnumber;
	}

	public String getCarttype() {
		return carttype;
	}

	public void setCarttype(String carttype) {
		this.carttype = carttype;
	}

	/** default constructor */
	public ViewCartListDTO() {
	}

	/** minimal constructor */
	public ViewCartListDTO(Long cartId, String UId, String GId) {
		this.cartId = cartId;
		this.UId = UId;
		this.GId = GId;
	}

	/** full constructor */
	public ViewCartListDTO(Long cartId, String UId, String GId, String pack,
			String GNumber, String GPrice, BigDecimal cupon, String GName,
			String GSn, String GUnit, BigDecimal marketPrice,
			BigDecimal shopPrice, BigDecimal promotePrice, String pack1,
			BigDecimal pack1Num, String pack2, BigDecimal pack2Num,
			String pack3, BigDecimal pack3Num, String pack4,
			BigDecimal pack4Num, String PUrl) {
		this.cartId = cartId;
		this.UId = UId;
		this.GId = GId;
		this.pack = pack;
		this.GNumber = GNumber;
		this.GPrice = GPrice;
		this.cupon = cupon;
		this.GName = GName;
		this.GSn = GSn;
		this.GUnit = GUnit;
		this.marketPrice = marketPrice;
		this.shopPrice = shopPrice;
		this.promotePrice = promotePrice;
		this.pack1 = pack1;
		this.pack1Num = pack1Num;
		this.pack2 = pack2;
		this.pack2Num = pack2Num;
		this.pack3 = pack3;
		this.pack3Num = pack3Num;
		this.pack4 = pack4;
		this.pack4Num = pack4Num;
		this.PUrl = PUrl;
	}

	// Property accessors

	public Long getCartId() {
		return this.cartId;
	}

	public void setCartId(Long cartId) {
		this.cartId = cartId;
	}

	public String getUId() {
		return this.UId;
	}

	public void setUId(String UId) {
		this.UId = UId;
	}

	public String getGId() {
		return this.GId;
	}

	public void setGId(String GId) {
		this.GId = GId;
	}

	public String getPack() {
		return this.pack;
	}

	public void setPack(String pack) {
		this.pack = pack;
	}

	public String getGNumber() {
		return this.GNumber;
	}

	public void setGNumber(String GNumber) {
		this.GNumber = GNumber;
	}

	public String getGPrice() {
		return this.GPrice;
	}

	public void setGPrice(String GPrice) {
		this.GPrice = GPrice;
	}

	public BigDecimal getCupon() {
		return this.cupon;
	}

	public void setCupon(BigDecimal cupon) {
		this.cupon = cupon;
	}

	public String getGName() {
		return this.GName;
	}

	public void setGName(String GName) {
		this.GName = GName;
	}

	public String getGSn() {
		return this.GSn;
	}

	public void setGSn(String GSn) {
		this.GSn = GSn;
	}

	public String getGUnit() {
		return this.GUnit;
	}

	public void setGUnit(String GUnit) {
		this.GUnit = GUnit;
	}

	public BigDecimal getMarketPrice() {
		return this.marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	public BigDecimal getShopPrice() {
		return this.shopPrice;
	}

	public void setShopPrice(BigDecimal shopPrice) {
		this.shopPrice = shopPrice;
	}

	public BigDecimal getPromotePrice() {
		return this.promotePrice;
	}

	public void setPromotePrice(BigDecimal promotePrice) {
		this.promotePrice = promotePrice;
	}

	public String getPack1() {
		return this.pack1;
	}

	public void setPack1(String pack1) {
		this.pack1 = pack1;
	}

	public BigDecimal getPack1Num() {
		return this.pack1Num;
	}

	public void setPack1Num(BigDecimal pack1Num) {
		this.pack1Num = pack1Num;
	}

	public String getPack2() {
		return this.pack2;
	}

	public void setPack2(String pack2) {
		this.pack2 = pack2;
	}

	public BigDecimal getPack2Num() {
		return this.pack2Num;
	}

	public void setPack2Num(BigDecimal pack2Num) {
		this.pack2Num = pack2Num;
	}

	public String getPack3() {
		return this.pack3;
	}

	public void setPack3(String pack3) {
		this.pack3 = pack3;
	}

	public BigDecimal getPack3Num() {
		return this.pack3Num;
	}

	public void setPack3Num(BigDecimal pack3Num) {
		this.pack3Num = pack3Num;
	}

	public String getPack4() {
		return this.pack4;
	}

	public void setPack4(String pack4) {
		this.pack4 = pack4;
	}

	public BigDecimal getPack4Num() {
		return this.pack4Num;
	}

	public void setPack4Num(BigDecimal pack4Num) {
		this.pack4Num = pack4Num;
	}

	public String getPUrl() {
		return this.PUrl;
	}

	public void setPUrl(String PUrl) {
		this.PUrl = PUrl;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ViewCartListDTO))
			return false;
		ViewCartListDTO castOther = (ViewCartListDTO) other;

		return ((this.getCartId() == castOther.getCartId()) || (this
				.getCartId() != null && castOther.getCartId() != null && this
				.getCartId().equals(castOther.getCartId())))
				&& ((this.getUId() == castOther.getUId()) || (this.getUId() != null
						&& castOther.getUId() != null && this.getUId().equals(
						castOther.getUId())))
				&& ((this.getGId() == castOther.getGId()) || (this.getGId() != null
						&& castOther.getGId() != null && this.getGId().equals(
						castOther.getGId())))
				&& ((this.getPack() == castOther.getPack()) || (this.getPack() != null
						&& castOther.getPack() != null && this.getPack()
						.equals(castOther.getPack())))
				&& ((this.getGNumber() == castOther.getGNumber()) || (this
						.getGNumber() != null && castOther.getGNumber() != null && this
						.getGNumber().equals(castOther.getGNumber())))
				&& ((this.getGPrice() == castOther.getGPrice()) || (this
						.getGPrice() != null && castOther.getGPrice() != null && this
						.getGPrice().equals(castOther.getGPrice())))
				&& ((this.getCupon() == castOther.getCupon()) || (this
						.getCupon() != null && castOther.getCupon() != null && this
						.getCupon().equals(castOther.getCupon())))
				&& ((this.getGName() == castOther.getGName()) || (this
						.getGName() != null && castOther.getGName() != null && this
						.getGName().equals(castOther.getGName())))
				&& ((this.getGSn() == castOther.getGSn()) || (this.getGSn() != null
						&& castOther.getGSn() != null && this.getGSn().equals(
						castOther.getGSn())))
				&& ((this.getGUnit() == castOther.getGUnit()) || (this
						.getGUnit() != null && castOther.getGUnit() != null && this
						.getGUnit().equals(castOther.getGUnit())))
				&& ((this.getMarketPrice() == castOther.getMarketPrice()) || (this
						.getMarketPrice() != null
						&& castOther.getMarketPrice() != null && this
						.getMarketPrice().equals(castOther.getMarketPrice())))
				&& ((this.getShopPrice() == castOther.getShopPrice()) || (this
						.getShopPrice() != null
						&& castOther.getShopPrice() != null && this
						.getShopPrice().equals(castOther.getShopPrice())))
				&& ((this.getPromotePrice() == castOther.getPromotePrice()) || (this
						.getPromotePrice() != null
						&& castOther.getPromotePrice() != null && this
						.getPromotePrice().equals(castOther.getPromotePrice())))
				&& ((this.getPack1() == castOther.getPack1()) || (this
						.getPack1() != null && castOther.getPack1() != null && this
						.getPack1().equals(castOther.getPack1())))
				&& ((this.getPack1Num() == castOther.getPack1Num()) || (this
						.getPack1Num() != null
						&& castOther.getPack1Num() != null && this
						.getPack1Num().equals(castOther.getPack1Num())))
				&& ((this.getPack2() == castOther.getPack2()) || (this
						.getPack2() != null && castOther.getPack2() != null && this
						.getPack2().equals(castOther.getPack2())))
				&& ((this.getPack2Num() == castOther.getPack2Num()) || (this
						.getPack2Num() != null
						&& castOther.getPack2Num() != null && this
						.getPack2Num().equals(castOther.getPack2Num())))
				&& ((this.getPack3() == castOther.getPack3()) || (this
						.getPack3() != null && castOther.getPack3() != null && this
						.getPack3().equals(castOther.getPack3())))
				&& ((this.getPack3Num() == castOther.getPack3Num()) || (this
						.getPack3Num() != null
						&& castOther.getPack3Num() != null && this
						.getPack3Num().equals(castOther.getPack3Num())))
				&& ((this.getPack4() == castOther.getPack4()) || (this
						.getPack4() != null && castOther.getPack4() != null && this
						.getPack4().equals(castOther.getPack4())))
				&& ((this.getPack4Num() == castOther.getPack4Num()) || (this
						.getPack4Num() != null
						&& castOther.getPack4Num() != null && this
						.getPack4Num().equals(castOther.getPack4Num())))
				&& ((this.getPUrl() == castOther.getPUrl()) || (this.getPUrl() != null
						&& castOther.getPUrl() != null && this.getPUrl()
						.equals(castOther.getPUrl())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getCartId() == null ? 0 : this.getCartId().hashCode());
		result = 37 * result
				+ (getUId() == null ? 0 : this.getUId().hashCode());
		result = 37 * result
				+ (getGId() == null ? 0 : this.getGId().hashCode());
		result = 37 * result
				+ (getPack() == null ? 0 : this.getPack().hashCode());
		result = 37 * result
				+ (getGNumber() == null ? 0 : this.getGNumber().hashCode());
		result = 37 * result
				+ (getGPrice() == null ? 0 : this.getGPrice().hashCode());
		result = 37 * result
				+ (getCupon() == null ? 0 : this.getCupon().hashCode());
		result = 37 * result
				+ (getGName() == null ? 0 : this.getGName().hashCode());
		result = 37 * result
				+ (getGSn() == null ? 0 : this.getGSn().hashCode());
		result = 37 * result
				+ (getGUnit() == null ? 0 : this.getGUnit().hashCode());
		result = 37
				* result
				+ (getMarketPrice() == null ? 0 : this.getMarketPrice()
						.hashCode());
		result = 37 * result
				+ (getShopPrice() == null ? 0 : this.getShopPrice().hashCode());
		result = 37
				* result
				+ (getPromotePrice() == null ? 0 : this.getPromotePrice()
						.hashCode());
		result = 37 * result
				+ (getPack1() == null ? 0 : this.getPack1().hashCode());
		result = 37 * result
				+ (getPack1Num() == null ? 0 : this.getPack1Num().hashCode());
		result = 37 * result
				+ (getPack2() == null ? 0 : this.getPack2().hashCode());
		result = 37 * result
				+ (getPack2Num() == null ? 0 : this.getPack2Num().hashCode());
		result = 37 * result
				+ (getPack3() == null ? 0 : this.getPack3().hashCode());
		result = 37 * result
				+ (getPack3Num() == null ? 0 : this.getPack3Num().hashCode());
		result = 37 * result
				+ (getPack4() == null ? 0 : this.getPack4().hashCode());
		result = 37 * result
				+ (getPack4Num() == null ? 0 : this.getPack4Num().hashCode());
		result = 37 * result
				+ (getPUrl() == null ? 0 : this.getPUrl().hashCode());
		return result;
	}

}
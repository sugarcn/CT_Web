package org.ctonline.dto.help;

public class CtHelpDTO {
	private Integer HId;
	private Integer HTypeId;
	private String HTitle;
	private int[] mid;
	private String HDesc;
	private String HTypeName;
	private int page;
	private int id;
	private String keyword;
	private String url;
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getHTypeName() {
		return HTypeName;
	}
	public void setHTypeName(String hTypeName) {
		HTypeName = hTypeName;
	}
	public Integer getHId() {
		return HId;
	}
	public void setHId(Integer hId) {
		HId = hId;
	}
	public Integer getHTypeId() {
		return HTypeId;
	}
	public void setHTypeId(Integer hTypeId) {
		HTypeId = hTypeId;
	}
	public String getHTitle() {
		return HTitle;
	}
	public void setHTitle(String hTitle) {
		HTitle = hTitle;
	}
	public int[] getMid() {
		return mid;
	}
	public void setMid(int[] mid) {
		this.mid = mid;
	}
	public String getHDesc() {
		return HDesc;
	}
	public void setHDesc(String hDesc) {
		HDesc = hDesc;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	
}

package org.ctonline.dto.help;

public class CtHelpTypeDTO {
	private Integer HTypeId;
	private String HTypeName;
	private int page;
	private String keyword;
	private int[] mid;
	private int id;
	private int AdId;	
	private String url;
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getHTypeId() {
		return HTypeId;
	}
	public void setHTypeId(Integer hTypeId) {
		HTypeId = hTypeId;
	}
	public String getHTypeName() {
		return HTypeName;
	}
	public void setHTypeName(String hTypeName) {
		HTypeName = hTypeName;
	}
	public int[] getMid() {
		return mid;
	}
	public void setMid(int[] mid) {
		this.mid = mid;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public int getAdId() {
		return AdId;
	}
	public void setAdId(int adId) {
		AdId = adId;
	}
	
}

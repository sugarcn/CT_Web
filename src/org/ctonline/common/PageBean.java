package org.ctonline.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.passingit.middleware.core.modules.buiz.template.PageInfo;

public class PageBean extends PageInfo {
	private static final long serialVersionUID = 1L;

	public PageBean() {
		this.setCurrentPage(1);
		this.setPerpage(10);
	}

	private int total;

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	private String totalpage = "10";

	private List<String> pagelist;

	public List<String> getPagelist() {
		return pagelist;
	}

	public void setPagelist(List<String> pagelist) {
		this.pagelist = pagelist;
	}

	public String getTotalpage() {
		return totalpage;
	}

	public void setTotalpage(String totalpage) {
		this.totalpage = totalpage;
	}

	public void createPage() {

		pagelist = new ArrayList<String>();
		if (new BigDecimal(getTotalSum())
				.compareTo(new BigDecimal(getPerpage())) > 0) {
			total = new BigDecimal(getTotalSum()).divide(
					new BigDecimal(getPerpage()), BigDecimal.ROUND_UP)
					.intValue();
		} else {
			total = 1;
		}
		if (total == 1) {
			pagelist.add("1");
		} else if (Integer.valueOf(totalpage) > total) {

			for (int i = 1; i <= total; i++) {
				pagelist.add("" + i);
			}
		} else {
			BigDecimal currentdecimal = new BigDecimal(getCurrentPage());
			BigDecimal totaldecimal = new BigDecimal(total);
			int startpage = 1;
			if (currentdecimal.add(new BigDecimal(-2)).compareTo(
					new BigDecimal(0)) < 1) {
				startpage = 1;

			} else if (currentdecimal.add(new BigDecimal(3)).compareTo(
					totaldecimal) > 0) {
				startpage = totaldecimal.add(
						new BigDecimal(-Integer.valueOf(totalpage) + 1))
						.intValue();
			} else {
				startpage = currentdecimal.add(new BigDecimal(-2)).intValue();
			}
			int size = Integer.valueOf(totalpage) + startpage;
			for (int i = startpage; i < size; i++) {
				pagelist.add("" + i);
			}
		}
	}
}

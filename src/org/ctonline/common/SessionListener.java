package org.ctonline.common;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

import org.ctonline.po.user.CtUser;

/**
 * 单点登录（一方登录在另一方登录后剔除已经登录的账号）
 * @author kyle
 *
 */
public class SessionListener implements HttpSessionAttributeListener {
	public static Map<String, HttpSession> sessionMap = new HashMap<String, HttpSession>();
	public void attributeAdded(HttpSessionBindingEvent event) {
	        String name = event.getName(); 
	        if("userinfosession".equals(name)){
	            CtUser user = (CtUser) event.getValue();
	            if (sessionMap.get(user.getUUsername()) != null) {  
	                HttpSession session = sessionMap.get(user.getUUsername());  
	                session.removeAttribute(user.getUUsername());  
	                session.invalidate();  
	            }
	            sessionMap.put(user.getUUsername(), event.getSession()); 
	        }    
	}
	public void attributeRemoved(HttpSessionBindingEvent event) {
	        String name = event.getName();  
	        if (name.equals("userinfosession")) {  
	        	CtUser user = (CtUser) event.getValue();  
	            sessionMap.remove(user.getUUsername());  
	        }            // TODO Auto-generated method stub    
	}
	public static Map<String, HttpSession> getSessionMap() {
	        return sessionMap;
	}
	public static void setSessionMap(Map<String, HttpSession> sessionMap) {
		SessionListener.sessionMap = sessionMap;
	}
	@Override
	public void attributeReplaced(HttpSessionBindingEvent event) {
		
	}
}

package org.ctonline.po.order;

/**
 * CtComplain entity. @author MyEclipse Persistence Tools
 */

public class CtComplain implements java.io.Serializable {

	// Fields

	private Integer comId;
	private String comType;
	private Long orderId;
	private Long UId;
	private String UTel;
	private String comContent;
	private String comFile;
	private String comDesc;
	private String comStatus;
	private String ansContent;
	private String ansTips;
	
	public String getAnsContent() {
		return ansContent;
	}

	public void setAnsContent(String ansContent) {
		this.ansContent = ansContent;
	}

	public String getAnsTips() {
		return ansTips;
	}

	public void setAnsTips(String ansTips) {
		this.ansTips = ansTips;
	}

	private CtOrderInfo orderInfo;
	
	// Constructors

	public CtOrderInfo getOrderInfo() {
		return orderInfo;
	}

	public void setOrderInfo(CtOrderInfo orderInfo) {
		this.orderInfo = orderInfo;
	}

	/** default constructor */
	public CtComplain() {
	}

	/** minimal constructor */
	public CtComplain(Long UId) {
		this.UId = UId;
	}

	/** full constructor */
	public CtComplain(String comType, Long orderId, Long UId, String UTel,
			String comContent, String comFile, String comDesc, String comStatus) {
		this.comType = comType;
		this.orderId = orderId;
		this.UId = UId;
		this.UTel = UTel;
		this.comContent = comContent;
		this.comFile = comFile;
		this.comDesc = comDesc;
		this.comStatus = comStatus;
	}

	// Property accessors

	public Integer getComId() {
		return this.comId;
	}

	public void setComId(Integer comId) {
		this.comId = comId;
	}

	public String getComType() {
		return this.comType;
	}

	public void setComType(String comType) {
		this.comType = comType;
	}

	public Long getOrderId() {
		return this.orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Long getUId() {
		return this.UId;
	}

	public void setUId(Long UId) {
		this.UId = UId;
	}

	public String getUTel() {
		return this.UTel;
	}

	public void setUTel(String UTel) {
		this.UTel = UTel;
	}

	public String getComContent() {
		return this.comContent;
	}

	public void setComContent(String comContent) {
		this.comContent = comContent;
	}

	public String getComFile() {
		return this.comFile;
	}

	public void setComFile(String comFile) {
		this.comFile = comFile;
	}

	public String getComDesc() {
		return this.comDesc;
	}

	public void setComDesc(String comDesc) {
		this.comDesc = comDesc;
	}

	public String getComStatus() {
		return this.comStatus;
	}

	public void setComStatus(String comStatus) {
		this.comStatus = comStatus;
	}

}
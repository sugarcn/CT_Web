package org.ctonline.po.order;

import java.io.File;

/**
 * CtPay entity. @author MyEclipse Persistence Tools
 */

public class CtPay implements java.io.Serializable {

	// Fields

	private Long payId;
	private Long orderId;
	private String orderSn;
	private Byte bankId;
	private String CName;
	private String CTel;
	private String CAName;
	private Double payAmount;
	private String payDate;
	private String payDesc;
	private String payFile;
	
	private String url;
	private String url1;
	
	private File fileName;
	private String upFileName;
	
	// Constructors

	public String getOrderSn() {
		return orderSn;
	}

	public void setOrderSn(String orderSn) {
		this.orderSn = orderSn;
	}

	public String getUpFileName() {
		return upFileName;
	}

	public void setUpFileName(String upFileName) {
		this.upFileName = upFileName;
	}

	public File getFileName() {
		return fileName;
	}

	public void setFileName(File fileName) {
		this.fileName = fileName;
	}

	public String getUrl1() {
		return url1;
	}

	public void setUrl1(String url1) {
		this.url1 = url1;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	/** default constructor */
	public CtPay() {
	}

	/** minimal constructor */
	public CtPay(Long orderId, Byte bankId) {
		this.orderId = orderId;
		this.bankId = bankId;
	}

	/** full constructor */
	public CtPay(Long orderId, Byte bankId, String CName, String CTel,
			String CAName, Double payAmount, String payDate, String payDesc,
			String payFile) {
		this.orderId = orderId;
		this.bankId = bankId;
		this.CName = CName;
		this.CTel = CTel;
		this.CAName = CAName;
		this.payAmount = payAmount;
		this.payDate = payDate;
		this.payDesc = payDesc;
		this.payFile = payFile;
	}

	// Property accessors

	public Long getPayId() {
		return this.payId;
	}

	public void setPayId(Long payId) {
		this.payId = payId;
	}

	public Long getOrderId() {
		return this.orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Byte getBankId() {
		return this.bankId;
	}

	public void setBankId(Byte bankId) {
		this.bankId = bankId;
	}

	public String getCName() {
		return this.CName;
	}

	public void setCName(String CName) {
		this.CName = CName;
	}

	public String getCTel() {
		return this.CTel;
	}

	public void setCTel(String CTel) {
		this.CTel = CTel;
	}

	public String getCAName() {
		return this.CAName;
	}

	public void setCAName(String CAName) {
		this.CAName = CAName;
	}

	public Double getPayAmount() {
		return this.payAmount;
	}

	public void setPayAmount(Double payAmount) {
		this.payAmount = payAmount;
	}

	public String getPayDate() {
		return this.payDate;
	}

	public void setPayDate(String payDate) {
		this.payDate = payDate;
	}

	public String getPayDesc() {
		return this.payDesc;
	}

	public void setPayDesc(String payDesc) {
		this.payDesc = payDesc;
	}

	public String getPayFile() {
		return this.payFile;
	}

	public void setPayFile(String payFile) {
		this.payFile = payFile;
	}

}
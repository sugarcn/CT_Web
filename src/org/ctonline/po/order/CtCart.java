package org.ctonline.po.order;

/**
 * CtCart entity. @author MyEclipse Persistence Tools
 */

public class CtCart implements java.io.Serializable {

	// Fields

	private Long cartId;
	private String UId;
	private String GId;
	private String GPrice;
	private Long GNumber;
	private String pack;
	private Long cupon;
	private String GParPrice;
	private Long GParNumber;
	private String cartType;
	private String cartAddTime;
	
	// Constructors

	public String getCartAddTime() {
		return cartAddTime;
	}

	public void setCartAddTime(String cartAddTime) {
		this.cartAddTime = cartAddTime;
	}

	/** default constructor */
	public CtCart() {
	}

	/** minimal constructor */
	public CtCart(String UId, String GId, String GPrice, Long GNumber) {
		this.UId = UId;
		this.GId = GId;
		this.GPrice = GPrice;
		this.GNumber = GNumber;
	}

	/** full constructor */
	public CtCart(String UId, String GId, String GPrice, Long GNumber,
			String pack, Long cupon, String GParPrice, Long GParNumber,
			String cartType) {
		this.UId = UId;
		this.GId = GId;
		this.GPrice = GPrice;
		this.GNumber = GNumber;
		this.pack = pack;
		this.cupon = cupon;
		this.GParPrice = GParPrice;
		this.GParNumber = GParNumber;
		this.cartType = cartType;
	}

	// Property accessors

	public Long getCartId() {
		return this.cartId;
	}

	public void setCartId(Long cartId) {
		this.cartId = cartId;
	}

	public String getUId() {
		return this.UId;
	}

	public void setUId(String UId) {
		this.UId = UId;
	}

	public String getGId() {
		return this.GId;
	}

	public void setGId(String GId) {
		this.GId = GId;
	}

	public String getGPrice() {
		return this.GPrice;
	}

	public void setGPrice(String GPrice) {
		this.GPrice = GPrice;
	}

	public Long getGNumber() {
		return this.GNumber;
	}

	public void setGNumber(Long GNumber) {
		this.GNumber = GNumber;
	}

	public String getPack() {
		return this.pack;
	}

	public void setPack(String pack) {
		this.pack = pack;
	}

	public Long getCupon() {
		return this.cupon;
	}

	public void setCupon(Long cupon) {
		this.cupon = cupon;
	}

	public String getGParPrice() {
		return this.GParPrice;
	}

	public void setGParPrice(String GParPrice) {
		this.GParPrice = GParPrice;
	}

	public Long getGParNumber() {
		return this.GParNumber;
	}

	public void setGParNumber(Long GParNumber) {
		this.GParNumber = GParNumber;
	}

	public String getCartType() {
		return this.cartType;
	}

	public void setCartType(String cartType) {
		this.cartType = cartType;
	}

}
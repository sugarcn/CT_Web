package org.ctonline.po.order;

import org.ctonline.po.user.CtUser;

/**
 * CtEvaluation entity. @author MyEclipse Persistence Tools
 */

public class CtEvaluation implements java.io.Serializable {

	// Fields

	private Long evaId;
	private Long orderId;
	private Long UId;
	private String evaDesc;
	private String evaTime;
	private CtUser ctUser;
	
	// Constructors

	public CtUser getCtUser() {
		return ctUser;
	}

	public void setCtUser(CtUser ctUser) {
		this.ctUser = ctUser;
	}

	/** default constructor */
	public CtEvaluation() {
	}

	/** full constructor */
	public CtEvaluation(Long orderId, Long UId, String evaDesc, String evaTime) {
		this.orderId = orderId;
		this.UId = UId;
		this.evaDesc = evaDesc;
		this.evaTime = evaTime;
	}

	// Property accessors

	public Long getEvaId() {
		return this.evaId;
	}

	public void setEvaId(Long evaId) {
		this.evaId = evaId;
	}

	public Long getOrderId() {
		return this.orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Long getUId() {
		return this.UId;
	}

	public void setUId(Long UId) {
		this.UId = UId;
	}

	public String getEvaDesc() {
		return this.evaDesc;
	}

	public void setEvaDesc(String evaDesc) {
		this.evaDesc = evaDesc;
	}

	public String getEvaTime() {
		return this.evaTime;
	}

	public void setEvaTime(String evaTime) {
		this.evaTime = evaTime;
	}

}
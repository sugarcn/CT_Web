package org.ctonline.po.order;

/**
 * CtShop entity. @author MyEclipse Persistence Tools
 */

public class CtShop implements java.io.Serializable {

	// Fields

	private Short SId;
	private String shopName;
	private String shopAdd;
	private String shopTel;
	private String shopDesc;

	// Constructors

	/** default constructor */
	public CtShop() {
	}

	/** full constructor */
	public CtShop(String shopName, String shopAdd, String shopTel,
			String shopDesc) {
		this.shopName = shopName;
		this.shopAdd = shopAdd;
		this.shopTel = shopTel;
		this.shopDesc = shopDesc;
	}

	// Property accessors

	public Short getSId() {
		return this.SId;
	}

	public void setSId(Short SId) {
		this.SId = SId;
	}

	public String getShopName() {
		return this.shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getShopAdd() {
		return this.shopAdd;
	}

	public void setShopAdd(String shopAdd) {
		this.shopAdd = shopAdd;
	}

	public String getShopTel() {
		return this.shopTel;
	}

	public void setShopTel(String shopTel) {
		this.shopTel = shopTel;
	}

	public String getShopDesc() {
		return this.shopDesc;
	}

	public void setShopDesc(String shopDesc) {
		this.shopDesc = shopDesc;
	}

}
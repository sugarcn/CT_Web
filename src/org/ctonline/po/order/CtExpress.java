package org.ctonline.po.order;

/**
 * CtExpress entity. @author MyEclipse Persistence Tools
 */

public class CtExpress implements java.io.Serializable {

	// Fields

	private Byte exId;
	private String exName;
	private String exUrl;
	private String exDesc;

	// Constructors

	/** default constructor */
	public CtExpress() {
	}

	/** full constructor */
	public CtExpress(String exName, String exUrl, String exDesc) {
		this.exName = exName;
		this.exUrl = exUrl;
		this.exDesc = exDesc;
	}

	// Property accessors

	public Byte getExId() {
		return this.exId;
	}

	public void setExId(Byte exId) {
		this.exId = exId;
	}

	public String getExName() {
		return this.exName;
	}

	public void setExName(String exName) {
		this.exName = exName;
	}

	public String getExUrl() {
		return this.exUrl;
	}

	public void setExUrl(String exUrl) {
		this.exUrl = exUrl;
	}

	public String getExDesc() {
		return this.exDesc;
	}

	public void setExDesc(String exDesc) {
		this.exDesc = exDesc;
	}

}
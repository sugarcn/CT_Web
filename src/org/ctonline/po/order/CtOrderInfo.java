package org.ctonline.po.order;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.user.CtUser;

/**
 * CtOrderInfo entity. @author MyEclipse Persistence Tools
 */

public class CtOrderInfo implements java.io.Serializable {

	// Fields

	private Long orderId;
	private String orderSn;
	private String UId;
	private String orderStatus;
	private String consignee;
	private Integer country;
	private Integer province;
	private Integer city;
	private Integer district;
	private String address;
	private String zipcode;
	private String tel;
	private String shippingType;
	private String invoiceType;
	private String invoice;
	private String pay;
	private String  couponId;
	private String freight;
	private String total;
	private String discount;
	private String dsc;
	private String orderTime;
	private String deliverTime;
	private String isOkTime;
	private Byte exId;
	private String exNo;
	private Long evaId;
	private String evaDesc;
	private String evaTime;
	private String orderIp;
	private Boolean shoppingTypeCollect;
	private List<CtOrderGoods> orderGoodsList = new ArrayList<CtOrderGoods>();
	private String isRefInfo;
	private CtRegion region;
	private String stokingTime;
	
	private String kfDsc;
	
	
	public String getKfDsc() {
		return kfDsc;
	}

	public void setKfDsc(String kfDsc) {
		this.kfDsc = kfDsc;
	}

	public String getStokingTime() {
		return stokingTime;
	}

	public void setStokingTime(String stokingTime) {
		this.stokingTime = stokingTime;
	}

	public CtRegion getRegion() {
		return region;
	}

	public void setRegion(CtRegion region) {
		this.region = region;
	}

	public String getIsRefInfo() {
		return isRefInfo;
	}

	public void setIsRefInfo(String isRefInfo) {
		this.isRefInfo = isRefInfo;
	}

	public List<CtOrderGoods> getOrderGoodsList() {
		return orderGoodsList;
	}

	public void setOrderGoodsList(List<CtOrderGoods> orderGoodsList) {
		this.orderGoodsList = orderGoodsList;
	}

	public Boolean getShoppingTypeCollect() {
		return shoppingTypeCollect;
	}

	public void setShoppingTypeCollect(Boolean shoppingTypeCollect) {
		this.shoppingTypeCollect = shoppingTypeCollect;
	}

	public String getOrderIp() {
		return orderIp;
	}

	public void setOrderIp(String orderIp) {
		this.orderIp = orderIp;
	}

	private CtUser ctUser;
	
	private String isParOrSim;
	
	private List<CtOrderInfo> tempListOrder = new ArrayList<CtOrderInfo>();
	
	
	public List<CtOrderInfo> getTempListOrder() {
		return tempListOrder;
	}

	public void setTempListOrder(List<CtOrderInfo> tempListOrder) {
		this.tempListOrder = tempListOrder;
	}

	public String getIsParOrSim() {
		return isParOrSim;
	}

	public void setIsParOrSim(String isParOrSim) {
		this.isParOrSim = isParOrSim;
	}

	private Set<CtOrderGoods> orderGoods = new HashSet<CtOrderGoods>(); 

	
	public Set<CtOrderGoods> getOrderGoods() {
		return orderGoods;
	}

	public void setOrderGoods(Set<CtOrderGoods> orderGoods) {
		this.orderGoods = orderGoods;
	}

	private Double retNumber; 
	private String retTime; 
	private String retStatus; 
	
	private Short SId;
	
	
 	// Constructors


	public Short getSId() {
		return SId;
	}

	public void setSId(Short sId) {
		SId = sId;
	}

	public CtUser getCtUser() {
		return ctUser;
	}

	public void setCtUser(CtUser ctUser) {
		this.ctUser = ctUser;
	}

	public Double getRetNumber() {
		return retNumber;
	}

	public void setRetNumber(Double retNumber) {
		this.retNumber = retNumber;
	}

	public String getRetTime() {
		return retTime;
	}

	public void setRetTime(String retTime) {
		this.retTime = retTime;
	}

	public String getRetStatus() {
		return retStatus;
	}

	public void setRetStatus(String retStatus) {
		this.retStatus = retStatus;
	}

	public String getEvaDesc() {
		return evaDesc;
	}

	public void setEvaDesc(String evaDesc) {
		this.evaDesc = evaDesc;
	}

	public String getEvaTime() {
		return evaTime;
	}

	public void setEvaTime(String evaTime) {
		this.evaTime = evaTime;
	}

	public Long getEvaId() {
		return evaId;
	}

	public void setEvaId(Long evaId) {
		this.evaId = evaId;
	}

	public String getExNo() {
		return exNo;
	}

	public void setExNo(String exNo) {
		this.exNo = exNo;
	}

	public Byte getExId() {
		return exId;
	}

	public void setExId(Byte exId) {
		this.exId = exId;
	}

	public String getDeliverTime() {
		return deliverTime;
	}

	public void setDeliverTime(String deliverTime) {
		this.deliverTime = deliverTime;
	}

	public String getIsOkTime() {
		return isOkTime;
	}

	public void setIsOkTime(String isOkTime) {
		this.isOkTime = isOkTime;
	}

	/** default constructor */
	public CtOrderInfo() {
	}

	/** minimal constructor */
	public CtOrderInfo(String orderSn, String UId, String orderStatus,
			String consignee) {
		this.orderSn = orderSn;
		this.UId = UId;
		this.orderStatus = orderStatus;
		this.consignee = consignee;
	}

	/** full constructor */
	public CtOrderInfo(String orderSn, String UId, String orderStatus,
			String consignee, Integer country, Integer province, Integer city,
			Integer district, String address, String zipcode, String tel,
			String shippingType, String invoiceType, String invoice,
			String pay, String  couponId, String freight, String total,
			String discount, String dsc, String orderTime) {
		this.orderSn = orderSn;
		this.UId = UId;
		this.orderStatus = orderStatus;
		this.consignee = consignee;
		this.country = country;
		this.province = province;
		this.city = city;
		this.district = district;
		this.address = address;
		this.zipcode = zipcode;
		this.tel = tel;
		this.shippingType = shippingType;
		this.invoiceType = invoiceType;
		this.invoice = invoice;
		this.pay = pay;
		this.couponId = couponId;
		this.freight = freight;
		this.total = total;
		this.discount = discount;
		this.dsc = dsc;
		this.orderTime = orderTime;
	}

	// Property accessors

	public Long getOrderId() {
		return this.orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getOrderSn() {
		return this.orderSn;
	}

	public void setOrderSn(String orderSn) {
		this.orderSn = orderSn;
	}

	public String getUId() {
		return this.UId;
	}

	public void setUId(String UId) {
		this.UId = UId;
	}

	public String getOrderStatus() {
		return this.orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getConsignee() {
		return this.consignee;
	}

	public void setConsignee(String consignee) {
		this.consignee = consignee;
	}

	public Integer getCountry() {
		return this.country;
	}

	public void setCountry(Integer country) {
		this.country = country;
	}

	public Integer getProvince() {
		return this.province;
	}

	public void setProvince(Integer province) {
		this.province = province;
	}

	public Integer getCity() {
		return this.city;
	}

	public void setCity(Integer city) {
		this.city = city;
	}

	public Integer getDistrict() {
		return this.district;
	}

	public void setDistrict(Integer district) {
		this.district = district;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZipcode() {
		return this.zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getTel() {
		return this.tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getShippingType() {
		return this.shippingType;
	}

	public void setShippingType(String shippingType) {
		this.shippingType = shippingType;
	}

	public String getInvoiceType() {
		return this.invoiceType;
	}

	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}

	public String getInvoice() {
		return this.invoice;
	}

	public void setInvoice(String invoice) {
		this.invoice = invoice;
	}

	public String getPay() {
		return this.pay;
	}

	public void setPay(String pay) {
		this.pay = pay;
	}

	public String getCouponId() {
		return this.couponId;
	}

	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}

	public String getFreight() {
		return this.freight;
	}

	public void setFreight(String freight) {
		this.freight = freight;
	}

	public String getTotal() {
		return this.total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getDiscount() {
		return this.discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getDsc() {
		return this.dsc;
	}

	public void setDsc(String dsc) {
		this.dsc = dsc;
	}

	public String getOrderTime() {
		return this.orderTime;
	}

	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}

}
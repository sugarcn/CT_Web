package org.ctonline.po.order;

/**
 * CtRefund entity. @author MyEclipse Persistence Tools
 */

public class CtRefund implements java.io.Serializable {

	// Fields

	private Integer refId;
	private Long refOrderid;
	private Long refGoodsid;
	private String refCtrefid;
	private String refPayId;
	private String refStatus;
	private String refPaytype;

	private String refGoodsType;
	
	
	// Constructors

	public String getRefGoodsType() {
		return refGoodsType;
	}

	public void setRefGoodsType(String refGoodsType) {
		this.refGoodsType = refGoodsType;
	}

	/** default constructor */
	public CtRefund() {
	}

	/** full constructor */
	public CtRefund(Long refOrderid, Long refGoodsid, String refCtrefid,
			String refType, String refPayId, String refStatus,
			String refPaytype) {
		this.refOrderid = refOrderid;
		this.refGoodsid = refGoodsid;
		this.refCtrefid = refCtrefid;
		this.refPayId = refPayId;
		this.refStatus = refStatus;
		this.refPaytype = refPaytype;
	}

	// Property accessors

	public Integer getRefId() {
		return this.refId;
	}

	public void setRefId(Integer refId) {
		this.refId = refId;
	}

	public Long getRefOrderid() {
		return this.refOrderid;
	}

	public void setRefOrderid(Long refOrderid) {
		this.refOrderid = refOrderid;
	}

	public Long getRefGoodsid() {
		return this.refGoodsid;
	}

	public void setRefGoodsid(Long refGoodsid) {
		this.refGoodsid = refGoodsid;
	}

	public String getRefCtrefid() {
		return this.refCtrefid;
	}

	public void setRefCtrefid(String refCtrefid) {
		this.refCtrefid = refCtrefid;
	}


	public String getRefPayId() {
		return this.refPayId;
	}

	public void setRefPayId(String refPayId) {
		this.refPayId = refPayId;
	}

	public String getRefStatus() {
		return this.refStatus;
	}

	public void setRefStatus(String refStatus) {
		this.refStatus = refStatus;
	}

	public String getRefPaytype() {
		return this.refPaytype;
	}

	public void setRefPaytype(String refPaytype) {
		this.refPaytype = refPaytype;
	}

//	public static void main(String[] args) {
//		String str = "Lemon Tree.";
//		char[] chai = str.toCharArray();
//		String a = "";
//		for (int i = chai.length-1; i >= 0; i--) {
//			a += chai[i];
//		}
//		System.out.println(a);
//	}
}
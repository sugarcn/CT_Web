package org.ctonline.po.views;

import java.math.BigDecimal;

/**
 * ViewOrderCheckId entity. @author MyEclipse Persistence Tools
 */

public class ViewOrderCheck implements java.io.Serializable {

	// Fields

	private Long cartId;
	private String UId;
	private String GId;
	private String PUrl;
	private String carttype;
	private String parprice;
	private Long parnumber;
	private String GName;
	private BigDecimal promotePrice;
	private BigDecimal shopPrice;
	private String GUnit;
	private String pack;
	private String pack1;
	private BigDecimal pack1Num;
	private String pack2;
	private BigDecimal pack2Num;
	private String pack3;
	private BigDecimal pack3Num;
	private String pack4;
	private BigDecimal pack4Num;
	private String GNumber;
	private String GPrice;
	private String sample;
	private String partial;

	// Constructors

	public String getSample() {
		return sample;
	}

	public void setSample(String sample) {
		this.sample = sample;
	}

	public String getPartial() {
		return partial;
	}

	public void setPartial(String partial) {
		this.partial = partial;
	}

	/** default constructor */
	public ViewOrderCheck() {
	}

	public String getParprice() {
		return parprice;
	}

	public void setParprice(String parprice) {
		this.parprice = parprice;
	}

	public Long getParnumber() {
		return parnumber;
	}

	public void setParnumber(Long parnumber) {
		this.parnumber = parnumber;
	}

	public String getCarttype() {
		return carttype;
	}

	public void setCarttype(String carttype) {
		this.carttype = carttype;
	}

	/** minimal constructor */
	public ViewOrderCheck(Long cartId, String UId, String GId,
			String GNumber, String GPrice) {
		this.cartId = cartId;
		this.UId = UId;
		this.GId = GId;
		this.GNumber = GNumber;
		this.GPrice = GPrice;
	}

	/** full constructor */
	public ViewOrderCheck(Long cartId, String UId, String GId, String PUrl,
			String GName, BigDecimal promotePrice, BigDecimal shopPrice,
			String GUnit, String pack, String pack1, BigDecimal pack1Num,
			String pack2, BigDecimal pack2Num, String pack3,
			BigDecimal pack3Num, String pack4, BigDecimal pack4Num,
			String GNumber, String GPrice) {
		this.cartId = cartId;
		this.UId = UId;
		this.GId = GId;
		this.PUrl = PUrl;
		this.GName = GName;
		this.promotePrice = promotePrice;
		this.shopPrice = shopPrice;
		this.GUnit = GUnit;
		this.pack = pack;
		this.pack1 = pack1;
		this.pack1Num = pack1Num;
		this.pack2 = pack2;
		this.pack2Num = pack2Num;
		this.pack3 = pack3;
		this.pack3Num = pack3Num;
		this.pack4 = pack4;
		this.pack4Num = pack4Num;
		this.GNumber = GNumber;
		this.GPrice = GPrice;
	}

	// Property accessors

	public Long getCartId() {
		return this.cartId;
	}

	public void setCartId(Long cartId) {
		this.cartId = cartId;
	}

	public String getUId() {
		return this.UId;
	}

	public void setUId(String UId) {
		this.UId = UId;
	}

	public String getGId() {
		return this.GId;
	}

	public void setGId(String GId) {
		this.GId = GId;
	}

	public String getPUrl() {
		return this.PUrl;
	}

	public void setPUrl(String PUrl) {
		this.PUrl = PUrl;
	}

	public String getGName() {
		return this.GName;
	}

	public void setGName(String GName) {
		this.GName = GName;
	}

	public BigDecimal getPromotePrice() {
		return this.promotePrice;
	}

	public void setPromotePrice(BigDecimal promotePrice) {
		this.promotePrice = promotePrice;
	}

	public BigDecimal getShopPrice() {
		return this.shopPrice;
	}

	public void setShopPrice(BigDecimal shopPrice) {
		this.shopPrice = shopPrice;
	}

	public String getGUnit() {
		return this.GUnit;
	}

	public void setGUnit(String GUnit) {
		this.GUnit = GUnit;
	}

	public String getPack() {
		return this.pack;
	}

	public void setPack(String pack) {
		this.pack = pack;
	}

	public String getPack1() {
		return this.pack1;
	}

	public void setPack1(String pack1) {
		this.pack1 = pack1;
	}

	public BigDecimal getPack1Num() {
		return this.pack1Num;
	}

	public void setPack1Num(BigDecimal pack1Num) {
		this.pack1Num = pack1Num;
	}

	public String getPack2() {
		return this.pack2;
	}

	public void setPack2(String pack2) {
		this.pack2 = pack2;
	}

	public BigDecimal getPack2Num() {
		return this.pack2Num;
	}

	public void setPack2Num(BigDecimal pack2Num) {
		this.pack2Num = pack2Num;
	}

	public String getPack3() {
		return this.pack3;
	}

	public void setPack3(String pack3) {
		this.pack3 = pack3;
	}

	public BigDecimal getPack3Num() {
		return this.pack3Num;
	}

	public void setPack3Num(BigDecimal pack3Num) {
		this.pack3Num = pack3Num;
	}

	public String getPack4() {
		return this.pack4;
	}

	public void setPack4(String pack4) {
		this.pack4 = pack4;
	}

	public BigDecimal getPack4Num() {
		return this.pack4Num;
	}

	public void setPack4Num(BigDecimal pack4Num) {
		this.pack4Num = pack4Num;
	}

	public String getGNumber() {
		return this.GNumber;
	}

	public void setGNumber(String GNumber) {
		this.GNumber = GNumber;
	}

	public String getGPrice() {
		return this.GPrice;
	}

	public void setGPrice(String GPrice) {
		this.GPrice = GPrice;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ViewOrderCheck))
			return false;
		ViewOrderCheck castOther = (ViewOrderCheck) other;

		return ((this.getCartId() == castOther.getCartId()) || (this
				.getCartId() != null && castOther.getCartId() != null && this
				.getCartId().equals(castOther.getCartId())))
				&& ((this.getUId() == castOther.getUId()) || (this.getUId() != null
						&& castOther.getUId() != null && this.getUId().equals(
						castOther.getUId())))
				&& ((this.getGId() == castOther.getGId()) || (this.getGId() != null
						&& castOther.getGId() != null && this.getGId().equals(
						castOther.getGId())))
				&& ((this.getPUrl() == castOther.getPUrl()) || (this.getPUrl() != null
						&& castOther.getPUrl() != null && this.getPUrl()
						.equals(castOther.getPUrl())))
				&& ((this.getGName() == castOther.getGName()) || (this
						.getGName() != null && castOther.getGName() != null && this
						.getGName().equals(castOther.getGName())))
				&& ((this.getPromotePrice() == castOther.getPromotePrice()) || (this
						.getPromotePrice() != null
						&& castOther.getPromotePrice() != null && this
						.getPromotePrice().equals(castOther.getPromotePrice())))
				&& ((this.getShopPrice() == castOther.getShopPrice()) || (this
						.getShopPrice() != null
						&& castOther.getShopPrice() != null && this
						.getShopPrice().equals(castOther.getShopPrice())))
				&& ((this.getGUnit() == castOther.getGUnit()) || (this
						.getGUnit() != null && castOther.getGUnit() != null && this
						.getGUnit().equals(castOther.getGUnit())))
				&& ((this.getPack() == castOther.getPack()) || (this.getPack() != null
						&& castOther.getPack() != null && this.getPack()
						.equals(castOther.getPack())))
				&& ((this.getPack1() == castOther.getPack1()) || (this
						.getPack1() != null && castOther.getPack1() != null && this
						.getPack1().equals(castOther.getPack1())))
				&& ((this.getPack1Num() == castOther.getPack1Num()) || (this
						.getPack1Num() != null
						&& castOther.getPack1Num() != null && this
						.getPack1Num().equals(castOther.getPack1Num())))
				&& ((this.getPack2() == castOther.getPack2()) || (this
						.getPack2() != null && castOther.getPack2() != null && this
						.getPack2().equals(castOther.getPack2())))
				&& ((this.getPack2Num() == castOther.getPack2Num()) || (this
						.getPack2Num() != null
						&& castOther.getPack2Num() != null && this
						.getPack2Num().equals(castOther.getPack2Num())))
				&& ((this.getPack3() == castOther.getPack3()) || (this
						.getPack3() != null && castOther.getPack3() != null && this
						.getPack3().equals(castOther.getPack3())))
				&& ((this.getPack3Num() == castOther.getPack3Num()) || (this
						.getPack3Num() != null
						&& castOther.getPack3Num() != null && this
						.getPack3Num().equals(castOther.getPack3Num())))
				&& ((this.getPack4() == castOther.getPack4()) || (this
						.getPack4() != null && castOther.getPack4() != null && this
						.getPack4().equals(castOther.getPack4())))
				&& ((this.getPack4Num() == castOther.getPack4Num()) || (this
						.getPack4Num() != null
						&& castOther.getPack4Num() != null && this
						.getPack4Num().equals(castOther.getPack4Num())))
				&& ((this.getGNumber() == castOther.getGNumber()) || (this
						.getGNumber() != null && castOther.getGNumber() != null && this
						.getGNumber().equals(castOther.getGNumber())))
				&& ((this.getGPrice() == castOther.getGPrice()) || (this
						.getGPrice() != null && castOther.getGPrice() != null && this
						.getGPrice().equals(castOther.getGPrice())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getCartId() == null ? 0 : this.getCartId().hashCode());
		result = 37 * result
				+ (getUId() == null ? 0 : this.getUId().hashCode());
		result = 37 * result
				+ (getGId() == null ? 0 : this.getGId().hashCode());
		result = 37 * result
				+ (getPUrl() == null ? 0 : this.getPUrl().hashCode());
		result = 37 * result
				+ (getGName() == null ? 0 : this.getGName().hashCode());
		result = 37
				* result
				+ (getPromotePrice() == null ? 0 : this.getPromotePrice()
						.hashCode());
		result = 37 * result
				+ (getShopPrice() == null ? 0 : this.getShopPrice().hashCode());
		result = 37 * result
				+ (getGUnit() == null ? 0 : this.getGUnit().hashCode());
		result = 37 * result
				+ (getPack() == null ? 0 : this.getPack().hashCode());
		result = 37 * result
				+ (getPack1() == null ? 0 : this.getPack1().hashCode());
		result = 37 * result
				+ (getPack1Num() == null ? 0 : this.getPack1Num().hashCode());
		result = 37 * result
				+ (getPack2() == null ? 0 : this.getPack2().hashCode());
		result = 37 * result
				+ (getPack2Num() == null ? 0 : this.getPack2Num().hashCode());
		result = 37 * result
				+ (getPack3() == null ? 0 : this.getPack3().hashCode());
		result = 37 * result
				+ (getPack3Num() == null ? 0 : this.getPack3Num().hashCode());
		result = 37 * result
				+ (getPack4() == null ? 0 : this.getPack4().hashCode());
		result = 37 * result
				+ (getPack4Num() == null ? 0 : this.getPack4Num().hashCode());
		result = 37 * result
				+ (getGNumber() == null ? 0 : this.getGNumber().hashCode());
		result = 37 * result
				+ (getGPrice() == null ? 0 : this.getGPrice().hashCode());
		return result;
	}

}
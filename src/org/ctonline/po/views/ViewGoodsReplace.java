package org.ctonline.po.views;

/**
 * ViewGoodsReplaceId entity. @author MyEclipse Persistence Tools
 */

public class ViewGoodsReplace implements java.io.Serializable {

	// Fields

	private Long GId;
	private Long replaceGId;
	private String GName;
	private String GSn;

	// Constructors

	/** default constructor */
	public ViewGoodsReplace() {
	}

	/** minimal constructor */
	public ViewGoodsReplace(Long GId, Long replaceGId, String GName) {
		this.GId = GId;
		this.replaceGId = replaceGId;
		this.GName = GName;
	}

	/** full constructor */
	public ViewGoodsReplace(Long GId, Long replaceGId, String GName,
			String GSn) {
		this.GId = GId;
		this.replaceGId = replaceGId;
		this.GName = GName;
		this.GSn = GSn;
	}

	// Property accessors

	public Long getGId() {
		return this.GId;
	}

	public void setGId(Long GId) {
		this.GId = GId;
	}

	public Long getReplaceGId() {
		return this.replaceGId;
	}

	public void setReplaceGId(Long replaceGId) {
		this.replaceGId = replaceGId;
	}

	public String getGName() {
		return this.GName;
	}

	public void setGName(String GName) {
		this.GName = GName;
	}

	public String getGSn() {
		return this.GSn;
	}

	public void setGSn(String GSn) {
		this.GSn = GSn;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ViewGoodsReplace))
			return false;
		ViewGoodsReplace castOther = (ViewGoodsReplace) other;

		return ((this.getGId() == castOther.getGId()) || (this.getGId() != null
				&& castOther.getGId() != null && this.getGId().equals(
				castOther.getGId())))
				&& ((this.getReplaceGId() == castOther.getReplaceGId()) || (this
						.getReplaceGId() != null
						&& castOther.getReplaceGId() != null && this
						.getReplaceGId().equals(castOther.getReplaceGId())))
				&& ((this.getGName() == castOther.getGName()) || (this
						.getGName() != null && castOther.getGName() != null && this
						.getGName().equals(castOther.getGName())))
				&& ((this.getGSn() == castOther.getGSn()) || (this.getGSn() != null
						&& castOther.getGSn() != null && this.getGSn().equals(
						castOther.getGSn())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getGId() == null ? 0 : this.getGId().hashCode());
		result = 37
				* result
				+ (getReplaceGId() == null ? 0 : this.getReplaceGId()
						.hashCode());
		result = 37 * result
				+ (getGName() == null ? 0 : this.getGName().hashCode());
		result = 37 * result
				+ (getGSn() == null ? 0 : this.getGSn().hashCode());
		return result;
	}

}
package org.ctonline.po.views;

/**
 * ViewGoodsLinkId entity. @author MyEclipse Persistence Tools
 */

public class ViewGoodsLink implements java.io.Serializable {

	// Fields

	private Long GId;
	private Long linkGId;
	private String GName;
	private String GSn;

	// Constructors

	/** default constructor */
	public ViewGoodsLink() {
	}

	/** minimal constructor */
	public ViewGoodsLink(Long GId, Long linkGId, String GName) {
		this.GId = GId;
		this.linkGId = linkGId;
		this.GName = GName;
	}

	/** full constructor */
	public ViewGoodsLink(Long GId, Long linkGId, String GName, String GSn) {
		this.GId = GId;
		this.linkGId = linkGId;
		this.GName = GName;
		this.GSn = GSn;
	}

	// Property accessors

	public Long getGId() {
		return this.GId;
	}

	public void setGId(Long GId) {
		this.GId = GId;
	}

	public Long getLinkGId() {
		return this.linkGId;
	}

	public void setLinkGId(Long linkGId) {
		this.linkGId = linkGId;
	}

	public String getGName() {
		return this.GName;
	}

	public void setGName(String GName) {
		this.GName = GName;
	}

	public String getGSn() {
		return this.GSn;
	}

	public void setGSn(String GSn) {
		this.GSn = GSn;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ViewGoodsLink))
			return false;
		ViewGoodsLink castOther = (ViewGoodsLink) other;

		return ((this.getGId() == castOther.getGId()) || (this.getGId() != null
				&& castOther.getGId() != null && this.getGId().equals(
				castOther.getGId())))
				&& ((this.getLinkGId() == castOther.getLinkGId()) || (this
						.getLinkGId() != null && castOther.getLinkGId() != null && this
						.getLinkGId().equals(castOther.getLinkGId())))
				&& ((this.getGName() == castOther.getGName()) || (this
						.getGName() != null && castOther.getGName() != null && this
						.getGName().equals(castOther.getGName())))
				&& ((this.getGSn() == castOther.getGSn()) || (this.getGSn() != null
						&& castOther.getGSn() != null && this.getGSn().equals(
						castOther.getGSn())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getGId() == null ? 0 : this.getGId().hashCode());
		result = 37 * result
				+ (getLinkGId() == null ? 0 : this.getLinkGId().hashCode());
		result = 37 * result
				+ (getGName() == null ? 0 : this.getGName().hashCode());
		result = 37 * result
				+ (getGSn() == null ? 0 : this.getGSn().hashCode());
		return result;
	}

}
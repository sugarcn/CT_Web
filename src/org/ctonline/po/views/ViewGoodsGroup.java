package org.ctonline.po.views;


/**
 * ViewGoodsGroupId entity. @author MyEclipse Persistence Tools
 */

public class ViewGoodsGroup implements java.io.Serializable {

	// Fields

	private Long id;
	private String goodsname;
	private String goodstype;

	// Constructors

	/** default constructor */
	public ViewGoodsGroup() {
	}

	/** full constructor */
	public ViewGoodsGroup(Long id, String goodsname, String goodstype) {
		this.id = id;
		this.goodsname = goodsname;
		this.goodstype = goodstype;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGoodsname() {
		return this.goodsname;
	}

	public void setGoodsname(String goodsname) {
		this.goodsname = goodsname;
	}

	public String getGoodstype() {
		return this.goodstype;
	}

	public void setGoodstype(String goodstype) {
		this.goodstype = goodstype;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ViewGoodsGroup))
			return false;
		ViewGoodsGroup castOther = (ViewGoodsGroup) other;

		return ((this.getId() == castOther.getId()) || (this.getId() != null
				&& castOther.getId() != null && this.getId().equals(
				castOther.getId())))
				&& ((this.getGoodsname() == castOther.getGoodsname()) || (this
						.getGoodsname() != null
						&& castOther.getGoodsname() != null && this
						.getGoodsname().equals(castOther.getGoodsname())))
				&& ((this.getGoodstype() == castOther.getGoodstype()) || (this
						.getGoodstype() != null
						&& castOther.getGoodstype() != null && this
						.getGoodstype().equals(castOther.getGoodstype())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (getId() == null ? 0 : this.getId().hashCode());
		result = 37 * result
				+ (getGoodsname() == null ? 0 : this.getGoodsname().hashCode());
		result = 37 * result
				+ (getGoodstype() == null ? 0 : this.getGoodstype().hashCode());
		return result;
	}

}
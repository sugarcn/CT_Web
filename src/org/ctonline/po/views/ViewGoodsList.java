package org.ctonline.po.views;

import java.math.BigDecimal;

/**
 * ViewGoodsListId entity. @author MyEclipse Persistence Tools
 */

public class ViewGoodsList implements java.io.Serializable {

	// Fields

	private Long GId;
	private String GName;
	private String uname;
	private Long gnum;
	private String series;
	private String alw;
	private String GSn;
	private BigDecimal marketPrice;
	private BigDecimal promotePrice;
	private BigDecimal shopPrice;
	private String BName;
	private String GUnit;
	private String pack1;
	private BigDecimal pack1Num;
	private String pack2;
	private BigDecimal pack2Num;
	private String pack3;
	private BigDecimal pack3Num;
	private String pack4;
	private BigDecimal pack4Num;
	private String PUrl;

	// Constructors

	/** default constructor */
	public ViewGoodsList() {
	}

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	public String getAlw() {
		return alw;
	}

	public void setAlw(String alw) {
		this.alw = alw;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public Long getGnum() {
		return gnum;
	}

	public void setGnum(Long gnum) {
		this.gnum = gnum;
	}

	/** minimal constructor */
	public ViewGoodsList(Long GId) {
		this.GId = GId;
	}

	/** full constructor */
	public ViewGoodsList(Long GId, String GName, String GSn,
			BigDecimal marketPrice, BigDecimal promotePrice,
			BigDecimal shopPrice, String BName, String GUnit, String pack1,
			BigDecimal pack1Num, String pack2, BigDecimal pack2Num,
			String pack3, BigDecimal pack3Num, String pack4,
			BigDecimal pack4Num, String PUrl) {
		this.GId = GId;
		this.GName = GName;
		this.GSn = GSn;
		this.marketPrice = marketPrice;
		this.promotePrice = promotePrice;
		this.shopPrice = shopPrice;
		this.BName = BName;
		this.GUnit = GUnit;
		this.pack1 = pack1;
		this.pack1Num = pack1Num;
		this.pack2 = pack2;
		this.pack2Num = pack2Num;
		this.pack3 = pack3;
		this.pack3Num = pack3Num;
		this.pack4 = pack4;
		this.pack4Num = pack4Num;
		this.PUrl = PUrl;
	}

	// Property accessors

	public Long getGId() {
		return this.GId;
	}

	public void setGId(Long GId) {
		this.GId = GId;
	}

	public String getGName() {
		return this.GName;
	}

	public void setGName(String GName) {
		this.GName = GName;
	}

	public String getGSn() {
		return this.GSn;
	}

	public void setGSn(String GSn) {
		this.GSn = GSn;
	}

	public BigDecimal getMarketPrice() {
		return this.marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	public BigDecimal getPromotePrice() {
		return this.promotePrice;
	}

	public void setPromotePrice(BigDecimal promotePrice) {
		this.promotePrice = promotePrice;
	}

	public BigDecimal getShopPrice() {
		return this.shopPrice;
	}

	public void setShopPrice(BigDecimal shopPrice) {
		this.shopPrice = shopPrice;
	}

	public String getBName() {
		return this.BName;
	}

	public void setBName(String BName) {
		this.BName = BName;
	}

	public String getGUnit() {
		return this.GUnit;
	}

	public void setGUnit(String GUnit) {
		this.GUnit = GUnit;
	}

	public String getPack1() {
		return this.pack1;
	}

	public void setPack1(String pack1) {
		this.pack1 = pack1;
	}

	public BigDecimal getPack1Num() {
		return this.pack1Num;
	}

	public void setPack1Num(BigDecimal pack1Num) {
		this.pack1Num = pack1Num;
	}

	public String getPack2() {
		return this.pack2;
	}

	public void setPack2(String pack2) {
		this.pack2 = pack2;
	}

	public BigDecimal getPack2Num() {
		return this.pack2Num;
	}

	public void setPack2Num(BigDecimal pack2Num) {
		this.pack2Num = pack2Num;
	}

	public String getPack3() {
		return this.pack3;
	}

	public void setPack3(String pack3) {
		this.pack3 = pack3;
	}

	public BigDecimal getPack3Num() {
		return this.pack3Num;
	}

	public void setPack3Num(BigDecimal pack3Num) {
		this.pack3Num = pack3Num;
	}

	public String getPack4() {
		return this.pack4;
	}

	public void setPack4(String pack4) {
		this.pack4 = pack4;
	}

	public BigDecimal getPack4Num() {
		return this.pack4Num;
	}

	public void setPack4Num(BigDecimal pack4Num) {
		this.pack4Num = pack4Num;
	}

	public String getPUrl() {
		return this.PUrl;
	}

	public void setPUrl(String PUrl) {
		this.PUrl = PUrl;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ViewGoodsList))
			return false;
		ViewGoodsList castOther = (ViewGoodsList) other;

		return ((this.getGId() == castOther.getGId()) || (this.getGId() != null
				&& castOther.getGId() != null && this.getGId().equals(
				castOther.getGId())))
				&& ((this.getGName() == castOther.getGName()) || (this
						.getGName() != null && castOther.getGName() != null && this
						.getGName().equals(castOther.getGName())))
				&& ((this.getGSn() == castOther.getGSn()) || (this.getGSn() != null
						&& castOther.getGSn() != null && this.getGSn().equals(
						castOther.getGSn())))
				&& ((this.getMarketPrice() == castOther.getMarketPrice()) || (this
						.getMarketPrice() != null
						&& castOther.getMarketPrice() != null && this
						.getMarketPrice().equals(castOther.getMarketPrice())))
				&& ((this.getPromotePrice() == castOther.getPromotePrice()) || (this
						.getPromotePrice() != null
						&& castOther.getPromotePrice() != null && this
						.getPromotePrice().equals(castOther.getPromotePrice())))
				&& ((this.getShopPrice() == castOther.getShopPrice()) || (this
						.getShopPrice() != null
						&& castOther.getShopPrice() != null && this
						.getShopPrice().equals(castOther.getShopPrice())))
				&& ((this.getBName() == castOther.getBName()) || (this
						.getBName() != null && castOther.getBName() != null && this
						.getBName().equals(castOther.getBName())))
				&& ((this.getGUnit() == castOther.getGUnit()) || (this
						.getGUnit() != null && castOther.getGUnit() != null && this
						.getGUnit().equals(castOther.getGUnit())))
				&& ((this.getPack1() == castOther.getPack1()) || (this
						.getPack1() != null && castOther.getPack1() != null && this
						.getPack1().equals(castOther.getPack1())))
				&& ((this.getPack1Num() == castOther.getPack1Num()) || (this
						.getPack1Num() != null
						&& castOther.getPack1Num() != null && this
						.getPack1Num().equals(castOther.getPack1Num())))
				&& ((this.getPack2() == castOther.getPack2()) || (this
						.getPack2() != null && castOther.getPack2() != null && this
						.getPack2().equals(castOther.getPack2())))
				&& ((this.getPack2Num() == castOther.getPack2Num()) || (this
						.getPack2Num() != null
						&& castOther.getPack2Num() != null && this
						.getPack2Num().equals(castOther.getPack2Num())))
				&& ((this.getPack3() == castOther.getPack3()) || (this
						.getPack3() != null && castOther.getPack3() != null && this
						.getPack3().equals(castOther.getPack3())))
				&& ((this.getPack3Num() == castOther.getPack3Num()) || (this
						.getPack3Num() != null
						&& castOther.getPack3Num() != null && this
						.getPack3Num().equals(castOther.getPack3Num())))
				&& ((this.getPack4() == castOther.getPack4()) || (this
						.getPack4() != null && castOther.getPack4() != null && this
						.getPack4().equals(castOther.getPack4())))
				&& ((this.getPack4Num() == castOther.getPack4Num()) || (this
						.getPack4Num() != null
						&& castOther.getPack4Num() != null && this
						.getPack4Num().equals(castOther.getPack4Num())))
				&& ((this.getPUrl() == castOther.getPUrl()) || (this.getPUrl() != null
						&& castOther.getPUrl() != null && this.getPUrl()
						.equals(castOther.getPUrl())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getGId() == null ? 0 : this.getGId().hashCode());
		result = 37 * result
				+ (getGName() == null ? 0 : this.getGName().hashCode());
		result = 37 * result
				+ (getGSn() == null ? 0 : this.getGSn().hashCode());
		result = 37
				* result
				+ (getMarketPrice() == null ? 0 : this.getMarketPrice()
						.hashCode());
		result = 37
				* result
				+ (getPromotePrice() == null ? 0 : this.getPromotePrice()
						.hashCode());
		result = 37 * result
				+ (getShopPrice() == null ? 0 : this.getShopPrice().hashCode());
		result = 37 * result
				+ (getBName() == null ? 0 : this.getBName().hashCode());
		result = 37 * result
				+ (getGUnit() == null ? 0 : this.getGUnit().hashCode());
		result = 37 * result
				+ (getPack1() == null ? 0 : this.getPack1().hashCode());
		result = 37 * result
				+ (getPack1Num() == null ? 0 : this.getPack1Num().hashCode());
		result = 37 * result
				+ (getPack2() == null ? 0 : this.getPack2().hashCode());
		result = 37 * result
				+ (getPack2Num() == null ? 0 : this.getPack2Num().hashCode());
		result = 37 * result
				+ (getPack3() == null ? 0 : this.getPack3().hashCode());
		result = 37 * result
				+ (getPack3Num() == null ? 0 : this.getPack3Num().hashCode());
		result = 37 * result
				+ (getPack4() == null ? 0 : this.getPack4().hashCode());
		result = 37 * result
				+ (getPack4Num() == null ? 0 : this.getPack4Num().hashCode());
		result = 37 * result
				+ (getPUrl() == null ? 0 : this.getPUrl().hashCode());
		return result;
	}

}
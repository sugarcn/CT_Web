package org.ctonline.po.views;

/**
 * ViewGoodsPriceId entity. @author MyEclipse Persistence Tools
 */

public class ViewGoodsPrice implements java.io.Serializable {

	// Fields

	private Long pid;
	private String goodsname;
	private String goodstype;
	private String username;
	private String useraccount;
	private String usertype;
	private String goodsprice;

	// Constructors

	/** default constructor */
	public ViewGoodsPrice() {
	}

	/** full constructor */
	public ViewGoodsPrice(Long pid, String goodsname, String goodstype,
			String username, String useraccount, String usertype,
			String goodsprice) {
		this.pid = pid;
		this.goodsname = goodsname;
		this.goodstype = goodstype;
		this.username = username;
		this.useraccount = useraccount;
		this.usertype = usertype;
		this.goodsprice = goodsprice;
	}

	// Property accessors

	public Long getPid() {
		return this.pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public String getGoodsname() {
		return this.goodsname;
	}

	public void setGoodsname(String goodsname) {
		this.goodsname = goodsname;
	}

	public String getGoodstype() {
		return this.goodstype;
	}

	public void setGoodstype(String goodstype) {
		this.goodstype = goodstype;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUseraccount() {
		return this.useraccount;
	}

	public void setUseraccount(String useraccount) {
		this.useraccount = useraccount;
	}

	public String getUsertype() {
		return this.usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public String getGoodsprice() {
		return this.goodsprice;
	}

	public void setGoodsprice(String goodsprice) {
		this.goodsprice = goodsprice;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ViewGoodsPrice))
			return false;
		ViewGoodsPrice castOther = (ViewGoodsPrice) other;

		return ((this.getPid() == castOther.getPid()) || (this.getPid() != null
				&& castOther.getPid() != null && this.getPid().equals(
				castOther.getPid())))
				&& ((this.getGoodsname() == castOther.getGoodsname()) || (this
						.getGoodsname() != null
						&& castOther.getGoodsname() != null && this
						.getGoodsname().equals(castOther.getGoodsname())))
				&& ((this.getGoodstype() == castOther.getGoodstype()) || (this
						.getGoodstype() != null
						&& castOther.getGoodstype() != null && this
						.getGoodstype().equals(castOther.getGoodstype())))
				&& ((this.getUsername() == castOther.getUsername()) || (this
						.getUsername() != null
						&& castOther.getUsername() != null && this
						.getUsername().equals(castOther.getUsername())))
				&& ((this.getUseraccount() == castOther.getUseraccount()) || (this
						.getUseraccount() != null
						&& castOther.getUseraccount() != null && this
						.getUseraccount().equals(castOther.getUseraccount())))
				&& ((this.getUsertype() == castOther.getUsertype()) || (this
						.getUsertype() != null
						&& castOther.getUsertype() != null && this
						.getUsertype().equals(castOther.getUsertype())))
				&& ((this.getGoodsprice() == castOther.getGoodsprice()) || (this
						.getGoodsprice() != null
						&& castOther.getGoodsprice() != null && this
						.getGoodsprice().equals(castOther.getGoodsprice())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getPid() == null ? 0 : this.getPid().hashCode());
		result = 37 * result
				+ (getGoodsname() == null ? 0 : this.getGoodsname().hashCode());
		result = 37 * result
				+ (getGoodstype() == null ? 0 : this.getGoodstype().hashCode());
		result = 37 * result
				+ (getUsername() == null ? 0 : this.getUsername().hashCode());
		result = 37
				* result
				+ (getUseraccount() == null ? 0 : this.getUseraccount()
						.hashCode());
		result = 37 * result
				+ (getUsertype() == null ? 0 : this.getUsertype().hashCode());
		result = 37
				* result
				+ (getGoodsprice() == null ? 0 : this.getGoodsprice()
						.hashCode());
		return result;
	}

}
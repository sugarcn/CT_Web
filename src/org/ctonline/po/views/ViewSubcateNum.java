package org.ctonline.po.views;

import java.math.BigDecimal;

/**
 * ViewSubcateNumId entity. @author MyEclipse Persistence Tools
 */

public class ViewSubcateNum implements java.io.Serializable {

	// Fields

	private Long cid;
	private BigDecimal ccount;

	// Constructors

	/** default constructor */
	public ViewSubcateNum() {
	}

	/** minimal constructor */
	public ViewSubcateNum(Long cid) {
		this.cid = cid;
	}

	/** full constructor */
	public ViewSubcateNum(Long cid, BigDecimal ccount) {
		this.cid = cid;
		this.ccount = ccount;
	}

	// Property accessors

	public Long getCid() {
		return this.cid;
	}

	public void setCid(Long cid) {
		this.cid = cid;
	}

	public BigDecimal getCcount() {
		return this.ccount;
	}

	public void setCcount(BigDecimal ccount) {
		this.ccount = ccount;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ViewSubcateNum))
			return false;
		ViewSubcateNum castOther = (ViewSubcateNum) other;

		return ((this.getCid() == castOther.getCid()) || (this.getCid() != null
				&& castOther.getCid() != null && this.getCid().equals(
				castOther.getCid())))
				&& ((this.getCcount() == castOther.getCcount()) || (this
						.getCcount() != null && castOther.getCcount() != null && this
						.getCcount().equals(castOther.getCcount())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getCid() == null ? 0 : this.getCid().hashCode());
		result = 37 * result
				+ (getCcount() == null ? 0 : this.getCcount().hashCode());
		return result;
	}

}
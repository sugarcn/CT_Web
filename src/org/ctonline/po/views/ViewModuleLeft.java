package org.ctonline.po.views;

import java.util.List;
import java.util.Set;

/**
 * ViewModuleLeftId entity. @author MyEclipse Persistence Tools
 */

public class ViewModuleLeft implements java.io.Serializable {

	// Fields

	private Integer moduleId;
	private String moduleName;
	private String moduleUrl;
	private Integer parentId;
	private String imgUrl;
	private Integer roleId;
	private String right;
	private List<ViewModuleLeft> childs;

	// Constructors

	/** default constructor */
	public ViewModuleLeft() {
	}

	/** minimal constructor */
	public ViewModuleLeft(Integer moduleId, String moduleName,
			String moduleUrl, Integer parentId) {
		this.moduleId = moduleId;
		this.moduleName = moduleName;
		this.moduleUrl = moduleUrl;
		this.parentId = parentId;
	}

	/** full constructor */
	public ViewModuleLeft(Integer moduleId, String moduleName,
			String moduleUrl, Integer parentId, String imgUrl, String right) {
		this.moduleId = moduleId;
		this.moduleName = moduleName;
		this.moduleUrl = moduleUrl;
		this.parentId = parentId;
		this.imgUrl = imgUrl;
		this.right = right;
	}

	// Property accessors

	public Integer getModuleId() {
		return this.moduleId;
	}

	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	public String getModuleName() {
		return this.moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getModuleUrl() {
		return this.moduleUrl;
	}

	public void setModuleUrl(String moduleUrl) {
		this.moduleUrl = moduleUrl;
	}

	public Integer getParentId() {
		return this.parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getImgUrl() {
		return this.imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getRight() {
		return this.right;
	}

	public void setRight(String right) {
		this.right = right;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ViewModuleLeft))
			return false;
		ViewModuleLeft castOther = (ViewModuleLeft) other;

		return ((this.getModuleId() == castOther.getModuleId()) || (this
				.getModuleId() != null && castOther.getModuleId() != null && this
				.getModuleId().equals(castOther.getModuleId())))
				&& ((this.getModuleName() == castOther.getModuleName()) || (this
						.getModuleName() != null
						&& castOther.getModuleName() != null && this
						.getModuleName().equals(castOther.getModuleName())))
				&& ((this.getModuleUrl() == castOther.getModuleUrl()) || (this
						.getModuleUrl() != null
						&& castOther.getModuleUrl() != null && this
						.getModuleUrl().equals(castOther.getModuleUrl())))
				&& ((this.getParentId() == castOther.getParentId()) || (this
						.getParentId() != null
						&& castOther.getParentId() != null && this
						.getParentId().equals(castOther.getParentId())))
				&& ((this.getImgUrl() == castOther.getImgUrl()) || (this
						.getImgUrl() != null && castOther.getImgUrl() != null && this
						.getImgUrl().equals(castOther.getImgUrl())))
				&& ((this.getRight() == castOther.getRight()) || (this
						.getRight() != null && castOther.getRight() != null && this
						.getRight().equals(castOther.getRight())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getModuleId() == null ? 0 : this.getModuleId().hashCode());
		result = 37
				* result
				+ (getModuleName() == null ? 0 : this.getModuleName()
						.hashCode());
		result = 37 * result
				+ (getModuleUrl() == null ? 0 : this.getModuleUrl().hashCode());
		result = 37 * result
				+ (getParentId() == null ? 0 : this.getParentId().hashCode());
		result = 37 * result
				+ (getImgUrl() == null ? 0 : this.getImgUrl().hashCode());
		result = 37 * result
				+ (getRight() == null ? 0 : this.getRight().hashCode());
		return result;
	}

	public List<ViewModuleLeft> getChilds() {
		return childs;
	}

	public void setChilds(List<ViewModuleLeft> childs) {
		this.childs = childs;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

}
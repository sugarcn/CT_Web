package org.ctonline.po.views;

/**
 * ViewGoodsAttributeId entity. @author MyEclipse Persistence Tools
 */

public class ViewGoodsAttribute implements java.io.Serializable {

	// Fields

	private Long GId;
	private Long attrId;
	private String attrValue;
	private String attrName;

	// Constructors

	/** default constructor */
	public ViewGoodsAttribute() {
	}

	/** minimal constructor */
	public ViewGoodsAttribute(Long GId, Long attrId, String attrValue) {
		this.GId = GId;
		this.attrId = attrId;
		this.attrValue = attrValue;
	}

	/** full constructor */
	public ViewGoodsAttribute(Long GId, Long attrId, String attrValue,
			String attrName) {
		this.GId = GId;
		this.attrId = attrId;
		this.attrValue = attrValue;
		this.attrName = attrName;
	}

	// Property accessors

	public Long getGId() {
		return this.GId;
	}

	public void setGId(Long GId) {
		this.GId = GId;
	}

	public Long getAttrId() {
		return this.attrId;
	}

	public void setAttrId(Long attrId) {
		this.attrId = attrId;
	}

	public String getAttrValue() {
		return this.attrValue;
	}

	public void setAttrValue(String attrValue) {
		this.attrValue = attrValue;
	}

	public String getAttrName() {
		return this.attrName;
	}

	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ViewGoodsAttribute))
			return false;
		ViewGoodsAttribute castOther = (ViewGoodsAttribute) other;

		return ((this.getGId() == castOther.getGId()) || (this.getGId() != null
				&& castOther.getGId() != null && this.getGId().equals(
				castOther.getGId())))
				&& ((this.getAttrId() == castOther.getAttrId()) || (this
						.getAttrId() != null && castOther.getAttrId() != null && this
						.getAttrId().equals(castOther.getAttrId())))
				&& ((this.getAttrValue() == castOther.getAttrValue()) || (this
						.getAttrValue() != null
						&& castOther.getAttrValue() != null && this
						.getAttrValue().equals(castOther.getAttrValue())))
				&& ((this.getAttrName() == castOther.getAttrName()) || (this
						.getAttrName() != null
						&& castOther.getAttrName() != null && this
						.getAttrName().equals(castOther.getAttrName())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getGId() == null ? 0 : this.getGId().hashCode());
		result = 37 * result
				+ (getAttrId() == null ? 0 : this.getAttrId().hashCode());
		result = 37 * result
				+ (getAttrValue() == null ? 0 : this.getAttrValue().hashCode());
		result = 37 * result
				+ (getAttrName() == null ? 0 : this.getAttrName().hashCode());
		return result;
	}

}
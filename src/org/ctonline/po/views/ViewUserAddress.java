package org.ctonline.po.views;

/**
 * ViewUserAddressId entity. @author MyEclipse Persistence Tools
 */

public class ViewUserAddress implements java.io.Serializable {

	// Fields

	private Long AId;
	private Long UId;
	private String AConsignee;
	private String country;
	private String province;
	private String city;
	private String district;
	private String address;
	private String zipcode;
	private String tel;
	private String mb;
	private String isdefault;
	private String adesc;
	
	private String customer;
	
	// Constructors

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	/** default constructor */
	public ViewUserAddress() {
	}

	/** minimal constructor */
	public ViewUserAddress(Long AId, Long UId) {
		this.AId = AId;
		this.UId = UId;
	}

	/** full constructor */
	public ViewUserAddress(Long AId, Long UId, String AConsignee,
			String country, String province, String city, String district,
			String address, String zipcode, String tel, String mb,
			String isdefault,String adesc) {
		this.AId = AId;
		this.UId = UId;
		this.AConsignee = AConsignee;
		this.country = country;
		this.province = province;
		this.city = city;
		this.district = district;
		this.address = address;
		this.zipcode = zipcode;
		this.tel = tel;
		this.mb = mb;
		this.isdefault = isdefault;
		this.adesc=adesc;
	}

	// Property accessors

	public Long getAId() {
		return this.AId;
	}

	public void setAId(Long AId) {
		this.AId = AId;
	}

	public Long getUId() {
		return this.UId;
	}

	public void setUId(Long UId) {
		this.UId = UId;
	}

	public String getAConsignee() {
		return this.AConsignee;
	}

	public void setAConsignee(String AConsignee) {
		this.AConsignee = AConsignee;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProvince() {
		return this.province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistrict() {
		return this.district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZipcode() {
		return this.zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getTel() {
		return this.tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getMb() {
		return this.mb;
	}

	public void setMb(String mb) {
		this.mb = mb;
	}

	public String getIsdefault() {
		return this.isdefault;
	}

	public void setIsdefault(String isdefault) {
		this.isdefault = isdefault;
	}
	
	
	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ViewUserAddress))
			return false;
		ViewUserAddress castOther = (ViewUserAddress) other;

		return ((this.getAId() == castOther.getAId()) || (this.getAId() != null
				&& castOther.getAId() != null && this.getAId().equals(
				castOther.getAId())))
				&& ((this.getUId() == castOther.getUId()) || (this.getUId() != null
						&& castOther.getUId() != null && this.getUId().equals(
						castOther.getUId())))
				&& ((this.getAConsignee() == castOther.getAConsignee()) || (this
						.getAConsignee() != null
						&& castOther.getAConsignee() != null && this
						.getAConsignee().equals(castOther.getAConsignee())))
				&& ((this.getCountry() == castOther.getCountry()) || (this
						.getCountry() != null && castOther.getCountry() != null && this
						.getCountry().equals(castOther.getCountry())))
				&& ((this.getProvince() == castOther.getProvince()) || (this
						.getProvince() != null
						&& castOther.getProvince() != null && this
						.getProvince().equals(castOther.getProvince())))
				&& ((this.getCity() == castOther.getCity()) || (this.getCity() != null
						&& castOther.getCity() != null && this.getCity()
						.equals(castOther.getCity())))
				&& ((this.getDistrict() == castOther.getDistrict()) || (this
						.getDistrict() != null
						&& castOther.getDistrict() != null && this
						.getDistrict().equals(castOther.getDistrict())))
				&& ((this.getAddress() == castOther.getAddress()) || (this
						.getAddress() != null && castOther.getAddress() != null && this
						.getAddress().equals(castOther.getAddress())))
				&& ((this.getZipcode() == castOther.getZipcode()) || (this
						.getZipcode() != null && castOther.getZipcode() != null && this
						.getZipcode().equals(castOther.getZipcode())))
				&& ((this.getTel() == castOther.getTel()) || (this.getTel() != null
						&& castOther.getTel() != null && this.getTel().equals(
						castOther.getTel())))
				&& ((this.getMb() == castOther.getMb()) || (this.getMb() != null
						&& castOther.getMb() != null && this.getMb().equals(
						castOther.getMb())))
				&& ((this.getIsdefault() == castOther.getIsdefault()) || (this
						.getIsdefault() != null
						&& castOther.getIsdefault() != null && this
						.getIsdefault().equals(castOther.getIsdefault())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getAId() == null ? 0 : this.getAId().hashCode());
		result = 37 * result
				+ (getUId() == null ? 0 : this.getUId().hashCode());
		result = 37
				* result
				+ (getAConsignee() == null ? 0 : this.getAConsignee()
						.hashCode());
		result = 37 * result
				+ (getCountry() == null ? 0 : this.getCountry().hashCode());
		result = 37 * result
				+ (getProvince() == null ? 0 : this.getProvince().hashCode());
		result = 37 * result
				+ (getCity() == null ? 0 : this.getCity().hashCode());
		result = 37 * result
				+ (getDistrict() == null ? 0 : this.getDistrict().hashCode());
		result = 37 * result
				+ (getAddress() == null ? 0 : this.getAddress().hashCode());
		result = 37 * result
				+ (getZipcode() == null ? 0 : this.getZipcode().hashCode());
		result = 37 * result
				+ (getTel() == null ? 0 : this.getTel().hashCode());
		result = 37 * result + (getMb() == null ? 0 : this.getMb().hashCode());
		result = 37 * result
				+ (getIsdefault() == null ? 0 : this.getIsdefault().hashCode());
		return result;
	}

	public String getAdesc() {
		return adesc;
	}

	public void setAdesc(String adesc) {
		this.adesc = adesc;
	}

}
package org.ctonline.po.views;


/**
 * ViewUserGroupId entity. @author MyEclipse Persistence Tools
 */

public class ViewUserGroup implements java.io.Serializable {

	// Fields

	private Long id;
	private String userid;
	private String username;
	private String usertype;

	// Constructors

	/** default constructor */
	public ViewUserGroup() {
	}

	/** full constructor */
	public ViewUserGroup(Long id, String userid, String username,
			String usertype) {
		this.id = id;
		this.userid = userid;
		this.username = username;
		this.usertype = usertype;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserid() {
		return this.userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsertype() {
		return this.usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ViewUserGroup))
			return false;
		ViewUserGroup castOther = (ViewUserGroup) other;

		return ((this.getId() == castOther.getId()) || (this.getId() != null
				&& castOther.getId() != null && this.getId().equals(
				castOther.getId())))
				&& ((this.getUserid() == castOther.getUserid()) || (this
						.getUserid() != null && castOther.getUserid() != null && this
						.getUserid().equals(castOther.getUserid())))
				&& ((this.getUsername() == castOther.getUsername()) || (this
						.getUsername() != null
						&& castOther.getUsername() != null && this
						.getUsername().equals(castOther.getUsername())))
				&& ((this.getUsertype() == castOther.getUsertype()) || (this
						.getUsertype() != null
						&& castOther.getUsertype() != null && this
						.getUsertype().equals(castOther.getUsertype())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (getId() == null ? 0 : this.getId().hashCode());
		result = 37 * result
				+ (getUserid() == null ? 0 : this.getUserid().hashCode());
		result = 37 * result
				+ (getUsername() == null ? 0 : this.getUsername().hashCode());
		result = 37 * result
				+ (getUsertype() == null ? 0 : this.getUsertype().hashCode());
		return result;
	}

}
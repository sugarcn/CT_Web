package org.ctonline.po.goods;

/**
 * CtGoodsResource entity. @author MyEclipse Persistence Tools
 */

public class CtGoodsResource implements java.io.Serializable {

	// Fields

	private Long GId;
	private Long resourceId;
	private String goodsType;

	// Constructors

	/** default constructor */
	public CtGoodsResource() {
	}

	/** full constructor */
	public CtGoodsResource(Long resourceId, String goodsType) {
		this.resourceId = resourceId;
		this.goodsType = goodsType;
	}

	// Property accessors

	public Long getGId() {
		return this.GId;
	}

	public void setGId(Long GId) {
		this.GId = GId;
	}

	public Long getResourceId() {
		return this.resourceId;
	}

	public void setResourceId(Long resourceId) {
		this.resourceId = resourceId;
	}

	public String getGoodsType() {
		return this.goodsType;
	}

	public void setGoodsType(String goodsType) {
		this.goodsType = goodsType;
	}

}
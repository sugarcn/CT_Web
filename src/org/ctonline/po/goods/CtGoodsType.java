package org.ctonline.po.goods;

/**
 * CtGoodsType entity. @author MyEclipse Persistence Tools
 */

public class CtGoodsType implements java.io.Serializable {

	// Fields

	private Long GTId;
	private String typeName;
	private String enabled;

	// Constructors

	/** default constructor */
	public CtGoodsType() {
	}

	/** minimal constructor */
	public CtGoodsType(String typeName) {
		this.typeName = typeName;
	}

	/** full constructor */
	public CtGoodsType(String typeName, String enabled) {
		this.typeName = typeName;
		this.enabled = enabled;
	}

	// Property accessors

	public Long getGTId() {
		return this.GTId;
	}

	public void setGTId(Long GTId) {
		this.GTId = GTId;
	}

	public String getTypeName() {
		return this.typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getEnabled() {
		return this.enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

}
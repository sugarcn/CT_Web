package org.ctonline.po.goods;

/**
 * CtGoodsAttributeRelation entity. @author MyEclipse Persistence Tools
 */

public class CtGoodsAttributeRelation implements java.io.Serializable {

	// Fields

	private Long GId;
	private Long attrId;
	private String attrValue;
	private String tag;

	private Long reId;
	
	// Constructors

	public Long getReId() {
		return reId;
	}

	public void setReId(Long reId) {
		this.reId = reId;
	}

	/** default constructor */
	public CtGoodsAttributeRelation() {
	}

	/** full constructor */
	public CtGoodsAttributeRelation(Long attrId, String attrValue, String tag) {
		this.attrId = attrId;
		this.attrValue = attrValue;
		this.tag = tag;
	}

	// Property accessors

	public Long getGId() {
		return this.GId;
	}

	public void setGId(Long GId) {
		this.GId = GId;
	}

	public Long getAttrId() {
		return this.attrId;
	}

	public void setAttrId(Long attrId) {
		this.attrId = attrId;
	}

	public String getAttrValue() {
		return this.attrValue;
	}

	public void setAttrValue(String attrValue) {
		this.attrValue = attrValue;
	}

	public String getTag() {
		return this.tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

}
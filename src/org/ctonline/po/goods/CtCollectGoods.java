package org.ctonline.po.goods;

/**
 * CtCollectGoods entity. @author MyEclipse Persistence Tools
 */

public class CtCollectGoods implements java.io.Serializable {

	// Fields

	private Long coid;
	private Long UId;
	private Long GId;
	private String addTime;
	private Long collid;

	// Constructors

	/** default constructor */
	public CtCollectGoods() {
	}

	/** full constructor */
	public CtCollectGoods(Long UId, Long GId, String addTime , Long collid) {
		this.UId = UId;
		this.GId = GId;
		this.addTime = addTime;
		this.collid=collid;
	}

	// Property accessors

	public Long getCoid() {
		return this.coid;
	}

	public void setCoid(Long coid) {
		this.coid = coid;
	}

	public Long getUId() {
		return this.UId;
	}

	public void setUId(Long UId) {
		this.UId = UId;
	}

	public Long getGId() {
		return this.GId;
	}

	public void setGId(Long GId) {
		this.GId = GId;
	}

	public String getAddTime() {
		return this.addTime;
	}

	public void setAddTime(String addTime) {
		this.addTime = addTime;
	}

	public Long getCollid() {
		return collid;
	}

	public void setCollid(Long collid) {
		this.collid = collid;
	}

}
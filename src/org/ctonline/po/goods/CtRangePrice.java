package org.ctonline.po.goods;

/**
 * CtRangePrice entity. @author MyEclipse Persistence Tools
 */

public class CtRangePrice implements java.io.Serializable {

	// Fields

	private Long RPid;
	private Long GId;
	private Integer simSNum;
	private Integer simENum;
	private Double simRPrice;
	private Integer simIncrease;
	private Integer parSNum;
	private Integer parENum;
	private Double parRPrice;
	private Integer parIncrease;
	private String insTime;
	private String addStr;
	private String addStrForPar;
	
	// Constructors


	public String getAddStrForPar() {
		return addStrForPar;
	}

	public CtRangePrice(Long rPid, Long gId, Integer simSNum, Integer simENum,
			Double simRPrice, Integer simIncrease, Integer parSNum,
			Integer parENum, Double parRPrice, Integer parIncrease,
			String insTime, String addStr, String addStrForPar) {
		super();
		RPid = rPid;
		GId = gId;
		this.simSNum = simSNum;
		this.simENum = simENum;
		this.simRPrice = simRPrice;
		this.simIncrease = simIncrease;
		this.parSNum = parSNum;
		this.parENum = parENum;
		this.parRPrice = parRPrice;
		this.parIncrease = parIncrease;
		this.insTime = insTime;
		this.addStr = addStr;
		this.addStrForPar = addStrForPar;
	}

	public Integer getSimSNum() {
		return simSNum;
	}

	public void setSimSNum(Integer simSNum) {
		this.simSNum = simSNum;
	}

	public Integer getSimENum() {
		return simENum;
	}

	public void setSimENum(Integer simENum) {
		this.simENum = simENum;
	}

	public Integer getSimIncrease() {
		return simIncrease;
	}

	public void setSimIncrease(Integer simIncrease) {
		this.simIncrease = simIncrease;
	}

	public Integer getParSNum() {
		return parSNum;
	}

	public void setParSNum(Integer parSNum) {
		this.parSNum = parSNum;
	}

	public Integer getParENum() {
		return parENum;
	}

	public void setParENum(Integer parENum) {
		this.parENum = parENum;
	}

	public Integer getParIncrease() {
		return parIncrease;
	}

	public void setParIncrease(Integer parIncrease) {
		this.parIncrease = parIncrease;
	}

	public String getInsTime() {
		return insTime;
	}

	public void setInsTime(String insTime) {
		this.insTime = insTime;
	}

	public void setAddStrForPar(String addStrForPar) {
		this.addStrForPar = addStrForPar;
	}

	public String getAddStr() {
		return addStr;
	}

	public void setAddStr(String addStr) {
		this.addStr = addStr;
	}

	/** default constructor */
	public CtRangePrice() {
	}


	// Property accessors

	public Long getRPid() {
		return this.RPid;
	}

	public void setRPid(Long RPid) {
		this.RPid = RPid;
	}

	public Long getGId() {
		return this.GId;
	}

	public void setGId(Long GId) {
		this.GId = GId;
	}


	public Double getSimRPrice() {
		return this.simRPrice;
	}

	public void setSimRPrice(Double simRPrice) {
		this.simRPrice = simRPrice;
	}



	public Double getParRPrice() {
		return this.parRPrice;
	}

	public void setParRPrice(Double parRPrice) {
		this.parRPrice = parRPrice;
	}


}
package org.ctonline.po.goods;

/**
 * CtBomGoods entity. @author MyEclipse Persistence Tools
 */

public class CtBomGoods implements java.io.Serializable {

	// Fields

	private Long bomGoodsId;
	private CtBom ctBom;
	private Long GId;
	private String pack;
	private Long goodsNum;
	private CtGoods goods;
	private Integer bomId;
	private String GPrice;
	private String GBomXh;
	
	private String imgUrl;
	
	// Constructors

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	/** default constructor */
	public CtBomGoods() {
	}

	/** full constructor */
	public CtBomGoods(CtBom ctBom, Long GId, String pack, Long goodsNum,Long bomGoodsId,CtGoods goods,Integer bomId,String GPrice) {
		this.ctBom = ctBom;
		this.GId = GId;
		this.pack = pack;
		this.goodsNum = goodsNum;
		this.bomGoodsId = bomGoodsId;
		this.goods = goods;
		this.bomId = bomId;
		this.GPrice = GPrice;
	}

	// Property accessors

	public Long getBomGoodsId() {
		return this.bomGoodsId;
	}

	public void setBomGoodsId(Long bomGoodsId) {
		this.bomGoodsId = bomGoodsId;
	}

	public CtBom getCtBom() {
		return this.ctBom;
	}

	public void setCtBom(CtBom ctBom) {
		this.ctBom = ctBom;
	}

	public Long getGId() {
		return this.GId;
	}

	public void setGId(Long GId) {
		this.GId = GId;
	}

	public String getPack() {
		return this.pack;
	}

	public void setPack(String pack) {
		this.pack = pack;
	}

	public Long getGoodsNum() {
		return this.goodsNum;
	}

	public void setGoodsNum(Long goodsNum) {
		this.goodsNum = goodsNum;
	}

	public CtGoods getGoods() {
		return goods;
	}

	public void setGoods(CtGoods goods) {
		this.goods = goods;
	}

	public Integer getBomId() {
		return bomId;
	}

	public void setBomId(Integer bomId) {
		this.bomId = bomId;
	}

	public String getGPrice() {
		return GPrice;
	}

	public void setGPrice(String gPrice) {
		GPrice = gPrice;
	}

	public String getGBomXh() {
		return GBomXh;
	}

	public void setGBomXh(String gBomXh) {
		GBomXh = gBomXh;
	}

}
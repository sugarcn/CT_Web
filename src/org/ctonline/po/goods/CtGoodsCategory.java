package org.ctonline.po.goods;

/**
 * CtGoodsCategory entity. @author MyEclipse Persistence Tools
 */

public class CtGoodsCategory implements java.io.Serializable {

	// Fields

	private Long CId;
	private String CName;
	private Long parentId;
	private Long sortOrder;
	private String filterAttr;

	private Long cateGoodsNum;
	private String mouseOverPos;
	private String mouseOutPos;
	
	// Constructors

	public String getMouseOverPos() {
		return mouseOverPos;
	}

	public Long getCateGoodsNum() {
		return cateGoodsNum;
	}

	public void setCateGoodsNum(Long cateGoodsNum) {
		this.cateGoodsNum = cateGoodsNum;
	}

	public void setMouseOverPos(String mouseOverPos) {
		this.mouseOverPos = mouseOverPos;
	}

	public String getMouseOutPos() {
		return mouseOutPos;
	}

	public void setMouseOutPos(String mouseOutPos) {
		this.mouseOutPos = mouseOutPos;
	}

	/** default constructor */
	public CtGoodsCategory() {
	}

	/** minimal constructor */
	public CtGoodsCategory(String CName) {
		this.CName = CName;
	}

	/** full constructor */
	public CtGoodsCategory(String CName, Long parentId, Long sortOrder,
			String filterAttr) {
		this.CName = CName;
		this.parentId = parentId;
		this.sortOrder = sortOrder;
		this.filterAttr = filterAttr;
	}

	// Property accessors

	public Long getCId() {
		return this.CId;
	}

	public void setCId(Long CId) {
		this.CId = CId;
	}

	public String getCName() {
		return this.CName;
	}

	public void setCName(String CName) {
		this.CName = CName;
	}

	public Long getParentId() {
		return this.parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Long getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(Long sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getFilterAttr() {
		return this.filterAttr;
	}

	public void setFilterAttr(String filterAttr) {
		this.filterAttr = filterAttr;
	}

}
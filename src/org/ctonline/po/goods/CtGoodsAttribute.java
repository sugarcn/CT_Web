package org.ctonline.po.goods;

/**
 * CtGoodsAttribute entity. @author MyEclipse Persistence Tools
 */

public class CtGoodsAttribute implements java.io.Serializable {

	// Fields

	private Long attrId;
	private Long GTId;
	private String attrName;
	private Short sortOrder;

	// Constructors

	/** default constructor */
	public CtGoodsAttribute() {
	}

	/** minimal constructor */
	public CtGoodsAttribute(Long GTId) {
		this.GTId = GTId;
	}

	/** full constructor */
	public CtGoodsAttribute(Long GTId, String attrName, Short sortOrder) {
		this.GTId = GTId;
		this.attrName = attrName;
		this.sortOrder = sortOrder;
	}

	// Property accessors

	public Long getAttrId() {
		return this.attrId;
	}

	public void setAttrId(Long attrId) {
		this.attrId = attrId;
	}

	public Long getGTId() {
		return this.GTId;
	}

	public void setGTId(Long GTId) {
		this.GTId = GTId;
	}

	public String getAttrName() {
		return this.attrName;
	}

	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}

	public Short getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(Short sortOrder) {
		this.sortOrder = sortOrder;
	}

}
package org.ctonline.po.goods;

import java.math.BigDecimal;

/**
 * CtGoodsImg entity. @author MyEclipse Persistence Tools
 */

public class CtGoodsImg implements java.io.Serializable {

	// Fields

	private Long PId;
	private String PUrl;
	private String PName;
	private Long GId;
	private Integer sortOrder;
	private Boolean coverImg;

	// Constructors

	/** default constructor */
	public CtGoodsImg() {
	}
	
	public CtGoodsImg(String pUrl) {
		super();
		PUrl = pUrl;
	}

	/** full constructor */
	public CtGoodsImg(String PUrl, String PName, Long GId,
			Integer sortOrder, Boolean coverImg) {
		this.PUrl = PUrl;
		this.PName = PName;
		this.GId = GId;
		this.sortOrder = sortOrder;
		this.coverImg = coverImg;
	}

	// Property accessors

	public Long getPId() {
		return this.PId;
	}

	public void setPId(Long PId) {
		this.PId = PId;
	}

	public String getPUrl() {
		return this.PUrl;
	}

	public void setPUrl(String PUrl) {
		this.PUrl = PUrl;
	}

	public String getPName() {
		return this.PName;
	}

	public void setPName(String PName) {
		this.PName = PName;
	}

	public Long getGId() {
		return this.GId;
	}

	public void setGId(Long GId) {
		this.GId = GId;
	}

	public Integer getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Boolean getCoverImg() {
		return this.coverImg;
	}

	public void setCoverImg(Boolean coverImg) {
		this.coverImg = coverImg;
	}

}
package org.ctonline.po.goods;

/**
 * CtPrice entity. @author MyEclipse Persistence Tools
 */

public class CtPrice implements java.io.Serializable {

	// Fields

	private Long id;
	private Long UId;
	private String UType;
	private Long GId;
	private String GType;
	private String price;

	// Constructors

	/** default constructor */
	public CtPrice() {
	}

	/** full constructor */
	public CtPrice(Long UId, String UType, Long GId, String GType, String price) {
		this.UId = UId;
		this.UType = UType;
		this.GId = GId;
		this.GType = GType;
		this.price = price;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUId() {
		return this.UId;
	}

	public void setUId(Long UId) {
		this.UId = UId;
	}

	public String getUType() {
		return this.UType;
	}

	public void setUType(String UType) {
		this.UType = UType;
	}

	public Long getGId() {
		return this.GId;
	}

	public void setGId(Long GId) {
		this.GId = GId;
	}

	public String getGType() {
		return this.GType;
	}

	public void setGType(String GType) {
		this.GType = GType;
	}

	public String getPrice() {
		return this.price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

}
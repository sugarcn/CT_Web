package org.ctonline.po.goods;

/**
 * CtGoodsGroup entity. @author MyEclipse Persistence Tools
 */

public class CtGoodsGroup implements java.io.Serializable {

	// Fields

	private Long GGId;
	private String GGName;
	private String GGDesc;

	// Constructors

	/** default constructor */
	public CtGoodsGroup() {
	}

	/** minimal constructor */
	public CtGoodsGroup(String GGName) {
		this.GGName = GGName;
	}

	/** full constructor */
	public CtGoodsGroup(String GGName, String GGDesc) {
		this.GGName = GGName;
		this.GGDesc = GGDesc;
	}

	// Property accessors

	public Long getGGId() {
		return this.GGId;
	}

	public void setGGId(Long GGId) {
		this.GGId = GGId;
	}

	public String getGGName() {
		return this.GGName;
	}

	public void setGGName(String GGName) {
		this.GGName = GGName;
	}

	public String getGGDesc() {
		return this.GGDesc;
	}

	public void setGGDesc(String GGDesc) {
		this.GGDesc = GGDesc;
	}

}
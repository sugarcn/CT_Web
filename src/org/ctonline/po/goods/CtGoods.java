package org.ctonline.po.goods;

/**
 * CtGoods entity. @author MyEclipse Persistence Tools
 */

public class CtGoods implements java.io.Serializable {

	// Fields

	private Long GId;
	private String GName;
	private Long CId;
	private String GSn;
	private Long BId;
	private Double marketPrice;
	private Double shopPrice;
	private Double promotePrice;
	private String isOnSale;
	private String GKeywords;
	private Long tokenCredit;
	private Long giveCredit;
	private String GUnit;
	private String pack1;
	private Integer pack1Num;
	private String pack2;
	private Integer pack2Num;
	private String pack3;
	private Integer pack3Num;
	private String pack4;
	private Integer pack4Num;
	private String isDel;
	private String isSubhead;
	private String UName;
	private String isSample;
	private String isPartial;
	private String tname;
	private Long GNum;
	private String section;
	
	
	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public Long getGNum() {
		return GNum;
	}

	public void setGNum(Long gNum) {
		GNum = gNum;
	}

	public String getTname() {
		return tname;
	}

	public void setTname(String tname) {
		this.tname = tname;
	}

	public String getIsSample() {
		return isSample;
	}

	public void setIsSample(String isSample) {
		this.isSample = isSample;
	}

	public String getIsPartial() {
		return isPartial;
	}

	public void setIsPartial(String isPartial) {
		this.isPartial = isPartial;
	}

	private String series;
	private String alw;
	
	private CtGoodsBrand goodsBrand;
	private CtGoodsCategory goodsCategory;

	private CtGoodsImg goodsImg = new CtGoodsImg();
	
	// Constructors

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	public String getAlw() {
		return alw;
	}

	public void setAlw(String alw) {
		this.alw = alw;
	}

	public CtGoodsImg getGoodsImg() {
		return goodsImg;
	}

	public String getUName() {
		return UName;
	}

	public void setUName(String uName) {
		UName = uName;
	}

	public void setGoodsImg(CtGoodsImg goodsImg) {
		this.goodsImg = goodsImg;
	}

	/** default constructor */
	public CtGoods() {
	}

	public String getIsSubhead() {
		return isSubhead;
	}

	public void setIsSubhead(String isSubhead) {
		this.isSubhead = isSubhead;
	}

	/** minimal constructor */
	public CtGoods(String GName, Long CId, String isDel) {
		this.GName = GName;
		this.CId = CId;
		this.isDel = isDel;
	}

	/** full constructor */
	public CtGoods(String GName, Long CId, String GSn, Long BId,
			Double marketPrice, Double shopPrice, Double promotePrice,
			String isOnSale, String GKeywords, Long tokenCredit,
			Long giveCredit, String GUnit, String pack1, Integer pack1Num,
			String pack2, Integer pack2Num, String pack3, Integer pack3Num,
			String pack4, Integer pack4Num, String isDel) {
		this.GName = GName;
		this.CId = CId;
		this.GSn = GSn;
		this.BId = BId;
		this.marketPrice = marketPrice;
		this.shopPrice = shopPrice;
		this.promotePrice = promotePrice;
		this.isOnSale = isOnSale;
		this.GKeywords = GKeywords;
		this.tokenCredit = tokenCredit;
		this.giveCredit = giveCredit;
		this.GUnit = GUnit;
		this.pack1 = pack1;
		this.pack1Num = pack1Num;
		this.pack2 = pack2;
		this.pack2Num = pack2Num;
		this.pack3 = pack3;
		this.pack3Num = pack3Num;
		this.pack4 = pack4;
		this.pack4Num = pack4Num;
		this.isDel = isDel;
	}

	// Property accessors

	public Long getGId() {
		return this.GId;
	}

	public void setGId(Long GId) {
		this.GId = GId;
	}

	public String getGName() {
		return this.GName;
	}

	public void setGName(String GName) {
		this.GName = GName;
	}

	public Long getCId() {
		return this.CId;
	}

	public void setCId(Long CId) {
		this.CId = CId;
	}

	public String getGSn() {
		return this.GSn;
	}

	public void setGSn(String GSn) {
		this.GSn = GSn;
	}

	public Long getBId() {
		return this.BId;
	}

	public void setBId(Long BId) {
		this.BId = BId;
	}

	public Double getMarketPrice() {
		return this.marketPrice;
	}

	public void setMarketPrice(Double marketPrice) {
		this.marketPrice = marketPrice;
	}

	public Double getShopPrice() {
		return this.shopPrice;
	}

	public void setShopPrice(Double shopPrice) {
		this.shopPrice = shopPrice;
	}

	public Double getPromotePrice() {
		return this.promotePrice;
	}

	public void setPromotePrice(Double promotePrice) {
		this.promotePrice = promotePrice;
	}

	public String getIsOnSale() {
		return this.isOnSale;
	}

	public void setIsOnSale(String isOnSale) {
		this.isOnSale = isOnSale;
	}

	public String getGKeywords() {
		return this.GKeywords;
	}

	public void setGKeywords(String GKeywords) {
		this.GKeywords = GKeywords;
	}

	public Long getTokenCredit() {
		return this.tokenCredit;
	}

	public void setTokenCredit(Long tokenCredit) {
		this.tokenCredit = tokenCredit;
	}

	public Long getGiveCredit() {
		return this.giveCredit;
	}

	public void setGiveCredit(Long giveCredit) {
		this.giveCredit = giveCredit;
	}

	public String getGUnit() {
		return this.GUnit;
	}

	public void setGUnit(String GUnit) {
		this.GUnit = GUnit;
	}

	public String getPack1() {
		return this.pack1;
	}

	public void setPack1(String pack1) {
		this.pack1 = pack1;
	}

	public Integer getPack1Num() {
		return this.pack1Num;
	}

	public void setPack1Num(Integer pack1Num) {
		this.pack1Num = pack1Num;
	}

	public String getPack2() {
		return this.pack2;
	}

	public void setPack2(String pack2) {
		this.pack2 = pack2;
	}

	public Integer getPack2Num() {
		return this.pack2Num;
	}

	public void setPack2Num(Integer pack2Num) {
		this.pack2Num = pack2Num;
	}

	public String getPack3() {
		return this.pack3;
	}

	public void setPack3(String pack3) {
		this.pack3 = pack3;
	}

	public Integer getPack3Num() {
		return this.pack3Num;
	}

	public void setPack3Num(Integer pack3Num) {
		this.pack3Num = pack3Num;
	}

	public String getPack4() {
		return this.pack4;
	}

	public void setPack4(String pack4) {
		this.pack4 = pack4;
	}

	public Integer getPack4Num() {
		return this.pack4Num;
	}

	public void setPack4Num(Integer pack4Num) {
		this.pack4Num = pack4Num;
	}

	public String getIsDel() {
		return this.isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	public CtGoodsBrand getGoodsBrand() {
		return goodsBrand;
	}

	public void setGoodsBrand(CtGoodsBrand goodsBrand) {
		this.goodsBrand = goodsBrand;
	}

	public CtGoodsCategory getGoodsCategory() {
		return goodsCategory;
	}

	public void setGoodsCategory(CtGoodsCategory goodsCategory) {
		this.goodsCategory = goodsCategory;
	}

}
package org.ctonline.po.goods;

/**
 * CtGoodsBrand entity. @author MyEclipse Persistence Tools
 */

public class CtGoodsBrand implements java.io.Serializable {

	// Fields

	private Long BId;
	private String BName;
	private String BLogo;
	private String BDesc;
	private String BUrl;
	private Integer sortOrder;
	private Boolean isShow;
	private String BBrief;
	
	
	// Constructors

	public String getBBrief() {
		return BBrief;
	}

	public void setBBrief(String bBrief) {
		BBrief = bBrief;
	}

	/** default constructor */
	public CtGoodsBrand() {
	}

	/** minimal constructor */
	public CtGoodsBrand(String BName) {
		this.BName = BName;
	}

	/** full constructor */
	public CtGoodsBrand(String BName, String BLogo, String BDesc, String BUrl,
			Integer sortOrder, Boolean isShow) {
		this.BName = BName;
		this.BLogo = BLogo;
		this.BDesc = BDesc;
		this.BUrl = BUrl;
		this.sortOrder = sortOrder;
		this.isShow = isShow;
	}

	// Property accessors

	public Long getBId() {
		return this.BId;
	}

	public void setBId(Long BId) {
		this.BId = BId;
	}

	public String getBName() {
		return this.BName;
	}

	public void setBName(String BName) {
		this.BName = BName;
	}

	public String getBLogo() {
		return this.BLogo;
	}

	public void setBLogo(String BLogo) {
		this.BLogo = BLogo;
	}

	public String getBDesc() {
		return this.BDesc;
	}

	public void setBDesc(String BDesc) {
		this.BDesc = BDesc;
	}

	public String getBUrl() {
		return this.BUrl;
	}

	public void setBUrl(String BUrl) {
		this.BUrl = BUrl;
	}

	public Integer getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Boolean getIsShow() {
		return this.isShow;
	}

	public void setIsShow(Boolean isShow) {
		this.isShow = isShow;
	}

}
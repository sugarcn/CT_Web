package org.ctonline.po.goods;

import java.util.HashSet;
import java.util.Set;

/**
 * CtBom entity. @author MyEclipse Persistence Tools
 */

public class CtBom implements java.io.Serializable {

	// Fields

	private Integer bomId;
	private String bomTitle;
	private String bomDesc;
	private String bomTime;
	private Long UId;
	private String isHot;
	private Integer num;
	
	private Integer count;
	
	// Constructors

	/** default constructor */
	public CtBom() {
	}

	/** minimal constructor */
	public CtBom(String bomTitle) {
		this.bomTitle = bomTitle;
	}

	/** full constructor */
	public CtBom(String bomTitle, String bomDesc, String bomTime) {
		this.bomTitle = bomTitle;
		this.bomDesc = bomDesc;
		this.bomTime = bomTime;
	}

	// Property accessors

	public Integer getBomId() {
		return this.bomId;
	}

	public void setBomId(Integer bomId) {
		this.bomId = bomId;
	}

	public String getBomTitle() {
		return this.bomTitle;
	}

	public void setBomTitle(String bomTitle) {
		this.bomTitle = bomTitle;
	}

	public String getBomDesc() {
		return this.bomDesc;
	}

	public void setBomDesc(String bomDesc) {
		this.bomDesc = bomDesc;
	}

	public String getBomTime() {
		return this.bomTime;
	}

	public void setBomTime(String bomTime) {
		this.bomTime = bomTime;
	}

	public Long getUId() {
		return UId;
	}

	public void setUId(Long uId) {
		UId = uId;
	}

	public String getIsHot() {
		return isHot;
	}

	public void setIsHot(String isHot) {
		this.isHot = isHot;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}
}
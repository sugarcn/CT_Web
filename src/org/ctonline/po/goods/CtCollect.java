package org.ctonline.po.goods;

/**
 * CtCollectGoods entity. @author MyEclipse Persistence Tools
 */

public class CtCollect implements java.io.Serializable {

	// Fields

	private Long collid;
	private String colltitle;
	private String colldesc;
	private String colltime;
	private Long UId;


	// Constructors

	/** default constructor */
	public CtCollect() {
	}

	/** full constructor */
	public CtCollect( String colltitle , String colldesc , String colltime , Long UId) {
		this.colltitle=colltitle;
		this.colldesc=colldesc;
		this.colltime = colltime;
		this.UId = UId;		
	}

	public Long getCollid() {
		return collid;
	}

	public void setCollid(Long collid) {
		this.collid = collid;
	}

	public String getColltitle() {
		return colltitle;
	}

	public void setColltitle(String colltitle) {
		this.colltitle = colltitle;
	}

	public String getColldesc() {
		return colldesc;
	}

	public void setColldesc(String colldesc) {
		this.colldesc = colldesc;
	}

	public String getColltime() {
		return colltime;
	}

	public void setColltime(String colltime) {
		this.colltime = colltime;
	}

	public Long getUId() {
		return UId;
	}

	public void setUId(Long uId) {
		UId = uId;
	}

	// Property accessors
}
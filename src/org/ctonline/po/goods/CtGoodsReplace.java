package org.ctonline.po.goods;

/**
 * CtGoodsReplace entity. @author MyEclipse Persistence Tools
 */

public class CtGoodsReplace implements java.io.Serializable {

	// Fields

	private Long GId;
	private Long replaceGId;
	private String isDouble;

	// Constructors

	/** default constructor */
	public CtGoodsReplace() {
	}

	/** minimal constructor */
	public CtGoodsReplace(Long replaceGId) {
		this.replaceGId = replaceGId;
	}

	/** full constructor */
	public CtGoodsReplace(Long replaceGId, String isDouble) {
		this.replaceGId = replaceGId;
		this.isDouble = isDouble;
	}

	// Property accessors

	public Long getGId() {
		return this.GId;
	}

	public void setGId(Long GId) {
		this.GId = GId;
	}

	public Long getReplaceGId() {
		return this.replaceGId;
	}

	public void setReplaceGId(Long replaceGId) {
		this.replaceGId = replaceGId;
	}

	public String getIsDouble() {
		return this.isDouble;
	}

	public void setIsDouble(String isDouble) {
		this.isDouble = isDouble;
	}

}
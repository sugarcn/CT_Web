package org.ctonline.po.pay;

/**
 * CtPayInterface entity. @author MyEclipse Persistence Tools
 */

public class CtPayInterface implements java.io.Serializable {

	// Fields

	private Long PInId;
	private String totalFee;
	private String tradeType;
	public Long getPInId() {
		return PInId;
	}

	public void setPInId(Long pInId) {
		PInId = pInId;
	}

	public String getTradeType() {
		return tradeType;
	}

	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}

	public String getTradeNo() {
		return tradeNo;
	}

	public String getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(String totalFee) {
		this.totalFee = totalFee;
	}

	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}

	public String getOrderSn() {
		return orderSn;
	}

	public void setOrderSn(String orderSn) {
		this.orderSn = orderSn;
	}

	public String getTradeStatus() {
		return tradeStatus;
	}

	public void setTradeStatus(String tradeStatus) {
		this.tradeStatus = tradeStatus;
	}

	private String tradeNo;
	private String orderSn;
	private String tradeStatus;
	private String payDate;
	
	// Constructors

	public String getPayDate() {
		return payDate;
	}

	public void setPayDate(String payDate) {
		this.payDate = payDate;
	}

	/** default constructor */
	public CtPayInterface() {
	}

	/** full constructor */
	public CtPayInterface(Long PInId,String tradeNo, String orderSn, String tradeStatus,String tradeType) {
		this.PInId = PInId;
		this.tradeNo = tradeNo;
		this.orderSn = orderSn;
		this.tradeStatus =tradeStatus;
		this.tradeType = tradeType;
	}

	// Property accessors



}
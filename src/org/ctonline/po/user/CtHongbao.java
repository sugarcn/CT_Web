package org.ctonline.po.user;

import java.math.BigDecimal;

/**
 * Hongbao entity. @author MyEclipse Persistence Tools
 */

public class CtHongbao implements java.io.Serializable {

	// Fields

	private Integer hongbaoId;
	private Long hongbaoUserid;
	private String hongbaoMoney;
	private String hongbaoTime;
	private String hongbaoOpenid;
	private String hongbaoDesc;

	// Constructors

	/** default constructor */
	public CtHongbao() {
	}

	public String getHongbaoDesc() {
		return hongbaoDesc;
	}

	public void setHongbaoDesc(String hongbaoDesc) {
		this.hongbaoDesc = hongbaoDesc;
	}

	/** full constructor */
	public CtHongbao(Long hongbaoUserid, String hongbaoMoney,
			String hongbaoTime, String hongbaoOpenid) {
		this.hongbaoUserid = hongbaoUserid;
		this.hongbaoMoney = hongbaoMoney;
		this.hongbaoTime = hongbaoTime;
		this.hongbaoOpenid = hongbaoOpenid;
	}

	// Property accessors

	public Integer getHongbaoId() {
		return this.hongbaoId;
	}

	public void setHongbaoId(Integer hongbaoId) {
		this.hongbaoId = hongbaoId;
	}

	public Long getHongbaoUserid() {
		return this.hongbaoUserid;
	}

	public void setHongbaoUserid(Long hongbaoUserid) {
		this.hongbaoUserid = hongbaoUserid;
	}

	public String getHongbaoMoney() {
		return this.hongbaoMoney;
	}

	public void setHongbaoMoney(String hongbaoMoney) {
		this.hongbaoMoney = hongbaoMoney;
	}

	public String getHongbaoTime() {
		return this.hongbaoTime;
	}

	public void setHongbaoTime(String hongbaoTime) {
		this.hongbaoTime = hongbaoTime;
	}

	public String getHongbaoOpenid() {
		return this.hongbaoOpenid;
	}

	public void setHongbaoOpenid(String hongbaoOpenid) {
		this.hongbaoOpenid = hongbaoOpenid;
	}

}
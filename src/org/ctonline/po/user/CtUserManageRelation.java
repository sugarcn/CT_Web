package org.ctonline.po.user;

/**
 * CtUserManageRelation entity. @author MyEclipse Persistence Tools
 */

public class CtUserManageRelation implements java.io.Serializable {

	// Fields

	private Long UId;
	private Long MId;
	private String UType;

	// Constructors

	/** default constructor */
	public CtUserManageRelation() {
	}

	/** full constructor */
	public CtUserManageRelation(Long UId, Long MId) {
		this.UId = UId;
		this.MId = MId;
	}

	// Property accessors

	public Long getMId() {
		return this.MId;
	}

	public void setMId(Long MId) {
		this.MId = MId;
	}

	public String getUType() {
		return UType;
	}

	public void setUType(String uType) {
		UType = uType;
	}

	public Long getUId() {
		return UId;
	}

	public void setUId(Long uId) {
		UId = uId;
	}

}
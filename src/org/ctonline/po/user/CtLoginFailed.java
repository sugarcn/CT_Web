package org.ctonline.po.user;

import java.io.Serializable;

public class CtLoginFailed implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4966604836114878393L;
	private Integer fid;
	private String fnetworkMac;
	private String flginName;
	private String floginTime;
	private Boolean fnameOrPass;
	
	
	public Boolean getFnameOrPass() {
		return fnameOrPass;
	}
	public void setFnameOrPass(Boolean fnameOrPass) {
		this.fnameOrPass = fnameOrPass;
	}
	public Integer getFid() {
		return fid;
	}
	public void setFid(Integer fid) {
		this.fid = fid;
	}
	public String getFlginName() {
		return flginName;
	}
	public void setFlginName(String flginName) {
		this.flginName = flginName;
	}
	public String getFloginTime() {
		return floginTime;
	}
	public void setFloginTime(String floginTime) {
		this.floginTime = floginTime;
	}
	public String getFnetworkMac() {
		return fnetworkMac;
	}
	public void setFnetworkMac(String fnetworkMac) {
		this.fnetworkMac = fnetworkMac;
	}
	
}

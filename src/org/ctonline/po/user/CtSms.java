package org.ctonline.po.user;

/**
 * CtSmsId entity. @author MyEclipse Persistence Tools
 */

public class CtSms implements java.io.Serializable {

	// Fields

	private Long UId;
	private String sms;
	private String smsTime;
	private String smsIp;
	private String UMb;

	// Constructors

	/** default constructor */
	public CtSms() {
	}

	/** full constructor */
	public CtSms(Long UId, String sms, String smsTime, String smsIp) {
		this.UId = UId;
		this.sms = sms;
		this.smsTime = smsTime;
		this.smsIp = smsIp;
	}

	// Property accessors

	public Long getUId() {
		return this.UId;
	}

	public void setUId(Long UId) {
		this.UId = UId;
	}

	public String getSms() {
		return this.sms;
	}

	public void setSms(String sms) {
		this.sms = sms;
	}

	public String getSmsTime() {
		return this.smsTime;
	}

	public void setSmsTime(String smsTime) {
		this.smsTime = smsTime;
	}

	public String getSmsIp() {
		return this.smsIp;
	}

	public void setSmsIp(String smsIp) {
		this.smsIp = smsIp;
	}

	public String getUMb() {
		return UMb;
	}

	public void setUMb(String uMb) {
		UMb = uMb;
	}
}
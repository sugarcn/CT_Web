package org.ctonline.po.user;

import java.math.BigDecimal;

/**
 * Hongbao entity. @author MyEclipse Persistence Tools
 */

public class CtHongbaoTemp implements java.io.Serializable {

	// Fields

	private Integer htid;
	private String htdate;
	private String htopenid;
	private String htwym;

	// Constructors

	/** default constructor */
	public CtHongbaoTemp() {
	}



	/** full constructor */
	public CtHongbaoTemp(Integer htid, String htdate,
			String htopenid, String htwym) {
		this.htid = htid;
		this.htdate = htdate;
		this.htopenid = htopenid;
		this.htwym = htwym;
	}



	public Integer getHtid() {
		return htid;
	}



	public void setHtid(Integer htid) {
		this.htid = htid;
	}



	public String getHtdate() {
		return htdate;
	}



	public void setHtdate(String htdate) {
		this.htdate = htdate;
	}



	public String getHtopenid() {
		return htopenid;
	}



	public void setHtopenid(String htopenid) {
		this.htopenid = htopenid;
	}



	public String getHtwym() {
		return htwym;
	}



	public void setHtwym(String htwym) {
		this.htwym = htwym;
	}

	// Property accessors


}
package org.ctonline.po.user;

import java.io.File;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * CtAddTicket entity. @author MyEclipse Persistence Tools
 */

public class CtAddTicket implements java.io.Serializable {

	// Fields

	private Long ATId;
	private Long UId;
	private String companyName;
	private String companySn;
	private String registeAddress;
	private String registeTel;
	private String depositBank;
	private String bankAccount;
	private String lisence;
	private String taxCertifi;
	private String generalCertifi;
	private String bankLisence;
	private String isPass;
	private String lisenceSmall;
	private String taxCertifiSmall;
	private String generalCertifiSmall;
	private String bankLisenceSmall;
	private CtUser ctUser;
	private String theDefault;
	
	private File[] fileNames;
	
	private String lisenceName;
	private String taxCertifiName;
	private String generalCertifiName;
	private String bankLisenceName;
	
	// Constructors


	/** default constructor */
	public CtAddTicket() {
	}

	public String getLisenceName() {
		return lisenceName;
	}

	public void setLisenceName(String lisenceName) {
		this.lisenceName = lisenceName;
	}

	public String getTaxCertifiName() {
		return taxCertifiName;
	}

	public void setTaxCertifiName(String taxCertifiName) {
		this.taxCertifiName = taxCertifiName;
	}

	public String getGeneralCertifiName() {
		return generalCertifiName;
	}

	public void setGeneralCertifiName(String generalCertifiName) {
		this.generalCertifiName = generalCertifiName;
	}

	public String getBankLisenceName() {
		return bankLisenceName;
	}

	public void setBankLisenceName(String bankLisenceName) {
		this.bankLisenceName = bankLisenceName;
	}

	public File[] getFileNames() {
		return fileNames;
	}

	public void setFileNames(File[] fileNames) {
		this.fileNames = fileNames;
	}

	/** minimal constructor */
	public CtAddTicket(Long UId, String companyName, String companySn,
			String registeAddress, String registeTel, String depositBank,
			String bankAccount, String isPass) {
		this.UId = UId;
		this.companyName = companyName;
		this.companySn = companySn;
		this.registeAddress = registeAddress;
		this.registeTel = registeTel;
		this.depositBank = depositBank;
		this.bankAccount = bankAccount;
		this.isPass = isPass;
	}

	/** full constructor */
	public CtAddTicket(Long UId, String companyName, String companySn,
			String registeAddress, String registeTel, String depositBank,
			String bankAccount, String lisence, String taxCertifi,
			String generalCertifi, String bankLisence, String isPass,
			String lisenceSmall, String taxCertifiSmall,
			String generalCertifiSmall, String bankLisenceSmall, CtUser ctUser) {
		this.UId = UId;
		this.companyName = companyName;
		this.companySn = companySn;
		this.registeAddress = registeAddress;
		this.registeTel = registeTel;
		this.depositBank = depositBank;
		this.bankAccount = bankAccount;
		this.lisence = lisence;
		this.taxCertifi = taxCertifi;
		this.generalCertifi = generalCertifi;
		this.bankLisence = bankLisence;
		this.isPass = isPass;
		this.lisenceSmall = lisenceSmall;
		this.taxCertifiSmall = taxCertifiSmall;
		this.generalCertifiSmall = generalCertifiSmall;
		this.bankLisenceSmall = bankLisenceSmall;
		this.ctUser = ctUser;
	}

	// Property accessors


	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanySn() {
		return this.companySn;
	}

	public void setCompanySn(String companySn) {
		this.companySn = companySn;
	}
	
	public String getRegisteAddress() {
		return this.registeAddress;
	}

	public void setRegisteAddress(String registeAddress) {
		this.registeAddress = registeAddress;
	}

	public String getRegisteTel() {
		return this.registeTel;
	}

	public void setRegisteTel(String registeTel) {
		this.registeTel = registeTel;
	}

	public String getDepositBank() {
		return this.depositBank;
	}

	public void setDepositBank(String depositBank) {
		this.depositBank = depositBank;
	}

	public String getBankAccount() {
		return this.bankAccount;
	}

	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	public String getLisence() {
		return this.lisence;
	}

	public void setLisence(String lisence) {
		this.lisence = lisence;
	}

	public String getTaxCertifi() {
		return this.taxCertifi;
	}

	public void setTaxCertifi(String taxCertifi) {
		this.taxCertifi = taxCertifi;
	}

	public String getGeneralCertifi() {
		return this.generalCertifi;
	}

	public void setGeneralCertifi(String generalCertifi) {
		this.generalCertifi = generalCertifi;
	}

	public String getBankLisence() {
		return this.bankLisence;
	}

	public void setBankLisence(String bankLisence) {
		this.bankLisence = bankLisence;
	}

	public String getIsPass() {
		return this.isPass;
	}

	public void setIsPass(String isPass) {
		this.isPass = isPass;
	}

	public String getLisenceSmall() {
		return this.lisenceSmall;
	}

	public void setLisenceSmall(String lisenceSmall) {
		this.lisenceSmall = lisenceSmall;
	}

	public String getTaxCertifiSmall() {
		return this.taxCertifiSmall;
	}

	public void setTaxCertifiSmall(String taxCertifiSmall) {
		this.taxCertifiSmall = taxCertifiSmall;
	}

	public String getGeneralCertifiSmall() {
		return this.generalCertifiSmall;
	}

	public void setGeneralCertifiSmall(String generalCertifiSmall) {
		this.generalCertifiSmall = generalCertifiSmall;
	}

	public String getBankLisenceSmall() {
		return this.bankLisenceSmall;
	}

	public void setBankLisenceSmall(String bankLisenceSmall) {
		this.bankLisenceSmall = bankLisenceSmall;
	}

	public Long getATId() {
		return ATId;
	}

	public void setATId(Long aTId) {
		ATId = aTId;
	}

	public Long getUId() {
		return UId;
	}

	public void setUId(Long uId) {
		UId = uId;
	}

	public CtUser getCtUser() {
		return ctUser;
	}

	public void setCtUser(CtUser ctUser) {
		this.ctUser = ctUser;
	}

	public String getTheDefault() {
		return theDefault;
	}

	public void setTheDefault(String theDefault) {
		this.theDefault = theDefault;
	}

}
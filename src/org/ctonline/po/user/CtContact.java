package org.ctonline.po.user;

/**
 * CtAd entity. @author MyEclipse Persistence Tools
 */

public class CtContact implements java.io.Serializable {

	private Long conId;
	private String cemail;
	private String ctel;
	private String cdesc;
	private String CIp;
	private String ctime;
	private String csales;
	
	public String getCIp() {
		return CIp;
	}
	public void setCIp(String cIp) {
		CIp = cIp;
	}
	public Long getConId() {
		return conId;
	}
	public void setConId(Long conId) {
		this.conId = conId;
	}
	public String getCemail() {
		return cemail;
	}
	public void setCemail(String cemail) {
		this.cemail = cemail;
	}
	public String getCtel() {
		return ctel;
	}
	public void setCtel(String ctel) {
		this.ctel = ctel;
	}
	public String getCdesc() {
		return cdesc;
	}
	public void setCdesc(String cdesc) {
		this.cdesc = cdesc;
	}
	public String getCtime() {
		return ctime;
	}
	public void setCtime(String ctime) {
		this.ctime = ctime;
	}
	public String getCsales() {
		return csales;
	}
	public void setCsales(String csales) {
		this.csales = csales;
	}

}
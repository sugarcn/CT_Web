package org.ctonline.po.user;

/**
 * CtDraw entity. @author MyEclipse Persistence Tools
 */

public class CtDraw implements java.io.Serializable {

	// Fields

	private Integer drId;
	private Integer UId;
	private Integer drNum;

	// Constructors

	/** default constructor */
	public CtDraw() {
	}

	/** full constructor */
	public CtDraw(Integer UId, Integer drNum) {
		this.UId = UId;
		this.drNum = drNum;
	}

	// Property accessors

	public Integer getDrId() {
		return this.drId;
	}

	public void setDrId(Integer drId) {
		this.drId = drId;
	}

	public Integer getUId() {
		return this.UId;
	}

	public void setUId(Integer UId) {
		this.UId = UId;
	}

	public Integer getDrNum() {
		return this.drNum;
	}

	public void setDrNum(Integer drNum) {
		this.drNum = drNum;
	}

}
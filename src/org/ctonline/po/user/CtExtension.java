package org.ctonline.po.user;

import java.math.BigDecimal;

/**
 * CtExtension entity. @author MyEclipse Persistence Tools
 */

public class CtExtension implements java.io.Serializable {

	// Fields

	private Integer extensionId;
	private Long extensionUserid;
	private String extensionDesc;
	private String extensionTime;
	private String extensionName;

	private String extensionMoney;
	
	// Constructors

	public String getExtensionMoney() {
		return extensionMoney;
	}

	public void setExtensionMoney(String extensionMoney) {
		this.extensionMoney = extensionMoney;
	}

	/** default constructor */
	public CtExtension() {
	}

	/** full constructor */
	public CtExtension(Long extensionUserid, String extensionDesc,
			String extensionTime, String extensionName) {
		this.extensionUserid = extensionUserid;
		this.extensionDesc = extensionDesc;
		this.extensionTime = extensionTime;
		this.extensionName = extensionName;
	}

	// Property accessors

	public Integer getExtensionId() {
		return this.extensionId;
	}

	public void setExtensionId(Integer extensionId) {
		this.extensionId = extensionId;
	}

	public Long getExtensionUserid() {
		return this.extensionUserid;
	}

	public void setExtensionUserid(Long extensionUserid) {
		this.extensionUserid = extensionUserid;
	}

	public String getExtensionDesc() {
		return this.extensionDesc;
	}

	public void setExtensionDesc(String extensionDesc) {
		this.extensionDesc = extensionDesc;
	}

	public String getExtensionTime() {
		return this.extensionTime;
	}

	public void setExtensionTime(String extensionTime) {
		this.extensionTime = extensionTime;
	}

	public String getExtensionName() {
		return this.extensionName;
	}

	public void setExtensionName(String extensionName) {
		this.extensionName = extensionName;
	}

}
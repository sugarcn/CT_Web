package org.ctonline.po.user;

/**
 * CtUserGroup entity. @author MyEclipse Persistence Tools
 */

public class CtUserGroup implements java.io.Serializable {

	// Fields

	private Long UGId;
	private String UGName;
	private String UGDesc;

	// Constructors

	/** default constructor */
	public CtUserGroup() {
	}

	/** minimal constructor */
	public CtUserGroup(String UGName) {
		this.UGName = UGName;
	}

	/** full constructor */
	public CtUserGroup(String UGName, String UGDesc) {
		this.UGName = UGName;
		this.UGDesc = UGDesc;
	}

	// Property accessors

	public Long getUGId() {
		return this.UGId;
	}

	public void setUGId(Long UGId) {
		this.UGId = UGId;
	}

	public String getUGName() {
		return this.UGName;
	}

	public void setUGName(String UGName) {
		this.UGName = UGName;
	}

	public String getUGDesc() {
		return this.UGDesc;
	}

	public void setUGDesc(String UGDesc) {
		this.UGDesc = UGDesc;
	}

}
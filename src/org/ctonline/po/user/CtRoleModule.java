package org.ctonline.po.user;

import org.ctonline.po.basic.CtModule;

/**
 * CtRoleModule entity. @author MyEclipse Persistence Tools
 */

public class CtRoleModule implements java.io.Serializable {

	// Fields

	private Long roleModuleId;
	private CtRole ctRole;
	private CtModule ctModule;
	private String right;
	private Long moduleId;
	private Long roleId;

	// Constructors

	/** default constructor */
	public CtRoleModule() {
	}

	/** full constructor */
	public CtRoleModule(CtRole ctRole, CtModule ctModule, String right) {
		this.ctRole = ctRole;
		this.ctModule = ctModule;
		this.right = right;
	}

	// Property accessors

	public Long getRoleModuleId() {
		return this.roleModuleId;
	}

	public void setRoleModuleId(Long roleModuleId) {
		this.roleModuleId = roleModuleId;
	}

	public CtRole getCtRole() {
		return this.ctRole;
	}

	public void setCtRole(CtRole ctRole) {
		this.ctRole = ctRole;
	}

	public CtModule getCtModule() {
		return this.ctModule;
	}

	public void setCtModule(CtModule ctModule) {
		this.ctModule = ctModule;
	}

	public String getRight() {
		return this.right;
	}

	public void setRight(String right) {
		this.right = right;
	}

	public Long getModuleId() {
		return moduleId;
	}

	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}


}
package org.ctonline.po.user;

import java.util.List;
import java.util.Set;

/**
 * CtUser entity. @author MyEclipse Persistence Tools
 */

public class CtUser implements java.io.Serializable {

	// Fields

	private Long UId;
	private String UUserid;
	private String UUsername;
	private String UPassword;
	private String UMb;
	private String UEmail;
	private String UQq;
	private String UWeibo;
	private String UTb;
	private String USex;
	private String UBirthday;
	private String ULastTime;
	private String URegTime;
	private String ULastIp;
	private Long RId;
	private Long UGId;
	private String UState;
	private String UCode;
	private Integer UCredit;
	
	private String nameTemp;
	private String UAliloginUserid;
	private String UQqloginUserid;
	private String UTbloginUserid;
	private String UWeixinloginUserid;
	private String UWeibologinUserid;
	private String URestype;

	private String UExtensionid;
	private String exTenUserId;
	private String UWeixingztime;
	private String UEmp;
	
	private Double UCreditLimit;
	private Double URemainingAmount;
	
	private Boolean UIsLock;
	private String UIsCustomer;
	
	
//	private CtAddTicket ctAddTicket;
	
	// Constructors

	public String getUIsCustomer() {
		return UIsCustomer;
	}

	public void setUIsCustomer(String uIsCustomer) {
		UIsCustomer = uIsCustomer;
	}

	public Boolean getUIsLock() {
		return UIsLock;
	}

	public void setUIsLock(Boolean uIsLock) {
		UIsLock = uIsLock;
	}

	public Double getUCreditLimit() {
		return UCreditLimit;
	}

	public void setUCreditLimit(Double uCreditLimit) {
		UCreditLimit = uCreditLimit;
	}

	public Double getURemainingAmount() {
		return URemainingAmount;
	}

	public void setURemainingAmount(Double uRemainingAmount) {
		URemainingAmount = uRemainingAmount;
	}

	public String getUEmp() {
		return UEmp;
	}

	public void setUEmp(String uEmp) {
		UEmp = uEmp;
	}

	public String getUWeixingztime() {
		return UWeixingztime;
	}

	public void setUWeixingztime(String uWeixingztime) {
		UWeixingztime = uWeixingztime;
	}

	public String getExTenUserId() {
		return exTenUserId;
	}

	public void setExTenUserId(String exTenUserId) {
		this.exTenUserId = exTenUserId;
	}

	public String getUExtensionid() {
		return UExtensionid;
	}

	public void setUExtensionid(String uExtensionid) {
		UExtensionid = uExtensionid;
	}

	public String getUQqloginUserid() {
		return UQqloginUserid;
	}

	public void setUQqloginUserid(String uQqloginUserid) {
		UQqloginUserid = uQqloginUserid;
	}

	public String getUTbloginUserid() {
		return UTbloginUserid;
	}

	public void setUTbloginUserid(String uTbloginUserid) {
		UTbloginUserid = uTbloginUserid;
	}

	public String getUWeixinloginUserid() {
		return UWeixinloginUserid;
	}

	public void setUWeixinloginUserid(String uWeixinloginUserid) {
		UWeixinloginUserid = uWeixinloginUserid;
	}

	public String getUWeibologinUserid() {
		return UWeibologinUserid;
	}

	public void setUWeibologinUserid(String uWeibologinUserid) {
		UWeibologinUserid = uWeibologinUserid;
	}

	public String getURestype() {
		return URestype;
	}

	public void setURestype(String uRestype) {
		URestype = uRestype;
	}

	public String getNameTemp() {
		return nameTemp;
	}

	public String getUAliloginUserid() {
		return UAliloginUserid;
	}

	public void setUAliloginUserid(String uAliloginUserid) {
		UAliloginUserid = uAliloginUserid;
	}

	public void setNameTemp(String nameTemp) {
		this.nameTemp = nameTemp;
	}

	/** default constructor */
	public CtUser() {
	}

	/** minimal constructor */
	public CtUser(String UUserid, String UUsername, String UPassword, String UMb) {
		this.UUserid = UUserid;
		this.UUsername = UUsername;
		this.UPassword = UPassword;
		this.UMb = UMb;
	}

	/** full constructor */
	public CtUser(String UUserid, String UUsername, String UPassword,
			String UMb, String UEmail, String UQq, String UWeibo, String UTb,
			String USex, String UBirthday, String ULastTime, String URegTime,
			String ULastIp, Long RId, Long UGId) {
		this.UUserid = UUserid;
		this.UUsername = UUsername;
		this.UPassword = UPassword;
		this.UMb = UMb;
		this.UEmail = UEmail;
		this.UQq = UQq;
		this.UWeibo = UWeibo;
		this.UTb = UTb;
		this.USex = USex;
		this.UBirthday = UBirthday;
		this.ULastTime = ULastTime;
		this.URegTime = URegTime;
		this.ULastIp = ULastIp;
		this.RId = RId;
		this.UGId = UGId;
	}

	// Property accessors

	public Long getUId() {
		return this.UId;
	}

	public void setUId(Long UId) {
		this.UId = UId;
	}

	public String getUUserid() {
		return this.UUserid;
	}

	public void setUUserid(String UUserid) {
		this.UUserid = UUserid;
	}

	public String getUUsername() {
		return this.UUsername;
	}

	public void setUUsername(String UUsername) {
		this.UUsername = UUsername;
	}

	public String getUPassword() {
		return this.UPassword;
	}

	public void setUPassword(String UPassword) {
		this.UPassword = UPassword;
	}

	public String getUMb() {
		return this.UMb;
	}

	public void setUMb(String UMb) {
		this.UMb = UMb;
	}

	public String getUEmail() {
		return this.UEmail;
	}

	public void setUEmail(String UEmail) {
		this.UEmail = UEmail;
	}

	public String getUQq() {
		return this.UQq;
	}

	public void setUQq(String UQq) {
		this.UQq = UQq;
	}

	public String getUWeibo() {
		return this.UWeibo;
	}

	public void setUWeibo(String UWeibo) {
		this.UWeibo = UWeibo;
	}

	public String getUTb() {
		return this.UTb;
	}

	public void setUTb(String UTb) {
		this.UTb = UTb;
	}

	public String getUSex() {
		return this.USex;
	}

	public void setUSex(String USex) {
		this.USex = USex;
	}

	public String getUBirthday() {
		return this.UBirthday;
	}

	public void setUBirthday(String UBirthday) {
		this.UBirthday = UBirthday;
	}

	public String getULastTime() {
		return this.ULastTime;
	}

	public void setULastTime(String ULastTime) {
		this.ULastTime = ULastTime;
	}

	public String getURegTime() {
		return this.URegTime;
	}

	public void setURegTime(String URegTime) {
		this.URegTime = URegTime;
	}

	public String getULastIp() {
		return this.ULastIp;
	}

	public void setULastIp(String ULastIp) {
		this.ULastIp = ULastIp;
	}

	public Long getRId() {
		return this.RId;
	}

	public void setRId(Long RId) {
		this.RId = RId;
	}

	public Long getUGId() {
		return this.UGId;
	}

	public void setUGId(Long UGId) {
		this.UGId = UGId;
	}

	public String getUState() {
		return UState;
	}

	public void setUState(String uState) {
		UState = uState;
	}

	public String getUCode() {
		return UCode;
	}

	public void setUCode(String uCode) {
		UCode = uCode;
	}

	public Integer getUCredit() {
		return UCredit;
	}

	public void setUCredit(Integer uCredit) {
		UCredit = uCredit;
	}

//	public CtAddTicket getCtAddTicket() {
//		return ctAddTicket;
//	}
//
//	public void setCtAddTicket(CtAddTicket ctAddTicket) {
//		this.ctAddTicket = ctAddTicket;
//	}


}
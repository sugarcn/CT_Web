package org.ctonline.po.user;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * CtRole entity. @author MyEclipse Persistence Tools
 */

public class CtRole implements java.io.Serializable {

	// Fields

	private Integer roleId;
	private String roleName;
	private String roleDesc;

	// Constructors

	/** default constructor */
	public CtRole() {
	}

	/** minimal constructor */
	public CtRole(String roleName, String roleDesc) {
		this.roleName = roleName;
		this.roleDesc = roleDesc;
	}

	// Property accessors

	public Integer getRoleId() {
		return this.roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return this.roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleDesc() {
		return this.roleDesc;
	}

	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}


}
package org.ctonline.po.user;

import org.ctonline.po.basic.CtRegion;

/**
 * CtUserAddress entity. @author MyEclipse Persistence Tools
 */

public class CtUserAddress implements java.io.Serializable {

	// Fields

	private Long AId;
	private Long UId;
	private String AConsignee;
	private Long country;
	private Long province;
	private Long city;
	private Long district;
	private String address;
	private String zipcode;
	private String tel;
	private String mb;
	private String isdefault;
	private String adesc;
	private CtRegion regionCountry;
	private CtRegion regionProvince;
	private CtRegion regionCity;
	private CtRegion regionDistrict;
	private CtUser user;
	private String ACustomer;
	
	private Integer cid;
	
	
	// Constructors

	public Integer getCid() {
		return cid;
	}

	public void setCid(Integer cid) {
		this.cid = cid;
	}

	public String getACustomer() {
		return ACustomer;
	}

	public void setACustomer(String aCustomer) {
		ACustomer = aCustomer;
	}

	/** default constructor */
	public CtUserAddress() {
	}

	/** minimal constructor */
	public CtUserAddress(Long UId) {
		this.UId = UId;
	}

	/** full constructor */
	public CtUserAddress(Long UId, String AConsignee, Long country,
			Long province, Long city, Long district, String address,
			String zipcode, String tel, String mb,String adesc) {
		this.UId = UId;
		this.AConsignee = AConsignee;
		this.country = country;
		this.province = province;
		this.city = city;
		this.district = district;
		this.address = address;
		this.zipcode = zipcode;
		this.tel = tel;
		this.mb = mb;
		this.adesc = adesc;
	}
	// Property accessors

	public Long getAId() {
		return this.AId;
	}

	public void setAId(Long AId) {
		this.AId = AId;
	}

	public Long getUId() {
		return this.UId;
	}

	public void setUId(Long UId) {
		this.UId = UId;
	}

	public String getAConsignee() {
		return this.AConsignee;
	}

	public void setAConsignee(String AConsignee) {
		this.AConsignee = AConsignee;
	}

	public Long getCountry() {
		return this.country;
	}

	public void setCountry(Long country) {
		this.country = country;
	}

	public Long getProvince() {
		return this.province;
	}

	public void setProvince(Long province) {
		this.province = province;
	}

	public Long getCity() {
		return this.city;
	}

	public void setCity(Long city) {
		this.city = city;
	}

	public Long getDistrict() {
		return this.district;
	}

	public void setDistrict(Long district) {
		this.district = district;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZipcode() {
		return this.zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getTel() {
		return this.tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getMb() {
		return this.mb;
	}

	public void setMb(String mb) {
		this.mb = mb;
	}

	public String getIsdefault() {
		return isdefault;
	}

	public void setIsdefault(String isdefault) {
		this.isdefault = isdefault;
	}

	public String getAdesc() {
		return adesc;
	}

	public void setAdesc(String adesc) {
		this.adesc = adesc;
	}

	public CtRegion getRegionCountry() {
		return regionCountry;
	}

	public void setRegionCountry(CtRegion regionCountry) {
		this.regionCountry = regionCountry;
	}

	public CtRegion getRegionProvince() {
		return regionProvince;
	}

	public void setRegionProvince(CtRegion regionProvince) {
		this.regionProvince = regionProvince;
	}

	public CtRegion getRegionCity() {
		return regionCity;
	}

	public void setRegionCity(CtRegion regionCity) {
		this.regionCity = regionCity;
	}

	public CtRegion getRegionDistrict() {
		return regionDistrict;
	}

	public void setRegionDistrict(CtRegion regionDistrict) {
		this.regionDistrict = regionDistrict;
	}

	public CtUser getUser() {
		return user;
	}

	public void setUser(CtUser user) {
		this.user = user;
	}

}
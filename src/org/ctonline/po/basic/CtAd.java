package org.ctonline.po.basic;

/**
 * CtAd entity. @author MyEclipse Persistence Tools
 */

public class CtAd implements java.io.Serializable {

	// Fields

	private Integer adId;
	private String adTitle;
	private String adImg;
	private String adUrl;
	private String adDesc;
	private String adWidth;
	private String adHeight;

	private Integer adSort;
	private String adIsUp;
	
	private Integer adType;
	
	private CtAdType ctAdType;
	
	
	// Constructors

	public CtAdType getCtAdType() {
		return ctAdType;
	}

	public void setCtAdType(CtAdType ctAdType) {
		this.ctAdType = ctAdType;
	}

	public Integer getAdSort() {
		return adSort;
	}

	public void setAdSort(Integer adSort) {
		this.adSort = adSort;
	}

	public String getAdIsUp() {
		return adIsUp;
	}

	public void setAdIsUp(String adIsUp) {
		this.adIsUp = adIsUp;
	}

	public Integer getAdType() {
		return adType;
	}

	public void setAdType(Integer adType) {
		this.adType = adType;
	}

	/** default constructor */
	public CtAd() {
	}

	/** full constructor */
	public CtAd(String adTitle, String adImg, String adUrl, String adDesc,
			String adWidth, String adHeight) {
		this.adTitle = adTitle;
		this.adImg = adImg;
		this.adUrl = adUrl;
		this.adDesc = adDesc;
		this.adWidth = adWidth;
		this.adHeight = adHeight;
	}

	// Property accessors

	public Integer getAdId() {
		return this.adId;
	}

	public void setAdId(Integer adId) {
		this.adId = adId;
	}

	public String getAdTitle() {
		return this.adTitle;
	}

	public void setAdTitle(String adTitle) {
		this.adTitle = adTitle;
	}

	public String getAdImg() {
		return this.adImg;
	}

	public void setAdImg(String adImg) {
		this.adImg = adImg;
	}

	public String getAdUrl() {
		return this.adUrl;
	}

	public void setAdUrl(String adUrl) {
		this.adUrl = adUrl;
	}

	public String getAdDesc() {
		return this.adDesc;
	}

	public void setAdDesc(String adDesc) {
		this.adDesc = adDesc;
	}

	public String getAdWidth() {
		return this.adWidth;
	}

	public void setAdWidth(String adWidth) {
		this.adWidth = adWidth;
	}

	public String getAdHeight() {
		return this.adHeight;
	}

	public void setAdHeight(String adHeight) {
		this.adHeight = adHeight;
	}

}
package org.ctonline.po.basic;

/**
 * CtResourceType entity. @author MyEclipse Persistence Tools
 */

public class CtResourceType implements java.io.Serializable {

	// Fields

	private Long RTId;
	private String RTName;

	// Constructors

	/** default constructor */
	public CtResourceType() {
	}

	/** full constructor */
	public CtResourceType(String RTName) {
		this.RTName = RTName;
	}

	// Property accessors

	public Long getRTId() {
		return this.RTId;
	}

	public void setRTId(Long RTId) {
		this.RTId = RTId;
	}

	public String getRTName() {
		return this.RTName;
	}

	public void setRTName(String RTName) {
		this.RTName = RTName;
	}

}
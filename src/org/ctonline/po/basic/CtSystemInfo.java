package org.ctonline.po.basic;

import com.aliyun.oss.OSSClient;

/**
 * CtUserRank entity. @author MyEclipse Persistence Tools
 */

public class CtSystemInfo implements java.io.Serializable {

	// Fields

	private Integer ID;
	private String CtName="长亭易购";
	private String CtTitle="电子元器件样品及小批量采购平台-长亭易购";
	private String CtDesc="深圳前海长亭易购电子商务有限公司";
	private String CtKey="电阻 电容 厚声UniOhm 华科 walsn 光颉 Viking ";
	private String CtQq="2880608370,2881112887,2881112904";
	private String CtTel="4008-620-062";
	private String CtImgUrl="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com";
	private String CtResUrl="https://ctegores.oss-cn-shenzhen.aliyuncs.com";
	private String CtUFile = "https://ctufile.oss-cn-shenzhen.aliyuncs.com";
	
	private String bucketName = "ctufile";
	private String bucketNameDownLoad = "ctegores";
	private String access_id = "YVMiRqRZ4YvNNUhm";
	private String access_key = "5GCeAQMVlW78nNYn98FBzSzVSFbjjz";
	private String endpoint = "https://oss-cn-shenzhen.aliyuncs.com";
	private String resurl = "https://ctufile.oss-cn-shenzhen.aliyuncs.com/pay";
	private String key = "pay/";
	//private String notify_url = "http://127.0.0.1:8080/CT_Web/alinotify";
	//private String return_url = "http://127.0.0.1:8080/CT_Web/alireturn";
	//private String notify_url = "http://www.ctego.com/alinotify";
	//private String return_url = "http://www.ctego.com/alireturn";
	//private String return_url = "http://www.ctego.com/return_url.jsp";



	public String getResurl() {
		return resurl;
	}


	public void setResurl(String resurl) {
		this.resurl = resurl;
	}




	//需http://格式的完整路径，不能加?id=123这类自定义参数
	//页面跳转同步通知页面路径
	
	public String getCtUFile() {
		return CtUFile;
	}


	public String getBucketNameDownLoad() {
		return bucketNameDownLoad;
	}


	public void setBucketNameDownLoad(String bucketNameDownLoad) {
		this.bucketNameDownLoad = bucketNameDownLoad;
	}


	public String getKey() {
		return key;
	}


	public void setKey(String key) {
		this.key = key;
	}


	public String getBucketName() {
		return bucketName;
	}


	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}


	public String getAccess_id() {
		return access_id;
	}


	public void setAccess_id(String access_id) {
		this.access_id = access_id;
	}


	public String getAccess_key() {
		return access_key;
	}


	public void setAccess_key(String access_key) {
		this.access_key = access_key;
	}


	public String getEndpoint() {
		return endpoint;
	}


	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}


	public void setCtUFile(String ctUFile) {
		CtUFile = ctUFile;
	}

	/** default constructor */
	public CtSystemInfo() {
	}

	/** full constructor */
	public CtSystemInfo(String CtName,String CtTitle,String CtDesc,String CtKey,String CtQq,String CtTel,String CtImgUrl,String CtResUrl) {
		this.CtName = CtName;
		this.CtTitle = CtTitle;
		this.CtDesc = CtDesc;
		this.CtKey = CtKey;
		this.CtQq = CtQq;
		this.CtTel = CtTel;
		this.CtImgUrl = CtImgUrl;
		this.CtResUrl = CtResUrl;
	}

	public Integer getID() {
		return ID;
	}

	public void setID(Integer iD) {
		ID = iD;
	}

	public String getCtName() {
		return CtName;
	}

	public void setCtName(String ctName) {
		CtName = ctName;
	}

	public String getCtTitle() {
		return CtTitle;
	}

	public void setCtTitle(String ctTitle) {
		CtTitle = ctTitle;
	}

	public String getCtDesc() {
		return CtDesc;
	}

	public void setCtDesc(String ctDesc) {
		CtDesc = ctDesc;
	}

	public String getCtKey() {
		return CtKey;
	}

	public void setCtKey(String ctKey) {
		CtKey = ctKey;
	}

	public String getCtQq() {
		return CtQq;
	}

	public void setCtQq(String ctQq) {
		CtQq = ctQq;
	}

	public String getCtTel() {
		return CtTel;
	}

	public void setCtTel(String ctTel) {
		CtTel = ctTel;
	}

	public String getCtImgUrl() {
		return CtImgUrl;
	}

	public void setCtImgUrl(String ctImgUrl) {
		CtImgUrl = ctImgUrl;
	}

	public String getCtResUrl() {
		return CtResUrl;
	}

	public void setCtResUrl(String ctResUrl) {
		CtResUrl = ctResUrl;
	}

	// Property accessors

}
package org.ctonline.po.basic;

import java.util.HashSet;
import java.util.Set;

/**
 * CtModule entity. @author MyEclipse Persistence Tools
 */

public class CtModule implements java.io.Serializable {

	// Fields

	private Integer moduleId;
	private String moduleName;
	private String moduleUrl;
	private String moduleDesc;
	private Integer parentId;
	private String imgUrl;

	// Constructors

	/** default constructor */
	public CtModule() {
	}

	/** minimal constructor */
	public CtModule(String moduleName, String moduleUrl, String moduleDesc,
			Integer parentId) {
		this.moduleName = moduleName;
		this.moduleUrl = moduleUrl;
		this.moduleDesc = moduleDesc;
		this.parentId = parentId;
	}

	/** full constructor */
	public CtModule(String moduleName, String moduleUrl, String moduleDesc,
			Integer parentId, String imgUrl, Set ctRoleModules) {
		this.moduleName = moduleName;
		this.moduleUrl = moduleUrl;
		this.moduleDesc = moduleDesc;
		this.parentId = parentId;
		this.imgUrl = imgUrl;
		
	}

	// Property accessors

	public Integer getModuleId() {
		return this.moduleId;
	}

	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	public String getModuleName() {
		return this.moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getModuleUrl() {
		return this.moduleUrl;
	}

	public void setModuleUrl(String moduleUrl) {
		this.moduleUrl = moduleUrl;
	}

	public String getModuleDesc() {
		return this.moduleDesc;
	}

	public void setModuleDesc(String moduleDesc) {
		this.moduleDesc = moduleDesc;
	}

	public Integer getParentId() {
		return this.parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getImgUrl() {
		return this.imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}


}
package org.ctonline.po.basic;

/**
 * CtResource entity. @author MyEclipse Persistence Tools
 */

public class CtResource implements java.io.Serializable {

	// Fields

	private Long RId;
	private String RName;
	private String RUrl;
	private Long RTId;

	private Integer RSort;
	
	private Long GId;
	
	
	// Constructors

	
	public Long getGId() {
		return GId;
	}

	public Integer getRSort() {
		return RSort;
	}

	public void setRSort(Integer rSort) {
		RSort = rSort;
	}

	public void setGId(Long gId) {
		GId = gId;
	}

	/** default constructor */
	public CtResource() {
	}

	/** minimal constructor */
	public CtResource(String RName) {
		this.RName = RName;
	}

	/** full constructor */
	public CtResource(String RName, String RUrl, Long RTId) {
		this.RName = RName;
		this.RUrl = RUrl;
		this.RTId = RTId;
	}

	// Property accessors

	public Long getRId() {
		return this.RId;
	}

	public void setRId(Long RId) {
		this.RId = RId;
	}

	public String getRName() {
		return this.RName;
	}

	public void setRName(String RName) {
		this.RName = RName;
	}

	public String getRUrl() {
		return this.RUrl;
	}

	public void setRUrl(String RUrl) {
		this.RUrl = RUrl;
	}

	public Long getRTId() {
		return this.RTId;
	}

	public void setRTId(Long RTId) {
		this.RTId = RTId;
	}

}
package org.ctonline.po.basic;

/**
 * CtRegion entity. @author MyEclipse Persistence Tools
 */

public class CtRegion implements java.io.Serializable {

	// Fields

	private Long regionId;
	private String regionName;
	private Long parentId;
	private Integer regionType;
	private String shortname;
	private String citycode;
	private String zipcode;
	private String mergername;
	private String lng;
	private String lat;
	private String pinyin;
	
	// Constructors

	public String getShortname() {
		return shortname;
	}

	public void setShortname(String shortname) {
		this.shortname = shortname;
	}

	public String getCitycode() {
		return citycode;
	}

	public void setCitycode(String citycode) {
		this.citycode = citycode;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getMergername() {
		return mergername;
	}

	public void setMergername(String mergername) {
		this.mergername = mergername;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getPinyin() {
		return pinyin;
	}

	public void setPinyin(String pinyin) {
		this.pinyin = pinyin;
	}

	/** default constructor */
	public CtRegion() {
	}

	/** minimal constructor */
	public CtRegion(String regionName, Long parentId) {
		this.regionName = regionName;
		this.parentId = parentId;
	}

	/** full constructor */
	public CtRegion(String regionName, Long parentId, Integer regionType) {
		this.regionName = regionName;
		this.parentId = parentId;
		this.regionType = regionType;
	}

	// Property accessors

	public Long getRegionId() {
		return this.regionId;
	}

	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}

	public String getRegionName() {
		return this.regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public Long getParentId() {
		return this.parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Integer getRegionType() {
		return this.regionType;
	}

	public void setRegionType(Integer regionType) {
		this.regionType = regionType;
	}

}
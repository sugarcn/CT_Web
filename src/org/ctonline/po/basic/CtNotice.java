package org.ctonline.po.basic;


/**
 * CtNotice entity. @author MyEclipse Persistence Tools
 */

public class CtNotice implements java.io.Serializable {

	// Fields

	private Integer noId;
	private String noTitle;
	private String noContent;
	private String noUrl;
	private String noTime;
	private Integer noTypeId;
	
	private String noTimeNew;
	private String noImgUrl;
	
	
	// Constructors

	public String getNoImgUrl() {
		return noImgUrl;
	}

	public void setNoImgUrl(String noImgUrl) {
		this.noImgUrl = noImgUrl;
	}

	public String getNoTimeNew() {
		return noTimeNew;
	}

	public void setNoTimeNew(String noTimeNew) {
		this.noTimeNew = noTimeNew;
	}

	public Integer getNoTypeId() {
		return noTypeId;
	}

	public void setNoTypeId(Integer noTypeId) {
		this.noTypeId = noTypeId;
	}

	/** default constructor */
	public CtNotice() {
	}

	/** full constructor */
	public CtNotice(String noTitle, String noContent, String noUrl , String noTime) {
		this.noTitle = noTitle;
		this.noContent = noContent;
		this.noUrl = noUrl;
		this.noTime=noTime;
	}

	// Property accessors

	public Integer getNoId() {
		return this.noId;
	}

	public void setNoId(Integer noId) {
		this.noId = noId;
	}

	public String getNoTitle() {
		return this.noTitle;
	}

	public void setNoTitle(String noTitle) {
		this.noTitle = noTitle;
	}

	public String getNoContent() {
		return this.noContent;
	}

	public void setNoContent(String noContent) {
		this.noContent = noContent;
	}

	public String getNoUrl() {
		return this.noUrl;
	}

	public void setNoUrl(String noUrl) {
		this.noUrl = noUrl;
	}

	public String getNoTime() {
		return noTime;
	}

	public void setNoTime(String noTime) {
		this.noTime = noTime;
	}

}
package org.ctonline.po.basic;

/**
 * CtCouponDetail entity. @author MyEclipse Persistence Tools
 */

public class CtCouponDetail implements java.io.Serializable {

	// Fields

	private Integer CDetailId;
	private CtCoupon ctCoupon;
	private String couponNumber;
	private Long UId;
	private String useTime;
	private Long orderId;
	private Double prePrice;
	private Integer releaseUId;
	private String status;
	private String isUse;
	private Integer couponId;

	// Constructors

	/** default constructor */
	public CtCouponDetail() {
	}

	/** minimal constructor */
	public CtCouponDetail(CtCoupon ctCoupon, String couponNumber) {
		this.ctCoupon = ctCoupon;
		this.couponNumber = couponNumber;
	}

	/** full constructor */
	public CtCouponDetail(CtCoupon ctCoupon, String couponNumber, Long UId,
			String useTime, Long orderId, Double prePrice, Integer releaseUId) {
		this.ctCoupon = ctCoupon;
		this.couponNumber = couponNumber;
		this.UId = UId;
		this.useTime = useTime;
		this.orderId = orderId;
		this.prePrice = prePrice;
		this.releaseUId = releaseUId;
	}

	// Property accessors

	public Integer getCDetailId() {
		return this.CDetailId;
	}

	public void setCDetailId(Integer CDetailId) {
		this.CDetailId = CDetailId;
	}

	public CtCoupon getCtCoupon() {
		return this.ctCoupon;
	}

	public void setCtCoupon(CtCoupon ctCoupon) {
		this.ctCoupon = ctCoupon;
	}

	public String getCouponNumber() {
		return this.couponNumber;
	}

	public void setCouponNumber(String couponNumber) {
		this.couponNumber = couponNumber;
	}


	public String getUseTime() {
		return this.useTime;
	}

	public void setUseTime(String useTime) {
		this.useTime = useTime;
	}

	public Long getOrderId() {
		return this.orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Double getPrePrice() {
		return this.prePrice;
	}

	public void setPrePrice(Double prePrice) {
		this.prePrice = prePrice;
	}

	public Integer getReleaseUId() {
		return this.releaseUId;
	}

	public void setReleaseUId(Integer releaseUId) {
		this.releaseUId = releaseUId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIsUse() {
		return isUse;
	}

	public void setIsUse(String isUse) {
		this.isUse = isUse;
	}

	public Integer getCouponId() {
		return couponId;
	}

	public void setCouponId(Integer couponId) {
		this.couponId = couponId;
	}

	public Long getUId() {
		return UId;
	}

	public void setUId(Long uId) {
		UId = uId;
	}

}
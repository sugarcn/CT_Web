package org.ctonline.po.basic;

/**
 * CtSearchKeyword entity. @author MyEclipse Persistence Tools
 */

public class CtSearchKeyword implements java.io.Serializable {

	// Fields

	private Integer SId;
	private String SKeyword;
	private Integer SNum;
	private Integer SGoodsnum;

	// Constructors

	/** default constructor */
	public CtSearchKeyword() {
	}

	/** minimal constructor */
	public CtSearchKeyword(String SKeyword) {
		this.SKeyword = SKeyword;
	}

	/** full constructor */
	public CtSearchKeyword(String SKeyword, Integer SNum, Integer SGoodsnum) {
		this.SKeyword = SKeyword;
		this.SNum = SNum;
		this.SGoodsnum = SGoodsnum;
	}

	// Property accessors

	public Integer getSId() {
		return this.SId;
	}

	public void setSId(Integer SId) {
		this.SId = SId;
	}

	public String getSKeyword() {
		return this.SKeyword;
	}

	public void setSKeyword(String SKeyword) {
		this.SKeyword = SKeyword;
	}

	public Integer getSNum() {
		return this.SNum;
	}

	public void setSNum(Integer SNum) {
		this.SNum = SNum;
	}

	public Integer getSGoodsnum() {
		return this.SGoodsnum;
	}

	public void setSGoodsnum(Integer SGoodsnum) {
		this.SGoodsnum = SGoodsnum;
	}

}
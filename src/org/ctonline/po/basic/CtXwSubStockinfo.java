package org.ctonline.po.basic;

/**
 * CtXwSubStockinfo entity. @author MyEclipse Persistence Tools
 */

public class CtXwSubStockinfo implements java.io.Serializable {

	// Fields

	private Integer subStockId;
	private Integer subStockNum;
	private Double subStockPrice;

	// Constructors

	/** default constructor */
	public CtXwSubStockinfo() {
	}

	/** full constructor */
	public CtXwSubStockinfo(Integer subStockNum, Double subStockPrice) {
		this.subStockNum = subStockNum;
		this.subStockPrice = subStockPrice;
	}

	// Property accessors

	public Integer getSubStockId() {
		return this.subStockId;
	}

	public void setSubStockId(Integer subStockId) {
		this.subStockId = subStockId;
	}

	public Integer getSubStockNum() {
		return this.subStockNum;
	}

	public void setSubStockNum(Integer subStockNum) {
		this.subStockNum = subStockNum;
	}

	public Double getSubStockPrice() {
		return this.subStockPrice;
	}

	public void setSubStockPrice(Double subStockPrice) {
		this.subStockPrice = subStockPrice;
	}

}
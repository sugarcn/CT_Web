package org.ctonline.po.basic;

import java.io.Serializable;

public class CtLoginOperation
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  private Long logId;
  private Integer UId;
  private String logIp;
  private String logTime;
  private String logModule;
  private String logDesc;
  private String logClass;
  private String logState;
  private String logType;
  
  
  public String getLogType() {
	return logType;
}

public void setLogType(String logType) {
	this.logType = logType;
}

public String getLogState() {
	return logState;
}

public void setLogState(String logState) {
	this.logState = logState;
}

public CtLoginOperation() {}
  
  public CtLoginOperation(Integer UId, String logIp, String logTime, String logModule, String logDesc, String logClass)
  {
    this.UId = UId;
    this.logIp = logIp;
    this.logTime = logTime;
    this.logModule = logModule;
    this.logDesc = logDesc;
    this.logClass = logClass;
  }
  
  public Long getLogId()
  {
    return this.logId;
  }
  
  public void setLogId(Long logId)
  {
    this.logId = logId;
  }
  
  public Integer getUId()
  {
    return this.UId;
  }
  
  public void setUId(Integer UId)
  {
    this.UId = UId;
  }
  
  public String getLogIp()
  {
    return this.logIp;
  }
  
  public void setLogIp(String logIp)
  {
    this.logIp = logIp;
  }
  
  public String getLogTime()
  {
    return this.logTime;
  }
  
  public void setLogTime(String logTime)
  {
    this.logTime = logTime;
  }
  
  public String getLogModule()
  {
    return this.logModule;
  }
  
  public void setLogModule(String logModule)
  {
    this.logModule = logModule;
  }
  
  public String getLogDesc()
  {
    return this.logDesc;
  }
  
  public void setLogDesc(String logDesc)
  {
    this.logDesc = logDesc;
  }
  
  public String getLogClass()
  {
    return this.logClass;
  }
  
  public void setLogClass(String logClass)
  {
    this.logClass = logClass;
  }
}

package org.ctonline.po.basic;

/**
 * CtXwStock entity. @author MyEclipse Persistence Tools
 */

public class CtXwStock implements java.io.Serializable {

	// Fields

	private Integer stockId;
	private Long stockNum;
	private Double stockAllprice;

	// Constructors

	/** default constructor */
	public CtXwStock() {
	}

	/** full constructor */
	public CtXwStock(Long stockNum, Double stockAllprice) {
		this.stockNum = stockNum;
		this.stockAllprice = stockAllprice;
	}

	// Property accessors

	public Integer getStockId() {
		return this.stockId;
	}

	public void setStockId(Integer stockId) {
		this.stockId = stockId;
	}

	public Long getStockNum() {
		return this.stockNum;
	}

	public void setStockNum(Long stockNum) {
		this.stockNum = stockNum;
	}

	public Double getStockAllprice() {
		return this.stockAllprice;
	}

	public void setStockAllprice(Double stockAllprice) {
		this.stockAllprice = stockAllprice;
	}

}
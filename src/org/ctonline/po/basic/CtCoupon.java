package org.ctonline.po.basic;

import java.util.HashSet;
import java.util.Set;

import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.goods.CtGoodsGroup;
import org.ctonline.po.goods.CtGoodsImg;

/**
 * CtCoupon entity. @author MyEclipse Persistence Tools
 */

public class CtCoupon implements java.io.Serializable {

	// Fields

	public CtGoods getGoods() {
		return goods;
	}

	public void setGoods(CtGoods goods) {
		this.goods = goods;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer couponId;
	private String couponName;
	private String couponType;
	private Double amount;
	private Double discount;
	private String stime;
	private String etime;
	private Integer GId;
	private String GType;
	private Integer releaseNum;
	private Integer limitNum;
	private CtGoodsImg goodsImg;
	private CtGoodsGroup goodsGroup;
	private CtGoods goods;

	// Constructors

	/** default constructor */
	public CtCoupon() {
	}

	/** minimal constructor */
	public CtCoupon(String couponName, String couponType, Double amount,
			Double discount, String stime, String etime) {
		this.couponName = couponName;
		this.couponType = couponType;
		this.amount = amount;
		this.discount = discount;
		this.stime = stime;
		this.etime = etime;
	}

	/** full constructor */
	public CtCoupon(String couponName, String couponType, Double amount,
			Double discount, String stime, String etime, Integer GId,
			String GType, Integer releaseNum, Integer limitNum) {
		this.couponName = couponName;
		this.couponType = couponType;
		this.amount = amount;
		this.discount = discount;
		this.stime = stime;
		this.etime = etime;
		this.GId = GId;
		this.GType = GType;
		this.releaseNum = releaseNum;
		this.limitNum = limitNum;
	}

	// Property accessors

	public Integer getCouponId() {
		return this.couponId;
	}

	public void setCouponId(Integer couponId) {
		this.couponId = couponId;
	}

	public String getCouponName() {
		return this.couponName;
	}

	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}

	public String getCouponType() {
		return this.couponType;
	}

	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}

	public Double getAmount() {
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getDiscount() {
		return this.discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public String getStime() {
		return this.stime;
	}

	public void setStime(String stime) {
		this.stime = stime;
	}

	public String getEtime() {
		return this.etime;
	}

	public void setEtime(String etime) {
		this.etime = etime;
	}

	public Integer getGId() {
		return this.GId;
	}

	public void setGId(Integer GId) {
		this.GId = GId;
	}

	public String getGType() {
		return this.GType;
	}

	public void setGType(String GType) {
		this.GType = GType;
	}

	public Integer getReleaseNum() {
		return this.releaseNum;
	}

	public void setReleaseNum(Integer releaseNum) {
		this.releaseNum = releaseNum;
	}

	public Integer getLimitNum() {
		return this.limitNum;
	}

	public void setLimitNum(Integer limitNum) {
		this.limitNum = limitNum;
	}

	public CtGoodsImg getGoodsImg() {
		return goodsImg;
	}

	public void setGoodsImg(CtGoodsImg goodsImg) {
		this.goodsImg = goodsImg;
	}

	public CtGoodsGroup getGoodsGroup() {
		return goodsGroup;
	}

	public void setGoodsGroup(CtGoodsGroup goodsGroup) {
		this.goodsGroup = goodsGroup;
	}
}
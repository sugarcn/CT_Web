package org.ctonline.po.basic;

/**
 * CtAd entity. @author MyEclipse Persistence Tools
 */

public class CtSuggestion implements java.io.Serializable {

	private Long suId;
	private String gemail;
	private String gtel;
	private String gdesc;
	
	
	public Long getSuId() {
		return suId;
	}
	public void setSuId(Long suId) {
		this.suId = suId;
	}
	public String getGemail() {
		return gemail;
	}
	public void setGemail(String gemail) {
		this.gemail = gemail;
	}
	public String getGtel() {
		return gtel;
	}
	public void setGtel(String gtel) {
		this.gtel = gtel;
	}
	public String getGdesc() {
		return gdesc;
	}
	public void setGdesc(String gdesc) {
		this.gdesc = gdesc;
	}

	
}
package org.ctonline.po.basic;

import java.math.BigDecimal;

/**
 * CtBasicLog entity. @author MyEclipse Persistence Tools
 */

public class CtBasicLog implements java.io.Serializable {

	// Fields

	private BigDecimal logid;
	private String UUserid;
	private String opertype;
	private String opermodule;
	private String opercontent;
	private String opertime;
	private String operip;

	// Constructors

	/** default constructor */
	public CtBasicLog() {
	}

	/** full constructor */
	public CtBasicLog(String UUserid, String opertype, String opermodule,
			String opercontent, String opertime, String operip) {
		this.UUserid = UUserid;
		this.opertype = opertype;
		this.opermodule = opermodule;
		this.opercontent = opercontent;
		this.opertime = opertime;
		this.operip = operip;
	}

	// Property accessors

	public BigDecimal getLogid() {
		return this.logid;
	}

	public void setLogid(BigDecimal logid) {
		this.logid = logid;
	}

	public String getUUserid() {
		return this.UUserid;
	}

	public void setUUserid(String UUserid) {
		this.UUserid = UUserid;
	}

	public String getOpertype() {
		return this.opertype;
	}

	public void setOpertype(String opertype) {
		this.opertype = opertype;
	}

	public String getOpermodule() {
		return this.opermodule;
	}

	public void setOpermodule(String opermodule) {
		this.opermodule = opermodule;
	}

	public String getOpercontent() {
		return this.opercontent;
	}

	public void setOpercontent(String opercontent) {
		this.opercontent = opercontent;
	}

	public String getOpertime() {
		return this.opertime;
	}

	public void setOpertime(String opertime) {
		this.opertime = opertime;
	}

	public String getOperip() {
		return this.operip;
	}

	public void setOperip(String operip) {
		this.operip = operip;
	}

}
package org.ctonline.po.basic;

/**
 * CtTransaction entity. @author MyEclipse Persistence Tools
 */

public class CtTransaction implements java.io.Serializable {

	// Fields

	private Integer tranId;
	private String tranUsername;
	private String orderSn;
	private String tranTime;
	private String tranCount;
	private String tranPay;
	private String tranOrderStart;
	private String tranIsZhenOrJia;
	
	// Constructors

	public String getTranIsZhenOrJia() {
		return tranIsZhenOrJia;
	}

	public void setTranIsZhenOrJia(String tranIsZhenOrJia) {
		this.tranIsZhenOrJia = tranIsZhenOrJia;
	}

	/** default constructor */
	public CtTransaction() {
	}

	/** full constructor */
	public CtTransaction(String tranUsername, String orderSn, String tranTime,
			String tranCount, String tranPay, String tranOrderStart) {
		this.tranUsername = tranUsername;
		this.orderSn = orderSn;
		this.tranTime = tranTime;
		this.tranCount = tranCount;
		this.tranPay = tranPay;
		this.tranOrderStart = tranOrderStart;
	}

	// Property accessors

	public Integer getTranId() {
		return this.tranId;
	}

	public void setTranId(Integer tranId) {
		this.tranId = tranId;
	}

	public String getTranUsername() {
		return this.tranUsername;
	}

	public void setTranUsername(String tranUsername) {
		this.tranUsername = tranUsername;
	}

	public String getOrderSn() {
		return this.orderSn;
	}

	public void setOrderSn(String orderSn) {
		this.orderSn = orderSn;
	}

	public String getTranTime() {
		return this.tranTime;
	}

	public void setTranTime(String tranTime) {
		this.tranTime = tranTime;
	}

	public String getTranCount() {
		return this.tranCount;
	}

	public void setTranCount(String tranCount) {
		this.tranCount = tranCount;
	}

	public String getTranPay() {
		return this.tranPay;
	}

	public void setTranPay(String tranPay) {
		this.tranPay = tranPay;
	}

	public String getTranOrderStart() {
		return this.tranOrderStart;
	}

	public void setTranOrderStart(String tranOrderStart) {
		this.tranOrderStart = tranOrderStart;
	}

}
package org.ctonline.po.basic;

/**
 * CtAdType entity. @author MyEclipse Persistence Tools
 */

public class CtAdType implements java.io.Serializable {

	// Fields

	private Integer adTypeId;
	private String adTypeName;

	// Constructors

	/** default constructor */
	public CtAdType() {
	}

	/** full constructor */
	public CtAdType(String adTypeName) {
		this.adTypeName = adTypeName;
	}

	// Property accessors

	public Integer getAdTypeId() {
		return this.adTypeId;
	}

	public void setAdTypeId(Integer adTypeId) {
		this.adTypeId = adTypeId;
	}

	public String getAdTypeName() {
		return this.adTypeName;
	}

	public void setAdTypeName(String adTypeName) {
		this.adTypeName = adTypeName;
	}

}
package org.ctonline.po.basic;

/**
 * CtUserRank entity. @author MyEclipse Persistence Tools
 */

public class CtUserRank implements java.io.Serializable {

	// Fields

	private Long RId;
	private String RName;
	private Integer minPoints;
	private Integer maxPoints;

	// Constructors

	/** default constructor */
	public CtUserRank() {
	}

	/** full constructor */
	public CtUserRank(String RName, Integer minPoints, Integer maxPoints) {
		this.RName = RName;
		this.minPoints = minPoints;
		this.maxPoints = maxPoints;
	}

	// Property accessors

	public Long getRId() {
		return this.RId;
	}

	public void setRId(Long RId) {
		this.RId = RId;
	}

	public String getRName() {
		return this.RName;
	}

	public void setRName(String RName) {
		this.RName = RName;
	}

	public Integer getMinPoints() {
		return minPoints;
	}

	public void setMinPoints(Integer minPoints) {
		this.minPoints = minPoints;
	}

	public Integer getMaxPoints() {
		return maxPoints;
	}

	public void setMaxPoints(Integer maxPoints) {
		this.maxPoints = maxPoints;
	}

}
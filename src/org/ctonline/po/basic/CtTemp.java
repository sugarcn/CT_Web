package org.ctonline.po.basic;

/**
 * CtAd entity. @author MyEclipse Persistence Tools
 */

public class CtTemp implements java.io.Serializable {

	// Fields

	private Integer tId;
	private String tName;
	private String tSN;
	private String tNum;
	private String tN;
	private String tT;
	private String tZ;
	private String tD;
	public String gettZ() {
		return tZ;
	}

	public void settZ(String tZ) {
		this.tZ = tZ;
	}

	public String gettD() {
		return tD;
	}

	public void settD(String tD) {
		this.tD = tD;
	}

	public String gettZH() {
		return tZH;
	}

	public void settZH(String tZH) {
		this.tZH = tZH;
	}

	private String tZH;

	// Constructors

	public String gettT() {
		return tT;
	}

	public void settT(String tT) {
		this.tT = tT;
	}

	public String gettN() {
		return tN;
	}

	public void settN(String tN) {
		this.tN = tN;
	}

	/** default constructor */
	public CtTemp() {
	}

	/** full constructor */
	public CtTemp(Integer tId,String tName, String tSN, String tNum,String tN) {
		this.tId = tId;
		this.tName = tName;
		this.tSN = tSN;
		this.tNum = tNum;
		this.tN = tN;
	}

	public Integer gettId() {
		return tId;
	}

	public void settId(Integer tId) {
		this.tId = tId;
	}

	public String gettName() {
		return tName;
	}

	public void settName(String tName) {
		this.tName = tName;
	}

	public String gettSN() {
		return tSN;
	}

	public void settSN(String tSN) {
		this.tSN = tSN;
	}

	public String gettNum() {
		return tNum;
	}

	public void settNum(String tNum) {
		this.tNum = tNum;
	}


}
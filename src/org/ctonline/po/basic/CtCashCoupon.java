package org.ctonline.po.basic;

import java.util.Date;


public class CtCashCoupon implements java.io.Serializable {

	// Fields

	private Integer cashId;
	private String cashName;
	private String orderSn;
	private String cashType;
	private Double amount;
	private Double discount;
	private String stime;
	private String etime;
	private String useTime;
	
	private Date sdatetime;
	private String isDisOk;
	
	
	// Constructors

	public String getIsDisOk() {
		return isDisOk;
	}

	public void setIsDisOk(String isDisOk) {
		this.isDisOk = isDisOk;
	}

	public Date getSdatetime() {
		return sdatetime;
	}

	public void setSdatetime(Date sdatetime) {
		this.sdatetime = sdatetime;
	}

	public String getUseTime() {
		return useTime;
	}

	public void setUseTime(String useTime) {
		this.useTime = useTime;
	}

	/** default constructor */
	public CtCashCoupon() {
	}

	/** full constructor */
	public CtCashCoupon(String cashName, String orderSn, String cashType,
			Double amount, Double discount, String stime, String etime) {
		this.cashName = cashName;
		this.orderSn = orderSn;
		this.cashType = cashType;
		this.amount = amount;
		this.discount = discount;
		this.stime = stime;
		this.etime = etime;
	}

	// Property accessors

	public Integer getCashId() {
		return this.cashId;
	}

	public void setCashId(Integer cashId) {
		this.cashId = cashId;
	}

	public String getCashName() {
		return this.cashName;
	}

	public void setCashName(String cashName) {
		this.cashName = cashName;
	}

	public String getOrderSn() {
		return this.orderSn;
	}

	public void setOrderSn(String orderSn) {
		this.orderSn = orderSn;
	}

	public String getCashType() {
		return this.cashType;
	}

	public void setCashType(String cashType) {
		this.cashType = cashType;
	}

	public Double getAmount() {
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getDiscount() {
		return this.discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public String getStime() {
		return this.stime;
	}

	public void setStime(String stime) {
		this.stime = stime;
	}

	public String getEtime() {
		return this.etime;
	}

	public void setEtime(String etime) {
		this.etime = etime;
	}

}
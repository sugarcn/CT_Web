package org.ctonline.po.basic;

/**
 * CtFreeSampleReg entity. @author MyEclipse Persistence Tools
 */

public class CtFreeSampleReg implements java.io.Serializable {

	// Fields

	private Integer samId;
	private String samUsername;
	private String samUserphone;
	private String samCompanyname;
	private String samCompanytype;
	private String samGoodsName;
	private String samTime;
	private String samReguser;

	// Constructors

	/** default constructor */
	public CtFreeSampleReg() {
	}

	/** full constructor */
	public CtFreeSampleReg(String samUsername, String samUserphone,
			String samCompanyname, String samCompanytype, String samGoodsName,
			String samTime, String samReguser) {
		this.samUsername = samUsername;
		this.samUserphone = samUserphone;
		this.samCompanyname = samCompanyname;
		this.samCompanytype = samCompanytype;
		this.samGoodsName = samGoodsName;
		this.samTime = samTime;
		this.samReguser = samReguser;
	}

	// Property accessors

	public Integer getSamId() {
		return this.samId;
	}

	public void setSamId(Integer samId) {
		this.samId = samId;
	}

	public String getSamUsername() {
		return this.samUsername;
	}

	public void setSamUsername(String samUsername) {
		this.samUsername = samUsername;
	}

	public String getSamUserphone() {
		return this.samUserphone;
	}

	public void setSamUserphone(String samUserphone) {
		this.samUserphone = samUserphone;
	}

	public String getSamCompanyname() {
		return this.samCompanyname;
	}

	public void setSamCompanyname(String samCompanyname) {
		this.samCompanyname = samCompanyname;
	}

	public String getSamCompanytype() {
		return this.samCompanytype;
	}

	public void setSamCompanytype(String samCompanytype) {
		this.samCompanytype = samCompanytype;
	}

	public String getSamGoodsName() {
		return this.samGoodsName;
	}

	public void setSamGoodsName(String samGoodsName) {
		this.samGoodsName = samGoodsName;
	}

	public String getSamTime() {
		return this.samTime;
	}

	public void setSamTime(String samTime) {
		this.samTime = samTime;
	}

	public String getSamReguser() {
		return this.samReguser;
	}

	public void setSamReguser(String samReguser) {
		this.samReguser = samReguser;
	}

}
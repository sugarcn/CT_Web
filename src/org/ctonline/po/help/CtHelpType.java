package org.ctonline.po.help;

import java.util.HashSet;
import java.util.Set;

/**
 * CtHelpType entity. @author MyEclipse Persistence Tools
 */

public class CtHelpType implements java.io.Serializable {

	// Fields

	private Integer HTypeId;
	private String HTypeName;

	// Constructors

	/** default constructor */
	public CtHelpType() {
	}

	/** full constructor */
	public CtHelpType(String HTypeName) {
		this.HTypeName = HTypeName;
	}
	
	// Property accessors

	public Integer getHTypeId() {
		return this.HTypeId;
	}

	public void setHTypeId(Integer HTypeId) {
		this.HTypeId = HTypeId;
	}

	public String getHTypeName() {
		return this.HTypeName;
	}

	public void setHTypeName(String HTypeName) {
		this.HTypeName = HTypeName;
	}

}
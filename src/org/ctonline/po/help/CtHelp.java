package org.ctonline.po.help;

/**
 * CtAd entity. @author MyEclipse Persistence Tools
 */

public class CtHelp implements java.io.Serializable {

	// Fields

	private Integer HId;
	private Integer HTypeId;
	private String HTitle;
	private String HDesc;
	private CtHelpType helptype;
	public CtHelpType getHelptype() {
		return helptype;
	}

	public void setHelptype(CtHelpType helptype) {
		this.helptype = helptype;
	}

	// Constructors
	/** default constructor */
	public CtHelp() {
	}

	/** full constructor */
	public CtHelp(Integer HTypeId, String HTitle,String HDesc) {
		this.HTypeId =HTypeId ;
		this.HTitle =HTitle ;
		this.HDesc =HDesc ;	
	}
	
	public CtHelp(Integer HTypeId, String HTitle,String HDesc,CtHelpType helptype) {
		this.helptype=helptype;
		this.HTypeId =HTypeId ;
		this.HTitle =HTitle ;
		this.HDesc =HDesc ;	
	}
	
	public Integer getHId() {
		return HId;
	}

	public void setHId(Integer hId) {
		HId = hId;
	}

	public Integer getHTypeId() {
		return HTypeId;
	}

	public void setHTypeId(Integer hTypeId) {
		HTypeId = hTypeId;
	}

	public String getHTitle() {
		return HTitle;
	}

	public void setHTitle(String hTitle) {
		HTitle = hTitle;
	}

	public String getHDesc() {
		return HDesc;
	}

	public void setHDesc(String hDesc) {
		HDesc = hDesc;
	}

	// Property accessors

}
package org.ctonline.action.basic;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.RequestAware;
import org.ctonline.action.BaseAction;
import org.ctonline.dto.basic.BasicDTO;
import org.ctonline.manager.basic.BasicManager;
import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.po.views.ViewCateNum;
import org.ctonline.po.views.ViewSubcateNum;

import com.opensymphony.xwork2.ModelDriven;

public class BasicAction  extends BaseAction implements RequestAware, ModelDriven<Object>{
	
	private BasicDTO basicDTO = new BasicDTO();
	private Map<String, Object> request;
	private List<CtGoodsCategory> cateList;
	private List<ViewCateNum> catenumList;
	private List<ViewSubcateNum> subcatenumList;
	private BasicManager basicManager;
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.basicDTO;
	}
	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request; 
	}
	public BasicDTO getBasicDTO() {
		return basicDTO;
	}
	public void setBasicDTO(BasicDTO basicDTO) {
		this.basicDTO = basicDTO;
	}
	public List<CtGoodsCategory> getCateList() {
		return cateList;
	}
	public void setCateList(List<CtGoodsCategory> cateList) {
		this.cateList = cateList;
	}
	public List<ViewCateNum> getCatenumList() {
		return catenumList;
	}
	public void setCatenumList(List<ViewCateNum> catenumList) {
		this.catenumList = catenumList;
	}
	public List<ViewSubcateNum> getSubcatenumList() {
		return subcatenumList;
	}
	public void setSubcatenumList(List<ViewSubcateNum> subcatenumList) {
		this.subcatenumList = subcatenumList;
	}
	public Map<String, Object> getRequest() {
		return request;
	}
	public BasicManager getBasicManager() {
		return basicManager;
	}
	public void setBasicManager(BasicManager basicManager) {
		this.basicManager = basicManager;
	}

}

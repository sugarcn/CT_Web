package org.ctonline.action.basic;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.ctonline.dto.basic.CtRegionDTO;
import org.ctonline.manager.basic.CtRegionManager;
import org.ctonline.po.basic.CtRegion;
import org.ctonline.util.Page;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class CtRegionAction extends ActionSupport implements RequestAware,
		ModelDriven<Object> {
	private List<CtRegion> regions;
	private CtRegionManager regionManager;
	private Map<String, Object> request;
	private Page page;
	private CtRegionDTO regionDTO = new CtRegionDTO();
	private CtRegion region;
	private Long parentId;

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public List<CtRegion> getRegions() {
		return regions;
	}

	public void setRegions(List<CtRegion> regions) {
		this.regions = regions;
	}

	public CtRegionManager getRegionManager() {
		return regionManager;
	}

	public void setRegionManager(CtRegionManager regionManager) {
		this.regionManager = regionManager;
	}

	public CtRegion getRegion() {
		return region;
	}

	public void setRegion(CtRegion region) {
		this.region = region;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.regionDTO;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}

	public String list() {
		Long id = regionDTO.getRegionId();
		Integer type = regionDTO.getRegionType();
		Long pid;
		if (id == null || "".equals(id)) {
			pid = (long) 0;

		} else {
			pid = id;
		}
		if (type == null || "".equals(type)) {
			type = 0;

		} else {
			type += 1;
		}
		int regionType = type;
		Page page = new Page();
		if (regionDTO.getPage() == 0) {
			regionDTO.setPage(1);
		}
		page.setCurrentPage(regionDTO.getPage());
		this.regions = regionManager.searchAll(pid, page);
		page.setTotalPage(page.getTotalPage());
		this.request.put("pages", page);
		CtRegion region = new CtRegion();
		region.setParentId(pid);
		region.setRegionType(regionType);
		this.request.put("pid", region);
		return SUCCESS;
	}

	public String search() {

		Page page = new Page();
		if (regionDTO.getPage() == 0) {
			regionDTO.setPage(1);
		}
		page.setCurrentPage(regionDTO.getPage());
		if (regionDTO.getKeyword() != null
				&& !regionDTO.getKeyword().equals("��������Ҫ��Ĺؼ��")) {
			this.regions = regionManager.findAll(regionDTO.getKeyword(), page);
		} else {
			this.regions = regionManager.loadAll(page);
		}
		page.setTotalPage(page.getTotalPage());
		this.request.put("pages", page);

		return SUCCESS;
	}

	public String add() {

		CtRegion m = new CtRegion();
		m.setRegionName(regionDTO.getRegionName());
		m.setParentId(regionDTO.getParentId());
		m.setRegionType(regionDTO.getRegionType());
		this.regionManager.save(m);
		Long regionId = regionDTO.getParentId();
		parentId = regionId;
		return SUCCESS;
	}

	public String del() {

		for (Long m : regionDTO.getMid()) {
			this.regionManager.delete(m);

		}
		return SUCCESS;
	}

	public String update() {

		CtRegion m = new CtRegion();
		m = this.regionManager.findById(regionDTO.getId());

		m.setRegionName(regionDTO.getRegionName());
		regionManager.update(m);
		parentId=m.getParentId();
		
		return SUCCESS;
	}

	public String find() {

		Long id = regionDTO.getId();
		region = new CtRegion();
		region.setRegionId(id);
		this.region = this.regionManager.findById(id);
		regionDTO.setParentId(region.getParentId());
		regionDTO.setRegionName(region.getRegionName());

		return SUCCESS;

	}
	
	//��ҡ�ʡ���С�����
	public String query(){
		Long id=regionDTO.getRegionId();
		Long pid;
		if (id == null || "".equals(id)) {
			pid = (long) 0;

		} else {
			pid = id;
		}
		this.regions=regionManager.queryById(pid);
		
		return SUCCESS;
	}
	
	public String queryPro(){
		Long id=regionDTO.getRegionId();
		this.regions=regionManager.queryById(id);
		regionDTO.setPlist(regions);
		
		return SUCCESS;
	}
}
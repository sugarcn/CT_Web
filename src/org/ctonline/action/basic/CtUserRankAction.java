package org.ctonline.action.basic;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.ctonline.dto.basic.CtUserRankDTO;
import org.ctonline.manager.basic.CtUserRankManager;
import org.ctonline.manager.user.CtUserManager;
import org.ctonline.po.basic.CtUserRank;
import org.ctonline.po.user.CtUser;
import org.ctonline.util.Page;
import org.omg.CORBA.UserException;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class CtUserRankAction extends ActionSupport implements RequestAware, ModelDriven<Object>,SessionAware{

	private List<CtUserRank> userRanks;
	private CtUserRankManager userRankManager;
	private CtUserManager userManager;
	private Map<String, Object> request;
	private Map<String, Object> session;
	private Page page;
	private CtUserRankDTO userRankDto = new CtUserRankDTO();
	private CtUserRank userRank = new CtUserRank();
	private String msg;
	private CtUser ctUser;
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.userRankDto;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}

	public List<CtUserRank> getUserRanks() {
		return userRanks;
	}

	public void setUserRanks(List<CtUserRank> userRanks) {
		this.userRanks = userRanks;
	}

	public CtUserRankManager getUserRankManager() {
		return userRankManager;
	}

	public void setUserRankManager(CtUserRankManager userRankManager) {
		this.userRankManager = userRankManager;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public CtUserRankDTO getUserRankDto() {
		return userRankDto;
	}

	public void setUserRankDto(CtUserRankDTO userRankDto) {
		this.userRankDto = userRankDto;
	}

	public CtUserRank getUserRank() {
		return userRank;
	}

	public void setUserRank(CtUserRank userRank) {
		this.userRank = userRank;
	}
	
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Map<String, Object> getSession() {
		return session;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public CtUserManager getUserManager() {
		return userManager;
	}

	public void setUserManager(CtUserManager userManager) {
		this.userManager = userManager;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public CtUser getCtUser() {
		return ctUser;
	}

	public void setCtUser(CtUser ctUser) {
		this.ctUser = ctUser;
	}

	public String list(){
		page=new Page();
		if (userRankDto.getPage()==0){
			userRankDto.setPage(1);
		}
		page.setCurrentPage(userRankDto.getPage());
		this.userRanks = userRankManager.loadAll(page);
		page.setTotalPage(page.getTotalPage());
		this.request.put("pages", page);
		userRankDto.setKeyword("璇疯緭鍏ヤ綘瑕佹煡璇㈢殑鍏抽敭璇�");
		return SUCCESS;
	}
	
	//锟斤拷锟斤拷锟饺硷拷锟斤拷锟�
//	public String add(){
//		CtUserRank userRank = new CtUserRank();
//		userRank.setRId(userRankDto.getId());
//		userRank.setRName("V"+userRankDto.getRName());
//		userRank.setMinPoints(userRankDto.getMinPoints());
//		userRank.setMaxPoints(userRankDto.getMaxPoints());
//		
//		Long aa = this.userRankManager.save(userRank);
//		return SUCCESS;
//	}
//	
//	//锟斤拷锟斤拷某一锟斤拷锟饺硷拷锟斤拷锟�
//	
//	public String find(){
//		Long id = userRankDto.getId();
//		userRank = new CtUserRank();
//		userRank.setRId(id);
//		this.userRank = userRankManager.findById(id);
//		userRankDto.setRId(userRank.getRId());
//		userRankDto.setRName(userRank.getRName());
//		userRankDto.setMinPoints(userRank.getMinPoints());
//		userRankDto.setMaxPoints(userRank.getMaxPoints());
//		
//		return SUCCESS;
//	}
//	
//	//锟睫改等硷拷锟斤拷锟�
//	public String update(){
//		
//		CtUserRank m = new CtUserRank();
//		m = this.userRankManager.findById(userRankDto.getId());
//		m.setRName(userRankDto.getRName());
//		m.setMinPoints(userRankDto.getMinPoints());
//		m.setMaxPoints(userRankDto.getMaxPoints());
//		this.userRankManager.update(m);
//		
//		return SUCCESS;
//	}
	
	//删锟斤拷某锟斤拷锟饺硷拷锟斤拷锟�
	public String del(){
		for(Long m:userRankDto.getMid()){
			this.userRankManager.delete(m);
		
		}
		return SUCCESS;
	}
	
	//锟斤拷询
	public String search(){
		Page page=new Page();
		if (userRankDto.getPage()==0){
			userRankDto.setPage(1);
		}
		page.setCurrentPage(userRankDto.getPage());
		if (userRankDto.getKeyword()!=null && !userRankDto.getKeyword().equals("锟斤拷锟斤拷锟斤拷锟斤拷要锟斤拷询锟侥关硷拷锟�")){
			this.userRanks= userRankManager.findAll(userRankDto.getKeyword(),page);
		}else{
			this.userRanks= userRankManager.loadAll(page);
		}
		page.setTotalPage(page.getTotalPage());
		this.request.put("pages", page);
		
		return SUCCESS;
	}
	//我的等级
	public String goMyRank(){
		try {
			CtUser cu = (CtUser) this.session.get("userinfosession");
			this.ctUser = userManager.getCtUserByUUserid(cu.getUUserid());
			Integer UCredit = ctUser.getUCredit();
			if(UCredit !=null){
				this.userRanks = userRankManager.getAll();
				for(int i=0;i<userRanks.size();i++){
					if(UCredit>=userRanks.get(i).getMinPoints() && UCredit<=userRanks.get(i).getMaxPoints()){
						this.userRank.setRName(userRanks.get(i).getRName());
					}
				}
			}else {
				this.userRank.setRName("普通");
				this.ctUser.setUCredit(0);
				this.userRanks = userRankManager.getAll();
			}
			return "goMyRank";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	public String errorpage(){
		return "errorpage";
	}
}

package org.ctonline.action.basic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;

import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.ctonline.action.BaseAction;
import org.ctonline.common.CommonFunc;
import org.ctonline.dto.basic.CtCouponDTO;
import org.ctonline.dto.basic.CtCouponDetailDTO;
import org.ctonline.dto.goods.F_GoodsDTO;
import org.ctonline.manager.basic.BasicManager;
import org.ctonline.manager.basic.CtCouponDetailManager;
import org.ctonline.po.basic.CtCoupon;
import org.ctonline.po.basic.CtResourceType;
import org.ctonline.po.basic.CtCouponDetail;
import org.ctonline.po.user.CtUser;
import org.ctonline.util.Page;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ModelDriven;
import com.sun.org.apache.bcel.internal.generic.NEW;

public class CtCouponDetailAction extends BaseAction implements RequestAware,
		ModelDriven<Object>,SessionAware {

	private static final long serialVersionUID = 1L;
	private List<CtCouponDetail> couponDetailsList;
	private List<CtCouponDetail> couponDetailsAdmitList;
	private List<CtCouponDetail> couponDetailsSamList;
	public List<CtCouponDetail> getCouponDetailsSamList() {
		return couponDetailsSamList;
	}

	public void setCouponDetailsSamList(List<CtCouponDetail> couponDetailsSamList) {
		this.couponDetailsSamList = couponDetailsSamList;
	}

	private BasicManager basicManager;
	private List<?> querylist;
	int id;
	private Map<String, Object> session;

	public CtCouponDetailManager getCoupondetailManager() {
		return coupondetailManager;
	}

	public void setCoupondetailManager(CtCouponDetailManager coupondetailManager) {
		this.coupondetailManager = coupondetailManager;
	}

	public CtCouponDetail getCoupondetail() {
		return coupondetail;
	}

	public void setCoupondetail(CtCouponDetail coupondetail) {
		this.coupondetail = coupondetail;
	}

	public CtCouponDetailDTO getCoupondetailDTO() {
		return coupondetailDTO;
	}

	public void setCoupondetailDTO(CtCouponDetailDTO coupondetailDTO) {
		this.coupondetailDTO = coupondetailDTO;
	}

	public List<?> getQuerylist() {
		return querylist;
	}

	public void setQuerylist(List<?> querylist) {
		this.querylist = querylist;
	}

	private List<CtResourceType> typeList;

	public List<CtResourceType> getTypeList() {
		return typeList;
	}

	public void setTypeList(List<CtResourceType> typeList) {
		this.typeList = typeList;
	}

	private CtCouponDetailManager coupondetailManager;
	private Map<String, Object> request;
	private Page page;
	private CtCouponDetail coupondetail;
	private CtCouponDetailDTO coupondetailDTO = new CtCouponDetailDTO();
	private CtCouponDTO couponDTO = new CtCouponDTO();
	private String filePath;
	private Long cout_all;
	private Long cout_admit;
	private Long cout_sam;
	public Long getCout_sam() {
		return cout_sam;
	}

	public void setCout_sam(Long cout_sam) {
		this.cout_sam = cout_sam;
	}

	private JSONArray resultJson;
	private F_GoodsDTO fgoodsDTO = new F_GoodsDTO();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public List<CtCouponDetail> getCouponDetailsList() {
		return couponDetailsList;
	}

	public void setCouponDetailsList(List<CtCouponDetail> couponDetailsList) {
		this.couponDetailsList = couponDetailsList;
	}

	public List<CtCouponDetail> getCouponDetailsAdmitList() {
		return couponDetailsAdmitList;
	}

	public Long getCout_all() {
		return cout_all;
	}

	public void setCout_all(Long cout_all) {
		this.cout_all = cout_all;
	}

	public Long getCout_admit() {
		return cout_admit;
	}

	public void setCout_admit(Long cout_admit) {
		this.cout_admit = cout_admit;
	}

	public JSONArray getResultJson() {
		return resultJson;
	}

	public CtCouponDTO getCouponDTO() {
		return couponDTO;
	}

	public void setCouponDTO(CtCouponDTO couponDTO) {
		this.couponDTO = couponDTO;
	}

	public void setResultJson(JSONArray resultJson) {
		this.resultJson = resultJson;
	}

	public F_GoodsDTO getFgoodsDTO() {
		return fgoodsDTO;
	}

	public void setFgoodsDTO(F_GoodsDTO fgoodsDTO) {
		this.fgoodsDTO = fgoodsDTO;
	}

	public void setCouponDetailsAdmitList(
			List<CtCouponDetail> couponDetailsAdmitList) {
		this.couponDetailsAdmitList = couponDetailsAdmitList;
	}

	public BasicManager getBasicManager() {
		return basicManager;
	}

	public void setBasicManager(BasicManager basicManager) {
		this.basicManager = basicManager;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public CtCouponDetailDTO getCouponDetailDTO() {
		return coupondetailDTO;
	}

	public void setCouponDetailDTO(CtCouponDetailDTO coupondetailDTO) {
		this.coupondetailDTO = coupondetailDTO;
	}


	public CtCouponDetailManager getCouponDetailManager() {
		return coupondetailManager;
	}

	public void setCouponDetailManager(CtCouponDetailManager coupondetailManager) {
		this.coupondetailManager = coupondetailManager;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public CtCouponDetail getCoupon() {
		return coupondetail;
	}

	public void setCouponDetail(CtCouponDetail coupondetail) {
		this.coupondetail = coupondetail;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.coupondetailDTO;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}
	//优惠券列表
	public String list() {
		try {
			page = new Page();
			if (coupondetailDTO.getPage() == 0) {
				coupondetailDTO.setPage(1);
			}
			page.setCurrentPage(coupondetailDTO.getPage());
			this.cout_all = coupondetailManager.getCoutOwn();
			this.cout_admit = coupondetailManager.getCoutAdmit();
			this.cout_sam = coupondetailManager.getCoutSam();
			String type1 = this.coupondetailDTO.getType1();
			String type2 = this.coupondetailDTO.getType2();
			if("1".equals(type1)){
				this.couponDetailsList = coupondetailManager.loadAll(page,type1,type2);
			}else if("2".equals(type1)){
				this.couponDetailsAdmitList = coupondetailManager.getAdmitGoods(page,type1,type2);
				Map usersession = ActionContext.getContext().getSession();
				CtUser cu = (CtUser) usersession.get("userinfosession");
				Long UId = cu.getUId();
				if(couponDetailsAdmitList != null && couponDetailsAdmitList.size()>0){
					for(int i=0;i<couponDetailsAdmitList.size();i++){
						Long gid = Long.parseLong(couponDetailsAdmitList.get(i).getCtCoupon().getGId().toString());
						String price = this.basicManager.getPrice(UId, gid);
						if(price!=null){
							couponDetailsAdmitList.get(i).getCtCoupon().getGoods().setPromotePrice(Double.parseDouble(price));
						}
					}
				}
			}else if ("3".equals(type1)){
				this.couponDetailsSamList = coupondetailManager.getSamGoods(page,type1,type2);
				
			}
			//page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "list";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}

	}
	//激活优惠券
	public String activate(){
		try {
			String couponNum = this.coupondetailDTO.getCouponNumber();
			CtCouponDetail ccd = this.coupondetailManager.getCtCouponDetailByCouponNum(couponNum);
			if(ccd != null){
				String sts = ccd.getStatus().trim();
				if("1".equals(sts)){
					List<String> l = new ArrayList<String>();
					l.add("alreadyActivate");
					JSONArray json = new JSONArray();
					json = JSONArray.fromObject(l);
					resultJson = json;
				}else {
					Map usersession = ActionContext.getContext().getSession();
					CtUser cu = (CtUser) usersession.get("userinfosession");
					ccd.setUId(cu.getUId());
					ccd.setStatus("1");
					this.coupondetailManager.update(ccd);
					List<String> l = new ArrayList<String>();
					l.add("success");
					JSONArray json = new JSONArray();
					json = JSONArray.fromObject(l);
					resultJson = json;
				}
			}else {
				List<String> l = new ArrayList<String>();
				l.add("error");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
			}
			return SUCCESS;
		} catch (Exception e) {
			return "error"; 
		}
	}
	//获取某个商品的优惠券列表
	public String getCtCouponsByGid(){
		try {
			List<CtCouponDetail> coupons = this.coupondetailManager.getCtCouponsByGid(null,fgoodsDTO.getGId());
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(coupons);
			resultJson = json;
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	//判断某个类型的优惠券是否被领取
	public String isGetCoupom(){
		try {
			Integer couponId = this.coupondetailDTO.getCouponId();
			List<CtCouponDetail> couponDetails = coupondetailManager.isGetCoupom(couponId);
			if(couponDetails != null && couponDetails.size()>0){
				List<String> l = new ArrayList<String>();
				l.add("error");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
			}else {
				//随机分配一个优惠券
				List<CtCouponDetail> couponDetails2 = coupondetailManager.getCouponDetails(couponId);
				if(couponDetails2 == null || couponDetails2.size() == 0){
					List<String> l = new ArrayList<String>();
					l.add("nocoupon");
					JSONArray json = new JSONArray();
					json = JSONArray.fromObject(l);
					resultJson = json;
				}else {
					CtCouponDetail ccd = couponDetails2.get(0);
					if(this.session.get("userinfosession")!=null){
						CtUser cu = (CtUser) this.session.get("userinfosession");
						ccd.setUId(cu.getUId());
						ccd.setStatus("1");
						coupondetailManager.update(ccd);
					}
					List<String> l = new ArrayList<String>();
					l.add("success");
					JSONArray json = new JSONArray();
					json = JSONArray.fromObject(l);
					resultJson = json;
				}
			}
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	
	//仅判断某个类型的优惠券是否被领取
	public String isGetCoupom2(){
		try {
			Integer couponId = this.coupondetailDTO.getCouponId();
			List<CtCouponDetail> couponDetails = coupondetailManager.isGetCoupom(couponId);
			if(couponDetails != null && couponDetails.size()>0){
				List<String> l = new ArrayList<String>();
				l.add("get");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
			}else {
				List<String> l = new ArrayList<String>();
				l.add("notget");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
			}
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	public String errorpage(){
		return "errorpage";
	}
}

package org.ctonline.action.basic;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.ctonline.dto.basic.CtNoticeDTO;
import org.ctonline.manager.basic.CtNoticeManager;
import org.ctonline.manager.help.CtHelpManager;
import org.ctonline.po.basic.CtNotice;
import org.ctonline.po.basic.CtResource;
import org.ctonline.po.help.CtHelp;
import org.ctonline.po.help.CtHelpType;
import org.ctonline.util.Page;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
/**
 * 公告Action
 * @author ZWT
 *
 */
public class CtNoticeAction extends ActionSupport implements RequestAware,
		ModelDriven<Object>,SessionAware {
	private List<CtNotice> notices;
	private CtHelpManager helpManager;
	private List<CtHelpType> typeList;
	private List<CtHelp> helps;
	public CtHelpManager getHelpManager() {
		return helpManager;
	}

	public void setHelpManager(CtHelpManager helpManager) {
		this.helpManager = helpManager;
	}

	public List<CtHelpType> getTypeList() {
		return typeList;
	}

	public void setTypeList(List<CtHelpType> typeList) {
		this.typeList = typeList;
	}

	public List<CtHelp> getHelps() {
		return helps;
	}

	public void setHelps(List<CtHelp> helps) {
		this.helps = helps;
	}

	private CtNoticeManager noticeManager;
	private Page page;
	private CtNoticeDTO noticeDTO = new CtNoticeDTO();
	private Map<String, Object> request;
	private CtNotice notice;
	private Map<String, Object> session;
	public CtNoticeManager getNoticeManager() {
		return noticeManager;
	}

	public void setNoticeManager(CtNoticeManager noticeManager) {
		this.noticeManager = noticeManager;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public CtNoticeDTO getNoticeDTO() {
		return noticeDTO;
	}

	public void setNoticeDTO(CtNoticeDTO noticeDTO) {
		this.noticeDTO = noticeDTO;
	}

	public List<CtNotice> getNotices() {
		return notices;
	}

	public void setNotices(List<CtNotice> notices) {
		this.notices = notices;
	}

	public CtNotice getNotice() {
		return notice;
	}

	public void setNotice(CtNotice notice) {
		this.notice = notice;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.noticeDTO;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}

	public String list() {
		try {
			page = new Page();
			if (noticeDTO.getPage() == 0) {
				noticeDTO.setPage(1);
			}
			page.setCurrentPage(noticeDTO.getPage());
			this.notices = noticeManager.loadAll(page, 1);
			this.typeList = helpManager.queryType();
			this.helps = helpManager.loadAll(page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("noteType", 1);
			this.request.put("pages", page);
			return SUCCESS;
		} catch (Exception e) {
			//e.printStackTrace();
			return "error";
		}
	}
	public String exhibitionInfo() {
		try {
			page = new Page();
			if (noticeDTO.getPage() == 0) {
				noticeDTO.setPage(1);
			}
			page.setCurrentPage(noticeDTO.getPage());
			this.notices = noticeManager.loadAll(page, 24);
			this.typeList = helpManager.queryType();
			this.helps = helpManager.loadAll(page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("noteType", 2);
			this.request.put("pages", page);
			return SUCCESS;
		} catch (Exception e) {
			//e.printStackTrace();
			return "error";
		}
	}
	public String industryDynamicsInfo() {
		try {
			page = new Page();
			if (noticeDTO.getPage() == 0) {
				noticeDTO.setPage(1);
			}
			page.setCurrentPage(noticeDTO.getPage());
			this.notices = noticeManager.loadAll(page, 3);
			this.typeList = helpManager.queryType();
			this.helps = helpManager.loadAll(page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("noteType", 3);
			this.request.put("pages", page);
			return SUCCESS;
		} catch (Exception e) {
			//e.printStackTrace();
			return "error";
		}
	}
	private List<CtResource> resourcesList = new ArrayList<CtResource>();
	
	public List<CtResource> getResourcesList() {
		return resourcesList;
	}

	public void setResourcesList(List<CtResource> resourcesList) {
		this.resourcesList = resourcesList;
	}


	public String find() {
		try {
			page = new Page();
			if (noticeDTO.getPage() == 0) {
				noticeDTO.setPage(1);
			}
			int id = noticeDTO.getNoId();
			notice = new CtNotice();
			notice.setNoId(id);
			this.notice = noticeManager.findById(id);
			this.typeList = helpManager.queryType();
			this.helps = helpManager.loadAll(page);
			return SUCCESS;
		} catch (Exception e) {
			//e.printStackTrace();
			return "error";
		}
	}
	public String search() {
		try {
			Page page = new Page();
			if (noticeDTO.getPage() == 0) {
				noticeDTO.setPage(1);
			}
			page.setCurrentPage(noticeDTO.getPage());
			if (noticeDTO.getKeyword() != null
					&& !noticeDTO.getKeyword().equals("请输入关键字")) {
				this.notices = noticeManager.findAll(noticeDTO.getKeyword(),
						page);
			} else {
				this.notices = noticeManager.loadAll(page, 1);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return SUCCESS;
		} catch (Exception e) {
			//e.printStackTrace();
			return "error";
		}
	}

	public Map<String, Object> getSession() {
		return session;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}
}

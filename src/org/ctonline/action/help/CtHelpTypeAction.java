package org.ctonline.action.help;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.RequestAware;
import org.ctonline.dto.help.CtHelpTypeDTO;
import org.ctonline.manager.help.CtHelpTypeManager;
import org.ctonline.po.help.CtHelpType;
import org.ctonline.util.Page;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class CtHelpTypeAction extends ActionSupport implements RequestAware,
		ModelDriven<Object> {
	private List<CtHelpType> helpTypes;
	private CtHelpTypeManager helpTypeManager;
	private Map<String, Object> request;
	private Page page;
	private CtHelpTypeDTO helpTypeDTO = new CtHelpTypeDTO();
	private CtHelpType helpType;
	private Integer parentId;
	
	public List<CtHelpType> getHelpTypes() {
		return helpTypes;
	}

	public void setHelpTypes(List<CtHelpType> helpTypes) {
		this.helpTypes = helpTypes;
	}

	public CtHelpTypeManager getHelpTypeManager() {
		return helpTypeManager;
	}

	public void setHelpTypeManager(CtHelpTypeManager helpTypeManager) {
		this.helpTypeManager = helpTypeManager;
	}

	public CtHelpTypeDTO getHelpTypeDTO() {
		return helpTypeDTO;
	}

	public void setHelpTypeDTO(CtHelpTypeDTO helpTypeDTO) {
		this.helpTypeDTO = helpTypeDTO;
	}

	public CtHelpType getHelpType() {
		return helpType;
	}

	public void setHelpType(CtHelpType helpType) {
		this.helpType = helpType;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.helpTypeDTO;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}

	public String list() {
		try {
			page = new Page();
			if (helpTypeDTO.getPage() == 0) {
				helpTypeDTO.setPage(1);
			}
			page.setCurrentPage(helpTypeDTO.getPage());
			this.helpTypes = helpTypeManager.loadAll(page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}

	public String add() {
		try {
			CtHelpType a = new CtHelpType();
			a.setHTypeId(helpTypeDTO.getHTypeId());
			a.setHTypeName(helpTypeDTO.getHTypeName());
			String res = this.helpTypeManager.save(a);
			Map session = ActionContext.getContext().getSession();
			if(res.equals("Exerror")){
				String url = helpTypeDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
			}
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}

	public String find() {
		try {
			Integer id = helpTypeDTO.getId();
			helpType = new CtHelpType();
			helpType.setHTypeId(id);
			this.helpType = helpTypeManager.findById(id);
			helpTypeDTO.setHTypeId(helpType.getHTypeId());
			helpTypeDTO.setHTypeName(helpType.getHTypeName());
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}

	public String update() {
		try {
			CtHelpType m = new CtHelpType();
			Integer mid = helpTypeDTO.getHTypeId();
			m = this.helpTypeManager.findById(helpTypeDTO.getId());
			m.setHTypeName(helpTypeDTO.getHTypeName());
			String res=helpTypeManager.update(m);
			Map session = ActionContext.getContext().getSession();
			if(res.equals("Exerror")){
				String url = helpTypeDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
			}
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}

	public String del() {
		try {
			for (int m : helpTypeDTO.getMid()) {
				this.helpTypeManager.delete(m);
			}
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}

	public String search() {
		try {
			Page page = new Page();
			if (helpTypeDTO.getPage() == 0) {
				helpTypeDTO.setPage(1);
			}
			page.setCurrentPage(helpTypeDTO.getPage());
			if (helpTypeDTO.getKeyword() != null
					&& !helpTypeDTO.getKeyword().equals("请输入关键字")) {
				this.helpTypes = helpTypeManager.findAll(helpTypeDTO.getKeyword(), page);
			} else {
				this.helpTypes = helpTypeManager.loadAll(page);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
}

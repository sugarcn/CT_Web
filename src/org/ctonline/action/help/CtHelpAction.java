package org.ctonline.action.help;


import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.RequestAware;
import org.ctonline.common.Solr;
import org.ctonline.dto.goods.CtBrandDTO;
import org.ctonline.dto.help.CtHelpDTO;
import org.ctonline.dto.help.CtHelpTypeDTO;
import org.ctonline.manager.basic.ICtFreeSampleRegManager;
import org.ctonline.manager.goods.F_GoodsManager;
import org.ctonline.manager.help.CtHelpManager;
import org.ctonline.manager.help.CtHelpTypeManager;
import org.ctonline.manager.order.OrderManager;
import org.ctonline.manager.user.CtUserManager;

import org.ctonline.po.basic.CtAd;
import org.ctonline.po.basic.CtFreeSampleReg;
import org.ctonline.po.basic.CtNotice;
import org.ctonline.po.basic.CtResource;
import org.ctonline.po.basic.CtSearchKeyword;
import org.ctonline.po.basic.CtSystemInfo;
import org.ctonline.po.basic.CtTransaction;
import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.goods.CtGoodsBrand;
import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.po.goods.CtGoodsImg;
import org.ctonline.po.help.CtHelp;
import org.ctonline.po.help.CtHelpType;
import org.ctonline.po.order.CtEvaluation;
import org.ctonline.po.order.CtOrderGoods;
import org.ctonline.po.order.CtOrderInfo;
import org.ctonline.po.user.CtUser;
import org.ctonline.util.Page;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class CtHelpAction extends ActionSupport implements RequestAware,
		ModelDriven<Object> {
	private List<CtHelp> helps;
	private CtHelpManager helpManager;
	private CtHelpTypeManager helpTypeManager;
	private CtUserManager userManager;
	private Page page;
	private CtHelp help;
	private CtUser ctUser;
	private CtHelpDTO helpDTO = new CtHelpDTO();
	private Map<String, Object> request;
	private CtHelpType helpType;
	private CtHelpTypeDTO helptypeDTO = new CtHelpTypeDTO();
	private List<CtHelpType> typeList;
	private Map<String, Object> session=ActionContext.getContext().getSession();
	
	private ICtFreeSampleRegManager regManager;
	
	private CtFreeSampleReg sampleReg;
	
	
	public CtFreeSampleReg getSampleReg() {
		return sampleReg;
	}

	public void setSampleReg(CtFreeSampleReg sampleReg) {
		this.sampleReg = sampleReg;
	}

	public ICtFreeSampleRegManager getRegManager() {
		return regManager;
	}

	public void setRegManager(ICtFreeSampleRegManager regManager) {
		this.regManager = regManager;
	}

	public CtUserManager getUserManager() {
		return userManager;
	}

	public void setUserManager(CtUserManager userManager) {
		this.userManager = userManager;
	}

	public CtUser getCtUser() {
		return ctUser;
	}

	public void setCtUser(CtUser ctUser) {
		this.ctUser = ctUser;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public List<CtHelp> getHelps() {
		return helps;
	}

	public void setHelps(List<CtHelp> helps) {
		this.helps = helps;
	}

	public CtHelpManager getHelpManager() {
		return helpManager;
	}

	public void setHelpManager(CtHelpManager helpManager) {
		this.helpManager = helpManager;
	}

	public CtHelpTypeManager getHelpTypeManager() {
		return helpTypeManager;
	}

	public void setHelpTypeManager(CtHelpTypeManager helpTypeManager) {
		this.helpTypeManager = helpTypeManager;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public CtHelp getHelp() {
		return help;
	}

	public void setHelp(CtHelp help) {
		this.help = help;
	}

	public CtHelpDTO getHelpDTO() {
		return helpDTO;
	}

	public void setHelpDTO(CtHelpDTO helpDTO) {
		this.helpDTO = helpDTO;
	}

	public CtHelpType getHelpType() {
		return helpType;
	}

	public void setHelpType(CtHelpType helpType) {
		this.helpType = helpType;
	}

	public CtHelpTypeDTO getHelptypeDTO() {
		return helptypeDTO;
	}

	public void setHelptypeDTO(CtHelpTypeDTO helptypeDTO) {
		this.helptypeDTO = helptypeDTO;
	}

	public List<CtHelpType> getTypeList() {
		return typeList;
	}

	public void setTypeList(List<CtHelpType> typeList) {
		this.typeList = typeList;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.getHelpDTO();
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}
	public String tohelptype() {
		try {
			page = new Page();
			if (helptypeDTO.getPage() == 0) {
				helptypeDTO.setPage(1);
			}
			page.setCurrentPage(helptypeDTO.getPage());
			this.typeList = helpTypeManager.loadAll(page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	public String footlist(){
		try {	
			page = new Page();
			if (helpDTO.getPage() == 0) {
				helpDTO.setPage(1);
			}
			this.typeList = helpManager.queryType();
			Map<String, Object> session = ActionContext.getContext().getSession();
			session.put("typeList", typeList);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	public String footlisth(){
		try {	
			page = new Page();
			if (helpDTO.getPage() == 0) {
				helpDTO.setPage(1);
			}
			this.helps = helpManager.loadAll(page);
			Map<String, Object> session = ActionContext.getContext().getSession();
			session.put("helps", helps);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	
	private String helpStr;
	private String helpStr1;
	
	public String getHelpStr1() {
		return helpStr1;
	}

	public void setHelpStr1(String helpStr1) {
		this.helpStr1 = helpStr1;
	}

	public String getHelpStr() {
		return helpStr;
	}

	public void setHelpStr(String helpStr) {
		this.helpStr = helpStr;
	}

	public String findStrHelp(){
		helpStr = (String) session.get("helpStr");
		//<li><span>关于我们</span><a href="/" target="_blank">长亭易购简介</a><a href="/" target="_blank">线下服务点</a><a href="/" target="_blank">企业文化</a></li>
		//helpStr = new String();
		try {
			if(helpStr == null || helpStr.trim().length() == 0){
				helpStr = "";
				this.typeList = helpManager.queryType();
				this.helps = helpManager.loadAll(null);
				for (int i = 0; i < typeList.size(); i++) {
					helpStr += "<li><span name="+typeList.get(i).getHTypeId()+">"+typeList.get(i).getHTypeName()+"</span>";
					for (int j = 0; j < helps.size(); j++) {
						if(typeList.get(i).getHTypeId() == helps.get(j).getHTypeId()){
							helpStr += "<a href='list_help_desc?help.HId="+helps.get(j).getHId()+"'>"+helps.get(j).getHTitle()+"</a>";
						}
					}
					helpStr += "</li>";
				}
				session.put("helpStr", helpStr);
				session.put("helpStr1", helpStr1);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public String findStrHelp1(){
		helpStr1 = (String) session.get("helpStr1");
		//<li><span>关于我们</span><a href="/" target="_blank">长亭易购简介</a><a href="/" target="_blank">线下服务点</a><a href="/" target="_blank">企业文化</a></li>
		//helpStr = new String();
		if(helpStr1 == null || helpStr1.trim().length() == 0){
			helpStr1 = "";
			this.typeList = helpManager.queryType();
			this.helps = helpManager.loadAll(null);
			for (int i = 0; i < typeList.size(); i++) {
				helpStr1 += "<dl class='fuwe_5' name="+ typeList.get(i).getHTypeId() +" ><dt>" +
						typeList.get(i).getHTypeName()+
						"</dt>";
				for (int j = 0; j < helps.size(); j++) {
					if(typeList.get(i).getHTypeId() == helps.get(j).getHTypeId()){
						helpStr1 += "<dd><a href='list_help_desc?help.HId="+helps.get(j).getHId()+"'>"+
								helps.get(j).getHTitle()
								+"</a></dd>";
					}
				}
				helpStr1 += "</dl>";
			}
			session.put("helpStr1", helpStr1);
		}
		return SUCCESS;
	}
	public String findStrHelpNew(){

		if (session.get("helpStrNew") !=null){
			helpStr1 = (String) session.get("helpStrNew");
		}
		String  temStr="";
		//<li><span>关于我们</span><a href="/" target="_blank">长亭易购简介</a><a href="/" target="_blank">线下服务点</a><a href="/" target="_blank">企业文化</a></li>
		//helpStr = new String();
		if(helpStr1 == null || helpStr1.trim().length() == 0){
			helpStr1 = "";
			this.typeList = helpManager.queryType();
			this.helps = helpManager.loadAll(null);
			temStr += "<ul class=\"ul_1\">";
			
			/*
			for (int i = 0; i < typeList.size(); i++) {
				helpStr1 += "<li name="+ typeList.get(i).getHTypeId() +"><span>|</span>"+typeList.get(i).getHTypeName()+"</li>";
			}
			helpStr1 += "</ul>";
			helpStr1 += " <div class=\"serve\">";
			
			*/
			
			for (int i = 0; i < typeList.size(); i++) {
				temStr += "<li name="+ typeList.get(i).getHTypeId() +"><span>|</span>"+typeList.get(i).getHTypeName()+"</li>";
				
				helpStr1 += "<ul>";
				for (int j = 0; j < helps.size(); j++) {
					if(typeList.get(i).getHTypeId() == helps.get(j).getHTypeId()){
						if(helps.get(j).getHId().toString().equals("1")){
							helpStr1 += "<li><a href=\"introduce.jsp\">"+helps.get(j).getHTitle()+"</a></li>";
						} else {
							helpStr1 += "<li><a href=\"list_help_desc?help.HId="+helps.get(j).getHId()+"\">"+helps.get(j).getHTitle()+"</a></li>";
						}
					}
				}
				helpStr1 += "</ul>";
			}
			
			temStr += "</ul>";
			temStr += " <div class=\"serve\">"+helpStr1;
			
			temStr += "</div>";
			helpStr1 = temStr;
			session.put("helpStrNew", helpStr1);
		}
		return SUCCESS;
	}
	
	public String list() {
		try {	
			page = new Page();
			if (helpDTO.getPage() == 0) {
				helpDTO.setPage(1);
			}
			page.setCurrentPage(helpDTO.getPage());
			if(this.session.get("userinfosession")!=null){
				CtUser cu = (CtUser)this.session.get("userinfosession");	
				ctUser = userManager.getCtUserByUUserid(cu.getUUserid());
			}
			this.typeList = helpManager.queryType();
			this.helps = helpManager.loadAll(page);
			Map<String, Object> session = ActionContext.getContext().getSession();
			session.put("typeList", typeList);
			session.put("helps", helps);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	public String list1() {
		page = new Page();
		if (helpDTO.getPage() == 0) {
			helpDTO.setPage(1);
		}
		page.setCurrentPage(helpDTO.getPage());
		if(this.session.get("userinfosession")!=null){
			CtUser cu = (CtUser)this.session.get("userinfosession");	
			ctUser = userManager.getCtUserByUUserid(cu.getUUserid());
		}
		this.typeList = helpManager.queryType();
		this.helps = helpManager.loadAll(page);		
		page.setTotalPage(page.getTotalPage());
		this.request.put("pages", page);
		if (help!=null){
			Integer hid=help.getHId();
			this.help = helpManager.findById(hid);
			Map<String, Object> session = ActionContext.getContext().getSession();
			session.put("help", help);
			return SUCCESS;
		}else{
			return "error";
		}
			// TODO: handle exception
	}
	

	public String getOneHelp() {
		int HId = 2;
		this.help = helpManager.getHelpByHId(HId);
		return "showad";
	}

	public String add() {
		try {
			CtHelp a = new CtHelp();
			a.setHId(helpDTO.getHId());
			a.setHTitle(helpDTO.getHTitle());			
			a.setHDesc(helpDTO.getHDesc());
			a.setHTypeId(helpDTO.getHTypeId());
			String res= this.helpManager.save(a);
			Map session = ActionContext.getContext().getSession();
			if(res.equals("Exerror")){
				String url = helpDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
		  }
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}

	
	public String find() {
		try {
			Page page = new Page();
			if (helpDTO.getPage() == 0) {
				helpDTO.setPage(1);
			}
			int id = helpDTO.getId();
			CtHelp m = new CtHelp();
			m.setHId(id);
			this.typeList = helpManager.queryType();
			this.help = helpManager.findById(id);
			Map<String, Object> session = ActionContext.getContext().getSession();
			session.put("help", help);
			helpDTO.setHDesc(help.getHDesc());
			helpDTO.setHTitle(help.getHTitle());
			helpDTO.setHTypeId(help.getHTypeId());
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}


	public String del() {
		try {
			for (int m : helpDTO.getMid()) {
				this.helpManager.delete(m);
			}
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	public String saveHelp() {
		try {
			CtHelp help = new CtHelp();
			help.setHTitle(helpDTO.getHTitle());
			help.setHDesc(helpDTO.getHDesc());
			help.setHTypeId(helpDTO.getHTypeId());
			String res = this.helpManager.save(help);
			Map session = ActionContext.getContext().getSession();
			if(res.equals("Exerror")){
				String url = helpDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
		  }
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		
		}
	}
	
	public String update() {
		try {
			CtHelp m = new CtHelp();
			Integer mid = helpDTO.getHId();
			m=this.helpManager.findById(helpDTO.getId());
			m.setHTitle(helpDTO.getHTitle());
			m.setHDesc(helpDTO.getHDesc());
			m.setHTypeId(helpDTO.getHTypeId());
			String res=this.helpManager.update(m);
			Map session = ActionContext.getContext().getSession();
			if(res.equals("Exerror")){
				String url = helpDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
		  }
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}

	public String search() {
		try {
			Page page = new Page();
			if (helpDTO.getPage() == 0) {
				helpDTO.setPage(1);
			}
			page.setCurrentPage(helpDTO.getPage());
			if (helpDTO.getKeyword() != null
					&& !helpDTO.getKeyword().equals("请输入关键字")) {
				this.helps = helpManager.findAll(helpDTO.getKeyword(), page);
			} else {
				this.helps = helpManager.loadAll(page);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	
	public String service_email() {
		try {

			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	
	public String aboutus() {
		try {

			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	private CtNotice notice = new CtNotice();
	private List<CtNotice> noticesList = new ArrayList<CtNotice>();
	
	public CtNotice getNotice() {
		return notice;
	}

	public void setNotice(CtNotice notice) {
		this.notice = notice;
	}

	public List<CtNotice> getNoticesList() {
		return noticesList;
	}

	public void setNoticesList(List<CtNotice> noticesList) {
		this.noticesList = noticesList;
	}
	private CtGoods goods = new CtGoods();
	private List<CtGoods> goodsListZu = new ArrayList<CtGoods>();
	private List<CtGoods> goodsListRong = new ArrayList<CtGoods>();
	
	public CtGoods getGoods() {
		return goods;
	}

	public List<CtGoods> getGoodsListZu() {
		return goodsListZu;
	}

	public void setGoodsListZu(List<CtGoods> goodsListZu) {
		this.goodsListZu = goodsListZu;
	}

	public List<CtGoods> getGoodsListRong() {
		return goodsListRong;
	}

	public void setGoodsListRong(List<CtGoods> goodsListRong) {
		this.goodsListRong = goodsListRong;
	}

	public void setGoods(CtGoods goods) {
		this.goods = goods;
	}
	
	private List<CtGoodsImg> imgList = new ArrayList<CtGoodsImg>();
	private List<CtGoods> goodsListForSub = new ArrayList<CtGoods>();
	
	public List<CtGoodsImg> getImgList() {
		return imgList;
	}

	public void setImgList(List<CtGoodsImg> imgList) {
		this.imgList = imgList;
	}

	public List<CtGoods> getGoodsListForSub() {
		return goodsListForSub;
	}

	public void setGoodsListForSub(List<CtGoods> goodsListForSub) {
		this.goodsListForSub = goodsListForSub;
	}
	private F_GoodsManager fgoodsManager;
	
	public F_GoodsManager getFgoodsManager() {
		return fgoodsManager;
	}

	public void setFgoodsManager(F_GoodsManager fgoodsManager) {
		this.fgoodsManager = fgoodsManager;
	}

	public HttpSession getSessions() {
		return sessions;
	}

	public void setSessions(HttpSession sessions) {
		this.sessions = sessions;
	}

	public ServletContext getApplication() {
		return application;
	}

	public void setApplication(ServletContext application) {
		this.application = application;
	}

	private void getSubList() {
		goodsListForSub = fgoodsManager.getSubHade();
		for (int i = 0; i < goodsListForSub.size(); i++) {
			imgList = fgoodsManager.getImg(goodsListForSub.get(i).getGId());
			CtGoodsImg img = null;
			if(imgList != null && imgList.size() > 0){
				img = imgList.get(0);
			}
			if(img != null){
				goodsListForSub.get(i).setGoodsImg(img);
			} else {
				img = new CtGoodsImg();
				img.setPUrl("order//${systemInfo.ctImgUrl }//ct_s.gif");
				goodsListForSub.get(i).setGoodsImg(img);
			}
			if(goodsListForSub.get(i).getUName() == null){
				goodsListForSub.get(i).setUName("1210 F 1.54KT");
			}
		}
		application.setAttribute("goodsListForSub", goodsListForSub);
	}
	
	HttpSession sessions = ServletActionContext.getRequest().getSession();
	ServletContext application = sessions.getServletContext();
	//记录二级分类对应的三级分类集合
	private Map<String, List<CtGoodsCategory>> bodyCareMap = new HashMap<String, List<CtGoodsCategory>>();
	private List<CtGoodsCategory> cateList = new ArrayList<CtGoodsCategory>();
	private Map<Long,String> cateErjiName = new HashMap<Long, String>();

	public Map<Long, String> getCateErjiName() {
		return cateErjiName;
	}

	public void setCateErjiName(Map<Long, String> cateErjiName) {
		this.cateErjiName = cateErjiName;
	}

	public List<CtGoodsCategory> getCateList() {
		return cateList;
	}

	public void setCateList(List<CtGoodsCategory> cateList) {
		this.cateList = cateList;
	}

	public Map<String, List<CtGoodsCategory>> getBodyCareMap() {
		return bodyCareMap;
	}

	public void setBodyCareMap(Map<String, List<CtGoodsCategory>> bodyCareMap) {
		this.bodyCareMap = bodyCareMap;
	}

	private OrderManager orderManager;
	
	public OrderManager getOrderManager() {
		return orderManager;
	}

	public void setOrderManager(OrderManager orderManager) {
		this.orderManager = orderManager;
	}
	private List<CtEvaluation> evaList = new ArrayList<CtEvaluation>();
	
	public List<CtEvaluation> getEvaList() {
		return evaList;
	}

	public void setEvaList(List<CtEvaluation> evaList) {
		this.evaList = evaList;
	}
	private List<CtGoods> goodsList;
	private List<CtGoods> goodsList1;
	
	//加载首页
	public List<CtGoods> getGoodsList() {
		return goodsList;
	}

	public void setGoodsList(List<CtGoods> goodsList) {
		this.goodsList = goodsList;
	}
	private List<CtBrandDTO> brandDTOs = new ArrayList<CtBrandDTO>();
	private List<List<CtBrandDTO>> brandDTOList = new ArrayList<List<CtBrandDTO>>();
	
	public List<List<CtBrandDTO>> getBrandDTOList() {
		return brandDTOList;
	}

	public void setBrandDTOList(List<List<CtBrandDTO>> brandDTOList) {
		this.brandDTOList = brandDTOList;
	}

	public List<CtBrandDTO> getBrandDTOs() {
		return brandDTOs;
	}

	public void setBrandDTOs(List<CtBrandDTO> brandDTOs) {
		this.brandDTOs = brandDTOs;
	}
	private List<CtSearchKeyword> searchKeywordsList = new ArrayList<CtSearchKeyword>();
	
	public List<CtSearchKeyword> getSearchKeywordsList() {
		return searchKeywordsList;
	}

	public void setSearchKeywordsList(List<CtSearchKeyword> searchKeywordsList) {
		this.searchKeywordsList = searchKeywordsList;
	}

//	public String index1() {
//		searchKeywordsList = fgoodsManager.findKeyWordOrderTop4();
//        session.put("searchKeywordsList", searchKeywordsList);
//		Map<String, Object>  session = ServletActionContext.getContext().getSession();;
//		try {
//			noticesList = helpManager.findAllNotices();
//			
//			//免费样品
//			goodsListZu = getDianZuForSolr("\"电阻\"",18);
//			goodsListRong = getDianZuForSolr("\"电容\"",13);
//            //goodsListZu = helpManager.getDianZuGoods();
//
//			CtSystemInfo systemInfo = new CtSystemInfo();
//			
//			for (int i = 0; i < goodsListZu.size(); i++) {
//				this.imgList = helpManager.getImg(goodsListZu.get(i).getGId());
//				if(imgList != null && imgList.size() > 0){
//					CtGoodsImg img = new CtGoodsImg();
//					for (int j = 0; j < imgList.size(); j++) {
//						if(imgList.get(j).getCoverImg()){
//							String path = systemInfo.getCtImgUrl() + "//"+imgList.get(j).getPUrl();
//							img.setPUrl(path);
//						}
//					}
//					goodsListZu.get(i).setGoodsImg(img);
//				} else {
//					goodsListZu.get(i).setGoodsImg(new CtGoodsImg(systemInfo.getCtImgUrl() + "//ct_s.gif"));
//				}
//			}
//			session.put("goodsListZu", goodsListZu);
//			
//			
//			for (int i = 0; i < goodsListRong.size(); i++) {
//				this.imgList = helpManager.getImg(goodsListRong.get(i).getGId());
//				if(imgList != null && imgList.size() > 0){
//					CtGoodsImg img = new CtGoodsImg();
//					for (int j = 0; j < imgList.size(); j++) {
//						if(imgList.get(j).getCoverImg()){
//							String path = systemInfo.getCtImgUrl() + "//"+imgList.get(j).getPUrl();
//							img.setPUrl(path);
//						}
//					}
//					goodsListRong.get(i).setGoodsImg(img);
//				} else {
//					goodsListRong.get(i).setGoodsImg(new CtGoodsImg(systemInfo.getCtImgUrl() + "//ct_s.gif"));
//				}
//			}
//			session.put("goodsListRong", goodsListRong);
//			
//			
//		
//			cateErjiName = helpManager.getErjiName();
//			for (Long cid : cateErjiName.keySet()) {
//				cateList = helpManager.getCateListByCid(cid);
//				bodyCareMap.put(cateErjiName.get(cid), cateList);
//			}
//			
//			
//			List<CtGoodsBrand> brands = orderManager.getGoodsBrandTopSix();
//			for (int i = 0; i < brands.size(); i++) {
//				CtBrandDTO brandDTO = new CtBrandDTO();
//				goodsList = fgoodsManager.getGoodsByBrandId(brands.get(i).getBId());
//				if(goodsList != null){
//					brandDTO.setGoodsName(goodsList);
//					brandDTO.setGoodsCate(fgoodsManager.getCateNameByCateId(goodsList.get(0).getCId()));
//				} else {
////					goodsList = new ArrayList<CtGoods>();
////					goodsList.add(null);
////					goodsList.add(null);
////					goodsList.add(null);
////					brandDTO.setGoodsName(goodsList);
////					brandDTO.setGoodsCate(null);
//				}
//				brandDTO.setBname(brands.get(i).getBName());
//				brandDTO.setBlogn(brands.get(i).getBLogo());
//				brandDTO.setBdesc(brands.get(i).getBBrief());
//				brandDTO.setBid(brands.get(i).getBId());
//				brandDTOs.add(brandDTO);
//			}
//			
//			
//			 evaList = orderManager.getEvaAll();//有问题
//			for (int i = 0; i < evaList.size(); i++) {
//				if(evaList.get(i).getEvaDesc().length() > 15){
//					evaList.get(i).setEvaDesc(evaList.get(i).getEvaDesc().substring(0, 13) + "....");
//				}
//				evaList.get(i).getCtUser().setNameTemp(evaList.get(i).getCtUser().getUUsername().substring(0, 1)+"**");
//				orderInfo = orderManager.getOrderByOrderId(evaList.get(i).getOrderId());
//				if(orderInfo != null){
//					orderGoodssList = orderManager.getOrderGoodsByOrderId(orderInfo.getOrderId());
//					if(orderGoodssList != null && orderGoodssList.size() > 0){
//						evaList.get(i).setOrderId(orderGoodssList.get(0).getGId());
//					}
//					orderInfoList.add(orderInfo);
//				}
//			}
//			
//			ServletActionContext.getRequest().getSession().setAttribute("systemInfo", systemInfo);
//			
//			
//			//加载subHade内容存放至application中
//			//getSubList();
//			return SUCCESS;
//		} catch (Exception e) {
//			// TODO: handle exception
//			e.printStackTrace();
//			return "error";
//		}
//	}
	List<CtGoods> goodsListForSubShou = new ArrayList<CtGoods>();
	private List<CtGoodsCategory> cateListSer = new ArrayList<CtGoodsCategory>();
	private List<CtGoodsCategory> cateListThree = new ArrayList<CtGoodsCategory>();
//	//获取一级分类首页
//		public String getFirstCateForShou(){
//			goodsListForSubShou = (List<CtGoods>) session.get("goodsListForSubForHadeShou");
//			//goodsListForSubShou = new ArrayList<CtGoods>();
//			if(goodsListForSubShou == null || goodsListForSubShou.size() == 0 || goodsListForSubShou.get(0).getGName()==null){
//				goodsListForSubShou = new ArrayList<CtGoods>();
//				//得到一级
//				cateList = fgoodsManager.getCateForFirst();
//				//得到二级
//				cateListSer = fgoodsManager.getCateSer(cateList);
//				//得到三级
//				cateListThree = fgoodsManager.getCateThree(cateListSer);
//				session.put("cateListThreeSetSession", cateListThree);
//				StringBuffer sb = new StringBuffer();
//				sb.append("<div class=\"mod-menu\">");
//				sb.append("<ul class=\"menu-item\">");
//				sb.append("<li class=\"rul\">");
//				sb.append("<a href=\"javascript:;\" onclick=\"getQuEncode('免费样品')\" >免费样品</a>");
//				sb.append("</li>");
//				for (int i = 0; i < cateList.size(); i++) {
//					sb.append("<li class=\"rul\">");
//					sb.append("<a href=\"javascript:;\"  >"+cateList.get(i).getCName()+"</a>");
//					sb.append("</li>");
//				}
//				sb.append("</ul>");
//				int temp = 0;
//				sb.append("<div class=\"menu-cont\" style=\"display:none;top:241px;\">");
//				for (int i = 0; i < cateList.size()+1; i++) {
//					if(i == cateList.size()){
//					} else {
//						if(temp==0){
//							String typeName = "";
//							String hrefUrl = "";
//							if(temp==0){
//								typeName = "免费样品";
//								hrefUrl = "href=\"goods_search?1=1&page=1&keyword="+typeName+"\"";
//							}
//							
//						} else {
//							
//						}
//						if(temp==0){
//							sb.append("<div class=\"menu-cont-list\" style=\"display:none;\">");
//							sb.append("<ul>");
////							for (int j = 0; j < cateListSer.size(); j++) {
////								sb.append("<li>");
////								sb.append("<span>");
////								sb.append("<a>"+cateListSer.get(j).getCName()+"</a>");
////								sb.append("</span>");
////								sb.append("<p>");
////								for (int j2 = 0; j2 < cateListThree.size(); j2++) {
////									if(cateListSer.get(j).getCId().equals(cateListThree.get(j2).getParentId())){
////										sb.append("<a href=\"javascript:;\" onclick=\"getQuEncode('"+cateListThree.get(j2).getCName()+"')\">"+cateListThree.get(j2).getCName()+"</a>");
////										sb.append("<i></i>");
////									}
////								}
////								sb.append("</p>");
////								sb.append("</li>");
////							}
//							sb.append("</ul>");
//							sb.append("</div>");
//						} else {
//							sb.append("<div class=\"menu-cont-list\" style=\"display:none;\">");
//							sb.append("<ul>");
//							for (int j = 0; j < cateListSer.size(); j++) {
//								if(cateList.get(i).getCId().equals(cateListSer.get(j).getParentId())){
//									sb.append("<li>");
//									sb.append("<span>");
//									sb.append("<a>"+cateListSer.get(j).getCName()+"</a>");
//									sb.append("</span>");
//									sb.append("<p>");
//									for (int j2 = 0; j2 < cateListThree.size(); j2++) {
//										if(cateListSer.get(j).getCId().equals(cateListThree.get(j2).getParentId())){
//											sb.append("<a href=\"javascript:;\" onclick=\"getQuEncode('"+cateListThree.get(j2).getCName()+"')\">"+cateListThree.get(j2).getCName()+"</a>");
//											sb.append("<i></i>");
//										}
//									}
//									sb.append("</p>");
//									sb.append("</li>");
//								}
//							}
//							sb.append("</ul>");
//							sb.append("</div>");
//						}
//					}
////					if(temp == 0){
////						i--;
////					}
//					temp++;
//					if(temp == (cateList.size()+3)){
//						
//					}
//				}
//				sb.append("</div>");
//				CtGoods gg = new CtGoods();
//				gg.setGName(sb.toString());
//			
//				//System.out.println(temp + cateList.size());
//				goodsListForSubShou.add(gg);
//				session.put("goodsListForSubForHadeShou", goodsListForSubShou);
//				session.put("gg",gg);
//			}
//			return SUCCESS;
//		}
		private CtTransaction transaction = new CtTransaction();
		private List<CtTransaction> transactionList = new ArrayList<CtTransaction>();
		
	public CtTransaction getTransaction() {
			return transaction;
		}

		public void setTransaction(CtTransaction transaction) {
			this.transaction = transaction;
		}

		public List<CtTransaction> getTransactionList() {
			return transactionList;
		}

		public void setTransactionList(List<CtTransaction> transactionList) {
			this.transactionList = transactionList;
		}
		List<CtHelpType> helpTypeList = new ArrayList<CtHelpType>();
		List<CtHelp> helpList = new ArrayList<CtHelp>();
		
	public List<CtHelpType> getHelpTypeList() {
			return helpTypeList;
		}

		public void setHelpTypeList(List<CtHelpType> helpTypeList) {
			this.helpTypeList = helpTypeList;
		}

		public List<CtHelp> getHelpList() {
			return helpList;
		}

		public void setHelpList(List<CtHelp> helpList) {
			this.helpList = helpList;
		}

	List<CtNotice> noticesListHang = new ArrayList<CtNotice>();
	
	public List<CtNotice> getNoticesListHang() {
		return noticesListHang;
	}

	public void setNoticesListHang(List<CtNotice> noticesListHang) {
		this.noticesListHang = noticesListHang;
	}

	private List<CtNotice> zhanghuiNotice = new ArrayList<CtNotice>();
	
	public List<CtNotice> getZhanghuiNotice() {
		return zhanghuiNotice;
	}

	public void setZhanghuiNotice(List<CtNotice> zhanghuiNotice) {
		this.zhanghuiNotice = zhanghuiNotice;
	}
	public List<CtResource> resourcesList = new ArrayList<CtResource>();

	//资源列表页
	public String resourcesInfo(){
		page = new Page();
		if (helpDTO.getPage() == 0) {
			helpDTO.setPage(1);
		}
		page.setCurrentPage(helpDTO.getPage());
		page.setPageSize(24);
		resourcesList = fgoodsManager.findResoursByPage(page);
		this.typeList = helpManager.queryType();
		this.helps = helpManager.loadAll(page);
		page.setTotalPage(page.getTotalPage());
		this.request.put("pages", page);
		return SUCCESS;
	}

	public String index() {
		//getFirstCateForShou();
		searchKeywordsList = fgoodsManager.findKeyWordOrderTop4();
        session.put("searchKeywordsList", searchKeywordsList);
		Map<String, Object>  session = ServletActionContext.getContext().getSession();
		String tahchu = (String) session.get("tanchu");
		if(tahchu == null){
			session.put("tanchu", "1");
		} else {
			session.put("tanchu", "2");
		}
		
		try {
			//根据指定公告类型查询公告 1 一般公告 2 新闻 3 行业动态
			noticesList = helpManager.findAllNotices(1);
			
			for (int i = 0; i < noticesList.size(); i++) {
				Long newDate = new Date().getTime();
				Integer threeTime = 3*24*60*60*1000;
				Long noticeTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(noticesList.get(i).getNoTime()).getTime();
				Long nowTime = newDate - noticeTime;
				if(nowTime <= threeTime){
					noticesList.get(i).setNoTimeNew("1");
				}
			}
			
			noticesListHang =  helpManager.findAllNotices(3);
			
			zhanghuiNotice =  helpManager.findAllNotices(24);
			
			resourcesList = fgoodsManager.findResouresByTopSex();
			
			for (int i = 0; i < noticesListHang.size(); i++) {
				noticesListHang.get(i).setNoTimeNew(noticesListHang.get(i).getNoTime().split(" ")[0]);
			}
			//免费样品
			goodsListZu = getDianZuForSolr("\"免费样品\"",3);
            //goodsListZu = helpManager.getDianZuGoods();

			CtSystemInfo systemInfo = new CtSystemInfo();
			
			for (int i = 0; i < goodsListZu.size(); i++) {
				this.imgList = helpManager.getImg(goodsListZu.get(i).getGId());
				if(imgList != null && imgList.size() > 0){
					CtGoodsImg img = new CtGoodsImg();
					for (int j = 0; j < imgList.size(); j++) {
						if(!imgList.get(j).getCoverImg()){
							String path = systemInfo.getCtImgUrl() + "//"+imgList.get(j).getPUrl();
							img.setPUrl(path);
						}
					}
					goodsListZu.get(i).setGoodsImg(img);
				} else {
					goodsListZu.get(i).setGoodsImg(new CtGoodsImg(systemInfo.getCtImgUrl() + "//ct_s.gif"));
				}
				CtGoodsBrand brand = fgoodsManager.findById(goodsListZu.get(i).getBId());
				goodsListZu.get(i).setGoodsBrand(brand);
			}
			if(goodsListZu.size() > 3){
				goodsListZu.remove(0);
				goodsListZu.remove(1);
			}
			
		
			cateErjiName = helpManager.getErjiName();
			for (Long cid : cateErjiName.keySet()) {
				cateList = helpManager.getCateListByCid(cid);
				bodyCareMap.put(cateErjiName.get(cid), cateList);
			}
			
			List<CtGoodsBrand> brands = orderManager.getGoodsBrandTopSix();
			for (int i = 0; i < brands.size(); i++) {
				CtBrandDTO brandDTO = new CtBrandDTO();
				//品牌推荐商品区
//				List<CtGoods> goodsListfir = fgoodsManager.getGoodsByBrandId(brands.get(i).getBId());
//				if(goodsListfir != null){
//					goodsList = new ArrayList<CtGoods>();
//					if(goodsListfir.size()>3){
//						List<CtGoods> goodsListOne = fgoodsManager.getGoodsByBrandId(brands.get(i).getBId());
//						goodsList1 = new ArrayList<CtGoods>();
//						for (int j = 3; j < goodsListOne.size(); j++) {
//							goodsList1.add(goodsListOne.get(j));
//						}
//						brandDTO.setGoodsName1(goodsList1);
//					}
//					for (int j = 0; j < 3; j++) {
//						goodsList.add(goodsListfir.get(j));
//					}
//					brandDTO.setGoodsName(goodsList);
//					brandDTO.setGoodsCate(fgoodsManager.getCateNameByCateId(goodsList.get(0).getCId()));
//				} else {
////					goodsList = new ArrayList<CtGoods>();
////					goodsList.add(null);
////					goodsList.add(null);
////					goodsList.add(null);
////					brandDTO.setGoodsName(goodsList);
////					brandDTO.setGoodsCate(null);
//				}
				brandDTO.setBname(brands.get(i).getBName());
				brandDTO.setBlogn(brands.get(i).getBLogo());
				brandDTO.setBdesc(brands.get(i).getBBrief());
				brandDTO.setBid(brands.get(i).getBId());
				brandDTOs.add(brandDTO);
				if(i == 11){
					brandDTOList.add(brandDTOs);
					brandDTOs = new ArrayList<CtBrandDTO>();
				}
				if(i == 23){
					brandDTOList.add(brandDTOs);
				}
			}
			
//			 evaList = orderManager.getEvaAll();//有问题
//			for (int i = 0; i < evaList.size(); i++) {
//				if(evaList.get(i).getEvaDesc().length() > 15){
//					evaList.get(i).setEvaDesc(evaList.get(i).getEvaDesc().substring(0, 13) + "....");
//				}
//				evaList.get(i).getCtUser().setNameTemp(evaList.get(i).getCtUser().getUUsername().substring(0, 1)+"**");
//				orderInfo = orderManager.getOrderByOrderId(evaList.get(i).getOrderId());
//				if(orderInfo != null){
//					orderGoodssList = orderManager.getOrderGoodsByOrderId(orderInfo.getOrderId());
//					if(orderGoodssList != null && orderGoodssList.size() > 0){
//						evaList.get(i).setOrderId(orderGoodssList.get(0).getGId());
//					}
//					orderInfoList.add(orderInfo);
//				}
//			}
			session.put("systemInfo", systemInfo);
			transactionList = orderManager.findTranByTop30();
			int count1 = orderManager.findTramByCountList(1);
			int count3 = orderManager.findTramByCountList(3);
			int count30 = orderManager.findTramByCountList(30);
			System.out.println(count1);
			System.out.println(count3);
			System.out.println(count30);
			this.request.put("count1", count1);
			this.request.put("count3", count3);
			this.request.put("count30", count30);
			
			
			ActionContext context = ActionContext.getContext();
			Map<String,Object> application = context.getApplication();
			cateList = (List<CtGoodsCategory>) application.get("cateListApplication");
			if(cateList == null || cateList.size() == 0){
				//得到一级
				cateList = fgoodsManager.getCateForFirst();
				Solr ts = new Solr();
				String field[] = new String[1];
				String[] key = new String[1];
				field[0]="name";
				String[] sortfield={"id","name"};
				Boolean[] flag={true,true};//flag 需要排序的字段的排序方式如果为true 升序 如果为false 降序
				Boolean hightlight=true;//hightlight 是否需要高亮显示
				for (int i = 0; i < cateList.size(); i++) {
					key[0] = "\""+cateList.get(i).getCName()+"\"";
					QueryResponse qr = ts.Search(field, key, 1, 5, sortfield, flag, hightlight,null);
					SolrDocumentList slist=qr.getResults();//获取按条件查询筛选后的结果（ solr自带的方法）
					Long numfound = slist.getNumFound();//结果条数
					cateList.get(i).setCateGoodsNum(numfound);
				}
				//得到二级
				cateListSer = fgoodsManager.getCateSer(cateList);
				for (int i = 0; i < cateListSer.size(); i++) {
					key[0] = "\""+cateListSer.get(i).getCName()+"\"";
					QueryResponse qr = ts.Search(field, key, 1, 5, sortfield, flag, hightlight,null);
					SolrDocumentList slist=qr.getResults();//获取按条件查询筛选后的结果（ solr自带的方法）
					Long numfound = slist.getNumFound();//结果条数
					cateListSer.get(i).setCateGoodsNum(numfound);
				}
				
				//得到三级
				//cateListThree = fgoodsManager.getCateThree(cateListSer);
				application.put("cateListApplication", cateList);
				application.put("cateListSerApplication", cateListSer);
				//application.put("cateListThreeApplication", cateListThree);
			}
			
//			CtUser user = (CtUser) session.get("userinfosession");
//			if(user != null){
//				String totalAll = orderManager.getOrderTotalByTime("2016-11-06", user);
//				System.out.println(totalAll);
//				DecimalFormat df = new DecimalFormat("0.00");
//				String newt = df.format(Double.valueOf(totalAll == null ? "0" : totalAll));
//				request.put("shuangshiyit", newt);
//				System.out.println(newt);          
//			}
			//加载subHade内容存放至application中
			//getSubList();
			//加载首页轮播图
			ctAdsList = fgoodsManager.findAdByIsUpAndType(1, "首页轮播图");
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}
	
	private CtAd ad = new CtAd();
	private List<CtAd> ctAdsList = new ArrayList<CtAd>();
	
	
	public List<CtAd> getCtAdsList() {
		return ctAdsList;
	}

	public void setCtAdsList(List<CtAd> ctAdsList) {
		this.ctAdsList = ctAdsList;
	}

	public CtAd getAd() {
		return ad;
	}

	public void setAd(CtAd ad) {
		this.ad = ad;
	}

	public List<CtGoods> getGoodsListForSubShou() {
		return goodsListForSubShou;
	}

	public void setGoodsListForSubShou(List<CtGoods> goodsListForSubShou) {
		this.goodsListForSubShou = goodsListForSubShou;
	}

	public List<CtGoodsCategory> getCateListSer() {
		return cateListSer;
	}

	public void setCateListSer(List<CtGoodsCategory> cateListSer) {
		this.cateListSer = cateListSer;
	}

	public List<CtGoodsCategory> getCateListThree() {
		return cateListThree;
	}

	public void setCateListThree(List<CtGoodsCategory> cateListThree) {
		this.cateListThree = cateListThree;
	}

	private List<CtGoods> getDianZuForSolr(String typeName, int size) {
		List<CtGoods> list = new ArrayList<CtGoods>();
		Solr ts = new Solr();
		String[] field = {"C_NAME"};
		String[] key = {typeName};
		String[] sortfield={"id","name"};
		if(typeName.equals("\"免费样品\"")){
			field = new String[2];
			key = new String[2];
			field[0] = "IS_PARTIAL";
			field[1] = "IS_SAMPLE";
			key[0] = "0";
			key[1] = "1";
		}
		Boolean[] flag={true,true};//flag 需要排序的字段的排序方式如果为true 升序 如果为false 降序
		Boolean hightlight=true;//hightlight 是否需要高亮显示
		try{
			QueryResponse qr = ts.Search(field, key, 1, size, sortfield, flag, hightlight,null);
			SolrDocumentList slist=qr.getResults();//获取按条件查询筛选后的结果（ solr自带的方法）
			
			for (SolrDocument solrDocument : slist) {//转换为对象
                CtGoods ctGoods = new CtGoods();
                ctGoods.setGId(Long.valueOf(solrDocument.getFirstValue("id").toString()));
                ctGoods.setGName(solrDocument.getFirstValue("U_NAME").toString());
                ctGoods.setGSn(solrDocument.getFirstValue("G_SN").toString());
//                ctGoods.setShopPrice(Double.valueOf(solrDocument.getFirstValue("SHOP_PRICE").toString()));
                ctGoods.setGUnit(solrDocument.getFirstValue("G_UNIT").toString());
                ctGoods.setPack4(solrDocument.getFirstValue("B_NAME").toString());
                ctGoods.setUName(solrDocument.getFirstValue("C_NAME").toString());
                ctGoods.setBId(Long.valueOf(solrDocument.getFirstValue("B_ID").toString()));
				list.add(ctGoods);
		     }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	private List<CtOrderInfo> orderInfoList = new ArrayList<CtOrderInfo>();
	private List<CtOrderGoods> orderGoodssList = new ArrayList<CtOrderGoods>();
	
	public List<CtOrderGoods> getOrderGoodssList() {
		return orderGoodssList;
	}

	public void setOrderGoodssList(List<CtOrderGoods> orderGoodssList) {
		this.orderGoodssList = orderGoodssList;
	}
	private CtOrderInfo orderInfo = new CtOrderInfo();
	
	public CtOrderInfo getOrderInfo() {
		return orderInfo;
	}

	public void setOrderInfo(CtOrderInfo orderInfo) {
		this.orderInfo = orderInfo;
	}

	public List<CtOrderInfo> getOrderInfoList() {
		return orderInfoList;
	}

	public void setOrderInfoList(List<CtOrderInfo> orderInfoList) {
		this.orderInfoList = orderInfoList;
	}

	
	
	public String findrank() {
		try {
			Page page = new Page();
			if (helpDTO.getPage() == 0) {
				helpDTO.setPage(1);
			}
			String name="会员等级";
			CtHelp m = new CtHelp();
			m.setHTitle(name);
			this.help=helpManager.findByTitle(name);
			helpDTO.setHId(help.getHId());
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}

	public List<CtGoods> getGoodsList1() {
		return goodsList1;
	}

	public void setGoodsList1(List<CtGoods> goodsList1) {
		this.goodsList1 = goodsList1;
	}
	
	
}

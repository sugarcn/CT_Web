package org.ctonline.action.pay;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.util.EntityUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.ctonline.config.AlipayConfig;
import org.ctonline.dto.order.OrderDTO;
import org.ctonline.dto.pay.CtPayDTO;
import org.ctonline.dto.pay.WxPayDto;
import org.ctonline.manager.basic.CtCouponDetailManager;
import org.ctonline.manager.basic.CtRegionManager;
import org.ctonline.manager.order.OrderManager;
import org.ctonline.manager.pay.CtPayManager;
import org.ctonline.po.basic.CtCashCoupon;
import org.ctonline.po.basic.CtPaymentWx;
import org.ctonline.po.basic.CtSystemInfo;
import org.ctonline.po.order.CtOrderGoods;
import org.ctonline.po.order.CtOrderInfo;
import org.ctonline.po.pay.CtPayInterface;
import org.ctonline.po.user.CtUser;
import org.ctonline.service.EJLInfoServiceImplDelegate;
import org.ctonline.service.EJLInfoServiceImplService;
import org.ctonline.service.JdpCtegoTrade;
import org.ctonline.tenpay.RequestHandler;
import org.ctonline.tenpay.ResponseHandler;
import org.ctonline.tenpay.client.ClientResponseHandler;
import org.ctonline.tenpay.client.TenpayHttpClient;
import org.ctonline.tenpay.util.TenpayUtil;
import org.ctonline.test.RanInit;
import org.ctonline.test.serviceInterface;
import org.ctonline.util.AlipayNotify;
import org.ctonline.util.AlipaySubmit;
import org.ctonline.util.Config;
import org.ctonline.util.FormatJson;
import org.ctonline.util.GetWxOrderno;
import org.ctonline.util.JsonOrderInfo;
import org.ctonline.util.PayCommonUtil;
import org.ctonline.util.Util;
import org.ctonline.util.UtilDate;
import org.dom4j.DocumentException;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.xml.sax.InputSource;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class CtPayAction extends ActionSupport implements RequestAware, ModelDriven<Object>,SessionAware{
	private static final String KEY_CHINAPAY_MERKEY_FILEPATH = "chinapay.merkey.filepath";
	private CtPayManager payManager;
	private CtPayInterface payInterface;
	private OrderManager orderManager;
	private CtRegionManager regionManager;
	private CtOrderInfo orinfo;

	private CtCouponDetailManager coupondetailManager;
	

	public CtCouponDetailManager getCoupondetailManager() {
		return coupondetailManager;
	}

	public void setCoupondetailManager(CtCouponDetailManager coupondetailManager) {
		this.coupondetailManager = coupondetailManager;
	}

	public OrderManager getOrderManager() {
		return orderManager;
	}

	public CtOrderInfo getOrinfo() {
		return orinfo;
	}

	public CtRegionManager getRegionManager() {
		return regionManager;
	}

	public void setRegionManager(CtRegionManager regionManager) {
		this.regionManager = regionManager;
	}

	public void setOrinfo(CtOrderInfo orinfo) {
		this.orinfo = orinfo;
	}

	public void setOrderManager(OrderManager orderManager) {
		this.orderManager = orderManager;
	}

	public CtPayInterface getPayInterface() {
		return payInterface;
	}

	public void setPayInterface(CtPayInterface payInterface) {
		this.payInterface = payInterface;
	}



	private String sHtmlText;
	private Map<String, Object> request;
	private Map<String, Object> session;
	private CtPayDTO payDTO = new CtPayDTO();

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.payDTO;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}


	public Map<String, Object> getRequest() {
		return request;
	}




	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}
	public CtPayManager getPayManager() {
		return payManager;
	}

	public void setPayManager(CtPayManager payManager) {
		this.payManager = payManager;
	}

	public String getsHtmlText() {
		return sHtmlText;
	}

	public void setsHtmlText(String sHtmlText) {
		this.sHtmlText = sHtmlText;
	}


	
	public String pay_ali() throws Exception{
		return aliPayUrl(payDTO,0);
		

	}

	private String aliPayUrl(CtPayDTO payDTO, int type) {
		try{
			
			//支付类型
			String payment_type = "1";
			//必填，不能修改
			//服务器异步通知页面路径
			//String notify_url = "http://127.0.0.1:8080/CT_Web/alinotify";
			//需http://格式的完整路径，不能加?id=123这类自定义参数

			CtSystemInfo sysinfo = new CtSystemInfo();
			
			
			String notify_url =AlipayConfig.notify_url;
			String return_url = AlipayConfig.return_url;
			//判断是否还款支付
			if(type == 1){
//				public static String notify_url = "http://127.0.0.1/CT_Web/alinotify";
//				public static String return_url = "http://127.0.0.1/CT_Web/alireturn";
//				notify_url = "http://127.0.0.1/CT_Web/alinotify_Credit";
//				return_url = "http://127.0.0.1/CT_Web/alireturn_Credit";
				notify_url = "http://www.ctego.com/alinotify_Credit";
				return_url = "http://www.ctego.com/alireturn_Credit";
			}
					
			
			//页面跳转同步通知页面路径
			//String return_url = "http://127.0.0.1:8080/CT_Web/alireturn";
			//需http://格式的完整路径，不能加?id=123这类自定义参数，不能写成http://localhost/

			//商户订单号
			String out_trade_no = new String(payDTO.getOut_trade_no().getBytes("ISO-8859-1"),"UTF-8");
			//商户网站订单系统中唯一订单号，必填

			//订单名称
			String subject = payDTO.getSubject();
			//必填

			//付款金额
			String total_fee = new String(payDTO.getTotal_fee().getBytes("ISO-8859-1"),"UTF-8");
			//必填

			//订单描述

			String body = payDTO.getOrderbody();
			//商品展示地址
			String show_url = new String(payDTO.getGoods_address().getBytes("ISO-8859-1"),"UTF-8");
			//需以http://开头的完整路径，例如：http://www.商户网址.com/myorder.html

			//防钓鱼时间戳
			String anti_phishing_key = UtilDate.getDateFormatter();
			//若要使用请调用类文件submit中的query_timestamp函数

			//客户端的IP地址
			HttpServletRequest req = ServletActionContext.getRequest();
			String exter_invoke_ip =UtilDate.getIpAddr(req);
			//非局域网的外网IP地址，如：221.0.0.1
			
			//会员id     mem_id
			String mem_id = new String(payDTO.getMem_id().getBytes("ISO-8859-1"),"UTF-8");
			
			
			
			//////////////////////////////////////////////////////////////////////////////////
			
			//把请求参数打包成数组
			Map<String, String> sParaTemp = new HashMap<String, String>();
			sParaTemp.put("service", "create_direct_pay_by_user");
	        sParaTemp.put("partner", AlipayConfig.partner);
	        sParaTemp.put("seller_email", AlipayConfig.seller_email);
	        sParaTemp.put("_input_charset", AlipayConfig.input_charset);
			sParaTemp.put("payment_type", payment_type);
			sParaTemp.put("notify_url", notify_url);
			sParaTemp.put("return_url", return_url);
			sParaTemp.put("out_trade_no", out_trade_no);
			sParaTemp.put("subject", subject);
			sParaTemp.put("total_fee", total_fee);
			sParaTemp.put("body", body);
			sParaTemp.put("show_url", show_url);
			sParaTemp.put("anti_phishing_key", anti_phishing_key);
			sParaTemp.put("exter_invoke_ip", exter_invoke_ip);
			sParaTemp.put("mem_id", mem_id);
			
			String tempa = AlipaySubmit.buildRequest(sParaTemp,"get","确认");
			//建立请求
			tempa = tempa.substring(0, tempa.indexOf("<script>"));
			this.request.put("sHtmlText", tempa);
			result += tempa;
			//System.out.println(tempa);
			return SUCCESS;			
			
		}catch(Exception e){
			
			return "error";
		}
	}
	
	
	private String chinaPayUrl(CtPayDTO payDTO) {
		try{
			
			String MerId, OrdId, TransAmt, CuryId, TransDate, TransType,ChkValue,Version,BgRetUrl,PageRetUrl,GateId,Priv1;
			String plainData, ChkValue2;
			String MerKeyPath = null;
			/* …… 给以上变量分别赋值 */
			ChkValue="";
			chinapay.PrivateKey key=new chinapay.PrivateKey();
			chinapay.SecureLink t;
			MerId = AlipayConfig.MerId;//商户号
			Properties config = Config.getInstance().getProperties();
			MerKeyPath = config.getProperty(KEY_CHINAPAY_MERKEY_FILEPATH);
			
			SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");//设置日期格式

		
			SimpleDateFormat df1 = new SimpleDateFormat("yyyyMMddHHmmssSSS");//设置日期格式
			//CT4591196230
			//String aa = payDTO.getOut_trade_no();
			//OrdId = df1.format(new Date()).substring(0,16);//原始支付订单号，16位长度
						OrdId = "10"+payDTO.getOut_trade_no().substring(2,12)+"0000";
			
			NumberFormat nf = NumberFormat.getNumberInstance();
	        nf.setMaximumFractionDigits(2);
	        //System.out.println(nf.format(Double.valueOf(payDTO.getTotal_fee())));//你要保留两位小数的数
	        		
			DecimalFormat b = new DecimalFormat("#.00");
			//System.out.println(b.format(Double.valueOf(payDTO.getTotal_fee())).toString());
			
			TransAmt = this.addZero(12, b.format(Double.valueOf(payDTO.getTotal_fee())).toString());//订单交易金额，12位长度，左补0,精确到分
			
			CuryId = "156";//交易币种，固定为156
			TransDate = df.format(new Date());//订单交易日期，8位长度，必填
			TransType = "0001";//交易类型,4位长度，必填
			Version = "20141120";//版本号
			BgRetUrl =AlipayConfig.BgRetUrl;//后台接收Url
			PageRetUrl = AlipayConfig.PageRetUrl;//页面接收Url
			GateId = "";//支付网关号
			Priv1 = "Memo";//商户私有域
			
			
			
			boolean flag;
			
			
			flag=key.buildKey(MerId,0,MerKeyPath);
			if (flag==false) 
			{
				//System.out.println("build key error!");
				return "error";
			}
			
			t=new chinapay.SecureLink (key);


			ChkValue = t.Sign(MerId +  OrdId + TransAmt + CuryId
									+ TransDate + TransType + Version + BgRetUrl + PageRetUrl + GateId + Priv1);
			
			
			StringBuffer tempa = new StringBuffer();
			tempa.append("<form name='chinapay' action='"+AlipayConfig.postUrl+"' METHOD=POST>");
			tempa.append("<input type=hidden name='MerId' value='"+MerId+"' >");
			tempa.append("<input type=hidden name='OrdId' value='"+OrdId+"' >");
			tempa.append("<input type=hidden name='TransAmt' value='"+TransAmt+"'>");
			tempa.append("<input type=hidden name='CuryId' value='"+CuryId+"'>");
			tempa.append("<input type=hidden name='TransDate' value='"+TransDate+"'>");
			tempa.append("<input type=hidden name='TransType' value='"+TransType+"' >");
			tempa.append("<input type=hidden name='Version' value='"+Version+"'>");
			tempa.append("<input type=hidden name='BgRetUrl' value='"+BgRetUrl+"'>");
			tempa.append("<input type=hidden name='PageRetUrl' value='"+PageRetUrl+"'>");
			tempa.append("<input type=hidden name='GateId' value='"+GateId+"'>");
			tempa.append("<input type=hidden name='Priv1' value='"+Priv1+"'>");
			tempa.append("<input type=hidden name='ChkValue' value='"+ChkValue+"'>");
			tempa.append("</form>");
			
			this.session.put("chinaPayHtmlText", tempa);
			//System.out.println(tempa);
			return SUCCESS;			
			
		}catch(Exception e){
			
			return "error";
		}
	}
	

	public CtPayDTO getPayDTO() {
		return payDTO;
	}

	public void setPayDTO(CtPayDTO payDTO) {
		this.payDTO = payDTO;
	}
	
	
	

	public static void insertEJLOrder(CtOrderInfo orderInfo, CtUser user, CtPayManager payManager) {
		
		orderInfo.setOrderStatus("1");
		EJLInfoServiceImplDelegate delegate = new EJLInfoServiceImplService().getEJLInfoServiceImplPort();
		//List<JdpCtegoTrade> list = delegate.getAllTrade();
		JdpCtegoTrade trade = new JdpCtegoTrade();
		trade = new JdpCtegoTrade();
		trade.setOrderId(orderInfo.getOrderId());
		trade.setStatus(orderInfo.getOrderStatus());
		trade.setSellerNick("长亭易购");
		trade.setType("fixed");
		trade.setUserId(Long.valueOf(orderInfo.getUId()));
		trade.setBuyerNick(user.getUUsername());
		trade.setCreated(orderInfo.getOrderTime());
		trade.setModified(orderInfo.getOrderTime());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = sdf.format(new Date());
		trade.setJdpCreated(date);
		trade.setJdpModified(date);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
		String payTime = payManager.findPayByOrderSn(orderInfo.getOrderSn());
		if(orderInfo.getPay().equals("1")){
			payTime = payManager.findXianXiaPayTimeByOrderSn(orderInfo.getOrderId());
		}
		JsonOrderInfo info = FormatJson.formatJsonOrder(orderInfo,payTime);
		
		JSONArray jsonArray = JSONArray.fromObject(info);
		String jsonStr = jsonArray.toString().substring(1, jsonArray.toString().length()-1);
		jsonStr = "{\"trade_fullinfo_get_response\": { \"trade\": " + jsonStr + " } }";
		trade.setJdpResponse(jsonStr);
		
		String mess = delegate.jdpInsertInfo(trade);
		System.out.println(mess);
		
		//logger.info("成功新增一条记录+++++++++++++++++++++++++++");
		//System.out.println(jsonArray);
		
	}
	
	public String ret_ali() throws Exception{
		try{
			//获取支付宝GET过来反馈信息
			Map<String,String> params = new HashMap<String,String>();
			HttpServletRequest req = ServletActionContext.getRequest();
			Map requestParams = req.getParameterMap();
			
			//Map requestParams = getRequest();
			for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
				String name = (String) iter.next();
				String[] values = (String[]) requestParams.get(name);
				String valueStr = "";
				for (int i = 0; i < values.length; i++) {
					valueStr = (i == values.length - 1) ? valueStr + values[i]
							: valueStr + values[i] + ",";
				}
				//乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
				//valueStr = new String(valueStr.getBytes("ISO-8859-1"), "UTF-8");
				params.put(name, valueStr);
			}
			
			//获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以下仅供参考)//
			//商户订单号

			String out_trade_no = new String(payDTO.getOut_trade_no().getBytes("ISO-8859-1"),"UTF-8");

			//支付宝交易号

			String trade_no = new String(payDTO.getTrade_no().getBytes("ISO-8859-1"),"UTF-8");

			//交易状态
			String trade_status = new String(payDTO.getTrade_status().getBytes("ISO-8859-1"),"UTF-8");

			//获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以上仅供参考)//
			String total_fee = new String(payDTO.getTotal_fee().getBytes("ISO-8859-1"),"UTF-8");
			//计算得出通知验证结果
			//boolean verify_result = AlipayNotify.verify(params);
			
			if(true){//验证成功
				//////////////////////////////////////////////////////////////////////////////////////////
				//请在这里加上商户的业务逻辑程序代码
				

				//——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
				if(trade_status.equals("TRADE_FINISHED") || trade_status.equals("TRADE_SUCCESS")){
					//判断该笔订单是否在商户网站中已经做过处理
					//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
					//如果有做过处理，不执行商户的业务程序
					orinfo = orderManager.getOrderStatusByOrderSn(out_trade_no);
						if (orinfo!=null){
							String sta = orinfo.getOrderStatus();
							Long oid = orinfo.getOrderId();
							String total = orinfo.getTotal();
							
							//判断是否使用现金劵
							String couponStr = orinfo.getCouponId();
							CtCashCoupon cash = new CtCashCoupon();
							if(couponStr != null){
								String chai[] = couponStr.split(",");
								for (int i = 0; i < chai.length; i++) {
									cash = orderManager.getCashInfoByCashId(Integer.valueOf(chai[i]));
									if(cash != null){
										cash.setUseTime(new SimpleDateFormat("yyyy-MM-dd HH:dd:ss").format(new Date()));
										orderManager.updateCashCouponById(cash);
										Double totalStr = sub(total, cash.getDiscount().toString());
										total = totalStr.toString();
										orinfo.setTotal(total);
										break;
									}
								}
							}
							
							Double to = Double.valueOf(total);
							Double to_fee = Double.valueOf(total_fee);
							if (to.toString().equals(to_fee.toString())){
								
								//更新payInterface
								payInterface = new CtPayInterface();
								payInterface.setOrderSn(out_trade_no);
								payInterface.setTradeNo(trade_no);
								payInterface.setTradeStatus(trade_status);
								payInterface.setTotalFee(total_fee);
								payInterface.setTradeType("1");
								payInterface.setPayDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
								
								if(payManager.findPayByOrderSn(out_trade_no) == null){
									payManager.save(payInterface);
								}
								
								int credit = 0;
								//判断是否存在小数，存在就去掉小数，按整数计算积分
								if(to.toString().indexOf(".") > 0){
									String[] chai = to.toString().split("\\.");
									credit = Integer.valueOf(chai[0]);
								} else {
									credit = Integer.valueOf(to.toString());
								}
								//获取当前用的积分
								CtUser cu = (CtUser) this.session.get("userinfosession");
								//双十一满足指定金额发放优惠券
//								coupondetailManager.udpateCouponDetails(121, cu.getUId(), 5);
//								coupondetailManager.udpateCouponDetails(122, cu.getUId(), 5);
//								coupondetailManager.udpateCouponDetails(123, cu.getUId(), 5);
//								coupondetailManager.udpateCouponDetails(124, cu.getUId(), 5);
								
								//发放活动优惠券
								activityCoupon(credit, cu);
								
								if(cu.getUCredit() == null){
									Integer cre = 0;
									credit += cre;
								} else {
									Integer cre = cu.getUCredit();
									credit += cre;
								}
								cu.setUCredit(credit);
								orderManager.updateUserCredit(cu);
								orderManager.upChangeStatus(oid, "1");
								this.request.put("orderInfo", orinfo);
								orinfo = orderManager.getOrderStatusByOrderSn(out_trade_no);
								orinfo.setPay("2");
								orderManager.saveOrderInfo(orinfo);
								
								
								insertEJLOrder(orinfo, cu, payManager);
								
								//发送下单成功短信
								if(cu.getUMb() != null){
									s.paySuccessInfoMess(cu.getUMb(), orinfo.getOrderSn());
								}
								//向EDB推送数据
								/*
								ArrayList al = new ArrayList();
								al.add(0, orinfo.getOrderSn());//外部平台编号
								al.add(1,orinfo.getUId());//买家ID
								
								al.add(1,orinfo.getDsc());//买家留言
								al.add(1,orinfo.getOrderTime());//付款日期
								al.add(1,orinfo.getPay());//支付方式
								al.add(1,orinfo.getConsignee());//收货人
								al.add(1,orinfo.getProvince());//省
								al.add(1,orinfo.getCity());//市
								al.add(1,orinfo.getDistrict());//区
								al.add(1,orinfo.getTotal());//产品总金额
								al.add(1,orinfo.getTotal());//订单总金额
								al.add(1,orinfo.getTotal());//实收金额
								al.add(1,orinfo.getFreight());//实收运费
								al.add(1,orinfo.getDistrict());//电话
								al.add(1,orinfo.getTel());//手机
								al.add(1,orinfo.getOrderTime());//订货日期
								al.add(1,"");//是否货到付款
								*/
								
//EDB
//								List<CtOrderGoods> orderGoodslist = orderManager.getOrderGoodsByOrderId(orinfo.getOrderId());
								
//								this.edbTradeAdd(orinfo, orderGoodslist);
//								
								
								//EDB
								List<CtOrderGoods> orderGoodslist = orderManager.getOrderGoodsByOrderId(orinfo.getOrderId());
								edbTradeAdd(orinfo, orderGoodslist,regionManager,orderManager);
								
								
							}else{
								session.put("errorMess", "异常提交，订单金额为__"+to_fee+"___"+to);
								session.put("errorTotle", "异常提交14");
								return "error";
							}
					
						}else{
							session.put("errorMess", "异常提交,订单信息为空"+out_trade_no);
							session.put("errorTotle", "异常提交13");
							return "error";
						}
					//该页面可做页面美工编辑
					return SUCCESS;
						//return 
				}else{
					//trade_status.equals("TRADE_FINISHED") || trade_status.equals("TRADE_SUCCESS")
					session.put("errorMess", "异常提交,支付宝状态异常"+trade_status);
					session.put("errorTotle", "异常提交12");
					//该页面可做页面美工编辑
					return "error";
				}
//					
			}else{
				session.put("errorMess", "异常提交312");
				session.put("errorTotle", "异常提交123");
				//该页面可做页面美工编辑
				return "error";
			}
		}catch(Exception e){
			session.put("errorMess", "异常提交，异常信息"+e.getMessage());
			session.put("errorTotle", "异常提交11");
			return "error";
		}
	}
	//http://127.0.0.1/CT_Web/alireturn_Credit?buyer_email=18566785567&buyer_id=2088702635143881&exterface=create_direct_pay_by_user&is_success=T&notify_id=RqPnCoPT3K9%252Fvwbh3InZdaZzlhywkFHJ3g%252BIrRBlXIyJFblyHIIQl2q3rgKepNnrh4AS&notify_time=2017-02-13+17%3A48%3A26&notify_type=trade_status_sync&out_trade_no=CT201702135833769656&payment_type=1&seller_email=s%40ctego.com&seller_id=2088221825235989&subject=%E9%95%BF%E4%BA%AD%E6%98%93%E8%B4%AD-CT201702135833769656&total_fee=0.01&trade_no=2017021321001004880214312935&trade_status=TRADE_SUCCESS&sign=11466989566d213b06ac239cca546e37&sign_type=MD5
	//信用支付支付宝支付返回
	public String ret_ali_cre() throws Exception{
		try{
			//获取支付宝GET过来反馈信息
			Map<String,String> params = new HashMap<String,String>();
			HttpServletRequest req = ServletActionContext.getRequest();
			Map requestParams = req.getParameterMap();
			
			//Map requestParams = getRequest();
			for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
				String name = (String) iter.next();
				String[] values = (String[]) requestParams.get(name);
				String valueStr = "";
				for (int i = 0; i < values.length; i++) {
					valueStr = (i == values.length - 1) ? valueStr + values[i]
							: valueStr + values[i] + ",";
				}
				//乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
				//valueStr = new String(valueStr.getBytes("ISO-8859-1"), "UTF-8");
				params.put(name, valueStr);
			}
			
			//获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以下仅供参考)//
			//商户订单号

			String out_trade_no = new String(payDTO.getOut_trade_no().getBytes("ISO-8859-1"),"UTF-8");

			//支付宝交易号

			String trade_no = new String(payDTO.getTrade_no().getBytes("ISO-8859-1"),"UTF-8");

			//交易状态
			String trade_status = new String(payDTO.getTrade_status().getBytes("ISO-8859-1"),"UTF-8");

			//获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以上仅供参考)//
			String total_fee = new String(payDTO.getTotal_fee().getBytes("ISO-8859-1"),"UTF-8");
			//计算得出通知验证结果
			//boolean verify_result = AlipayNotify.verify(params);
			
			if(true){//验证成功
				//////////////////////////////////////////////////////////////////////////////////////////
				//请在这里加上商户的业务逻辑程序代码
				

				//——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
				if(trade_status.equals("TRADE_FINISHED") || trade_status.equals("TRADE_SUCCESS")){
					//判断该笔订单是否在商户网站中已经做过处理
					//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
					//如果有做过处理，不执行商户的业务程序
					orinfo = orderManager.getOrderStatusByOrderSn(out_trade_no);
					List<CtOrderInfo> listOrderInfo = new ArrayList<CtOrderInfo>();
					Double d = 0D;
					if(orinfo == null){
						String data = (String) session.get("oneKeyOrderSn");
						if(data != null){
							String orderSns[] = data.split(",");
							for (int i = 0; i < orderSns.length; i++) {
								CtOrderInfo info = orderManager.getOrderStatusByOrderSn(orderSns[i]);
								listOrderInfo.add(info);
								d = add(d.toString(), info.getTotal());
							}
						} else {
							session.put("errorMess", "订单信息为空"+listOrderInfo.size()+"_"+data);
							session.put("errorTotle", "异常提交13");
							return "error";
						}
					} else {
						listOrderInfo.add(orinfo);
					}
					for (int i = 0; i < listOrderInfo.size(); i++) {
						orinfo = listOrderInfo.get(i);
						if (orinfo!=null){
							String sta = orinfo.getOrderStatus();
							Long oid = orinfo.getOrderId();
							String total = d.toString();
							
							Double to = Double.valueOf(total);
							Double to_fee = Double.valueOf(total_fee);
							if (to.toString().equals(to_fee.toString())){
								payInterface = payManager.findPayInfoByOrderSn(orinfo.getOrderSn());
								//更新payInterface
								CtOrderInfo info = orderManager.getOrderInfoByOrderSn(orinfo.getOrderSn());
								payInterface.setTradeNo(trade_no);
								payInterface.setTradeStatus(trade_status);
								payInterface.setTotalFee(info.getTotal());
								payInterface.setTradeType("1");
								payInterface.setPayDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
								
								
								payManager.save(payInterface);
								
								int credit = 0;
								//判断是否存在小数，存在就去掉小数，按整数计算积分
								if(to.toString().indexOf(".") > 0){
									String[] chai = to.toString().split("\\.");
									credit = Integer.valueOf(chai[0]);
								} else {
									credit = Integer.valueOf(to.toString());
								}
								//获取当前用的积分
								//获取当前用的积分
								CtUser cuSess = (CtUser) this.session.get("userinfosession");
								CtUser cu = orderManager.getUserByIUid(cuSess.getUId());
								//双十一满足指定金额发放优惠券
//								coupondetailManager.udpateCouponDetails(121, cu.getUId(), 5);
//								coupondetailManager.udpateCouponDetails(122, cu.getUId(), 5);
//								coupondetailManager.udpateCouponDetails(123, cu.getUId(), 5);
//								coupondetailManager.udpateCouponDetails(124, cu.getUId(), 5);
								
								//发放活动优惠券
								//activityCoupon(credit, cu);
								
								if(cu.getUCredit() == null){
									Integer cre = 0;
									credit += cre;
								} else {
									Integer cre = cu.getUCredit();
									credit += cre;
								}
								cu.setUCredit(credit);
								cu.setURemainingAmount(add(cu.getURemainingAmount().toString(), to_fee.toString()));
								orderManager.updateUser(cu);
								orderManager.upChangeStatus(oid, "1");
								this.request.put("orderInfo", orinfo);
								orinfo = orderManager.getOrderStatusByOrderSn(orinfo.getOrderSn());
								orinfo.setPay("6");
								orderManager.saveOrderInfo(orinfo);
								this.session.put("userinfosession",cu);
//								insertEJLOrder(orinfo, cu, payManager);
//								
//								//发送下单成功短信
//								if(cu.getUMb() != null){
//									s.paySuccessInfoMess(cu.getUMb(), orinfo.getOrderSn());
//								}
//								
								
							}else{
								session.put("errorMess", "异常提交，订单金额为__"+to_fee+"___"+to);
								session.put("errorTotle", "异常提交14");
								return "error";
							}
					
						}else{
							session.put("errorMess", "异常提交,订单信息为空"+out_trade_no);
							session.put("errorTotle", "异常提交13");
							return "error";
						}
					}
					//该页面可做页面美工编辑
					return SUCCESS;
						//return 
				}else{
					//trade_status.equals("TRADE_FINISHED") || trade_status.equals("TRADE_SUCCESS")
					session.put("errorMess", "异常提交,支付宝状态异常"+trade_status);
					session.put("errorTotle", "异常提交12");
					//该页面可做页面美工编辑
					return "error";
				}
//					
			}else{
				session.put("errorMess", "异常提交312");
				session.put("errorTotle", "异常提交123");
				//该页面可做页面美工编辑
				return "error";
			}
		}catch(Exception e){
			session.put("errorMess", "异常提交，异常信息"+e.getMessage());
			session.put("errorTotle", "异常提交11");
			return "error";
		}
		
	}
	private serviceInterface s = new serviceInterface();
	
	public serviceInterface getS() {
		return s;
	}

	public void setS(serviceInterface s) {
		this.s = s;
	}

	private void activityCoupon(int credit, CtUser cu) {
		if(new SimpleDateFormat("yyyy-MM-dd").format(new Date()).equals("2016-11-11")){
			if(credit >= 100 && credit < 200){
				coupondetailManager.udpateCouponDetails(121, cu.getUId(), 1);
			}
			if(credit >= 200 && credit < 500){
				coupondetailManager.udpateCouponDetails(122, cu.getUId(), 1);
			}
			if(credit >= 500 && credit < 1000){
				coupondetailManager.udpateCouponDetails(123, cu.getUId(), 1);
			}
			if(credit >= 1000){
				coupondetailManager.udpateCouponDetails(124, cu.getUId(), 1);
			}
		}
	}
	
	
	public String notify_ali() throws Exception{
		try{
			//获取支付宝POST过来反馈信息
			Map<String,String> params = new HashMap<String,String>();
			HttpServletRequest req = ServletActionContext.getRequest();
			Map requestParams = req.getParameterMap();

			for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
				String name = (String) iter.next();
				String[] values = (String[]) requestParams.get(name);
				String valueStr = "";
				for (int i = 0; i < values.length; i++) {
					valueStr = (i == values.length - 1) ? valueStr + values[i]
							: valueStr + values[i] + ",";
				}
				//乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
				//valueStr = new String(valueStr.getBytes("ISO-8859-1"), "gbk");
				params.put(name, valueStr);
			}
			
			//获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以下仅供参考)//
			//商户订单号
			
			String out_trade_no = new String(payDTO.getOut_trade_no().getBytes("ISO-8859-1"),"UTF-8");

			//支付宝交易号

			String trade_no = new String(payDTO.getTrade_no().getBytes("ISO-8859-1"),"UTF-8");

			//交易状态
			String trade_status = new String(payDTO.getTrade_status().getBytes("ISO-8859-1"),"UTF-8");

			String total_fee = new String(payDTO.getTotal_fee().getBytes("ISO-8859-1"),"UTF-8");
			//获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以上仅供参考)//

			if(true){//验证成功
				//////////////////////////////////////////////////////////////////////////////////////////
				//请在这里加上商户的业务逻辑程序代码
//				payInterface = new CtPayInterface();
//				payInterface.setOrderSn(out_trade_no);
//				payInterface.setTradeNo(trade_no);
//				payInterface.setTradeStatus(trade_status);
//				payInterface.setTotalFee(total_fee);
//				payInterface.setPayDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
//				payManager.save(payInterface);
				//——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
				
				if(trade_status.equals("TRADE_FINISHED")){
					//判断该笔订单是否在商户网站中已经做过处理
						//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
						//如果有做过处理，不执行商户的业务程序
						
					//注意：
					//退款日期超过可退款期限后（如三个月可退款），支付宝系统发送该交易状态通知
				} else if (trade_status.equals("TRADE_SUCCESS")){
					//判断该笔订单是否在商户网站中已经做过处理
						//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
						//如果有做过处理，不执行商户的业务程序
//					orinfo = orderManager.getOrderStatusByOrderSn(out_trade_no);
//					if (orinfo!=null){
//						String sta = orinfo.getOrderStatus();
//						Long oid = orinfo.getOrderId();
//						String total = orinfo.getTotal();
//				
//						
//						if (total.equals(total_fee) && sta.equals("0")){
//							orderManager.changeStatus(oid, "1");
//							this.request.put("orderInfo", orinfo);
//						}else{
//							
//							return "error";
//						}
//						
//					}else{
//						
//						return "error";
//					}
					//注意：
					//付款完成后，支付宝系统发送该交易状态通知
						orinfo = orderManager.getOrderStatusByOrderSn(out_trade_no);
						if (orinfo!=null){
							String sta = orinfo.getOrderStatus();
							Long oid = orinfo.getOrderId();
							String total = orinfo.getTotal();
							
							//判断是否使用现金劵
							String couponStr = orinfo.getCouponId();
							CtCashCoupon cash = new CtCashCoupon();
							if(couponStr != null){
								String chai[] = couponStr.split(",");
								for (int i = 0; i < chai.length; i++) {
									cash = orderManager.getCashInfoByCashId(Integer.valueOf(chai[i]));
									if(cash != null){
										cash.setUseTime(new SimpleDateFormat("yyyy-MM-dd HH:dd:ss").format(new Date()));
										orderManager.updateCashCouponById(cash);
										Double totalStr = sub(total, cash.getDiscount().toString());
										total = totalStr.toString();
										orinfo.setTotal(total);
										break;
									}
								}
							}
							
							
							Double to = Double.valueOf(total);
							Double to_fee = Double.valueOf(total_fee);
							if (to.equals(to_fee) && sta.equals("0")){
								
								//更新payInterface
								payInterface = new CtPayInterface();
								payInterface.setOrderSn(out_trade_no);
								payInterface.setTradeNo(trade_no);
								payInterface.setTradeStatus(trade_status);
								payInterface.setTotalFee(total_fee);
								payInterface.setTradeType("1");
								payInterface.setPayDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
								
								if(payManager.findPayByOrderSn(out_trade_no) == null){
									payManager.save(payInterface);
								}
								
								int credit = 0;
								//判断是否存在小数，存在就去掉小数，按整数计算积分
								if(to.toString().indexOf(".") > 0){
									String[] chai = to.toString().split("\\.");
									credit = Integer.valueOf(chai[0]);
								} else {
									credit = Integer.valueOf(to.toString());
								}
								
								//获取当前用的积分
								CtUser cu = (CtUser) this.session.get("userinfosession");
								
								//发放活动优惠券
								activityCoupon(credit, cu);
								
								if(cu.getUCredit() == null){
									Integer cre = 0;
									credit += cre;
								} else {
									Integer cre = cu.getUCredit();
									credit += cre;
								}
								
								cu.setUCredit(credit);
								orderManager.updateUserCredit(cu);
								orderManager.upChangeStatus(oid, "1");
								orinfo = orderManager.getOrderStatusByOrderSn(out_trade_no);
								this.request.put("orderInfo", orinfo);
								
								orinfo.setPay("2");
								orderManager.saveOrderInfo(orinfo);
								
								insertEJLOrder(orinfo, cu, payManager);
								//发送下单成功短信
								if(cu.getUMb() != null){
									s.paySuccessInfoMess(cu.getUMb(), orinfo.getOrderSn());
								}
								//EDB
								List<CtOrderGoods> orderGoodslist = orderManager.getOrderGoodsByOrderId(orinfo.getOrderId());
								edbTradeAdd(orinfo, orderGoodslist,regionManager,orderManager);
								
								
							}else{
								session.put("errorMess", "异常提交1");
								session.put("errorTotle", "异常提交10");
								return "error";
							}
					
						}else{
							session.put("errorMess", "异常提交2");
							session.put("errorTotle", "异常提交9");
							return "error";
						}
					session.put("errorMess", out_trade_no);
					session.put("errorTotle", "异常提交8");
					//该页面可做页面美工编辑
					return SUCCESS;
					
				}

				//——请根据您的业务逻辑来编写程序（以上代码仅作参考）——
					

 			//该页面可做页面美工编辑
				return SUCCESS;
				//——请根据您的业务逻辑来编写程序（以上代码仅作参考）——

				//////////////////////////////////////////////////////////////////////////////////////////
			}else{
				//该页面可做页面美工编辑
				session.put("errorMess", "异常提交验证失败");
				session.put("errorTotle", "异常提交7");
				return "error";
			}
			
			//return SUCCESS;
			
		}catch(Exception e){
			session.put("errorMess", "异常提交验证失败123"+e.getMessage());
			session.put("errorTotle", "异常提交6");
			return "error";
		}
		
	}
	private HttpServletRequest requ = ServletActionContext.getRequest();
	private HttpServletResponse respon = ServletActionContext.getResponse();
	
	public HttpServletRequest getRequ() {
		return requ;
	}

	public void setRequ(HttpServletRequest requ) {
		this.requ = requ;
	}

	public HttpServletResponse getRespon() {
		return respon;
	}

	public void setRespon(HttpServletResponse respon) {
		this.respon = respon;
	}
	private OrderDTO orderDTO = new OrderDTO();
	public OrderDTO getOrderDTO() {
		return orderDTO;
	}

	public void setOrderDTO(OrderDTO orderDTO) {
		this.orderDTO = orderDTO;
	}

	private List<CtCashCoupon> cashCouponsList = new ArrayList<CtCashCoupon>();
	private CtCashCoupon cashCoupon = new CtCashCoupon();
	
	public List<CtCashCoupon> getCashCouponsList() {
		return cashCouponsList;
	}

	public void setCashCouponsList(List<CtCashCoupon> cashCouponsList) {
		this.cashCouponsList = cashCouponsList;
	}

	public CtCashCoupon getCashCoupon() {
		return cashCoupon;
	}

	public void setCashCoupon(CtCashCoupon cashCoupon) {
		this.cashCoupon = cashCoupon;
	}

	//一键还款到支付页面
	public String oneKeyPay(){
		HttpServletRequest req = ServletActionContext.getRequest();
		String data = orderDTO.getData();
		String orderSns[] = data.split(",");
		Double total = 0D;
		for (int i = 0; i < orderSns.length; i++) {
			orinfo = orderManager.getOrderInfoByOrderSn(orderSns[i]);
			total = add(orinfo.getTotal(), total.toString());
		}
		Date dt = new Date();
		Long time = dt.getTime();
		Long osn = time * 8888;
		String orderSN = "CTHK"
				+ osn.toString().substring(osn.toString().length() - 10,
						osn.toString().length());
		Long gidsTest = orderManager.getOrderGoodsByOrderId(orinfo.getOrderId()).get(0).getGId();
		String path = req.getContextPath();
		String url = req.getScheme()+"://"+req.getServerName()+":"+req.getServerPort()+path+"/";
		url += "goods_detail?gid=" + gidsTest;
		//System.out.println(url);
		payDTO.setPay_desc("额度还款");
		payDTO.setOut_trade_no(orderSN);
		payDTO.setSubject("长亭易购-"+orderSN);
		payDTO.setTotal_fee(total.toString());
		payDTO.setGoods_address(url);
		payDTO.setOrderbody("合并还款");
		payDTO.setMem_id(orinfo.getUId());
		session.put("oneKeyOrderSn", data);
		session.put("hkOrderSn", orderSN);
		aliPayUrl(payDTO, 1);
		return SUCCESS;
	}
	
	//还款信用支付，单笔还款
	public String goXinyongHuan(){
		HttpServletRequest req = ServletActionContext.getRequest();
		String orderSn = orderDTO.getOrderSn();
    	CtUser cu = (CtUser) this.session.get("userinfosession");
    	orinfo = orderManager.getOrderByOrderSn(orderSn, cu.getUId());
    	payInterface = payManager.findPayInfoByOrderSn(orderSn);
    	
		if(!cu.getUId().toString().equals(orinfo.getUId())){
			return ERROR;
		}
		Long gidsTest = orderManager.getOrderGoodsByOrderId(orinfo.getOrderId()).get(0).getGId();
		String path = req.getContextPath();
		String url = req.getScheme()+"://"+req.getServerName()+":"+req.getServerPort()+path+"/";
		url += "goods_detail?gid=" + gidsTest;
		//System.out.println(url);
		payDTO.setPay_desc(orinfo.getDsc());
		payDTO.setOut_trade_no(orinfo.getOrderSn());
		payDTO.setSubject("长亭易购-"+orinfo.getOrderSn());
		payDTO.setTotal_fee(orinfo.getTotal());
		payDTO.setGoods_address(url);
		payDTO.setOrderbody(orinfo.getDsc());
		payDTO.setMem_id(orinfo.getUId());
		
		aliPayUrl(payDTO, 1);
		
		return SUCCESS;
	}
	
	public String getPayAllUrl(){
		HttpServletRequest req = ServletActionContext.getRequest();
		if(orderDTO.getOrderSn() != null){
			CtOrderInfo orderInfo = orderManager.getOrderInfoByOrderSn(orderDTO.getOrderSn());
			CtUser cu = (CtUser) this.session.get("userinfosession");
			if(!cu.getUId().toString().equals(orderInfo.getUId())){
				return ERROR;
			}
			Long gidsTest = orderManager.getOrderGoodsByOrderId(orderInfo.getOrderId()).get(0).getGId();
			String path = req.getContextPath();
			String url = req.getScheme()+"://"+req.getServerName()+":"+req.getServerPort()+path+"/";
			url += "goods_detail?gid=" + gidsTest;
			//System.out.println(url);
			payDTO.setPay_desc(orderInfo.getDsc());
			payDTO.setOut_trade_no(orderInfo.getOrderSn());
			payDTO.setSubject("长亭易购-"+orderInfo.getOrderSn());
			payDTO.setTotal_fee(orderInfo.getTotal());
			payDTO.setGoods_address(url);
			payDTO.setOrderbody(orderInfo.getDsc());
			payDTO.setMem_id(orderInfo.getUId());
			cashCouponsList = orderManager.findCashCouponByOrderSn(orderDTO.getOrderSn());
		} else {
			payDTO = (CtPayDTO) session.get("payDTOForZhi");
			System.out.println(payDTO.getOut_trade_no());
			CtOrderInfo orderInfo = orderManager.getOrderInfoByOrderSn(payDTO.getOut_trade_no());
			Long gidsTest = orderManager.getOrderGoodsByOrderId(orderInfo.getOrderId()).get(0).getGId();
			String path = req.getContextPath();
			String url = req.getScheme()+"://"+req.getServerName()+":"+req.getServerPort()+path+"/";
			url += "goods_detail?gid=" + gidsTest;
			//System.out.println(url);
			payDTO.setPay_desc(orderInfo.getDsc());
			payDTO.setOut_trade_no(orderInfo.getOrderSn());
			payDTO.setSubject("长亭易购-"+orderInfo.getOrderSn());
			payDTO.setTotal_fee(orderInfo.getTotal());
			payDTO.setGoods_address(url);
			payDTO.setOrderbody(orderInfo.getDsc());
			payDTO.setMem_id(orderInfo.getUId());
			cashCouponsList = orderManager.findCashCouponByOrderSn(payDTO.getOut_trade_no());
			if(payDTO == null){
				return ERROR;
			}
		}
		try {
			SimpleDateFormat sim=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			if(cashCouponsList != null){
//				for (int i = 0; i < cashCouponsList.size(); i++) {
//					cashCouponsList.get(i).setSdatetime(sim.parse(cashCouponsList.get(i).getEtime()));
//					if(cashCouponsList.get(i).getCashType().equals("1")){
//						if(Double.valueOf(payDTO.getTotal_fee()) < cashCouponsList.get(i).getAmount()){
//							cashCouponsList.get(i).setIsDisOk("disabled=\"disabled\"");
//						}
//					}
//				}
				if(cashCouponsList.get(0).getCashType().equals("1")){
					for (int i = 0; i < cashCouponsList.size(); i++) {
						if(cashCouponsList.get(i).getAmount() <= Double.valueOf(payDTO.getTotal_fee())){
							cashCoupon = cashCouponsList.get(i);
							break;
						}
					}
				} else {
					cashCoupon = cashCouponsList.get(0);
				}
//				payDTO.setTotal_fee((Double.valueOf(payDTO.getTotal_fee()) - cashCouponsList.get(0).getDiscount()) + "");
				Date nowDate = new Date();
				Long dateTime = nowDate.getTime();
				Date guoDate = sim.parse(cashCoupon.getEtime());
				Long guoTime = guoDate.getTime();
				if(dateTime > guoTime){
					cashCoupon.setUseTime(null);
					cashCoupon = null;
				} else {
					cashCoupon.setSdatetime(sim.parse(cashCoupon.getEtime()));
					Double t = Double.valueOf(payDTO.getTotal_fee());
					System.out.println(t);
					Double total = sub(t.toString(), cashCoupon.getDiscount().toString());
					CtOrderInfo orderInfo = orderManager.getOrderInfoByOrderSn(payDTO.getOut_trade_no());
					String couponList = orderInfo.getCouponId();
					CtCashCoupon c = null;
					if(couponList != null){
						String chai[] = couponList.split(",");
						for (int i = 0; i < chai.length; i++) {
							c = orderManager.getCashInfoByCashId(Integer.valueOf(chai[i]));
						}
					}
					if(c == null){
						if(total.toString().equals("0.0") || total <= 0){
//						result = "0";
							payDTO.setTotal_fee("0.00");
						} else {
							orderInfo.setDiscount((Double.valueOf(orderInfo.getDiscount() == null ? "0" : orderInfo.getDiscount()) + cashCoupon.getDiscount()) + "");
							if(orderInfo.getCouponId() != null){
								orderInfo.setCouponId(orderInfo.getCouponId() + "," + cashCoupon.getCashId());
							} else {
								orderInfo.setCouponId(cashCoupon.getCashId().toString());
							}
//							orderInfo.setTotal(total.toString());
//							cashCoupon.setUseTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
							orderManager.saveOrderInfo(orderInfo);
//						orderManager.updateCashCouponById(cashCoupon);
							payDTO.setTotal_fee(total.toString());
						}
					} else {
						payDTO.setTotal_fee(total.toString());
					}
				}
			} else {
				cashCoupon = null;
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		CtUser c = (CtUser) this.session.get("userinfosession");
		payDTO.setUser(c);
		payDTO.setCoupon(cashCoupon);
//		payDTO.setCashCouponList(cashCouponsList);
		getTenUrl(payDTO);
		aliPayUrl(payDTO,0);
		chinaPayUrl(payDTO);
		return SUCCESS;
	}
	
	//检测现金劵是否过期
	public String findTimeOk(){
		try {
			SimpleDateFormat sim=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			cashCoupon = orderManager.getCashInfoByCashId(payDTO.getCashId());
			if(cashCoupon != null){
				Date nowDate = new Date();
				Long dateTime = nowDate.getTime();
				Date guoDate = sim.parse(cashCoupon.getEtime());
				Long guoTime = guoDate.getTime();
				if(dateTime > guoTime){
					CtOrderInfo orderInfo = orderManager.getOrderInfoByOrderSn(payDTO.getOut_trade_no());
					String couponstr = orderInfo.getCouponId();
					CtCashCoupon c = null;
					String coupon = "";
					if(couponstr != null){
						String chai[] = couponstr.split(",");
						for (int i = 0; i < chai.length; i++) {
							c = orderManager.getCashInfoByCashId(Integer.valueOf(chai[i]));
							if(c == null){
								coupon += chai[i] + ",";
							}
						}
					}
					orderInfo.setCouponId(coupon);
					Double total = add(orderInfo.getTotal(), cashCoupon.getDiscount().toString());
//					orderInfo.setTotal(total.toString());
					orderManager.saveOrderInfo(orderInfo);
					result = "no";
				} else {
					result = "ok";
				}
			} else {
				result = "ok";
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	/**
     * 提供精确加法计算的add方法
     * @param value1 被加数
     * @param value2 加数
     * @return 两个参数的和
     */
    public static double add(String value1,String value2){
        BigDecimal b1 = new BigDecimal(value1);
        BigDecimal b2 = new BigDecimal(value2);
        return b1.add(b2).doubleValue();
    }
    /**
     * 提供精确减法运算的sub方法
     * @param value1 被减数
     * @param value2 减数
     * @return 两个参数的差
     */
    public static double sub(String value1,String value2){
        BigDecimal b1 = new BigDecimal(value1);
        BigDecimal b2 = new BigDecimal(value2);
        return b1.subtract(b2).doubleValue();
    }
	
	//现金劵使用，更新价格
	public String upCashCouponTotal(){
		HttpServletRequest req = ServletActionContext.getRequest();
		String orderSn = payDTO.getOut_trade_no();
		Integer cashId = payDTO.getCashId();
		cashCoupon = orderManager.getCashInfoByCashId(cashId);
		orinfo = orderManager.getOrderInfoByOrderSn(orderSn);
		if(cashCoupon.getCashType().equals("1")){
			if(Double.valueOf(orinfo.getTotal()) < cashCoupon.getAmount()){
				result = "1";
			} else {
				Double total = Double.valueOf(orinfo.getTotal()) - cashCoupon.getDiscount();
				if(total.toString().equals("0.0")){
					result = "0";
				} else {
					String path = req.getContextPath();
					String url = req.getScheme()+"://"+req.getServerName()+":"+req.getServerPort()+path+"/";
					Long gidsTest = orderManager.getOrderGoodsByOrderId(orinfo.getOrderId()).get(0).getGId();
					url += "goods_detail?gid=" + gidsTest;
					payDTO.setPay_desc(orinfo.getDsc());
					payDTO.setOut_trade_no(orinfo.getOrderSn());
					payDTO.setSubject("长亭易购-"+orinfo.getOrderSn());
					payDTO.setTotal_fee(total.toString());
					payDTO.setGoods_address(url);
					payDTO.setOrderbody(orinfo.getDsc());
					payDTO.setMem_id(orinfo.getUId());
					getTenUrl(payDTO);
					aliPayUrl(payDTO,0);
				}
				orinfo.setDiscount((Double.valueOf(orinfo.getDiscount() == null ? "0" : orinfo.getDiscount()) + cashCoupon.getDiscount()) + "");
				if(orinfo.getCouponId() != null){
					orinfo.setCouponId(orinfo.getCouponId() + "," + cashCoupon.getCashId());
				} else {
					orinfo.setCouponId(cashCoupon.getCashId().toString());
				}
				orinfo.setTotal(total.toString());
				cashCoupon.setUseTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				orderManager.saveOrderInfo(orinfo);
				orderManager.updateCashCouponById(cashCoupon);
//				chinaPayUrl(payDTO);
			}
		} else if(cashCoupon.getCashType().equals("2")) {
			Double total = Double.valueOf(orinfo.getTotal()) - cashCoupon.getDiscount();
			if(total.toString().equals("0.0")){
				result = "0";
			} else {
				String path = req.getContextPath();
				String url = req.getScheme()+"://"+req.getServerName()+":"+req.getServerPort()+path+"/";
				Long gidsTest = orderManager.getOrderGoodsByOrderId(orinfo.getOrderId()).get(0).getGId();
				url += "goods_detail?gid=" + gidsTest;
				payDTO.setPay_desc(orinfo.getDsc());
				payDTO.setOut_trade_no(orinfo.getOrderSn());
				payDTO.setSubject("长亭易购-"+orinfo.getOrderSn());
				payDTO.setTotal_fee(total.toString());
				payDTO.setGoods_address(url);
				payDTO.setOrderbody(orinfo.getDsc());
				payDTO.setMem_id(orinfo.getUId());
				getTenUrl(payDTO);
				aliPayUrl(payDTO,0);
			}
			orinfo.setDiscount((Double.valueOf(orinfo.getDiscount() == null ? "0" : orinfo.getDiscount()) + cashCoupon.getDiscount()) + "");
			if(orinfo.getCouponId() != null){
				orinfo.setCouponId(orinfo.getCouponId() + "," + cashCoupon.getCashId());
			} else {
				orinfo.setCouponId(cashCoupon.getCashId().toString());
			}
			orinfo.setTotal(total.toString());
			cashCoupon.setUseTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			orderManager.saveOrderInfo(orinfo);
			orderManager.updateCashCouponById(cashCoupon);
		}
		System.out.println(payDTO);
		return SUCCESS;
	}
	
	public String pay_ten(){
		return getTenUrl(payDTO);
	}

	private String getTenUrl(CtPayDTO payDTO) {
		//---------------------------------------------------------
		//财付通网关支付请求示例，商户按照此文档进行开发即可
		//---------------------------------------------------------

		try {
			
			requ.setCharacterEncoding("utf-8");
			//获取提交的商品价格
			String order_price=payDTO.getTotal_fee();
			Double price = Double.valueOf(order_price);
			/* 商品价格（包含运费），以分为单位 */
			double total_fee = (Double.valueOf(price) * 100);
			int fee = (int)total_fee;
			
			//获取提交的商品名称
			String product_name=payDTO.getSubject();

			//获取提交的备注信息
			String remarkexplain=payDTO.getPay_desc();

			String desc = "商品：" + product_name+",备注："+remarkexplain;

			//获取提交的订单号
			String out_trade_no=payDTO.getOut_trade_no();

			//支付方式
			String trade_mode="1";

			//---------------生成订单号 开始------------------------
			//当前时间 yyyyMMddHHmmss
			//String currTime = TenpayUtil.getCurrTime();
			//8位日期
			//String strTime = currTime.substring(8, currTime.length());
			//四位随机数
			//String strRandom = TenpayUtil.buildRandom(4) + "";
			//10位序列号,可以自行调整。
			//String strReq = strTime + strRandom;
			//订单号，此处用时间加随机数生成，商户根据自己情况调整，只要保持全局唯一就行
			//String out_trade_no = strReq;
			//---------------生成订单号 结束------------------------


			String currTime = TenpayUtil.getCurrTime();
			//创建支付请求对象
			RequestHandler reqHandler = new RequestHandler(requ, respon);
			reqHandler.init();
			//设置密钥
			reqHandler.setKey(AlipayConfig.keyTen);
			//设置支付网关
			reqHandler.setGateUrl("https://gw.tenpay.com/gateway/pay.htm");


			//-----------------------------
			//设置支付参数
			//-----------------------------
			reqHandler.setParameter("partner", AlipayConfig.partnerTen);		        //商户号
			reqHandler.setParameter("out_trade_no", out_trade_no);		//商家订单号
			reqHandler.setParameter("total_fee", String.valueOf(fee));			        //商品金额,以分为单位
			reqHandler.setParameter("return_url", AlipayConfig.return_urlTen);		    //交易完成后跳转的URL
			reqHandler.setParameter("notify_url", AlipayConfig.notify_urlTen);		    //接收财付通通知的URL
			reqHandler.setParameter("body", desc);	                    //商品描述
			reqHandler.setParameter("bank_type", "DEFAULT");		    //银行类型(中介担保时此参数无效)
			reqHandler.setParameter("spbill_create_ip",requ.getRemoteAddr());   //用户的公网ip，不是商户服务器IP
			reqHandler.setParameter("fee_type", "1");                    //币种，1人民币
			reqHandler.setParameter("subject", desc);              //商品名称(中介交易时必填)

			//系统可选参数
			reqHandler.setParameter("sign_type", "MD5");                //签名类型,默认：MD5
			reqHandler.setParameter("service_version", "1.0");			//版本号，默认为1.0
			reqHandler.setParameter("input_charset", "utf-8");            //字符编码
			reqHandler.setParameter("sign_key_index", "1");             //密钥序号


			//业务可选参数
			reqHandler.setParameter("attach", "");                      //附加数据，原样返回
			reqHandler.setParameter("product_fee", "");                 //商品费用，必须保证transport_fee + product_fee=total_fee
			reqHandler.setParameter("transport_fee", "0");               //物流费用，必须保证transport_fee + product_fee=total_fee
			reqHandler.setParameter("time_start", currTime);            //订单生成时间，格式为yyyymmddhhmmss
			reqHandler.setParameter("time_expire", "");                 //订单失效时间，格式为yyyymmddhhmmss
			reqHandler.setParameter("buyer_id", "");                    //买方财付通账号
			reqHandler.setParameter("goods_tag", "");                   //商品标记
			reqHandler.setParameter("trade_mode", trade_mode);                 //交易模式，1即时到账(默认)，2中介担保，3后台选择（买家进支付中心列表选择）
			reqHandler.setParameter("transport_desc", "");              //物流说明
			reqHandler.setParameter("trans_type", "1");                  //交易类型，1实物交易，2虚拟交易
			reqHandler.setParameter("agentid", "");                     //平台ID
			reqHandler.setParameter("agent_type", "");                  //代理模式，0无代理(默认)，1表示卡易售模式，2表示网店模式
			reqHandler.setParameter("seller_id", "");                   //卖家商户号，为空则等同于partner

			//请求的urlnew String(payDTO.getSubject().getBytes("ISO-8859-1"),"UTF-8");
			String requestUrl =reqHandler.getRequestURL();
			//获取debug信息,建议把请求和debug信息写入日志，方便定位问题
			String debuginfo = reqHandler.getDebugInfo();
			//System.out.println(requestUrl);
			session.put("requestUrl", requestUrl);
			result = requestUrl + "||";
			////System.out.println("requestUrl:  " + requestUrl);
			////System.out.println("sign_String:  " + debuginfo);
			return SUCCESS;
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			return ERROR;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			return ERROR;
		}
	}
	
	@SuppressWarnings("unused")
	public String ten_return(){
		
		
		//---------------------------------------------------------
		//财付通支付应答（处理回调）示例，商户按照此文档进行开发即可
		//---------------------------------------------------------
		
		//创建支付应答对象
		ResponseHandler resHandler = new ResponseHandler(requ, respon);
		resHandler.setKey(AlipayConfig.keyTen);
		
		//System.out.println("前台回调返回参数:"+resHandler.getAllParameters());
		
		//判断签名
		if(resHandler.isTenpaySign()) {
		
		    //通知id
			String notify_id = resHandler.getParameter("notify_id");
			//商户订单号
			String out_trade_no = resHandler.getParameter("out_trade_no");
			//财付通订单号
			String transaction_id = resHandler.getParameter("transaction_id");
			//金额,以分为单位
			String total_fee = resHandler.getParameter("total_fee");
			//如果有使用折扣券，discount有值，total_fee+discount=原请求的total_fee
			String discount = resHandler.getParameter("discount");
			//支付结果
			String trade_state = resHandler.getParameter("trade_state");
			//交易模式，1即时到账，2中介担保
			String trade_mode = resHandler.getParameter("trade_mode");
			
			if("1".equals(trade_mode)){       //即时到账 
				if( "0".equals(trade_state)){
			        //------------------------------
					//即时到账处理业务开始
					//------------------------------

		
					//注意交易单不要重复处理
					//注意判断返回金额
					if (trade_state.equals("0")) {	
						orinfo = orderManager.getOrderStatusByOrderSn(out_trade_no);
						if (orinfo!=null){
							String sta = orinfo.getOrderStatus();
							Long oid = orinfo.getOrderId();
							String total = orinfo.getTotal();
							
							//判断是否使用现金劵
							String couponStr = orinfo.getCouponId();
							CtCashCoupon cash = new CtCashCoupon();
							if(couponStr != null){
								String chai[] = couponStr.split(",");
								for (int i = 0; i < chai.length; i++) {
									cash = orderManager.getCashInfoByCashId(Integer.valueOf(chai[i]));
									if(cash != null){
										cash.setUseTime(new SimpleDateFormat("yyyy-MM-dd HH:dd:ss").format(new Date()));
										orderManager.updateCashCouponById(cash);
										Double totalStr = sub(total, cash.getDiscount().toString());
										total = totalStr.toString();
										orinfo.setTotal(total);
										break;
									}
								}
							}
							
							Double price = (Double.valueOf(total) * 100);
							if (price.equals(Double.valueOf(total_fee)) && sta.equals("0")){
								
								payInterface = new CtPayInterface();
								payInterface.setOrderSn(out_trade_no);
								payInterface.setTradeNo(transaction_id);
								payInterface.setTradeStatus(trade_state);
								payInterface.setTotalFee(total_fee);
								payInterface.setTradeType("2");
								payInterface.setPayDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
								
								if(payManager.findPayByOrderSn(out_trade_no) == null){
									payManager.save(payInterface);
								}
								
								int credit = 0;
								//判断是否存在小数，存在就去掉小数，按整数计算积分
								if(price.toString().indexOf(".") > 0){
									String[] chai = price.toString().split("\\.");
									credit = Integer.valueOf(chai[0]);
								} else {
									credit = Integer.valueOf(price.toString());
								}
								//获取当前用的积分
								CtUser cu = (CtUser) this.session.get("userinfosession");
								
								//发放活动优惠券
								activityCoupon(credit, cu);
								
								if(cu.getUCredit() == null){
									Integer cre = 0;
									credit += cre;
								} else {
									Integer cre = cu.getUCredit();
									credit += cre;
								}
								
								cu.setUCredit(credit);
								orderManager.updateUserCredit(cu);
								orderManager.upChangeStatus(oid, "1");
								session.put("resultError", "付款成功");
								orinfo = orderManager.getOrderStatusByOrderSn(out_trade_no);
								
								orinfo.setPay("5");
								orderManager.saveOrderInfo(orinfo);
								
								insertEJLOrder(orinfo, cu, payManager);
								//发送下单成功短信
								if(cu.getUMb() != null){
									s.paySuccessInfoMess(cu.getUMb(), orinfo.getOrderSn());
								}
								//EDB
								List<CtOrderGoods> orderGoodslist = orderManager.getOrderGoodsByOrderId(orinfo.getOrderId());
								edbTradeAdd(orinfo, orderGoodslist,regionManager,orderManager);
								
							} else {
								session.put("resultError", "余额不足或者已经付款");
								return SUCCESS;
							}
						}
					
					//------------------------------
					//即时到账处理业务完毕
					//------------------------------
					
						//System.out.println("即时到帐付款成功");
					}else{
						//System.out.println("即时到帐付款失败");
					}
			}
		} else {
			//System.out.println("认证签名失败");
		}
		}
		//获取debug信息,建议把debug信息写入日志，方便定位问题
		String debuginfo = resHandler.getDebugInfo();
		//System.out.println("debuginfo:" + debuginfo);
		//out.print("sign_String:  " + debuginfo + "<br><br>");
		return SUCCESS;
	}
	
	
	
	public String notify_ten(){
		try {
			//---------------------------------------------------------
			//财付通支付通知（后台通知）示例，商户按照此文档进行开发即可
			//---------------------------------------------------------

			//创建支付应答对象
			ResponseHandler resHandler = new ResponseHandler(requ, respon);
			resHandler.setKey(AlipayConfig.keyTen);

			//System.out.println("后台回调返回参数:"+resHandler.getAllParameters());

			//判断签名
			if(resHandler.isTenpaySign()) {
				
				//通知id
				String notify_id = resHandler.getParameter("notify_id");
				
				//创建请求对象
				RequestHandler queryReq = new RequestHandler(null, null);
				//通信对象
				TenpayHttpClient httpClient = new TenpayHttpClient();
				//应答对象
				ClientResponseHandler queryRes = new ClientResponseHandler();
				
				//通过通知ID查询，确保通知来至财付通
				queryReq.init();
				queryReq.setKey(AlipayConfig.keyTen);
				queryReq.setGateUrl("https://gw.tenpay.com/gateway/simpleverifynotifyid.xml");
				queryReq.setParameter("partner", AlipayConfig.partnerTen);
				queryReq.setParameter("notify_id", notify_id);
				
				//通信对象
				httpClient.setTimeOut(5);
				//设置请求内容
				httpClient.setReqContent(queryReq.getRequestURL());
				//System.out.println("验证ID请求字符串:" + queryReq.getRequestURL());
				
				//后台调用
				if(httpClient.call()) {
					//设置结果参数
					queryRes.setContent(httpClient.getResContent());
					//System.out.println("验证ID返回字符串:" + httpClient.getResContent());
					queryRes.setKey(AlipayConfig.keyTen);
						
						
					//获取id验证返回状态码，0表示此通知id是财付通发起
					String retcode = queryRes.getParameter("retcode");
					
					//商户订单号
					String out_trade_no = resHandler.getParameter("out_trade_no");
					//财付通订单号
					String transaction_id = resHandler.getParameter("transaction_id");
					//金额,以分为单位
					String total_fee = resHandler.getParameter("total_fee");
					//如果有使用折扣券，discount有值，total_fee+discount=原请求的total_fee
					String discount = resHandler.getParameter("discount");
					//支付结果
					String trade_state = resHandler.getParameter("trade_state");
					//交易模式，1即时到账，2中介担保
					String trade_mode = resHandler.getParameter("trade_mode");
						
					//判断签名及结果
					if(queryRes.isTenpaySign()&& "0".equals(retcode)){ 
						//System.out.println("id验证成功");
						
						if("1".equals(trade_mode)){       //即时到账 
							if( "0".equals(trade_state)){

								if (trade_state.equals("0")) {	
									orinfo = orderManager.getOrderStatusByOrderSn(out_trade_no);
									if (orinfo!=null){
										String sta = orinfo.getOrderStatus();
										Long oid = orinfo.getOrderId();
										String total = orinfo.getTotal();
										
										//判断是否使用现金劵
										String couponStr = orinfo.getCouponId();
										CtCashCoupon cash = new CtCashCoupon();
										if(couponStr != null){
											String chai[] = couponStr.split(",");
											for (int i = 0; i < chai.length; i++) {
												cash = orderManager.getCashInfoByCashId(Integer.valueOf(chai[i]));
												if(cash != null){
													cash.setUseTime(new SimpleDateFormat("yyyy-MM-dd HH:dd:ss").format(new Date()));
													orderManager.updateCashCouponById(cash);
													Double totalStr = sub(total, cash.getDiscount().toString());
													total = totalStr.toString();
													orinfo.setTotal(total);
													break;
												}
											}
										}
										
										Double price = (Double.valueOf(total) * 100);
										if (price.equals(Double.valueOf(total_fee)) && sta.equals("0")){
											
											payInterface = new CtPayInterface();
											payInterface.setOrderSn(out_trade_no);
											payInterface.setTradeNo(transaction_id);
											payInterface.setTradeStatus(trade_state);
											payInterface.setTotalFee(total_fee);
											payInterface.setTradeType("2");
											payInterface.setPayDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
											
											if(payManager.findPayByOrderSn(out_trade_no) == null){
												payManager.save(payInterface);
											}
											
											
											int credit = 0;
											//判断是否存在小数，存在就去掉小数，按整数计算积分
											if(price.toString().indexOf(".") > 0){
												String[] chai = price.toString().split("\\.");
												credit = Integer.valueOf(chai[0]);
											} else {
												credit = Integer.valueOf(price.toString());
											}
											//获取当前用的积分
											CtUser cu = (CtUser) this.session.get("userinfosession");
											
											//发放活动优惠券
											activityCoupon(credit, cu);
											
											if(cu.getUCredit() == null){
												Integer cre = 0;
												credit += cre;
											} else {
												Integer cre = cu.getUCredit();
												credit += cre;
											}
											
											cu.setUCredit(credit);
											orderManager.updateUserCredit(cu);
											orderManager.upChangeStatus(oid, "1");
											session.put("resultError", "付款成功");
											orinfo = orderManager.getOrderStatusByOrderSn(out_trade_no);
											
											orinfo.setPay("5");
											orderManager.saveOrderInfo(orinfo);
											
											
											insertEJLOrder(orinfo, cu, payManager);
											//发送下单成功短信
											if(cu.getUMb() != null){
												s.paySuccessInfoMess(cu.getUMb(), orinfo.getOrderSn());
											}
											//EDB
											List<CtOrderGoods> orderGoodslist = orderManager.getOrderGoodsByOrderId(orinfo.getOrderId());
											edbTradeAdd(orinfo, orderGoodslist,regionManager,orderManager);
											
											
										} else {
											session.put("resultError", "余额不足或者已经付款");
											return SUCCESS;
										}
									}
								
								//------------------------------
								//即时到账处理业务完毕
								//------------------------------
								
									//System.out.println("即时到帐付款成功");
								}else{
									//System.out.println("即时到帐付款失败");
								}
								
								//System.out.println("即时到账支付成功");
								//给财付通系统发送成功信息，财付通系统收到此结果后不再进行后续通知
								resHandler.sendToCFT("success");
								
							}else{
								//System.out.println("即时到账支付失败");
								resHandler.sendToCFT("fail");
							}
						}
					}else{
						//错误时，返回结果未签名，记录retcode、retmsg看失败详情。
						//System.out.println("查询验证签名失败或id验证失败"+",retcode:" + queryRes.getParameter("retcode"));
					}
				} else {
					//System.out.println("后台调用通信失败");
					//System.out.println(httpClient.getResponseCode());
					//System.out.println(httpClient.getErrInfo());
					//有可能因为网络原因，请求已经处理，但未收到应答。
				}
			}else{
				//System.out.println("通知签名验证失败");
			}
			return SUCCESS;
		} catch (UnsupportedEncodingException e) {
			//e.printStackTrace();
			return ERROR;
		} catch (IOException e) {
			//e.printStackTrace();
			return ERROR;
		} catch (Exception e) {
			//e.printStackTrace();
			return ERROR;
		}

	}
	
	public String getWeiXinImgUrl_XinYong(){
		WxPayDto wxPayDto = new WxPayDto();
		String orderSn = payDTO.getOut_trade_no();
		
		
		String data = (String) session.get("oneKeyOrderSn");
		List<String> orderSnList = new ArrayList<String>();
		if(data != null){
			String[] orderSns = data.split(",");
			for (int i = 0; i < orderSns.length; i++) {
				orderSnList.add(orderSns[i]);
			}
		} else {
			orderSnList.add(orderSn);
		}
		if(orderSnList == null || orderSnList.size() ==0){
			return "orderInfoList";
		}
		Double tot = 0D;
		for (int i = 0; i < orderSnList.size(); i++) {
			String orderSnNow = orderSnList.get(i);
			CtOrderInfo orderInfo = orderManager.getOrderInfoByOrderSn(orderSnNow);
			tot = add(tot.toString(), orderInfo.getTotal());
		}
		CtOrderInfo c = new CtOrderInfo();
		c.setTotal(tot.toString());
		c.setOrderSn(orderSn);
		wxPayDto.setBody("长亭易购订单");
		wxPayDto.setNotifyUrl(AlipayConfig.notifyurl);
		wxPayDto.setOrderId(orderSn);
		HttpServletRequest req = ServletActionContext.getRequest();
		String Ip =UtilDate.getIpAddr(req);
		wxPayDto.setSpbillCreateIp(Ip);
		wxPayDto.setTotalFee(tot.toString());
		String url = "";
		url = getCodeurl(wxPayDto);
		session.put("weixinPayurl", url);
		session.put("weixinorderSn", orderSn);
		session.put("orderInfowx", c);
		
		return SUCCESS; 
	}
	
	//获取微信支付的二维码
	public String getWeiXinImgUrl(){
		WxPayDto wxPayDto = new WxPayDto();
		String orderSn = payDTO.getOut_trade_no();
		if(orderSn == null){
			return "orderInfoList";
		}
		CtOrderInfo orderInfo = orderManager.getOrderInfoByOrderSn(orderSn);
		wxPayDto.setBody("长亭易购订单");
		wxPayDto.setNotifyUrl(AlipayConfig.notifyurl);
		wxPayDto.setOrderId(orderSn);
		HttpServletRequest req = ServletActionContext.getRequest();
		String Ip =UtilDate.getIpAddr(req);
		wxPayDto.setSpbillCreateIp(Ip);
		wxPayDto.setTotalFee(orderInfo.getTotal());
		String url = "";
		url = getCodeurl(wxPayDto);
		session.put("weixinPayurl", url);
		session.put("weixinorderSn", orderSn);
		session.put("orderInfowx", orderInfo);
		return SUCCESS;
	}
	private String result;
	
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	//一定时间之后重新获取二维码
	public String againwxEr(){
		WxPayDto wxPayDto = new WxPayDto();
		String orderSn = payDTO.getOut_trade_no();
		CtOrderInfo orderInfo = orderManager.getOrderInfoByOrderSn(orderSn);
		wxPayDto.setBody("长亭易购订单");
		wxPayDto.setNotifyUrl(AlipayConfig.notifyurl);
		wxPayDto.setOrderId(orderSn);
		HttpServletRequest req = ServletActionContext.getRequest();
		String Ip =UtilDate.getIpAddr(req);
		wxPayDto.setSpbillCreateIp(Ip);
		wxPayDto.setTotalFee(orderInfo.getTotal());
		String url = getCodeurl(wxPayDto);
		result = url;
		session.put("weixinPayurl", url);
		return SUCCESS;
	}
	
	//定时查询是否付款
	public String FindOrderWeiXinIsOk(){
		String orderSn = payDTO.getOut_trade_no();
		WxPayDto wxPayDto = new WxPayDto();
		wxPayDto.setOrderId(orderSn);
		String sta = getOrderInfoSta(wxPayDto);
		if(sta.equals("SUCCESS")){
			String infoXml = getOrderInfo(wxPayDto);
			session.put("infoXml", infoXml);
		}
		result = sta;
		return SUCCESS;
	}
	
	//信用支付微信支付成功返回
	public String wxreturnOk_cre(){
		String inputLine;
		String notityXml = (String) session.get("infoXml");
		String resXml = "";
		//System.out.println("接收到的报文：" + notityXml);
		Map m = parseXmlToList2(notityXml);
		org.ctonline.dto.pay.WxPayResult wpr = new org.ctonline.dto.pay.WxPayResult();
		wpr.setAppid(m.get("appid").toString());
		wpr.setBankType(m.get("bank_type").toString());
		wpr.setCashFee(m.get("cash_fee").toString());
		wpr.setFeeType(m.get("fee_type").toString());
		wpr.setIsSubscribe(m.get("is_subscribe").toString());
		wpr.setMchId(m.get("mch_id").toString());
		wpr.setNonceStr(m.get("nonce_str").toString());
		wpr.setOpenid(m.get("openid").toString());
		wpr.setOutTradeNo(m.get("out_trade_no").toString());
		wpr.setResultCode(m.get("result_code").toString());
		wpr.setReturnCode(m.get("return_code").toString());
		wpr.setSign(m.get("sign").toString());
		wpr.setTimeEnd(m.get("time_end").toString());
		wpr.setTotalFee(m.get("total_fee").toString());
		wpr.setTradeType(m.get("trade_type").toString());
		wpr.setTransactionId(m.get("transaction_id").toString());
		
		if("SUCCESS".equals(wpr.getResultCode())){
			//支付成功
			
			orinfo = orderManager.getOrderStatusByOrderSn(wpr.getOutTradeNo());
			List<CtOrderInfo> listOrderInfo = new ArrayList<CtOrderInfo>();
			Double d = 0D;
			if(orinfo == null){
				String data = (String) session.get("oneKeyOrderSn");
				if(data != null){
					String orderSns[] = data.split(",");
					for (int i = 0; i < orderSns.length; i++) {
						CtOrderInfo info = orderManager.getOrderStatusByOrderSn(orderSns[i]);
						listOrderInfo.add(info);
						d = add(d.toString(), info.getTotal());
					}
				} else {
					session.put("errorMess", "订单信息为空"+listOrderInfo.size()+"_"+data);
					session.put("errorTotle", "异常提交13");
					return "error";
				}
			} else {
				listOrderInfo.add(orinfo);
			}
			for (int i = 0; i < listOrderInfo.size(); i++) {
				orinfo = listOrderInfo.get(i);
				if (orinfo!=null){
					String sta = orinfo.getOrderStatus();
					Long oid = orinfo.getOrderId();
					String total = d.toString();
					
					Double to = Double.valueOf(total);
					Double to_fee = Double.valueOf(wpr.getTotalFee())/100;
					if (to.equals(to_fee)){
						
						payInterface = payManager.findPayInfoByOrderSn(orinfo.getOrderSn());
						//更新payInterface
						CtOrderInfo info = orderManager.getOrderInfoByOrderSn(orinfo.getOrderSn());
						payInterface.setTradeNo(wpr.getTransactionId());
						payInterface.setTradeStatus(wpr.getResultCode());
						payInterface.setTotalFee(to.toString());
						payInterface.setTradeType("3");
						payInterface.setPayDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						
						
						payManager.save(payInterface);
						
						int credit = 0;
						//判断是否存在小数，存在就去掉小数，按整数计算积分
						if(to.toString().indexOf(".") > 0){
							String[] chai = to.toString().split("\\.");
							credit = Integer.valueOf(chai[0]);
						} else {
							credit = Integer.valueOf(to.toString());
						}
						//获取当前用的积分
						CtUser cuSess = (CtUser) this.session.get("userinfosession");
						CtUser cu = orderManager.getUserByIUid(cuSess.getUId());
						
						if(cu.getUCredit() == null){
							Integer cre = 0;
							credit += cre;
						} else {
							Integer cre = cu.getUCredit();
							credit += cre;
						}
						cu.setUCredit(credit);
						cu.setURemainingAmount(add(cu.getURemainingAmount().toString(), to_fee.toString()));
						orderManager.updateUser(cu);
						orderManager.upChangeStatus(oid, "1");
						this.request.put("orderInfo", orinfo);
						orinfo = orderManager.getOrderStatusByOrderSn(orinfo.getOrderSn());
						orinfo.setPay("6");
						orderManager.saveOrderInfo(orinfo);
						this.session.put("userinfosession",cu);
//						insertEJLOrder(orinfo, cu, payManager);
//						//发送下单成功短信
//						if(cu.getUMb() != null){
//							s.paySuccessInfoMess(cu.getUMb(), orinfo.getOrderSn());
//						}
					}else{
						session.put("errorMess", "异常提交");
						session.put("errorTotle", "异常提交5");
						return "error";
					}
			
				}else{
					session.put("errorMess", "异常提交");
					session.put("errorTotle", "异常提交4");
					return "error";
				}
				
			}
			
			resXml = "<xml>" + "<return_code><![CDATA[SUCCESS]]></return_code>"
			+ "<return_msg><![CDATA[OK]]></return_msg>" + "</xml> ";
		}else{
			resXml = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>"
			+ "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
		}

		//System.out.println("微信支付回调数据结束");
		session.put("orderInfo", orinfo);
		return SUCCESS;
	}
	
	//支付成功之后回调的请求
	public String wxreturnOk(){
		String inputLine;
		String notityXml = (String) session.get("infoXml");
		String resXml = "";
		//System.out.println("接收到的报文：" + notityXml);
		Map m = parseXmlToList2(notityXml);
		org.ctonline.dto.pay.WxPayResult wpr = new org.ctonline.dto.pay.WxPayResult();
		wpr.setAppid(m.get("appid").toString());
		wpr.setBankType(m.get("bank_type").toString());
		wpr.setCashFee(m.get("cash_fee").toString());
		wpr.setFeeType(m.get("fee_type").toString());
		wpr.setIsSubscribe(m.get("is_subscribe").toString());
		wpr.setMchId(m.get("mch_id").toString());
		wpr.setNonceStr(m.get("nonce_str").toString());
		wpr.setOpenid(m.get("openid").toString());
		wpr.setOutTradeNo(m.get("out_trade_no").toString());
		wpr.setResultCode(m.get("result_code").toString());
		wpr.setReturnCode(m.get("return_code").toString());
		wpr.setSign(m.get("sign").toString());
		wpr.setTimeEnd(m.get("time_end").toString());
		wpr.setTotalFee(m.get("total_fee").toString());
		wpr.setTradeType(m.get("trade_type").toString());
		wpr.setTransactionId(m.get("transaction_id").toString());
		
		if("SUCCESS".equals(wpr.getResultCode())){
			//支付成功
			
			orinfo = orderManager.getOrderStatusByOrderSn(wpr.getOutTradeNo());
			if (orinfo!=null){
				String sta = orinfo.getOrderStatus();
				Long oid = orinfo.getOrderId();
				String total = orinfo.getTotal();
				
				//判断是否使用现金劵
				String couponStr = orinfo.getCouponId();
				CtCashCoupon cash = new CtCashCoupon();
				if(couponStr != null){
					String chai[] = couponStr.split(",");
					for (int i = 0; i < chai.length; i++) {
						cash = orderManager.getCashInfoByCashId(Integer.valueOf(chai[i]));
						if(cash != null){
							cash.setUseTime(new SimpleDateFormat("yyyy-MM-dd HH:dd:ss").format(new Date()));
							orderManager.updateCashCouponById(cash);
							Double totalStr = sub(total, cash.getDiscount().toString());
							total = totalStr.toString();
							orinfo.setTotal(total);
							break;
						}
					}
				}
				
				Double to = Double.valueOf(total);
				Double to_fee = Double.valueOf(wpr.getTotalFee())/100;
				if (to.equals(to_fee) && sta.equals("0")){
					
					//更新payInterface
					payInterface = new CtPayInterface();
					payInterface.setOrderSn(orinfo.getOrderSn());
					payInterface.setTradeNo(wpr.getTransactionId());
					payInterface.setTradeStatus(wpr.getResultCode());
					payInterface.setTotalFee(to.toString());
					payInterface.setTradeType("3");
					payInterface.setPayDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
					
					if(payManager.findPayByOrderSn(orinfo.getOrderSn()) == null){
						payManager.save(payInterface);
					}
					int credit = 0;
					//判断是否存在小数，存在就去掉小数，按整数计算积分
					if(to.toString().indexOf(".") > 0){
						String[] chai = to.toString().split("\\.");
						credit = Integer.valueOf(chai[0]);
					} else {
						credit = Integer.valueOf(to.toString());
					}
					//获取当前用的积分
					CtUser cu = (CtUser) this.session.get("userinfosession");
					
					//发放活动优惠券
					activityCoupon(credit, cu);
					
					if(cu.getUCredit() == null){
						Integer cre = 0;
						credit += cre;
					} else {
						Integer cre = cu.getUCredit();
						credit += cre;
					}
					
					cu.setUCredit(credit);
					orderManager.updateUserCredit(cu);
					orderManager.upChangeStatus(oid, "1");
					orinfo = orderManager.getOrderStatusByOrderSn(wpr.getOutTradeNo());
					this.request.put("orderInfo", orinfo);
					
					orinfo.setPay("3");
					orderManager.saveOrderInfo(orinfo);
					
					
					insertEJLOrder(orinfo, cu, payManager);
					//发送下单成功短信
					if(cu.getUMb() != null){
						s.paySuccessInfoMess(cu.getUMb(), orinfo.getOrderSn());
					}
					//EDB
					List<CtOrderGoods> orderGoodslist = orderManager.getOrderGoodsByOrderId(orinfo.getOrderId());
					edbTradeAdd(orinfo, orderGoodslist,regionManager,orderManager);
					
					
				}else{
					session.put("errorMess", "异常提交");
					session.put("errorTotle", "异常提交5");
					return "error";
				}
		
			}else{
				session.put("errorMess", "异常提交");
				session.put("errorTotle", "异常提交4");
				return "error";
			}
			
			
			
			resXml = "<xml>" + "<return_code><![CDATA[SUCCESS]]></return_code>"
			+ "<return_msg><![CDATA[OK]]></return_msg>" + "</xml> ";
		}else{
			resXml = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>"
			+ "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
		}

		//System.out.println("微信支付回调数据结束");
		session.put("orderInfo", orinfo);
		return SUCCESS;
	}
	/**
	 * description: 解析微信通知xml
	 * 
	 * @param xml
	 * @return
	 * @author ex_yangxiaoyi
	 * @see
	 */
	@SuppressWarnings({ "unused", "rawtypes", "unchecked" })
	private static Map parseXmlToList2(String xml) {
		Map retMap = new HashMap();
		try {
			StringReader read = new StringReader(xml);
			// 创建新的输入源SAX 解析器将使用 InputSource 对象来确定如何读取 XML 输入
			InputSource source = new InputSource(read);
			// 创建一个新的SAXBuilder
			SAXBuilder sb = new SAXBuilder();
			// 通过输入源构造一个Document
			Document doc = (Document) sb.build(source);
			Element root = doc.getRootElement();// 指向根节点
			List<Element> es = root.getChildren();
			if (es != null && es.size() != 0) {
				for (Element element : es) {
					retMap.put(element.getName(), element.getValue());
				}
			}
		} catch (Exception e) {
			//e.printStackTrace();
		}
		return retMap;
	}
	
	
	/**
	 * 获取微信扫码支付二维码连接
	 */
	public String getCodeurl(WxPayDto tpWxPayDto){
		// 1 参数
		// 订单号
		String orderId = tpWxPayDto.getOrderId();
		// 附加数据 原样返回
		String attach = "";
		// 总金额以分为单位，不带小数点
		String totalFee = getMoney(tpWxPayDto.getTotalFee());
		
		// 订单生成的机器 IP
		String spbill_create_ip = tpWxPayDto.getSpbillCreateIp();
		// 这里notify_url是 支付完成后微信发给该链接信息，可以判断会员是否支付成功，改变订单状态等。
		String notify_url = AlipayConfig.notifyurl;
		String trade_type = "NATIVE";

		// 商户号
		String mch_id = AlipayConfig.partner_Weixin;
		// 随机字符串
		String nonce_str = getNonceStr();

		// 商品描述根据情况修改
		String body = tpWxPayDto.getBody();

		// 商户订单号
		String out_trade_no = orderId;

		SortedMap<String, String> packageParams = new TreeMap<String, String>();
		packageParams.put("appid", AlipayConfig.appid);
		packageParams.put("mch_id", mch_id);
		packageParams.put("nonce_str", nonce_str);
		packageParams.put("body", body);
		packageParams.put("attach", attach);
		packageParams.put("out_trade_no", out_trade_no);

		// 这里写的金额为1 分到时修改
		packageParams.put("total_fee", totalFee);
		packageParams.put("spbill_create_ip", spbill_create_ip);
		packageParams.put("notify_url", notify_url);

		packageParams.put("trade_type", trade_type);

		org.ctonline.util.RequestHandler reqHandler = new org.ctonline.util.RequestHandler(null, null);
		reqHandler.init(AlipayConfig.appid, AlipayConfig.appsecret, AlipayConfig.partnerkey);

		String sign = reqHandler.createSign(packageParams);
		String xml = "<xml>" + "<appid>" + AlipayConfig.appid + "</appid>" + "<mch_id>"
				+ mch_id + "</mch_id>" + "<nonce_str>" + nonce_str
				+ "</nonce_str>" + "<sign>" + sign + "</sign>"
				+ "<body><![CDATA[" + body + "]]></body>" 
				+ "<out_trade_no>" + out_trade_no
				+ "</out_trade_no>" + "<attach>" + attach + "</attach>"
				+ "<total_fee>" + totalFee + "</total_fee>"
				+ "<spbill_create_ip>" + spbill_create_ip
				+ "</spbill_create_ip>" + "<notify_url>" + notify_url
				+ "</notify_url>" + "<trade_type>" + trade_type
				+ "</trade_type>" + "</xml>";
		String code_url = "";
		String createOrderURL = "https://api.mch.weixin.qq.com/pay/unifiedorder";
		
		code_url = new GetWxOrderno().getCodeUrl(createOrderURL, xml);
		
		return code_url;
	}
	
	
	/**
	 * 获取随机字符串
	 * 
	 * @return
	 */
	public static String getNonceStr() {
		// 随机数
		String currTime = TenpayUtil.getCurrTime();
		// 8位日期
		String strTime = currTime.substring(8, currTime.length());
		// 四位随机数
		String strRandom = TenpayUtil.buildRandom(4) + "";
		// 10位序列号,可以自行调整。
		return strTime + strRandom;
	}

	/**
	 * 元转换成分
	 * 
	 * @param money
	 * @return
	 */
	public static String getMoney(String amount) {
		if (amount == null) {
			return "";
		}
		// 金额转化为分为单位
		String currency = amount.replaceAll("\\$|\\￥|\\,", ""); // 处理包含, ￥
																// 或者$的金额
		int index = currency.indexOf(".");
		int length = currency.length();
		Long amLong = 0l;
		if (index == -1) {
			amLong = Long.valueOf(currency + "00");
		} else if (length - index >= 3) {
			amLong = Long.valueOf((currency.substring(0, index + 3)).replace(
					".", ""));
		} else if (length - index == 2) {
			amLong = Long.valueOf((currency.substring(0, index + 2)).replace(
					".", "") + 0);
		} else {
			amLong = Long.valueOf((currency.substring(0, index + 1)).replace(
					".", "") + "00");
		}
		return amLong.toString();
	}

	/**
	 * 获取订单信息
	 * @param tpWxPayDto
	 * @return
	 */
	public static String getOrderInfoSta(WxPayDto tpWxPayDto){
		String orderId = tpWxPayDto.getOrderId();
		// 商户号
		String mch_id = AlipayConfig.partner_Weixin;
		// 随机字符串
		String nonce_str = getNonceStr();

		// 商户订单号
		String out_trade_no = orderId;
		SortedMap<String, String> packageParams = new TreeMap<String, String>();
		packageParams.put("appid", AlipayConfig.appid);
		packageParams.put("mch_id", mch_id);
		packageParams.put("nonce_str", nonce_str);
		packageParams.put("out_trade_no", out_trade_no);

		org.ctonline.util.RequestHandler reqHandler = new org.ctonline.util.RequestHandler(null, null);
		reqHandler.init(AlipayConfig.appid, AlipayConfig.appsecret, AlipayConfig.partnerkey);
		String sign = reqHandler.createSign(packageParams);
		StringBuffer sb = new StringBuffer();
		sb.append("<xml>");
		sb.append("   <appid>"+AlipayConfig.appid+"</appid>");
		sb.append("   <mch_id>"+mch_id+"</mch_id>");
		sb.append("   <nonce_str>"+nonce_str+"</nonce_str>");
		sb.append("   <out_trade_no>"+out_trade_no+"</out_trade_no>");
		sb.append("   <sign>"+sign+"</sign>");
		sb.append("</xml>");
		String findOrderSta = "";
		String createOrderURL = "https://api.mch.weixin.qq.com/pay/orderquery";
		
		
		findOrderSta = new GetWxOrderno().getOrderSta(createOrderURL, sb.toString());
		return findOrderSta;
	}
	/**
	 * 获取订单信息
	 * @param tpWxPayDto
	 * @return
	 */
	public static String getOrderInfo(WxPayDto tpWxPayDto){
		String orderId = tpWxPayDto.getOrderId();
		// 商户号
		String mch_id = AlipayConfig.partner_Weixin;
		// 随机字符串
		String nonce_str = getNonceStr();

		// 商户订单号
		String out_trade_no = orderId;
		SortedMap<String, String> packageParams = new TreeMap<String, String>();
		packageParams.put("appid", AlipayConfig.appid);
		packageParams.put("mch_id", mch_id);
		packageParams.put("nonce_str", nonce_str);
		packageParams.put("out_trade_no", out_trade_no);

		org.ctonline.util.RequestHandler reqHandler = new org.ctonline.util.RequestHandler(null, null);
		reqHandler.init(AlipayConfig.appid, AlipayConfig.appsecret, AlipayConfig.partnerkey);
		String sign = reqHandler.createSign(packageParams);
		StringBuffer sb = new StringBuffer();
		sb.append("<xml>");
		sb.append("   <appid>"+AlipayConfig.appid+"</appid>");
		sb.append("   <mch_id>"+mch_id+"</mch_id>");
		sb.append("   <nonce_str>"+nonce_str+"</nonce_str>");
		sb.append("   <out_trade_no>"+out_trade_no+"</out_trade_no>");
		sb.append("   <sign>"+sign+"</sign>");
		sb.append("</xml>");
		String findOrderSta = "";
		String createOrderURL = "https://api.mch.weixin.qq.com/pay/orderquery";
		
		
		findOrderSta = new GetWxOrderno().getOrderInfo(createOrderURL, sb.toString());
		//System.out.println("findOrderSta----------------"+findOrderSta);
		
		
		return findOrderSta;
	}
	
	
	/**
	 * 获取订单信息
	 * @param tpWxPayDto
	 * @return
	 */
	public String ret_chinaPay() throws Exception{
		String ret="";
		
		chinapay.PrivateKey key=new chinapay.PrivateKey();
		chinapay.SecureLink t;
		boolean flag;

		//String MerId, OrdId, TransAmt, CuryId, TransDate, TransType,ChkValue,Status;
		String plainData,ChkValue2;
		flag=key.buildKey("999999999999999",0,AlipayConfig.pubk);
		//flag=key.buildKey("808080201308522",0,"D:\\MerPrk1.key");
		if (flag==false) {		
			//System.out.println("build key error!");
			return "error";
		}
		t=new chinapay.SecureLink (key);
		
		HttpServletRequest req = ServletActionContext.getRequest();


		String Version = req.getParameter("version");
		String MerId = req.getParameter("merid");
		String OrdId = req.getParameter("orderno");
		String TransAmt = req.getParameter("amount");// 12
		String CuryId = req.getParameter("currencycode");// 3
		String TransDate = req.getParameter("transdate");// 8
		String TransType = req.getParameter("transtype");// 4
		String Status = req.getParameter("status");//status
		String BgRetUrl = req.getParameter("BgRetUrl");
		String PageRetUrl = req.getParameter("PageRetUrl");
		String GateId = req.getParameter("GateId");
		String Priv1 = req.getParameter("Priv1");
		String ChkValue = req.getParameter("checkvalue");
		//System.out.println(MerId+OrdId+TransAmt+CuryId+TransDate+TransType+Status+ChkValue);
 
		flag=t.verifyTransResponse(MerId,OrdId, TransAmt, CuryId, TransDate, TransType, Status,ChkValue); // ChkValue为ChinaPay应答传回的域段 

		if (flag!=false){//确认支付成功
			OrdId = "CT"+OrdId.substring(2,12);
			orinfo = orderManager.getOrderStatusByOrderSn(OrdId);
			if (orinfo!=null){
				String sta = orinfo.getOrderStatus();
				Long oid = orinfo.getOrderId();
				String total = addZero(12,orinfo.getTotal());
				//Double to = Double.valueOf(total);
				//Double to_fee = Double.valueOf(total_fee);
				if (total.equals(TransAmt) && sta.equals("0")){
					
					//更新payInterface
					payInterface = new CtPayInterface();
					payInterface.setOrderSn(OrdId);
					payInterface.setTradeNo(OrdId);
					payInterface.setTradeStatus(Status);
					payInterface.setTotalFee(orinfo.getTotal());
					payInterface.setTradeType("4");
					payInterface.setPayDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
					
					if(payManager.findPayByOrderSn(OrdId) == null){
						payManager.save(payInterface);
					}
					
					
					int credit = 0;
					//判断是否存在小数，存在就去掉小数，按整数计算积分
					if(orinfo.getTotal().toString().indexOf(".") > 0){
						String[] chai = orinfo.getTotal().toString().split("\\.");
						credit = Integer.valueOf(chai[0]);
					} else {
						credit = Integer.valueOf(orinfo.getTotal().toString());
					}
					//获取当前用的积分
					CtUser cu = (CtUser) this.session.get("userinfosession");
					
					//发放活动优惠券
					activityCoupon(credit, cu);
					
					if(cu.getUCredit() == null){
						Integer cre = 0;
						credit += cre;
					} else {
						Integer cre = cu.getUCredit();
						credit += cre;
					}
					
					cu.setUCredit(credit);
					orderManager.updateUserCredit(cu);
					orderManager.upChangeStatus(oid, "1");
					orinfo = orderManager.getOrderStatusByOrderSn(OrdId);
					this.request.put("orderInfo", orinfo);
					
					
					orinfo.setPay("4");
					orderManager.saveOrderInfo(orinfo);
					
					insertEJLOrder(orinfo, cu, payManager);
					//发送下单成功短信
					if(cu.getUMb() != null){
						s.paySuccessInfoMess(cu.getUMb(), orinfo.getOrderSn());
					}
					//EDB
					List<CtOrderGoods> orderGoodslist = orderManager.getOrderGoodsByOrderId(orinfo.getOrderId());
					edbTradeAdd(orinfo, orderGoodslist,regionManager,orderManager);
					
				}else{
					session.put("errorMess", "异常提交");
					session.put("errorTotle", "异常提交3");
					return "error";
				}
		
			}else{
				session.put("errorMess", "异常提交");
				session.put("errorTotle", "异常提交2");
				return "error";
			}
			session.put("errorMess", OrdId);
			session.put("errorTotle", "异常提交1");
			//该页面可做页面美工编辑
			return SUCCESS;
		}
		

		return ret;
	}
	
	
	
	//阿里快捷登录获取地址
	public String getAliPayApi() throws MalformedURLException, DocumentException, IOException{
		////////////////////////////////////请求参数//////////////////////////////////////
		
		//目标服务地址
		String target_service = AlipayConfig.target_service;
		//必填
		//必填，页面跳转同步通知页面路径
		String return_url = AlipayConfig.aliLogin_return_url;
		//需http://格式的完整路径，不允许加?id=123这类自定义参数
		
		//防钓鱼时间戳
		String anti_phishing_key = AlipaySubmit.query_timestamp();
		//若要使用请调用类文件submit中的query_timestamp函数
		
		//客户端的IP地址
		InetAddress addr = InetAddress.getLocalHost();
		String exter_invoke_ip = addr.getHostAddress().toString();
		//非局域网的外网IP地址，如：221.0.0.1
		
		
		//////////////////////////////////////////////////////////////////////////////////
		
		//把请求参数打包成数组
		Map<String, String> sParaTemp = new HashMap<String, String>();
		sParaTemp.put("service", "alipay.auth.authorize");
		sParaTemp.put("partner", AlipayConfig.partner);
		sParaTemp.put("_input_charset", AlipayConfig.input_charset);
		sParaTemp.put("target_service", target_service);
		sParaTemp.put("return_url", return_url);
		sParaTemp.put("anti_phishing_key", anti_phishing_key);
		sParaTemp.put("exter_invoke_ip", exter_invoke_ip);
		
		//建立请求
		String sHtmlText = AlipaySubmit.buildRequest(sParaTemp,"get","确认");
		request.put("sHtmlTextAliLogin", sHtmlText);
		return SUCCESS;
	}
	
    public static String addZero(int length, String number) {//数值前补零
    	number = number.replace(".", "");
        String f = "%0" + length + "d";
        return String.format(f, Integer.valueOf(number));
    }
    
    
    
	/*
	 * @Soify
	 * 向EDB推送订单
	 * 
	 */
	public void edbTradeAdd(CtOrderInfo orinfo,List<CtOrderGoods> orderGoodslist,CtRegionManager regionManager,OrderManager orderManager) {

		TreeMap<String, String> apiparamsMap = new TreeMap<String, String>();

		apiparamsMap.put("dbhost", AlipayConfig.dbhost);//添加请求参数——主帐号
		
        apiparamsMap.put("format", AlipayConfig.format);//添加请求参数——返回格式

        apiparamsMap.put("method", "edbTradeAdd");//添加请求参数——接口名称

        apiparamsMap.put("slencry","0");//添加请求参数——返回结果是否加密（0，为不加密 ，1.加密）

		apiparamsMap.put("ip","192.168.60.80");//添加请求参数——IP地址

        apiparamsMap.put("appkey",AlipayConfig.appkey);//添加请求参数——appkey

		apiparamsMap.put("appscret",AlipayConfig.secret);//添加请求参数——appscret

		apiparamsMap.put("token",AlipayConfig.token);//添加请求参数——token

        apiparamsMap.put("v", "2.0");//添加请求参数——版本号（目前只提供2.0版本）

		apiparamsMap.put("fields", "result");

		String timestamp = new SimpleDateFormat("yyyyMMddHHmm")
				.format(new Date());

		apiparamsMap.put("timestamp", timestamp);//添加请求参数——时间戳
		/*shop_name,店铺名称
		 * wangwang 旺旺号
		 * level 用户级别，传入名称
		 * customer_type 客户分组  可以传多个，，以,号分隔
		 * buy_num 交易 次数
		 * order_money 订货总额
		 * custom_1  星座   ----value_1 星座名称
		 * custom_2 生日类型   ----value_2生日名称
		 * custom_3  情感状态   ----value_3 情感名称
		 * custom_4  血型   ----value_4 血型名称
		 * custom_5 生肖   ----value_5 生肖名称
		 */		
		String postdate = "<order>";	
		for(int i=0;i<1;i++)
		{
			postdate		+= "<orderInfo><out_tid>"+orinfo.getOrderSn()+"</out_tid><!-- 外部平台编号-->";
			postdate		+= "<out_order_status>买家已付款，等待卖家发货</out_order_status><!-- 外部订单状态（字符串，随意填写）-->";
			postdate		+= "<storage_id>1</storage_id><!-- 库房ID-->";
			postdate		+= "<processState>"+orinfo.getDsc()+"</processState><!-- 处理状态（全部   未确认   已财务审核 已归档   已确认   已作废   ）-->";
			postdate		+= "<mergeStatus></mergeStatus><!-- 合并状态（合并 手拆 自动拆分 正常 ）-->";
			postdate		+= "<pay_Status>已付款</pay_Status><!-- 付款状态（字符串，随意填写）-->";
			postdate		+= "<shipStatus>未发货</shipStatus><!-- 发货状态（待退货部分退货  待退货全部退货    退货到货部分退货 退货到货全部退货  未发货   已发货）-->";
			postdate		+= "<express>顺丰</express><!-- 快递公司名称-->";
			postdate		+= "<WuLiu_no></WuLiu_no><!--快递单号 -->";
			postdate		+= "<buyer_id>"+orderManager.getUserIdByUid(orinfo.getUId())+"</buyer_id><!-- 买家ID-->";
			postdate		+= "<buyer_msg>"+orinfo.getDsc()+"</buyer_msg><!-- 买家留言-->";
			postdate		+= "<seller_remark></seller_remark><!-- 客服备注-->";
			postdate		+= "<pay_date>"+orinfo.getOrderTime()+"</pay_date><!-- 付款日期-->";
			postdate		+= "<order_type>正常订单</order_type><!-- 订单类型-->";
			postdate		+= "<shop_id>2</shop_id><!-- 店铺代码-->";
			postdate		+= "<orderSource></orderSource><!-- 订单渠道-->";
//			postdate		+= "<express></express><!-- 快递公司名称-->";
			postdate		+= "<payWay>"+orinfo.getPay()+"</payWay><!-- 支付方式-->";
			postdate		+= "<WuLiu></WuLiu><!-- 物流公司（线上物流公司）-->";
			postdate		+= "<in_memo></in_memo><!-- 内部便签-->";
			postdate		+= "<other_remark></other_remark><!-- 其他备注-->";
			postdate		+= "<consignee>"+orinfo.getConsignee()+"</consignee><!-- 收货人-->";

			postdate		+= "<privince>"+regionManager.findById(Long.valueOf(orinfo.getProvince().toString())).getRegionName()+"</privince><!-- 省-->";
			postdate		+= "<city>"+regionManager.findById(Long.valueOf(orinfo.getCity())).getRegionName()+"</city><!-- 市-->";
			postdate		+= "<area>"+regionManager.findById(Long.valueOf(orinfo.getDistrict())).getRegionName()+"</area><!-- 区-->";
			postdate		+= "<address>"+orinfo.getAddress()+"</address><!-- 收货地址 -->";
			postdate		+= "<product_totalMoney>"+orinfo.getTotal()+"</product_totalMoney><!-- 产品总金额-->";
			postdate		+= "<order_totalMoney>"+orinfo.getTotal()+"</order_totalMoney><!-- 订单总金额-->";
			postdate		+= "<actual_RP>"+orinfo.getTotal()+"</actual_RP><!-- 实收金额-->";
			postdate		+= "<actual_freight_get>"+orinfo.getFreight()+"</actual_freight_get><!--实收运费-->";
			postdate		+= "<telephone>"+orinfo.getTel()+"</telephone><!-- 电话-->";
			postdate		+= "<mobilPhone>"+orinfo.getTel()+"</mobilPhone><!-- 手机-->";
			postdate		+= "<buyer_email></buyer_email><!-- 邮件-->";
			postdate		+= "<order_date>"+orinfo.getOrderTime()+"</order_date><!-- 订货日期-->";
			postdate		+= "<invoice_title></invoice_title><!--发票抬头 -->";
			postdate		+= "<invoice_type></invoice_type><!--发票类型 -->";
			postdate		+= "<invoice_money></invoice_money><!--发票金额 -->";
			postdate		+= "<invoice_msg></invoice_msg><!--开票内容 -->";
			postdate		+= "<is_needInvoice></is_needInvoice><!--是否需要开发票（0：否 1：是）默认：0 -->";
			postdate		+= "<is_invoiceOpened></is_invoiceOpened><!-- 是否已打印发票（0：否 1：是）-->";
			postdate		+= "<is_COD></is_COD><!--是否货到付款（0：否 1：是）默认：0 -->";
			postdate		+= "<serverCost_COD></serverCost_COD><!--货到付款服务费 -->";
			postdate		+= "<is_scorePay></is_scorePay><!--是否积分换购（0：否 1：是）默认：0 -->";
			postdate		+= "<pay_score></pay_score><!-- 支付积分-->";
			postdate		+= "<totalPackage></totalPackage><!-- 总件数-->";
			postdate		+= "<totalCount></totalCount><!-- 总条数-->";
			postdate		+= "<product_info>";
			
			
			for(int j=0;j< orderGoodslist.size();j++)
			{
				CtOrderGoods cg = new CtOrderGoods();
				cg = orderGoodslist.get(j);
				String price = "";
				String gnum = "";
				if (cg.getGPrice() !=null){
					price = cg.getGPrice();
					gnum = cg.getGNumber();
				}
				else{
					price = cg.getGParPrice();
					gnum = cg.getGParNumber().toString();
				}
				postdate		+= "<product_item>";
				postdate		+= "<barCode>"+ cg.getCtGoods().getGSn() +"</barCode><!--条形码 -->";
				postdate		+= "<product_title>"+ cg.getCtGoods().getGName() + "</product_title><!-- 网店品名-->";
				postdate		+= "<standard>"+cg.getCtGoods().getSeries()+"</standard><!-- 网店规格-->";
				postdate		+= "<out_price>"+price+"</out_price><!-- 销售价-->";
				postdate		+= "<favorite_money>0</favorite_money><!-- 优惠金额-->";
				postdate		+= "<orderGoods_Num>"+gnum+"</orderGoods_Num><!-- 订货数量-->";
				postdate		+= "<gift_Num></gift_Num><!--赠品数量-->";
				postdate		+= "<cost_Price>"+price+"</cost_Price><!--成交单价-->";
				postdate		+= "<product_stockout>0</product_stockout><!--缺货状态（0：否 1：是）默认：0-->";
				postdate		+= "<is_Book></is_Book><!--是否预订（0：否 1：是）默认：0-->";
				postdate		+= "<is_presell></is_presell><!--是否预售-->";
				postdate		+= "<is_Gift></is_Gift><!--是否赠品（0：否 1：是）默认：0-->";
				postdate		+= "<avg_price></avg_price><!--加权平均单价-->";
				postdate		+= "<product_freight></product_freight><!--产品运费-->";
				postdate		+= "<shop_id>1</shop_id><!--店铺编号-->";
				postdate		+= "<out_tid>"+orinfo.getOrderSn()+"</out_tid><!--外部平台单号-->";
				postdate		+= "<out_productId>"+cg.getGId()+"</out_productId><!--外部平台产品Id-->";
				postdate		+= "<out_barCode>"+cg.getCtGoods().getGSn()+"</out_barCode><!--外部平台条形码-->";
				postdate		+= "<product_intro></product_intro><!--产品简介-->";
				postdate		+= "</product_item>";
			}
			
			postdate		+= "</product_info>";
			postdate		+= "</orderInfo>";								
		}				
		postdate		+= "</order>";
		java.text.DateFormat df=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String st = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
		.format(new Date());	
		apiparamsMap.put("xmlValues",postdate);
		//获取数字签名
		String sign = Util.md5Signature(apiparamsMap, AlipayConfig.appkey);

		apiparamsMap.put("sign", sign);

		StringBuilder param = new StringBuilder();

		for (Iterator<Map.Entry<String, String>> it = apiparamsMap.entrySet()
				.iterator(); it.hasNext();) {
			Map.Entry<String, String> e = it.next();
			if(e.getKey()!="appscret" && e.getKey()!="token")
			{	
				if(e.getKey()=="xmlValues")
				{
					try {
						param.append("&").append(e.getKey()).append("=").append(Util.encodeUri(e.getValue()));
					} catch (UnsupportedEncodingException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				else
				{
					param.append("&").append(e.getKey()).append("=").append(e.getValue());
				}
			}
		}
		String PostData="";
		PostData=param.toString().substring(1);
		System.out.println(AlipayConfig.testUrl+"?"+PostData);
		String result="";
		result=Util.getResult(AlipayConfig.testUrl,PostData);
		System.out.println(result);
		
		String et = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
		.format(new Date());
		try
		{
		java.util.Date begin=df.parse(st);
		java.util.Date end = df.parse(et);
		System.out.println(st);
		System.out.println(et);
		long between=(end.getTime()-begin.getTime())/1000;
		System.out.println(between);
		}catch(java.text.ParseException e){
		System.err.println("格式不正确");
		}
	}
	//更新支付方式
	public String updatePayZhuang(){
		String type = payDTO.getPayType();
		orderManager.updatePayByOrder(payDTO.getOut_trade_no(),type);
		return SUCCESS;
	}
	
private String code;
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	private String wxName;
	
	public String getWxName() {
		return wxName;
	}

	public void setWxName(String wxName) {
		this.wxName = wxName;
	}

	public String gotupay(){
		try {
			String url1 = "https://api.weixin.qq.com/sns/oauth2/access_token?appid="+AlipayConfig.appid+"&secret="
					+AlipayConfig.appsecret+"&code="+code+"&grant_type=authorization_code";
			
			HttpPost httpost= org.ctonline.util.http.HttpClientConnectionManager.getPostMethod(url1);
			HttpResponse response = GetWxOrderno.httpclient.execute(httpost);
			String jsonStr = EntityUtils.toString(response.getEntity(), "UTF-8");
			org.ctonline.weibo4j.org.json.JSONObject map1 = new org.ctonline.weibo4j.org.json.JSONObject(jsonStr);  
			String openid = (String) map1.get("openid");
			//String openid = "o7ihfs9f88SD4fMZscap8-Yi-l8Q";
			session.put("openId", openid);
			
			wx = orderManager.findPayWxByOpenId(openid);
			if(wx != null ){
				wxName = wx.getPayPayname();
			} else {
				wxName = "";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public String pay() throws Exception{
		//code = requ.getParameter("code");
		//code = (String) params1.get("code");  
//		if(code == null){
//			//result = code;
//			result = requ.getRequestURL().toString();
//			return SUCCESS;
//		}
		
		
		//HttpServletRequest req1 = ServletActionContext.getRequest();
		//String query = req1.getQueryString();
		//String u = URLEncoder.encode(AlipayConfig.payGetOpenId, "UTF-8");
		//String[] fen = query.split("&");
		//code = (String) req1.getAttribute("code");
		//code = fen[0].substring(5);
		//获取用户信息 得到code
		//String url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxdcdffa64194cf0f7&redirect_uri="+u+"&response_type=code&scope=snsapi_base#wechat_redirect";
		//URL ur = new URL(url);
		//String url11 ="https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx20d092d7dac249d5&redirect_uri=http://www.ctego.com/weixinpaysc/gotupay&response_type=code&scope=snsapi_base#wechat_redirect";
		
		
		//String url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid="+AlipayConfig.weixinAppId+"&secret="
		//		+AlipayConfig.weixinAppSecret+"&code="+code+"&grant_type=authorization_code";
		//code = "011fMrng1djeKk0xeApg1Sonng1fMrnX";
//		if(code == null){
//			result = req1+"_________"+query + "___________" + requ + "__________" +requ.getQueryString();
//			return SUCCESS;
//		}
//		String url1 = "https://api.weixin.qq.com/sns/oauth2/access_token?appid="+AlipayConfig.appid+"&secret="
//				+AlipayConfig.appsecret+"&code="+code+"&grant_type=authorization_code";
//		
//		HttpPost httpost= org.ctonline.util.http.HttpClientConnectionManager.getPostMethod(url1);
//		HttpResponse response = GetWxOrderno.httpclient.execute(httpost);
//		String jsonStr = EntityUtils.toString(response.getEntity(), "UTF-8");
////		httpost = HttpClientConnectionManager.getPostMethod(url1);
////		response = GetWxOrderno.httpclient.execute(httpost);
////		jsonStr = EntityUtils.toString(response.getEntity(), "UTF-8");
//		org.ctonline.weibo4j.org.json.JSONObject map1 = new org.ctonline.weibo4j.org.json.JSONObject(jsonStr);  
		//String access_token = (String) map1.get("access_token");
		//String access_token = "ax4g58r2hW1sh6K69d_K4AuFMoe_zXwM7NuBiL3iB1ShXEMG-4So8mZQ6uLIaFeClu3CTL7yAhdxkDqIETCYG465Lu_rzLYqo_7ZDd52MO4LLKhABAHHG";
		String openid = (String) session.get("openId");
		//openid = "o7ihfs9f88SD4fMZscap8-Yi-l8Q";
		WxPayDto wxPayDto = new WxPayDto();
		wxPayDto.setBody("长亭易购订单");
		wxPayDto.setNotifyUrl(AlipayConfig.notifyurl);
		wxPayDto.setOrderId("asasasasa");
		HttpServletRequest req = ServletActionContext.getRequest();
		String Ip =UtilDate.getIpAddr(req);
		wxPayDto.setSpbillCreateIp(Ip);
		//wxPayDto.setTotalFee(Double.valueOf(orderDTO.getTotal())*100);
		String url = "";
		// 1 参数
			Date dt = new Date();
			Long time = dt.getTime();
			Long osn = time * new Random().nextInt(9999);
			String orderSN = "CT"
					+ osn.toString().substring(osn.toString().length() - 10,
							osn.toString().length());
		
				// 订单号
				String orderId = orderSN;
				session.put("orderSNByPay", orderSN);
				// 附加数据 原样返回
				String attach = "";
				// 总金额以分为单位，不带小数点
				String totalFee = getMoney(orderDTO.getTotal());
				session.put("totalFeeByPay", totalFee);
				
				session.put("payName", orderDTO.getPayName());
				
				// 订单生成的机器 IP
				String spbill_create_ip = Ip;
				// 这里notify_url是 支付完成后微信发给该链接信息，可以判断会员是否支付成功，改变订单状态等。
				String notify_url = AlipayConfig.notifyurl;
				String trade_type = "JSAPI";
				
				// 商户号
				String mch_id = AlipayConfig.partner_Weixin;
				// 随机字符串
				String nonce_str = getNonceStr();

				// 商品描述根据情况修改
				String body = "货款支付";

				// 商户订单号
				String out_trade_no = orderId;

				SortedMap<String, String> packageParams = new TreeMap<String, String>();
				packageParams.put("appid", AlipayConfig.appid);
				packageParams.put("mch_id", mch_id);
				packageParams.put("nonce_str", nonce_str);
				packageParams.put("body", body);
				packageParams.put("attach", attach);
				packageParams.put("out_trade_no", out_trade_no);

				// 这里写的金额为1 分到时修改
				packageParams.put("total_fee", totalFee);
				packageParams.put("spbill_create_ip", spbill_create_ip);
				packageParams.put("notify_url", notify_url);

				packageParams.put("trade_type", trade_type);
				packageParams.put("openid", openid);
				org.ctonline.util.RequestHandler reqHandler = new org.ctonline.util.RequestHandler(null, null);
				reqHandler.init(AlipayConfig.appid, AlipayConfig.appsecret, AlipayConfig.partnerkey);

				String sign = reqHandler.createSign(packageParams);
				packageParams.put("sign", sign);
				String reqXml = PayCommonUtil.getRequestXml(packageParams);
//				//XMLUtil.getChildrenText(children)
//				String xml = "<xml>" + "<appid>" + AlipayConfig.appid + "</appid>" + "<mch_id>"
//						+ mch_id + "</mch_id>" + "<nonce_str>" + nonce_str
//						+ "</nonce_str>" + "<sign>" + sign + "</sign>"
//						+ "<body><![CDATA[" + body + "]]></body>" 
//						+ "<out_trade_no>" + out_trade_no
//						+ "</out_trade_no>" + "<attach>" + attach + "</attach>"
//						+ "<total_fee>" + totalFee + "</total_fee>"
//						+ "<spbill_create_ip>" + spbill_create_ip
//						+ "</spbill_create_ip>" + "<notify_url>" + notify_url
//						+ "</notify_url>" + "<trade_type>" + trade_type
//						+ "</trade_type>" + "</xml>";
				String code_url = "";
				String createOrderURL = "https://api.mch.weixin.qq.com/pay/unifiedorder";
				
				Map<String, String> map = new GetWxOrderno().getXml(createOrderURL, reqXml);
				if(map != null){
					SortedMap<String, String> params = new TreeMap<String, String>();
					session.put("prepayByPay", map.get("prepay_id"));
			        params.put("appId", AlipayConfig.appid);
			        params.put("timeStamp", Long.toString(new Date().getTime()));
			        params.put("nonceStr", PayCommonUtil.CreateNoncestr());
			        params.put("package", "prepay_id="+map.get("prepay_id"));
			        params.put("signType", "JSAPI");
			        String paySign =  reqHandler.createSign(params);
			        params.put("packageValue", "prepay_id="+map.get("prepay_id"));    //这里用packageValue是预防package是关键字在js获取值出错
			        params.put("paySign", paySign);                                                          //paySign的生成规则和Sign的生成规则一致
			        params.put("sendUrl", AlipayConfig.paySuccessUrl);                               //付款成功后跳转的页面
			        String userAgent = requ.getHeader("user-agent");
			        char agent = userAgent.charAt(userAgent.indexOf("MicroMessenger")+15);
			        System.out.println(map.get("prepay_id"));
			        reqHandler = new org.ctonline.util.RequestHandler(null, null);
					reqHandler.init(AlipayConfig.appid, AlipayConfig.appsecret, AlipayConfig.partnerkey);
			        params.put("agent", new String(new char[]{agent}));//微信版本号，用于前面提到的判断用户手机微信的版本是否是5.0以上版本。
			        String json = JSONObject.fromObject(params).toString();
					result = json;
					return SUCCESS;
					
				} else {
					return ERROR;
				}
	}
	private CtPaymentWx wx = new CtPaymentWx();
	
	public CtPaymentWx getWx() {
		return wx;
	}

	public void setWx(CtPaymentWx wx) {
		this.wx = wx;
	}

	public String paySuccess(){
			 try {
				//HttpServletRequest request = ServletActionContext.getRequest();
				    HttpServletResponse response = ServletActionContext.getResponse();
				    //InputStream inStream = request.getInputStream();
				
				    response.getWriter().write(PayCommonUtil.setXML("SUCCESS", ""));
				    
				    wx = orderManager.getWxPayByOrderSn((String) session.get("orderSNByPay"));
				    if(wx != null){
				    	return SUCCESS;
				    } 
				    wx = new CtPaymentWx();
				    Double price = (double) (Double.valueOf((String)session.get("totalFeeByPay")) / 100);
				    wx.setPayDatatime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				    wx.setPayOrdersn((String) session.get("orderSNByPay"));
				    wx.setPayPayname((String) session.get("payName"));
				    wx.setPayPaytotal(price.toString());
				    wx.setPayPrepayid((String) session.get("prepayByPay"));
				    wx.setPayOpenId((String) session.get("openId"));
				    orderManager.saveWxPay(wx);
				    //发送短信到指定手机号
				    serviceInterface.paySuccessWxNew("17722639820",
				    		(String) session.get("payName"),
				    		new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()),
				    		price.toString());
//				    ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
//				    byte[] buffer = new byte[1024];
//				    int len = 0;
//				    while ((len = inStream.read(buffer)) != -1) {
//				        outSteam.write(buffer, 0, len);
//				    }
//				    System.out.println("~~~~~~~~~~~~~~~~付款成功~~~~~~~~~");
//				    outSteam.close();
//				    inStream.close();
//				    String result  = new String(outSteam.toByteArray(),"utf-8");//获取微信调用我们notify_url的返回信息
//				    Map<Object, Object> map = XMLUtil.doXMLParse(result);
//				    for(Object keyValue : map.keySet()){
//				        System.out.println(keyValue+"="+map.get(keyValue));
//				    }
//				    if (map.get("result_code").toString().equalsIgnoreCase("SUCCESS")) {
//				        //TODO 对数据库的操作
//				        response.getWriter().write(PayCommonUtil.setXML("SUCCESS", ""));   //告诉微信服务器，我收到信息了，不要在调用回调action了
//				        System.out.println("-------------"+PayCommonUtil.setXML("SUCCESS", ""));
//				    }
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		return SUCCESS;
	}
	public static String setXML(String return_code, String return_msg) {
		  return "<xml><return_code><![CDATA[" + return_code
				  + "]]></return_code><return_msg><![CDATA[" + return_msg
				  + "]]></return_msg></xml>";
	}
}

package org.ctonline.action.goods;

import java.io.File;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.ctonline.action.BaseAction;
import org.ctonline.common.Solr;
import org.ctonline.dto.goods.CtBomDTO;
import org.ctonline.dto.goods.CtBomGoodsDTO;
import org.ctonline.manager.basic.BasicManager;
import org.ctonline.manager.goods.CtBomManager;
import org.ctonline.manager.goods.F_GoodsManager;
import org.ctonline.manager.order.OrderManager;
import org.ctonline.po.basic.CtSystemInfo;
import org.ctonline.po.goods.CtBom;
import org.ctonline.po.goods.CtBomGoods;
import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.po.goods.CtGoodsImg;
import org.ctonline.po.goods.CtGroupPrice;
import org.ctonline.po.goods.CtRangePrice;
import org.ctonline.po.order.CtCart;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.views.ViewCateNum;
import org.ctonline.po.views.ViewGoodsList;
import org.ctonline.po.views.ViewSubcateNum;
import org.ctonline.util.CsvExUtil;
import org.ctonline.util.Page;

import com.csvreader.CsvReader;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ModelDriven;

public class CtBomAction extends BaseAction implements RequestAware, ModelDriven<Object>,SessionAware{

	private Map<String, Object> request;
	private CtBomDTO bomDTO = new CtBomDTO();
	private CtBom bom;
	private CtBomManager bomManager;
	private BasicManager basicManager;
	private OrderManager orderManager;
	private CtGoods goods;
	private F_GoodsManager fgoodsManager;
	private CtBomGoods bomGoods;
	private List<CtRangePrice> rangePricesList = new ArrayList<CtRangePrice>();
	public OrderManager getOrderManager() {
		return orderManager;
	}

	public CtGoods getGoods() {
		return goods;
	}

	public void setGoods(CtGoods goods) {
		this.goods = goods;
	}

	public F_GoodsManager getFgoodsManager() {
		return fgoodsManager;
	}

	public void setFgoodsManager(F_GoodsManager fgoodsManager) {
		this.fgoodsManager = fgoodsManager;
	}

	public void setOrderManager(OrderManager orderManager) {
		this.orderManager = orderManager;
	}
	private Page page;
	private List<CtBom> bomList;
	private List<CtBomGoods> bomGoodsList;
	private CtBomGoodsDTO bomGoodsDTO = new CtBomGoodsDTO();
	private String result;
	private JSONArray resultJson;
	private Map<String, Object> session;
	private List<CtGoodsCategory> cateList;
	private List<ViewCateNum> catenumList;
	private List<ViewSubcateNum> subcatenumList;
	


	public List<ViewCateNum> getCatenumList() {
		return catenumList;
	}

	public void setCatenumList(List<ViewCateNum> catenumList) {
		this.catenumList = catenumList;
	}

	public List<ViewSubcateNum> getSubcatenumList() {
		return subcatenumList;
	}

	public void setSubcatenumList(List<ViewSubcateNum> subcatenumList) {
		this.subcatenumList = subcatenumList;
	}

	public List<CtGoodsCategory> getCateList() {
		return cateList;
	}

	public void setCateList(List<CtGoodsCategory> cateList) {
		this.cateList = cateList;
	}

	//bom列表
	public String list(){
		try {
			this.cateList = basicManager.queryAllCate();
			this.catenumList = basicManager.cateCount();
			this.subcatenumList = basicManager.subcateCount();
			page=new Page();
			if (bomDTO.getPage()==0){
				bomDTO.setPage(1);
			}
			session.put("cateList", cateList);
			session.put("catenumList", catenumList);
			session.put("subcatenumList", subcatenumList);
			page.setCurrentPage(bomDTO.getPage());
			this.bomList = bomManager.loadAll(page);
			for (CtBom b : bomList) {
				Integer c = this.bomManager.countBom(b.getBomId());
				b.setCount(c);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "list";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	
	//Bom中心列表
	public String hotList(){
		try {
			page=new Page();
			if (bomDTO.getPage()==0){
				bomDTO.setPage(1);
			}
			page.setCurrentPage(bomDTO.getPage());
			if(this.session.get("userinfosession")==null){
				bomList = bomManager.loadAllHot(page);
			}else {
				CtUser cu = (CtUser)session.get("userinfosession");
				this.bomList = bomManager.loadAllHotLogin(page);
				List<CtBom> boms = bomManager.getCtBomsByUId(cu.getUId());
				for(int j=0;j<boms.size();j++){
					for(int i=0;i<bomList.size();i++){
						if(bomList.get(i).getBomTitle().equals(boms.get(j).getBomTitle())){
							bomList.remove(i);
						}
					}
				}
			}
			for(int i=0;i<bomList.size();i++){
				if(bomList.get(i).getNum() == null){
					bomList.get(i).setNum(0);
				}
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "hotList";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	
	//新增bom
	public String add(){
		try {
			return "add";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	
	//保存bom
	public String saveAdd(){
		List<String> l = new ArrayList<String>();
		try {
			Map session = ActionContext.getContext().getSession();
			CtUser cu = (CtUser)session.get("userinfosession");
			Long UId = cu.getUId();
			String bomTitle = this.bomDTO.getBomTitle();
			String bomDesc = this.bomDTO.getBomDesc();
			Date date = new Date();
			SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			CtBom cb = new CtBom();
			cb.setBomTitle(bomTitle);
			cb.setBomDesc(bomDesc);
			cb.setBomTime(siFormat.format(date));
			cb.setUId(UId);
			cb.setIsHot("0");
			cb.setNum(1);
			String res = this.bomManager.save(cb);
			if(res.equals("Exerror")){
				String url = bomDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
			}
			if(res != null){
				l.add("success");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;	
			}else {
				l.add("error");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;
			}
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			l.add("error");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		}
	}
	
	//修改Bom
	public String editBom(){
		List<String> l = new ArrayList<String>();
		try {
			//获取登录的用户ID
			Long uid = null;
			if(this.session.get("userinfosession")!=null){
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
			}
			Integer[] bomId = this.bomDTO.getMid();
			CtBom bom = this.bomManager.getCtBomByBomId(bomId[0], uid);
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(bom);
			resultJson = json;
			return SUCCESS;	
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			l.add("error");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		}
	}
	//保存修改Bom
	public String saveUpdate(){
		List<String> l = new ArrayList<String>();
		try {
			//获取登录的用户ID
			Long uid = null;
			if(this.session.get("userinfosession")!=null){
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
			}
			Integer bomId = this.bomDTO.getBomId();
			String bomTitle = this.bomDTO.getBomTitle();
			String bomDesc = this.bomDTO.getBomDesc();
			CtBom cb = this.bomManager.getCtBomByBomId(bomId, uid);
			cb.setBomTitle(bomTitle);
			cb.setBomDesc(bomDesc);
			String res = this.bomManager.update(cb);
			if(res.equals("Exerror")){
				String url = bomDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
			}
			l.add("success");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;	
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			l.add("error");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		}
	}
	//删除Bom
	public String del(){
		try {
			for(Integer b:bomDTO.getMid()){
				this.bomManager.delBomGoodsByBomId(b);
				this.bomManager.delete(b);
			}
			return "del";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	//搜素
	public String search(){
		if(bomDTO.getKeyword() != null){
			Page page=new Page();
			if (bomDTO.getPage()==0){
				bomDTO.setPage(1);
			}
			page.setCurrentPage(bomDTO.getPage());
			if (!"".equals(bomDTO.getKeyword())){
				this.bomList= bomManager.findAll(bomDTO.getKeyword(),page);
			}else{
				this.bomList= bomManager.loadAll(page);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "search";
		}else {
			return "golist";
		}
		
	}
	//检索bom是否有商品
	public String getBomIsGoods(){
		Integer bomId = bomDTO.getBomId();
		bomGoodsList = bomManager.getCtBomGoodsByBomId(bomId);
		if(bomGoodsList != null && bomGoodsList.size() > 0){
			result = "yes";
			return SUCCESS;
		} else {
			result = "您的bom中还么有添加商品呢，请添加后尝试";
		}
		return "goOrderChear";
	}
	
	//搜索Bom中心
	public String searchBC(){
		try {
			Page page=new Page();
			if (bomDTO.getPage()==0){
				bomDTO.setPage(1);
			}
			page.setCurrentPage(bomDTO.getPage());
			if(this.session.get("userinfosession")==null){
				if (!"".equals(bomDTO.getKeyword())){
					this.bomList = bomManager.findByKeyWord(bomDTO.getKeyword(), page);
				}else {
					this.bomList= bomManager.loadAllHot(page);
				}
			}else {
				if (!"".equals(bomDTO.getKeyword())){
					CtUser cu = (CtUser)session.get("userinfosession");
					this.bomList = this.bomManager.findByKeyWordLogin(bomDTO.getKeyword(), page);
					List<CtBom> boms = bomManager.getCtBomsByUId(cu.getUId());
					for(int j=0;j<boms.size();j++){
						for(int i=0;i<bomList.size();i++){
							if(bomList.get(i).getBomTitle().equals(boms.get(j).getBomTitle())){
								bomList.remove(i);
							}
						}
					}
				}else {
					CtUser cu = (CtUser)session.get("userinfosession");
					this.bomList = bomManager.loadAllHotLogin(page);
					List<CtBom> boms = bomManager.getCtBomsByUId(cu.getUId());
					for(int j=0;j<boms.size();j++){
						for(int i=0;i<bomList.size();i++){
							if(bomList.get(i).getBomTitle().equals(boms.get(j).getBomTitle())){
								bomList.remove(i);
							}
						}
					}
				}
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "searchBC";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	
	//复制Bom
	public String copyBom(){
		try {
			//获取登录的用户ID
			Long uid = null;
			if(this.session.get("userinfosession")!=null){
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
			}
			for(Integer b:bomDTO.getMid()){
				//this.bomManager.delete(b);
				CtBom cb = this.bomManager.getCtBomByBomId(b,uid);
				CtBom cbcopy = new CtBom();
				String bomTitleCopy = cb.getBomTitle()+"副本";
				Date date = new Date();
				SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				cbcopy.setBomTitle(bomTitleCopy);
				cbcopy.setBomTime(siFormat.format(date));
				cbcopy.setBomDesc(cb.getBomDesc());
				cbcopy.setUId(cb.getUId());
				cbcopy.setIsHot(cb.getIsHot());
				cbcopy.setNum(1);
				String res=this.bomManager.save(cbcopy);
				if(res.equals("Exerror")){
					String url = bomDTO.getUrl();
					//System.out.println(url);
					String errorTotle = "抱歉，提交异常";
					String errorMess = "含非法字符";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", url);
					return ERROR;
				}
				//复制相应的子表数据
				CtBom cb2 = this.bomManager.getCtBomByBomTitle(bomTitleCopy,"");
				this.bomGoodsList = bomManager.getCtBomGoodsByBomId(cb.getBomId());
				for(int i =0 ;i<bomGoodsList.size();i++){
					CtBomGoods cbg = new CtBomGoods();
					cbg.setBomId(cb2.getBomId());
					cbg.setBomGoodsId(bomGoodsList.get(i).getBomGoodsId());
					cbg.setGId(bomGoodsList.get(i).getGId());
					cbg.setGoodsNum(bomGoodsList.get(i).getGoodsNum());
					cbg.setPack(bomGoodsList.get(i).getPack());
					String re=this.bomManager.saveCtBomGoods(cbg);
					if(re.equals("Exerror")){
						String url = bomDTO.getUrl();
						//System.out.println(url);
						String errorTotle = "抱歉，提交异常";
						String errorMess = "含非法字符";
						session.put("errorMess", errorMess);
						session.put("errorTotle", errorTotle);
						session.put("errorBackUrl", url);
						return ERROR;
					}
				}
			}
			return "copyBom";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	
	//加入我的Bom
	public String addToMyBom(){
		try {
			//获取登录的用户ID
			Long uid = null;
			if(this.session.get("userinfosession")!=null){
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
			}
			Integer bomId = this.bomDTO.getBomId();
			this.bom = this.bomManager.getCtBomByBomId(bomId,uid);
			bom.setNum(bom.getNum()+1);
			this.bomManager.update(bom);
			CtUser cu = (CtUser)session.get("userinfosession");
			Date date = new Date();
			SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			bom.setUId(cu.getUId());
			bom.setBomTime(siFormat.format(date));
			String res=this.bomManager.save(bom);
			if(res.equals("Exerror")){
				String url = bomDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
			}
			//复制相应的子表数据
			this.bomList = this.bomManager.getCtBomsByUId(cu.getUId());
			CtBom cb2 = bomList.get(0);
//			CtBom cb2 = this.bomManager.getCtBomByBomId(bomId);
			this.bomGoodsList = bomManager.getCtBomGoodsByBomId(bomId);
			for(int i =0 ;i<bomGoodsList.size();i++){
				CtBomGoods cbg = new CtBomGoods();
				cbg.setBomId(cb2.getBomId());
				cbg.setBomGoodsId(bomGoodsList.get(i).getBomGoodsId());
				cbg.setGId(bomGoodsList.get(i).getGId());
				cbg.setGoodsNum(bomGoodsList.get(i).getGoodsNum());
				cbg.setPack(bomGoodsList.get(i).getPack());
				String re=this.bomManager.saveCtBomGoods(cbg);
				if(re.equals("Exerror")){
					String url = bomDTO.getUrl();
					//System.out.println(url);
					String errorTotle = "抱歉，提交异常";
					String errorMess = "含非法字符";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", url);
					return ERROR;
				}
			}
			return "addToMyBom";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	private List<CtGoodsImg> imgList;
	
	public List<CtGoodsImg> getImgList() {
		return imgList;
	}

	public void setImgList(List<CtGoodsImg> imgList) {
		this.imgList = imgList;
	}

	//Bom详细
	public String goBomDetail(){
		try {
			//获取登录的用户ID
			Long uid = null;
			if(this.session.get("userinfosession")!=null){
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
			}
			CtSystemInfo systemInfo = new CtSystemInfo();
			if(bomDTO.getBomId() != null){
				Integer bomId = this.bomDTO.getBomId();
				this.bom = this.bomManager.getCtBomByBomId(bomId, uid);
				page=new Page();
				if (bomGoodsDTO.getPage()==0){
					bomGoodsDTO.setPage(1);
				} 
				page.setCurrentPage(bomGoodsDTO.getPage());
				this.bomGoodsList = bomManager.loadBomDetailAll(page,bomId);
				
				//取价
				if(bomGoodsList.size()>0){
					
					//循环查询结果替换价格查询结果
					for(int i=0;i<bomGoodsList.size();i++){
						if(bomGoodsList.get(i).getGoodsNum() != -1){
							Long gid = bomGoodsList.get(i).getGId();
							String price = this.basicManager.getPrice(uid, gid);
							if(price!=null){
								bomGoodsList.get(i).getGoods().setPromotePrice(Double.parseDouble(price));
							}
							this.imgList = fgoodsManager.getImg(bomGoodsList.get(i).getGoods().getGId());
							//System.out.println(imgList==null);
							if (imgList!=null && imgList.size()!=0){
								bomGoodsList.get(i).setImgUrl(systemInfo.getCtImgUrl() +"//"+imgList.get(3).getPUrl());
							}else{
								bomGoodsList.get(i).setImgUrl(systemInfo.getCtImgUrl() +"//ct_s.gif");
							}
						} else {
							
						}
					}
				}
				page.setTotalPage(page.getTotalPage());
				this.request.put("pages", page);
				return "goBomDetail";
			}else {
				return "golist";
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}
	
	//Bom中心详细
	public String goBomCenterDetail(){
		try {
			
			//获取登录的用户ID
			Long uid = null;
			if(this.session.get("userinfosession")!=null){
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
			}
			Integer bomId = this.bomDTO.getBomId();
			this.bom = this.bomManager.getCtBomByBomId(bomId, uid);
			page=new Page();
			if (bomGoodsDTO.getPage()==0){
				bomGoodsDTO.setPage(1);
			}
			page.setCurrentPage(bomGoodsDTO.getPage());
			this.bomGoodsList = bomManager.loadBomDetailAll(page,bomId);
			
			//取价
			if(bomGoodsList.size()>0){
				//循环查询结果替换价格查询结果
				for(int i=0;i<bomGoodsList.size();i++){
					Long gid = bomGoodsList.get(i).getGId();
					String price = this.basicManager.getPrice(uid, gid);
					if(price!=null){
						bomGoodsList.get(i).getGoods().setPromotePrice(Double.parseDouble(price));
					}
				}
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "goBomCenterDetail";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	
	//删除Bom详情中的某个商品
	public String delGoodsOfBomDetail(){
		try {
			//获取登录的用户ID
			Long uid = null;
			if(this.session.get("userinfosession")!=null){
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
			}
			Integer bomId = this.bomDTO.getBomId();
			this.bom = this.bomManager.getCtBomByBomId(bomId, uid);
			for(Long b:bomGoodsDTO.getMid()){
				this.bomManager.delGoodsOfBomDetail(b);
			}
			page=new Page();
			if (bomGoodsDTO.getPage()==0){
				bomGoodsDTO.setPage(1);
			}
//			page.setCurrentPage(bomGoodsDTO.getPage());
//			this.bomGoodsList = bomManager.loadBomDetailAll(page,bomId);
//			page.setTotalPage(page.getTotalPage());
//			this.request.put("pages", page);
			return "delGoodsOfBomDetail";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
	}
	//搜索Bom详情中的商品
	public String searchGoodsOfBomDetail(){
		try {
			//获取登录的用户ID
			Long uid = null;
			if(this.session.get("userinfosession")!=null){
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
			}
			Integer bomId = this.bomGoodsDTO.getBomId();
			this.bom = this.bomManager.getCtBomByBomId(bomId, uid);
			Page page=new Page();
			if (bomGoodsDTO.getPage()==0){
				bomGoodsDTO.setPage(1);
			}
			page.setCurrentPage(bomGoodsDTO.getPage());
			if (!"".equals(bomGoodsDTO.getKeyword()) && !"订单编号/收货人/商品编号/商品名称".equals(bomGoodsDTO.getKeyword())){
				this.bomGoodsList= bomManager.findBomDetailAll(bomGoodsDTO.getKeyword(),page,bomId);
			}else{
				this.bomGoodsList= bomManager.loadBomDetailAll(page,bomId);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "searchGoodsOfBomDetail";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	
	//保存Pack
	public String savePack(){
		try {
			String pack = this.bomGoodsDTO.getPack();
			Long bomGoodsId = this.bomGoodsDTO.getBomGoodsId();
			CtBomGoods cbg = this.bomManager.getCtBomGoodsBybomGoodsId(bomGoodsId);
			cbg.setPack(pack);
			this.bomManager.updateCtBomGoods(cbg);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	
	
	//更新bom详情商品数量
	public String updateGoodsNumNew(){
		String type = bomGoodsDTO.getUpdateType();
		rangePricesList = basicManager.getAllRanPrice(bomGoodsDTO.getBomGoodsId());
		Integer newSum = 0;
		if(type.equals("add")){
			boolean isok = false;
			for (int i = 0; i < rangePricesList.size(); i++) {
				if((bomGoodsDTO.getGoodsNum() >= rangePricesList.get(i).getSimSNum()) && (bomGoodsDTO.getGoodsNum() <= rangePricesList.get(i).getSimENum())){
					newSum = bomGoodsDTO.getGoodsNum().intValue() + rangePricesList.get(i).getSimIncrease();
					isok = true;
				}
				if(bomGoodsDTO.getGoodsNum() < rangePricesList.get(0).getSimSNum()){
					newSum =  rangePricesList.get(0).getSimSNum();
					isok = true;
					break;
				}
				if(((i+1) <= rangePricesList.size() ) && (bomGoodsDTO.getGoodsNum() > rangePricesList.get(i).getSimENum()) && (bomGoodsDTO.getGoodsNum() < rangePricesList.get(i+1).getSimSNum()) ){
					newSum = rangePricesList.get(i+1).getSimSNum();
					isok = true;
				}
				if(newSum > rangePricesList.get(rangePricesList.size()-1).getSimENum() || bomGoodsDTO.getGoodsNum() > rangePricesList.get(rangePricesList.size()-1).getSimENum()){
					newSum =  rangePricesList.get(rangePricesList.size()-1).getSimENum();
					isok = true;
					break;
				}
			}
			if(!isok){
				newSum = rangePricesList.get(rangePricesList.size()-1).getSimENum();
			}
			System.out.println(newSum);
			System.out.println("add");
		}
		if(type.equals("sub")){
			boolean isok = false;
			for (int i = 0; i < rangePricesList.size(); i++) {
				if((bomGoodsDTO.getGoodsNum() >= rangePricesList.get(i).getSimSNum()) && (bomGoodsDTO.getGoodsNum() <= rangePricesList.get(i).getSimENum())){
					newSum = bomGoodsDTO.getGoodsNum().intValue() - rangePricesList.get(i).getSimIncrease();
					isok = true;
				}
				if(bomGoodsDTO.getGoodsNum() < rangePricesList.get(0).getSimSNum()){
					newSum =  rangePricesList.get(0).getSimSNum();
					isok = true;
					break;
				}
				if(((i+1) <= rangePricesList.size() ) && (bomGoodsDTO.getGoodsNum() > rangePricesList.get(i).getSimENum()) && (bomGoodsDTO.getGoodsNum() < rangePricesList.get(i+1).getSimSNum()) ){
					newSum = rangePricesList.get(i+1).getSimSNum();
					break;
				}
				if(newSum > rangePricesList.get(rangePricesList.size()-1).getSimENum() || bomGoodsDTO.getGoodsNum() > rangePricesList.get(rangePricesList.size()-1).getSimENum()){
					newSum =  rangePricesList.get(rangePricesList.size()-1).getSimENum();
					isok = true;
					break;
				}
			}
			if(!isok){
				newSum = rangePricesList.get(rangePricesList.size()-1).getSimENum();
			}
			System.out.println(newSum);
			System.out.println("sub");
		}
		bomGoods = bomManager.getCtBomGoodsByGId(bomGoodsDTO.getBomGoodsId(), bomGoodsDTO.getBomId());
		bomGoods.setGoodsNum(newSum.longValue());
		bomManager.updateCtBomGoods(bomGoods);
		result = "success_"+newSum;
		return SUCCESS;
	}
	
	//更新BOM数量
	public String updateBomNum(){
		List<String> l = new ArrayList<String>();
		try {
			
			Long bomNum = this.bomDTO.getGoodsNum();
			Long bomId = Long.valueOf(this.bomDTO.getBomId());
			
			
			//CtBomGoods cbg = this.bomManager.getCtBomGoodsBybomGoodsId(bomGoodsId);
			//cbg.setGoodsNum(goodsNum);
			String res=this.bomManager.updateCtBomNum(bomId,bomNum);
			
			l.add("success");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;	
		} catch (Exception e) {
			// TODO: handle exception
			String errorTotle = "抱歉，提交异常";
			String errorMess = "含非法字符";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			//session.put("errorBackUrl", url);
			return ERROR;
		}
	}
	
	
	
	//更新数量
	public String updateGoodsNum(){
		List<String> l = new ArrayList<String>();
		try {
			Long goodsNum = this.bomGoodsDTO.getGoodsNum();
			Long GId = this.bomGoodsDTO.getGId();
			Integer bomId = this.bomGoodsDTO.getBomId();
			Long bomGoodsId = bomGoodsDTO.getBomGoodsId();
			CtBomGoods cbg = this.bomManager.getCtBomGoodsBybomGoodsId(bomGoodsId);
			cbg.setGoodsNum(goodsNum);
			String res=this.bomManager.updateCtBomGoods(cbg);
			if(res.equals("Exerror")){
				String url = bomDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
			}
			l.add("success");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;	
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			l.add("error");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		}
	}
	//新增Bom详情中的商品
	public String addGoodsOfBomDetail(){
		try {
			//获取登录的用户ID
			Long uid = null;
			if(this.session.get("userinfosession")!=null){
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
			}
			Integer bomId = this.bomGoodsDTO.getBomId();
			this.bom = this.bomManager.getCtBomByBomId(bomId, uid);
			Long GId = this.bomGoodsDTO.getGId();
			List<CtBomGoods> cbgs = this.bomManager.getCtBomGoodsByGIdAndBomId(GId, bomId);
			
			//获取基础数据
			rangePricesList = basicManager.getAllRanPrice(GId);
			String[] sims = new String[3];
			String[] sime = new String[3];
			String[] siminc = new String[3];
			String[] simprice = new String[3];
			
			String[] pars = new String[3];
			String[] pare = new String[3];
			String[] parinc = new String[3];
			String[] parprice = new String[3];
			for (int i = 0; i < rangePricesList.size(); i++) {
				sims[i]=rangePricesList.get(i).getSimSNum().toString();
				sime[i]=rangePricesList.get(i).getSimENum().toString();
				siminc[i] = rangePricesList.get(i).getSimIncrease().toString();
				simprice[i] = rangePricesList.get(i).getSimRPrice().toString();
				
				pars[i] = rangePricesList.get(i).getParSNum() == null ? "" : rangePricesList.get(i).getParSNum().toString();
				pare[i] = rangePricesList.get(i).getParENum() == null ? "" : rangePricesList.get(i).getParENum().toString();
				parinc[i] = rangePricesList.get(i).getParIncrease() == null ? "" : rangePricesList.get(i).getParIncrease().toString();
				parprice[i] = rangePricesList.get(i).getParRPrice() == null ? "" : rangePricesList.get(i).getParRPrice().toString();
			}
			
			if(cbgs != null && cbgs.size()>0){
				for(int i=0;i<cbgs.size();i++){
					if("5K/盘".equals(cbgs.get(i).getPack())){
						nowCount = cbgs.get(i).getGoodsNum().intValue() + 1;
						simQuJian(sims,sime,siminc,pars,pare,parinc);
						
						cbgs.get(i).setGoodsNum(nowCount.longValue());
						this.bomManager.updateCtBomGoods(cbgs.get(i));
						break;
					}else {
						CtBomGoods cbg2 = new CtBomGoods();
						cbg2.setBomId(bomId);
						cbg2.setGId(GId);
						nowCount = 1;
						simQuJian(sims,sime,siminc,pars,pare,parinc);
						cbg2.setGoodsNum(nowCount.longValue());
						cbg2.setPack("5K/盘");
						String res=this.bomManager.saveCtBomGoods(cbg2);
						if(res.equals("Exerror")){
							String url = bomDTO.getUrl();
							//System.out.println(url);
							String errorTotle = "抱歉，提交异常";
							String errorMess = "含非法字符";
							session.put("errorMess", errorMess);
							session.put("errorTotle", errorTotle);
							session.put("errorBackUrl", url);
							return ERROR;
						}
					}
				}
			}else {
				CtBomGoods cbg2 = new CtBomGoods();
				cbg2.setBomId(bomId);
				cbg2.setGId(GId);
				nowCount = 1;
				simQuJian(sims,sime,siminc,pars,pare,parinc);
				cbg2.setGoodsNum(nowCount.longValue());
				cbg2.setPack("5K/盘");
				cbg2.setGoods(fgoodsManager.getGood(GId));
				cbg2.setCtBom(bomManager.getCtBomByBomId(bomId, uid));
				cbg2.setGPrice("1");
				String res=this.bomManager.saveCtBomGoods(cbg2);
//				bomManager.loadAll();
//				String res="a";
				if(res.equals("Exerror")){
					String url = bomDTO.getUrl();
					//System.out.println(url);
					String errorTotle = "抱歉，提交异常";
					String errorMess = "含非法字符";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", url);
					return ERROR;
				}
			}
			return "addGoodsOfBomDetail";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	private Integer nowCount;
	
	public Integer getNowCount() {
		return nowCount;
	}

	public void setNowCount(Integer nowCount) {
		this.nowCount = nowCount;
	}

	private int simQuJian(String[] sims, String[] sime,
			String[] siminc, String[] pars, String[] pare, String[] parinc) {
		Integer count = nowCount;
		if(count < Integer.valueOf(sims[0])){
			nowCount = Integer.valueOf(siminc[0]);
			return 3;
		}
		if(count > Integer.valueOf(sime[0]) && count < Integer.valueOf(sims[1])){
			//介于样品第一区间和第二区间之间
			nowCount = Integer.valueOf(siminc[1]);
			return 4;
		}
		if(count > Integer.valueOf(sime[1]) && count < Integer.valueOf(sims[2])){
			nowCount = Integer.valueOf(siminc[2]);
			return 5;
		}
		if(count >= Integer.valueOf(sims[0]) && count <= Integer.valueOf(sime[0])){
			//样品第一区间
			if(count!=0){
				String chu = nowCount / Double.valueOf(siminc[0]) + "";
				String[] chai = chu.split("\\.");
				if(!chai[1].equals("0")){
					Integer zheng = Integer.valueOf(chai[0]) * Integer.valueOf(siminc[0]);
					Integer cha = count - zheng;
					nowCount = count - cha + Integer.valueOf(siminc[0]);
				} else {
					nowCount = count;
				}
			}
			//nowCount = Integer.valueOf(parinc[0]);
			return 0;
		}
		if(count >= Integer.valueOf(sims[1]) && count <= Integer.valueOf(sime[1])){
			//样品第二区间
			if(count != 0){
				String chu = nowCount / Double.valueOf(siminc[1]) + "";
				String[] chai = chu.split("\\.");
				if(!chai[1].equals("0")){
					Integer zheng = Integer.valueOf(chai[0]) * Integer.valueOf(siminc[1]);
					Integer cha = count - zheng;
					nowCount = count - cha + Integer.valueOf(siminc[1]);
				} else {
					nowCount = count;
				}
			}
			//nowCount = Integer.valueOf(parinc[1]);
			return 1;
		}
		if(count >= Integer.valueOf(sims[2]) && count <= Integer.valueOf(sime[2])){
			//样品第三区间
			if(count != 0){
				String chu = nowCount / Double.valueOf(siminc[2]) + "";
				String[] chai = chu.split("\\.");
				if(!chai[1].equals("0")){
					Integer zheng = Integer.valueOf(chai[0]) * Integer.valueOf(siminc[2]);
					Integer cha = count - zheng;
					nowCount = count - cha + Integer.valueOf(siminc[2]);
				} else {
					nowCount = count;
				}
			}
			//nowCount = Integer.valueOf(parinc[2]);
			return 2;
		}
		return 0;
	}
	//商品详情-加入我的bom两种方式
	public String addToBomGoodsTwo(){
		try {
			String ctids = bomDTO.getCtids();
			Long goodsNum;
			if (ctids != null) {
				String[] g = ctids.split(",");
				for (int i = 1; i < g.length; i++) {
					CtCart cart =bomManager.getcartBycartid(Long.parseLong(g[i]));
					String gid=cart.getGId();
					if(cart.getCartType().equals("0")){
						 goodsNum =Long.valueOf(1);
					}else{
						 goodsNum=cart.getGNumber();
					}
			//把商品加入新建的bom
			String bomTitle = this.bomDTO.getBomTitle();
			String pack = this.bomDTO.getPack();
			Map session = ActionContext.getContext().getSession();
			CtUser cu = (CtUser)session.get("userinfosession");
			Long UId = cu.getUId();
			//String bomTitle = this.bomDTO.getBomTitle();
			String bomDesc = this.bomDTO.getBomDesc();
			Date date = new Date();
			SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			bom = this.bomManager.getCtBomByBomTitle(bomTitle,"");
			if(bom==null){
			CtBom cb = new CtBom();
			cb.setBomTitle(bomTitle);
			cb.setBomDesc(bomTitle);
			cb.setBomTime(siFormat.format(date));
			cb.setUId(UId);
			cb.setIsHot("0");
			cb.setNum(0);
			String res = this.bomManager.save(cb);
			if(res.equals("Exerror")){
				String url = bomDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
			}
			}
			//查询bom
			bom = this.bomManager.getCtBomByBomTitle(bomTitle,"");
			CtBomGoods cbg = this.bomManager.getCtBomGoodsByGId(Long.valueOf(gid),bom.getBomId());
			if(cbg != null){
				if(cbg.getGoodsNum() == null){
					cbg.setGoodsNum((long) 2);
					cbg.setPack(pack);
				}else {
					cbg.setGoodsNum(cbg.getGoodsNum() + goodsNum);
					cbg.setPack(pack);
				}
				String res=this.bomManager.updateCtBomGoods(cbg);
				if(res.equals("Exerror")){
					String url = bomDTO.getUrl();
					//System.out.println(url);
					String errorTotle = "抱歉，提交异常";
					String errorMess = "含非法字符";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", url);
					return ERROR;
				}
			}else {
				CtBomGoods cbg2 = new CtBomGoods();
				cbg2.setBomId(bom.getBomId());
				cbg2.setGId(Long.valueOf(gid));
				cbg2.setGoodsNum(goodsNum);
				cbg2.setPack(pack);
				String res=this.bomManager.saveCtBomGoods(cbg2);
				if(res.equals("Exerror")){
					String url = bomDTO.getUrl();
					//System.out.println(url);
					String errorTotle = "抱歉，提交异常";
					String errorMess = "含非法字符";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", url);
					return ERROR;
				}
			}
			//把商品加入已有的bom
			for(Integer b:bomDTO.getMid()){
				CtBomGoods c = this.bomManager.getCtBomGoodsByGId(Long.valueOf(gid),b);
				if(c != null){
					if(c.getGoodsNum() == null){
						c.setGoodsNum((long) 2);
						c.setPack(pack);
					}else {
						c.setGoodsNum(c.getGoodsNum() + goodsNum);
						c.setPack(pack);
					}
					String res=this.bomManager.updateCtBomGoods(c);
					if(res.equals("Exerror")){
						String url = bomDTO.getUrl();
						//System.out.println(url);
						String errorTotle = "抱歉，提交异常";
						String errorMess = "含非法字符";
						session.put("errorMess", errorMess);
						session.put("errorTotle", errorTotle);
						session.put("errorBackUrl", url);
						return ERROR;
					}
				}else {
					CtBomGoods cbg2 = new CtBomGoods();
					cbg2.setBomId(b);
					cbg2.setGId(Long.valueOf(gid));
					cbg2.setGoodsNum(goodsNum);
					cbg2.setPack(pack);
					String res=this.bomManager.saveCtBomGoods(cbg2);
					if(res.equals("Exerror")){
						String url = bomDTO.getUrl();
						//System.out.println(url);
						String errorTotle = "抱歉，提交异常";
						String errorMess = "含非法字符";
						session.put("errorMess", errorMess);
						session.put("errorTotle", errorTotle);
						session.put("errorBackUrl", url);
						return ERROR;
					}
				}
			}
				}
			}
			List<String> l = new ArrayList<String>();
			l.add("success");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			List<String> l = new ArrayList<String>();
			l.add("error");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		}
	}
	//商品详情-加入我的bom，新建
	public String addToBomGoodsNew(){
		try {
			String ctids = bomDTO.getCtids();
			Long goodsNum;
			if (ctids != null) {
				String[] g = ctids.split(",");
				for (int i = 1; i < g.length; i++) {
					CtCart cart =bomManager.getcartBycartid(Long.parseLong(g[i]));
					String gid=cart.getGId();
					if(cart.getCartType().equals("0")){
						 goodsNum =Long.valueOf(1);
					}else{
						 goodsNum=cart.getGNumber();
					}
			//把商品加入新建的bom
			String pack = this.bomDTO.getPack();
			String bomTitle = this.bomDTO.getBomTitle();
			Map session = ActionContext.getContext().getSession();
			CtUser cu = (CtUser)session.get("userinfosession");
			Long UId = cu.getUId();
			//String bomTitle = this.bomDTO.getBomTitle();
			String bomDesc = this.bomDTO.getBomDesc();
			Date date = new Date();
			SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			bom = this.bomManager.getCtBomByBomTitle(bomTitle,"");
			if(bom==null){
			CtBom cb = new CtBom();
			cb.setBomTitle(bomTitle);
			cb.setBomDesc(bomTitle);
			cb.setBomTime(siFormat.format(date));
			cb.setUId(UId);
			cb.setIsHot("0");
			cb.setNum(0);
			String res = this.bomManager.save(cb);
			if(res.equals("Exerror")){
				String url = bomDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
			}
			}
			//查询bom
			bom = this.bomManager.getCtBomByBomTitle(bomTitle,"");
			CtBomGoods cbg = this.bomManager.getCtBomGoodsByGId(Long.valueOf(gid),bom.getBomId());
			if(cbg != null){
				if(cbg.getGoodsNum() == null){
					cbg.setGoodsNum((long) 2);
					cbg.setPack(pack);
				}else {
					cbg.setGoodsNum(cbg.getGoodsNum() + goodsNum);
					cbg.setPack(pack);
				}
				String res=this.bomManager.updateCtBomGoods(cbg);
				if(res.equals("Exerror")){
					String url = bomDTO.getUrl();
					//System.out.println(url);
					String errorTotle = "抱歉，提交异常";
					String errorMess = "含非法字符";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", url);
					return ERROR;
				}
			}else {
				CtBomGoods cbg2 = new CtBomGoods();
				cbg2.setBomId(bom.getBomId());
				cbg2.setGId(Long.valueOf(gid));
				cbg2.setGoodsNum(goodsNum);
				cbg2.setPack(pack);
				String res=this.bomManager.saveCtBomGoods(cbg2);
				if(res.equals("Exerror")){
					String url = bomDTO.getUrl();
					//System.out.println(url);
					String errorTotle = "抱歉，提交异常";
					String errorMess = "含非法字符";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", url);
					return ERROR;
				}
			}
				}
			}
			List<String> l = new ArrayList<String>();
			l.add("success");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			List<String> l = new ArrayList<String>();
			l.add("error");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		}
	}
	
	//把商品加入我的bom 已经存在的bom
	public String addToBomGoodsExisting(){
		try {
			Map session = ActionContext.getContext().getSession();
			CtUser cu = (CtUser)session.get("userinfosession");
			Long goodsNum;
			String price = "";
			String ctids = bomDTO.getCtids();
			if (ctids != null) {
				String[] g = ctids.split(",");
				for (int i = 1; i < g.length; i++) {
					List<CtCart> cartList = orderManager.getCartByUIdGIdPack(cu.getUId(), g[i], "notPack");
					for (int j = 0; j < cartList.size(); j++) {
						CtCart cart =bomManager.getcartBycartid(cartList.get(j).getCartId());
						String gid=cart.getGId();
						if(cart.getCartType().equals("0")){
							goodsNum=cart.getGParNumber();
							price = cart.getGParPrice();
						}else{
							goodsNum=cart.getGNumber();
							price = cart.getGPrice();
						}
						
						Long UId = cu.getUId();
						//String bomTitle = this.bomDTO.getBomTitle();
						for(Integer b:bomDTO.getMid()){
							CtBomGoods cboms = this.bomManager.getCtBomGoodsByGId(Long.valueOf(gid),b);
							if(cboms != null){
								//this.bomManager.updateCtBomGoods(c);
								goodsNum=cboms.getGoodsNum()+goodsNum;
								cboms.setGoodsNum(goodsNum);
								cboms.setPack(" ");
								this.bomManager.updateCtBomGoods(cboms);
							}else {
								CtBomGoods cb2 = new CtBomGoods();
								cb2.setGId(Long.valueOf(gid));
								cb2.setBomId(b);
								cb2.setGoodsNum(goodsNum);
								cb2.setGPrice(price);
								cb2.setPack(" ");
								String res=this.bomManager.saveCtBomGoods(cb2);
								if(res.equals("Exerror")){
									String url = bomDTO.getUrl();
									//System.out.println(url);
									String errorTotle = "抱歉，提交异常";
									String errorMess = "含非法字符";
									session.put("errorMess", errorMess);
									session.put("errorTotle", errorTotle);
									session.put("errorBackUrl", url);
									return ERROR;
								}
							}
						}
					}
				}
			}
			List<String> l = new ArrayList<String>();
			l.add("success");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
			} catch (Exception e) {
				// TODO: handle exception
				//e.printStackTrace();
				List<String> l = new ArrayList<String>();
				l.add("error");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
			}
			return SUCCESS;
		}
	//检验BomTitle是否存在
	public String checkBomTitle(){
		try {
			String bomTitle = bomDTO.getBomTitle();
			String bomId = "";
			if (bomDTO.getBomId()!=null){
				bomId =bomDTO.getBomId().toString();
			}
			
			CtBom cb = bomManager.getCtBomByBomTitle(bomTitle,bomId);
			if(cb != null){
				List<String> l = new ArrayList<String>();
				l.add("exists");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
			}
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			List<String> l = new ArrayList<String>();
			l.add("exists");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		}
	}
	
	
	
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.bomDTO;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request; 
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public CtBomDTO getBomDTO() {
		return bomDTO;
	}

	public void setBomDTO(CtBomDTO bomDTO) {
		this.bomDTO = bomDTO;
	}

	public CtBomManager getBomManager() {
		return bomManager;
	}

	public void setBomManager(CtBomManager bomManager) {
		this.bomManager = bomManager;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public List<CtBom> getBomList() {
		return bomList;
	}

	public void setBomList(List<CtBom> bomList) {
		this.bomList = bomList;
	}

	public CtBom getBom() {
		return bom;
	}

	public void setBom(CtBom bom) {
		this.bom = bom;
	}

	public List<CtBomGoods> getBomGoodsList() {
		return bomGoodsList;
	}

	public void setBomGoodsList(List<CtBomGoods> bomGoodsList) {
		this.bomGoodsList = bomGoodsList;
	}

	public CtBomGoodsDTO getBomGoodsDTO() {
		return bomGoodsDTO;
	}

	public void setBomGoodsDTO(CtBomGoodsDTO bomGoodsDTO) {
		this.bomGoodsDTO = bomGoodsDTO;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public JSONArray getResultJson() {
		return resultJson;
	}

	public void setResultJson(JSONArray resultJson) {
		this.resultJson = resultJson;
	}

	public BasicManager getBasicManager() {
		return basicManager;
	}

	public void setBasicManager(BasicManager basicManager) {
		this.basicManager = basicManager;
	}
	
	public Map<String, Object> getSession() {
		return session;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}
	//商品详情-加入我的bom两种方式
		public String addToBomFromGoodsTwo(){
			try {
				//把商品加入新建的bom
				String bomTitle = this.bomDTO.getBomTitle();
				Long gid = this.bomDTO.getGid();
				String pack = this.bomDTO.getPack();
				Long goodsNum = 1L;
				Map session = ActionContext.getContext().getSession();
				CtUser cu = (CtUser)session.get("userinfosession");
				Long UId = cu.getUId();
				//String bomTitle = this.bomDTO.getBomTitle();
				String bomDesc = this.bomDTO.getBomDesc();
				Date date = new Date();
				SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				CtBom cb = new CtBom();
				cb.setBomTitle(bomTitle);
				cb.setBomDesc(bomTitle);
				cb.setBomTime(siFormat.format(date));
				cb.setUId(UId);
				cb.setIsHot("0");
				cb.setNum(1);
				String res = this.bomManager.save(cb);
				if(res.equals("Exerror")){
					String url = bomDTO.getUrl();
					//System.out.println(url);
					String errorTotle = "抱歉，提交异常";
					String errorMess = "含非法字符";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", url);
					return ERROR;
				}
				//查询bom
				bom = this.bomManager.getCtBomsByUId(UId).get(0);
				CtBomGoods cbg = this.bomManager.getCtBomGoodsByGId(gid,bom.getBomId());
				if(cbg != null){
					if(cbg.getGoodsNum() == null){
						cbg.setGoodsNum((long) 2);
						cbg.setPack(pack);
					}else {
						cbg.setGoodsNum(cbg.getGoodsNum() + goodsNum);
						cbg.setPack(pack);
					}
					if(cbg.getPack() == null){
						cbg.setPack("1-1");
					}
					String re=this.bomManager.updateCtBomGoods(cbg);
					if(re.equals("Exerror")){
						String url = bomDTO.getUrl();
						//System.out.println(url);
						String errorTotle = "抱歉，提交异常";
						String errorMess = "含非法字符";
						session.put("errorMess", errorMess);
						session.put("errorTotle", errorTotle);
						session.put("errorBackUrl", url);
						return ERROR;
					}
				}else {
					CtBomGoods cbg2 = new CtBomGoods();
					cbg2.setBomId(bom.getBomId());
					cbg2.setGId(gid);
					cbg2.setGoodsNum(goodsNum);
					cbg2.setPack(pack);
					if(cbg2.getPack() == null){
						cbg2.setPack("1-1");
					}
					String re=this.bomManager.saveCtBomGoods(cbg2);
					if(re.equals("Exerror")){
						String url = bomDTO.getUrl();
						//System.out.println(url);
						String errorTotle = "抱歉，提交异常";
						String errorMess = "含非法字符";
						session.put("errorMess", errorMess);
						session.put("errorTotle", errorTotle);
						session.put("errorBackUrl", url);
						return ERROR;
					}
				}
				//把商品加入已有的bom
				for(Integer b:bomDTO.getMid()){
					CtBomGoods c = this.bomManager.getCtBomGoodsByGId(gid,b);
					if(c != null){
						if(c.getGoodsNum() == null){
							c.setGoodsNum((long) 2);
							c.setPack(pack);
						}else {
							c.setGoodsNum(c.getGoodsNum() + goodsNum);
							c.setPack(pack);
						}
						this.bomManager.updateCtBomGoods(c);
					}else {
						CtBomGoods cbg2 = new CtBomGoods();
						cbg2.setBomId(b);
						cbg2.setGId(gid);
						cbg2.setGoodsNum(goodsNum);
						cbg2.setPack(pack);
						if(cbg2.getPack() == null){
							cbg2.setPack("1-1");
						}
						String re=this.bomManager.saveCtBomGoods(cbg2);
						if(re.equals("Exerror")){
							String url = bomDTO.getUrl();
							//System.out.println(url);
							String errorTotle = "抱歉，提交异常";
							String errorMess = "含非法字符";
							session.put("errorMess", errorMess);
							session.put("errorTotle", errorTotle);
							session.put("errorBackUrl", url);
							return ERROR;
						}
					}
				}
				List<String> l = new ArrayList<String>();
				l.add("success");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;
			} catch (Exception e) {
				// TODO: handle exception
				//e.printStackTrace();
				List<String> l = new ArrayList<String>();
				l.add("error");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;
			}
		}
		//商品详情-加入我的bom，新建
		public String addToBomFromGoodsNew(){
			try {
				//把商品加入新建的bom
				String pack = this.bomDTO.getPack();
				Long goodsNum = 1L;
				String bomTitle = this.bomDTO.getBomTitle();
				Long gid = this.bomDTO.getGid();
				Map session = ActionContext.getContext().getSession();
				CtUser cu = (CtUser)session.get("userinfosession");
				Long UId = cu.getUId();
				//String bomTitle = this.bomDTO.getBomTitle();
				String bomDesc = this.bomDTO.getBomDesc();
				Date date = new Date();
				SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				CtBom cb = new CtBom();
				cb.setBomTitle(bomTitle);
				cb.setBomDesc(bomTitle);
				cb.setBomTime(siFormat.format(date));
				cb.setUId(UId);
				cb.setIsHot("0");
				cb.setNum(1);
				String res = this.bomManager.save(cb);
				if(res.equals("Exerror")){
					String url = bomDTO.getUrl();
					//System.out.println(url);
					String errorTotle = "抱歉，提交异常";
					String errorMess = "含非法字符";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", url);
					return ERROR;
				}
				//查询bom
				bom = this.bomManager.getCtBomsByUId(UId).get(0);
				CtBomGoods cbg = this.bomManager.getCtBomGoodsByGId(gid,bom.getBomId());
				if(cbg != null){
					if(cbg.getGoodsNum() == null){
						cbg.setGoodsNum((long) 2);
						cbg.setPack(pack);
					}else {
						cbg.setGoodsNum(cbg.getGoodsNum() + goodsNum);
						cbg.setPack(pack);
					}
					if(cbg.getPack() == null){
						cbg.setPack("1-1");
					}
					String re=this.bomManager.updateCtBomGoods(cbg);
					if(re.equals("Exerror")){
						String url = bomDTO.getUrl();
						//System.out.println(url);
						String errorTotle = "抱歉，提交异常";
						String errorMess = "含非法字符";
						session.put("errorMess", errorMess);
						session.put("errorTotle", errorTotle);
						session.put("errorBackUrl", url);
						return ERROR;
					}
				}else {
					CtBomGoods cbg2 = new CtBomGoods();
					cbg2.setBomId(bom.getBomId());
					cbg2.setGId(gid);
					cbg2.setGoodsNum(goodsNum);
					cbg2.setPack(pack);
					if(cbg2.getPack() == null){
						cbg2.setPack("1-1");
					}
					this.bomManager.saveCtBomGoods(cbg2);
				}
				List<String> l = new ArrayList<String>();
				l.add("success");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;
			} catch (Exception e) {
				// TODO: handle exception
				//e.printStackTrace();
				List<String> l = new ArrayList<String>();
				l.add("error");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;
			}
		}
		
		//把商品加入我的bom 已经存在的bom
		public String addToBomFromGoodsExisting(){
			CtCart cart = new CtCart();
			Long gid = this.bomDTO.getGid();
			String pack = this.bomDTO.getPack();
			Long goodsNum = 1L; 
			Long cartId = this.bomDTO.getCartId();
			if (cartId!=null){
				cart = orderManager.getCartByCartId(cartId);
				pack=cart.getPack();
				goodsNum=cart.getGNumber();
			//	cart = orderManager.getCartByCartIdandpack(cartId,gid,pack);
				pack=cart.getPack();
				if (pack.equals("1")) {
					pack = "5K/盘";
				}
				if (pack.equals("2")) {
					pack = "50K/盒";
				}
				if (pack.equals("3")) {
					pack = "400K/箱";
				}
				goodsNum=cart.getGNumber();
			}
			try {
				for(Integer b:bomDTO.getMid()){
					//CtBomGoods c = this.bomManager.getCtBomGoodsByGId(gid,b);
					CtBomGoods c=this.bomManager.getCtBomGoodsByGIdandPack(gid, b, pack);
					if(c != null ){
						if (cartId!=null){//购物车
							c.setGoodsNum(c.getGoodsNum()+goodsNum);								
						}else{//单独商品页
							if(c.getGoodsNum() == null){
								c.setGoodsNum((long) 2);
								c.setPack(pack);
							}else {
								c.setGoodsNum(c.getGoodsNum() + goodsNum);
								c.setPack(pack);
							}
						}
						
					String res=this.bomManager.updateCtBomGoods(c);
					if(res.equals("Exerror")){
						String url = bomDTO.getUrl();
						//System.out.println(url);
						String errorTotle = "抱歉，提交异常";
						String errorMess = "含非法字符";
						session.put("errorMess", errorMess);
						session.put("errorTotle", errorTotle);
						session.put("errorBackUrl", url);
						return ERROR;
					}
					}else {
						CtBomGoods cbg2 = new CtBomGoods();
						cbg2.setBomId(b);
						cbg2.setGId(gid);
//						cbg2.setPack(pack);
						if (cartId!=null){//购物车
							cbg2.setGoodsNum(cart.getGNumber());
							cbg2.setPack(pack);
							cbg2.setGPrice(cart.getGPrice());
							
						}else{//单独商品页
							
							cbg2.setGoodsNum(goodsNum);
							cbg2.setPack(pack);
						}
						
						if(cbg2.getPack() == null){
							cbg2.setPack("1-1");
						}
					String res=this.bomManager.saveCtBomGoods(cbg2);
					if(res.equals("Exerror")){
						String url = bomDTO.getUrl();
						//System.out.println(url);
						String errorTotle = "抱歉，提交异常";
						String errorMess = "含非法字符";
						session.put("errorMess", errorMess);
						session.put("errorTotle", errorTotle);
						session.put("errorBackUrl", url);
						return ERROR;
					}
					}
				}
				List<String> l = new ArrayList<String>();
				l.add("success");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;
			} catch (Exception e) {
				// TODO: handle exception
				//e.printStackTrace();
				List<String> l = new ArrayList<String>();
				l.add("error");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
			}
			return SUCCESS;
		}
	public String errorpage(){
		return "errorpage";
	}
	private File file;              //文件  
    private String fileFileName;    //文件名   
    private String filePath;        //文件路径
    private String downloadFilePath;  //文件下载路径
    
    private Map<String, Integer> bomGoodsNumAndKey = new HashMap<String, Integer>();
    
    public Map<String, Integer> getBomGoodsNumAndKey() {
		return bomGoodsNumAndKey;
	}

	public void setBomGoodsNumAndKey(Map<String, Integer> bomGoodsNumAndKey) {
		this.bomGoodsNumAndKey = bomGoodsNumAndKey;
	}

	public String uploadBomNew(){
    	try {
    		Solr ts = new Solr();
			QueryResponse qr;
			SolrDocumentList slist;
			Long numfound;
			List<CsvExUtil> exUtils = readerCsvNew(this.file.getPath());
			for (int i = 0; i < exUtils.size(); i++) {
				String[] searchKeys = exUtils.get(i).getGoodsSn().split(" ");
				String str = "";
				for (int j = 0; j < searchKeys.length; j++) {
					str += "(name:"+searchKeys[j] + " OR B_NAME:"+searchKeys[j]+") OR "; 
				}
				str = str.substring(0, str.length()-4);
				qr=ts.SearchBomGoodsNum(str,0,10,true);
				slist=qr.getResults();//获取按条件查询筛选后的结果（ solr自带的方法）
				numfound = slist.getNumFound();//结果条数
				bomGoodsNumAndKey.put(exUtils.get(i).getGoodsSn(), numfound.intValue());
				System.out.println(searchKeys);
			}
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(bomGoodsNumAndKey);
			resultJson = json;
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	return SUCCESS;
    }
    
	//导入bom
	public String uploadBomCsv(){
		String bomId = "";
		try {
			CtUser cu = (CtUser)session.get("userinfosession");
			Solr ts = new Solr();
			String[] sortfield={"id","name"};
			Boolean[] flag={true,true};//flag 需要排序的字段的排序方式如果为true 升序 如果为false 降序
			Boolean hightlight=true;//hightlight 是否需要高亮显示
			String[] field;
			String[] key;
			QueryResponse qr;
			SolrDocumentList slist;
			Object[] ctGoods;
			Long numfound;
			List<CsvExUtil> exUtils = readerCsv(this.file.getPath());
			for (int i = 0; i < exUtils.size(); i++)  //外循环是循环的次数
            {
                for (int j = exUtils.size() - 1 ; j > i; j--)  //内循环是 外循环一次比较的次数
                {

                    if (exUtils.get(i).getGoodsSn().equals(exUtils.get(j).getGoodsSn()) || exUtils.get(i).getGoodsName().equals(exUtils.get(j).getGoodsName()))
                    {
                    	exUtils.remove(j);
                    }
                }
            }
			
			if(exUtils != null && exUtils.size() > 0){
				bom = new CtBom();
				bom.setBomTitle(fileFileName);
				bom.setBomDesc("自动生成bom");
				bom.setBomTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				bom.setUId(cu.getUId());
				bom.setIsHot("0");
				bom.setNum(1);
				bomId = bomManager.save(bom);
				if(!bomId.equals("Exerror")){
					try {
						for (int i = 0; i < exUtils.size(); i++) {
							if(!exUtils.get(i).getGoodsSn().equals("")){
								field = new String[1];
								key = new String[1];
								field[0] = "name";
								key[0] = ""+exUtils.get(i).getGoodsSn()+"";
								qr = ts.Search(field, key, 0, 999, sortfield, flag, hightlight,null);
								slist=qr.getResults();//获取按条件查询筛选后的结果（ solr自带的方法）
								ctGoods = new Object[20];
								for (SolrDocument solrDocument : slist) {//转换为对象
									ctGoods[0]=solrDocument.getFirstValue("id");
								}
								numfound = slist.getNumFound();//结果条数
								if(numfound != 0){
									bomGoods = new CtBomGoods();
									bomGoods.setBomId(Integer.valueOf(bomId));
									bomGoods.setBomGoodsId(Long.valueOf(ctGoods[0].toString()));
									bomGoods.setGId(Long.valueOf(ctGoods[0].toString()));
									bomGoods.setGBomXh(exUtils.get(i).getGoodsSn());
									bomGoods.setPack("-");
									bomGoods.setGPrice("-");
									Long goodsNum = getGoodsNum(ctGoods[0].toString(), exUtils.get(i).getGoodsNum());
									bomGoods.setGoodsNum(goodsNum);
									String bomGoodsId = bomManager.saveCtBomGoods(bomGoods);
									if(!bomGoodsId.equals("Exerror")){
										System.out.println("添加一条数据");
									}
								} else {
									if(!exUtils.get(i).getGoodsName().equals("")){
										field = new String[1];
										key = new String[1];
										field[0] = "name";
										key[0] = "\""+exUtils.get(i).getGoodsName()+"\"";
										qr = ts.Search(field, key, 0, 999, sortfield, flag, hightlight,null);
										slist = qr.getResults();//获取按条件查询筛选后的结果（ solr自带的方法）
										for (SolrDocument solrDocument : slist) {//转换为对象
											ctGoods[0]=solrDocument.getFirstValue("id");
										}
										numfound = slist.getNumFound();//结果条数
										if(numfound != 0){
											bomGoods = new CtBomGoods();
											bomGoods.setBomId(Integer.valueOf(bomId));
											bomGoods.setBomGoodsId(Long.valueOf(ctGoods[0].toString()));
											bomGoods.setGId(Long.valueOf(ctGoods[0].toString()));
											bomGoods.setGBomXh(exUtils.get(i).getGoodsSn());
											bomGoods.setPack("-");
											bomGoods.setGPrice("-");
											Long goodsNum = getGoodsNum(ctGoods[0].toString(), exUtils.get(i).getGoodsNum());
											bomGoods.setGoodsNum(goodsNum);
											String bomGoodsId = bomManager.saveCtBomGoods(bomGoods);
											if(!bomGoodsId.equals("Exerror")){
												System.out.println("添加一条数据");
											}
										} else {
											bomGoods = new CtBomGoods();
											bomGoods.setBomId(Integer.valueOf(bomId));
											bomGoods.setBomGoodsId(Long.valueOf(100));
											bomGoods.setGId(Long.valueOf(100));
											bomGoods.setGBomXh(exUtils.get(i).getGoodsSn());
											bomGoods.setPack("-");
											bomGoods.setGPrice("-");
											bomGoods.setGoodsNum(Long.valueOf(-1));
											String bomGoodsId = bomManager.saveCtBomGoods(bomGoods);
											if(!bomGoodsId.equals("Exerror")){
												System.out.println("添加一条数据");
											}
										}
									} else {
										bomGoods = new CtBomGoods();
										bomGoods.setBomId(Integer.valueOf(bomId));
										bomGoods.setBomGoodsId(Long.valueOf(100));
										bomGoods.setGId(Long.valueOf(100));
										bomGoods.setGBomXh(exUtils.get(i).getGoodsSn());
										bomGoods.setPack("-");
										bomGoods.setGPrice("-");
										bomGoods.setGoodsNum(Long.valueOf(-1));
										String bomGoodsId = bomManager.saveCtBomGoods(bomGoods);
										if(!bomGoodsId.equals("Exerror")){
											System.out.println("添加一条数据");
										}
									}
								}
								System.out.println(numfound);
							} else if(!exUtils.get(i).getGoodsName().equals("")) {
								ctGoods = new Object[20];
								field = new String[1];
								key = new String[1];
								field[0] = "name";
								key[0] = ""+exUtils.get(i).getGoodsName()+"";
								qr = ts.Search(field, key, 0, 999, sortfield, flag, hightlight,null);
								slist = qr.getResults();//获取按条件查询筛选后的结果（ solr自带的方法）
								for (SolrDocument solrDocument : slist) {//转换为对象
									ctGoods[0]=solrDocument.getFirstValue("id");
								}
								numfound = slist.getNumFound();//结果条数
								if(numfound != 0){
									bomGoods = new CtBomGoods();
									bomGoods.setBomId(Integer.valueOf(bomId));
									bomGoods.setBomGoodsId(Long.valueOf(ctGoods[0].toString()));
									bomGoods.setGId(Long.valueOf(ctGoods[0].toString()));
									bomGoods.setGBomXh(exUtils.get(i).getGoodsName());
									bomGoods.setPack("-");
									bomGoods.setGPrice("-");
									Long goodsNum = getGoodsNum(ctGoods[0].toString(), exUtils.get(i).getGoodsNum());
									bomGoods.setGoodsNum(goodsNum);
									String bomGoodsId = bomManager.saveCtBomGoods(bomGoods);
									if(!bomGoodsId.equals("Exerror")){
										System.out.println("添加一条数据");
									}
								} else {
									bomGoods = new CtBomGoods();
									bomGoods.setBomId(Integer.valueOf(bomId));
									bomGoods.setBomGoodsId(Long.valueOf(100));
									bomGoods.setGId(Long.valueOf(100));
									bomGoods.setGBomXh(exUtils.get(i).getGoodsName());
									bomGoods.setPack("-");
									bomGoods.setGPrice("-");
									bomGoods.setGoodsNum(Long.valueOf(-1));
									String bomGoodsId = bomManager.saveCtBomGoods(bomGoods);
									if(!bomGoodsId.equals("Exerror")){
										System.out.println("添加一条数据");
									}
								}
							} else {
								System.out.println("不存在");
							}
							System.out.println(i+"\t"+exUtils.get(i).toString());
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} else {
				
			}
			System.out.println(exUtils);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			List<String> l = new ArrayList<String>();
			JSONArray json = new JSONArray();
			l.add(bomId);
			json = JSONArray.fromObject(l);
			resultJson = json;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	private Long getGoodsNum(String gid, String goodsNum) {
		Double num = goodsNum.equals("") ? 0 : Double.valueOf(goodsNum);
		Long newNum = 0L;
		Boolean isflag = false;
		rangePricesList = bomManager.findRanByBGid(gid);
		for (int i = 0; i < rangePricesList.size(); i++) {
			try {
				if(num >= Double.valueOf(rangePricesList.get(i).getSimSNum()) && num <= Double.valueOf(rangePricesList.get(i).getSimENum())){
					Double suan = (num / Double.valueOf(rangePricesList.get(i).getSimIncrease()));
					if(suan.toString().indexOf(".") != -1){
						String chai[] = suan.toString().split("\\.");
						if(chai[1].equals("0")){
							newNum = Long.valueOf(chai[0]) * Long.valueOf(rangePricesList.get(i).getSimIncrease());
						} else {
							newNum = (Long.valueOf((chai[0])) + 1) * Long.valueOf(rangePricesList.get(i).getSimIncrease());
						}
					} else {
						newNum = Long.valueOf(goodsNum);
					}
					isflag = true;
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
		if(!isflag){
			for (int i = 0; i < rangePricesList.size(); i++) {
				try {
					if(num <= Double.valueOf(rangePricesList.get(i).getSimSNum()) && num >= (i+1==rangePricesList.size() ? 1 : Double.valueOf(rangePricesList.get(i+1).getSimENum()))){
						newNum = Long.valueOf(rangePricesList.get(i).getSimSNum());
						break;
					} else if(num >= Double.valueOf(rangePricesList.get(0).getSimENum())) {
						newNum = Long.valueOf(rangePricesList.get(0).getSimENum());
						break;
					} else {
						newNum = Long.valueOf(rangePricesList.get(2).getSimSNum());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return newNum;
	}

	public static void main(String[] args) {
		int[] arr = new int[]{8,2,1,0,3};
		int[] index = new int[]{2,0,3,2,4,0,1,3,2,3,3};
		
		String tel = "";
		for (int i : index) {
			tel += arr[i];
		}
		System.out.println(tel);
//		Integer i = 1000;
//		Integer j = 1000;
//		System.out.println(i==j);
		
//        try {
//			readerCsv("D://demo//bomexample.csv");
//			
//			
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}   		
	}
	 /**  
     * 读取csv  
     *   
     * @param csvFilePath  
     * @throws Exception  
     */  
    public static List<CsvExUtil> readerCsvNew(String csvFilePath) throws Exception {   
    	List<CsvExUtil> exUtils = new ArrayList<CsvExUtil>();
        CsvReader reader = new CsvReader(csvFilePath, ',',   
                Charset.forName("GB2312"));// shift_jis日语字体,utf-8  
        reader.readHeaders();   
        String[] headers = reader.getHeaders();   
  
        List<Object[]> list = new ArrayList<Object[]>();   
        while (reader.readRecord()) {   
            list.add(reader.getValues());   
        }   
        Object[][] datas = new String[list.size()][];   
        for (int i = 0; i < list.size(); i++) {   
            datas[i] = list.get(i);   
        }   
  
        /*  
         * 以下输出  
         */  
  
        for (int i = 0; i < headers.length; i++) {   
            System.out.print(headers[i] + "\t");   
        }   
        System.out.println("");   
        for (int i = 1; i < datas.length; i++) {   
        	String str = "";
            Object[] data = datas[i]; // 取出一组数据  
            CsvExUtil exUtil = new CsvExUtil();
            for (int j = 0; j < data.length; j++) { 
                Object cell = data[j];   
                System.out.print(cell + "\t"); 
                str += cell + ",===";
            }   
            String[] chai = str.split(",===");
            try {
				exUtil.setGoodsSn(chai[0]);
			} catch (Exception e2) {
				exUtil.setGoodsSn("");
				e2.printStackTrace();
			}
            exUtils.add(exUtil);
            System.out.println("");   
        }   
        return exUtils;
    }
    /**  
     * 读取csv  
     *   
     * @param csvFilePath  
     * @throws Exception  
     */  
    public static List<CsvExUtil> readerCsv(String csvFilePath) throws Exception {   
    	List<CsvExUtil> exUtils = new ArrayList<CsvExUtil>();
    	CsvReader reader = new CsvReader(csvFilePath, ',',   
    			Charset.forName("UTF-8"));// shift_jis日语字体,utf-8  
    	reader.readHeaders();   
    	String[] headers = reader.getHeaders();   
    	
    	List<Object[]> list = new ArrayList<Object[]>();   
    	while (reader.readRecord()) {   
    		list.add(reader.getValues());   
    	}   
    	Object[][] datas = new String[list.size()][];   
    	for (int i = 0; i < list.size(); i++) {   
    		datas[i] = list.get(i);   
    	}   
    	
    	/*  
    	 * 以下输出  
    	 */  
    	
    	for (int i = 0; i < headers.length; i++) {   
    		System.out.print(headers[i] + "\t");   
    	}   
    	System.out.println("");   
    	for (int i = 1; i < datas.length; i++) {   
    		String str = "";
    		Object[] data = datas[i]; // 取出一组数据  
    		CsvExUtil exUtil = new CsvExUtil();
    		for (int j = 0; j < data.length; j++) { 
    			Object cell = data[j];   
    			System.out.print(cell + "\t"); 
    			str += cell + ",===";
    		}   
    		String[] chai = str.split(",===");
    		try {
    			exUtil.setGoodsSn(chai[0]);
    		} catch (Exception e2) {
    			exUtil.setGoodsSn("");
    			e2.printStackTrace();
    		}
    		try {
    			exUtil.setGoodsName(chai[1]);
    		} catch (Exception e1) {
    			e1.printStackTrace();
    			exUtil.setGoodsName("");
    		}
    		try {
    			exUtil.setGoodsNum(chai[2]);
    		} catch (Exception e) {
    			e.printStackTrace();
    			exUtil.setGoodsNum("");
    		}
    		exUtils.add(exUtil);
    		System.out.println("");   
    	}   
    	return exUtils;
    }

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getDownloadFilePath() {
		return downloadFilePath;
	}

	public void setDownloadFilePath(String downloadFilePath) {
		this.downloadFilePath = downloadFilePath;
	}

	public CtBomGoods getBomGoods() {
		return bomGoods;
	}

	public void setBomGoods(CtBomGoods bomGoods) {
		this.bomGoods = bomGoods;
	}

	public List<CtRangePrice> getRangePricesList() {
		return rangePricesList;
	}

	public void setRangePricesList(List<CtRangePrice> rangePricesList) {
		this.rangePricesList = rangePricesList;
	}   
}

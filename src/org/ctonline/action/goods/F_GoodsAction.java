package org.ctonline.action.goods;

import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.apache.solr.common.util.NamedList;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.ctonline.action.BaseAction;
import org.ctonline.common.Solr;
import org.ctonline.dto.basic.CtCouponDetailDTO;
import org.ctonline.dto.goods.CtRangePriceDTO;
import org.ctonline.dto.goods.F_GoodsDTO;
import org.ctonline.dto.order.OrderDTO;
import org.ctonline.manager.basic.BasicManager;
import org.ctonline.manager.basic.CtCouponDetailManager;
import org.ctonline.manager.basic.ICtFreeSampleRegManager;
import org.ctonline.manager.goods.CtBomManager;
import org.ctonline.manager.goods.F_GoodsManager;
import org.ctonline.manager.order.OrderManager;
import org.ctonline.manager.user.CtUserManager;
import org.ctonline.po.basic.CtCoupon;
import org.ctonline.po.basic.CtCouponDetail;
import org.ctonline.po.basic.CtFreeSampleReg;
import org.ctonline.po.basic.CtNotice;
import org.ctonline.po.basic.CtReplace;
import org.ctonline.po.basic.CtResource;
import org.ctonline.po.basic.CtSearchKeyword;
import org.ctonline.po.basic.CtSystemInfo;
import org.ctonline.po.basic.CtXwStock;
import org.ctonline.po.goods.CtBom;
import org.ctonline.po.goods.CtBomGoods;
import org.ctonline.po.goods.CtCollect;
import org.ctonline.po.goods.CtCollectGoods;
import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.goods.CtGoodsAttribute;
import org.ctonline.po.goods.CtGoodsAttributeRelation;
import org.ctonline.po.goods.CtGoodsBrand;
import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.po.goods.CtGoodsDetail;
import org.ctonline.po.goods.CtGoodsGroup;
import org.ctonline.po.goods.CtGoodsImg;
import org.ctonline.po.goods.CtGoodsResource;
import org.ctonline.po.goods.CtGroupPrice;
import org.ctonline.po.goods.CtRangePrice;
import org.ctonline.po.goods.CtSearchCollect;
import org.ctonline.po.order.CtCart;
import org.ctonline.po.order.CtEvaluation;
import org.ctonline.po.order.CtOrderGoods;
import org.ctonline.po.order.CtOrderInfo;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.views.ViewCateNum;
import org.ctonline.po.views.ViewGoodsList;
import org.ctonline.po.views.ViewSubcateNum;
import org.ctonline.test.GetEjlGoodsInfo;
import org.ctonline.test.GetTrueOrderInfo;
import org.ctonline.test.RanPayOrder;
import org.ctonline.test.RanPayOrderLing;
import org.ctonline.test.StockNumInfo;
import org.ctonline.test.StockNumInfoJia;
import org.ctonline.test.UpdateEjlOrder;
import org.ctonline.test.UpdateOrderInfoStauts;
import org.ctonline.util.OSSObjectSample;
import org.ctonline.util.Page;
import org.ctonline.util.ZipFileUtil;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import com.aliyun.oss.OSSClient;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ModelDriven;
import com.sun.org.apache.bcel.internal.generic.NEW;

public class F_GoodsAction extends BaseAction implements RequestAware,
		ModelDriven<Object>, SessionAware {

	private F_GoodsDTO fgoodsDTO = new F_GoodsDTO();
	private Page page;
	private Map<String, Object> request;
	private F_GoodsManager fgoodsManager;
	private BasicManager basicManager;
	private CtUserManager userManager;
	private CtUser ctUser;
	private List<CtGoodsCategory> cateList;
	private List<CtGoodsCategory> cate;
	private List<CtGoodsCategory> ca;
	private List<CtGoodsCategory> cgclist=new ArrayList<CtGoodsCategory>();
	private List<CtGoodsCategory> cgcalist=new ArrayList<CtGoodsCategory>();
	private List<CtCollect> collects;
	
	private ICtFreeSampleRegManager regManager;
	
	public ICtFreeSampleRegManager getRegManager() {
		return regManager;
	}

	public void setRegManager(ICtFreeSampleRegManager regManager) {
		this.regManager = regManager;
	}

	private CtSearchKeyword searchKeyWord = new CtSearchKeyword();
	private List<CtSearchKeyword> searchKeywordsList = new ArrayList<CtSearchKeyword>();
	
	private CtFreeSampleReg sampleReg;
	
	public CtFreeSampleReg getSampleReg() {
		return sampleReg;
	}

	public void setSampleReg(CtFreeSampleReg sampleReg) {
		this.sampleReg = sampleReg;
	}

	public List<CtSearchKeyword> getSearchKeywordsList() {
		return searchKeywordsList;
	}

	public void setSearchKeywordsList(List<CtSearchKeyword> searchKeywordsList) {
		this.searchKeywordsList = searchKeywordsList;
	}

	public CtSearchKeyword getSearchKeyWord() {
		return searchKeyWord;
	}

	public void setSearchKeyWord(CtSearchKeyword searchKeyWord) {
		this.searchKeyWord = searchKeyWord;
	}

	private List<CtGoods> coupgoods;
	private CtGoods coupgood;
	
	public CtGoods getCoupgood() {
		return coupgood;
	}

	public void setCoupgood(CtGoods coupgood) {
		this.coupgood = coupgood;
	}

	public List<CtGoods> getCoupgoods() {
		return coupgoods;
	}

	public void setCoupgoods(List<CtGoods> coupgoods) {
		this.coupgoods = coupgoods;
	}

	public CtUserManager getUserManager() {
		return userManager;
	}

	public void setUserManager(CtUserManager userManager) {
		this.userManager = userManager;
	}

	public CtUser getCtUser() {
		return ctUser;
	}

	public void setCtUser(CtUser ctUser) {
		this.ctUser = ctUser;
	}

	public List<CtCollect> getCollects() {
		return collects;
	}

	public void setCollects(List<CtCollect> collects) {
		this.collects = collects;
	}

	public List<CtGoodsCategory> getCgclist() {
		return cgclist;
	}

	public void setCgclist(List<CtGoodsCategory> cgclist) {
		this.cgclist = cgclist;
	}

	public List<CtGoodsCategory> getCgcalist() {
		return cgcalist;
	}

	public void setCgcalist(List<CtGoodsCategory> cgcalist) {
		this.cgcalist = cgcalist;
	}

	private List<CtCollectGoods> collectsList;
	private List<CtCollect> collList;
	private List<ViewCateNum> catenumList;
	private List<ViewSubcateNum> subcatenumList;	
	private List<CtGoodsBrand> brandList;
	private List<ViewGoodsList> goodsList;
	private List<CtGoodsDetail> introduceList;
	private ViewGoodsList goods;
	private CtCollect collect;
	private CtGoodsBrand brand;
	private List<CtGoodsImg> imgList;
	private OrderManager orderManager;
	private JSONArray resultJson;

	private List<CtRangePrice> rangePricesList = new ArrayList<CtRangePrice>();
	private List<CtGroupPrice> groupPricesList = new ArrayList<CtGroupPrice>();
	
	
	public CtGoodsBrand getBrand() {
		return brand;
	}

	public void setBrand(CtGoodsBrand brand) {
		this.brand = brand;
	}

	public List<CtGoodsCategory> getCa() {
		return ca;
	}

	public void setCa(List<CtGoodsCategory> ca) {
		this.ca = ca;
	}

	public List<CtGoodsCategory> getCate() {
		return cate;
	}

	public void setCate(List<CtGoodsCategory> cate) {
		this.cate = cate;
	}

	private List<List<CtRangePriceDTO>> priceAllList = new ArrayList<List<CtRangePriceDTO>>();
	
	public CtCollect getCollect() {
		return collect;
	}

	public void setCollect(CtCollect collect) {
		this.collect = collect;
	}

	public OrderManager getOrderManager() {
		return orderManager;
	}

	public void setOrderManager(OrderManager orderManager) {
		this.orderManager = orderManager;
	}

	public List<CtRangePrice> getRangePricesList() {
		return rangePricesList;
	}

	public void setRangePricesList(List<CtRangePrice> rangePricesList) {
		this.rangePricesList = rangePricesList;
	}

	public List<CtGroupPrice> getGroupPricesList() {
		return groupPricesList;
	}

	public void setGroupPricesList(List<CtGroupPrice> groupPricesList) {
		this.groupPricesList = groupPricesList;
	}

	// private List<CtGoods> ctGoodsList;
	private String result;
	private List<CtCouponDetail> ctCouponDetails = new ArrayList<CtCouponDetail>();

	public List<CtCouponDetail> getCtCouponDetails() {
		return ctCouponDetails;
	}

	public void setCtCouponDetails(List<CtCouponDetail> ctCouponDetails) {
		this.ctCouponDetails = ctCouponDetails;
	}

	private List<?> probrand;
	private List<CtGoodsCategory> procate;
	private List<?> protype;
	private List<?> proattr;
	private List<?> proattrval;
	private List<?> searchList;
	private List<?> Rgoods;
	private List<?> Lgoods;
	private Map<String, Object> session;
	private CtCouponDetailManager coupondetailManager;
	private List<CtCouponDetail> couponDetails;
	private CtBomManager bomManager;
	private List<CtBom> boms;
	private List<?> collectList;

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.fgoodsDTO;
	}

	public CtCouponDetailManager getCoupondetailManager() {
		return coupondetailManager;
	}

	public void setCoupondetailManager(CtCouponDetailManager coupondetailManager) {
		this.coupondetailManager = coupondetailManager;
	}

	public List<CtCouponDetail> getCouponDetails() {
		return couponDetails;
	}

	public void setCouponDetails(List<CtCouponDetail> couponDetails) {
		this.couponDetails = couponDetails;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}

	public List<List<CtRangePriceDTO>> getPriceAllList() {
		return priceAllList;
	}

	public void setPriceAllList(List<List<CtRangePriceDTO>> priceAllList) {
		this.priceAllList = priceAllList;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public F_GoodsDTO getFgoodsDTO() {
		return fgoodsDTO;
	}

	public void setFgoodsDTO(F_GoodsDTO fgoodsDTO) {
		this.fgoodsDTO = fgoodsDTO;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public F_GoodsManager getFgoodsManager() {
		return fgoodsManager;
	}

	public void setFgoodsManager(F_GoodsManager fgoodsManager) {
		this.fgoodsManager = fgoodsManager;
	}

	public List<CtGoodsCategory> getCateList() {
		return cateList;
	}

	public void setCateList(List<CtGoodsCategory> cateList) {
		this.cateList = cateList;
	}

	public List<ViewCateNum> getCatenumList() {
		return catenumList;
	}

	public void setCatenumList(List<ViewCateNum> catenumList) {
		this.catenumList = catenumList;
	}

	public List<ViewSubcateNum> getSubcatenumList() {
		return subcatenumList;
	}

	public void setSubcatenumList(List<ViewSubcateNum> subcatenumList) {
		this.subcatenumList = subcatenumList;
	}

	public List<CtGoodsBrand> getBrandList() {
		return brandList;
	}

	public void setBrandList(List<CtGoodsBrand> brandList) {
		this.brandList = brandList;
	}

	public List<ViewGoodsList> getGoodsList() {
		return goodsList;
	}

	public void setGoodsList(List<ViewGoodsList> goodsList) {
		this.goodsList = goodsList;
	}

	public BasicManager getBasicManager() {
		return basicManager;
	}

	public void setBasicManager(BasicManager basicManager) {
		this.basicManager = basicManager;
	}

	public List<?> getProbrand() {
		return probrand;
	}

	public void setProbrand(List<?> probrand) {
		this.probrand = probrand;
	}

	public List<CtGoodsCategory> getProcate() {
		return procate;
	}

	public void setProcate(List<CtGoodsCategory> procate) {
		this.procate = procate;
	}

	public List<?> getProtype() {
		return protype;
	}

	public void setProtype(List<?> protype) {
		this.protype = protype;
	}

	public List<?> getProattr() {
		return proattr;
	}

	public void setProattr(List<?> proattr) {
		this.proattr = proattr;
	}

	public List<?> getProattrval() {
		return proattrval;
	}

	public void setProattrval(List<?> proattrval) {
		this.proattrval = proattrval;
	}

	public List<?> getSearchList() {
		return searchList;
	}

	public void setSearchList(List<?> searchList) {
		this.searchList = searchList;
	}

	public ViewGoodsList getGoods() {
		return goods;
	}

	public void setGoods(ViewGoodsList goods) {
		this.goods = goods;
	}

	public List<CtGoodsImg> getImgList() {
		return imgList;
	}

	public void setImgList(List<CtGoodsImg> imgList) {
		this.imgList = imgList;
	}

	public List<CtGoodsDetail> getIntroduceList() {
		return introduceList;
	}

	public void setIntroduceList(List<CtGoodsDetail> introduceList) {
		this.introduceList = introduceList;
	}

	public List<?> getRgoods() {
		return Rgoods;
	}

	public void setRgoods(List<?> rgoods) {
		Rgoods = rgoods;
	}

	public List<?> getLgoods() {
		return Lgoods;
	}

	public void setLgoods(List<?> lgoods) {
		Lgoods = lgoods;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	// 点式筛选
	public String gridChose() {
		// 头
		this.cateList = fgoodsManager.queryAllCate();
		this.catenumList = fgoodsManager.cateCount();
		this.subcatenumList = fgoodsManager.subcateCount();

		int type = fgoodsDTO.getType();
		String mod = fgoodsDTO.getMod();
		String cont = fgoodsDTO.getCont();
		String price1 = fgoodsDTO.getPrice1();
		String price2 = fgoodsDTO.getPrice2();
		if (type == 1) {
			Long caid = fgoodsDTO.getCaid();
			Long paid = fgoodsDTO.getPaid();
			String keyw = fgoodsDTO.getKeyw();
			if(paid == null || paid == 1){
				paid = (Long) session.get("cids");
				if(paid == null){
					keyw = (String) session.get("typeName");
					if(keyw != null){
						keyw+="testStr";
					}
				}
			}
			if (price1 != null && price2 != null) {
				caid = 1L;
			}
			if (caid != null) {
				page = new Page();
				if (fgoodsDTO.getPage() == 0) {
					fgoodsDTO.setPage(1);
				}
				page.setCurrentPage(fgoodsDTO.getPage());
				if(cont==null){
					if(paid == null){
						cont = keyw;
					} else {
						cont = paid.toString();
					}
				}
				this.searchList = fgoodsManager.choseGoodsmod2(caid, mod, cont,
						page, price1, price2);
				// 取价
				if (searchList.size() > 0) {
					// 获取登录的用户ID
					Long uid = null;
					if (this.session.get("userinfosession") != null) {
						CtUser cu = (CtUser) this.session
								.get("userinfosession");
						uid = cu.getUId();
					}
					// 循环查询结果替换价格查询结果
					for (int i = 0; i < searchList.size(); i++) {
						Object[] obj = (Object[]) searchList.get(i);
						BigDecimal bgid = (BigDecimal) obj[0];
						Long gid = bgid.longValue();
						String price = this.basicManager.getPrice(uid, gid);
						if (price != null) {
							obj[4] = price;
						}
					}
				}
				page.setTotalPage(page.getTotalPage());
				this.request.put("type", 1);
				session.put("searchList", searchList);
				session.put("page", page);
				session.put("cidss", cont);
				this.request.put("pages", page);
				this.request.put("caid", caid);
				this.request.put("mode", mod);
				this.request.put("cont", cont);
				session.remove("jiluPriceFind");
				return SUCCESS;
			} else if (paid != null) {
				page = new Page();
				if (fgoodsDTO.getPage() == 0) {
					fgoodsDTO.setPage(1);
				}
				page.setCurrentPage(fgoodsDTO.getPage());
				this.searchList = fgoodsManager.choseGoodsmod1(paid, keyw, mod,
						cont, page);
				// 取价
				if (searchList.size() > 0) {
					// 获取登录的用户ID
					Long uid = null;
					if (this.session.get("userinfosession") != null) {
						CtUser cu = (CtUser) this.session
								.get("userinfosession");
						uid = cu.getUId();
					}
					// 循环查询结果替换价格查询结果
					for (int i = 0; i < searchList.size(); i++) {
						Object[] obj = (Object[]) searchList.get(i);
						BigDecimal bgid = (BigDecimal) obj[0];
						Long gid = bgid.longValue();
						String price = this.basicManager.getPrice(uid, gid);
						if (price != null) {
							obj[4] = price;
						}
					}
				}
				this.request.put("type", 1);
				page.setTotalPage(page.getTotalPage());
				this.request.put("pages", page);
				this.request.put("paid", paid);
				this.request.put("keyw", keyw);
				this.request.put("mode", mod);
				this.request.put("cont", cont);
				session.put("searchList", searchList);
				session.put("page", page);
				session.remove("jiluPriceFind");
				return SUCCESS;
			} else {
				return "error";
			}
		} else if (type == 2) {
			Long brid = fgoodsDTO.getBrid();
			if (brid != null) {
				page = new Page();
				if (fgoodsDTO.getPage() == 0) {
					fgoodsDTO.setPage(1);
				}
				page.setCurrentPage(fgoodsDTO.getPage());
				this.searchList = fgoodsManager.choseGoodsmod3(brid, mod, cont,
						page);
				// 取价
				if (searchList.size() > 0) {
					// 获取登录的用户ID
					Long uid = null;
					if (this.session.get("userinfosession") != null) {
						CtUser cu = (CtUser) this.session
								.get("userinfosession");
						uid = cu.getUId();
					}
					// 循环查询结果替换价格查询结果
					for (int i = 0; i < searchList.size(); i++) {
						Object[] obj = (Object[]) searchList.get(i);
						BigDecimal bgid = (BigDecimal) obj[0];
						Long gid = bgid.longValue();
						String price = this.basicManager.getPrice(uid, gid);
						if (price != null) {
							obj[4] = price;
						}
					}
				}
				page.setTotalPage(page.getTotalPage());
				session.put("searchList", searchList);
				session.put("page", page);
				this.request.put("type", 2);
				this.request.put("brid", brid);
				this.request.put("mode", mod);
				this.request.put("cont", cont);
				this.request.put("pages", page);
				session.remove("jiluPriceFind");
				return SUCCESS;
			} else {
				return "error";
			}
		}
		return SUCCESS;
	}

	// 列表筛选
	public String listChose() {
		// 头
		this.cateList = fgoodsManager.queryAllCate();
		this.catenumList = fgoodsManager.cateCount();
		this.subcatenumList = fgoodsManager.subcateCount();

		int type = fgoodsDTO.getType();
		String mod = fgoodsDTO.getMod();
		String cont = fgoodsDTO.getCont();
		String price1 = fgoodsDTO.getPrice1();
		String price2 = fgoodsDTO.getPrice2();
		if (type == 1) {
			Long caid = fgoodsDTO.getCaid();
			Long paid = fgoodsDTO.getPaid();
			String keyw = fgoodsDTO.getKeyw();
			if (caid != null) {
				page = new Page();
				if (fgoodsDTO.getPage() == 0) {
					fgoodsDTO.setPage(1);
				}
				page.setCurrentPage(fgoodsDTO.getPage());
				this.searchList = fgoodsManager.choseGoodsmod2(caid, mod, cont,
						page, price1, price2);
				// 取价
				if (searchList.size() > 0) {
					// 获取登录的用户ID
					Long uid = null;
					if (this.session.get("userinfosession") != null) {
						CtUser cu = (CtUser) this.session
								.get("userinfosession");
						uid = cu.getUId();
					}
					// 循环查询结果替换价格查询结果
					for (int i = 0; i < searchList.size(); i++) {
						Object[] obj = (Object[]) searchList.get(i);
						BigDecimal bgid = (BigDecimal) obj[0];
						Long gid = bgid.longValue();
						String price = this.basicManager.getPrice(uid, gid);
						if (price != null) {
							obj[4] = price;
						}
					}
				}
				page.setTotalPage(page.getTotalPage());
				session.put("searchList", searchList);
				session.put("page", page);
				this.request.put("type", 1);
				this.request.put("pages", page);
				this.request.put("caid", caid);
				this.request.put("mode", mod);
				this.request.put("cont", cont);
				session.remove("jiluPriceFind");
				return SUCCESS;
			} else if (paid != null) {
				page = new Page();
				if (fgoodsDTO.getPage() == 0) {
					fgoodsDTO.setPage(1);
				}
				page.setCurrentPage(fgoodsDTO.getPage());
				this.searchList = fgoodsManager.choseGoodsmod1(paid, keyw, mod,
						cont, page);
				// 取价
				if (searchList.size() > 0) {
					// 获取登录的用户ID
					Long uid = null;
					if (this.session.get("userinfosession") != null) {
						CtUser cu = (CtUser) this.session
								.get("userinfosession");
						uid = cu.getUId();
					}
					// 循环查询结果替换价格查询结果
					for (int i = 0; i < searchList.size(); i++) {
						Object[] obj = (Object[]) searchList.get(i);
						BigDecimal bgid = (BigDecimal) obj[0];
						Long gid = bgid.longValue();
						String price = this.basicManager.getPrice(uid, gid);
						if (price != null) {
							obj[4] = price;
						}
					}
				}
				page.setTotalPage(page.getTotalPage());
				this.request.put("type", 1);
				this.request.put("pages", page);
				this.request.put("paid", paid);
				this.request.put("keyw", keyw);
				this.request.put("mode", mod);
				this.request.put("cont", cont);
				session.remove("jiluPriceFind");
				return SUCCESS;
			} else {
				return "error";
			}
		} else if (type == 2) {
			Long brid = fgoodsDTO.getBrid();
			if (brid != null) {
				page = new Page();
				if (fgoodsDTO.getPage() == 0) {
					fgoodsDTO.setPage(1);
				}
				page.setCurrentPage(fgoodsDTO.getPage());
				this.searchList = fgoodsManager.choseGoodsmod3(brid, mod, cont,
						page);
				// 取价
				if (searchList.size() > 0) {
					// 获取登录的用户ID
					Long uid = null;
					if (this.session.get("userinfosession") != null) {
						CtUser cu = (CtUser) this.session
								.get("userinfosession");
						uid = cu.getUId();
					}
					// 循环查询结果替换价格查询结果
					for (int i = 0; i < searchList.size(); i++) {
						Object[] obj = (Object[]) searchList.get(i);
						BigDecimal bgid = (BigDecimal) obj[0];
						Long gid = bgid.longValue();
						String price = this.basicManager.getPrice(uid, gid);
						if (price != null) {
							obj[4] = price;
						}
					}
				}
				page.setTotalPage(page.getTotalPage());
				this.request.put("type", 2);
				this.request.put("brid", brid);
				this.request.put("mode", mod);
				this.request.put("cont", cont);
				this.request.put("pages", page);
				session.remove("jiluPriceFind");
				return SUCCESS;
			} else {
				return "error";
			}
		}
		return SUCCESS;
	}

	// 点式品牌筛选
	public String gridByBrand() {
		try {
			// 头
			this.cateList = fgoodsManager.queryAllCate();
			this.catenumList = fgoodsManager.cateCount();
			this.subcatenumList = fgoodsManager.subcateCount();
			Long brid = fgoodsDTO.getBrid();
			Long bid = fgoodsDTO.getBId();
			if (bid == null) {
				if (brid == null) {
					return "error";
				} else {
					bid = brid;
				}
			}
			page = new Page();
			if (fgoodsDTO.getPage() == 0) {
				fgoodsDTO.setPage(1);
			}
			page.setCurrentPage(fgoodsDTO.getPage());
			this.searchList = fgoodsManager.searchGoodsByBrand(bid, page);

			if (searchList.size() > 0) {
				// 获取品牌名称
				Object[] obj1 = (Object[]) searchList.get(0);
				String brand = (String) obj1[6];
				this.request.put("braname", brand);
				// 获取类型和属性
				this.protype = fgoodsManager.getTypeByBId(bid);
				this.proattr = fgoodsManager.getAttrByBId(bid);
				this.proattrval = fgoodsManager.getAttrValByBId(bid);
				// 取价
				// 获取登录的用户ID
				Long uid = null;
				if (this.session.get("userinfosession") != null) {
					CtUser cu = (CtUser) this.session.get("userinfosession");
					uid = cu.getUId();
				}
				// 循环查询结果替换价格查询结果
				for (int i = 0; i < searchList.size(); i++) {
					Object[] obj = (Object[]) searchList.get(i);
					BigDecimal bgid = (BigDecimal) obj[0];
					Long gid = bgid.longValue();
					String price = this.basicManager.getPrice(uid, gid);
					if (price != null) {
						obj[4] = price;
					}
				}
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			this.request.put("brid", bid);
		} catch (Exception e) {
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}

		return SUCCESS;
	}

	// 列表品牌筛选
	public String listByBrand() {
		// 头
		this.cateList = fgoodsManager.queryAllCate();
		this.catenumList = fgoodsManager.cateCount();
		this.subcatenumList = fgoodsManager.subcateCount();
		Long brid = fgoodsDTO.getBrid();
		Long bid = fgoodsDTO.getBId();
		if (bid == null) {
			if (brid == null) {
				return "error";
			} else {
				bid = brid;
			}
		}
		page = new Page();
		if (fgoodsDTO.getPage() == 0) {
			fgoodsDTO.setPage(1);
		}
		page.setCurrentPage(fgoodsDTO.getPage());
		this.searchList = fgoodsManager.searchGoodsByBrand(bid, page);

		if (searchList.size() > 0) {
			// 获取品牌名称
			Object[] obj1 = (Object[]) searchList.get(0);
			String brand = (String) obj1[6];
			this.request.put("braname", brand);
			// 获取类型和属性
			this.protype = fgoodsManager.getTypeByBId(bid);
			this.proattr = fgoodsManager.getAttrByBId(bid);
			this.proattrval = fgoodsManager.getAttrValByBId(bid);
			// 取价
			// 获取登录的用户ID
			Long uid = null;
			if (this.session.get("userinfosession") != null) {
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
			}
			// 循环查询结果替换价格查询结果
			for (int i = 0; i < searchList.size(); i++) {
				Object[] obj = (Object[]) searchList.get(i);
				BigDecimal bgid = (BigDecimal) obj[0];
				Long gid = bgid.longValue();
				String price = this.basicManager.getPrice(uid, gid);
				if (price != null) {
					obj[4] = price;
				}
			}
		}
		page.setTotalPage(page.getTotalPage());
		this.request.put("pages", page);
		this.request.put("brid", bid);
		session.remove("jiluPriceFind");
		return SUCCESS;
	}

	private String price1 = null;
	private String price2 = null;

	public String getPrice1() {
		return price1;
	}

	public void setPrice1(String price1) {
		this.price1 = price1;
	}

	public String getPrice2() {
		return price2;
	}

	public void setPrice2(String price2) {
		this.price2 = price2;
	}

	// 按照价格区间查询
	public String findPriceGroup() {
		this.cateList = fgoodsManager.queryAllCate();
		this.catenumList = fgoodsManager.cateCount();
		this.subcatenumList = fgoodsManager.subcateCount();
		this.goodsList = fgoodsManager.callGoodsByPrice(price1, price2);
		return SUCCESS;
	}
	
	public static String getEncoding(String str) {      
	       String encode = "GB2312";      
	      try {      
	          if (str.equals(new String(str.getBytes(encode), encode))) {      
	               String s = encode;      
	              return s;      
	           }      
	       } catch (Exception exception) {      
	       }      
	       encode = "ISO-8859-1";      
	      try {      
	          if (str.equals(new String(str.getBytes(encode), encode))) {      
	               String s1 = encode;      
	              return s1;      
	           }      
	       } catch (Exception exception1) {      
	       }      
	       encode = "UTF-8";      
	      try {      
	          if (str.equals(new String(str.getBytes(encode), encode))) {      
	               String s2 = encode;      
	              return s2;      
	           }      
	       } catch (Exception exception2) {      
	       }      
	       encode = "GBK";      
	      try {      
	          if (str.equals(new String(str.getBytes(encode), encode))) {      
	               String s3 = encode;      
	              return s3;      
	           }      
	       } catch (Exception exception3) {      
	       }      
	      return "";      
	   }      
	
	
//	public String goodsbrand(){
//		 List<String> cnameTemp = new ArrayList<String>();
//		 List<String> bnameTemp = new ArrayList<String>();
//		searchKeywordsList = fgoodsManager.findKeyWordOrderTop4();
//        session.put("searchKeywordsList", searchKeywordsList);
//		getSubHade();
//		Solr ts = new Solr();
//		String[] field = new String[1];
//		String[] key = new String[1];
//		field[0] = "B_ID";
//		key[0]=fgoodsDTO.getBId().toString();
//		CtSystemInfo systemInfo = new CtSystemInfo();
//		String[] sortfield={"id","name"};
//		List<ViewGoodsList> list= new ArrayList<ViewGoodsList>();
//		Boolean[] flag={true,true};//flag 需要排序的字段的排序方式如果为true 升序 如果为false 降序
//		Boolean hightlight=true;//hightlight 是否需要高亮显示
//		
//		try{
//			List<Object[]> goodsList = new ArrayList<Object[]>();
//			page = new Page();
//			if (fgoodsDTO.getPage() == 0) {
//				fgoodsDTO.setPage(1);
//			}
////			QueryResponse response = server.query(params);
////           // SolrDocumentList slist = response.getResults();
//            
//			page.setCurrentPage(fgoodsDTO.getPage());
//			QueryResponse qr = ts.Search(field, key, page.getFirstIndex(), page.getPageSize(), sortfield, flag, hightlight,null);
//			SolrDocumentList slist=qr.getResults();//获取按条件查询筛选后的结果（ solr自带的方法）
//			
//			
//			
//			
//			Long numfound = slist.getNumFound();//结果条数
//			page.setTotalCount(numfound);
//			
//	        for (SolrDocument solrDocument : slist) {//转换为对象
//	        	Object[] ctGoods = new Object[25];
//	        	this.imgList = fgoodsManager.getImg(Long.valueOf(solrDocument.getFirstValue("id").toString()));
//	        	String path = "";
//				if(imgList.size() == 0){
//					path =systemInfo.getCtImgUrl() +"//"+"ct_s.gif";
//				} else {
//					path =systemInfo.getCtImgUrl() +"//"+imgList.get(3).getPUrl();
//				}
//	            ctGoods[0]=solrDocument.getFirstValue("id");
//				if(ctGoods[1] == null){
//					ctGoods[1]=solrDocument.getFirstValue("G_NAME");
//				}
//				ctGoods[2]=solrDocument.getFirstValue("G_SN");
//				ctGoods[5]=solrDocument.getFirstValue("SHOP_PRICE");
//				ctGoods[6]=solrDocument.getFirstValue("B_NAME");
//				ctGoods[7]=solrDocument.getFirstValue("G_UNIT");
//				ctGoods[8]=solrDocument.getFirstValue("PACK1");
//				ctGoods[9]=solrDocument.getFirstValue("PACK1_NUM");
//				ctGoods[10]=solrDocument.getFirstValue("G_NUM");
//				ctGoods[11]=solrDocument.getFirstValue("IS_SAMPLE");
//				ctGoods[12]=solrDocument.getFirstValue("IS_PARTIAL");
//				ctGoods[13]=solrDocument.getFirstValue("ATTR_VALUE");
//				goodsBName.add((String) solrDocument.getFirstValue("B_NAME"));
//				goodsCName.add((String) solrDocument.getFirstValue("C_NAME"));
//				ctGoods[16]=path;
//	            rangePricesList = basicManager.getAllRanPrice(Long.valueOf(solrDocument.getFirstValue("id").toString()));
//	            rangePricesList = basicManager.getAllRanPrice(Long.valueOf(solrDocument.getFirstValue("id").toString()));
//				ranListDTO = new ArrayList<CtRangePriceDTO>();
//				int cunsim = 0;
//				int cunPar = 0;
//				for (int i = 0; i < rangePricesList.size(); i++) {
//					CtRangePriceDTO rangePriceDTO = new CtRangePriceDTO();
//					Double parsnum = Double.valueOf(rangePricesList.get(i).getParSNum() == null ? 0 : rangePricesList.get(i).getParSNum()) / 1000;
//					Double parenum = Double.valueOf(rangePricesList.get(i).getParENum()==null ? 0 : rangePricesList.get(i).getParENum()) / 1000;
//					Double parinc = Double.valueOf(rangePricesList.get(i).getParIncrease() == null ? 0 : rangePricesList.get(i).getParIncrease()) / 1000;
//					rangePriceDTO.setGId(rangePricesList.get(i).getGId());
//					rangePriceDTO.setRPid(rangePricesList.get(i).getRPid());
//					rangePriceDTO.setSimSNum(rangePricesList.get(i).getSimSNum() == null ? 0 : rangePricesList.get(i).getSimSNum());
//					rangePriceDTO.setSimENum(rangePricesList.get(i).getSimENum() == null ? 0 : rangePricesList.get(i).getSimENum());
//					rangePriceDTO.setSimRPrice(rangePricesList.get(i).getSimRPrice() == null ? 0 : rangePricesList.get(i).getSimRPrice());
//					rangePriceDTO.setSimIncrease(rangePricesList.get(i).getSimIncrease() == null ? 0 : rangePricesList.get(i).getSimIncrease());
//					rangePriceDTO.setParENum(df.format(parenum.equals(0)? "" : parenum));
//					rangePriceDTO.setParIncrease(df.format(parinc));
//					rangePriceDTO.setParSNum(df.format(parsnum));
//					rangePriceDTO.setParRPrice(rangePricesList.get(i).getParRPrice());
//					rangePriceDTO.setInsTime(rangePricesList.get(i).getInsTime());
//					ranListDTO.add(rangePriceDTO);
//					if(rangePricesList.get(i).getSimSNum() != null){
//						cunsim++;
//					}
//					if(rangePricesList.get(i).getParSNum() != null){
//						cunPar++;
//					}
//				}
//				if(cunsim != 0){
//					ctGoods[24]="yes";
//					session.put("cunsim", "yes");
//				} else {
//					ctGoods[24]="no";
//					session.put("cunsim", "no");
//				}
//				if(cunPar != 0){
//					ctGoods[23]="yes";
//					session.put("cunPar", "yes");
//				} else {
//					ctGoods[23]="no";
//					session.put("cunPar", "no");
//				}
//				goodsList.add(ctGoods);
//				priceAllList.add(ranListDTO);
//	         }
//	        this.request.put("tkword", goodsList.get(0)[6]);
//	        this.request.put("typeName", goodsList.get(0)[6]);
//	        page.setTotalPage(page.getTotalPage());
//	        this.request.put("pages", page);
//	        searchList = goodsList;
//	      //获取分类以及数量
//			String tempName = "";
//			List<FacetField> cnameList = ts.Search11(field, key, page.getFirstIndex(), page.getPageSize(), sortfield, flag, hightlight,"C_NAME");
//			if(cnameList.size() == 2){
//				for (int j = 0; j < cnameList.get(1).getValues().size(); j++) {
//					cnameTemp.add(cnameList.get(1).getValues().get(j).getName() + " ( "+cnameList.get(1).getValues().get(j).getCount()+" ) ");
//					goodsCName.add(cnameList.get(1).getValues().get(j).getName());
////					if(corbname.equals(cnameList.get(1).getValues().get(j).getName())){
////						tempName = cnameList.get(1).getValues().get(j).getName() + " ( "+cnameList.get(1).getValues().get(j).getCount()+" ) ";
////					}
//					System.out.println("["+ cnameList.get(1).getValues().get(j).getName() + "]  " + cnameList.get(1).getValues().get(j).getCount());
//				}
//				for (int j = 0; j < cnameList.get(0).getValues().size(); j++) {
//					goodsBName.add(cnameList.get(0).getValues().get(j).getName());
//					bnameTemp.add(cnameList.get(0).getValues().get(j).getName() + " ( "+cnameList.get(0).getValues().get(j).getCount()+" ) ");
////					if(corbname.equals(cnameList.get(0).getValues().get(j).getName())){
////						tempName = cnameList.get(0).getValues().get(j).getName() + " ( "+cnameList.get(0).getValues().get(j).getCount()+" ) ";
////					}
//					System.out.println("["+ cnameList.get(0).getValues().get(j).getName() + "]  " + cnameList.get(0).getValues().get(j).getCount());
//				}
//			} else {
//				for (int j = 0; j < cnameList.get(0).getValues().size(); j++) {
//					goodsBName.add(cnameList.get(0).getValues().get(j).getName());
//					bnameTemp.add(cnameList.get(0).getValues().get(j).getName() + " ( "+cnameList.get(0).getValues().get(j).getCount()+" ) ");
////					if(corbname.equals(cnameList.get(0).getValues().get(j).getName())){
////						tempName = cnameList.get(0).getValues().get(j).getName() + " ( "+cnameList.get(0).getValues().get(j).getCount()+" ) ";
////					}
//					System.out.println("["+ cnameList.get(0).getValues().get(j).getName() + "]  " + cnameList.get(0).getValues().get(j).getCount());
//				}
//			}
//			for (int i = 0; i < cnameTemp.size(); i++) {
//				//goodsCNameStr.add(goodsCName.get(i)+" (" + numfound + ")");
//				goodsCNameStr.add(cnameTemp.get(i));
//			}
//			for (int i = 0; i < bnameTemp.size(); i++) {
//				//goodsCNameStr.add(goodsCName.get(i)+" (" + numfound + ")");
//				goodsBNameStr.add(bnameTemp.get(i));
//			}
//			//this.request.put("tempName", tempName);
//			session.put("goodsCNameStr", goodsCNameStr);
//			session.put("goodsBNameStr", goodsBNameStr);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		session.put("brand", "true");
//		return SUCCESS;
//	}
	private List<String> keyList = new ArrayList<String>();
	
	public String findKeywordAjax(){
		try {
			Solr ts = new Solr();
			String[] field = new String[1];
			String[] key = new String[1];
			field[0] = "name";
			key[0]=fgoodsDTO.getKeyword();
			String[] sortfield={"id","name"};
			Boolean[] flag={true,true};//flag 需要排序的字段的排序方式如果为true 升序 如果为false 降序
			Boolean hightlight=true;//hightlight 是否需要高亮显示
			
			QueryResponse qr = ts.Search(field, key, 0, 9999, sortfield, flag, hightlight,null);
			SolrDocumentList slist=qr.getResults();//获取按条件查询筛选后的结果（ solr自带的方法）
			String s = "[";
//			Map<String, Map<String, List<String>>> map = qr.getHighlighting();
//			for (Map<String, List<String>> solrDocument : map.values()) {
//				for (List<String> li : solrDocument.values()) {
//					System.out.println(li.get(0));
//					s += "'"+li.get(0) + "',";
//					keyList.add(li.get(0));
//				}
//			}
			for (SolrDocument solrDocument : slist) {//转换为对象
				s += "'"+solrDocument.getFirstValue("G_NAME") + "',";
				keyList.add((String) solrDocument.getFirstValue("G_NAME"));
				Object o = solrDocument.getFieldValue("highlighting");
				System.out.println(o);
	         }
			s=s.substring(0, s.length() - 1);
			s += "]";
			
			 System.out.println(s);
			 result = s;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	private List<String> goodsBName = new ArrayList<String>();
	private List<String> goodsCName = new ArrayList<String>();
	private List<String> goodsBNameStr = new ArrayList<String>();
	private List<String> goodsCNameStr = new ArrayList<String>();
	

	public List<String> getGoodsBNameStr() {
		return goodsBNameStr;
	}

	public void setGoodsBNameStr(List<String> goodsBNameStr) {
		this.goodsBNameStr = goodsBNameStr;
	}

	public List<String> getGoodsCNameStr() {
		return goodsCNameStr;
	}

	public void setGoodsCNameStr(List<String> goodsCNameStr) {
		this.goodsCNameStr = goodsCNameStr;
	}

	public List<String> getGoodsBName() {
		return goodsBName;
	}

	public void setGoodsBName(List<String> goodsBName) {
		this.goodsBName = goodsBName;
	}

	public List<String> getGoodsCName() {
		return goodsCName;
	}

	public void setGoodsCName(List<String> goodsCName) {
		this.goodsCName = goodsCName;
	}

	private String searchType;
	private CtReplace replace = new CtReplace();
	private List<CtReplace> replacesList = new ArrayList<CtReplace>();
	
	public CtReplace getReplace() {
		return replace;
	}

	public void setReplace(CtReplace replace) {
		this.replace = replace;
	}

	public List<CtReplace> getReplacesList() {
		return replacesList;
	}

	public void setReplacesList(List<CtReplace> replacesList) {
		this.replacesList = replacesList;
	}
	/**
     * 判断字符是否是中文
     *
     * @param c 字符
     * @return 是否是中文
     */
    public static boolean isChinese(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
                || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS) {
            return true;
        }
        return false;
    }
    /**
     * 判断字符串是否是乱码
     *
     * @param strName 字符串
     * @return 是否是乱码
     */
    public static boolean isMessyCode(String strName) {
        Pattern p = Pattern.compile("\\s*|t*|r*|n*");
        Matcher m = p.matcher(strName);
        String after = m.replaceAll("");
        String temp = after.replaceAll("\\p{P}", "");
        char[] ch = temp.trim().toCharArray();
        float chLength = ch.length;
        float count = 0;
        for (int i = 0; i < ch.length; i++) {
            char c = ch[i];
            if (!Character.isLetterOrDigit(c)) {
                if (!isChinese(c)) {
                    count = count + 1;
                }
            }
        }
        float result = count / chLength;
        if (result > 0.4) {
            return true;
        } else {
            return false;
        }
 
    }
 public static void main(String[] args) throws UnsupportedEncodingException {
	 String keyword = "ÃƒÆ’Ã‚Â§ÃƒÂ¢Ã¢â€šÂ¬Ã‚ÂÃƒâ€šÃ‚ÂµÃƒÆ’Ã‚Â©Ãƒâ€¹Ã…â€œÃƒâ€šÃ‚Â»";
	 System.out.println(isMessyCode(keyword));
	 String s = new String(keyword.getBytes("ISO-8859-1"),"UTF-8");
	 System.out.println(s);
}
	public String search() throws SQLException, UnsupportedEncodingException {
		CtSystemInfo systemInfo = new CtSystemInfo();
		getSubHade();
		List<String> cnameTemp = new ArrayList<String>();
		List<String> bnameTemp = new ArrayList<String>();
		
		Solr ts = new Solr();
		if(this.session.get("userinfosession")!=null){
			F_GoodsDTO te = (F_GoodsDTO) session.get("loginLiebie");
			if(te != null && !te.equals("")){
				fgoodsDTO = te;
			}
			session.remove("loginLiebie");
		}
		if(fgoodsDTO.getCname()!=null || fgoodsDTO.getBname() != null){
			List<ViewGoodsList> list= new ArrayList<ViewGoodsList>();
			String[] sortfield={"id","name"};
			Boolean[] flag={true,true};//flag 需要排序的字段的排序方式如果为true 升序 如果为false 降序
			Boolean hightlight=true;//hightlight 是否需要高亮显示
			
			List<Object[]> goodsList = new ArrayList<Object[]>();
			page = new Page();
			if (fgoodsDTO.getPage() == 0) {
				fgoodsDTO.setPage(1);
			}
			page.setPageSize(10);
//		QueryResponse response = server.query(params);
//       // SolrDocumentList slist = response.getResults();
			String corbname = "";
			String[] field;
			String[] key;
			if(fgoodsDTO.getKeyword()!= null && fgoodsDTO.getKeyword().equals("免费样品")){
				field = new String[3];
				key = new String[3];
				if (fgoodsDTO.getKeyword() !=null && fgoodsDTO.getKeyword().equals("免费样品")){
					field[1] = "IS_PARTIAL";
					field[2] = "IS_SAMPLE";
					key[1] = "0";
					key[2] = "1";
					if(fgoodsDTO.getCname()!=null){
						field[0] = "C_NAME";
						String[] cha = fgoodsDTO.getCname().split(" ");
						corbname = cha[0];
						key[0] = "\""+cha[0]+"\"";
					}
					if(fgoodsDTO.getBname() != null){
						field[0] = "B_NAME";
						String[] cha = fgoodsDTO.getBname().split(" ");
						key[0] = "\""+cha[0]+"\"";
						corbname = cha[0];
					}
				}
			} else {
				field = new String[2];
				key = new String[2];
				String type = (String) session.get("brand");
				if(type != null && type.equals("true")){
					field[0] = "B_NAME";
				} else {
					field[0] = "name";
				}
				if(fgoodsDTO.getKeyword().split("  ").length == -1){
					key[0] = "\""+fgoodsDTO.getKeyword()+"\"";
				} else {
					key[0] = fgoodsDTO.getKeyword().equals("") ? fgoodsDTO.getKword() : fgoodsDTO.getKeyword().split("  ")[0];
				}
				if(fgoodsDTO.getCname()==null || fgoodsDTO.getCname().equals("") && (fgoodsDTO.getBname() == null ||fgoodsDTO.getBname().equals("") )){
					field = new String[1];
					key = new String[1];
					type = (String) session.get("brand");
					if(type != null && type.equals("true")){
						field[0] = "B_NAME";
					} else {
						field[0] = "name";
					}
					if(fgoodsDTO.getKeyword().split("  ").length == -1){
						key[0] = "\""+fgoodsDTO.getKeyword()+"\"";
					} else {
						key[0] = fgoodsDTO.getKeyword().equals("") ? fgoodsDTO.getKword() : fgoodsDTO.getKeyword().split("  ")[0];
					}
				}
				if(fgoodsDTO.getCname()!=null && !fgoodsDTO.getCname().equals("") && (fgoodsDTO.getBname() == null ||fgoodsDTO.getBname().equals("") )){
					field[1] = "C_NAME";
					String[] cha = fgoodsDTO.getCname().split(" ");
					corbname = cha[0];
					key[1] = "\""+cha[0]+"\"";
				}
				if(fgoodsDTO.getBname() != null && !fgoodsDTO.getBname().equals("") && ( fgoodsDTO.getCname() == null || fgoodsDTO.getCname().equals(""))){
					field[1] = "B_NAME";
					String[] cha = fgoodsDTO.getBname().split(" ");
					key[1] = "\""+cha[0]+"\"";
					corbname = cha[0];
				}
				if(fgoodsDTO.getCname()!=null && fgoodsDTO.getBname() != null && !fgoodsDTO.getCname().equals("") && !fgoodsDTO.getBname().equals("")){
					field = new String[3];
					key = new String[3];
					field[0] = "name";
					if(fgoodsDTO.getKeyword().split("  ").length == -1){
						key[0] = "\""+fgoodsDTO.getKeyword()+"\"";
					} else {
						key[0] = fgoodsDTO.getKeyword().split("  ")[0];
					}
					field[1] = "C_NAME";
					String[] cha = fgoodsDTO.getCname().split(" ");
					corbname = cha[0];
					key[1] = "\""+cha[0]+"\" OR B_NAME:"+"\""+cha[0]+"\"";
					
					field[2] = "B_NAME";
					cha = fgoodsDTO.getBname().split(" ");
					key[2] = "\""+cha[0]+"\" OR C_NAME:"+"\""+cha[0]+"\"";
					corbname += "  "+cha[0];
				}
			}
			
			
			page.setCurrentPage(fgoodsDTO.getPage());
			
//			if((String)session.get("brandSearch") == null && key[0].indexOf("=brand") != -1){
//				String[] chai = key[0].split("=");
//				key = new String[1];
//				field = new String[1];
//				key[0] = chai[0];
//				field[0] = "B_NAME";
//				session.put("brandSearch", "1");
//			} else if ((String)session.get("brandSearch") != null) {
//				field[0] = "B_NAME";
//				if(key[0].indexOf("=brand") != -1){
//					String[] c = key[0].split("=");
//					key[0] = c[0];
//				}
//			}
			replace = fgoodsManager.findReplaceByKey(key[0].replaceAll("\"", ""));
			String repStr = "";
			if(replace != null){
				if(replace.getRe1() != null){
					repStr += replace.getRe1()+",";
				}
				if(replace.getRe2() != null){
					repStr += replace.getRe2()+",";
				}
				if(replace.getRe3() != null){
					repStr += replace.getRe3()+",";
				}
				if(replace.getRe4() != null){
					repStr += replace.getRe4()+",";
				}
				if(replace.getRe5() != null){
					repStr += replace.getRe5()+",";
				}
				repStr = repStr.substring(0, repStr.length()-1);
			} else {
				repStr = null;
			}
			String timestamp = fgoodsDTO.getTimestamp();
			if(timestamp != null){
				System.out.println(timestamp);
			}
			QueryResponse qr = ts.Search(field, key, page.getFirstIndex(), page.getPageSize(), sortfield, flag, hightlight,repStr);
			SolrDocumentList slist=qr.getResults();//获取按条件查询筛选后的结果（ solr自带的方法）
			
			
			
			
			Long numfound = slist.getNumFound();//结果条数
			page.setTotalCount(numfound);
			
			
			
//			Map<String, Map<String, List<String>>> map = qr.getHighlighting();
//			for (Map<String, List<String>> solrDocument : map.values()) {
//				for (List<String> li : solrDocument.values()) {
//					System.out.println(li.get(0));
//					ctGoods[1]=li.get(0);
//				}
//			}
			for (SolrDocument solrDocument : slist) {//转换为对象
				Object[] ctGoods = new Object[25];
				this.imgList = fgoodsManager.getImg(Long.valueOf(solrDocument.getFirstValue("id").toString()));
				String path = "";
				if(imgList.size() == 0){
					path =systemInfo.getCtImgUrl() +"//"+"ct_s.gif";
				} else {
					path =systemInfo.getCtImgUrl() +"//"+imgList.get(3).getPUrl();
				}
				ctGoods[0]=solrDocument.getFirstValue("id");
				if(ctGoods[1] == null){
					ctGoods[1]=solrDocument.getFirstValue("G_NAME");
				}
				ctGoods[2]=solrDocument.getFirstValue("G_SN");
				ctGoods[5]=solrDocument.getFirstValue("SHOP_PRICE");
				ctGoods[6]=solrDocument.getFirstValue("B_NAME");
				ctGoods[7]=solrDocument.getFirstValue("G_UNIT");
				ctGoods[8]=solrDocument.getFirstValue("PACK1");
				ctGoods[9]=solrDocument.getFirstValue("PACK1_NUM");
				ctGoods[10]=solrDocument.getFirstValue("G_NUM");
				ctGoods[11]=solrDocument.getFirstValue("IS_SAMPLE");
				ctGoods[12]=solrDocument.getFirstValue("IS_PARTIAL");
				ctGoods[13]=solrDocument.getFirstValue("ATTR_VALUE");
				//goodsBName.add((String) solrDocument.getFirstValue("B_NAME"));
				//goodsCName.add((String) solrDocument.getFirstValue("C_NAME"));
				ctGoods[16]=path;
				rangePricesList = basicManager.getAllRanPrice(Long.valueOf(solrDocument.getFirstValue("id").toString()));
				ranListDTO = new ArrayList<CtRangePriceDTO>();
				int cunsim = 0;
				int cunPar = 0;
				for (int i = 0; i < rangePricesList.size(); i++) {
					CtRangePriceDTO rangePriceDTO = new CtRangePriceDTO();
					Double parsnum = Double.valueOf(rangePricesList.get(i).getParSNum() == null ? 0 : rangePricesList.get(i).getParSNum()) / 1000;
					Double parenum = Double.valueOf(rangePricesList.get(i).getParENum()==null ? 0 : rangePricesList.get(i).getParENum()) / 1000;
					Double parinc = Double.valueOf(rangePricesList.get(i).getParIncrease() == null ? 0 : rangePricesList.get(i).getParIncrease()) / 1000;
					rangePriceDTO.setGId(rangePricesList.get(i).getGId());
					rangePriceDTO.setRPid(rangePricesList.get(i).getRPid());
					rangePriceDTO.setSimSNum(rangePricesList.get(i).getSimSNum() == null ? 0 : rangePricesList.get(i).getSimSNum());
					rangePriceDTO.setSimENum(rangePricesList.get(i).getSimENum() == null ? 0 : rangePricesList.get(i).getSimENum());
					rangePriceDTO.setSimRPrice(rangePricesList.get(i).getSimRPrice() == null ? 0 : rangePricesList.get(i).getSimRPrice());
					rangePriceDTO.setSimIncrease(rangePricesList.get(i).getSimIncrease() == null ? 0 : rangePricesList.get(i).getSimIncrease());
					rangePriceDTO.setParENum(df.format(parenum.equals(0)? "" : parenum));
					rangePriceDTO.setParIncrease(df.format(parinc));
					rangePriceDTO.setParSNum(df.format(parsnum));
					rangePriceDTO.setParRPrice(rangePricesList.get(i).getParRPrice());
					rangePriceDTO.setInsTime(rangePricesList.get(i).getInsTime());
					ranListDTO.add(rangePriceDTO);
					if(rangePricesList.get(i).getSimSNum() != null){
						cunsim++;
					}
					if(rangePricesList.get(i).getParSNum() != null){
						cunPar++;
					}
				}
				if(cunsim != 0){
					ctGoods[24]="yes";
					session.put("cunsim", "yes");
				} else {
					ctGoods[24]="no";
					session.put("cunsim", "no");
				}
				if(cunPar != 0){
					ctGoods[23]="yes";
					session.put("cunPar", "yes");
				} else {
					ctGoods[23]="no";
					session.put("cunPar", "no");
				}
				goodsList.add(ctGoods);
				priceAllList.add(ranListDTO);
			}
			page.setTotalPage(page.getTotalPage());
			String kk = fgoodsDTO.getKeyword().indexOf("  ") == -1 ? fgoodsDTO.getKeyword() :fgoodsDTO.getKeyword().split("  ")[0];
			for (int i = 1; i < key.length; i++) {
				kk+=" "+key[i]+" ";
			}
			this.request.put("keyw", kk);
			
			session.put("typeName", fgoodsDTO.getKeyword().indexOf("  ") == -1 ? fgoodsDTO.getKeyword() :fgoodsDTO.getKeyword().split("  ")[0]);
			this.request.put("pages", page);
			
			String tkword = fgoodsDTO.getKeyword().indexOf("  ") == -1 ? fgoodsDTO.getKeyword() :fgoodsDTO.getKeyword().split("  ")[0];
			if (fgoodsDTO.getKeyword()==null || fgoodsDTO.getKeyword().equals("")){
				tkword = fgoodsDTO.getKeyword();
			}
			this.request.put("tkword", tkword + "  " + corbname);
			if(this.session.get("userinfosession")==null){
				session.put("loginLiebie", fgoodsDTO);
			}
			searchList = goodsList;
			//goodsBName = removeDuplicateWithOrder(goodsBName);
			this.request.put("tempSearch", corbname);
			
			//获取分类以及数量
				String[] cor = corbname.split("  ");
				String[] tempName = new String[2];
				replace = fgoodsManager.findReplaceByKey(key[0].replaceAll("\"", ""));
				String repStr1 = "";
				if(replace != null){
					if(replace.getRe1() != null){
						repStr1 += replace.getRe1()+",";
					}
					if(replace.getRe2() != null){
						repStr1 += replace.getRe2()+",";
					}
					if(replace.getRe3() != null){
						repStr1 += replace.getRe3()+",";
					}
					if(replace.getRe4() != null){
						repStr1 += replace.getRe4()+",";
					}
					if(replace.getRe5() != null){
						repStr1 += replace.getRe5()+",";
					}
					repStr1 = repStr1.substring(0, repStr1.length()-1);
				} else {
					repStr1 = null;
				}
				List<FacetField> cnameList = ts.Search11(field, key, page.getFirstIndex(), page.getPageSize(), sortfield, flag, hightlight,"C_NAME",repStr1);
				if(true){
				if(cnameList.size() == 2){
					for (int j = 0; j < cnameList.get(1).getValues().size(); j++) {
						cnameTemp.add(cnameList.get(1).getValues().get(j).getName() + " ( "+cnameList.get(1).getValues().get(j).getCount()+" ) ");
						goodsCName.add(cnameList.get(1).getValues().get(j).getName());
						for (int i = 0; i < cor.length; i++) {
							if(cor[i].equals(cnameList.get(1).getValues().get(j).getName())){
								tempName[i] = cnameList.get(1).getValues().get(j).getName() + " ( "+cnameList.get(1).getValues().get(j).getCount()+" ) ";
							}
						}
						System.out.println("["+ cnameList.get(1).getValues().get(j).getName() + "]  " + cnameList.get(1).getValues().get(j).getCount());
					}
					for (int j = 0; j < cnameList.get(0).getValues().size(); j++) {
						goodsBName.add(cnameList.get(0).getValues().get(j).getName());
						bnameTemp.add(cnameList.get(0).getValues().get(j).getName() + " ( "+cnameList.get(0).getValues().get(j).getCount()+" ) ");
						for (int i = 0; i < cor.length; i++) {
							if(cor[i].equals(cnameList.get(0).getValues().get(j).getName())){
								tempName[i] = cnameList.get(0).getValues().get(j).getName() + " ( "+cnameList.get(0).getValues().get(j).getCount()+" ) ";
							}
						}
						System.out.println("["+ cnameList.get(0).getValues().get(j).getName() + "]  " + cnameList.get(0).getValues().get(j).getCount());
					}
				} else {
					for (int j = 0; j < cnameList.get(0).getValues().size(); j++) {
						goodsBName.add(cnameList.get(0).getValues().get(j).getName());
						bnameTemp.add(cnameList.get(0).getValues().get(j).getName() + " ( "+cnameList.get(0).getValues().get(j).getCount()+" ) ");
						for (int i = 0; i < cor.length; i++) {
							if(cor[i].equals(cnameList.get(0).getValues().get(j).getName())){
								tempName[i] = cnameList.get(0).getValues().get(j).getName() + " ( "+cnameList.get(0).getValues().get(j).getCount()+" ) ";
							}
						}
						System.out.println("["+ cnameList.get(0).getValues().get(j).getName() + "]  " + cnameList.get(0).getValues().get(j).getCount());
					}
				}
				this.request.put("tempName", tempName);
			}
			for (int i = 0; i < cnameTemp.size(); i++) {
				//goodsCNameStr.add(goodsCName.get(i)+" (" + numfound + ")");
				goodsCNameStr.add(cnameTemp.get(i));
			}
			for (int i = 0; i < bnameTemp.size(); i++) {
				//goodsCNameStr.add(goodsCName.get(i)+" (" + numfound + ")");
				goodsBNameStr.add(bnameTemp.get(i));
			}
			
			session.put("goodsCNameStr", goodsCNameStr);
			session.put("goodsBNameStr", goodsBNameStr);
			searchKeyWord = fgoodsManager.findByKeyWord(fgoodsDTO.getKeyword());
			if(searchKeyWord == null){
				if(goodsList != null && goodsList.size() > 0){
					searchKeyWord = new CtSearchKeyword();
					searchKeyWord.setSKeyword(fgoodsDTO.getKeyword());
					searchKeyWord.setSGoodsnum(goodsList.size());
					searchKeyWord.setSNum(1);
					try {
						fgoodsManager.GoodsKeyNew(searchKeyWord);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					if(!fgoodsDTO.getKeyword().trim().equals("")){
						searchCollect = (CtSearchCollect) fgoodsManager.findByTypeName(fgoodsDTO.getKeyword(), "searchKey", "CtSearchCollect", 0);
						if(searchCollect == null){
							searchCollect = new CtSearchCollect();
							searchCollect.setSeachSum(1);
						} else {
							searchCollect.setSeachSum(searchCollect.getSeachSum() + 1);
						}
						searchCollect.setIsOptimise("0");
						searchCollect.setSearchKey(fgoodsDTO.getKeyword());
						searchCollect.setSearchTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						fgoodsManager.merge(searchCollect);
					}
				}
				
			} else {
				searchKeyWord.setSNum(searchKeyWord.getSNum() + 1);
				fgoodsManager.addGoodsKey(searchKeyWord);
			}
			searchKeywordsList = fgoodsManager.findKeyWordOrderTop4();
			session.put("searchKeywordsList", searchKeywordsList);
			return SUCCESS;
		} else {
			session.remove("brand");
			if(this.session.get("userinfosession")!=null){
				CtUser cu = (CtUser)this.session.get("userinfosession");	
				ctUser = userManager.getCtUserByUUserid(cu.getUUserid());
			}
			String kword = fgoodsDTO.getKword();//用于关键字
			//kword = java.net.URLDecoder.decode(fgoodsDTO.getKword(),"UTF-8");
//		if (kword !=null)
//			kword = java.net.URLDecoder.decode(kword.replaceAll("%", "%25"),"UTF-8");
			
			String keyword = fgoodsDTO.getKeyword();//用于类别
			//keyword = URLDecoder.decode(keyword , "utf-8");
			if (keyword !=null)
				keyword = java.net.URLDecoder.decode(keyword,"UTF-8");
			
			if(keyword != null && isMessyCode(keyword)){
				String s = new String(keyword.getBytes("ISO-8859-1"),"UTF-8");
				keyword = s;
			}
//		if(kword != null){
//			kword = new String(kword.getBytes("iso8859-1"), "utf-8");
//		}
//		if(keyword != null){
//			keyword = new String(keyword.getBytes("iso8859-1"), "utf-8");
//		}
//		
//		if(kword == null){
//			kword = "电阻";
//		}else if (kword != null && keyword != null  && keyword.equals("全部产品分类")){
//			keyword = kword;
//			kword = null;
//		} else if(kword != null && keyword == null) {
//			keyword = kword;
//		}
//		
			
			
			
			String newPrice = "";
			if(fgoodsDTO.getPrice1() != null && fgoodsDTO.getPrice2() != null && !fgoodsDTO.getPrice1().equals("") && !fgoodsDTO.getPrice2().equals("")){
				newPrice = fgoodsDTO.getPrice1() + " TO " + fgoodsDTO.getPrice2();
//			ts.querytop20();
				String[] field;
				String[] key;
				
				
				//第一种，从检索框输入+分类名称
				
				if (keyword !=null && kword != null){
					
					if (keyword.equals("全部产品分类")){
						
						field = new String[2];
						key = new String[2];
						field[0] = "name";
						key[0] = kword;
						field[1] = "SHOP_PRICE";
						key[1] = newPrice;
					}else{
						
						field = new String[3];
						key = new String[3];
						field[0] = "name";
						field[1] = "C_NAME";
						field[2] = "SHOP_PRICE";
						key[0] = kword;
						key[1] = "\""+keyword+"\"";
						key[2] = newPrice;
					}
					
				}else{
					//第二种，无输入框，直接分类
					
					//if (keyword !=null && kword == null){
					field = new String[2];
					key = new String[2];
					field[0] = "C_NAME";
					field[1] = "SHOP_PRICE";
					key[0] = "\""+keyword+"\"";
					key[1] = newPrice;
					
					if (keyword !=null && keyword.equals("免费样品")){
						
						field = new String[3];
						key = new String[3];
						field[0] = "IS_PARTIAL";
						field[1] = "IS_SAMPLE";
						field[2] = "SHOP_PRICE";
						key[0] = "0";
						key[1] = "1";
						key[2] = newPrice;
					}
					
				}
				
				
				
//			
//			
//			if(cid == null && kword != null){
//				field = new String[3];
//				key = new String[3];
//				field[0] = "name";
//				field[1] = "C_NAME";
//				field[2] = "SHOP_PRICE";
//				key[0] = kword;
//				key[1] = "\""+keyword+"\"";
//				key[2] = newPrice;
//			} else if(kword != null && keyword != null) {
//				field = new String[2];
//				key = new String[2];
//				field[0] = "C_NAME";
//				field[1] = "SHOP_PRICE";
//				key[0] = "\""+keyword+"\"";
//				key[1] = newPrice;
//			} else {
//				field = new String[4];
//				key = new String[4];
//				field[0] = "name";
//				field[1] = "C_NAME";
//				field[2] = "SHOP_PRICE";
//				field[3] = "C_ID";
//				key[0] = kword;
//				key[1] = "\""+keyword+"\"";
//				key[2] = newPrice;
//				key[3] = cid.toString();
//			}
				String[] sortfield={"id","name"};
				List<ViewGoodsList> list= new ArrayList<ViewGoodsList>();
				Boolean[] flag={true,true};//flag 需要排序的字段的排序方式如果为true 升序 如果为false 降序
				Boolean hightlight=true;//hightlight 是否需要高亮显示
				
				try{
					List<Object[]> goodsList = new ArrayList<Object[]>();
					page = new Page();
					if (fgoodsDTO.getPage() == 0) {
						fgoodsDTO.setPage(1);
					}
					page.setPageSize(10);
//				QueryResponse response = server.query(params);
//	           // SolrDocumentList slist = response.getResults();
					
					page.setCurrentPage(fgoodsDTO.getPage());
					replace = fgoodsManager.findReplaceByKey(key[0].replaceAll("\"", ""));
					String repStr = "";
					if(replace != null){
						if(replace.getRe1() != null){
							repStr += replace.getRe1()+",";
						}
						if(replace.getRe2() != null){
							repStr += replace.getRe2()+",";
						}
						if(replace.getRe3() != null){
							repStr += replace.getRe3()+",";
						}
						if(replace.getRe4() != null){
							repStr += replace.getRe4()+",";
						}
						if(replace.getRe5() != null){
							repStr += replace.getRe5()+",";
						}
						repStr = repStr.substring(0, repStr.length()-1);
					} else {
						repStr = null;
					}
					QueryResponse qr = ts.Search(field, key, page.getFirstIndex(), page.getPageSize(), sortfield, flag, hightlight,repStr);
					SolrDocumentList slist=qr.getResults();//获取按条件查询筛选后的结果（ solr自带的方法）
					
					
					
					
					Long numfound = slist.getNumFound();//结果条数
					page.setTotalCount(numfound);
					
//					Map<String, Map<String, List<String>>> map = qr.getHighlighting();
//					for (Map<String, List<String>> solrDocument : map.values()) {
//						for (List<String> li : solrDocument.values()) {
//							System.out.println(li.get(0));
//							ctGoods[1]=li.get(0);
//						}
//					}
					for (SolrDocument solrDocument : slist) {//转换为对象
						Object[] ctGoods = new Object[25];
						this.imgList = fgoodsManager.getImg(Long.valueOf(solrDocument.getFirstValue("id").toString()));
						String path = "";
						if (imgList!=null && imgList.size()!=0){
							path = systemInfo.getCtImgUrl() +"//"+imgList.get(3).getPUrl();
						}else{
							path = systemInfo.getCtImgUrl() +"//ct_s.gif";
						}
						ctGoods[0]=solrDocument.getFirstValue("id");
						if(ctGoods[1] == null){
							ctGoods[1]=solrDocument.getFirstValue("G_NAME");
						}
						ctGoods[2]=solrDocument.getFirstValue("G_SN");
						ctGoods[5]=solrDocument.getFirstValue("SHOP_PRICE");
						ctGoods[6]=solrDocument.getFirstValue("B_NAME");
						ctGoods[7]=solrDocument.getFirstValue("G_UNIT");
						ctGoods[8]=solrDocument.getFirstValue("PACK1");
						ctGoods[9]=solrDocument.getFirstValue("PACK1_NUM");
						ctGoods[10]=solrDocument.getFirstValue("G_NUM");
						ctGoods[11]=solrDocument.getFirstValue("IS_SAMPLE");
						ctGoods[12]=solrDocument.getFirstValue("IS_PARTIAL");
						ctGoods[13]=solrDocument.getFirstValue("ATTR_VALUE");
						ctGoods[16]=path;
						//goodsBName.add((String) solrDocument.getFirstValue("B_NAME"));
						//goodsCName.add((String) solrDocument.getFirstValue("C_NAME"));
						rangePricesList = basicManager.getAllRanPrice(Long.valueOf(solrDocument.getFirstValue("id").toString()));
						rangePricesList = basicManager.getAllRanPrice(Long.valueOf(solrDocument.getFirstValue("id").toString()));
						ranListDTO = new ArrayList<CtRangePriceDTO>();
						int cunsim = 0;
						int cunPar = 0;
						for (int i = 0; i < rangePricesList.size(); i++) {
							CtRangePriceDTO rangePriceDTO = new CtRangePriceDTO();
							Double parsnum = Double.valueOf(rangePricesList.get(i).getParSNum() == null ? 0 : rangePricesList.get(i).getParSNum()) / 1000;
							Double parenum = Double.valueOf(rangePricesList.get(i).getParENum()==null ? 0 : rangePricesList.get(i).getParENum()) / 1000;
							Double parinc = Double.valueOf(rangePricesList.get(i).getParIncrease() == null ? 0 : rangePricesList.get(i).getParIncrease()) / 1000;
							rangePriceDTO.setGId(rangePricesList.get(i).getGId());
							rangePriceDTO.setRPid(rangePricesList.get(i).getRPid());
							rangePriceDTO.setSimSNum(rangePricesList.get(i).getSimSNum() == null ? 0 : rangePricesList.get(i).getSimSNum());
							rangePriceDTO.setSimENum(rangePricesList.get(i).getSimENum() == null ? 0 : rangePricesList.get(i).getSimENum());
							rangePriceDTO.setSimRPrice(rangePricesList.get(i).getSimRPrice() == null ? 0 : rangePricesList.get(i).getSimRPrice());
							rangePriceDTO.setSimIncrease(rangePricesList.get(i).getSimIncrease() == null ? 0 : rangePricesList.get(i).getSimIncrease());
							rangePriceDTO.setParENum(df.format(parenum.equals(0)? "" : parenum));
							rangePriceDTO.setParIncrease(df.format(parinc));
							rangePriceDTO.setParSNum(df.format(parsnum));
							rangePriceDTO.setParRPrice(rangePricesList.get(i).getParRPrice());
							rangePriceDTO.setInsTime(rangePricesList.get(i).getInsTime());
							ranListDTO.add(rangePriceDTO);
							if(rangePricesList.get(i).getSimSNum() != null){
								cunsim++;
							}
							if(rangePricesList.get(i).getParSNum() != null){
								cunPar++;
							}
						}
						if(cunsim != 0){
							ctGoods[24]="yes";
							session.put("cunsim", "yes");
						} else {
							ctGoods[24]="no";
							session.put("cunsim", "no");
						}
						if(cunPar != 0){
							ctGoods[23]="yes";
							session.put("cunPar", "yes");
						} else {
							ctGoods[23]="no";
							session.put("cunPar", "no");
						}
						goodsList.add(ctGoods);
						priceAllList.add(ranListDTO);
					}
					page.setTotalPage(page.getTotalPage());
					
					String tkword = kword;
					if (kword==null || kword.equals("")){
						tkword = keyword;
					}
					this.request.put("tkword", tkword);
					if(this.session.get("userinfosession")==null){
						session.put("loginCunKword", tkword);
					}
					//this.request.put("keyw", kword);
					session.put("typeName", kword);
					this.request.put("pages", page);
					searchList = goodsList;
					searchKeyWord = fgoodsManager.findByKeyWord(kword);
					if(searchKeyWord == null){
						if(goodsList != null && goodsList.size() > 0){
							searchKeyWord = new CtSearchKeyword();
							searchKeyWord.setSKeyword(kword);
							searchKeyWord.setSGoodsnum(goodsList.size());
							searchKeyWord.setSNum(1);
							try {
								fgoodsManager.GoodsKeyNew(searchKeyWord);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else {
							if(!kword.trim().equals("")){
								searchCollect = (CtSearchCollect) fgoodsManager.findByTypeName(kword, "searchKey", "CtSearchCollect", 0);
								if(searchCollect == null){
									searchCollect = new CtSearchCollect();
									searchCollect.setSeachSum(1);
								} else {
									searchCollect.setSeachSum(searchCollect.getSeachSum() + 1);
								}
								searchCollect.setIsOptimise("0");
								searchCollect.setSearchKey(kword);
								searchCollect.setSearchTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
								fgoodsManager.merge(searchCollect);
							}
						}
					} else {
						searchKeyWord.setSNum(searchKeyWord.getSNum() + 1);
						fgoodsManager.addGoodsKey(searchKeyWord);
					}
					searchKeywordsList = fgoodsManager.findKeyWordOrderTop4();
					session.put("searchKeywordsList", searchKeywordsList);
					//goodsBName = removeDuplicateWithOrder(goodsBName);
					//goodsCName = removeDuplicateWithOrder(goodsCName);
					//获取分类以及数量
					String corbname = "";
					String tempName = "";
					replace = fgoodsManager.findReplaceByKey(key[0].replaceAll("\"", ""));
					String repStr1 = "";
					if(replace != null){
						if(replace.getRe1() != null){
							repStr1 += replace.getRe1()+",";
						}
						if(replace.getRe2() != null){
							repStr1 += replace.getRe2()+",";
						}
						if(replace.getRe3() != null){
							repStr1 += replace.getRe3()+",";
						}
						if(replace.getRe4() != null){
							repStr1 += replace.getRe4()+",";
						}
						if(replace.getRe5() != null){
							repStr1 += replace.getRe5()+",";
						}
						repStr1 = repStr1.substring(0, repStr1.length()-1);
					} else {
						repStr1 = null;
					}
					List<FacetField> cnameList = ts.Search11(field, key, page.getFirstIndex(), page.getPageSize(), sortfield, flag, hightlight,"C_NAME",repStr1);
					if(cnameList.size() == 2){
						for (int j = 0; j < cnameList.get(1).getValues().size(); j++) {
							cnameTemp.add(cnameList.get(1).getValues().get(j).getName() + " ( "+cnameList.get(1).getValues().get(j).getCount()+" ) ");
							goodsCName.add(cnameList.get(1).getValues().get(j).getName());
							if(corbname.equals(cnameList.get(0).getValues().get(j).getName())){
								tempName = cnameList.get(0).getValues().get(j).getName() + " ( "+cnameList.get(0).getValues().get(j).getCount()+" ) ";
							}
							System.out.println("["+ cnameList.get(1).getValues().get(j).getName() + "]  " + cnameList.get(1).getValues().get(j).getCount());
						}
						for (int j = 0; j < cnameList.get(0).getValues().size(); j++) {
							goodsBName.add(cnameList.get(0).getValues().get(j).getName());
							bnameTemp.add(cnameList.get(0).getValues().get(j).getName() + " ( "+cnameList.get(0).getValues().get(j).getCount()+" ) ");
							if(corbname.equals(cnameList.get(0).getValues().get(j).getName())){
								tempName = cnameList.get(0).getValues().get(j).getName() + " ( "+cnameList.get(0).getValues().get(j).getCount()+" ) ";
							}
							System.out.println("["+ cnameList.get(0).getValues().get(j).getName() + "]  " + cnameList.get(0).getValues().get(j).getCount());
						}
					} else {
						for (int j = 0; j < cnameList.get(0).getValues().size(); j++) {
							goodsBName.add(cnameList.get(0).getValues().get(j).getName());
							bnameTemp.add(cnameList.get(0).getValues().get(j).getName() + " ( "+cnameList.get(0).getValues().get(j).getCount()+" ) ");
							if(corbname.equals(cnameList.get(0).getValues().get(j).getName())){
								tempName = cnameList.get(0).getValues().get(j).getName() + " ( "+cnameList.get(0).getValues().get(j).getCount()+" ) ";
							}
							System.out.println("["+ cnameList.get(0).getValues().get(j).getName() + "]  " + cnameList.get(0).getValues().get(j).getCount());
						}
					}
					for (int i = 0; i < cnameTemp.size(); i++) {
						//goodsCNameStr.add(goodsCName.get(i)+" (" + numfound + ")");
						goodsCNameStr.add(cnameTemp.get(i));
					}
					for (int i = 0; i < bnameTemp.size(); i++) {
						//goodsCNameStr.add(goodsCName.get(i)+" (" + numfound + ")");
						goodsBNameStr.add(bnameTemp.get(i));
					}
					this.request.put("tempName", tempName);
					session.put("goodsCNameStr", goodsCNameStr);
					session.put("goodsBNameStr", goodsBNameStr);
					
					return SUCCESS;
				}catch(Exception e){
					//e.printStackTrace();
					String errorTotle = "请输入搜索关键字";
					String errorMess = " ";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					return SUCCESS;
				}
			} else {
//			ts.querytop20();
				
				String[] field;
				String[] key;
//			if(cid == null && kword != null){
//				field = new String[2];
//				key = new String[2];
//				field[0] = "name";
//				field[1] = "C_NAME";
//				key[0] = kword;
//				key[1] = "\""+keyword+"\"";
//			} else if(kword != null && keyword != null) {
//				field = new String[1];
//				key = new String[1];
//				field[0] = "C_NAME";
//				key[0] = "\""+keyword+"\"";
//			}  else {
//				field = new String[3];
//				key = new String[3];
//				field[0] = "name";
//				field[1] = "C_NAME";
//				
//				key[0] = kword;
//				key[1] = keyword;
//				if (cid !=null){
//					field[2] = "C_ID";
//					key[2] = cid.toString();
//				}
//					
//			}
				
				
				//第一种，从检索框输入+分类名称
				
				if ((keyword !=null && kword != null) && (!keyword.equals("")) && !kword.equals("")){
					
					if (keyword.equals("全部产品分类")){
						int i = kword.trim().indexOf("  ");
						field = new String[1];
						key = new String[1];
						if (i == -1) {
							field[0] = "name";
							key[0] = ""+kword+"";
						} else {
							field[0] = "name";
							field = new String[2];
							field[0] = "name";
							field[1] = "C_NAME";
							key = new String[2];
							String[] chai = kword.split("  ");
							key[0]=chai[0];
							key[1] = chai[1] + " OR B_NAME:"+chai[1];
							key[1] = "("+key[1]+")";
						}
						//key[0] = kword;
					}else{
						
						field = new String[2];
						key = new String[2];
						if(keyword.equals(kword)){
							field = new String[1];
							key = new String[1];
							field[0] = "name";
							key[0] = kword;
						} else {
							int i = kword.indexOf(" ");
							if (i == -1) {
								field[0] = "name";
							} else {
								field[0] = "name";
							}
							field[1] = "C_NAME";
							key[0] = kword;
							key[1] = "\""+keyword+"\"";
						}
					}
					
				}else{
					//第二种，无输入框，直接分类
					
					//if (keyword !=null && kword == null){
					field = new String[1];
					key = new String[1];
					field[0] = "name";
					key[0] = "\""+keyword+"\"";
					
					if (keyword !=null && keyword.equals("免费样品")){
						
						field = new String[2];
						key = new String[2];
						field[0] = "IS_PARTIAL";
						field[1] = "IS_SAMPLE";
						key[0] = "0";
						key[1] = "1";
					}
					
				}
				String[] sortfield={"id","name"};
				List<ViewGoodsList> list= new ArrayList<ViewGoodsList>();
				Boolean[] flag={true,true};//flag 需要排序的字段的排序方式如果为true 升序 如果为false 降序
				Boolean hightlight=true;//hightlight 是否需要高亮显示
				
				try{
					List<Object[]> goodsList = new ArrayList<Object[]>();
					page = new Page();
					if (fgoodsDTO.getPage() == 0) {
						fgoodsDTO.setPage(1);
					}
					page.setPageSize(10);
					page.setCurrentPage(fgoodsDTO.getPage());
					if(this.session.get("userinfosession")!=null){
						if(key.length == 1){
							if(key[0].equals("\"null\"")){
								key[0] = (String) session.get("loginCunKword");
								keyword = key[0];
							}
						}
						session.remove("loginCunKword");
					}
//					if((String)session.get("brandSearch") == null && key[0].indexOf("=brand") != -1){
//						String[] chai = key[0].split("=");
//						key = new String[1];
//						field = new String[1];
//						key[0] = chai[0];
//						field[0] = "B_NAME";
//						session.put("brandSearch", "1");
//					} else if ((String)session.get("brandSearch") != null) {
//						field[0] = "B_NAME";
//						if(key[0].indexOf("=brand") != -1){
//							String[] c = key[0].split("=");
//							key[0] = c[0];
//						}
//					}
					replace = fgoodsManager.findReplaceByKey(key[0].replaceAll("\"", ""));
					String repStr = "";
					if(replace != null){
						if(replace.getRe1() != null){
							repStr += replace.getRe1()+",";
						}
						if(replace.getRe2() != null){
							repStr += replace.getRe2()+",";
						}
						if(replace.getRe3() != null){
							repStr += replace.getRe3()+",";
						}
						if(replace.getRe4() != null){
							repStr += replace.getRe4()+",";
						}
						if(replace.getRe5() != null){
							repStr += replace.getRe5()+",";
						}
						repStr = repStr.substring(0, repStr.length()-1);
					} else {
						repStr = null;
					}
					String timestamp = fgoodsDTO.getTimestamp();
					Long nowDateTime = new Date().getTime();
					QueryResponse qr = null;
					if(timestamp != null){
						String timeStr = nowDateTime.toString().substring(0, 9);
						String webTimeStr  = timestamp.substring(0,9);
						if(timeStr.equals(webTimeStr)){
							System.out.println(true);
							String s = "";
							if(fgoodsDTO.getKeyword() != null && isMessyCode(fgoodsDTO.getKeyword())){
								s = new String(fgoodsDTO.getKeyword().getBytes("ISO-8859-1"),"UTF-8");
							}
							String[] k = s.split(" ");
							String str = "";
							for (int i = 0; i < k.length; i++) {
								str += "(name:"+k[i] + " OR B_NAME:"+k[i]+") OR "; 
							}
							str = str.substring(0, str.length()-4);
							qr = ts.SearchBomGoodsNum(str,page.getFirstIndex(), page.getPageSize(),true);
						} else {
							qr = ts.Search(field, key, page.getFirstIndex(), page.getPageSize(), sortfield, flag, hightlight,repStr);
						}
						System.out.println("timestamp" + timestamp);
						System.out.println("nowDateTime" + nowDateTime);
					} else {
						qr = ts.Search(field, key, page.getFirstIndex(), page.getPageSize(), sortfield, flag, hightlight,repStr);
					}
					SolrDocumentList slist=qr.getResults();//获取按条件查询筛选后的结果（ solr自带的方法）
					Long numfound = slist.getNumFound();//结果条数
					page.setTotalCount(numfound);
					
//					Map<String, Map<String, List<String>>> map = qr.getHighlighting();
//					for (Map<String, List<String>> solrDocument : map.values()) {
//						for (List<String> li : solrDocument.values()) {
//							System.out.println(li.get(0));
//							ctGoods[1]=li.get(0);
//						}
//					}
					for (SolrDocument solrDocument : slist) {//转换为对象
						Object[] ctGoods = new Object[25];
						String path = "";
						this.imgList = fgoodsManager.getImg(Long.valueOf(solrDocument.getFirstValue("id").toString()));
						//System.out.println(imgList==null);
						if (imgList!=null && imgList.size()!=0){
							path = systemInfo.getCtImgUrl() +"//"+imgList.get(3).getPUrl();
						}else{
							path = systemInfo.getCtImgUrl() +"//ct_s.gif";
						}
						
						ctGoods[0]=solrDocument.getFirstValue("id");
						if(ctGoods[1] == null){
							ctGoods[1]=solrDocument.getFirstValue("G_NAME");
						}
						ctGoods[2]=solrDocument.getFirstValue("G_SN");
						ctGoods[5]=solrDocument.getFirstValue("SHOP_PRICE");
						ctGoods[6]=solrDocument.getFirstValue("B_NAME");
						ctGoods[7]=solrDocument.getFirstValue("G_UNIT");
						ctGoods[8]=solrDocument.getFirstValue("PACK1");
						ctGoods[9]=solrDocument.getFirstValue("PACK1_NUM");
						ctGoods[10]=solrDocument.getFirstValue("G_NUM");
						ctGoods[11]=solrDocument.getFirstValue("IS_SAMPLE");
						ctGoods[12]=solrDocument.getFirstValue("IS_PARTIAL");
						ctGoods[13]=solrDocument.getFirstValue("ATTR_VALUE");
						ctGoods[16]=path;
						ctGoods[18]=solrDocument.getFirstValue("C_NAME");
						//goodsBName.add((String) solrDocument.getFirstValue("B_NAME"));
						//goodsCName.add((String) solrDocument.getFirstValue("C_NAME"));
						rangePricesList = basicManager.getAllRanPrice(Long.valueOf(solrDocument.getFirstValue("id").toString()));
						ranListDTO = new ArrayList<CtRangePriceDTO>();
						int cunsim = 0;
						int cunPar = 0;
						for (int i = 0; i < rangePricesList.size(); i++) {
							CtRangePriceDTO rangePriceDTO = new CtRangePriceDTO();
							Double parsnum = Double.valueOf(rangePricesList.get(i).getParSNum() == null ? 0 : rangePricesList.get(i).getParSNum()) / 1000;
							Double parenum = Double.valueOf(rangePricesList.get(i).getParENum()==null ? 0 : rangePricesList.get(i).getParENum()) / 1000;
							Double parinc = Double.valueOf(rangePricesList.get(i).getParIncrease() == null ? 0 : rangePricesList.get(i).getParIncrease()) / 1000;
							rangePriceDTO.setGId(rangePricesList.get(i).getGId());
							rangePriceDTO.setRPid(rangePricesList.get(i).getRPid());
							rangePriceDTO.setSimSNum(rangePricesList.get(i).getSimSNum() == null ? 0 : rangePricesList.get(i).getSimSNum());
							rangePriceDTO.setSimENum(rangePricesList.get(i).getSimENum() == null ? 0 : rangePricesList.get(i).getSimENum());
							rangePriceDTO.setSimRPrice(rangePricesList.get(i).getSimRPrice() == null ? 0 : rangePricesList.get(i).getSimRPrice());
							rangePriceDTO.setSimIncrease(rangePricesList.get(i).getSimIncrease() == null ? 0 : rangePricesList.get(i).getSimIncrease());
							rangePriceDTO.setParENum(df.format(parenum.equals(0)? "" : parenum));
							rangePriceDTO.setParIncrease(df.format(parinc));
							rangePriceDTO.setParSNum(df.format(parsnum));
							rangePriceDTO.setParRPrice(rangePricesList.get(i).getParRPrice());
							rangePriceDTO.setInsTime(rangePricesList.get(i).getInsTime());
							ranListDTO.add(rangePriceDTO);
							if(rangePricesList.get(i).getSimSNum() != null){
								cunsim++;
							}
							if(rangePricesList.get(i).getParSNum() != null){
								cunPar++;
							}
						}
						if(cunsim != 0){
							ctGoods[24]="yes";
							session.put("cunsim", "yes");
						} else {
							ctGoods[24]="no";
							session.put("cunsim", "no");
						}
						if(cunPar != 0){
							ctGoods[23]="yes";
							session.put("cunPar", "yes");
						} else {
							ctGoods[23]="no";
							session.put("cunPar", "no");
						}
						goodsList.add(ctGoods);
						priceAllList.add(ranListDTO);
					}
					if(keyword != null && !keyword.equals("null")){
						if(keyword.equals("全部产品分类")){
							keyword = kword;
						}
						searchKeyWord = fgoodsManager.findByKeyWord(keyword);
						if(searchKeyWord == null){
							if(goodsList != null && goodsList.size() > 0){
								searchKeyWord = new CtSearchKeyword();
								searchKeyWord.setSKeyword(keyword);
								searchKeyWord.setSGoodsnum(goodsList.size());
								searchKeyWord.setSNum(1);
								try {
									fgoodsManager.GoodsKeyNew(searchKeyWord);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							} else {
								if(!keyword.trim().equals("")){
									searchCollect = (CtSearchCollect) fgoodsManager.findByTypeName(keyword, "searchKey", "CtSearchCollect", 0);
									if(searchCollect == null){
										searchCollect = new CtSearchCollect();
										searchCollect.setSeachSum(1);
									} else {
										searchCollect.setSeachSum(searchCollect.getSeachSum() + 1);
									}
									searchCollect.setIsOptimise("0");
									searchCollect.setSearchKey(keyword);
									searchCollect.setSearchTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
									fgoodsManager.merge(searchCollect);
								}
							}
						} else {
							searchKeyWord.setSNum(searchKeyWord.getSNum() + 1);
							fgoodsManager.addGoodsKey(searchKeyWord);
						}
					}
					searchKeywordsList = fgoodsManager.findKeyWordOrderTop4();
					session.put("searchKeywordsList", searchKeywordsList);
					page.setTotalPage(page.getTotalPage());
					String tyn = "";
					if(this.session.get("userinfosession")!=null){
						if(key.length == 1){
							if(key[0].equals("\"null\"")){
								key[0] = (String) session.get("loginCunKword");
								keyword = key[0];
							}
						}
						session.remove("loginCunKword");
					}
					if(keyword.trim().indexOf("  ") != -1){
						String[] chai = keyword.split("  ");
						keyword = chai[0];
						tyn = chai[1];
					} else {
						tyn = keyword;
					}
					this.request.put("keyw", keyword);
					//this.request.put("tempName", tyn);
					session.put("typeName", tyn);
					this.request.put("pages", page);
					
					String tkword = kword;
					if (kword==null || kword.equals("")){
						tkword = keyword;
					}
					this.request.put("tkword", tkword);
					if(this.session.get("userinfosession")==null){
						session.put("loginCunKword", tkword);
					}
					searchList = goodsList;
					String corbname =tyn;
					String[] cor = corbname.split("  ");
					String[] tempName = new String[2];
					replace = fgoodsManager.findReplaceByKey(key[0].replaceAll("\"", ""));
					String repStr1 = "";
					if(replace != null){
						if(replace.getRe1() != null){
							repStr1 += replace.getRe1()+",";
						}
						if(replace.getRe2() != null){
							repStr1 += replace.getRe2()+",";
						}
						if(replace.getRe3() != null){
							repStr1 += replace.getRe3()+",";
						}
						if(replace.getRe4() != null){
							repStr1 += replace.getRe4()+",";
						}
						if(replace.getRe5() != null){
							repStr1 += replace.getRe5()+",";
						}
						repStr1 = repStr1.substring(0, repStr1.length()-1);
					} else {
						repStr1 = null;
					}
					List<FacetField> cnameList = ts.Search11(field, key, page.getFirstIndex(), page.getPageSize(), sortfield, flag, hightlight,"C_NAME",repStr1);
					if(cnameList.size() == 2){
						for (int j = 0; j < cnameList.get(1).getValues().size(); j++) {
							cnameTemp.add(cnameList.get(1).getValues().get(j).getName() + " ( "+cnameList.get(1).getValues().get(j).getCount()+" ) ");
							goodsCName.add(cnameList.get(1).getValues().get(j).getName());
							for (int i = 0; i < cor.length; i++) {
								if((fgoodsDTO.getCname() != null || fgoodsDTO.getBname() != null || (fgoodsDTO.getKword() != null && fgoodsDTO.getKword().trim().indexOf("  ") != -1)) && cor[i].equals(cnameList.get(1).getValues().get(j).getName())){
									tempName[i] = cnameList.get(1).getValues().get(j).getName() + " ( "+cnameList.get(1).getValues().get(j).getCount()+" ) ";
								}
							}
							System.out.println("["+ cnameList.get(1).getValues().get(j).getName() + "]  " + cnameList.get(1).getValues().get(j).getCount());
						}
						for (int j = 0; j < cnameList.get(0).getValues().size(); j++) {
							goodsBName.add(cnameList.get(0).getValues().get(j).getName());
							bnameTemp.add(cnameList.get(0).getValues().get(j).getName() + " ( "+cnameList.get(0).getValues().get(j).getCount()+" ) ");
							for (int i = 0; i < cor.length; i++) {
								if((fgoodsDTO.getCname() != null || fgoodsDTO.getBname() != null || (fgoodsDTO.getKword() != null && fgoodsDTO.getKword().trim().indexOf("  ") != -1)) && cor[i].equals(cnameList.get(0).getValues().get(j).getName())){
									tempName[i] = cnameList.get(0).getValues().get(j).getName() + " ( "+cnameList.get(0).getValues().get(j).getCount()+" ) ";
								}
							}
							System.out.println("["+ cnameList.get(0).getValues().get(j).getName() + "]  " + cnameList.get(0).getValues().get(j).getCount());
						}
					} else {
						for (int j = 0; j < cnameList.get(0).getValues().size(); j++) {
							goodsBName.add(cnameList.get(0).getValues().get(j).getName());
							bnameTemp.add(cnameList.get(0).getValues().get(j).getName() + " ( "+cnameList.get(0).getValues().get(j).getCount()+" ) ");
							for (int i = 0; i < cor.length; i++) {
								if((fgoodsDTO.getCname() != null || fgoodsDTO.getBname() != null || (fgoodsDTO.getKword() != null && fgoodsDTO.getKword().trim().indexOf("  ") != -1)) && cor[i].equals(cnameList.get(0).getValues().get(j).getName())){
									tempName[i] = cnameList.get(0).getValues().get(j).getName() + " ( "+cnameList.get(0).getValues().get(j).getCount()+" ) ";
								}
							}
							System.out.println("["+ cnameList.get(0).getValues().get(j).getName() + "]  " + cnameList.get(0).getValues().get(j).getCount());
						}
					}
					if(keyword.indexOf("  ") != -1){
						String[] chai = keyword.split("  ");
						keyword = chai[0];
						tyn = chai[0];
					} else {
						tyn = keyword;
					}
					this.request.put("keyw", keyword);
					//this.request.put("tempName", tyn);
					session.put("typeName", tyn);
					this.request.put("pages", page);
					
					tkword = kword;
					if (kword==null || kword.equals("")){
						tkword = keyword;
					}
					this.request.put("tkword", tkword);
					if(this.session.get("userinfosession")==null){
						session.put("loginCunKword", tkword);
					}
					for (int i = 0; i < cnameTemp.size(); i++) {
						//goodsCNameStr.add(goodsCName.get(i)+" (" + numfound + ")");
						goodsCNameStr.add(cnameTemp.get(i));
					}
					for (int i = 0; i < bnameTemp.size(); i++) {
						//goodsCNameStr.add(goodsCName.get(i)+" (" + numfound + ")");
						goodsBNameStr.add(bnameTemp.get(i));
					}
					this.request.put("tempName", tempName);
					session.put("goodsCNameStr", goodsCNameStr);
					session.put("goodsBNameStr", goodsBNameStr);
					return SUCCESS;
				}catch(Exception e){
					e.printStackTrace();
					String errorTotle = "请输入搜索关键字";
					String errorMess = e.getMessage();
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					return ERROR;
				}
			}
			
		}
	
	}
	private CtSearchCollect searchCollect = new CtSearchCollect();
	
	public CtSearchCollect getSearchCollect() {
		return searchCollect;
	}

	public void setSearchCollect(CtSearchCollect searchCollect) {
		this.searchCollect = searchCollect;
	}

	public   static   List  removeDuplicateWithOrder(List list)   { 
	      Set set  =   new  HashSet(); 
	      List newList  =   new  ArrayList(); 
	   for  (Iterator iter  =  list.iterator(); iter.hasNext();)   { 
	         Object element  =  iter.next(); 
	         if  (set.add(element)) 
	            newList.add(element); 
	     } 
	     list.clear(); 
	     list.addAll(newList); 
	     System.out.println( " remove duplicate "   +  list); 
	     return list;
	} 
	// 点式搜索商品
	public String search1() throws SQLException {
		// 头
		this.cateList = fgoodsManager.queryAllCate();
		this.catenumList = fgoodsManager.cateCount();
		this.subcatenumList = fgoodsManager.subcateCount();

		Long cid = fgoodsDTO.getCid();
		Long pid = fgoodsDTO.getPid();
		String kword = fgoodsDTO.getKword();
		Long caid = fgoodsDTO.getCaid();
		Long paid = fgoodsDTO.getPaid();
		String keyw = fgoodsDTO.getKeyw();
		String price1 = fgoodsDTO.getPrice1();
		String price2 = fgoodsDTO.getPrice2();
		if (kword != null && kword.equals("price")) {
			searchList = (List<?>) session.get("searchList");
			page = (Page) session.get("page");
			cid = (Long) session.get("cid");
			Object objOne = null;
			List<Object[]> goodsList = new ArrayList<Object[]>();
			for (int i = 0; i < searchList.size(); i++) {
				Object[] obj = (Object[]) searchList.get(i);
				Object[] ctGoods = new Object[25];
				ctGoods[0]=obj[0];
				ctGoods[1]=obj[1];
				ctGoods[2]=obj[2];
				ctGoods[3]=obj[3];
				ctGoods[4]=obj[4];
				ctGoods[5]=obj[5];
				ctGoods[6]=obj[6];
				ctGoods[7]=obj[7];
				ctGoods[8]=obj[8];
				ctGoods[9]=obj[9];
				ctGoods[10]=obj[10];
				ctGoods[11]=obj[11];
				ctGoods[12]=obj[12];
				ctGoods[13]=obj[13];
				ctGoods[14]=obj[14];
				ctGoods[15]=obj[15];
				ctGoods[16]=obj[16];
				goodsList.add(ctGoods);
			}
			Collections.sort(goodsList, new Comparator<Object[]>() {

				@Override
				public int compare(Object[] goods1, Object[] goods2) {
					return ((BigDecimal) goods1[5]).compareTo((BigDecimal) goods2[5]);
				}
			});
			searchList = goodsList;
			this.request.put("pages", page);
			this.request.put("paid", pid);
			this.request.put("keyw", keyw);
			session.put("jiluPriceFind", "price");
			session.put("typeNameToCid", keyw);
		} else {
			if (cid == null) {
				if (caid == null) {
					if (pid == null) {
						if (keyw == null) {
							if (price1 == null && price2 == null) {
								if (paid == null) {
									return "error";
								}
							}
							pid = paid;
							kword = keyw;
						}
					}
					// 筛选条件
//					this.probrand = fgoodsManager.callProBrand(pid, kword,
//							price1, price2);
//					this.procate = fgoodsManager.getSubCate(pid);
//					this.protype = fgoodsManager.callProType(pid, kword,
//							price1, price2);
//					this.proattr = fgoodsManager.callProAttr(pid, kword,
//							price1, price2);
//					this.proattrval = fgoodsManager.callProAttrVal(pid, kword,
//							price1, price2);
//					this.goodsList = fgoodsManager.callGoodsByPrice(price1,
//							price2);
					// 商品列表
					page = new Page();
					if (fgoodsDTO.getPage() == 0) {
						fgoodsDTO.setPage(1);
					}
					page.setCurrentPage(fgoodsDTO.getPage());
					this.searchList = fgoodsManager.searchGoodsList(pid, kword,
							page, price1, price2);
					// 取价
					if (searchList.size() > 0) {
						// 获取登录的用户ID
						Long uid = null;
						if (this.session.get("userinfosession") != null) {
							CtUser cu = (CtUser) this.session
									.get("userinfosession");
							uid = cu.getUId();
						}
						// 循环查询结果替换价格查询结果
						for (int i = 0; i < searchList.size(); i++) {
							Object[] obj = (Object[]) searchList.get(i);
							BigDecimal bgid = (BigDecimal) obj[0];
							Long gid = bgid.longValue();
							String price = this.basicManager.getPrice(uid, gid);
							if (price != null) {
								obj[4] = price;
							}
						}
					}
					page.setTotalPage(page.getTotalPage());
					session.put("searchList", searchList);
					session.put("page", page);
					session.put("cids", cid);
					this.request.put("pages", page);
					this.request.put("paid", pid);
					this.request.put("keyw", kword);
					session.remove("jiluPriceFind");

				} else {
					cid = caid;
					// 筛选条件
					this.probrand = fgoodsManager.getBrandByCId(cid);
					this.protype = fgoodsManager.getTypeByCId(cid);
					this.proattr = fgoodsManager.getAttrByCId(cid);
					this.proattrval = fgoodsManager.getAttrValByCId(cid);
					// 商品列表
					page = new Page();
					if (fgoodsDTO.getPage() == 0) {
						fgoodsDTO.setPage(1);
					}
					page.setCurrentPage(fgoodsDTO.getPage());
					this.searchList = fgoodsManager.searchGoodsByCId(cid, page);
					// 取价
					if (searchList.size() > 0) {
						// 获取登录的用户ID
						Long uid = null;
						if (this.session.get("userinfosession") != null) {
							CtUser cu = (CtUser) this.session
									.get("userinfosession");
							uid = cu.getUId();
						}
						// 循环查询结果替换价格查询结果
						for (int i = 0; i < searchList.size(); i++) {
							Object[] obj = (Object[]) searchList.get(i);
							BigDecimal bgid = (BigDecimal) obj[0];
							Long gid = bgid.longValue();
							String price = this.basicManager.getPrice(uid, gid);
							if (price != null) {
								obj[4] = price;
							}
						}
					}
					page.setTotalPage(page.getTotalPage());
					session.put("searchList", searchList);
					session.put("page", page);
					session.put("cids", cid);
					this.request.put("pages", page);
					this.request.put("caid", cid);
					String showcate = "no";
					this.request.put("showcate", showcate);
					session.remove("jiluPriceFind");
				}
			} else {
				// 筛选条件
				this.probrand = fgoodsManager.getBrandByCId(cid);
				this.protype = fgoodsManager.getTypeByCId(cid);
				this.proattr = fgoodsManager.getAttrByCId(cid);
				this.proattrval = fgoodsManager.getAttrValByCId(cid);
				if (probrand != null && probrand.size() > 0) {
					this.request.put("showbrand", "no");
				}
				if (protype != null && protype.size() > 0) {
					this.request.put("showtype", "no");
				}
				if (proattr != null && proattr.size() > 0) {
					this.request.put("showattr", "no");
				}
				if (proattrval != null && proattrval.size() > 0) {
					this.request.put("showattrval", "no");
				}
				// 商品列表
				page = new Page();
				if (fgoodsDTO.getPage() == 0) {
					fgoodsDTO.setPage(1);
				}
				page.setCurrentPage(fgoodsDTO.getPage());
				this.searchList = fgoodsManager.searchGoodsByCId(cid, page);
				kword = fgoodsManager.getCNameByCid(cid).getCName();
				// 取价
				if (searchList.size() > 0) {
					// 获取登录的用户ID
					Long uid = null;
					if (this.session.get("userinfosession") != null) {
						CtUser cu = (CtUser) this.session
								.get("userinfosession");
						uid = cu.getUId();
					}
					// 循环查询结果替换价格查询结果
					for (int i = 0; i < searchList.size(); i++) {
						Object[] obj = (Object[]) searchList.get(i);
						BigDecimal bgid = (BigDecimal) obj[0];
						Long gid = bgid.longValue();
						String price = this.basicManager.getPrice(uid, gid);
						if (price != null) {
							obj[4] = price;
						}
						rangePricesList = basicManager.getAllRanPrice(gid);
						//priceAllList.add(rangePricesList);
					}
				}
				this.request.put("keyw", kword);
				page.setTotalPage(page.getTotalPage());
				session.put("searchList", searchList);
				session.put("page", page);
				session.put("cids", cid);
				this.request.put("pages", page);
				this.request.put("caid", cid);
				this.session.put("caidOne", cid);
				String showcate = "no";
				this.request.put("showcate", showcate);
				session.remove("jiluPriceFind");
			}
		}

		Map<Long, String> cateMap = new Hashtable<Long, String>();
		cateMap.clear();
		for (CtGoodsCategory cate : cateList) {
			cateMap.put(cate.getCId(), cate.getCName());
		}
		if (cateMap != null && cateMap.size() != 0) {
			Set<Long> setType = cateMap.keySet();
			String typeName = null;
			for (Long typeId : setType) {
				if (typeId.equals(cid)) {
					typeName = cateMap.get(typeId);
					cid = typeId;
					break;
				}
			}
			if (kword != null && kword.trim().length() > 0) {
				typeName = kword;
				if(kword.equals("price")){
					typeName =keyw;
				}
			}
			this.request.put("caid", cid);
			this.session.put("caidOne", cid);
			session.put("typeName", typeName);
			session.put("typeNameToCid", typeName);

		} else {
			return ERROR;
		}
		return SUCCESS;
	}

	public JSONArray getResultJson() {
		return resultJson;
	}

	public void setResultJson(JSONArray resultJson) {
		this.resultJson = resultJson;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	// public List<CtGoods> getCtGoodsList() {
	// return ctGoodsList;
	// }
	//
	// public void setCtGoodsList(List<CtGoods> ctGoodsList) {
	// this.ctGoodsList = ctGoodsList;
	// }

	// 商品分类列表
	public String catelist() {
		try {
			getSubHade();
			if(this.session.get("userinfosession")!=null){
				CtUser cu = (CtUser)this.session.get("userinfosession");	
				ctUser = userManager.getCtUserByUUserid(cu.getUUserid());
			}
			this.catenumList = fgoodsManager.cateCount();
			this.subcatenumList = fgoodsManager.subcateCount();
			this.brandList = fgoodsManager.queryBrand();
			this.cateList = fgoodsManager.queryAllCate();
			cate=fgoodsManager.queryAll();
			ca=fgoodsManager.queryAll();
			session.put("catenumList", catenumList);
			session.put("subcatenumList", subcatenumList);
			session.put("brandList", brandList);
			session.put("cateList", cateList);
			session.put("cate", cate);
			session.put("ca", ca);
			Map<Long, String> cateMap = new Hashtable<Long, String>();
			cateMap.clear();
			for (CtGoodsCategory cate : cateList) {
				cateMap.put(cate.getCId(), cate.getCName());
			}
			session.put("cateListTypeName", cateMap);
		} catch (Exception e) {
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
		return SUCCESS;
	}
	public String brandList() {
		try {	
			this.brandList = fgoodsManager.queryBrand();
			return SUCCESS;
		} catch (Exception e) {
			 
			//e.printStackTrace();
			return "error";
		}
	}
	
	public String brandDetail() {
		try {
			page = new Page();
			if (fgoodsDTO.getPage() == 0) {
				fgoodsDTO.setPage(1);
			}
			Long id = fgoodsDTO.getBId();
			brand = new CtGoodsBrand();
			brand.setBId(id);
			//得到一级
			cateList = fgoodsManager.getCateForFirst();
			//得到二级
			cateListSer = fgoodsManager.getCateSer(cateList);
			//得到三级
			//cateListThree = fgoodsManager.getCateThree(cateListSer);
			this.brand = fgoodsManager.findById(id);			
			return SUCCESS;
		} catch (Exception e) {
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
	}
	// 提前获取商品列表商品分类列表
	public String catelistNow() {
		this.cateList = fgoodsManager.queryAllCate();
		this.catenumList = fgoodsManager.cateCount();
		this.subcatenumList = fgoodsManager.subcateCount();
		this.brandList = fgoodsManager.queryBrand();
		cate=fgoodsManager.queryAll();
		ca=fgoodsManager.queryAll();
		Map<Long, String> cateMap = new Hashtable<Long, String>();
		cateMap.clear();
		for (CtGoodsCategory cate : cateList) {
			cateMap.put(cate.getCId(), cate.getCName());
		}
		session.put("cateListTypeName", cateMap);
		return listByBrand();
	}

	private CtRangePrice rangePrice;
	private CtGroupPrice groupPrice;
	
	public CtRangePrice getRangePrice() {
		return rangePrice;
	}

	public void setRangePrice(CtRangePrice rangePrice) {
		this.rangePrice = rangePrice;
	}

	public CtGroupPrice getGroupPrice() {
		return groupPrice;
	}

	public void setGroupPrice(CtGroupPrice groupPrice) {
		this.groupPrice = groupPrice;
	}

	// 商品列表
	public String goodslist() throws SQLException, UnsupportedEncodingException {
		// 头
//		this.cateList = fgoodsManager.queryAllCate();
//		this.catenumList = fgoodsManager.cateCount();
//		this.subcatenumList = fgoodsManager.subcateCount();

		Long cid = fgoodsDTO.getCid();
		Long pid = fgoodsDTO.getPid();
		String kword = fgoodsDTO.getKword();
		Long caid = fgoodsDTO.getCaid();
		Long paid = fgoodsDTO.getPaid();
		String keyw = fgoodsDTO.getKeyw();
		/*if(keyw != null){
			keyw = new String(keyw.getBytes("iso8859-1"),"UTF-8");
		}*/
		String price1 = fgoodsDTO.getPrice1();
		String price2 = fgoodsDTO.getPrice2();
		if (cid == null) {
			if (caid == null) {
				if (pid == null) {
					if (keyw == null) {
						if (paid == null) {
							return "error";
						}
					}
					pid = paid;
					kword = keyw;
				}
				// 筛选条件
				this.probrand = fgoodsManager.callProBrand(pid, kword, null,
						null);
				this.procate = fgoodsManager.getSubCate(pid);
				this.protype = fgoodsManager
						.callProType(pid, kword, null, null);
				this.proattr = fgoodsManager
						.callProAttr(pid, kword, null, null);
				this.proattrval = fgoodsManager.callProAttrVal(pid, kword,
						null, null);
				// 商品列表
				page = new Page();
				if (fgoodsDTO.getPage() == 0) {
					fgoodsDTO.setPage(1);
				}
				page.setCurrentPage(fgoodsDTO.getPage());
				this.searchList = fgoodsManager.searchGoodsList(pid, kword,
						page, price1, price2);
				// 取价
				if (searchList.size() > 0) {
					// 获取登录的用户ID
					Long uid = null;
					if (this.session.get("userinfosession") != null) {
						CtUser cu = (CtUser) this.session
								.get("userinfosession");
						uid = cu.getUId();
					}
					// 循环查询结果替换价格查询结果
					for (int i = 0; i < searchList.size(); i++) {
						Object[] obj = (Object[]) searchList.get(i);
						BigDecimal bgid = (BigDecimal) obj[0];
						Long gid = bgid.longValue();
						String price = this.basicManager.getPrice(uid, gid);
						if (price != null) {
							obj[4] = price;
						}
					}
				}
				page.setTotalPage(page.getTotalPage());
				this.request.put("pages", page);
				this.request.put("paid", pid);
				this.request.put("keyw", kword);

			} else {
				cid = caid;
				// 筛选条件
				this.probrand = fgoodsManager.getBrandByCId(cid);
				this.protype = fgoodsManager.getTypeByCId(cid);
				this.proattr = fgoodsManager.getAttrByCId(cid);
				this.proattrval = fgoodsManager.getAttrValByCId(cid);
				// 商品列表
				page = new Page();
				if (fgoodsDTO.getPage() == 0) {
					fgoodsDTO.setPage(1);
				}
				page.setCurrentPage(fgoodsDTO.getPage());
				if(keyw != null){
					kword = keyw;
					if(kword.equals("1210")){
						this.searchList = fgoodsManager.searchGoodsList(pid, kword,
								page, "0", "1000");
					} else if(kword.equals("高功率厚膜电阻") || kword.equals("电阻") || kword.equals("电容")) {
						this.searchList = fgoodsManager.searchGoodsByCId(cid, page);
					} else {
						this.searchList = fgoodsManager.searchGoodsList(pid, kword,
								page, "2000", "3000");
					}
				} else {
					this.searchList = fgoodsManager.searchGoodsByCId(cid, page);
				}
				// 取价
				if (searchList.size() > 0) {
					// 获取登录的用户ID
					Long uid = null;
					if (this.session.get("userinfosession") != null) {
						CtUser cu = (CtUser) this.session
								.get("userinfosession");
						uid = cu.getUId();
					}
					// 循环查询结果替换价格查询结果
					for (int i = 0; i < searchList.size(); i++) {
						Object[] obj = (Object[]) searchList.get(i);
						BigDecimal bgid = (BigDecimal) obj[0];
						Long gid = bgid.longValue();
						String price = this.basicManager.getPrice(uid, gid);
						if (price != null) {
							obj[4] = price;
						}
						rangePricesList = basicManager.getAllRanPrice(gid);
						//priceAllList.add(rangePricesList);
					}
				}
				page.setTotalPage(page.getTotalPage());
				this.request.put("pages", page);
				this.request.put("caid", cid);
				String showcate = "no";
				this.request.put("showcate", showcate);
			}
		} else {
			// 筛选条件
			this.probrand = fgoodsManager.getBrandByCId(cid);
			this.protype = fgoodsManager.getTypeByCId(cid);
			this.proattr = fgoodsManager.getAttrByCId(cid);
			this.proattrval = fgoodsManager.getAttrValByCId(cid);
			// 商品列表
			page = new Page();
			if (fgoodsDTO.getPage() == 0) {
				fgoodsDTO.setPage(1);
			}
			page.setCurrentPage(fgoodsDTO.getPage());
				this.searchList = fgoodsManager.searchGoodsByCId(cid, page);
			// 取价
			if (searchList.size() > 0) {
				// 获取登录的用户ID
				Long uid = null;
				if (this.session.get("userinfosession") != null) {
					CtUser cu = (CtUser) this.session.get("userinfosession");
					uid = cu.getUId();
				}
				// 循环查询结果替换价格查询结果
				for (int i = 0; i < searchList.size(); i++) {
					Object[] obj = (Object[]) searchList.get(i);
					BigDecimal bgid = (BigDecimal) obj[0];
					Long gid = bgid.longValue();
					String price = this.basicManager.getPrice(uid, gid);
					if (price != null) {
						obj[4] = price;
					}
				}
			}
			
			session.put("searchList", searchList);
			session.put("page", page);
			session.put("cids", cid);
			page.setTotalPage(page.getTotalPage());
			Page page = new Page();
			page.setTotalPage(Long.valueOf(searchList.size()));
			String pa = page.getPageGoodsStr();
			this.request.put("pageTest", pa);
			this.request.put("pages", page);
			this.request.put("caid", cid);
			String showcate = "no";
			this.request.put("showcate", showcate);
		}
		
		return SUCCESS;
	}

	// 点式商品列表
	public String goodsgrid() throws SQLException {
		Long pid = fgoodsDTO.getPid(); 
		String kword = fgoodsDTO.getKword();
		if (pid == null) {
			return "error";
		}
		// 头
		this.cateList = fgoodsManager.queryAllCate();
		this.catenumList = fgoodsManager.cateCount();
		this.subcatenumList = fgoodsManager.subcateCount();
		// 筛选条件
		this.probrand = fgoodsManager.callProBrand(pid, kword, null, null);
		this.procate = fgoodsManager.getSubCate(pid);
		this.protype = fgoodsManager.callProType(pid, kword, null, null);
		this.proattr = fgoodsManager.callProAttr(pid, kword, null, null);
		this.proattrval = fgoodsManager.callProAttrVal(pid, kword, null, null);
		// 商品列表
		page = new Page();
		if (fgoodsDTO.getPage() == 0) {
			fgoodsDTO.setPage(1);
		}
		page.setCurrentPage(fgoodsDTO.getPage());
		this.searchList = fgoodsManager.searchGoodsList(pid, kword, page,
				price1, price2);
		// 取价
		if (searchList.size() > 0) {
			// 获取登录的用户ID
			Long uid = null;
			if (this.session.get("userinfosession") != null) {
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
			}
			// 循环查询结果替换价格查询结果
			for (int i = 0; i < searchList.size(); i++) {
				Object[] obj = (Object[]) searchList.get(i);
				BigDecimal bgid = (BigDecimal) obj[0];
				Long gid = bgid.longValue();
				String price = this.basicManager.getPrice(uid, gid);
				if (price != null) {
					obj[4] = price;
				}
			}
		}
		session.put("searchList", searchList);
		session.put("page", page);
		page.setTotalPage(page.getTotalPage());
		this.request.put("pages", page);

		return SUCCESS;
	}

	private CtCouponDetailDTO couponDetailDTO = new CtCouponDetailDTO();

	public CtCouponDetailDTO getCouponDetailDTO() {
		return couponDetailDTO;
	}

	public void setCouponDetailDTO(CtCouponDetailDTO couponDetailDTO) {
		this.couponDetailDTO = couponDetailDTO;
	}
	private String couponIdForPage;
	
	public String getCouponIdForPage() {
		return couponIdForPage;
	}

	public void setCouponIdForPage(String couponIdForPage) {
		this.couponIdForPage = couponIdForPage;
	}
	
	// 领取优惠券
	public String getCouPonLing() {
		try {
			if (this.session.get("userinfosession") != null) {
				Integer couponId = Integer.valueOf(couponIdForPage);
				List<CtCouponDetail> couponDetails = fgoodsManager
						.getCouponByCouponId(couponId);
				
				if (couponDetails == null || couponDetails.size() == 0) {
					List<String> l = new ArrayList<String>();
					l.add("nocoupon");
					JSONArray json = new JSONArray();
					json = JSONArray.fromObject(l);
					resultJson = json;
					result = "您已经领取过这个优惠券了";
				} else {
					CtCouponDetail ccd = couponDetails.get(0);
					if (this.session.get("userinfosession") != null) {
						CtUser cu = (CtUser) this.session
								.get("userinfosession");
						ccd.setUId(cu.getUId());
						ccd.setStatus("1");
						String res=coupondetailManager.update(ccd);
						if(res.equals("Exerror")){
							String url = fgoodsDTO.getUrl();
							//System.out.println(url);
							String errorTotle = "抱歉，提交异常";
							String errorMess = "含非法字符";
							session.put("errorMess", errorMess);
							session.put("errorTotle", errorTotle);
							session.put("errorBackUrl", url);
							return ERROR;
						}
					}
					List<String> l = new ArrayList<String>();
					l.add("success");
					JSONArray json = new JSONArray();
					json = JSONArray.fromObject(l);
					resultJson = json;
					result = "领取成功";
				}
			} else {
				List<String> l = new ArrayList<String>();
				l.add("login");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				result = "login";
				return "login";
			}
			return SUCCESS;
		} catch (Exception e) {
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
	}

	public String getAllBomByUid() {
		if (this.session.get("userinfosession") != null) {
			this.boms = bomManager.loadAll();
		}
		return SUCCESS;
	}
	private CtCoupon coupon = new CtCoupon();
	private List<CtCoupon> ctCoupons = new ArrayList<CtCoupon>();
	public CtCoupon getCoupon() {
		return coupon;
	}

	public void setCoupon(CtCoupon coupon) {
		this.coupon = coupon;
	}

	public List<CtCoupon> getCtCoupons() {
		return ctCoupons;
	}

	public void setCtCoupons(List<CtCoupon> ctCoupons) {
		this.ctCoupons = ctCoupons;
	}

	private CtGoodsImg goodsImgBig = new CtGoodsImg();
	
	public CtGoodsImg getGoodsImgBig() {
		return goodsImgBig;
	}

	public void setGoodsImgBig(CtGoodsImg goodsImgBig) {
		this.goodsImgBig = goodsImgBig;
	}
	private Map<Integer, String> cateTypeNew = new HashMap<Integer, String>();
	
	public Map<Integer, String> getCateTypeNew() {
		return cateTypeNew;
	}

	public void setCateTypeNew(Map<Integer, String> cateTypeNew) {
		this.cateTypeNew = cateTypeNew;
	}

	private List<CtGoodsResource> goodsResourcesList = new ArrayList<CtGoodsResource>();
	private List<CtResource> resourcesList = new ArrayList<CtResource>();
	
	public List<CtResource> getResourcesList() {
		return resourcesList;
	}

	public void setResourcesList(List<CtResource> resourcesList) {
		this.resourcesList = resourcesList;
	}

	public List<CtGoodsResource> getGoodsResourcesList() {
		return goodsResourcesList;
	}

	public void setGoodsResourcesList(List<CtGoodsResource> goodsResourcesList) {
		this.goodsResourcesList = goodsResourcesList;
	}

	public String getAttachmentPath() {
		return attachmentPath;
	}

	public void setAttachmentPath(String attachmentPath) {
		this.attachmentPath = attachmentPath;
	}
	private CtGoods goodsInfo = new CtGoods();
	
	public CtGoods getGoodsInfo() {
		return goodsInfo;
	}

	public void setGoodsInfo(CtGoods goodsInfo) {
		this.goodsInfo = goodsInfo;
	}
	private List<CtRangePriceDTO> ranListDTO = new ArrayList<CtRangePriceDTO>();
	
	public List<CtRangePriceDTO> getRanListDTO() {
		return ranListDTO;
	}

	public void setRanListDTO(List<CtRangePriceDTO> ranListDTO) {
		this.ranListDTO = ranListDTO;
	}

	// 商品详情
	public String goodsdetail() {
		try {
			// String typeNameToCid = (String)session.get("typeNameToCid");
			// cateList = fgoodsManager.queryAllCateToType(typeNameToCid);
			searchKeywordsList = fgoodsManager.findKeyWordOrderTop4();
	        session.put("searchKeywordsList", searchKeywordsList);
			//得到一级
			cateList = fgoodsManager.getCateForFirst();
			//得到二级
			cateListSer = fgoodsManager.getCateSer(cateList);
			//得到三级
			//cateListThree = fgoodsManager.getCateThree(cateListSer);
			
			
			
			getSubHade();
			this.cateList = fgoodsManager.queryAllCate();
			this.catenumList = fgoodsManager.cateCount();
			this.subcatenumList = fgoodsManager.subcateCount();
			if (this.session.get("userinfosession") != null) {
				this.boms = bomManager.loadAll();
				this.collects=fgoodsManager.findAll(null);
			}
			Long gid = fgoodsDTO.getGid();
			if (gid == null) {
				return "error";
			}
			List<CtGoods> coupgoods=fgoodsManager.getGoodsByGid(gid);
			if (coupgoods == null || coupgoods.size() < 1) {
				return "error";
			} else {
				this.coupgood = coupgoods.get(0);
			}
			List<?> list = fgoodsManager.getGoods(gid);
			ctCoupons = fgoodsManager.getCouponByGidNew(gid);
			ctCouponDetails = fgoodsManager.getCouponByGid(gid);
			if (ctCoupons == null || ctCoupons.size() == 0) {
				result = "无";
			} else {
				result = "有";
			}
			session.put("result", result);
			if (list == null || list.size() < 1) {
				return "error";
			} else {
				// this.couponDetails =
				// this.coupondetailManager.getCtCouponsByGid(fgoodsDTO.getGId());
				this.goods = (ViewGoodsList) list.get(0);
				goodsInfo = fgoodsManager.getGood(goods.getGId());
				DecimalFormat df = new DecimalFormat("#.##");
				rangePricesList = basicManager.getAllRanPrice(goods.getGId());
				int cunsim = 0;
				int cunPar = 0;
				for (int i = 0; i < rangePricesList.size(); i++) {
					CtRangePriceDTO rangePriceDTO = new CtRangePriceDTO();
					Double parsnum = Double.valueOf(rangePricesList.get(i).getParSNum() == null ? 0 : rangePricesList.get(i).getParSNum()) / 1000;
					Double parenum = Double.valueOf(rangePricesList.get(i).getParENum()==null ? 0 : rangePricesList.get(i).getParENum()) / 1000;
					Double parinc = Double.valueOf(rangePricesList.get(i).getParIncrease() == null ? 0 : rangePricesList.get(i).getParIncrease()) / 1000;
					rangePriceDTO.setGId(rangePricesList.get(i).getGId());
					rangePriceDTO.setRPid(rangePricesList.get(i).getRPid());
					rangePriceDTO.setSimSNum(rangePricesList.get(i).getSimSNum() == null ? 0 : rangePricesList.get(i).getSimSNum());
					rangePriceDTO.setSimENum(rangePricesList.get(i).getSimENum() == null ? 0 : rangePricesList.get(i).getSimENum());
					rangePriceDTO.setSimRPrice(rangePricesList.get(i).getSimRPrice() == null ? 0 : rangePricesList.get(i).getSimRPrice());
					rangePriceDTO.setSimIncrease(rangePricesList.get(i).getSimIncrease() == null ? 0 : rangePricesList.get(i).getSimIncrease());
					rangePriceDTO.setParENum(df.format(parenum.equals(0)? "" : parenum));
					rangePriceDTO.setParIncrease(df.format(parinc));
					rangePriceDTO.setParSNum(df.format(parsnum));
					rangePriceDTO.setParRPrice(rangePricesList.get(i).getParRPrice());
					rangePriceDTO.setInsTime(rangePricesList.get(i).getInsTime());
					ranListDTO.add(rangePriceDTO);
					if(rangePricesList.get(i).getSimSNum() != null){
						cunsim++;
					}
					if(rangePricesList.get(i).getParSNum() != null){
						cunPar++;
					}
				}
				if(cunsim != 0){
					session.put("cunsim", "yes");
				} else {
					session.put("cunsim", "no");
				}
				if(cunPar != 0){
					session.put("cunPar", "yes");
				} else {
					session.put("cunPar", "no");
				}
				Map<Integer, String> cateType = fgoodsManager.getCateFenLeiAll(goods.getGId());
				String cids =  "";
				for (int i = 0; i < cateType.size(); i++) {
					cids += cateType.get(i).substring(cateType.get(i).indexOf("_")+1, cateType.get(i).length()) + ",";
				}
				if(cateType.size() == 3){
					cateTypeNew.put(0, cateType.get(2).substring(0, cateType.get(2).indexOf("_")));
					cateTypeNew.put(1, cateType.get(1).substring(0, cateType.get(1).indexOf("_")));
					cateTypeNew.put(2, cateType.get(0).substring(0, cateType.get(0).indexOf("_")));
				} else if (cateType.size() == 2){
					cateTypeNew.put(0, cateType.get(1).substring(0, cateType.get(1).indexOf("_")));
					cateTypeNew.put(1, cateType.get(0).substring(0, cateType.get(0).indexOf("_")));
				} else {
					cateTypeNew.put(0, cateType.get(0).substring(0, cateType.get(0).indexOf("_")));
				}
				
				this.imgList = fgoodsManager.getImg(gid);
				CtSystemInfo systemInfo = new CtSystemInfo();
				List<CtGoodsImg> imgListNew = new ArrayList<CtGoodsImg>();
				for (int i = 0; i < imgList.size()-1; i++) {
					CtGoodsImg img = new CtGoodsImg();
					img.setPUrl(systemInfo.getCtImgUrl() + "//"+imgList.get(i).getPUrl());
					String path = img.getPUrl();
					path = path.substring(path.lastIndexOf("https://"), path.length());
					img.setPUrl(path);
					img.setPName(imgList.get(i).getPName());
					imgListNew.add(img);
				}
				if(imgList.size() == 0){
					CtGoodsImg img = new CtGoodsImg();
					img.setPUrl(systemInfo.getCtImgUrl() + "//non.jpg");
					String path = img.getPUrl();
					path = path.substring(path.lastIndexOf("https://"), path.length());
					img.setPUrl(path);
					img.setPName("暂无图片");
					imgListNew.add(img);
				}
				imgList = imgListNew;
				if(imgList.size() > 0){
					goodsImgBig = imgList.get(0);
				}
				
				//商品规格书
				resourcesList = (List<CtResource>) fgoodsManager.findByTypeName(goods.getGId().toString(), "GId", "CtResource", 1);
				
				//分类的资源
//				String[] cid = cids.split(",");
//				for (int j = 0; j < cid.length; j++) {
//					goodsResourcesList = fgoodsManager.getResultIdByGid(Long.valueOf(cid[j]));
//					if(goodsResourcesList !=null && goodsResourcesList.size() > 0){
//						for (int i = 0; i < goodsResourcesList.size(); i++) {
//							CtResource res = fgoodsManager.getResByResId(goodsResourcesList.get(i).getResourceId());
//							resourcesList.add(res);
//						}
//					}
//				}
				this.introduceList = fgoodsManager.getGoodsIntroduce(gid);
				this.Rgoods = fgoodsManager.getGoodsReplaces(gid);
				this.Lgoods = fgoodsManager.getGoodsLink(gid);
				int gnum = fgoodsManager.getGNum(gid);
				this.request.put("gnum", gnum);
			}
			int temp = 0;
			//获取订单中的商品评价
			orderInfo = orderManager.getOrderByOrderSn("goods",0L);
			if(orderInfo != null){
				for (int i = 0; i < orderInfo.getTempListOrder().size(); i++) {
					//查询订单的中的所有商品
					orderGoodssList = orderManager.getOrderGoodsByOrderId(orderInfo.getTempListOrder().get(i).getOrderId());
					
					for (int j = 0; j < orderGoodssList.size(); j++) {
						//确定当前商品是否被购买过
						if(orderGoodssList.get(j).getGId().equals(gid)){
							evaluation = orderManager.getEvaByEvaId(orderInfo.getTempListOrder().get(i).getEvaId());
							//System.out.println(evaluation.getEvaDesc());
							if(orderInfo.getTempListOrder().get(i).getCtUser().getUUsername().length() > 1){
								evaluation.getCtUser().setNameTemp(orderInfo.getTempListOrder().get(i).getCtUser().getUUsername().substring(0,1) + "**");
							} else {
								evaluation.getCtUser().setNameTemp(orderInfo.getTempListOrder().get(i).getCtUser().getUUsername() + "**");
							}
							evaList.add(evaluation);
						}
					}
				}
			}
			for (int i = 0; i < evaList.size(); i++)  //外循环是循环的次数
			{
			    for (int j = evaList.size() - 1 ; j > i; j--)  //内循环是 外循环一次比较的次数
			    {

			        if (evaList.get(i).getEvaId().equals(evaList.get(j).getEvaId()) && (evaList.get(i).getCtUser().getNameTemp().equals(evaList.get(j).getCtUser().getNameTemp())))
			        {
			        	evaList.remove(j);
			        }

			    }
			}
			attributeRelationList = fgoodsManager.getAttrRelByGid(gid);
			for (int i = 0; i < attributeRelationList.size(); i++)  //外循环是循环的次数
			{
			    for (int j = attributeRelationList.size() - 1 ; j > i; j--)  //内循环是 外循环一次比较的次数
			    {

			        if (attributeRelationList.get(i).getAttrId().equals(attributeRelationList.get(j).getAttrId()))
			        {
			        	attributeRelationList.remove(j);
			        }

			    }
			}
			if(attributeRelationList != null){
				for (int i = 0; i < attributeRelationList.size(); i++) {
					attribute = fgoodsManager.getattrByAttrId(attributeRelationList.get(i).getAttrId());
					if(attribute != null){
						attributeList.add(attribute);
					}
				}
			}
			for (int i = 0; i < attributeList.size(); i++) {
				attributeRelationList = fgoodsManager.getAttrRelByAttrId(attributeList.get(i).getAttrId(),gid);
				mapAttrAll.put(attributeList.get(i).getAttrName(), attributeRelationList);
			}
			session.put("evaCount", evaList.size());
		} catch (Exception e) {
			e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
		return SUCCESS;
	}
	
	private CtGoodsAttributeRelation attributeRelation = new CtGoodsAttributeRelation();
	private List<CtGoodsAttributeRelation> attributeRelationList = new ArrayList<CtGoodsAttributeRelation>();
	private CtGoodsAttribute attribute = new CtGoodsAttribute();
	private List<CtGoodsAttribute> attributeList = new ArrayList<CtGoodsAttribute>();
	private Map<String, List<CtGoodsAttributeRelation>> mapAttrAll = new HashMap<String, List<CtGoodsAttributeRelation>>();
	
	
	
	public CtGoodsAttributeRelation getAttributeRelation() {
		return attributeRelation;
	}

	public void setAttributeRelation(CtGoodsAttributeRelation attributeRelation) {
		this.attributeRelation = attributeRelation;
	}

	public List<CtGoodsAttributeRelation> getAttributeRelationList() {
		return attributeRelationList;
	}

	public void setAttributeRelationList(
			List<CtGoodsAttributeRelation> attributeRelationList) {
		this.attributeRelationList = attributeRelationList;
	}

	public CtGoodsAttribute getAttribute() {
		return attribute;
	}

	public void setAttribute(CtGoodsAttribute attribute) {
		this.attribute = attribute;
	}

	public List<CtGoodsAttribute> getAttributeList() {
		return attributeList;
	}

	public void setAttributeList(List<CtGoodsAttribute> attributeList) {
		this.attributeList = attributeList;
	}

	public Map<String, List<CtGoodsAttributeRelation>> getMapAttrAll() {
		return mapAttrAll;
	}

	public void setMapAttrAll(Map<String, List<CtGoodsAttributeRelation>> mapAttrAll) {
		this.mapAttrAll = mapAttrAll;
	}

	private CtOrderInfo orderInfo = new CtOrderInfo();
	private List<CtOrderGoods> orderGoodssList = new ArrayList<CtOrderGoods>();
	private CtEvaluation evaluation = new CtEvaluation();
	private List<CtEvaluation> evaList = new ArrayList<CtEvaluation>();

	
	public List<CtOrderGoods> getOrderGoodssList() {
		return orderGoodssList;
	}

	public void setOrderGoodssList(List<CtOrderGoods> orderGoodssList) {
		this.orderGoodssList = orderGoodssList;
	}

	public CtEvaluation getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(CtEvaluation evaluation) {
		this.evaluation = evaluation;
	}

	public List<CtEvaluation> getEvaList() {
		return evaList;
	}

	public void setEvaList(List<CtEvaluation> evaList) {
		this.evaList = evaList;
	}

	public CtOrderInfo getOrderInfo() {
		return orderInfo;
	}

	public void setOrderInfo(CtOrderInfo orderInfo) {
		this.orderInfo = orderInfo;
	}

	// 获取Goods列表
	public String getGoodsByGname() {
		String Gname = this.fgoodsDTO.getGName();
		List<CtGoods> list = this.fgoodsManager.getGoodsByGname(Gname.toUpperCase());
		JSONArray json = new JSONArray();
		json = JSONArray.fromObject(list);
		resultJson = json;
		return SUCCESS;
	}

	// 获取一个Goods
	public String getOneGoodsByGname() {
		try {
			String Gname = this.fgoodsDTO.getGName();
			Object cg = this.fgoodsManager.getOneGoodsByGname(Gname.toUpperCase());
			if (cg == null) {
				List<String> l = new ArrayList<String>();
				l.add("error");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
			} else {
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(cg);
				resultJson = json;
			}
			return SUCCESS;
		} catch (Exception e) {
			// TODO: 
			//e.printStackTrace();
			return "error";
		}

	}

	// 收藏商品
	public String collect() {
		try {
			Long GId = fgoodsDTO.getGid();
			CtUser cu = (CtUser) this.session.get("userinfosession");
			CtCollectGoods ccg = fgoodsManager.getCtCollectGoodsByUIdAndGid(cu.getUId(), GId);
			if (ccg == null) {
				CtCollectGoods ccg2 = new CtCollectGoods();
				ccg2.setUId(cu.getUId());
				ccg2.setGId(GId);
				Date date = new Date();
				SimpleDateFormat siFormat = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");
				ccg2.setAddTime(siFormat.format(date));
				String res = fgoodsManager.save(ccg2);
				if(res.equals("Exerror")){
					String url = fgoodsDTO.getUrl();
					//System.out.println(url);
					String errorTotle = "抱歉，提交异常";
					String errorMess = "含非法字符";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", url);
					return ERROR;
				}
				if (res != null) {
					this.result = "addsuccess";
				} else {
					this.result = "adderror";
				}
			} else {
				this.result = "exist";
			}
			return SUCCESS;
		} catch (Exception e) {
			// TODO: 
			//e.printStackTrace();
			this.result = "error";
			return SUCCESS;
		}
	}
//购物车收藏商品
	public String favorites() {
		try {
			Long GId = fgoodsDTO.getGid();
			CtUser cu = (CtUser) this.session.get("userinfosession");
			CtCart cart=orderManager.getCartByFCartId(GId);
			CtCollectGoods ccg = fgoodsManager.getCtCollectGoodsByUIdAndGid(cu.getUId(), Long.valueOf(cart.getGId()));
			if (ccg == null) {
				CtCollectGoods ccg2 = new CtCollectGoods();
				ccg2.setUId(cu.getUId());
				ccg2.setGId(Long.valueOf(cart.getGId()));
				Date date = new Date();
				SimpleDateFormat siFormat = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");
				ccg2.setAddTime(siFormat.format(date));
				String res= fgoodsManager.save(ccg2);
				if(res.equals("Exerror")){
					String url = fgoodsDTO.getUrl();
					//System.out.println(url);
					String errorTotle = "抱歉，提交异常";
					String errorMess = "含非法字符";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", url);
					return ERROR;
				}
				if (res != null) {
					this.result = "addsuccess";
				} else {
					this.result = "adderror";
				}
			} else {
				this.result = "exist";
			}
			return SUCCESS;
		} catch (Exception e) {
			// TODO: 
			//e.printStackTrace();
			this.result = "error";
			return SUCCESS;
		}
	}
	
	// 商品收藏列表
	public String collectlist() {
		try {
			if(fgoodsDTO.getCollid() != null){
				Long collid = this.fgoodsDTO.getCollid();
				this.collect = this.fgoodsManager.getCtCollByCollId(collid);
			page = new Page();
			if (fgoodsDTO.getPage() == 0) {
				fgoodsDTO.setPage(1);
			}
			page.setCurrentPage(fgoodsDTO.getPage());
			CtUser cu = (CtUser) this.session.get("userinfosession");
			collectList = fgoodsManager.collectList(cu.getUId(), page,collid);
//			collectList = fgoodsManager.loadCollDetailAll(page, collid);
			// 取价
			if (collectList.size() > 0) {
				// 获取登录的用户ID
				Long uid = null;
				for (int i = 0; i < collectList.size(); i++) {
					for (int j = 0; j < collectList.size(); j++) {
						if (collectList.get(i) == collectList.get(j)) {
							
						}
					}
				}
				if (this.session.get("userinfosession") != null) {
					CtUser cu2 = (CtUser) this.session.get("userinfosession");
					uid = cu2.getUId();
				}
				// 循环查询结果替换价格查询结果
				for (int i = 0; i < collectList.size(); i++) {
					Object[] obj = (Object[]) collectList.get(i);
					BigDecimal bgid = (BigDecimal) obj[0];
					Long gid = bgid.longValue();
					String price = this.basicManager.getPrice(uid, gid);
					if (price != null) {
						obj[4] = price;
					}
				}
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "collectlist";
			}
			return "goColl";
		} catch (Exception e) {
			// TODO: 
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
	}

	// 商品收藏的删除
	public String delcollect() {
		try {
			Long collid=fgoodsDTO.getCollid();			
			Long GId = fgoodsDTO.getGid();
			CtUser cu = (CtUser) this.session.get("userinfosession");
			Integer id = fgoodsManager.delGoodsByByUIdAndGid(cu.getUId(), GId,collid);
			if (id != null) {
				List<String> l = new ArrayList<String>();
				l.add("success");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;
			} else {
				List<String> l = new ArrayList<String>();
				l.add("error");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;
			}
			
		} catch (Exception e) {
			// TODO: 
			//e.printStackTrace();
			List<String> l = new ArrayList<String>();
			l.add("error");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		}
	}

	// 查询某个商品的库存
	public String getGoodsNum() {
		try {
			Long GId = fgoodsDTO.getGid();
			Integer num = this.fgoodsManager.getGNum(GId);
			List<String> l = new ArrayList<String>();
			l.add(num.toString());
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		} catch (Exception e) {
			// TODO: 
			//e.printStackTrace();
			List<String> l = new ArrayList<String>();
			l.add("error");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		}
	}

	// 查询购物车共有多少个宝贝
	public String getCartGoodsNum() {
		try {
			CtUser cu = (CtUser) this.session.get("userinfosession");
			Long num = basicManager.getCartGoodsNum(cu.getUId());
			List<String> l = new ArrayList<String>();
			l.add(num.toString());
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		} catch (Exception e) {
			// TODO: 
			//e.printStackTrace();
			List<String> l = new ArrayList<String>();
			l.add("error");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		}
	}
	private OrderDTO orderDTO = new OrderDTO();
	
	public OrderDTO getOrderDTO() {
		return orderDTO;
	}

	public void setOrderDTO(OrderDTO orderDTO) {
		this.orderDTO = orderDTO;
	}

	public String list(){
		try {	
			CtUser cu = (CtUser) this.session.get("userinfosession");
			page = new Page();
			if (fgoodsDTO.getPage() == 0) {
				fgoodsDTO.setPage(1);
			}
			page.setCurrentPage(fgoodsDTO.getPage());
			this.collList = fgoodsManager.findAll(page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return SUCCESS;
		} catch (Exception e) {
			 
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
	}
	private CtNotice notice = new CtNotice();
	private List<CtNotice> noticesList = new ArrayList<CtNotice>();
	
	public CtNotice getNotice() {
		return notice;
	}

	public void setNotice(CtNotice notice) {
		this.notice = notice;
	}

	public List<CtNotice> getNoticesList() {
		return noticesList;
	}

	public void setNoticesList(List<CtNotice> noticesList) {
		this.noticesList = noticesList;
	}

		//跳转到首页
		public String goToIndex(){
			noticesList = fgoodsManager.findAllNotices();
			return SUCCESS;
		}
		private  CtGoodsCategory category = new CtGoodsCategory();
		private List<CtGoodsCategory> listCate = new ArrayList<CtGoodsCategory>();
		
		public CtGoodsCategory getCategory() {
			return category;
		}

		public void setCategory(CtGoodsCategory category) {
			this.category = category;
		}

		public List<CtGoodsCategory> getListCate() {
			return listCate;
		}

		public void setListCate(List<CtGoodsCategory> listCate) {
			this.listCate = listCate;
		}

		//首页三级分类电阻
		public String getSanJiFenLei(){
			listCate = fgoodsManager.getSanjiFenLei();
			return SUCCESS;
		}
		//首页三级分类电容
		public String loadSanjiFenleiDianRong(){
			listCate = fgoodsManager.getSanjiFenLeiDianRong();
			return SUCCESS;
		}
	
	
	public String checkCollTitle(){
		try {
			String collTitle = fgoodsDTO.getColltitle();
			String collid = "";
			if (fgoodsDTO.getCollid() !=null){
				collid =fgoodsDTO.getCollid().toString();
			}
			CtCollect cc = fgoodsManager.getCtCollByCollTitle(collTitle,collid);
			
			if(cc != null){
				List<String> l = new ArrayList<String>();
				l.add("exists");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
			}
			return SUCCESS;
		} catch (Exception e) {
			 
			//e.printStackTrace();
			List<String> l = new ArrayList<String>();
			l.add("exists");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		}
	}
	public String saveColl(){
		List<String> l = new ArrayList<String>();
		try {
			Map session = ActionContext.getContext().getSession();
			CtUser cu = (CtUser)session.get("userinfosession");
			Long UId = cu.getUId();
			String collTitle = this.fgoodsDTO.getColltitle();
			String collDesc = this.fgoodsDTO.getColldesc();
			Date date = new Date();
			SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			CtCollect cc = new CtCollect();
			cc.setColltitle(collTitle);
			cc.setColltime(siFormat.format(date));
			cc.setColldesc(collDesc);
			cc.setUId(UId);
			String res= this.fgoodsManager.save(cc);
			if(res.equals("Exerror")){
				String url = fgoodsDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
			}
			if(res != null){
				l.add("success");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;	
			}else {
				l.add("error");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;
			}
			
		} catch (Exception e) {
			 
			//e.printStackTrace();
			l.add("error");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		}
	}
	
	private List<CtGoodsCategory> cateListSer = new ArrayList<CtGoodsCategory>();
	private List<CtGoodsCategory> cateListThree = new ArrayList<CtGoodsCategory>();
	
	public List<CtGoodsCategory> getCateListSer() {
		return cateListSer;
	}

	public void setCateListSer(List<CtGoodsCategory> cateListSer) {
		this.cateListSer = cateListSer;
	}

	public List<CtGoodsCategory> getCateListThree() {
		return cateListThree;
	}

	public void setCateListThree(List<CtGoodsCategory> cateListThree) {
		this.cateListThree = cateListThree;
	}
	List<CtGoods> goodsListForSub1 = new ArrayList<CtGoods>();
	
	public List<CtGoods> getGoodsListForSub1() {
		return goodsListForSub1;
	}

	public void setGoodsListForSub1(List<CtGoods> goodsListForSub1) {
		this.goodsListForSub1 = goodsListForSub1;
	}
	
	//获取一级分类
	public String getFirstCate(){
		goodsListForSub1 = (List<CtGoods>) session.get("goodsListForSubForHade");
		if(goodsListForSub1 == null || goodsListForSub1.size() == 0 || goodsListForSub1.get(0).getGName()==null){
			goodsListForSub1 = new ArrayList<CtGoods>();
			//得到一级
			cateList = fgoodsManager.getCateForFirst();
			//得到二级
			cateListSer = fgoodsManager.getCateSer(cateList);
			//得到三级
			cateListThree = fgoodsManager.getCateThree(cateListSer);
			session.put("cateListThreeSetSession", cateListThree);
			StringBuffer sb = new StringBuffer();
			sb.append("<div class=\"cateMenu hide\">");
			sb.append("<ul class=\"ct_left_bar\">");
			int temp = 0;
			for (int i = 0; i < cateList.size()+1; i++) {
				if(i == cateList.size()){
					sb.append("<li>");
					sb.append("<div class=\"cate-tag\"><a  target=\"_blank\">更多</a></div>");
					sb.append("<div class=\"list-item hide\">");
					sb.append("<ul class=\"itemleft\">");
					sb.append("<dl>");
					sb.append("<dd>");
					sb.append("<div class=\"ct_t_pp\"></div>");
					sb.append("<div class=\"ct_t_er\">");
					sb.append("<div class=\"ct_t_jt\"><a href=\"javascript:;\" onclick=\"getQuEncode('电阻')\" class=\"jt_ni\">电阻</a></div>");
					sb.append("</div>");
					sb.append("<div class=\"ct_t_er\">");
					sb.append("<div class=\"ct_t_jt\"><a href=\"javascript:;\"  onclick=\"getQuEncode('电容')\" class=\"jt_ni\">电容</a></div>");
					sb.append("</div>");
					sb.append("</dd>");
					sb.append("</dl>");
					sb.append("<div class=\"fn-clear\"></div>");
					sb.append("</ul>");
					sb.append("</div>");
					sb.append("</li>");
					sb.append("<li style=\"display:none\">");
					sb.append("<div class=\"cate-tag\"><a href=\"javascript:;\" onclick=\"getQuEncode('电阻')\" >更多</a></div>");
					sb.append("<div class=\"list-item hide\">");
					sb.append("<ul class=\"itemleft\">");
					sb.append("<dl>");
					sb.append("<dd>");
					sb.append("<div class=\"ct_t_pp\"></div>");
					sb.append("</dd>");
					sb.append("</dl>");
					sb.append("<div class=\"fn-clear\"></div>");
					sb.append("</ul>");
					sb.append("</div>");
					sb.append("</li>");
				} else {
					if(temp==0 || temp == 1){
						String typeName = "";
						String hrefUrl = "";
						if(temp==0){
							typeName = "免费样品";
							hrefUrl = "href=\"goods_search?1=1&page=1&keyword="+typeName+"\"";
						}else if (temp == 1){
							typeName = "特卖专场";
							hrefUrl = "";
						}
						sb.append("<li>");
						sb.append("<div class=\"cate-tag\"><a href=\"javascript:;\" onclick=\"getQuEncode('"+typeName+"')\" >"+typeName+"</a></div>");
						sb.append("<div class=\"list-item hide\">");
						sb.append("<ul class=\"itemleft\">");
						sb.append("<dl>");
						sb.append("<dd>");
						sb.append("<div class=\"ct_t_pp\"></div>");
					} else {
						sb.append("<li>");
                        sb.append("<div class=\"cate-tag\"><a href=\"javascript:;\" onclick=\"getQuEncode('"+cateList.get(i).getCName()+"')\" >"+cateList.get(i).getCName()+"</a></div>");
						sb.append("<div class=\"list-item hide\">");
						sb.append("<ul class=\"itemleft\">");
						sb.append("<dl>");
						sb.append("<dd>");
						sb.append("<div class=\"ct_t_pp\"></div>");
					}
					if(temp==0 || temp == 1){
						for (int j = 0; j < cateListSer.size(); j++) {
								sb.append("<div class=\"ct_t_er\">");
								sb.append("<div class=\"ct_t_jt\"><a target=\"_blank\" class=\"jt_ni\">"+cateListSer.get(j).getCName()+"</a></div>");
								sb.append("<div class=\"ct_t_ne\">");
								for (int j2 = 0; j2 < cateListThree.size(); j2++) {
									if(cateListSer.get(j).getCId().equals(cateListThree.get(j2).getParentId())){
										sb.append("<a href=\"javascript:;\" onclick=\"getQuEncode('"+cateListThree.get(j2).getCName()+"')\">"+cateListThree.get(j2).getCName()+"</a>");
										sb.append(" <span class=\"ct_line_t\"></span>");
									}
								}
								sb.append("</div>");
								sb.append("</div>");
						}
						sb.append("</dd>");
						sb.append("</dl>");
						sb.append("<div class=\"fn-clear\"></div>");
						sb.append("</ul>");
						sb.append("</div>");
						sb.append("</li>");
					} else {
						for (int j = 0; j < cateListSer.size(); j++) {
							if(cateList.get(i).getCId().equals(cateListSer.get(j).getParentId())){
								sb.append("<div class=\"ct_t_er\">");
								sb.append("<div class=\"ct_t_jt\"><a target=\"_blank\" class=\"jt_ni\">"+cateListSer.get(j).getCName()+"</a></div>");
								sb.append("<div class=\"ct_t_ne\">");
								for (int j2 = 0; j2 < cateListThree.size(); j2++) {
									if(cateListSer.get(j).getCId().equals(cateListThree.get(j2).getParentId())){
										sb.append("<a href=\"javascript:;\" onclick=\"getQuEncode('"+cateListThree.get(j2).getCName()+"')\">"+cateListThree.get(j2).getCName()+"</a>");
										sb.append(" <span class=\"ct_line_t\"></span>");
									}
								}
								sb.append("</div>");
								sb.append("</div>");
							}
						}
						sb.append("</dd>");
						sb.append("</dl>");
						sb.append("<div class=\"fn-clear\"></div>");
						sb.append("</ul>");
						sb.append("</div>");
						sb.append("</li>");
					}
				}
				if(temp == 0){
					i--;
				}
				if(temp == 1){
					i--;
				}
				temp++;
				if(temp == (cateList.size()+3)){
					
				}
			}
			sb.append("</div>");
			sb.append("<li style=\"display:none;\"></li>");
			sb.append("</ul> ");
			sb.append("</div>");
			CtGoods gg = new CtGoods();
			gg.setGName(sb.toString());
		
			//System.out.println(temp + cateList.size());
			goodsListForSub1.add(gg);
			session.put("goodsListForSubForHade", goodsListForSub1);
		}
		return SUCCESS;
	}
	List<CtGoods> goodsListForSubShou = new ArrayList<CtGoods>();
	
	public List<CtGoods> getGoodsListForSubShou() {
		return goodsListForSubShou;
	}

	public void setGoodsListForSubShou(List<CtGoods> goodsListForSubShou) {
		this.goodsListForSubShou = goodsListForSubShou;
	}

	public String getFirstCateForShouNew(){
		ActionContext context = ActionContext.getContext();
		Map application = context.getApplication();
		
		goodsListForSubShou = (List<CtGoods>) application.get("goodsListForSubForHadeShouNew");
		
		if(goodsListForSubShou == null || goodsListForSubShou.size() == 0){
			Solr ts = new Solr();
			String field[] = new String[1];
			String[] key = new String[1];
			field[0]="name";
			String[] sortfield={"id","name"};
			Boolean[] flag={true,true};//flag 需要排序的字段的排序方式如果为true 升序 如果为false 降序
			Boolean hightlight=true;//hightlight 是否需要高亮显示
			
			goodsListForSubShou = new ArrayList<CtGoods>();
			
			cateList = (List<CtGoodsCategory>) application.get("cateListApplication");
			if(cateList == null || cateList.size() == 0){
				//得到一级
				cateList = fgoodsManager.getCateForFirst();
				//得到二级
				cateListSer = fgoodsManager.getCateSer(cateList);
				//得到三级
				//cateListThree = fgoodsManager.getCateThree(cateListSer);
				application.put("cateListApplication", cateList);
				application.put("cateListSerApplication", cateListSer);
				//application.put("cateListThreeApplication", cateListThree);
			}
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < cateList.size(); i++) {
				sb.append("<li onmouseover=\"$(this).children('i').css('background-position','"+cateList.get(i).getMouseOverPos()+"');\" onmouseout=\"$(this).children('i').css('background-position','"+cateList.get(i).getMouseOutPos()+"');\">");
				sb.append("<i style=\"background:url('img/icon02.png') no-repeat "+cateList.get(i).getMouseOutPos()+";\"></i>");
				sb.append("<b>&gt;</b>");
				key[0] = "\""+cateList.get(i).getCName()+"\"";
				QueryResponse qr = ts.Search(field, key, 1, 5, sortfield, flag, hightlight,null);
				SolrDocumentList slist=qr.getResults();//获取按条件查询筛选后的结果（ solr自带的方法）
				Long numfound = slist.getNumFound();//结果条数
				sb.append("<a href=\"#\">"+cateList.get(i).getCName()+"("+numfound+")</a>");
				sb.append("");
				sb.append("");
			}
			
			
			
		}
		
		/*
		 * <li onmouseover="$(this).children('i').css('background-position','-18px 0');" onmouseout="$(this).children('i').css('background-position','0 0');">
                <i style="background:url('img/icon02.png') no-repeat 0 0;"></i>
                <b>&gt;</b>
                <a href="#">电阻(14688)</a>
                <div class="tanchu">
                    <ul>
                        <li>
                            <a href="#">铝壳电阻(30)</a>
                        </li>
                        <li>
                            <a href="#">贴片电阻(9745)</a>
                        </li>
                        <li>
                            <a href="#">碳膜电阻(2968)</a>
                        </li>
                        <li>
                            <a href="#">金属膜电阻(1157)</a>
                        </li>
                        <li>
                            <a href="#">氧化膜电阻</a>
                        </li>
                        <li>
                            <a href="#">玻璃釉电阻(631)</a>
                        </li>
                        <li>
                            <a href="#">水泥电阻(30)</a>
                        </li>
                        <li>
                            <a href="#">直插排阻</a>
                        </li>
                        <li>
                            <a href="#">保险电阻</a>
                        </li>
                        <li>
                            <a href="#">绕线电阻</a>
                        </li>
                        <li>
                            <a href="#">光敏电阻(22)</a>
                        </li>
                        <li>
                            <a href="#">压敏电阻MOV</a>
                        </li>
                        <li>
                            <a href="#">热敏电阻NTC(13)</a>
                        </li>
                        <li>
                            <a href="#">热敏电阻PTC</a>
                        </li>
                        <li>
                            <a href="#">可调电阻(16)</a>
                        </li>
                        <li>
                            <a href="#">精密可调电阻</a>
                        </li>
                        <li>
                            <a href="#">密封式可调电位器</a>
                        </li>
                    </ul>
                </div>
            </li>
		 */
		return null;
	}
	
	//获取一级分类首页
	public String getFirstCateForShou(){
		
		goodsListForSubShou = (List<CtGoods>) session.get("goodsListForSubForHadeShou");
		//goodsListForSubShou = new ArrayList<CtGoods>();
		//System.out.println(goodsListForSubShou);
		Solr ts = new Solr();
		String field[] = new String[1];
		String[] key = new String[1];
		field[0]="name";
		String[] sortfield={"id","name"};
		Boolean[] flag={true,true};//flag 需要排序的字段的排序方式如果为true 升序 如果为false 降序
		Boolean hightlight=true;//hightlight 是否需要高亮显示
		if(goodsListForSubShou == null || goodsListForSubShou.size() == 0 || goodsListForSubShou.get(0).getGName()==null){
			goodsListForSubShou = new ArrayList<CtGoods>();
			//得到一级
			cateList = fgoodsManager.getCateForFirst();
			//得到二级
			cateListSer = fgoodsManager.getCateSer(cateList);
			//得到三级
			cateListThree = fgoodsManager.getCateThree(cateListSer);
			session.put("cateListThreeSetSession", cateListThree);
			StringBuffer sb = new StringBuffer();
			sb.append("<div class=\"mod-menu\">");
			sb.append("<ul class=\"menu-item\">");
			//sb.append("<li class=\"rul\">");
			//sb.append("<a href=\"javascript:;\" onclick=\"getQuEncode('免费样品')\" >免费样品</a>");
			//sb.append("</li>");
			for (int i = 0; i < cateList.size(); i++) {
				sb.append("<li class=\"rul\">");
				key[0] = "\""+cateList.get(i).getCName()+"\"";
				QueryResponse qr = ts.Search(field, key, 1, 20, sortfield, flag, hightlight,null);
				SolrDocumentList slist=qr.getResults();//获取按条件查询筛选后的结果（ solr自带的方法）
				Long numfound = slist.getNumFound();//结果条数
				sb.append("<a href=\"javascript:;\"  >"+cateList.get(i).getCName()+"("+numfound+")</a>");
				sb.append("</li>");
			}
			sb.append("</ul>");
			int temp = 0;
			sb.append("<div class=\"menu-cont\" style=\"display:none;top:241px;\">");
			for (int i = 0; i < cateList.size(); i++) {
				if(i == cateList.size()){
				} else {
					if(temp==99999){
						String typeName = "";
						String hrefUrl = "";
						if(temp==0){
							typeName = "免费样品";
							hrefUrl = "href=\"goods_search?1=1&page=1&keyword="+typeName+"\"";
						}
						
					} else {
						
					}
					if(temp==9999){
						sb.append("<div class=\"menu-cont-list\" style=\"display:none;\">");
						sb.append("<ul>");
//						for (int j = 0; j < cateListSer.size(); j++) {
//							sb.append("<li>");
//							sb.append("<span>");
//							sb.append("<a>"+cateListSer.get(j).getCName()+"</a>");
//							sb.append("</span>");
//							sb.append("<p>");
//							for (int j2 = 0; j2 < cateListThree.size(); j2++) {
//								if(cateListSer.get(j).getCId().equals(cateListThree.get(j2).getParentId())){
//									sb.append("<a href=\"javascript:;\" onclick=\"getQuEncode('"+cateListThree.get(j2).getCName()+"')\">"+cateListThree.get(j2).getCName()+"</a>");
//									sb.append("<i></i>");
//								}
//							}
//							sb.append("</p>");
//							sb.append("</li>");
//						}
						sb.append("</ul>");
						sb.append("</div>");
					} else {
						sb.append("<div class=\"menu-cont-list\" style=\"display:none;\">");
						sb.append("<ul>");
						for (int j = 0; j < cateListSer.size(); j++) {
							if(cateList.get(i).getCId().equals(cateListSer.get(j).getParentId())){
								sb.append("<li>");
								sb.append("<span>");
								key[0] = "\""+cateListSer.get(j).getCName()+"\"";
								QueryResponse qr = ts.Search(field, key, 1, 20, sortfield, flag, hightlight,null);
								SolrDocumentList slist=qr.getResults();//获取按条件查询筛选后的结果（ solr自带的方法）
								Long numfound = slist.getNumFound();//结果条数
								
								if(numfound == 0){
									sb.append("<a href=\"javascript:;\" >"+cateListSer.get(j).getCName()+"</a>");
								} else {
									sb.append("<a href=\"javascript:;\" onclick=\"getQuEncode('"+cateListSer.get(j).getCName()+"')\">"+cateListSer.get(j).getCName()+"("+numfound+")</a>");
								}
								
								
								sb.append("</span>");
								sb.append("<p>");
								for (int j2 = 0; j2 < cateListThree.size(); j2++) {
									if(cateListSer.get(j).getCId().equals(cateListThree.get(j2).getParentId())){
										key[0] = "\""+cateListThree.get(j2).getCName()+"\"";
										qr = ts.Search(field, key, 1, 20, sortfield, flag, hightlight,null);
										slist=qr.getResults();//获取按条件查询筛选后的结果（ solr自带的方法）
										numfound = slist.getNumFound();//结果条数
										sb.append("<a href=\"javascript:;\" onclick=\"getQuEncode('"+cateListThree.get(j2).getCName()+"')\">"+cateListThree.get(j2).getCName()+"("+numfound+")</a>");
										sb.append("<i></i>");
									}
								}
								sb.append("</p>");
								sb.append("</li>");
							}
						}
						sb.append("</ul>");
						sb.append("</div>");
					}
				}
				if(temp == 0){
					//i--;
				}
				
				temp++;
				if(temp == (cateList.size()+3)){
					
				}
			}
			sb.append("</div>");
			CtGoods gg = new CtGoods();
			gg.setGName(sb.toString());
		
			//System.out.println(temp + cateList.size());
			goodsListForSubShou.add(gg);
			session.put("goodsListForSubForHadeShou", goodsListForSubShou);
			session.put("gg",gg);
		}
		return SUCCESS;
	}
	//获取一级分类首页
	public String getFirstCateForShou1(){
		goodsListForSubShou = (List<CtGoods>) session.get("goodsListForSubForHadeShou");
		if(goodsListForSubShou == null || goodsListForSubShou.size() == 0 || goodsListForSubShou.get(0).getGName()==null){
			goodsListForSubShou = new ArrayList<CtGoods>();
			//得到一级
			cateList = fgoodsManager.getCateForFirst();
			//得到二级
			cateListSer = fgoodsManager.getCateSer(cateList);
			//得到三级
			cateListThree = fgoodsManager.getCateThree(cateListSer);
			session.put("cateListThreeSetSession", cateListThree);
			StringBuffer sb = new StringBuffer();
			sb.append("<div class=\"cateMenuindex\">");
			sb.append("<ul class=\"ct_left_bar\">");
			int temp = 0;
			for (int i = 0; i < cateList.size()+1; i++) {
				if(i == cateList.size()){
					sb.append("<li>");
					sb.append("<div class=\"cate-tag\"><a  target=\"_blank\">更多</a></div>");
					sb.append("<div class=\"list-item hide\">");
					sb.append("<ul class=\"itemleft\">");
					sb.append("<dl>");
					sb.append("<dd>");
					sb.append("<div class=\"ct_t_pp\"></div>");
					sb.append("<div class=\"ct_t_er\">");
					sb.append("<div class=\"ct_t_jt\"><a href=\"javascript:;\" onclick=\"getQuEncode('电阻')\" class=\"jt_ni\">电阻</a></div>");
					sb.append("</div>");
					sb.append("<div class=\"ct_t_er\">");
					sb.append("<div class=\"ct_t_jt\"><a href=\"javascript:;\"  onclick=\"getQuEncode('电容')\" class=\"jt_ni\">电容</a></div>");
					sb.append("</div>");
					sb.append("</dd>");
					sb.append("</dl>");
					sb.append("<div class=\"fn-clear\"></div>");
					sb.append("</ul>");
					sb.append("</div>");
					sb.append("</li>");
					sb.append("<li style=\"display:none\">");
					sb.append("<div class=\"cate-tag\"><a href=\"goods_search?1=1&page=1&paid=&keyword=电阻\"  target=\"_blank\">更多</a></div>");
					sb.append("<div class=\"list-item hide\">");
					sb.append("<ul class=\"itemleft\">");
					sb.append("<dl>");
					sb.append("<dd>");
					sb.append("<div class=\"ct_t_pp\"></div>");
					sb.append("</dd>");
					sb.append("</dl>");
					sb.append("<div class=\"fn-clear\"></div>");
					sb.append("</ul>");
					sb.append("</div>");
					sb.append("</li>");
				} else {
					if(temp==0 || temp == 1){
						String typeName = "";
						String hrefUrl = "";
						if(temp==0){
							typeName = "免费样品";
							hrefUrl = "href=\"goods_search?1=1&page=1&keyword="+typeName+"\"";
						}else if (temp == 1){
							typeName = "特卖专场";
							hrefUrl = "";
						}
						sb.append("<li>");
						sb.append("<div class=\"cate-tag\"><a href=\"javascript:;\" onclick=\"getQuEncode('"+typeName+"')\" >"+typeName+"</a></div>");
						sb.append("<div class=\"list-item hide\">");
						sb.append("<ul class=\"itemleft\">");
						sb.append("<dl>");
						sb.append("<dd>");
						sb.append("<div class=\"ct_t_pp\"></div>");
					} else {
						sb.append("<li>");
						sb.append("<div class=\"cate-tag\"><a href=\"javascript:;\" onclick=\"getQuEncode('"+cateList.get(i).getCName()+"')\" >"+cateList.get(i).getCName()+"</a></div>");
						sb.append("<div class=\"list-item hide\">");
						sb.append("<ul class=\"itemleft\">");
						sb.append("<dl>");
						sb.append("<dd>");
						sb.append("<div class=\"ct_t_pp\"></div>");
					}
					if(temp==0 || temp == 1){
						for (int j = 0; j < cateListSer.size(); j++) {
							sb.append("<div class=\"ct_t_er\">");
							sb.append("<div class=\"ct_t_jt\"><a target=\"_blank\" class=\"jt_ni\">"+cateListSer.get(j).getCName()+"</a></div>");
							sb.append("<div class=\"ct_t_ne\">");
							for (int j2 = 0; j2 < cateListThree.size(); j2++) {
								if(cateListSer.get(j).getCId().equals(cateListThree.get(j2).getParentId())){
									sb.append("<a href=\"javascript:;\" onclick=\"getQuEncode('"+cateListThree.get(j2).getCName()+"')\">"+cateListThree.get(j2).getCName()+"</a>");
									sb.append(" <span class=\"ct_line_t\"></span>");
								}
							}
							sb.append("</div>");
							sb.append("</div>");
						}
						sb.append("</dd>");
						sb.append("</dl>");
						sb.append("<div class=\"fn-clear\"></div>");
						sb.append("</ul>");
						sb.append("</div>");
						sb.append("</li>");
					} else {
						for (int j = 0; j < cateListSer.size(); j++) {
							if(cateList.get(i).getCId().equals(cateListSer.get(j).getParentId())){
								sb.append("<div class=\"ct_t_er\">");
								sb.append("<div class=\"ct_t_jt\"><a target=\"_blank\" class=\"jt_ni\">"+cateListSer.get(j).getCName()+"</a></div>");
								sb.append("<div class=\"ct_t_ne\">");
								for (int j2 = 0; j2 < cateListThree.size(); j2++) {
									if(cateListSer.get(j).getCId().equals(cateListThree.get(j2).getParentId())){
										sb.append("<a href=\"javascript:;\" onclick=\"getQuEncode('"+cateListThree.get(j2).getCName()+"')\">"+cateListThree.get(j2).getCName()+"</a>");
										sb.append(" <span class=\"ct_line_t\"></span>");
									}
								}
								sb.append("</div>");
								sb.append("</div>");
							}
						}
						sb.append("</dd>");
						sb.append("</dl>");
						sb.append("<div class=\"fn-clear\"></div>");
						sb.append("</ul>");
						sb.append("</div>");
						sb.append("</li>");
					}
				}
				if(temp == 0){
					i--;
				}
				if(temp == 1){
					i--;
				}
				temp++;
				if(temp == (cateList.size()+3)){
					
				}
			}
			sb.append("</div>");
			sb.append("<li style=\"display:none;\"></li>");
			sb.append("</ul> ");
			sb.append("</div>");
			CtGoods gg = new CtGoods();
			gg.setGName(sb.toString());
			
			//System.out.println(temp + cateList.size());
			goodsListForSubShou.add(gg);
			session.put("goodsListForSubForHadeShou", goodsListForSubShou);
		}
		return SUCCESS;
	}
	
	
	//修改收藏夹
		public String editCollect(){
			List<String> l = new ArrayList<String>();
			try {
				Long[] collid = this.fgoodsDTO.getMid();
				CtCollect collect = this.fgoodsManager.getCtCollByCollId(collid[0]);
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(collect);
				resultJson = json;
				return SUCCESS;	
			} catch (Exception e) {
				 
				//e.printStackTrace();
				l.add("error");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;
			}
		}
		//保存修改收藏夹
		public String saveUpdate(){
			List<String> l = new ArrayList<String>();
			try {
				Long collid = this.fgoodsDTO.getCollid();
				String colltitle = this.fgoodsDTO.getColltitle();
				String colldesc = this.fgoodsDTO.getColldesc();
				CtCollect collect = this.fgoodsManager.getCtCollByCollId(collid);
				collect.setColltitle(colltitle);
				collect.setColldesc(colldesc);
				String res=this.fgoodsManager.updateColl(collect);
				if(res.equals("Exerror")){
					String url = fgoodsDTO.getUrl();
					//System.out.println(url);
					String errorTotle = "抱歉，提交异常";
					String errorMess = "含非法字符";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", url);
					return ERROR;
				}
				l.add("success");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;	
			} catch (Exception e) {
				 
				//e.printStackTrace();
				l.add("error");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;
			}
		}
	
	public String delColl(){
		try {
			for(Long c:fgoodsDTO.getMid()){
				this.fgoodsManager.delCollGoodsByCollId(c);
				this.fgoodsManager.delete(c);
			}
			return "del";
		} catch (Exception e) {
			 
			//e.printStackTrace();
			return "error";
		}
	}
	//检索
		public String searchColl(){
			if(fgoodsDTO.getKeyword() != null){
				Page page=new Page();
				if (fgoodsDTO.getPage()==0){
					fgoodsDTO.setPage(1);
				}
				page.setCurrentPage(fgoodsDTO.getPage());
				if (!"".equals(fgoodsDTO.getKeyword())){
					this.collList=fgoodsManager.search(fgoodsDTO.getKeyword(),page);
				}else{
					this.collList= fgoodsManager.findAll(null);
				}
				page.setTotalPage(page.getTotalPage());
				this.request.put("pages", page);
				return "search";
			}else {
				return "goColl";
			}
			
		}
	/*
	 * 显示增票信息
	 */
	public String TicketAll() {

		return "";
	}

	public CtBomManager getBomManager() {
		return bomManager;
	}

	public void setBomManager(CtBomManager bomManager) {
		this.bomManager = bomManager;
	}

	public List<CtBom> getBoms() {
		return boms;
	}

	public void setBoms(List<CtBom> boms) {
		this.boms = boms;
	}

	public List<?> getCollectList() {
		return collectList;
	}

	public void setCollectList(List<?> collectList) {
		this.collectList = collectList;
	}

	public List<CtCollectGoods> getCollectsList() {
		return collectsList;
	}

	public void setCollectsList(List<CtCollectGoods> collectsList) {
		this.collectsList = collectsList;
	}

	public List<CtCollect> getCollList() {
		return collList;
	}

	public void setCollList(List<CtCollect> collList) {
		this.collList = collList;
	}
	//商品详情-加入收藏夹两种方式
		public String addToCollFromGoodsTwo(){
			try {
				//把商品加入新建的收藏夹
				String colltitle = this.fgoodsDTO.getColltitle();
				Long gid = this.fgoodsDTO.getGid();
				Map session = ActionContext.getContext().getSession();
				CtUser cu = (CtUser)session.get("userinfosession");
				Long UId = cu.getUId();
				//String bomTitle = this.bomDTO.getBomTitle();
				String colldesc = this.fgoodsDTO.getColltitle();
				Date date = new Date();
				SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				CtCollect cg=this.fgoodsManager.getCtCollByCollTitle(colltitle,"");
				if(cg!=null){
				
				}
				CtCollect cc = new CtCollect();
				cc.setColltitle(colltitle);
				cc.setColldesc(colldesc);
				cc.setColltime(siFormat.format(date));
				cc.setUId(UId);
				String res = this.fgoodsManager.save(cc);
				if(res.equals("Exerror")){
					String url = fgoodsDTO.getUrl();
					//System.out.println(url);
					String errorTotle = "抱歉，提交异常";
					String errorMess = "含非法字符";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", url);
					return ERROR;
				}
				//查询收藏夹
				collect = (CtCollect) this.fgoodsManager.getCtCollByUId(UId).get(0);
				CtCollectGoods ccg = this.fgoodsManager.getCtCollGoodsByGId(gid, collect.getCollid());
				if(ccg != null){
				//	this.fgoodsManager.updateCtBomGoods(ccg);
				}else {
					CtCollectGoods ccg2 = new CtCollectGoods();
					ccg2.setUId(UId);
					ccg2.setGId(gid);
					ccg2.setAddTime(siFormat.format(date));
					ccg2.setCollid(collect.getCollid());
					String re=this.fgoodsManager.save(ccg2);
					if(re.equals("Exerror")){
						String url = fgoodsDTO.getUrl();
						//System.out.println(url);
						String errorTotle = "抱歉，提交异常";
						String errorMess = "含非法字符";
						session.put("errorMess", errorMess);
						session.put("errorTotle", errorTotle);
						session.put("errorBackUrl", url);
						return ERROR;
					}
				}
				//把商品加入已有的收藏夹
				for(Long c:fgoodsDTO.getMid()){
					CtCollectGoods cgoods = this.fgoodsManager.getCtCollGoodsByGId(gid,c);
					if(cgoods != null){
						//this.bomManager.updateCtBomGoods(c);
					}else {
						CtCollectGoods ccg2 = new CtCollectGoods();
						ccg2.setUId(UId);
						ccg2.setGId(gid);
						ccg2.setAddTime(siFormat.format(date));
						ccg2.setCollid(c);
						String re=this.fgoodsManager.save(ccg2);
						if(re.equals("Exerror")){
							String url = fgoodsDTO.getUrl();
							//System.out.println(url);
							String errorTotle = "抱歉，提交异常";
							String errorMess = "含非法字符";
							session.put("errorMess", errorMess);
							session.put("errorTotle", errorTotle);
							session.put("errorBackUrl", url);
							return ERROR;
						}
					}
				}
				List<String> l = new ArrayList<String>();
				l.add("success");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;
			} catch (Exception e) {
				 
				//e.printStackTrace();
				List<String> l = new ArrayList<String>();
				l.add("error");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;
			}
		}
		//商品详情-加入我的收藏夹，新建
		public String addToCollFromGoodsNew(){
			try {
				//把商品加入新建的收藏夹
				String colltitle = this.fgoodsDTO.getColltitle();
				String colldesc = this.fgoodsDTO.getColltitle();
				Long gid = this.fgoodsDTO.getGid();
				Map session = ActionContext.getContext().getSession();
				CtUser cu = (CtUser)session.get("userinfosession");
				Long UId = cu.getUId();
				//String bomTitle = this.bomDTO.getBomTitle();
				Date date = new Date();
				SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				CtCollect cc = new CtCollect();
				cc.setColltitle(colltitle);
				cc.setColldesc(colldesc);
				cc.setColltime(siFormat.format(date));
				cc.setUId(UId);
				String res = this.fgoodsManager.save(cc);
				if(res.equals("Exerror")){
					String url = fgoodsDTO.getUrl();
					//System.out.println(url);
					String errorTotle = "抱歉，提交异常";
					String errorMess = "含非法字符";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", url);
					return ERROR;
				}
				//查询收藏夹
				collect = this.fgoodsManager.getCtCollByCollTitle(colltitle,"");
				CtCollectGoods ccg = this.fgoodsManager.getCtCollGoodsByGId(gid, collect.getCollid());
				if(ccg!= null){
				//this.bomManager.updateCtBomGoods(cbg);
				}else {
					CtCollectGoods ccg2 = new CtCollectGoods();
					ccg2.setCollid(collect.getCollid());
					ccg2.setGId(gid);
					ccg2.setAddTime(siFormat.format(date));
					ccg2.setUId(UId);
					String re=this.fgoodsManager.save(ccg2);
					if(re.equals("Exerror")){
						String url = fgoodsDTO.getUrl();
						//System.out.println(url);
						String errorTotle = "抱歉，提交异常";
						String errorMess = "含非法字符";
						session.put("errorMess", errorMess);
						session.put("errorTotle", errorTotle);
						session.put("errorBackUrl", url);
						return ERROR;
					}
				}
				List<String> l = new ArrayList<String>();
				l.add("success");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;
			} catch (Exception e) {
				 
				//e.printStackTrace();
				List<String> l = new ArrayList<String>();
				l.add("error");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;
			}
		}
		//把商品加入我的bom 已经存在的bom
		public String addToCollFromGoodsExisting(){
			Long gid = fgoodsDTO.getGid();
			Map session = ActionContext.getContext().getSession();
			CtUser cu = (CtUser)session.get("userinfosession");
			Long UId = cu.getUId();
			//String bomTitle = this.bomDTO.getBomTitle();
			Date date = new Date();
			SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			try {
				for(Long c:fgoodsDTO.getMid()){
					CtCollectGoods cgoods = this.fgoodsManager.getCtCollGoodsByGId(gid,c);
					if(cgoods != null){
						//this.bomManager.updateCtBomGoods(c);
					}else {
						CtCollectGoods ccg2 = new CtCollectGoods();
						ccg2.setUId(UId);
						ccg2.setGId(gid);
						ccg2.setAddTime(siFormat.format(date));
						ccg2.setCollid(c);
						String res=this.fgoodsManager.save(ccg2);
						if(res.equals("Exerror")){
							String url = fgoodsDTO.getUrl();
							//System.out.println(url);
							String errorTotle = "抱歉，提交异常";
							String errorMess = "含非法字符";
							session.put("errorMess", errorMess);
							session.put("errorTotle", errorTotle);
							session.put("errorBackUrl", url);
							return ERROR;
						}
					}
				}
				List<String> l = new ArrayList<String>();
				l.add("success");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;
			} catch (Exception e) {
				 
				//e.printStackTrace();
				List<String> l = new ArrayList<String>();
				l.add("error");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
			}
			return SUCCESS;
		}
		private List<CtGoods> goodsListForSub = new ArrayList<CtGoods>();
		
		public List<CtGoods> getGoodsListForSub() {
			return goodsListForSub;
		}

		public void setGoodsListForSub(List<CtGoods> goodsListForSub) {
			this.goodsListForSub = goodsListForSub;
		}

		//加载subHade
		public String getSubHade(){
			getSubList();
			
			return SUCCESS;
		}

		private void getSubList() {
			goodsListForSub = (List<CtGoods>) session.get("goodsListForSub");
			if(goodsListForSub == null){
				goodsListForSub = fgoodsManager.getSubHade();
				for (int i = 0; i < goodsListForSub.size(); i++) {
					imgList = fgoodsManager.getImg(goodsListForSub.get(i).getGId());
					CtGoodsImg img = null;
					CtSystemInfo ctSystemInfo = new CtSystemInfo();
					if(imgList != null && imgList.size() > 0){
						img = new CtGoodsImg();
						img.setCoverImg(imgList.get(3).getCoverImg());
						img.setGId(imgList.get(3).getGId());
						img.setPId(imgList.get(3).getPId());
						img.setPName(imgList.get(3).getPName());
						String path ="";
						if (imgList !=null){
							path = ctSystemInfo.getCtImgUrl() + "//" + imgList.get(3).getPUrl();
						}else{
							path= ctSystemInfo.getCtImgUrl() + "//ct_s.gif";
						}
						img.setPUrl(path);
						goodsListForSub.get(i).setGoodsImg(img);
					}
					if(img != null){
						goodsListForSub.get(i).setGoodsImg(img);
					} else {
						img = new CtGoodsImg();
						img.setPUrl(ctSystemInfo.getCtImgUrl()+"//ct_s.gif");
						goodsListForSub.get(i).setGoodsImg(img);
					}
					if(goodsListForSub.get(i).getUName() == null){
						goodsListForSub.get(i).setUName(goodsListForSub.get(i).getUName());
					}
				}
				session.put("goodsListForSub", goodsListForSub);
			}
		}
		
		public String cateListh(){
			try {	
				this.cateList = fgoodsManager.queryAllCate();
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(cateList);
				resultJson = json;
				return SUCCESS;
			} catch (Exception e) {
				 
				//e.printStackTrace();
				return "error";
			}
		}
		public String cate(){
			try {
				cate=fgoodsManager.queryAll();	
				Long cid=this.fgoodsDTO.getCid();
				CtGoodsCategory	cgc=fgoodsManager.getCNameByCid(cid);
				Long pid=cgc.getCId();
				ca=fgoodsManager.queryAllByPid(pid);
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(ca);
				resultJson = json;
				return SUCCESS;
			} catch (Exception e) {
				 
				//e.printStackTrace();
				String errorTotle = "糟糕,页面丢失了";
				String errorMess = " ";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				return ERROR;
			}
		}
		public String ca(){
			try {	
				ca=fgoodsManager.queryAll();
				List<CtGoodsCategory> cgcalist=new ArrayList<CtGoodsCategory>(0);
				Long cid=this.fgoodsDTO.getCid();
				CtGoodsCategory	cgc=fgoodsManager.getCNameByCid(cid);
				Long pid=cgc.getCId();
				ca=fgoodsManager.queryAllByPid(pid);
				for(int i=0;i<ca.size();i++){
					CtGoodsCategory	cc=fgoodsManager.queryByCName(ca.get(i).getCName());
					Long id=cc.getCId();
					cate=fgoodsManager.queryAllByPid(id);
				}
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(ca);
				resultJson = json;
				return SUCCESS;
			} catch (Exception e) {
				 
				//e.printStackTrace();
				return "error";
			}
		}
		
		private int number ;  
		  
	    private String fileName;  
	    
	    private String attachmentPath;    
		
		public int getNumber() {
			return number;
		}

		public void setNumber(int number) {
			this.number = number;
		}

		public String getFileName() {
			return fileName;
		}

		public void setFileName(String fileName) throws UnsupportedEncodingException {
			this.fileName = new String(fileName.getBytes("ISO-8859-1"),"UTF-8");
		}
		/**  
	     * 下载多个附件  
	     * 实现：将多个附近压缩成zip包,然后再下载zip包  
	     */  
	    public String downloadMultiFile() {   
	        //使用当前时间生成文件名称   
	        String formatDate =new  SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());   
	        //压缩后的zip文件存放路径   
	        fileName = "D://" + formatDate + ".zip";   
	            
	        if(attachmentPath != null && !"".equals(attachmentPath)) { 
	            //将多个附件的路径取出   
	            String[] attachmentPathArray = attachmentPath.split(",");   
	            if(attachmentPathArray != null && attachmentPathArray.length >0) {   
	                File[] files = new File[attachmentPathArray.length];   
	                for(int i=0;i<attachmentPathArray.length;i++) {   
	                	CtResource resource = fgoodsManager.getResByResId(Long.valueOf(attachmentPathArray[i].trim()));
	                    if(resource.getRUrl() != null) {   
	                        File file = new File(resource.getRUrl().trim());   
	                        if(file.exists()) {   
	                            files[i] = file;   
	                        }   
	                    }   
	                }   
	                //将多个附件压缩成zip   
	                ZipFileUtil.compressFiles2Zip(files,fileName);   
	            }   
	                
	        }   
	        return SUCCESS;   
	    }   
		public String getCateSumInfo(){
			CtUser cu = (CtUser)session.get("userinfosession");
			if(cu != null){
				String numSimPar = orderManager.getCateSimParNumByUserId(cu.getUId());
				result = numSimPar;
			} else {
				result = "0===0===0";
			}
			return SUCCESS;
		}
		 @Override  
		 public String execute() throws Exception { 
			 CtResource resource = fgoodsManager.getResByResId(Long.valueOf(number));
				CtSystemInfo systemInfo = new CtSystemInfo();
				OSSClient client = new OSSClient(systemInfo.getEndpoint(), systemInfo.getAccess_id(), systemInfo.getAccess_key());
				OSSObjectSample sample = new OSSObjectSample();
				sample.downloadFile(client, systemInfo.getBucketNameDownLoad(), resource.getRUrl().substring(1, resource.getRUrl().length()), "C://Program Files//"+resource.getRName());
		      result = "success";
				return SUCCESS;  
		 } 
		//商品详情-加入收藏夹两种方式
		public String addToCollGoodsTwo(){
			try {
				//把商品加入新建的收藏夹
				String ctids = fgoodsDTO.getCtids();
				if (ctids != null) {
					String[] g = ctids.split(",");
					for (int i = 1; i < g.length; i++) {
						CtCart cart =fgoodsManager.getcartBycartid(Long.parseLong(g[i]));
						String gid=cart.getGId();
				String colltitle = this.fgoodsDTO.getColltitle();
				Map session = ActionContext.getContext().getSession();
				CtUser cu = (CtUser)session.get("userinfosession");
				Long UId = cu.getUId();
				//String bomTitle = this.bomDTO.getBomTitle();
				String colldesc = this.fgoodsDTO.getColltitle();
				Date date = new Date();
				SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				CtCollect cg=this.fgoodsManager.getCtCollByCollTitle(colltitle,"");
				if(cg==null){
					CtCollect cc = new CtCollect();
					cc.setColltitle(colltitle);
					cc.setColldesc(colldesc);
					cc.setColltime(siFormat.format(date));
					cc.setUId(UId);
					String res = this.fgoodsManager.save(cc);
					if(res.equals("Exerror")){
						String url = fgoodsDTO.getUrl();
						//System.out.println(url);
						String errorTotle = "抱歉，提交异常";
						String errorMess = "含非法字符";
						session.put("errorMess", errorMess);
						session.put("errorTotle", errorTotle);
						session.put("errorBackUrl", url);
						return ERROR;
					}
				}
				//查询收藏夹
				collect = this.fgoodsManager.getCtCollByCollTitle(colltitle,"");
				CtCollectGoods ccg = this.fgoodsManager.getCtCollGoodsByGId(Long.valueOf(gid), collect.getCollid());
				if(ccg != null){
				//	this.fgoodsManager.updateCtBomGoods(ccg);
				}else {
					CtCollectGoods ccg2 = new CtCollectGoods();
					ccg2.setUId(UId);
					ccg2.setGId(Long.valueOf(gid));
					ccg2.setAddTime(siFormat.format(date));
					ccg2.setCollid(collect.getCollid());
					String res=this.fgoodsManager.save(ccg2);
					if(res.equals("Exerror")){
						String url = fgoodsDTO.getUrl();
						//System.out.println(url);
						String errorTotle = "抱歉，提交异常";
						String errorMess = "含非法字符";
						session.put("errorMess", errorMess);
						session.put("errorTotle", errorTotle);
						session.put("errorBackUrl", url);
						return ERROR;
					}
				}
				//把商品加入已有的收藏夹
				for(Long c:fgoodsDTO.getMid()){
					CtCollectGoods cgoods = this.fgoodsManager.getCtCollGoodsByGId(Long.valueOf(gid),c);
					if(cgoods != null){
						//this.bomManager.updateCtBomGoods(c);
					}else {
						CtCollectGoods ccg2 = new CtCollectGoods();
						ccg2.setUId(UId);
						ccg2.setGId(Long.valueOf(gid));
						ccg2.setAddTime(siFormat.format(date));
						ccg2.setCollid(c);
					String res=	this.fgoodsManager.save(ccg2);
					if(res.equals("Exerror")){
						String url = fgoodsDTO.getUrl();
						//System.out.println(url);
						String errorTotle = "抱歉，提交异常";
						String errorMess = "含非法字符";
						session.put("errorMess", errorMess);
						session.put("errorTotle", errorTotle);
						session.put("errorBackUrl", url);
						return ERROR;
					}
					}
				}
				}
			}
				List<String> l = new ArrayList<String>();
				l.add("success");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;
			} catch (Exception e) {
				 
				//e.printStackTrace();
				List<String> l = new ArrayList<String>();
				l.add("error");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;
			}
		}
		//商品详情-加入我的收藏夹，新建
		public String addToCollGoodsNew(){
			try {
				//把商品加入新建的收藏夹
				String ctids = fgoodsDTO.getCtids();
				if (ctids != null) {
					String[] g = ctids.split(",");
					for (int i = 1; i < g.length; i++) {
						CtCart cart =fgoodsManager.getcartBycartid(Long.parseLong(g[i]));
						String gid=cart.getGId();
				String colltitle = this.fgoodsDTO.getColltitle();
				String colldesc = this.fgoodsDTO.getColltitle();
				Map session = ActionContext.getContext().getSession();
				CtUser cu = (CtUser)session.get("userinfosession");
				Long UId = cu.getUId();
				//String bomTitle = this.bomDTO.getBomTitle();
				Date date = new Date();
				SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				//查询收藏夹  判断是否存在
				collect = this.fgoodsManager.getCtCollByCollTitle(colltitle,"");
				if(collect==null){
				CtCollect cc = new CtCollect();
				cc.setColltitle(colltitle);
				cc.setColldesc(colldesc);
				cc.setColltime(siFormat.format(date));
				cc.setUId(UId);
				String res = this.fgoodsManager.save(cc);
				if(res.equals("Exerror")){
					String url = fgoodsDTO.getUrl();
					//System.out.println(url);
					String errorTotle = "抱歉，提交异常";
					String errorMess = "含非法字符";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", url);
					return ERROR;
				}
				}
				collect = this.fgoodsManager.getCtCollByCollTitle(colltitle,"");
				CtCollectGoods ccg = this.fgoodsManager.getCtCollGoodsByGId(Long.valueOf(gid), collect.getCollid());
				if(ccg!= null){
				//this.bomManager.updateCtBomGoods(cbg);
				}else {
					CtCollectGoods ccg2 = new CtCollectGoods();
					ccg2.setCollid(collect.getCollid());
					ccg2.setGId(Long.valueOf(gid));
					ccg2.setAddTime(siFormat.format(date));
					ccg2.setUId(UId);
				String res=	this.fgoodsManager.save(ccg2);
				if(res.equals("Exerror")){
					String url = fgoodsDTO.getUrl();
					//System.out.println(url);
					String errorTotle = "抱歉，提交异常";
					String errorMess = "含非法字符";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", url);
					return ERROR;
				}
				}
					}
				}
				List<String> l = new ArrayList<String>();
				l.add("success");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;
			} catch (Exception e) {
				 
				//e.printStackTrace();
				List<String> l = new ArrayList<String>();
				l.add("error");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;
			}
		}
		//把商品加入我的收藏夹 已经存在的收藏夹
		public String addToCollGoodsExisting(){
	try {
			String ctids = fgoodsDTO.getCtids();
			if (ctids != null) {
				String[] g = ctids.split(",");
				for (int i = 1; i < g.length; i++) {
					CtCart cart =fgoodsManager.getcartBycartid(Long.parseLong(g[i]));
					String gid=cart.getGId();
			Map session = ActionContext.getContext().getSession();
			CtUser cu = (CtUser)session.get("userinfosession");
			Long UId = cu.getUId();
			//String bomTitle = this.bomDTO.getBomTitle();
			Date date = new Date();
			SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			
				for(Long c:fgoodsDTO.getMid()){
					CtCollectGoods cgoods = this.fgoodsManager.getCtCollGoodsByGId(Long.valueOf(gid),c);
					if(cgoods != null){
						//this.bomManager.updateCtBomGoods(c);
					}else {
						CtCollectGoods ccg2 = new CtCollectGoods();
						ccg2.setUId(UId);
						ccg2.setGId(Long.valueOf(gid));
						ccg2.setAddTime(siFormat.format(date));
						ccg2.setCollid(c);
						String res=this.fgoodsManager.save(ccg2);
						if(res.equals("Exerror")){
							String url = fgoodsDTO.getUrl();
							//System.out.println(url);
							String errorTotle = "抱歉，提交异常";
							String errorMess = "含非法字符";
							session.put("errorMess", errorMess);
							session.put("errorTotle", errorTotle);
							session.put("errorBackUrl", url);
							return ERROR;
						}
					}
				}
				}
				}
				List<String> l = new ArrayList<String>();
				l.add("success");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;
			} catch (Exception e) {
				 
				//e.printStackTrace();
				List<String> l = new ArrayList<String>();
				l.add("error");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
			}
			return SUCCESS;
		}

		public List<String> getKeyList() {
			return keyList;
		}

		public void setKeyList(List<String> keyList) {
			this.keyList = keyList;
		}

		public String getSearchType() {
			return searchType;
		}

		public void setSearchType(String searchType) {
			this.searchType = searchType;
		}
		
//		public String tempTimmer(){
//			session.remove("aaaa");
//			 //通过schedulerFactory获取一个调度器
//			   SchedulerFactory schedulerfactory=new StdSchedulerFactory();
//			   Scheduler scheduler=null;
//			   
//			   SchedulerFactory schedulerfactory1=new StdSchedulerFactory();
//			   Scheduler scheduler1=null;
//			   
//			   SchedulerFactory schedulerfactory2=new StdSchedulerFactory();
//			   Scheduler scheduler2=null;
//			   
//			   SchedulerFactory schedulerfactory3=new StdSchedulerFactory();
//			   Scheduler scheduler3=null;
//			   
//			   SchedulerFactory schedulerfactory6=new StdSchedulerFactory();
//			   Scheduler scheduler6=null;
//			   
//			   SchedulerFactory schedulerfactoryStock=new StdSchedulerFactory();
//			   Scheduler schedulerStock=null;
//			   
//			   SchedulerFactory schedulerfactoryStockJia=new StdSchedulerFactory();
//			   Scheduler schedulerStockJia=null;
//			   SchedulerFactory schedulerfactory4=new StdSchedulerFactory();
//			   Scheduler scheduler4=null;
////			   SchedulerFactory schedulerfactory3=new StdSchedulerFactory();
////			   Scheduler scheduler3=null;
//			   try{
////				通过schedulerFactory获取一个调度器
//				   scheduler=schedulerfactory.getScheduler();
//				   scheduler1=schedulerfactory1.getScheduler();
//				   scheduler2=schedulerfactory2.getScheduler();
//				   scheduler3=schedulerfactory3.getScheduler();
//				   scheduler4=schedulerfactory4.getScheduler();
//				   scheduler3=schedulerfactory3.getScheduler();
//				   scheduler6=schedulerfactory6.getScheduler();
//				   schedulerStock=schedulerfactoryStock.getScheduler();
//				   schedulerStockJia=schedulerfactoryStockJia.getScheduler();
//				   
////				 创建jobDetail实例，绑定Job实现类
////				 指明job的名称，所在组的名称，以及绑定job类
//				   JobDetail job=JobBuilder.newJob(RanPayOrder.class).withIdentity("job1", "job1").build();
//				   JobDetail job1=JobBuilder.newJob(RanPayOrderLing.class).withIdentity("job2", "job2").build();
//				   JobDetail job2=JobBuilder.newJob(GetTrueOrderInfo.class).withIdentity("job33", "job3").build();
//				   JobDetail job3=JobBuilder.newJob(UpdateOrderInfoStauts.class).withIdentity("job4", "job4").build();
//				   JobDetail job4=JobBuilder.newJob(GetEjlGoodsInfo.class).withIdentity("job5", "job5").build();
//				   JobDetail job6=JobBuilder.newJob(UpdateEjlOrder.class).withIdentity("job66", "job66").build();
//				   JobDetail jobStock=JobBuilder.newJob(StockNumInfo.class).withIdentity("jobStock", "jobStock").build();
//				   JobDetail jobStockJia=JobBuilder.newJob(StockNumInfoJia.class).withIdentity("jobStockJia", "jobStockJia").build();
//				 
//				    
////				 定义调度触发规则
//				   
////				使用simpleTrigger规则
////				  Trigger trigger=TriggerBuilder.newTrigger().withIdentity("simpleTrigger1", "triggerGroup1")
////						          .withSchedule(SimpleScheduleBuilder.repeatSecondlyForever(1).withRepeatCount(8))
////						          .startNow().build();
////				使用cornTrigger规则 
//				   //用来定时白天的订单 每12分钟一次，有五分之三的概率添加一条记录
//				   Trigger trigger6=TriggerBuilder.newTrigger().withIdentity("simpleTrigger66", "job66")
//			          .withSchedule(CronScheduleBuilder.cronSchedule("0 0/3 * * * ? *")).startNow().build(); ;
//				   
//			          Trigger triggerStock=TriggerBuilder.newTrigger().withIdentity("simpleTriggerStock", "jobStock")
//			        		  .withSchedule(CronScheduleBuilder.cronSchedule("0 0/1 9-18 ? * 2,3,5,7 *")).startNow().build();
//			        		  
//			        Trigger triggerStockJia=TriggerBuilder.newTrigger().withIdentity("simpleTriggerStockJia", "jobStockJia")
//			        		.withSchedule(CronScheduleBuilder.cronSchedule("0 0/1 9-18 ? * 4,6 *")).startNow().build();
//			        		  
//			          Trigger trigger=TriggerBuilder.newTrigger().withIdentity("simpleTrigger1", "job1")
//			          .withSchedule(CronScheduleBuilder.cronSchedule("0 0/3 9,10,11,12,13,14,15,16,17,18,19 * * ? *"))
//			          .startNow().build(); 
//					  //用来定时夜间的订单 规则为 每30分钟触发一次，有十分之一的概率添加一条记录
//					  Trigger trigger1=TriggerBuilder.newTrigger().withIdentity("simpleTrigger2", "simpleTrigger2")
//							  .withSchedule(CronScheduleBuilder.cronSchedule("0 0/25 22,23,1,2,3,5,6,7,8 * * ? *"))
//							  .startNow().build(); 
//					  
//					  //用来获取白天的真实订单  5分钟一次
//					  Trigger trigger2=TriggerBuilder.newTrigger().withIdentity("simpleTrigger3", "simpleTrigger3")
//							  .withSchedule(CronScheduleBuilder.cronSchedule("0 0/10 9,10,11,12,13,14,15,16,17,18,19,20 * * ? *"))
//							  .startNow().build(); 
//					  
////					  //用来定时更新已有信息的订单状态
//					  Trigger trigger3=TriggerBuilder.newTrigger().withIdentity("simpleTrigger4", "simpleTrigger4")
//							  .withSchedule(CronScheduleBuilder.cronSchedule("0 0 2 * * ?"))//0 0 2 * * ?
//							  .startNow().build(); 
//					  
//////					  //定时遍历ejl中间库商品数据
//					  Trigger trigger4=TriggerBuilder.newTrigger().withIdentity("simpleTrigger5", "simpleTrigger5")
//							  .withSchedule(CronScheduleBuilder.cronSchedule("0 0/5 0,1,2,3,4,5,20,21,22,19 * * ?"))//0 0 2 * * ?
//							  .startNow().build(); 
//				   //      0 0/1 14,18 * * ?  在每天下午2点到2:55期间和下午6点到6:55期间的每5分钟触发 
////				 把作业和触发器注册到任务调度中
//				   scheduler.scheduleJob(job, trigger);
////				   scheduler1.scheduleJob(job1, trigger1);
//				   scheduler2.scheduleJob(job2, trigger2);
//				   scheduler3.scheduleJob(job3, trigger3);
//				   scheduler4.scheduleJob(job4, trigger4);
//				   scheduler6.scheduleJob(job6, trigger6);
//				   schedulerStock.scheduleJob(jobStock, triggerStock);
//				   schedulerStockJia.scheduleJob(jobStockJia, triggerStockJia);
//				   
////				 启动调度
//				  scheduler.start();
////				   scheduler1.start();
//				   scheduler2.start(); 
//				   scheduler3.start();
//				   scheduler4.start();
//				   scheduler6.start();
//				   schedulerStock.start();
//				   schedulerStockJia.start();
//				   
//				   
//			   }catch(Exception e){
//				   e.printStackTrace();
//				   session.put("aaaa", e.getMessage());
//				   return ERROR;
//			  }
//			   
//			return SUCCESS;
//		}
		private CtXwStock stock = new CtXwStock();
		
		public CtXwStock getStock() {
			return stock;
		}

		public void setStock(CtXwStock stock) {
			this.stock = stock;
		}

		public  String XWStockInfo(){
			stock = fgoodsManager.findByXWStock();
			session.put("XWStockPrice", new BigDecimal(stock.getStockAllprice()));
			return SUCCESS;
		}
		
		private String simCount;
		private String parCount;
		private String countprice;
		
		public String getCountprice() {
			return countprice;
		}

		public void setCountprice(String countprice) {
			this.countprice = countprice;
		}

		public String getSimCount() {
			return simCount;
		}

		public void setSimCount(String simCount) {
			this.simCount = simCount;
		}

		public String getParCount() {
			return parCount;
		}

		public void setParCount(String parCount) {
			this.parCount = parCount;
		}

		//用户在批量失去焦点的计算
		public String parCountAjax(){
			
			return SUCCESS;
		}
		
		//用户在样品区失去焦点的计算
		public String simCountInAjax(){
			String allCount = fgoodsDTO.getAllCount();
			String simCount = fgoodsDTO.getSimCount();
			String parCount = fgoodsDTO.getParCount();
			int typeKorPan = fgoodsDTO.getType();
			String allCountNew = (String) session.get(fgoodsDTO.getGid()+"allCount1");
			String simCountNew = (String) session.get(fgoodsDTO.getGid()+"simCount1");
			String parCountNew = (String) session.get(fgoodsDTO.getGid()+"parCount1");
			System.out.println(session.get(fgoodsDTO.getGid()+"typeKorPanSim"));
			int typeKorPanNew = session.get(fgoodsDTO.getGid()+"typeKorPanSim")==null ? 1 : (Integer)  session.get(fgoodsDTO.getGid()+"typeKorPanSim");
			if(!simCount.equals(simCountNew) || !parCountNew.equals(parCount) || typeKorPan != typeKorPanNew){
				Long gid = fgoodsDTO.getGid();
				goodsInfo = fgoodsManager.getGood(fgoodsDTO.getGid());
				rangePricesList = basicManager.getAllRanPrice(fgoodsDTO.getGid());
				System.out.println(Double.valueOf(goodsInfo.getPack1Num())/1000);
				if(typeKorPan == 1){
					parCount = df.format(Double.valueOf(parCount) * Double.valueOf(Double.valueOf(goodsInfo.getPack1Num())/1000));
				}
				parCount = df.format(Double.valueOf(parCount) * 1000);
				String[] sims = new String[rangePricesList.size()];
				String[] sime = new String[rangePricesList.size()];
				String[] siminc = new String[rangePricesList.size()];
				String[] simprice = new String[rangePricesList.size()];
				
				String[] pars = new String[rangePricesList.size()];
				String[] pare = new String[rangePricesList.size()];
				String[] parinc = new String[rangePricesList.size()];
				String[] parprice = new String[rangePricesList.size()];
				for (int i = 0; i < rangePricesList.size(); i++) {
					sims[i]=rangePricesList.get(i).getSimSNum()== null ? "" :rangePricesList.get(i).getSimSNum().toString() ;
					sime[i]=rangePricesList.get(i).getSimENum()== null ? "" :rangePricesList.get(i).getSimENum().toString();
					siminc[i] = rangePricesList.get(i).getSimIncrease()== null ? "" :rangePricesList.get(i).getSimIncrease().toString();
					simprice[i] = rangePricesList.get(i).getSimRPrice()== null ? "" :rangePricesList.get(i).getSimRPrice().toString();
					
					pars[i] = rangePricesList.get(i).getParSNum()== null ? "" :rangePricesList.get(i).getParSNum().toString();
					pare[i] = rangePricesList.get(i).getParENum() == null ? "" : rangePricesList.get(i).getParENum().toString();
					parinc[i] = rangePricesList.get(i).getParIncrease()== null ? "" :rangePricesList.get(i).getParIncrease().toString();
					parprice[i] = rangePricesList.get(i).getParRPrice()== null ? "" :rangePricesList.get(i).getParRPrice().toString();
				}
				boolean isling = false;
				int sheng = Integer.valueOf(allCount) - Integer.valueOf(parCount);
				parsheng = Integer.valueOf(simCount);
				boolean issim = isparjibie(sims,sime,siminc,pars,pare,parinc);//判断是否还是批量
				if((!sims[0].equals(""))&&parsheng != 0 && parsheng < Integer.valueOf(sims[0])){
					parsheng = Integer.valueOf(sims[0]);
				} else if(parsheng == 0) {
					isling = true;
				}
				int type = issimorpar(sims, sime, pars, pare, parsheng);
				if(isling) {
					parsheng = 0;
				}
				int i = 0;
				if(type == 1){
					i = 0;
				}
				if(type == 2){
					i = 1;
				}
				if(type == 3){
					i = 2;
				}
				if(type == 4){
					i = 3;
				}
				if(type == 5){
					i = 4;
				}
				if(type == 10){
					i = 0;
				}
				String chu = siminc[i].equals("") ? "0.0" : (Integer.valueOf(simCount) / Double.valueOf(siminc[i]) + "");
				String[] chai = chu.split("\\.");
				if(!chai[1].equals("0")){
					Integer zheng = Integer.valueOf(chai[0]) * Integer.valueOf(siminc[i]);
					Integer cha = parsheng - zheng;
					simCount = parsheng - cha + Integer.valueOf(siminc[i]) + "";
				} else {
					if(!siminc[i].equals("")){
						simCount = parsheng.toString();
					} else {
						simCount = "0";
					}
				}
				String chuqusim = Integer.valueOf(allCount) - Integer.valueOf(sheng) + "";
				allCount = Integer.valueOf(chuqusim) + Integer.valueOf(simCount) + "";
				result = fomateCount(sims,sime,siminc,pars,pare,parinc,Integer.valueOf(allCount),simprice,parprice);
				session.put(fgoodsDTO.getGid()+"allCount1", allCount);
				session.put(fgoodsDTO.getGid()+"simCount1", simCount);
				session.put(fgoodsDTO.getGid()+"parCount1", typeKorPan == 1 ? df.format((Double.valueOf(parCount)/1000)/(Double.valueOf(goodsInfo.getPack1Num())/1000)): df.format(Double.valueOf(parCount)/1000));
				session.put(fgoodsDTO.getGid()+"typeKorPanSim", typeKorPan);
				session.put(fgoodsDTO.getGid()+"resultSimCount1", result);
			} else {
				result = (String) session.get(fgoodsDTO.getGid()+"resultSimCount1");
			}
			
			return SUCCESS;
		}
		
		private DecimalFormat df = new DecimalFormat("#.##");
		
		public DecimalFormat getDf() {
			return df;
		}

		public void setDf(DecimalFormat df) {
			this.df = df;
		}
		
		public String fomateCount(String[] sims, String[] sime,
				String[] siminc, String[] pars, String[] pare, String[] parinc,Integer count,String[] simprice,String[] parprice){
			Integer old = (Integer) session.get(fgoodsDTO.getGid()+"countsumold");
			Integer t = (Integer) session.get(fgoodsDTO.getGid()+"typeKorPan");
			String dansimprice = "";
			String danparprice = "";
			String allsimprice = "";
			String allparprice = "";
			int typeKorPan = fgoodsDTO.getType();
			int simqujian = 0;
			int parqujian = 0;
			boolean isling = false;
			if(!count.equals(old) || t != typeKorPan){
				System.out.println("进入了计算");
				if(count == 0){
					isling = true;
				}
				int type = issimorpar(sims, sime, pars, pare, count);
				if(isling){
					count = 0;
				}
				//System.out.println(type);
				switch (type) {
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
					int i = 0;
					if(type == 1){
						i = 0;
					}
					if(type == 2){
						i = 1;
					}
					if(type == 3){
						i = 2;
					}
					if(type == 4){
						i = 3;
					}
					if(type == 5){
						i = 4;
					}
					String chu = count / Double.valueOf(siminc[i]) + "";
					String[] chai = chu.split("\\.");
					if(!chai[1].equals("0")){
						Integer zheng = Integer.valueOf(chai[0]) * Integer.valueOf(siminc[i]);
						Integer cha = count - zheng;
						simCount = count - cha + Integer.valueOf(siminc[i]) + "";
						parCount = "0";
						countprice = df.format(Double.valueOf(simCount) * Double.valueOf(simprice[i]));
					} else {
						simCount = count.toString();
						countprice = df.format(Double.valueOf(simCount) * Double.valueOf(simprice[i]));
						parCount = "0";
					}
					dansimprice = simprice[i];
					danparprice = "0";
					allsimprice = countprice;
					allparprice = "0";
					simqujian = i;
					parqujian = 8;
					break;
				case 6:
				case 7:
				case 8:
				case 9:
					int j = 0;
					if(type == 6){
						j = 0;
					}
					if(type == 7){
						j = 1;
					}
					if(type == 8){
						j = 2;
					}
					if(type == 9){
						j = 3;
					}
					String parchu = Double.valueOf(count) / Double.valueOf(goodsInfo.getPack1Num()) + "";
					String parChai[] = parchu.split("\\.");
					if(!parChai[1].equals("0")){
						Integer parzheng = Integer.valueOf(parChai[0]) * Integer.valueOf(goodsInfo.getPack1Num());
						parsheng = count - parzheng;
					
						if(!sims[0].equals("")){
							boolean isok = false;
							do {
								isok = isparjibie(sims,sime,siminc,pars,pare,parinc);//判断是否还是批量
								if(isok){
									Integer she = isparjibiechai(sims,sime,siminc,pars,pare,new String[]{goodsInfo.getPack1Num().toString(),
											goodsInfo.getPack1Num().toString(),
											goodsInfo.getPack1Num().toString()});
									parsheng -= she;
									parzheng += she;
									isok = true;
								} else {
									simCount = parsheng.toString();
									isok = false;
								}
							} while (isok);
						} else {
							parzheng += Integer.valueOf(parinc[j]);
						}
						parCount = parzheng.toString(); 
						
					} else {
						parCount = df.format(Double.valueOf(parChai[0]) * Double.valueOf(goodsInfo.getPack1Num()));
						countprice = "0";
					}
					if(simCount != null && !simCount.equals("0")){
						int k = simQuJian(simCount,sims,sime,siminc,pars,pare,parinc);
						simqujian = k;
						countprice = df.format(Double.valueOf(simCount) * Double.valueOf(simprice[k]));
						dansimprice = simprice[k];
						allsimprice = countprice;
					} else {
						simqujian = 8;
						simCount = "0";
						dansimprice = simprice[0];
						allsimprice = "0";
					}
					String par = df.format(Double.valueOf(parCount) * Double.valueOf(parprice[j]));
					danparprice = parprice[j];
					allparprice = par;
					countprice = Double.valueOf(countprice == null ? "0" : countprice) + Double.valueOf(par) + "";
					countprice = df.format(Double.valueOf(countprice));
					parqujian = j;
					break;
				case 10:
					simCount = sims[0];
					countprice = df.format(Double.valueOf(simCount) * Double.valueOf(simprice[0]));
					parCount = "0";
					simqujian = 0;
					parqujian = 8;
					dansimprice = simprice[0];
					allsimprice = countprice;
					danparprice = "0";
					allparprice = "0";
					break;
				case 11:
					simCount = sims[1];
					countprice = df.format(Double.valueOf(simCount) * Double.valueOf(simprice[1]));
					parCount = "0";
					simqujian = 1;
					parqujian = 8;
					dansimprice = simprice[1];
					allsimprice = countprice;
					danparprice = "0";
					allparprice = "0";
					break;
				case 12:
					simCount = sims[2];
					countprice = df.format(Double.valueOf(simCount) * Double.valueOf(simprice[2]));
					parCount = "0";
					simqujian = 2;
					parqujian = 8;
					dansimprice = simprice[2];
					allsimprice = countprice;
					danparprice = "0";
					allparprice = "0";
					break;
				case 13:
					simCount = sims[3];
					countprice = df.format(Double.valueOf(simCount) * Double.valueOf(simprice[3]));
					parCount = "0";
					simqujian = 3;
					parqujian = 8;
					dansimprice = simprice[3];
					allsimprice = countprice;
					danparprice = "0";
					allparprice = "0";
					break;
				case 14:
					simCount = sims[4];
					countprice = df.format(Double.valueOf(simCount) * Double.valueOf(simprice[4]));
					parCount = "0";
					simqujian = 4;
					parqujian = 8;
					dansimprice = simprice[4];
					allsimprice = countprice;
					danparprice = "0";
					allparprice = "0";
					break;
				case 15:
					simCount = sims[5];
					countprice = df.format(Double.valueOf(simCount) * Double.valueOf(simprice[5]));
					parCount = "0";
					simqujian = 5;
					parqujian = 8;
					dansimprice = simprice[5];
					allsimprice = countprice;
					danparprice = "0";
					allparprice = "0";
					break;
				case 30:
					simCount = "0";
					parCount = pars[0];
					countprice = df.format(Double.valueOf(parCount) * Double.valueOf(parprice[0]));
					simqujian = 8;
					parqujian = 0;
					dansimprice = simprice[0];
					allsimprice = "0";
					danparprice = parprice[0];
					allparprice = countprice;
					break;
				case 22:
					Integer chupardesim = Integer.valueOf(count) - Integer.valueOf(pare[0]);
					int tempparsheng = parsheng;
					parsheng = chupardesim;
					boolean issim = isparjibie(sims,sime,siminc,pars,pare,parinc);//判断是否还是批量
					int typesim = issimorpar(sims, sime, pars, pare, parsheng);
					int n = 0;
					if(typesim == 1){
						n = 0;
					}
					if(typesim == 2){
						n = 1;
					}
					if(typesim == 3){
						n = 2;
					}
					simCount = parsheng.toString();
					parCount = pare[0];
					simqujian = n;
					parqujian = 0;
					dansimprice = simprice[n];
					allsimprice = df.format(Double.valueOf(simprice[n]) * Double.valueOf(parsheng));
					danparprice = parprice[0];
					countprice = df.format(Double.valueOf(parCount) * Double.valueOf(parprice[0]));
					allparprice = countprice;
					parsheng = tempparsheng;
					countprice = df.format(Double.valueOf(countprice) + Double.valueOf(allsimprice));
					break;
				case 23:
					Integer chupardesims = Integer.valueOf(count) - Integer.valueOf(pare[1]);
					int tempparshengs = parsheng;
					parsheng = chupardesims;
					boolean issims = isparjibie(sims,sime,siminc,pars,pare,parinc);//判断是否还是批量
					int typesims = issimorpar(sims, sime, pars, pare, parsheng);
					int ns = 0;
					if(typesims == 1){
						ns = 0;
					}
					if(typesims == 2){
						ns = 1;
					}
					if(typesims == 3){
						ns = 2;
					}
					
					simCount = parsheng.toString();
					parCount = pare[1];
					simqujian = ns;
					parqujian = 1;
					dansimprice = simprice[ns];
					allsimprice =  df.format(Double.valueOf(simprice[ns]) * Double.valueOf(parsheng));
					danparprice = parprice[1];
					countprice = df.format(Double.valueOf(parCount) * Double.valueOf(parprice[1]));
					allparprice = countprice;
					countprice = df.format(Double.valueOf(countprice) + Double.valueOf(allsimprice));
					break;
				case 24:
					Integer chupardesimsss = Integer.valueOf(count) - Integer.valueOf(pare[2]);
					int tempparshengsss = parsheng;
					parsheng = chupardesimsss;
					boolean issimsss = isparjibie(sims,sime,siminc,pars,pare,parinc);//判断是否还是批量
					int typesimsss = issimorpar(sims, sime, pars, pare, parsheng);
					int nsss = 0;
					if(typesimsss == 1){
						nsss = 0;
					}
					if(typesimsss == 2){
						nsss = 1;
					}
					if(typesimsss == 3){
						nsss = 2;
					}
					
					simCount = parsheng.toString();
					parCount = pare[2];
					simqujian = nsss;
					parqujian = 2;
					dansimprice = simprice[nsss];
					allsimprice =  df.format(Double.valueOf(simprice[nsss]) * Double.valueOf(parsheng));
					danparprice = parprice[2];
					countprice = df.format(Double.valueOf(parCount) * Double.valueOf(parprice[2]));
					allparprice = countprice;
					countprice = df.format(Double.valueOf(countprice) + Double.valueOf(allsimprice));
					break;
				case 25:
					Integer chupardesimss = Integer.valueOf(count) - Integer.valueOf(pare[3]);
					int tempparshengss = parsheng;
					parsheng = chupardesimss;
					boolean issimss = isparjibie(sims,sime,siminc,pars,pare,parinc);//判断是否还是批量
					int typesimss = issimorpar(sims, sime, pars, pare, parsheng);
					int nss = 0;
					if(typesimss == 1){
						nss = 0;
					}
					if(typesimss == 2){
						nss = 1;
					}
					if(typesimss == 3){
						nss = 2;
					}
					
					simCount = parsheng.toString();
					parCount = pare[3];
					simqujian = nss;
					parqujian = 3;
					dansimprice = simprice[nss];
					allsimprice =  df.format(Double.valueOf(simprice[nss]) * Double.valueOf(parsheng));
					danparprice = parprice[3];
					countprice = df.format(Double.valueOf(parCount) * Double.valueOf(parprice[3]));
					allparprice = countprice;
					countprice = df.format(Double.valueOf(countprice) + Double.valueOf(allsimprice));
					break;
					
				default:
					break;
				}
				Integer countsum = Integer.valueOf(simCount) + Integer.valueOf(parCount);
				if(typeKorPan == 1){
					parCount = df.format(Double.valueOf(parCount) / Double.valueOf(goodsInfo.getPack1Num()));
					parCount = df.format(Double.valueOf(parCount) * 1000);
				}
				session.put("countsumold", countsum);
				try {
					parCount = df.format(Double.valueOf(parCount) / 1000);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
				System.out.println("新的样品数量：" + simCount);
				System.out.println("新的批量数量：" + parCount);
				session.put(fgoodsDTO.getGid()+"simCount", simCount);
				session.put(fgoodsDTO.getGid()+"parCount", parCount);
				session.put(fgoodsDTO.getGid()+"countprice", countprice);
				session.put(fgoodsDTO.getGid()+"simqujian", simqujian);
				session.put(fgoodsDTO.getGid()+"parqujian", parqujian);
				session.put(fgoodsDTO.getGid()+"typeKorPan", typeKorPan);
				session.put(fgoodsDTO.getGid()+"dansimprice", dansimprice);
				session.put(fgoodsDTO.getGid()+"allsimprice", allsimprice);
				session.put(fgoodsDTO.getGid()+"danparprice", Double.valueOf(danparprice)*1000);
				session.put(fgoodsDTO.getGid()+"allparprice", allparprice);
				result = simCount + "____" + parCount + "____"+
								countsum + "____" + countprice + "____" + 
								simqujian + "____" + parqujian + "____" + 
								dansimprice + "____" + allsimprice + "____" +
								Double.valueOf(danparprice)*1000 + "____" + allparprice;
				session.put(fgoodsDTO.getGid()+"resultSimCount1", result);
			} else {
				result = (String) session.get(fgoodsDTO.getGid()+"resultSimCount1");
			}
			return result;
		}

		//计算总数及价格
		public String congAjax(){
			Integer count = Integer.valueOf(fgoodsDTO.getCont());
			Integer old = (Integer) session.get(fgoodsDTO.getGid()+"countsumold");
			Integer t = (Integer) session.get(fgoodsDTO.getGid()+"typeKorPan");
			int typeKorPan = fgoodsDTO.getType();
			int simqujian = 0;
			int parqujian = 0;
			String dansimprice = "";
			String danparprice = "";
			String allsimprice = "";
			String allparprice = "";
			if(!count.equals(old) || t != typeKorPan){
				goodsInfo = fgoodsManager.getGood(fgoodsDTO.getGid());
				rangePricesList = basicManager.getAllRanPrice(fgoodsDTO.getGid());
				String[] sims = new String[rangePricesList.size()];
				String[] sime = new String[rangePricesList.size()];
				String[] siminc = new String[rangePricesList.size()];
				String[] simprice = new String[rangePricesList.size()];
				
				String[] pars = new String[rangePricesList.size()];
				String[] pare = new String[rangePricesList.size()];
				String[] parinc = new String[rangePricesList.size()];
				String[] parprice = new String[rangePricesList.size()];
				for (int i = 0; i < rangePricesList.size(); i++) {
					sims[i]=rangePricesList.get(i).getSimSNum()== null ? "" :rangePricesList.get(i).getSimSNum().toString() ;
					sime[i]=rangePricesList.get(i).getSimENum()== null ? "" :rangePricesList.get(i).getSimENum().toString();
					siminc[i] = rangePricesList.get(i).getSimIncrease()== null ? "" :rangePricesList.get(i).getSimIncrease().toString();
					simprice[i] = rangePricesList.get(i).getSimRPrice()== null ? "" :rangePricesList.get(i).getSimRPrice().toString();
					
					pars[i] = rangePricesList.get(i).getParSNum()== null ? "" :rangePricesList.get(i).getParSNum().toString();
					pare[i] = rangePricesList.get(i).getParENum() == null ? "" : rangePricesList.get(i).getParENum().toString();
					parinc[i] = rangePricesList.get(i).getParIncrease()== null ? "" :rangePricesList.get(i).getParIncrease().toString();
					parprice[i] = rangePricesList.get(i).getParRPrice()== null ? "" :rangePricesList.get(i).getParRPrice().toString();
				}
				
				//Scanner input = new Scanner(System.in);
				//System.out.println("输入一个数");
				//String in = input.nextLine();
				
				
				boolean isling = false;
				if(count == 0){
					isling = true;
				}
				int type = issimorpar(sims, sime, pars, pare, count);
				if(isling){
					count = 0;
				}
				//System.out.println(type);
				switch (type) {
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
					int i = 0;
					if(type == 1){
						i = 0;
					}
					if(type == 2){
						i = 1;
					}
					if(type == 3){
						i = 2;
					}
					if(type == 4){
						i = 3;
					}
					if(type == 5){
						i = 4;
					}
					String chu = count / Double.valueOf(siminc[i]) + "";
					String[] chai = chu.split("\\.");
					if(!chai[1].equals("0")){
						Integer zheng = Integer.valueOf(chai[0]) * Integer.valueOf(siminc[i]);
						Integer cha = count - zheng;
						simCount = count - cha + Integer.valueOf(siminc[i]) + "";
						parCount = "0";
						countprice = df.format(Double.valueOf(simCount) * Double.valueOf(simprice[i]));
					} else {
						simCount = count.toString();
						countprice = df.format(Double.valueOf(simCount) * Double.valueOf(simprice[i]));
						parCount = "0";
					}
					dansimprice = simprice[i];
					danparprice = "0";
					allsimprice = countprice;
					allparprice = "0";
					simqujian = i;
					parqujian = 8;
					break;
				case 6:
				case 7:
				case 8:
				case 9:
					int j = 0;
					if(type == 6){
						j = 0;
					}
					if(type == 7){
						j = 1;
					}
					if(type == 8){
						j = 2;
					}
					if(type == 9){
						j = 3;
					}
					String parchu = Double.valueOf(count) / Double.valueOf(goodsInfo.getPack1Num()) + "";
					String parChai[] = parchu.split("\\.");
					if(!parChai[1].equals("0")){
						Integer parzheng = Integer.valueOf(parChai[0]) * Integer.valueOf(goodsInfo.getPack1Num());
						parsheng = count - parzheng;
					
						if(!sims[0].equals("")){
							boolean isok = false;
							do {
								isok = isparjibie(sims,sime,siminc,pars,pare,parinc);//判断是否还是批量
								if(isok){
									Integer she = isparjibiechai(sims,sime,siminc,pars,pare,new String[]{goodsInfo.getPack1Num().toString(),
											goodsInfo.getPack1Num().toString(),
											goodsInfo.getPack1Num().toString()});
									parsheng -= she;
									parzheng += she;
									isok = true;
								} else {
									simCount = parsheng.toString();
									isok = false;
								}
							} while (isok);
						} else {
							parzheng += Integer.valueOf(parinc[j]);
						}
						parCount = parzheng.toString(); 
						
					} else {
						parCount = df.format(Double.valueOf(parChai[0]) * Double.valueOf(goodsInfo.getPack1Num()));
						countprice = "0";
					}
					if(simCount != null && !simCount.equals("0")){
						int k = simQuJian(simCount,sims,sime,siminc,pars,pare,parinc);
						simqujian = k;
						countprice = df.format(Double.valueOf(simCount) * Double.valueOf(simprice[k]));
						dansimprice = simprice[k];
						allsimprice = countprice;
					} else {
						simqujian = 8;
						simCount = "0";
						dansimprice = simprice[0];
						allsimprice = "0";
					}
					String par = df.format(Double.valueOf(parCount) * Double.valueOf(parprice[j]));
					danparprice = parprice[j];
					allparprice = par;
					countprice = Double.valueOf(countprice == null ? "0" : countprice) + Double.valueOf(par) + "";
					countprice = df.format(Double.valueOf(countprice));
					parqujian = j;
					break;
				case 10:
					simCount = sims[0];
					countprice = df.format(Double.valueOf(simCount) * Double.valueOf(simprice[0]));
					parCount = "0";
					simqujian = 0;
					parqujian = 8;
					dansimprice = simprice[0];
					allsimprice = countprice;
					danparprice = "0";
					allparprice = "0";
					break;
				case 11:
					simCount = sims[1];
					countprice = df.format(Double.valueOf(simCount) * Double.valueOf(simprice[1]));
					parCount = "0";
					simqujian = 1;
					parqujian = 8;
					dansimprice = simprice[1];
					allsimprice = countprice;
					danparprice = "0";
					allparprice = "0";
					break;
				case 12:
					simCount = sims[2];
					countprice = df.format(Double.valueOf(simCount) * Double.valueOf(simprice[2]));
					parCount = "0";
					simqujian = 2;
					parqujian = 8;
					dansimprice = simprice[2];
					allsimprice = countprice;
					danparprice = "0";
					allparprice = "0";
					break;
				case 13:
					simCount = sims[3];
					countprice = df.format(Double.valueOf(simCount) * Double.valueOf(simprice[3]));
					parCount = "0";
					simqujian = 3;
					parqujian = 8;
					dansimprice = simprice[3];
					allsimprice = countprice;
					danparprice = "0";
					allparprice = "0";
					break;
				case 14:
					simCount = sims[4];
					countprice = df.format(Double.valueOf(simCount) * Double.valueOf(simprice[4]));
					parCount = "0";
					simqujian = 4;
					parqujian = 8;
					dansimprice = simprice[4];
					allsimprice = countprice;
					danparprice = "0";
					allparprice = "0";
					break;
				case 15:
					simCount = sims[5];
					countprice = df.format(Double.valueOf(simCount) * Double.valueOf(simprice[5]));
					parCount = "0";
					simqujian = 5;
					parqujian = 8;
					dansimprice = simprice[5];
					allsimprice = countprice;
					danparprice = "0";
					allparprice = "0";
					break;
				case 30:
					simCount = "0";
					parCount = pars[0];
					countprice = df.format(Double.valueOf(parCount) * Double.valueOf(parprice[0]));
					simqujian = 8;
					parqujian = 0;
					dansimprice = simprice[0];
					allsimprice = "0";
					danparprice = parprice[0];
					allparprice = countprice;
					break;
				case 22:
					Integer chupardesim = Integer.valueOf(count) - Integer.valueOf(pare[0]);
					int tempparsheng = parsheng;
					parsheng = chupardesim;
					boolean issim = isparjibie(sims,sime,siminc,pars,pare,parinc);//判断是否还是批量
					int typesim = issimorpar(sims, sime, pars, pare, parsheng);
					int n = 0;
					if(typesim == 1){
						n = 0;
					}
					if(typesim == 2){
						n = 1;
					}
					if(typesim == 3){
						n = 2;
					}
					simCount = parsheng.toString();
					parCount = pare[0];
					simqujian = n;
					parqujian = 0;
					dansimprice = simprice[n];
					allsimprice = df.format(Double.valueOf(simprice[n]) * Double.valueOf(parsheng));
					danparprice = parprice[0];
					countprice = df.format(Double.valueOf(parCount) * Double.valueOf(parprice[0]));
					allparprice = countprice;
					parsheng = tempparsheng;
					countprice = df.format(Double.valueOf(countprice) + Double.valueOf(allsimprice));
					break;
				case 23:
					Integer chupardesims = Integer.valueOf(count) - Integer.valueOf(pare[1]);
					int tempparshengs = parsheng;
					parsheng = chupardesims;
					boolean issims = isparjibie(sims,sime,siminc,pars,pare,parinc);//判断是否还是批量
					int typesims = issimorpar(sims, sime, pars, pare, parsheng);
					int ns = 0;
					if(typesims == 1){
						ns = 0;
					}
					if(typesims == 2){
						ns = 1;
					}
					if(typesims == 3){
						ns = 2;
					}
					
					simCount = parsheng.toString();
					parCount = pare[1];
					simqujian = ns;
					parqujian = 1;
					dansimprice = simprice[ns];
					allsimprice =  df.format(Double.valueOf(simprice[ns]) * Double.valueOf(parsheng));
					danparprice = parprice[1];
					countprice = df.format(Double.valueOf(parCount) * Double.valueOf(parprice[1]));
					allparprice = countprice;
					countprice = df.format(Double.valueOf(countprice) + Double.valueOf(allsimprice));
					break;
				case 24:
					Integer chupardesimsss = Integer.valueOf(count) - Integer.valueOf(pare[2]);
					int tempparshengsss = parsheng;
					parsheng = chupardesimsss;
					boolean issimsss = isparjibie(sims,sime,siminc,pars,pare,parinc);//判断是否还是批量
					int typesimsss = issimorpar(sims, sime, pars, pare, parsheng);
					int nsss = 0;
					if(typesimsss == 1){
						nsss = 0;
					}
					if(typesimsss == 2){
						nsss = 1;
					}
					if(typesimsss == 3){
						nsss = 2;
					}
					
					simCount = parsheng.toString();
					parCount = pare[2];
					simqujian = nsss;
					parqujian = 2;
					dansimprice = simprice[nsss];
					allsimprice =  df.format(Double.valueOf(simprice[nsss]) * Double.valueOf(parsheng));
					danparprice = parprice[2];
					countprice = df.format(Double.valueOf(parCount) * Double.valueOf(parprice[2]));
					allparprice = countprice;
					countprice = df.format(Double.valueOf(countprice) + Double.valueOf(allsimprice));
					break;
				case 25:
					Integer chupardesimss = Integer.valueOf(count) - Integer.valueOf(pare[3]);
					int tempparshengss = parsheng;
					parsheng = chupardesimss;
					boolean issimss = isparjibie(sims,sime,siminc,pars,pare,parinc);//判断是否还是批量
					int typesimss = issimorpar(sims, sime, pars, pare, parsheng);
					int nss = 0;
					if(typesimss == 1){
						nss = 0;
					}
					if(typesimss == 2){
						nss = 1;
					}
					if(typesimss == 3){
						nss = 2;
					}
					
					simCount = parsheng.toString();
					parCount = pare[3];
					simqujian = nss;
					parqujian = 3;
					dansimprice = simprice[nss];
					allsimprice =  df.format(Double.valueOf(simprice[nss]) * Double.valueOf(parsheng));
					danparprice = parprice[3];
					countprice = df.format(Double.valueOf(parCount) * Double.valueOf(parprice[3]));
					allparprice = countprice;
					countprice = df.format(Double.valueOf(countprice) + Double.valueOf(allsimprice));
					break;
					
				default:
					break;
				}
				Integer countsum = Integer.valueOf(simCount) + Integer.valueOf(parCount);
				if(typeKorPan == 1){
					parCount = df.format(Double.valueOf(parCount) / Double.valueOf(goodsInfo.getPack1Num()));
					parCount = df.format(Double.valueOf(parCount) * 1000);
				}
				session.put("countsumold", countsum);
				try {
					parCount = df.format(Double.valueOf(parCount) / 1000);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
				System.out.println("新的样品数量：" + simCount);
				System.out.println("新的批量数量：" + parCount);
				session.put(fgoodsDTO.getGid()+"simCount", simCount);
				session.put(fgoodsDTO.getGid()+"parCount", parCount);
				session.put(fgoodsDTO.getGid()+"countprice", countprice);
				session.put(fgoodsDTO.getGid()+"simqujian", simqujian);
				session.put(fgoodsDTO.getGid()+"parqujian", parqujian);
				session.put(fgoodsDTO.getGid()+"typeKorPan", typeKorPan);
				session.put(fgoodsDTO.getGid()+"dansimprice", dansimprice);
				session.put(fgoodsDTO.getGid()+"allsimprice", allsimprice);
				session.put(fgoodsDTO.getGid()+"danparprice", Double.valueOf(danparprice)*1000);
				session.put(fgoodsDTO.getGid()+"allparprice", allparprice);
				result = simCount + "____" + parCount + "____"+
								countsum + "____" + countprice + "____" + 
								simqujian + "____" + parqujian + "____" + 
								dansimprice + "____" + allsimprice + "____" +
								Double.valueOf(danparprice)*1000 + "____" + allparprice;
				session.put(fgoodsDTO.getGid()+"resultSimCount1", result);
			} else {
				result = (String) session.get(fgoodsDTO.getGid()+"resultSimCount1");
			}
			return SUCCESS;
		}
		private int simQuJian(String simCount2,String[] sims, String[] sime,
				String[] siminc, String[] pars, String[] pare, String[] parinc) {
			Integer count = parsheng;
			for (int i = 0; i < sims.length; i++) {
				if(count >= Integer.valueOf(sims[i]) && count <= Integer.valueOf(sime[i])){
					//样品第一区间
					//parsheng = Integer.valueOf(sims[1]);
					return i;
				}
			}
			return 0;
		}

		private static Integer parsheng;
		
		public static Integer getParsheng() {
			return parsheng;
		}

		public static void setParsheng(Integer parsheng) {
			F_GoodsAction.parsheng = parsheng;
		}

		private static Integer isparjibiechai(String[] sims, String[] sime,
				String[] siminc, String[] pars, String[] pare, String[] parinc) {
			Integer count = parsheng;
			int i = 0;
			for (int j = 0; j < pare.length-1; j++) {
				if(count >= Integer.valueOf(pare[j]) && count <= Integer.valueOf(pare[j])){
					//批量第一区间
					i = j;
				}
			}
//			if(count >= Integer.valueOf(pare[0]) && count <= Integer.valueOf(pare[0])){
//				//批量第一区间
//				i = 0;
//			}
//			if(count >= Integer.valueOf(pars[1]) && count <= Integer.valueOf(pare[1])){
//				//批量第二区间
//				i = 1;
//			}
			String chu = count / Double.valueOf(parinc[i]) + "";
			String chai[] = chu.split("\\.");
			count = Integer.valueOf(chai[0]) * Integer.valueOf(parinc[i]);
			return count;
		}

		private static boolean isparjibie(String[] sims, String[] sime, String[] siminc, String[] pars, String[] pare, String[] parinc) {
			Integer count = parsheng;
			for (int i = 0; i < sims.length; i++) {
				if(!sims[i].equals("")&&count >= Integer.valueOf(sims[i]) && count <= Integer.valueOf(sime[i])){
					//样品第一区间
					if(count!=0){
						String chu = parsheng / Double.valueOf(siminc[i]) + "";
						String[] chai = chu.split("\\.");
						if(!chai[1].equals("0")){
							Integer zheng = Integer.valueOf(chai[0]) * Integer.valueOf(siminc[i]);
							Integer cha = count - zheng;
							parsheng = count - cha + Integer.valueOf(siminc[i]);
						} else {
							parsheng = count;
						}
					}
					//parsheng = Integer.valueOf(sims[1]);
					return false;
				}
			}
//			if(count >= Integer.valueOf(sims[0]) && count <= Integer.valueOf(sime[0])){
//				//样品第一区间
//				if(count!=0){
//					String chu = parsheng / Double.valueOf(siminc[0]) + "";
//					String[] chai = chu.split("\\.");
//					if(!chai[1].equals("0")){
//						Integer zheng = Integer.valueOf(chai[0]) * Integer.valueOf(siminc[0]);
//						Integer cha = count - zheng;
//						parsheng = count - cha + Integer.valueOf(siminc[0]);
//					} else {
//						parsheng = count;
//					}
//				}
//				//parsheng = Integer.valueOf(sims[1]);
//				return false;
//			}
//			if(count >= Integer.valueOf(sims[1]) && count <= Integer.valueOf(sime[1])){
//				//样品第二区间
//				if(count != 0){
//					String chu = parsheng / Double.valueOf(siminc[1]) + "";
//					String[] chai = chu.split("\\.");
//					if(!chai[1].equals("0")){
//						Integer zheng = Integer.valueOf(chai[0]) * Integer.valueOf(siminc[1]);
//						Integer cha = count - zheng;
//						parsheng = count - cha + Integer.valueOf(siminc[1]);
//					} else {
//						parsheng = count;
//					}
//				}
//				return false;
//			}
//			if(count >= Integer.valueOf(sims[2]) && count <= Integer.valueOf(sime[2])){
//				//样品第三区间
//				if(count != 0){
//					String chu = parsheng / Double.valueOf(siminc[2]) + "";
//					String[] chai = chu.split("\\.");
//					if(!chai[1].equals("0")){
//						Integer zheng = Integer.valueOf(chai[0]) * Integer.valueOf(siminc[2]);
//						Integer cha = count - zheng;
//						parsheng = count - cha + Integer.valueOf(siminc[2]);
//					} else {
//						parsheng = count;
//					}
//				}
//				return false;
//			}
			for (int i = 0; i < sime.length-1; i++) {
				if(count > Integer.valueOf(sime[i]) && count < Integer.valueOf(sims[i+1])){
					//介于样品第一区间和第二区间之间
					parsheng = Integer.valueOf(sims[i+1]);
					return false;
				}
			}
//			if(count > Integer.valueOf(sime[1]) && count < Integer.valueOf(sims[2])){
//				//介于样品第二区间和第三区间之间
//				parsheng = Integer.valueOf(sims[2]);
//				return false;
//			}
			if(!sims[0].equals("") && count < Integer.valueOf(sims[0])){
				//parsheng = Integer.valueOf(sims[0]);
				return false;
			}
			
			
			

//			if(count > Integer.valueOf(pars[0]) && count < Integer.valueOf(pare[0])){
//				//批量第一区间
//				return true;
//			}
//			if(count >= Integer.valueOf(pars[1]) && count <= Integer.valueOf(pare[1])){
//				//批量第二区间
//				return true;
//			}
			for (int i = 0; i < pars.length; i++) {
				if((pars[i] != null&&!pars[i].equals("") || pare[i] != null&&!pare[i].equals("")) && count >= Integer.valueOf(pars[i]) && count <= Integer.valueOf(pare[i].equals("")?"99999999":pare[i])){
					//批量第三区间
					return true;
				}
			}
//			if(count > Integer.valueOf(sime[2]) && count < Integer.valueOf(pars[0])){
//				//大于样品第三区间和小于批量第一区间
//				
//			}
			for (int i = 0; i < pare.length-1; i++) {
				if((pars[i] != null&&!pars[i].equals("") || pare[i] != null&&!pare[i].equals("")) &&count > Integer.valueOf(pare[i]) && count < Integer.valueOf(pars[i+1])){
					//介于批量第一区间和第二区间之间
					parsheng = Integer.valueOf(pars[i+1]);
					return true;
				}
			}
			
//			if(count > Integer.valueOf(pare[1]) && count < Integer.valueOf(pars[2])){
//				//介于批量第二区间和第三区间之间
//				parsheng = Integer.valueOf(pars[2]);
//				return true;
//			}
			parsheng = Integer.valueOf(pars[0]);
			return true;
		}

		private static boolean isyu(Double count, Double inc){
			if(count % inc == 0){
				return true;
			}
			return false;
		}
		
		/**
		 * 
		 * @param sims
		 * @param sime
		 * @param pars
		 * @param pare
		 * @param count
		 * @return 1,样品第一区间
		 * 		   2,样品第二区间
		 * 		   3,样品第三区间
	     *         4,批量第一区间
	     *         5,批量第二区间
	     *         6,批量第三区间
	     *         10,小于样品第一区间
	     *         11,介于样品第一区间和第二区间之间
	     *         12,介于样品第二区间和第三区间之间
	     *         13,大于样品第三区间和小于批量第一区间
	     *         14,介于批量第一区间和第二区间之间
	     *         15,介于批量第二区间和第三区间之间
	     *         0 不在任何区间
		 */
		private static int issimorpar(String[] sims, String[] sime, String[] pars,
				String[] pare, Integer count) {
			try {
				for (int i = 0; i < sims.length; i++) {
					if((sims[i] != null && !sims[i].equals("") && sims[i] != null && !sims[i].equals("") )&&count >= Integer.valueOf(sims[i]) && count <= Integer.valueOf(sime[i])){
						//样品第一区间
						return i+1;
					}
				 }
				if( sims[0] !=null && !sims[0].equals("")){
					if(count < Integer.valueOf(sims[0])){
						//小于样品第一区间
						return 10;
					}
				}
				for (int i = 0; i < sims.length-1; i++) {
					if(count > Integer.valueOf(sime[i]) && count < Integer.valueOf(sims[i+1])){
						//介于样品第一区间和第二区间之间
						return i+11;
					}
				}
				
				for (int i = 0; i < pare.length-1; i++) {
					if((pars[i] != null&&!pars[i].equals("") && pars[i+1] != null&&!pars[i+1].equals("") )&&count > Integer.valueOf(pare[i]) && count < Integer.valueOf(pars[i+1])){
						//介于批量第一区间和第二区间之间
						return i+21;
					}
				}
				for (int i = 0; i < pars.length; i++) {
//					System.out.println(Integer.valueOf(pare[i].equals("")?"99999999":pare[i]));
//					System.out.println(count <= Integer.valueOf(pare[i].equals("")?"99999999":pare[i]));
//					System.out.println(pars[i] != null&&!pars[i].equals("") || pare[i] != null&&!pare[i].equals(""));
//					System.out.println(count >= Integer.valueOf(pars[i]));
					if((pars[i] != null&&!pars[i].equals("") || pare[i] != null&&!pare[i].equals(""))&&count >= Integer.valueOf(pars[i]) && count <= Integer.valueOf(pare[i].equals("")?"99999999":pare[i])){
						//批量第三区间
						return i+6;
					}
				}

//			if(count > Integer.valueOf(sime[2]) && count < Integer.valueOf(pars[0])){
//				//大于样品第三区间和小于批量第一区间
//			}
//			
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			return 30;
		}
	// 过滤特殊字符  
	public static String StringFilter(String str) throws PatternSyntaxException   {     
	     // 只允许字母和数字       
	     // String   regEx  =  "[^a-zA-Z0-9]";                     
	     // 清除掉所有特殊字符  
	     String regEx="[`~!@#$^&*()+=|{}':;',\\[\\].<>/?~！@#￥……&*（）——+|{}【】‘；：”“’。，、？]";
	     Pattern p = Pattern.compile(regEx);
	     Matcher m = p.matcher(str);
	     return m.replaceAll("").trim();
	}  
	
	//重复提交表单
	public String repeatCommit(){
		session.put("errorMess", "请不要重复提交表单");
		session.put("errorTotle", "重复提交");
		session.put("errorBackUrl", "webindex");
		return SUCCESS;
	}
	public String samRegInfoUp(){
		try {
			CtUser cu = (CtUser) this.session.get("userinfosession");
			Long uid = 0L;
			if(cu != null){
				uid = cu.getUId();
			}
			if(sampleReg == null){
				String errorTotle = "完善输入完整性";
				String errorMess = "包含为空字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", "webindex");
//				System.out.println("重复");
				return ERROR;
			}
			String name = sampleReg.getSamCompanyname();
			String type = sampleReg.getSamCompanytype();
			String goodsName = sampleReg.getSamGoodsName();
			String userName = sampleReg.getSamUsername();
			String phone = sampleReg.getSamUserphone();
			name = StringFilter(name);
			type = StringFilter(type);
			goodsName = StringFilter(goodsName);
			userName = StringFilter(userName);
			phone = StringFilter(phone);
			if(name.equals("") || type.equals("") || goodsName.equals("") || userName.equals("") || phone.equals("")){
				String errorTotle = "完善输入完整性";
				String errorMess = "包含为空字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", "webindex");
				return ERROR;
			}
			sampleReg.setSamTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			sampleReg.setSamReguser(uid.toString().equals("0") ? null : uid.toString());
			regManager.saveSamInfo(sampleReg);
			return SUCCESS;
		} catch (PatternSyntaxException e) {
			e.printStackTrace();
			String errorTotle = "完善输入完整性";
			String errorMess = "包含为空字符";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			session.put("errorBackUrl", "webindex");
			return ERROR;
		}
	}
	private List<CtGoods> ctGoodsList = new ArrayList<CtGoods>();
	
	public List<CtGoods> getCtGoodsList() {
		return ctGoodsList;
	}

	public void setCtGoodsList(List<CtGoods> ctGoodsList) {
		this.ctGoodsList = ctGoodsList;
	}

	public String activityInfo(){
		ctGoodsList = (List<CtGoods>) fgoodsManager.findByTypeName("2", "isSample", "CtGoods", 1);
		for (int i = 0; i < ctGoodsList.size(); i++) {
			ctGoodsList.get(i).setPack4Num(ctGoodsList.get(i).getPack1Num() / 1000);
		}
		return SUCCESS;
	}
	public String login_errorpage(){
		return SUCCESS;
	}
	public String findCartNum(){
		CtUser cu = (CtUser) this.session.get("userinfosession");
		if(cu != null){
			Long cartcount = basicManager.getCartGoodsNum(cu.getUId());
			result = cartcount.toString();
		}
		return SUCCESS;
	}
	
	
	/**
	 * 异步查询规格书
	 * @return 
	 */
	public String findResultByGid(){
		goodsInfo = fgoodsManager.getGood(fgoodsDTO.getGId());
		resourcesList = (List<CtResource>) fgoodsManager.findByTypeName(goodsInfo.getGId().toString(), "GId", "CtResource", 1);
		StringBuffer sb = new StringBuffer();
		if(resourcesList != null && resourcesList.size() > 0){
			for (int i = 0; i < resourcesList.size(); i++) {
				sb.append("		<li>");
				sb.append("			<span><b></b>"+resourcesList.get(i).getRName()+"</span>");
				sb.append("			<a target=\"_blank\" href=\""+resourcesList.get(i).getRUrl()+"\">点击下载</a>");
				sb.append("		</li>");
			}
		} else {
			sb.append("noress");
		}
		result = sb.toString();
		return SUCCESS;
	}
}

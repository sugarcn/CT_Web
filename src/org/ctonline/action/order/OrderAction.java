package org.ctonline.action.order;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;
import org.ctonline.action.BaseAction;
import org.ctonline.action.pay.CtPayAction;
import org.ctonline.config.AlipayConfig;
import org.ctonline.dto.goods.CtBomDTO;
import org.ctonline.dto.goods.CtOrderGoodsDTO;
import org.ctonline.dto.goods.CtRangePriceDTO;
import org.ctonline.dto.goods.ViewCartListDTO;
import org.ctonline.dto.goods.ViewOrderCheckDTO;
import org.ctonline.dto.order.ExDescInfoDTO;
import org.ctonline.dto.order.OrderCartDTO;
import org.ctonline.dto.order.OrderDTO;
import org.ctonline.dto.order.OrderInfoGoodsDTO;
import org.ctonline.dto.order.OrderListDTO;
import org.ctonline.dto.order.PayDTO;
import org.ctonline.dto.pay.CtPayDTO;
import org.ctonline.manager.basic.BasicManager;
import org.ctonline.manager.basic.CtCouponDetailManager;
import org.ctonline.manager.basic.CtRegionManager;
import org.ctonline.manager.goods.CtBomManager;
import org.ctonline.manager.goods.F_GoodsManager;
import org.ctonline.manager.order.ICtBankManager;
import org.ctonline.manager.order.ICtPayManager;
import org.ctonline.manager.order.OrderManager;
import org.ctonline.manager.pay.CtPayManager;
import org.ctonline.manager.user.CtUserAddressManager;
import org.ctonline.po.basic.CtCoupon;
import org.ctonline.po.basic.CtCouponDetail;
import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.basic.CtSystemInfo;
import org.ctonline.po.goods.CtBom;
import org.ctonline.po.goods.CtBomGoods;
import org.ctonline.po.goods.CtCollect;
import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.po.goods.CtRangePrice;
import org.ctonline.po.order.CtBank;
import org.ctonline.po.order.CtCart;
import org.ctonline.po.order.CtComplain;
import org.ctonline.po.order.CtEvaluation;
import org.ctonline.po.order.CtExpress;
import org.ctonline.po.order.CtOrderGoods;
import org.ctonline.po.order.CtOrderInfo;
import org.ctonline.po.order.CtOrderRelation;
import org.ctonline.po.order.CtPay;
import org.ctonline.po.order.CtRefund;
import org.ctonline.po.order.CtShop;
import org.ctonline.po.pay.CtPayInterface;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.user.CtUserAddress;
import org.ctonline.po.views.ViewCartList;
import org.ctonline.po.views.ViewCateNum;
import org.ctonline.po.views.ViewOrderBom;
import org.ctonline.po.views.ViewOrderCheck;
import org.ctonline.po.views.ViewSubcateNum;
import org.ctonline.po.views.ViewUserAddress;
import org.ctonline.test.serviceInterface;
import org.ctonline.util.OSSObjectSample;
import org.ctonline.util.POIExcelDown;
import org.ctonline.util.Page;
import org.ctonline.util.UtilDate;
import org.ctonline.util.WuLiu;
import org.ctonline.util.kuaidi100;
import org.json.JSONArray;
import org.json.JSONObject;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSException;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.opensymphony.xwork2.ModelDriven;


public class OrderAction extends BaseAction implements RequestAware,
		ModelDriven<Object>, SessionAware, ServletResponseAware {

	private ICtBankManager bankManager;
	private ICtPayManager payManager;
	private CtPayManager payInterManager;
	private CtPayInterface payInterface;
	private CtUserAddressManager addressManager;
	private List<CtBank> banks = new ArrayList<CtBank>();
	private int GgoodsNum;
	
	public int getGgoodsNum() {
		return GgoodsNum;
	}

	public CtPayManager getPayInterManager() {
		return payInterManager;
	}

	public void setPayInterManager(CtPayManager payInterManager) {
		this.payInterManager = payInterManager;
	}

	public void setGgoodsNum(int ggoodsNum) {
		GgoodsNum = ggoodsNum;
	}

	public CtBomDTO getBomDTO() {
		return bomDTO;
	}
	public CtUserAddressManager getAddressManager() {
		return addressManager;
	}

	public void setAddressManager(CtUserAddressManager addressManager) {
		this.addressManager = addressManager;
	}

	public void setBomDTO(CtBomDTO bomDTO) {
		this.bomDTO = bomDTO;
	}

	public List<CtBomGoods> getBomGoodsList() {
		return bomGoodsList;
	}

	public void setBomGoodsList(List<CtBomGoods> bomGoodsList) {
		this.bomGoodsList = bomGoodsList;
	}

	public List<CtBank> getBanks() {
		return banks;
	}

	public void setBanks(List<CtBank> banks) {
		this.banks = banks;
	}

	private OrderDTO orderDTO = new OrderDTO();
	public ICtBankManager getBankManager() {
		return bankManager;
	}

	public void setBankManager(ICtBankManager bankManager) {
		this.bankManager = bankManager;
	}

	public ICtPayManager getPayManager() {
		return payManager;
	}

	public void setPayManager(ICtPayManager payManager) {
		this.payManager = payManager;
	}

	private Page page;
	private Map<String, Object> request;
	private BasicManager basicManager;
	private OrderManager orderManager;
	private CtCouponDetailManager coupondetailManager;
	
	private List<CtCouponDetail> couponDetails;
	private List<List<CtCouponDetail>> couponsAll = new ArrayList<List<CtCouponDetail>>();
	private List<CtGoodsCategory> cateList;
	private List<ViewCateNum> catenumList;
	private List<ViewSubcateNum> subcatenumList;
	private Map<String, Object> session;
	private List<CtCart> cartList;
	public List<List<CtCouponDetail>> getCouponsAll() {
		return couponsAll;
	}

	public void setCouponsAll(List<List<CtCouponDetail>> couponsAll) {
		this.couponsAll = couponsAll;
	}

	private List<ViewCartList> viewCart = new ArrayList<ViewCartList>();
	private List<ViewCartListDTO> viewCartDTO = new ArrayList<ViewCartListDTO>();
	
	public List<ViewCartListDTO> getViewCartDTO() {
		return viewCartDTO;
	}

	public void setViewCartDTO(List<ViewCartListDTO> viewCartDTO) {
		this.viewCartDTO = viewCartDTO;
	}

	private List<ViewUserAddress> addressList = new ArrayList<ViewUserAddress>();
	private List<ViewOrderCheck> ocList = new ArrayList<ViewOrderCheck>();
	private List<ViewOrderCheckDTO> ocListDTO = new ArrayList<ViewOrderCheckDTO>();
	
	public List<ViewOrderCheckDTO> getOcListDTO() {
		return ocListDTO;
	}

	public void setOcListDTO(List<ViewOrderCheckDTO> ocListDTO) {
		this.ocListDTO = ocListDTO;
	}

	private List<ViewOrderBom> obList = new ArrayList<ViewOrderBom>();
	private List<CtOrderInfo> orderList;
	private List<?> orderGoods;
	private CtOrderInfo orderInfo = new CtOrderInfo();
	public CtOrderInfo getOrderInfo() {
		return orderInfo;
	}

	public void setOrderInfo(CtOrderInfo orderInfo) {
		this.orderInfo = orderInfo;
	}

	private HttpServletResponse response;
	private List<CtRegion> regions;
	private CtRegionManager regionManager;
	private String result;
	private CtCart cartToNum = new CtCart();
	

	public CtCart getCartToNum() {
		return cartToNum;
	}

	public void setCartToNum(CtCart cartToNum) {
		this.cartToNum = cartToNum;
	}

	public List<CtRegion> getRegions() {
		return regions;
	}

	public void setRegions(List<CtRegion> regions) {
		this.regions = regions;
	}

	public CtRegionManager getRegionManager() {
		return regionManager;
	}

	public void setRegionManager(CtRegionManager regionManager) {
		this.regionManager = regionManager;
	}

	public List<ViewOrderCheck> getOcList() {
		return ocList;
	}

	public void setOcList(List<ViewOrderCheck> ocList) {
		this.ocList = ocList;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public List<ViewOrderBom> getObList() {
		return obList;
	}

	public void setObList(List<ViewOrderBom> obList) {
		this.obList = obList;
	}

	public void setServletResponse(HttpServletResponse response) {
		this.response = response;
		this.response.setCharacterEncoding("UTF-8");
		this.response.setContentType("text/html");
	}

	public List<ViewUserAddress> getAddressList() {
		return addressList;
	}

	public void setAddressList(List<ViewUserAddress> addressList) {
		this.addressList = addressList;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.orderDTO;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}

	public BasicManager getBasicManager() {
		return basicManager;
	}

	public void setBasicManager(BasicManager basicManager) {
		this.basicManager = basicManager;
	}

	public OrderDTO getOrderDTO() {
		return orderDTO;
	}

	public void setOrderDTO(OrderDTO orderDTO) {
		this.orderDTO = orderDTO;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public OrderManager getOrderManager() {
		return orderManager;
	}

	public void setOrderManager(OrderManager orderManager) {
		this.orderManager = orderManager;
	}

	public List<CtCart> getCartList() {
		return cartList;
	}

	public void setCartList(List<CtCart> cartList) {
		this.cartList = cartList;
	}

	public List<ViewCartList> getViewCart() {
		return viewCart;
	}

	public void setViewCart(List<ViewCartList> viewCart) {
		this.viewCart = viewCart;
	}

	public List<CtGoodsCategory> getCateList() {
		return cateList;
	}

	public void setCateList(List<CtGoodsCategory> cateList) {
		this.cateList = cateList;
	}

	public List<ViewCateNum> getCatenumList() {
		return catenumList;
	}

	public void setCatenumList(List<ViewCateNum> catenumList) {
		this.catenumList = catenumList;
	}

	public List<ViewSubcateNum> getSubcatenumList() {
		return subcatenumList;
	}

	public void setSubcatenumList(List<ViewSubcateNum> subcatenumList) {
		this.subcatenumList = subcatenumList;
	}

	public List<CtOrderInfo> getOrderList() {
		return orderList;
	}

	public void setOrderList(List<CtOrderInfo> orderList) {
		this.orderList = orderList;
	}

	public List<?> getOrderGoods() {
		return orderGoods;
	}

	public void setOrderGoods(List<?> orderGoods) {
		this.orderGoods = orderGoods;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	private List<CtBom> boms;
	private CtBomManager bomManager;
	private List<CtCollect> collects;
	private F_GoodsManager fgoodsManager;
	
	public List<CtCollect> getCollects() {
		return collects;
	}

	public void setCollects(List<CtCollect> collects) {
		this.collects = collects;
	}

	public F_GoodsManager getFgoodsManager() {
		return fgoodsManager;
	}

	public void setFgoodsManager(F_GoodsManager fgoodsManager) {
		this.fgoodsManager = fgoodsManager;
	}

	public List<CtBom> getBoms() {
		return boms;
	}

	public void setBoms(List<CtBom> boms) {
		this.boms = boms;
	}

	public CtBomManager getBomManager() {
		return bomManager;
	}

	public void setBomManager(CtBomManager bomManager) {
		this.bomManager = bomManager;
	}
	private List<CtRangePrice> rangePricesList = new ArrayList<CtRangePrice>();
	private List<List<CtRangePriceDTO>> priceAllList = new ArrayList<List<CtRangePriceDTO>>();
	public List<List<CtRangePriceDTO>> getPriceAllList() {
		return priceAllList;
	}

	public void setPriceAllList(List<List<CtRangePriceDTO>> priceAllList) {
		this.priceAllList = priceAllList;
	}

	public List<CtRangePrice> getRangePricesList() {
		return rangePricesList;
	}

	public void setRangePricesList(List<CtRangePrice> rangePricesList) {
		this.rangePricesList = rangePricesList;
	}
	private List<CtCouponDetail> ctCouponDetails = new ArrayList<CtCouponDetail>();
	

	public List<CtCouponDetail> getCtCouponDetails() {
		return ctCouponDetails;
	}

	public void setCtCouponDetails(List<CtCouponDetail> ctCouponDetails) {
		this.ctCouponDetails = ctCouponDetails;
	}
	private List<CtCoupon> coup = new ArrayList<CtCoupon>();
	
	public List<CtCoupon> getCoup() {
		return coup;
	}

	public void setCoup(List<CtCoupon> coup) {
		this.coup = coup;
	}
	private List<CtRangePriceDTO> ranListDTO = new ArrayList<CtRangePriceDTO>();
	
	public List<CtRangePriceDTO> getRanListDTO() {
		return ranListDTO;
	}

	public void setRanListDTO(List<CtRangePriceDTO> ranListDTO) {
		this.ranListDTO = ranListDTO;
	}
	private DecimalFormat df = new DecimalFormat("#.##");
	
	public DecimalFormat getDf() {
		return df;
	}

	public void setDf(DecimalFormat df) {
		this.df = df;
	}
	
	private OrderCartDTO cartDTO = new OrderCartDTO();
	private List<OrderCartDTO> cartDTOList = new ArrayList<OrderCartDTO>();
	
	
	
	public List<OrderCartDTO> getCartDTOList() {
		return cartDTOList;
	}

	public void setCartDTOList(List<OrderCartDTO> cartDTOList) {
		this.cartDTOList = cartDTOList;
	}

	public OrderCartDTO getCartDTO() {
		return cartDTO;
	}

	public void setCartDTO(OrderCartDTO cartDTO) {
		this.cartDTO = cartDTO;
	}

	// 跳转到购物车
	public String toCart() {
		this.cateList = basicManager.queryAllCate();
		this.catenumList = basicManager.cateCount();
		this.subcatenumList = basicManager.subcateCount();
		if (this.session.get("userinfosession") != null) {
			this.boms = bomManager.loadAll();
			this.collects=fgoodsManager.findAll(null);
		}
		Long uid = null;
		if (this.session.get("userinfosession") != null) {
			CtUser cu = (CtUser) this.session.get("userinfosession");
			uid = cu.getUId();
			
			List<ViewCartList> cartLists = initViewCart(uid);
			
			
			init:
			for (int i = 0; i < cartLists.size(); i++) {
				System.out.println("i:"+i);
				cartDTO = new OrderCartDTO();
				rangePricesList = basicManager.getAllRanPrice(Long.valueOf(cartLists.get(i).getGId()));
				cartDTO.setPrices(rangePricesList);
				for (int j = 0; j < viewCart.size(); j++) {
					System.out.println("j:"+j);
					if(cartLists.get(i).getGId().equals(viewCart.get(j).getGId())){
						cartDTO.setCartGName(viewCart.get(j).getGName());
						cartDTO.setCartGoodsImgUrl(viewCart.get(j).getPUrl());
						cartDTO.setCartGId(Integer.valueOf(viewCart.get(j).getGId()));
						cartDTO.setIsSample(Integer.valueOf(viewCart.get(j).getSample()));
						try {
							if(viewCart.get(j).getCarttype().equals("1")){
								cartDTO.setCartSimNum(viewCart.get(j).getGNumber());
								cartDTO.setCartSimPrice(viewCart.get(j).getGPrice());
								for (int j2 = 0; j2 < rangePricesList.size(); j2++) {
									System.out.println("j2:"+j2);
									if((Integer.valueOf(viewCart.get(j).getGNumber()) >= rangePricesList.get(j2).getSimSNum()) && (Integer.valueOf(viewCart.get(j).getGNumber()) <= rangePricesList.get(j2).getSimENum())){
										cartDTO.setCartGoodsSelNum(j2);
									}
								}
							} else {
								cartDTO.setCartParNum((viewCart.get(j).getParnumber() / 1000)+"");
								cartDTO.setCartParPrice(viewCart.get(j).getParprice());
							}
						} catch (Exception e) {
							e.printStackTrace();
							//viewCart.remove(j);
							orderManager.delCartGoods(Long.valueOf(viewCart.get(j).getUId()), viewCart.get(j).getCartId());
							cartLists = initViewCart(uid);
							cartDTOList = new ArrayList<OrderCartDTO>();
							i--;
							continue init;
						}
						
					}
				}
				cartDTOList.add(cartDTO);
			}
			String allPrice = "0.00";
			for (int i = 0; i < cartDTOList.size(); i++) {
				cartDTOList.get(i).setCartCountNum((Integer.valueOf(cartDTOList.get(i).getCartSimNum()) + Integer.valueOf(cartDTOList.get(i).getCartParNum().equals("0") ? "0" : (Integer.valueOf(cartDTOList.get(i).getCartParNum()) * 1000)+""))+"");
				BigDecimal b1=new BigDecimal(cartDTOList.get(i).getCartSimPrice());  
		        BigDecimal b2=new BigDecimal(cartDTOList.get(i).getCartParPrice());
		        cartDTOList.get(i).setCartCountPrice(b1.add(b2)+"");
		        b1=new BigDecimal(allPrice);
		        b2=new BigDecimal(cartDTOList.get(i).getCartCountPrice());
		        allPrice = b1.add(b2) + "";
		        cartDTOList.get(i).setGoods(fgoodsManager.getGood(cartDTOList.get(i).getCartGId().longValue()));
			}
			this.request.put("allPrice", allPrice);
			this.request.put("allCartNum", cartDTOList.size());
			
			this.GgoodsNum=viewCart.size();
			cupon="";
			for (int i = 0; i < viewCart.size(); i++) {
				ctCoupons = fgoodsManager.getCouponByGidNew(Long.valueOf(viewCart.get(i).getGId()));
				ctCouponDetails = fgoodsManager.getCouponByGid(Long.valueOf(viewCart.get(i).getGId()));
				if (ctCoupons == null || ctCoupons.size() == 0) {
					cupon +=  "0|" + viewCart.get(i).getCartId() + ",";
				} else {
					for (int j = 0; j < ctCoupons.size(); j++) {
						coup.add(ctCoupons.get(j));
						cupon +=  ctCoupons.get(j).getCouponId()+"|" + viewCart.get(i).getCartId() + ",";
					}
				}
			}
			session.put("cupon", cupon);
			//记录是k单位的多还是pan区分购物车的显示方式
			int isK = 0;
			int isPan = 0;
			String gidtemp= "0";//临时存储gid
			for (int i = 0; i < viewCart.size(); i++) {
				if(viewCart.get(i) != null){
					String pr = this.basicManager.getPrice(uid,
							Long.parseLong(viewCart.get(i).getGId()));
					if (pr != null) {
						viewCart.get(i).setPromotePrice(
								new java.math.BigDecimal(pr));
					}
					if(viewCart.get(i).getCarttype().equals("0")){
						if(viewCart.get(i).getPack().equals("K")){
							isK++;
						} else if (viewCart.get(i).getPack().equals("Pan")){
							isPan++;
						}
					}
					if(!gidtemp.equals(viewCart.get(i).getGId())){
						rangePricesList = basicManager.getAllRanPrice(Long.valueOf(viewCart.get(i).getGId()));
						ranListDTO = new ArrayList<CtRangePriceDTO>();
						int cunsim = 0;
						for (int k = 0; k < rangePricesList.size(); k++) {
							CtRangePriceDTO rangePriceDTO = new CtRangePriceDTO();
							Double parsnum = Double.valueOf(rangePricesList.get(k).getParSNum() == null ? 0 : rangePricesList.get(k).getParSNum()) / 1000;
							Double parenum = Double.valueOf(rangePricesList.get(k).getParENum()==null ? 0 : rangePricesList.get(k).getParENum()) / 1000;
							Double parinc = Double.valueOf(rangePricesList.get(k).getParIncrease() == null ? 0 : rangePricesList.get(k).getParIncrease()) / 1000;
							rangePriceDTO.setGId(rangePricesList.get(k).getGId());
							rangePriceDTO.setRPid(rangePricesList.get(k).getRPid());
							rangePriceDTO.setSimSNum(rangePricesList.get(k).getSimSNum() == null ? 0 : rangePricesList.get(k).getSimSNum());
							rangePriceDTO.setSimENum(rangePricesList.get(k).getSimENum() == null ? 0 : rangePricesList.get(k).getSimENum());
							rangePriceDTO.setSimRPrice(rangePricesList.get(k).getSimRPrice() == null ? 0 : rangePricesList.get(k).getSimRPrice());
							rangePriceDTO.setSimIncrease(rangePricesList.get(k).getSimIncrease() == null ? 0 : rangePricesList.get(k).getSimIncrease());
							rangePriceDTO.setParENum(df.format(parenum.equals(0)? "" : parenum));
							rangePriceDTO.setParIncrease(df.format(parinc));
							rangePriceDTO.setParSNum(df.format(parsnum));
							rangePriceDTO.setParRPrice(rangePricesList.get(k).getParRPrice());
							rangePriceDTO.setInsTime(rangePricesList.get(k).getInsTime());
							ranListDTO.add(rangePriceDTO);
							if(rangePricesList.get(k).getSimSNum() != null){
								cunsim++;
							}
						}
						if(cunsim != 0){
							session.put("cunsim", "yes");
						} else {
							session.put("cunsim", "no");
						}
						gidtemp = viewCart.get(i).getGId();
						//System.out.println(viewCart.get(i).getGNumber());
						
						for (int k = 0; k < ranListDTO.size(); k++)  //外循环是循环的次数
				        {
				            for (int j = ranListDTO.size() - 1 ; j > k; j--)  //内循环是 外循环一次比较的次数
				            {
				            	if (ranListDTO.get(k).getRPid().toString().equals(ranListDTO.get(j).getRPid().toString()))
				            	{
				            		ranListDTO.remove(j);
				            	}

				            }
				        }
						
						
						priceAllList.add(ranListDTO);
					}
				}
			}
			for (int k = 0; k < priceAllList.size(); k++)  //外循环是循环的次数
	        {
	            for (int j = priceAllList.size() - 1 ; j > k; j--)  //内循环是 外循环一次比较的次数
	            {
	            	for (int j2 = 0; j2 < priceAllList.get(k).size(); j2++) {
	            		try {
							if (priceAllList.get(k).get(j2).getRPid().toString().equals(priceAllList.get(j).get(j2).getRPid().toString()))
							{
								priceAllList.remove(j);
								break;
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

	            }
	        }
			if(isK > isPan){
				for (int i = 0; i < viewCart.size(); i++){
					if(viewCart.get(i).getCarttype().equals("0")){
						if(viewCart.get(i).getPack().equals("Pan")){
							viewCart.get(i).setParnumber(viewCart.get(i).getParnumber()*rangePricesList.get(0).getParIncrease());
						}
					}
				}
			} else if (isK < isPan){
				for (int i = 0; i < viewCart.size(); i++){
					if(viewCart.get(i).getCarttype().equals("0")){
						if(viewCart.get(i).getPack().equals("K")){
							viewCart.get(i).setParnumber(viewCart.get(i).getParnumber()/rangePricesList.get(0).getParIncrease() );
						}
					}
				}
			} else {
				for (int i = 0; i < viewCart.size(); i++){
					if(viewCart.get(i).getCarttype().equals("0")){
						if(viewCart.get(i).getPack().equals("Pan")){
							viewCart.get(i).setParnumber(viewCart.get(i).getParnumber()*rangePricesList.get(0).getParIncrease());
						}
					}
				}
			}
		} else {
			return "nologin";
		}
		
		for (int i = 0; i < priceAllList.size(); i++)  //外循环是循环的次数
        {
            for (int j = priceAllList.size() - 1 ; j > i; j--)  //内循环是 外循环一次比较的次数
            {
            	if (priceAllList.get(i).equals(priceAllList.get(j)))
            	{
            		priceAllList.remove(j);
            	}

            }
        }
		for (int i = 0; i < coup.size(); i++)  //外循环是循环的次数
		{
			for (int j = coup.size() - 1 ; j > i; j--)  //内循环是 外循环一次比较的次数
			{
				
				if (coup.get(i).getCouponId().equals(coup.get(j).getCouponId()))
				{
					coup.remove(j);
				}
			}
		}
		Double parPriceAll = 0D;
		Double simPriceAll = 0D;
		//DecimalFormat dff = new DecimalFormat("0.00####");
		for (int i = 0; i < viewCart.size(); i++) {
			if(viewCart.get(i).getCarttype().equals("0")){
				viewCart.get(i).setParprice(df.format(Double.valueOf(viewCart.get(i).getParprice())));
//				if(viewCart.get(i).getPack().equals("Pan")){
//					Double price = Double.valueOf(viewCart.get(i).getParprice()) * rangePricesList.get(0).getParIncrease();
//					viewCart.get(i).setParprice(df.format(price));	
//				}
				parPriceAll += Double.valueOf(viewCart.get(i).getParprice());
				//System.out.println("parprice++++++++"+viewCart.get(i).getParprice());
				//System.out.println("parPriceAll++++++"+parPriceAll);
			} else if (viewCart.get(i).getCarttype().equals("1")){
				viewCart.get(i).setGPrice(df.format(Double.valueOf(viewCart.get(i).getGPrice())));
				simPriceAll += Double.valueOf(viewCart.get(i).getGPrice());
				//System.out.println("---------------price++++++++"+viewCart.get(i).getGId());
				//System.out.println("---------------price++++++++"+viewCart.get(i).getGPrice());
				//System.out.println("---------------PriceAll++++++"+simPriceAll);
			}
			
		}
		for (int i = 0; i < viewCart.size(); i++) {
			ViewCartListDTO dto = setViewCartDTO(df, i);
			viewCartDTO.add(dto);
		}
		//System.out.println(df.format(simPriceAll)+"======" + df.format(parPriceAll));
		this.request.put("simPriceAllStr", df.format(simPriceAll));
		this.request.put("parPriceAllStr", df.format(parPriceAll));
		Double priceAll = simPriceAll + parPriceAll;
		this.request.put("priceAllStr", df.format(priceAll));
		CtUser cu = (CtUser) this.session.get("userinfosession");
		Long cartcount = basicManager.getCartGoodsNum(cu.getUId());
		this.session.put("islgn", cartcount);
		return SUCCESS;
	}

	private List<ViewCartList> initViewCart(Long uid) {
		this.viewCart = orderManager.getCartByUId(uid);
		List<ViewCartList> cartLists = new ArrayList<ViewCartList>();
		for (int i = 0; i < viewCart.size(); i++) {
			cartLists.add(viewCart.get(i));
		}
		for (int k = 0; k < cartLists.size(); k++)  //外循环是循环的次数
		{
		    for (int j = cartLists.size() - 1 ; j > k; j--)  //内循环是 外循环一次比较的次数
		    {
		    	if (cartLists.get(k).getGId().toString().equals(cartLists.get(j).getGId().toString()))
		    	{
		    		cartLists.remove(j);
		    	}

		    }
		}
		return cartLists;
	}

	private ViewCartListDTO setViewCartDTO(DecimalFormat df, int i) {
		ViewCartListDTO dto = new ViewCartListDTO();
		dto.setCartId(viewCart.get(i).getCartId());
		dto.setCarttime(viewCart.get(i).getCarttime());
		dto.setCarttype(viewCart.get(i).getCarttype());
		dto.setCupon(viewCart.get(i).getCupon());
		dto.setGId(viewCart.get(i).getGId());
		dto.setGName(viewCart.get(i).getGName());
		dto.setGNumber(viewCart.get(i).getGNumber());
		dto.setGPrice(viewCart.get(i).getGPrice());
		dto.setGSn(viewCart.get(i).getGSn());
		dto.setGUnit(viewCart.get(i).getGUnit());
		dto.setMarketPrice(viewCart.get(i).getMarketPrice());
		dto.setPack(viewCart.get(i).getPack());
		dto.setPack1(viewCart.get(i).getPack1());
		dto.setPack1Num(viewCart.get(i).getPack1Num());
		dto.setPack2(viewCart.get(i).getPack2());
		dto.setPack2Num(viewCart.get(i).getPack2Num());
		dto.setPack3(viewCart.get(i).getPack3());
		dto.setPack3Num(viewCart.get(i).getPack3Num());
		dto.setPack4(viewCart.get(i).getPack4());
		dto.setPack4Num(viewCart.get(i).getPack4Num());
		
		Long parnum = viewCart.get(i).getParnumber();
		Double newparnum = Double.valueOf(parnum == null ? "0" : parnum.toString()) / 1000;
		dto.setParnumber(df.format(newparnum));
		
		dto.setParprice(viewCart.get(i).getParprice());
		dto.setPartial(viewCart.get(i).getPartial());
		dto.setPromotePrice(viewCart.get(i).getPromotePrice());
		dto.setPUrl(viewCart.get(i).getPUrl());
		dto.setSample(viewCart.get(i).getSample());
		dto.setShopPrice(viewCart.get(i).getShopPrice());
		dto.setUId(viewCart.get(i).getUId());
		return dto;
	}

	public String updateUtilPack(){
		CtUser cu = (CtUser) this.session.get("userinfosession");
		Long uid = cu.getUId();
		String str = orderDTO.getPack()+","+uid;
		String res=orderManager.updateCartUtil(str);
		if(res.equals("Exerror")){
			String url = orderDTO.getUrl();
			//System.out.println(url);
			String errorTotle = "抱歉，提交异常";
			String errorMess = "含非法字符";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			session.put("errorBackUrl", url);
			return ERROR;
		}
		return SUCCESS;
	}
	public String isPanOrK(){
		CtUser cu = (CtUser) this.session.get("userinfosession");
		Long uid = cu.getUId();
		String mess = orderManager.getIsPanOrK(uid);
		result = mess;
		return SUCCESS;
	}
	
	// 更新购物车
	public String updateCart() {
		String data = orderDTO.getData();
		if (data != null) {
			String[] dlist = data.split("==");
			String gid = dlist[0];
			Integer pack = Integer.parseInt(dlist[1]) + 1;
			Long num = Long.parseLong(dlist[2]);
			String price = dlist[3];
			Long uid = null;
			if (this.session.get("userinfosession") != null) {
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
				CtCart cart = orderManager.getCartByUIdGId(uid,
						Long.parseLong(gid));
				cart.setPack(pack.toString());
				cart.setGNumber(num);
				cart.setGPrice(price);
				String res=orderManager.updateCart(cart);
				if(res.equals("Exerror")){
					String url = orderDTO.getUrl();
					//System.out.println(url);
					String errorTotle = "抱歉，提交异常";
					String errorMess = "含非法字符";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", url);
					return ERROR;
				}
			} else {
				this.result = "error";
			}
		} else {
			this.result = "error";
		}
		return SUCCESS;
	}

	public String updateCartFotAjax(){
		if (this.session.get("userinfosession") != null) {
			CtUser cu = (CtUser) this.session.get("userinfosession");
			if(cartToNum != null){
				if(cartToNum.getCartType().equals("1")){
					//样品
					CtCart cart = orderManager.getCartByGidAndTypeId(cartToNum.getGId(),1,cu.getUId());
					if(cart == null){
						if(cartToNum.getGNumber() != 0){
							cart = new CtCart();
							cart.setCartAddTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
							cart.setCartType("1");
							cart.setGId(cartToNum.getGId());
							cart.setGNumber(cartToNum.getGNumber());
							cart.setGPrice(cartToNum.getGPrice());
							cart.setPack("K");
							cart.setUId(cu.getUId()+"");
							cart.setGParNumber(0L);
							cart.setGParPrice("0.00");
							orderManager.updateCart(cart);
						}
					} else {
						cart.setGNumber(cartToNum.getGNumber());
						cart.setGPrice(cartToNum.getGPrice());
						orderManager.updateCart(cart);
					}
				} else if(cartToNum.getCartType().equals("2")) {
					//批量
					CtCart cart = orderManager.getCartByGidAndTypeId(cartToNum.getGId(),0,cu.getUId());
					if(cart == null){
						if(cartToNum.getGParNumber() != 0){
							cart = new CtCart();
							cart.setCartAddTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
							cart.setCartType("0");
							cart.setGId(cartToNum.getGId());
							cart.setGNumber(0L);
							cart.setGPrice("0.00");
							cart.setPack("K");
							cart.setUId(cu.getUId()+"");
							cart.setGParNumber(cartToNum.getGParNumber());
							cart.setGParPrice(cartToNum.getGParPrice());
							orderManager.updateCart(cart);
						}
					} else {
						cart.setGParNumber(cartToNum.getGParNumber());
						cart.setGParPrice(cartToNum.getGParPrice());
						orderManager.updateCart(cart);
					}
				} else if(cartToNum.getCartType().equals("3")){
					//样品和批量
					List<CtCart> cartList = orderManager.getCartByUIdGIdPack(cu.getUId(), cartToNum.getGId(), "notPack");
					if(cartList.size() == 1){
						if(cartList.get(0).getCartType().equals("1") && cartToNum.getGParNumber() > 0){
							CtCart cart = new CtCart();
							cart.setCartAddTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
							cart.setCartType("0");
							cart.setGId(cartToNum.getGId());
							cart.setGNumber(0L);
							cart.setGPrice("0.00");
							cart.setPack("K");
							cart.setUId(cu.getUId()+"");
							cart.setGParNumber(cartToNum.getGParNumber());
							cart.setGParPrice(cartToNum.getGParPrice());
							orderManager.updateCart(cart);
						} else if (cartList.get(0).getCartType().equals("0") && cartToNum.getGNumber() > 0){
							CtCart cart = new CtCart();
							cart.setCartAddTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
							cart.setCartType("1");
							cart.setGId(cartToNum.getGId());
							cart.setGNumber(cartToNum.getGNumber());
							cart.setGPrice(cartToNum.getGPrice());
							cart.setPack("K");
							cart.setUId(cu.getUId()+"");
							cart.setGParNumber(0L);
							cart.setGParPrice("0.00");
							orderManager.updateCart(cart);
						} else {
							if(cartList.get(0).getCartType().equals("1")){
								cartList.get(0).setGNumber(cartToNum.getGNumber());
								cartList.get(0).setGPrice(cartToNum.getGPrice());
							} else if (cartList.get(0).getCartType().equals("0")){
								cartList.get(0).setGParNumber(cartToNum.getGParNumber());
								cartList.get(0).setGParPrice(cartToNum.getGParPrice());
							}
							orderManager.updateCart(cartList.get(0));
						}
					} else {
						for (int i = 0; i < cartList.size(); i++) {
							if(cartList.get(i).getCartType().equals("1")){
								cartList.get(i).setGNumber(cartToNum.getGNumber());
								cartList.get(i).setGPrice(cartToNum.getGPrice());
							} else if (cartList.get(i).getCartType().equals("0")){
								cartList.get(i).setGParNumber(cartToNum.getGParNumber());
								cartList.get(i).setGParPrice(cartToNum.getGParPrice());
							}
							orderManager.updateCart(cartList.get(i));
						}
						for (int i = 0; i < cartList.size(); i++) {
							if(cartList.get(i).getGNumber() == 0 && cartList.get(i).getGParNumber() == 0){
								orderManager.delCartGoods(cu.getUId(), cartList.get(i).getCartId());
							}
						}
					}
				} else if (cartToNum.getCartType().equals("4")){
					//删除购物车样品
					CtCart cart = orderManager.getCartByGidAndTypeId(cartToNum.getGId(),1,cu.getUId());
					if(cart != null){
						orderManager.delCartGoods(cu.getUId(), cart.getCartId());
					}
				} else if (cartToNum.getCartType().equals("5")){
					//删除购物车批量
					CtCart cart = orderManager.getCartByGidAndTypeId(cartToNum.getGId(),0,cu.getUId());
					if(cart != null){
						orderManager.delCartGoods(cu.getUId(), cart.getCartId());
					}
				}
			}
		}
		
//		if(cartToNum != null && cartToNum.getCartId() != null){
//			String res=orderManager.updateCartNum(cartToNum);
//			if(res.equals("Exerror")){
//				String url = orderDTO.getUrl();
//				//System.out.println(url);
//				String errorTotle = "抱歉，提交异常";
//				String errorMess = "含非法字符";
//				session.put("errorMess", errorMess);
//				session.put("errorTotle", errorTotle);
//				session.put("errorBackUrl", url);
//				return ERROR;
//			}
//			result = "success";
//			return SUCCESS;
//		}
		return SUCCESS;
	}
	private String cupon;
	
	public String getCupon() {
		return cupon;
	}

	public void setCupon(String cupon) {
		this.cupon = cupon;
	}
	private List<CtCoupon> ctCoupons = new ArrayList<CtCoupon>();
	
	public List<CtCoupon> getCtCoupons() {
		return ctCoupons;
	}

	public void setCtCoupons(List<CtCoupon> ctCoupons) {
		this.ctCoupons = ctCoupons;
	}

	// 添加购物车
	public String addCart() {
		DecimalFormat df = new DecimalFormat("0.00####");
		String data = orderDTO.getData();
		if (data != null) {
			String[] dlist = data.split("==");
			String gid = dlist[0];
			String pack = dlist[1];
			//String[] packTypes = pack.split("--");
			
			String num = dlist[2];
			String[] numsStr = num.split("--");
			
			String price = dlist[3];
			String[] priceStrs = price.split("--");
			priceStrs[0] = df.format(Double.valueOf(priceStrs[0]));
			priceStrs[1] = df.format(Double.valueOf(priceStrs[1]));
			
			Long uid = null;
			if (this.session.get("userinfosession") != null) {
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
				CtCart cart = new CtCart();
				cart.setGId(gid);
				cart.setUId(uid.toString());
				cart.setPack("K");
				cart.setGNumber(Long.valueOf(numsStr[0].equals("") ? "0" : numsStr[0]));
				cart.setGParNumber(Long.valueOf(numsStr[1]));
					Double simp = Double.valueOf(priceStrs[0]);
					Double parp = Double.valueOf(priceStrs[1]);
					
					cart.setGPrice(simp.toString());
					cart.setGParPrice(parp.toString());
				List<CtCart> caList = orderManager.getCartByUIdGIdPack(uid,
						gid, "notPack");
				cupon="";
				for (int i = 0; i < caList.size(); i++) {
					ctCoupons = fgoodsManager.getCouponByGidNew(Long.valueOf(caList.get(i).getGId()));
					ctCouponDetails = fgoodsManager.getCouponByGid(Long.valueOf(caList.get(i).getGId()));
					if (ctCoupons == null || ctCoupons.size() == 0) {
						cupon +=  "0|" + caList.get(i).getCartId() + ",";
					} else {
						for (int j = 0; j < ctCoupons.size(); j++) {
							cupon +=  ctCoupons.get(j).getCouponId()+"|" + caList.get(i).getCartId() + ",";
						}
					}
				}
				
				session.put("cupon", cupon);
				int parCount = 0;
				int simCount = 0;
				for (int i = 0; i < caList.size(); i++) {
					
					if(caList.get(i).getCartType() != null && caList.get(i).getCartType().equals("0")){
						parCount++;
					}
					if(caList.get(i).getCartType() != null && caList.get(i).getCartType().equals("1")){
						simCount++;
					}
				}
				if (parCount == 0 && simCount == 0) {
					addcartManager(pack, cart);
				} else if (parCount == 0 && simCount != 0 || (parCount != 0 && simCount == 0)){
					boolean isAdd = false;
					if(caList.get(0).getCartType().equals("0")){
						if(pack.equals("1--1")){
							pack = "1--0";
							addcartManager(pack, cart);
							caList = orderManager.getCartByUIdGIdPack(uid,
									gid, "notPack");
//							isAdd = true;
						} else if(pack.equals("0--1")) {
							
						} else {
							addcartManager(pack, cart);
							caList = orderManager.getCartByUIdGIdPack(uid,
									gid, "notPack");
							for (int i = 0; i < caList.size(); i++) {
								CtGoods goods = fgoodsManager.getGood(Long.valueOf(gid));
								if(caList.get(i).getCartType().equals("0")){
									if(goods.getGNum() == (caList.get(i).getGParNumber()/1000) && cart.getGNumber() != 0){
										orderManager.delCartGoodsByUidAndGid(uid, Long.valueOf(gid));
										CtCart ctCart = new CtCart();
										ctCart.setGParNumber(caList.get(i).getGParNumber());
										ctCart.setGParPrice(caList.get(i).getGParPrice());
										ctCart.setCartType("0");
										ctCart.setCartAddTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
										ctCart.setGNumber(0L);
										ctCart.setGPrice("0.00");
										ctCart.setPack("K");
										ctCart.setGId(gid);
										ctCart.setUId(uid+"");
										orderManager.updateCart(ctCart);
									}
								}
							}
							isAdd = true;
						}
					} else if (caList.get(0).getCartType().equals("1")){
						if(pack.equals("1--1")){
//							cart.setGParNumber(cart.getGParNumber() / 1000);
							pack = "0--1";
							addcartManager(pack, cart);
							caList = orderManager.getCartByUIdGIdPack(uid,
									gid, "notPack");
//							isAdd = true;
						} else if (pack.equals("0--1")){
//							cart.setGParNumber(cart.getGParNumber() / 1000);
							addcartManager(pack, cart);
							caList = orderManager.getCartByUIdGIdPack(uid,
									gid, "notPack");
							isAdd = true;
						}
					}
					if(!isAdd){
						for (int i = 0; i < 1; i++) {
							try {
								if(caList != null && caList.size() > 0){
									Double guoQianPrice = 0D;
									Long getParNum = caList.get(i).getGParNumber()+Long.valueOf(numsStr[1]);
									getParNum = getParNum / 1000;
									Long getNum = caList.get(i).getGNumber()
											+ Long.valueOf(numsStr[0]);
									getcartid1(priceStrs, cart, caList);
									if(cart.getCartId() == null){
										cart = new CtCart();
										cart = caList.get(i);
										cart.setCartId(caList.get(i).getCartId());
									}
									List<CtRangePrice>  renList = basicManager.getAllRanPrice(Long.valueOf(caList.get(i).getGId()));
									int tempi = 0;
									for (int j = 0; j < renList.size(); j++) {
										if(renList.get(j).getParIncrease() != null){
											tempi = j;
										}
									}
									if(getNum >= (renList.get(tempi).getParIncrease())){
										guoQianPrice = Double.valueOf(getNum) / renList.get(tempi).getParIncrease();
										if(guoQianPrice.toString().indexOf(".")>0){
											String chai[] = guoQianPrice.toString().split("\\.");
											chai[1] = "0."+chai[1];
											Double temp = Double.valueOf(chai[1])*renList.get(tempi).getParIncrease();
											String[] temp1 = temp.toString().split("\\.");
											getNum = Long.valueOf(temp1[0].toString());
											chai[0] = (Integer.valueOf(chai[0]) * renList.get(tempi).getParIncrease() / 1000)+"";
											getParNum = Long.valueOf(chai[0]) + caList.get(i).getGParNumber();
										} else {
											getNum = 0L;
											getParNum = Long.valueOf(guoQianPrice.toString()) + caList.get(i).getGParNumber();
										}
									}
									cart = caList.get(i);
									cart.setGNumber(getNum);
									if(priceStrs.length == 4 && !priceStrs[3].equals("0") && !priceStrs[3].equals("0.00")){
										Double newSimPar = mul(Double.valueOf(getParNum), Double.valueOf(priceStrs[3]));
										cart.setGParPrice(newSimPar.toString());
									}
									if(priceStrs.length == 4 && !priceStrs[2].equals("0") && !priceStrs[2].equals("0.00")){
										Double newSimSim = mul(Double.valueOf(getNum), Double.valueOf(priceStrs[2]));
										cart.setGPrice(newSimSim.toString());
									}
									CtGoods goods = fgoodsManager.getGood(Long.valueOf(gid));
									if(getNum != 0 && getParNum != 0){
										Long allCount = getNum + getParNum*1000;
										Double simPriceAll = 0D;
										if(goods.getGNum() < allCount){
											//Double shengYu = div(allCount.toString(), renList.get(tempi).getParIncrease().toString(), 2);
											Long count = goods.getGNum();
											Long gnum = count*1000 - getParNum*1000;
											for (int j = 0; j < renList.size(); j++) {
												if(gnum>=renList.get(j).getSimSNum() && gnum <= renList.get(j).getSimENum()){
													getNum = gnum;
													simPriceAll = getNum * renList.get(j).getSimRPrice();
												}
											}
										}
										orderManager.delCartGoodsByUidAndGid(uid, Long.valueOf(gid));
										CtCart ctCart = new CtCart();
										ctCart.setGParNumber(getParNum*1000);
										ctCart.setGParPrice((getParNum * 1000 * renList.get(tempi).getParRPrice())+"");
										ctCart.setCartType("0");
										ctCart.setCartAddTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
										ctCart.setGNumber(0L);
										ctCart.setGPrice("0.00");
										ctCart.setPack("K");
										ctCart.setGId(gid);
										ctCart.setUId(uid+"");
										orderManager.updateCart(ctCart);
										CtCart ctCart1 = new CtCart();
										ctCart1.setGParNumber(0L);
										ctCart1.setGParPrice("0.00");
										ctCart1.setCartType("1");
										ctCart1.setCartAddTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
										ctCart1.setGNumber(getNum);
										ctCart1.setGPrice(simPriceAll.toString());
										ctCart1.setPack("K");
										ctCart1.setGId(gid);
										ctCart1.setUId(uid+"");
										orderManager.updateCart(ctCart1);
										Long cartcount = basicManager.getCartGoodsNum(uid);
										this.session.put("islgn", cartcount);
										result = "success";
										
										//计算库存
										if(goods.getGNum() < getParNum){
											orderManager.delCartGoodsByUidAndGid(uid, Long.valueOf(gid));
											ctCart = new CtCart();
											ctCart.setGParNumber(goods.getGNum()*1000);
											ctCart.setGParPrice((goods.getGNum() * 1000 * renList.get(tempi).getParRPrice())+"");
											ctCart.setCartType("0");
											ctCart.setCartAddTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
											ctCart.setGNumber(0L);
											ctCart.setGPrice("0.00");
											ctCart.setPack("K");
											ctCart.setGId(gid);
											ctCart.setUId(uid+"");
											orderManager.updateCart(ctCart);
										}
										if(goods.getGNum() == getParNum && getNum != 0){
											orderManager.delCartGoodsByUidAndGid(uid, Long.valueOf(gid));
											ctCart = new CtCart();
											ctCart.setGParNumber(goods.getGNum()*1000);
											ctCart.setGParPrice((goods.getGNum() * 1000 * renList.get(tempi).getParRPrice())+"");
											ctCart.setCartType("0");
											ctCart.setCartAddTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
											ctCart.setGNumber(0L);
											ctCart.setGPrice("0.00");
											ctCart.setPack("K");
											ctCart.setGId(gid);
											ctCart.setUId(uid+"");
											orderManager.updateCart(ctCart);
										}
										return SUCCESS;
									}
									if(getNum == 0){
										CtCart ctCart = orderManager.getCartByCartId(cart.getCartId());
										if(ctCart != null){
											orderManager.delCartGoods(uid, ctCart.getCartId());
										}
										cart = new CtCart();
										cart.setGParNumber(getParNum *1000);
										cart.setGParPrice((getParNum *1000 * renList.get(tempi).getParRPrice())+"");
										cart.setCartType("0");
										cart.setCartAddTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
										cart.setGNumber(0L);
										cart.setGPrice("0.00");
										cart.setPack("K");
										cart.setGId(gid);
										cart.setUId(uid+"");
										orderManager.updateCart(cart);
										Long cartcount = basicManager.getCartGoodsNum(uid);
										this.session.put("islgn", cartcount);
										result = "success";
										CtGoods goods1 = fgoodsManager.getGood(Long.valueOf(gid));
										//计算库存
										if(goods1.getGNum() < getParNum){
											orderManager.delCartGoodsByUidAndGid(uid, Long.valueOf(gid));
											ctCart = new CtCart();
											ctCart.setGParNumber(goods1.getGNum()*1000);
											ctCart.setGParPrice((goods1.getGNum() * 1000 * renList.get(tempi).getParRPrice())+"");
											ctCart.setCartType("0");
											ctCart.setCartAddTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
											ctCart.setGNumber(0L);
											ctCart.setGPrice("0.00");
											ctCart.setPack("K");
											ctCart.setGId(gid);
											ctCart.setUId(uid+"");
											orderManager.updateCart(ctCart);
										}
										return SUCCESS;
									}
								}
								if(caList.get(i).getGParNumber() != null && caList.get(i).getCartType().equals("0")){
									Long getNum1 = caList.get(i).getGParNumber()
											+ Long.valueOf(numsStr[1]);
									cart.setGParNumber(getNum1);
									if(priceStrs.length == 4 && !priceStrs[3].equals("0") && !priceStrs[3].equals("0.00")){
										Double newSimPar = Double.valueOf(getNum1) * (Double.valueOf(priceStrs[3]));
										cart.setGParPrice(df.format(newSimPar));
									}
								}
								
								//cart.setCartId(caList.get(i).getCartId());
								String res=orderManager.updateCartNum(cart);
								if(res.equals("Exerror")){
									String url = orderDTO.getUrl();
									//System.out.println(url);
									String errorTotle = "抱歉，提交异常";
									String errorMess = "含非法字符";
									session.put("errorMess", errorMess);
									session.put("errorTotle", errorTotle);
									session.put("errorBackUrl", url);
									return ERROR;
								}
								Long cartcount = basicManager.getCartGoodsNum(uid);
								this.session.put("islgn", cartcount);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				} else {
					for (int i = 0; i < 2; i++) {
						try {
							List<CtRangePrice>  renList = basicManager.getAllRanPrice(Long.valueOf(caList.get(i).getGId()));
							if(caList != null && caList.size() > 0){
								Double guoQianPrice = 0D;
								Long getParNum = 0L;
								if(caList.get(0).getCartType().equals("0")){
									getParNum = caList.get(0).getGParNumber()+Long.valueOf(numsStr[1]);
								}
								if(caList.get(1).getCartType().equals("0")){
									getParNum = caList.get(1).getGParNumber()+Long.valueOf(numsStr[1]);
								}
								//getParNum = getParNum / 1000;
								int isNotLing = 0;
								if(caList.get(0).getGNumber().equals(0L)){
									isNotLing = 1;
								}
								getcartid(priceStrs, cart, caList);
								if(cart.getCartId() == null){
									cart = new CtCart();
									cart = caList.get(i);
									cart.setCartId(caList.get(i).getCartId());
								}
									Long getNum = cart.getGNumber()
											+ Long.valueOf(numsStr[0]);
									if(!caList.get(0).getCartType().equals("0")){
										getNum = caList.get(0).getGNumber()+Long.valueOf(numsStr[0]);
									}
									if(!caList.get(1).getCartType().equals("0")){
										getNum = caList.get(1).getGNumber()+Long.valueOf(numsStr[0]);
									}
									int tempi = 0;
									for (int j = 0; j < renList.size(); j++) {
										if(renList.get(j).getParIncrease() != null){
											tempi = j;
										}
									}
									if(getNum >= (renList.get(tempi).getParIncrease())){
										guoQianPrice = Double.valueOf(getNum)/1000;
										if(guoQianPrice.toString().indexOf(".")>0){
											String chai[] = guoQianPrice.toString().split("\\.");
											chai[1] = "0."+chai[1];
											Double temp = Double.valueOf(chai[1])*1000;
											String[] temp1 = temp.toString().split("\\.");
											getNum = Long.valueOf(temp1[0].toString());
											getParNum = Long.valueOf(chai[0])*1000 + caList.get(i).getGParNumber();
										} else {
											getNum = 0L;
											getParNum = Long.valueOf(guoQianPrice.toString()) + caList.get(i).getGParNumber();
										}
									}
									if(getNum == 0){
										CtCart ctCart = orderManager.getCartByCartId(cart.getCartId());
										if(ctCart != null){
											orderManager.delCartGoodsByUidAndGid(uid, Long.valueOf(gid));
										}
										cart = new CtCart();
										cart.setGParNumber(getParNum);
										cart.setGParPrice((getParNum * renList.get(tempi).getParRPrice())+"");
										cart.setCartType("0");
										cart.setCartAddTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
										cart.setGNumber(0L);
										cart.setGPrice("0.00");
										cart.setPack("K");
										cart.setGId(gid);
										cart.setUId(uid+"");
										orderManager.updateCart(cart);
										Long cartcount = basicManager.getCartGoodsNum(uid);
										this.session.put("islgn", cartcount);
										result = "success";
										CtGoods goods = fgoodsManager.getGood(Long.valueOf(gid));
										//计算库存
										if(goods.getGNum() < (getParNum/1000)){
											orderManager.delCartGoodsByUidAndGid(uid, Long.valueOf(gid));
											ctCart = new CtCart();
											ctCart.setGParNumber(goods.getGNum()*1000);
											ctCart.setGParPrice((goods.getGNum() * 1000 * renList.get(tempi).getParRPrice())+"");
											ctCart.setCartType("0");
											ctCart.setCartAddTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
											ctCart.setGNumber(0L);
											ctCart.setGPrice("0.00");
											ctCart.setPack("K");
											ctCart.setGId(gid);
											ctCart.setUId(uid+"");
											orderManager.updateCart(ctCart);
										}
//										return SUCCESS;
									}
									if(getNum != 0 && getParNum != 0){
										CtGoods goods = fgoodsManager.getGood(Long.valueOf(gid));
										Long allCount = getNum + getParNum;
										Double simPriceAll = 0D;
										if(goods.getGNum() < allCount){
											//Double shengYu = div(allCount.toString(), renList.get(tempi).getParIncrease().toString(), 2);
											Long count = goods.getGNum()*1000;
											Long gnum = count - getParNum;
											for (int j = 0; j < renList.size(); j++) {
												if(gnum>=renList.get(j).getSimSNum() && gnum <= renList.get(j).getSimENum()){
													getNum = gnum;
													simPriceAll = getNum * renList.get(j).getSimRPrice();
												}
											}
										}
										orderManager.delCartGoodsByUidAndGid(uid, Long.valueOf(gid));
										CtCart ctCart = new CtCart();
										ctCart.setGParNumber(getParNum);
										ctCart.setGParPrice((getParNum * renList.get(tempi).getParRPrice())+"");
										ctCart.setCartType("0");
										ctCart.setCartAddTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
										ctCart.setGNumber(0L);
										ctCart.setGPrice("0.00");
										ctCart.setPack("K");
										ctCart.setGId(gid);
										ctCart.setUId(uid+"");
										orderManager.updateCart(ctCart);
										CtCart ctCart1 = new CtCart();
										ctCart1.setGParNumber(0L);
										ctCart1.setGParPrice("0.00");
										ctCart1.setCartType("1");
										ctCart1.setCartAddTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
										ctCart1.setGNumber(getNum);
										ctCart1.setGPrice(simPriceAll.toString());
										ctCart1.setPack("K");
										ctCart1.setGId(gid);
										ctCart1.setUId(uid+"");
										orderManager.updateCart(ctCart1);
										Long cartcount = basicManager.getCartGoodsNum(uid);
										this.session.put("islgn", cartcount);
										result = "success";
										
										//计算库存
										if(goods.getGNum() < (getParNum/1000)){
											orderManager.delCartGoodsByUidAndGid(uid, Long.valueOf(gid));
											ctCart = new CtCart();
											ctCart.setGParNumber(goods.getGNum()*1000);
											ctCart.setGParPrice((goods.getGNum() * 1000 * renList.get(tempi).getParRPrice())+"");
											ctCart.setCartType("0");
											ctCart.setCartAddTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
											ctCart.setGNumber(0L);
											ctCart.setGPrice("0.00");
											ctCart.setPack("K");
											ctCart.setGId(gid);
											ctCart.setUId(uid+"");
											orderManager.updateCart(ctCart);
										}
										if(goods.getGNum() == (getParNum/1000) && getNum != 0){
											orderManager.delCartGoodsByUidAndGid(uid, Long.valueOf(gid));
											ctCart = new CtCart();
											ctCart.setGParNumber(goods.getGNum()*1000);
											ctCart.setGParPrice((goods.getGNum() * 1000 * renList.get(tempi).getParRPrice())+"");
											ctCart.setCartType("0");
											ctCart.setCartAddTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
											ctCart.setGNumber(0L);
											ctCart.setGPrice("0.00");
											ctCart.setPack("K");
											ctCart.setGId(gid);
											ctCart.setUId(uid+"");
											orderManager.updateCart(ctCart);
										}
										return SUCCESS;
									}
									cart.setGNumber(getNum);
//									if(priceStrs.length == 4 && !priceStrs[3].equals("0") && !priceStrs[3].equals("0.00")){
//										Double newSimPar = Double.valueOf(getParNum) * Double.valueOf(priceStrs[3]);
//										cart.setGParPrice(newSimPar.toString());
//									}
									if(priceStrs.length == 4 && !priceStrs[2].equals("0") && !priceStrs[2].equals("0.00")){
										Double newSimSim = Double.valueOf(getNum) * Double.valueOf(priceStrs[2]);
										cart.setGPrice(newSimSim.toString());
									}
									//cart.setGParNumber(getParNum);
//									getcartid(priceStrs, cart, caList);
//									if(cart.getCartId() == null){
//										cart = caList.get(i);
//									}
									if(caList.get(i).getGParNumber() != null && caList.get(i).getGParNumber()!=0L){
										Long getNum1 = cart.getGParNumber() 
												+ Long.valueOf(numsStr[1]); 
										if(priceStrs.length == 4 && !priceStrs[3].equals("0") && !priceStrs[3].equals("0.00")){
											Double newSimPar = Double.valueOf(getNum1) * (Double.valueOf(priceStrs[3]));
											cart.setGParPrice(df.format(newSimPar).toString());
										}
										cart.setGParNumber(getNum1);
									}
									
								//cart.setCartId(caList.get(i).getCartId());
								String res=orderManager.updateCartNum(cart);
								if(res.equals("Exerror")){
									String url = orderDTO.getUrl();
									//System.out.println(url);
									String errorTotle = "抱歉，提交异常";
									String errorMess = "含非法字符";
									session.put("errorMess", errorMess);
									session.put("errorTotle", errorTotle);
									session.put("errorBackUrl", url);
									return ERROR;
								}
							}
							Long cartcount = basicManager.getCartGoodsNum(uid);
							this.session.put("islgn", cartcount);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			} else {
				result = "error";
				return "error";
			}
		} else {
			result = "error";
			return "error";
		}
		CtUser cu = (CtUser) this.session.get("userinfosession");
		Long cartcount = basicManager.getCartGoodsNum(cu.getUId());
		this.session.put("islgn", cartcount);
		result = "success";
		return SUCCESS;
	}

	private void getcartid(String[] priceStrs, CtCart cart, List<CtCart> caList) {
		if(priceStrs[3].equals("0")){
			for (int j = 0; j < caList.size(); j++) {
				if(caList.get(j).getCartType().equals("1")){
					cart.setCartId(caList.get(j).getCartId());
					cart.setGNumber(caList.get(j).getGNumber());
				}
			}
		} else if(priceStrs[2].equals("0")) {
			for (int j = 0; j < caList.size(); j++) {
				if(caList.get(j).getCartType().equals("0")){
					cart.setCartId(caList.get(j).getCartId());
					cart.setGParNumber(caList.get(j).getGParNumber());
				}
			}
		} else {
			for (int j = 0; j < caList.size(); j++) {
				if(caList.get(j).getCartType().equals("1")){
					cart.setGNumber(caList.get(j).getGNumber());
				}
				if(caList.get(j).getCartType().equals("2")){
					cart.setGParNumber(caList.get(j).getGParNumber());
				}
			}
			cart.setCartId(null);
		}
	}
	private void getcartid1(String[] priceStrs, CtCart cart, List<CtCart> caList) {
		if(priceStrs[3].equals("0")){
			for (int j = 0; j < caList.size(); j++) {
				if(caList.get(j).getCartType().equals("1")){
					cart.setCartId(caList.get(j).getCartId());
					//cart.setGNumber(caList.get(j).getGNumber());
				}
			}
		} else if(priceStrs[2].equals("0")) {
			for (int j = 0; j < caList.size(); j++) {
				if(caList.get(j).getCartType().equals("0")){
					cart.setCartId(caList.get(j).getCartId());
					//cart.setGParNumber(caList.get(j).getGParNumber());
				}
			}
		} else {
			for (int j = 0; j < caList.size(); j++) {
//				if(caList.get(j).getCartType().equals("1")){
//					cart.setGNumber(caList.get(j).getGNumber());
//				}
//				if(caList.get(j).getCartType().equals("2")){
//					cart.setGParNumber(caList.get(j).getGParNumber());
//				}
			}
			cart.setCartId(null);
		}
	}

	private String addcartManager(String pack, CtCart cart) {
		if(pack.equals("1--1")){
			cart.setCartType("0");
			String res=orderManager.addCart(cart);
			if(res.equals("Exerror")){
				String url = orderDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
			}
			cart.setCartType("1");
			String re=orderManager.addCart(cart);
			if(re.equals("Exerror")){
				String url = orderDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
			}
		} else if (pack.equals("1--0")) {
			cart.setCartType("1");
			String r=orderManager.addCart(cart);
			if(r.equals("Exerror")){
				String url = orderDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
			}
		} else if (pack.equals("0--1")){
			cart.setCartType("0");
			String ress=orderManager.addCart(cart);
			if(ress.equals("Exerror")){
				String url = orderDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
			}
		}
		return "success";
	}

	// 异步加入购物车
	public String addCartASYN() {
		try {
			String data = orderDTO.getData();
			if (data != null) {
				String[] dlist = data.split("==");
				String gid = dlist[0];
				String pack = dlist[1];
				Long num = Long.parseLong(dlist[2]);
				String price = dlist[3];
				Long uid = null;
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
				CtCart cart = new CtCart();
				cart.setGId(gid);
				cart.setUId(uid.toString());
				cart.setPack(pack);
				cart.setGNumber(num);
				cart.setGPrice(price);
				String res=orderManager.addCart(cart);
				if(res.equals("Exerror")){
					String url = orderDTO.getUrl();
					//System.out.println(url);
					String errorTotle = "抱歉，提交异常";
					String errorMess = "含非法字符";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", url);
					return ERROR;
				}
			}
			this.result = "addsuccess";
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			this.result = "error";
			return SUCCESS;
		}

	}

	// 删除购物车商品
	public String delCartGoods() {
		Long uid = null;
		if (this.session.get("userinfosession") != null) {
			CtUser cu = (CtUser) this.session.get("userinfosession");
			uid = cu.getUId();
			Long ctid = orderDTO.getCtid();
			String ctids = orderDTO.getCtids();
			if (ctids != null) {
				String[] g = ctids.split(",");
				for (int i = 1; i < g.length; i++) {
					orderManager.delCartGoods(uid, Long.parseLong(g[i]));
				}
				Long cartcount = basicManager.getCartGoodsNum(uid);
				this.session.put("islgn", cartcount);
			} else {
				orderManager.delCartGoodsByUidAndGid(uid, ctid);
				Long cartcount = basicManager.getCartGoodsNum(uid);
				this.session.put("islgn", cartcount);
			}
		} else {
			return "error";
		}
		return SUCCESS;
	}
	public String delCartGoodsForAjax() {
		Long uid = null;
		if (this.session.get("userinfosession") != null) {
			CtUser cu = (CtUser) this.session.get("userinfosession");
			uid = cu.getUId();
			Long ctid = orderDTO.getCtid();
			String ctids = orderDTO.getCtids();
			if (ctids != null) {
				String[] g = ctids.split(",");
				for (int i = 1; i < g.length; i++) {
					orderManager.delCartGoodsByUidAndGid(uid, Long.parseLong(g[i]));
//					orderManager.delCartGoods(uid, Long.parseLong(g[i]));
				}
				Long cartcount = basicManager.getCartGoodsNum(uid);
				this.session.put("islgn", cartcount);
			} else {
				try {
					orderManager.delCartGoodsByUidAndGid(uid, ctid);
					Long cartcount = basicManager.getCartGoodsNum(uid);
					this.session.put("islgn", cartcount);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else {
			return "error";
		}
		result = "success";
		return SUCCESS;
	}

	// 清空购物车
	public String delCart() {
		Long uid = null;
		if (this.session.get("userinfosession") != null) {
			CtUser cu = (CtUser) this.session.get("userinfosession");
			uid = cu.getUId();
			orderManager.delCart(uid);
			Long cartcount = basicManager.getCartGoodsNum(uid);
			this.session.put("islgn", cartcount);
		} else {
			
			return "error";
		}
		
		return SUCCESS;
	}
	private CtShop shop = new CtShop();
	private List<CtShop> shopList = new ArrayList<CtShop>();
	
	public CtShop getShop() {
		return shop;
	}

	public void setShop(CtShop shop) {
		this.shop = shop;
	}

	public List<CtShop> getShopList() {
		return shopList;
	}

	public void setShopList(List<CtShop> shopList) {
		this.shopList = shopList;
	}
	private List<CtCouponDetail> ccdListTypeNate = new ArrayList<CtCouponDetail>();
	
	public List<CtCouponDetail> getCcdListTypeNate() {
		return ccdListTypeNate;
	}

	public void setCcdListTypeNate(List<CtCouponDetail> ccdListTypeNate) {
		this.ccdListTypeNate = ccdListTypeNate;
	}

	private String gidss = "";
	
	// 跳转确认订单
	public String orderCheck() {
		this.cateList = basicManager.queryAllCate();
		this.catenumList = basicManager.cateCount();
		this.subcatenumList = basicManager.subcateCount();
		this.couponDetails = coupondetailManager.getAllNotUse();
		DecimalFormat df = new DecimalFormat("0.00####");
		boolean ison = true;
		int isyN =0;
		boolean isfirst=false;//判断用户是否首单
		boolean ismin=true;// 判断是否支持样品券（最小批量商品）
		try {
			Long uid = null;
			String gids = null;
			Double samTotal=(double) 0;
			String gidsTest = (String) request.get("gids");
			if(gidsTest == null || gidsTest.length()==0){
				if(orderDTO.getGids()==null || orderDTO.getGids().equals("")){
					String gidStr = orderDTO.getPack();
					if(gidStr == null || gidStr.equals("")){
						gidss = (String) session.get("gidss");
						gidStr = gidss;
					}
					gidss = gidStr;
//					orderGoodslist = orderManager.getOrderGoodsByOrderId(Long.valueOf(gidStr));
//					session.put("gidss", gidss);
//					session.put("orderGoodslist", orderGoodslist);
				}
			}
			
			
			
			
			if (this.session.get("userinfosession") != null) {
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
				
				//判断用户是否首单
				//if (orderManager.getSomeOrderListByUId(uid).size()==0){
					//isfirst = true;   //取消首单免运费
				//}
				isfirst = false;
				
				
				
				
				// 取收货地址
				
				
				
				this.addressList = orderManager.getUserAddress(uid);
				if (addressList != null) {
					for (int c = 0; c < this.addressList.size(); c++) {
						if (this.addressList.get(c) != null) {
							Integer def = 0;
							ViewUserAddress view = (ViewUserAddress)this.addressList.get(c);
							if (view.getIsdefault() != null) {
								def = Integer.parseInt(view
										.getIsdefault().trim());
							}
							if (def == 1) {
								String address = view
										.getCountry()
										+ view.getProvince()
										+ view.getCity()
										+ view.getDistrict()
										+ view.getAddress();
								String name = view
										.getAConsignee();
								String mp="";
								if (view.getTel() !=null && !view.getTel().equals("")){
									mp = view.getTel();	
								}else{
									mp = view.getMb();	
								}
								
								this.request.put("addid", view
										.getAId());
								this.request.put("address", address);
								this.request.put("name", name);
								this.request.put("mp", mp);
							}
						}
					}
					int add = addressList.size();
					this.request.put("addsize", add);
				}
				// 取商品列表
				gids = orderDTO.getGids();
				if (gids != null) {
					request.put("gids", gids); 
				}
				int gtypeCartOrAg = 0;
				Object gidAll = request.get("gids");
				if(gidAll != null){
					session.remove("gg");
					session.put("gg", gidAll);
				} else {
					gidAll = session.get("gg");
					request.put("gids", gidAll); 
				}
				if(gidAll == null || gidAll.equals("")){
					for (int i = 0; i < orderGoodslist.size(); i++) {
						ViewOrderCheck voc = new ViewOrderCheck();
						List<CtRangePrice>  renList = basicManager.getAllRanPrice(orderGoodslist.get(i).getGId());
						List<CtCouponDetail> coupons = this.coupondetailManager.getCtCouponsByGid(uid,Long.valueOf(orderGoodslist.get(i).getGId().toString()));
						quchongCoupons(coupons);
						//再次购买
						gtypeCartOrAg = 0;
						voc.setGId(orderGoodslist.get(i).getGId().toString());
						voc.setGName(orderGoodslist.get(i).getCtGoods().getGName());
						if(orderGoodslist.get(i).getIsParOrSim().equals("1")){
							for (int j = 0; j < renList.size(); j++) {
								if(renList.get(j).getSimSNum() != null && (Integer.valueOf(orderGoodslist.get(i).getGNumber()) >= Integer.valueOf(renList.get(j).getSimSNum()) && Integer.valueOf(orderGoodslist.get(i).getGNumber()) <= Integer.valueOf(renList.get(j).getSimENum()))){
									System.out.println(j);
									Double price = Double.valueOf(renList.get(j).getSimRPrice()) * Integer.valueOf(orderGoodslist.get(i).getGNumber());
									//price = new DecimalFormat("0.00####").format(Double.valueOf(price));
									voc.setGPrice(price.toString());
								}
							}
							voc.setGNumber(orderGoodslist.get(i).getGNumber());
							voc.setSample(orderGoodslist.get(i).getCtGoods().getIsSample());
							voc.setPartial(orderGoodslist.get(i).getCtGoods().getIsPartial());
							
							
							voc.setCarttype("1");
						} else if (orderGoodslist.get(i).getIsParOrSim().equals("0")){
							Long num = orderGoodslist.get(i).getGParNumber();
							for (int j = 0; j < renList.size(); j++) {
								System.out.println(renList.get(j).getParSNum());
								System.out.println(orderGoodslist.get(i).getGParNumber());
								System.out.println(orderGoodslist.get(i).getGParNumber());
								System.out.println(renList.get(j).getParENum());
								if(renList.get(j).getParSNum() != null && (Integer.valueOf(orderGoodslist.get(i).getGParNumber().toString()) >= Integer.valueOf(renList.get(j).getParSNum()) && Integer.valueOf(orderGoodslist.get(i).getGParNumber().toString()) <= Integer.valueOf(renList.get(j).getParENum() == null ? 99999999 : renList.get(j).getParENum()))){
									Double price = mul(renList.get(j).getParRPrice(),Double.valueOf(orderGoodslist.get(i).getGParNumber()));
									//price = new DecimalFormat("0.00####").format(Double.valueOf(price));
									voc.setParprice(price.toString());
								}
							}
							String par = orderGoodslist.get(i).getGParPrice();
							Double price = Double.valueOf(par) * Double.valueOf(num);
							String temp = "";
							temp = new DecimalFormat("0.00####").format(price);
							voc.setParnumber(orderGoodslist.get(i).getGParNumber());
							voc.setCarttype("0");
						}
						voc.setGUnit(orderGoodslist.get(i).getCtGoods().getGUnit());
						voc.setPack(orderGoodslist.get(i).getPack());
						if(orderGoodslist.get(i).getCtGoods().getPromotePrice() != null){
							voc.setPromotePrice(new java.math.BigDecimal(orderGoodslist.get(i).getCtGoods().getPromotePrice()));
						} else {
							voc.setPromotePrice(new java.math.BigDecimal(9999));
						}
						voc.setPUrl("nourl");
						voc.setShopPrice(new java.math.BigDecimal(orderGoodslist.get(i).getCtGoods().getShopPrice()));
						voc.setUId(uid.toString());
						this.ocList.add(voc);
						//this.couponsAll.add(coupons);
					}
					List<CtCouponDetail> coupons = this.coupondetailManager.getCtCouponsByGid(uid,Long.valueOf(0));
					quchongCoupons(coupons);
					this.couponsAll.add(coupons);
				} else {//购物车
					String[] gid = gidAll.toString().split(",");
					for (int i = 1; i < gid.length; i++) {
						//CtCart ctCart = orderManager.getCartByCartId(Long.valueOf(gid[i].toString()));
						ViewOrderCheck voc = new ViewOrderCheck();
						
						List<ViewOrderCheck> vo = orderManager.getOrderCheck(uid,
								Long.parseLong(gid[i]),"GId");
						
						gtypeCartOrAg = 1;
						for (int j = 0; j < vo.size(); j++) {
							voc = vo.get(j);
							if(voc.getPack().equals("Pan")){
								Double priceDouble = Double.valueOf(voc.getParprice())/Double.valueOf(voc.getParnumber());
								List<CtRangePrice>  renList = basicManager.getAllRanPrice(Long.parseLong(gid[i]));
								if(renList == null || renList.size() == 0){
									voc.setParnumber(voc.getParnumber()*1);
									priceDouble = (priceDouble/1) * Double.valueOf(voc.getParnumber());
								} else {
									voc.setParnumber(voc.getParnumber()*renList.get(0).getParIncrease());
									priceDouble = (priceDouble/renList.get(0).getParIncrease()) * Double.valueOf(voc.getParnumber());
								}
								voc.setParprice(df.format(priceDouble).toString());
							}
							String pr = this.basicManager.getPrice(uid,
									Long.parseLong(gid[i]));
							if (pr != null) {
								voc.setPromotePrice(new java.math.BigDecimal(pr));
							}
							String parice = voc.getParprice();
							voc.setParprice(new DecimalFormat("0.00####").format(Double.valueOf(parice)));
							voc.setGPrice(new DecimalFormat("0.00####").format(Double.valueOf(voc.getGPrice())));
							this.ocList.add(voc);
						}
						
					}
					List<CtCouponDetail> coupons = this.coupondetailManager.getCtCouponsByGid(uid,Long.valueOf(0));
					quchongCoupons(coupons);
					this.couponsAll.add(coupons);
				}
				Double money = 0.0;
				samTotal=0.0;
				CtGoods good = new CtGoods();
				orderGoodslist = new ArrayList<CtOrderGoods>();
				orderInfoGoodsDTOList = new ArrayList<OrderInfoGoodsDTO>();
				newOrderCheckDTOData(ocList, gtypeCartOrAg);
				Double allPrice = 0D;
				for (int i = 0; i < orderInfoGoodsDTOList.size(); i++) {
					allPrice = add(orderInfoGoodsDTOList.get(i).getAllPrice(), allPrice.toString());
				}
				this.request.put("newAllGoodsPrice", allPrice);
				for (int j = 0; j < this.ocList.size(); j++) {
					if(ocList.get(j).getCarttype().equals("0")){//批量
						Long number = ocList.get(j).getParnumber();
						String price = ocList.get(j).getParprice();
						if(price.indexOf(".") == 0){
							price = "0" + price;
							ocList.get(j).setParprice(price);
						}
						Double danPrice = BigDecimal.valueOf(Double.valueOf(price)).divide(BigDecimal.valueOf(Double.valueOf(number)),4, BigDecimal.ROUND_HALF_UP).doubleValue();
						ocList.get(j).setPack4(new DecimalFormat("0.00####").format(danPrice));
						money = money
								+ Double.parseDouble(this.ocList.get(j).getParprice());
					} else if (ocList.get(j).getCarttype().equals("1")){//样品
						String number = ocList.get(j).getGNumber();
						String price = ocList.get(j).getGPrice();
						Double danPrice = Double.valueOf(price) / Double.valueOf(number);
						ocList.get(j).setPack4(df.format(danPrice));
						money = money
								+ Double.parseDouble(this.ocList.get(j).getGPrice());
						isyN ++;
						//判断当前采集数量是否是最小数量
						boolean a = fgoodsManager.isSmall(Long.valueOf(ocList.get(j).getGId()), Integer.valueOf(ocList.get(j).getGNumber()));
						good = fgoodsManager.getGood(Long.valueOf(ocList.get(j).getGId()));
						if (a && good.getIsSample().equals("1") && good.getIsPartial().equals("0")){
							samTotal += Double.valueOf(price);
						}
					}
				}
				
				//System.out.println(isyN>10);
				//System.out.println(!coupondetailManager.isDUse(uid));
				//System.out.println(samTotal==0d);
				if (isyN>10 || !coupondetailManager.isDUse(uid) || samTotal==0d){
					ison = false;
				}else{
					
					 //判断是否都是最小包装
					
				}
				
				//quchongCoupons(couponsAll);
				
				//活动优惠信息开始 ，商品价格满35 优惠十元
				/*
				 * 
				 
				if(money >= 35){
					this.request.put("huodongyuanjia", money);
					this.request.put("huodongyouhui", "10");
					this.request.put("total", money-10);
				} else {
					this.request.put("total", money);
				}
				//活动优惠信息结束
				 */
				if(gids != null && !gids.equals("")){
					this.request.put("gids", gids);
				}
				this.request.put("total", money); 
				
				this.request.put("samTotal", samTotal);
			} else {
				return "nologin";
			}

			this.regions = regionManager.queryById(0l);
		} catch (Exception e) {
			e.printStackTrace();
				String errorTotle = "抱歉，发生异常了";
				String errorMess = "后台错误";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
			return ERROR;
		}
		int temp = 0;
		int temp1 = 0;
		
		if (ison==true){
			for (int j = 0; j < couponsAll.size(); j++) {
				for (int j2 = 0; j2 < couponsAll.get(j).size(); j2++) {
					if(couponsAll.get(j).get(j2).getCtCoupon().getCouponType().equals("1") || couponsAll.get(j).get(j2).getCtCoupon().getCouponType().equals("9")||couponsAll.get(j).get(j2).getCtCoupon().getCouponType().equals("2")){
						if(couponsAll.get(j).get(j2).getCtCoupon().getCouponType().equals("9") && (isyN>0 && isyN<=10)){
							ccdListTypeNate.add(couponsAll.get(j).get(j2));
						}
						temp++;
					} else {
						temp1++;
					}
				}
			}
		}else{
			ccdListTypeNate = new ArrayList<CtCouponDetail>();
			
		}
		
		
		if(temp1 == 0){
			if(couponsAll.size() > 1){
				for (int i = 1; i < couponsAll.size(); i++) {
					couponsAll.remove(i);
				}
			}
			
		} else {
			List<CtCouponDetail> ccdListNew = new ArrayList<CtCouponDetail>();
			Map<Integer, List<CtCouponDetail>> mapTemp = new HashMap<Integer, List<CtCouponDetail>>();
			for (int j = 0; j < couponsAll.size(); j++) {
				ccdListNew = new ArrayList<CtCouponDetail>();
				for (int j2 = 0; j2 < couponsAll.get(j).size(); j2++) {
					ccdListNew.add(couponsAll.get(j).get(j2));
				}
				mapTemp.put(j, ccdListNew);
			}
			List<CtCouponDetail> ccdListNew1 = new ArrayList<CtCouponDetail>();
			for (List<CtCouponDetail> ctCouponDetail : mapTemp.values()) {
				for (int i = 0; i < ctCouponDetail.size(); i++) {
					ccdListNew1.add(ctCouponDetail.get(i));
				}
			}
			quchongCoupons(ccdListNew1);
			couponsAll.add(ccdListNew1);
		}
		shopList = orderManager.getAllShop();
		
		//判断是否当前有过订单
		this.request.put("isfirst", isfirst);
		for (int i = 0; i < ocList.size(); i++) {
			ViewOrderCheckDTO dto = setViewOrderCheckDTO(df, i);
			ocListDTO.add(dto);
		}
//		if(new SimpleDateFormat("yyyy-MM-dd").format(new Date())){
//			
//		}
		String week = getWeek(new Date());
		if(ocList.size() <= 20){
			session.put("zititime", 1);
		}
		if(ocList.size() > 20 && ocList.size() <= 40){
			session.put("zititime", 2);
		}
		if(ocList.size() > 40 && ocList.size() <= 70){
			session.put("zititime", 3);
		}
		if(ocList.size() > 70 && ocList.size() <= 100){
			session.put("zititime", 4);
		}
		if(ocList.size() >= 100){
			session.put("zititime", 5);
		}
		if(week.equals("星期日")){
			session.put("zititime", 0);
		}
		try { 
			SimpleDateFormat sdf = new SimpleDateFormat("HH");
			String sixDate = sdf.format(new Date());
			if((Integer.valueOf(sixDate) >= 18 || Integer.valueOf(sixDate) <= 9) && !week.equals("星期日")){
				session.put("zititimeSix", 1);
				if(ocList.size() <= 20){
					session.put("zititime", 1);
				}
				if(ocList.size() > 20 && ocList.size() <= 40){
					session.put("zititime", 2);
				}
				if(ocList.size() > 40 && ocList.size() <= 70){
					session.put("zititime", 3);
				}
				if(ocList.size() > 70 && ocList.size() <= 100){
					session.put("zititime", 4);
				}
				if(ocList.size() >= 100){
					session.put("zititime", 5);
				}
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public static String getWeek(Date date){ 
		SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
		String week = sdf.format(date);
		return week;
	}
	private void newOrderCheckDTOData(List<ViewOrderCheck> goodsList, int type) throws IllegalAccessException {
		List<ViewOrderCheck> orderGoodsTempList = new ArrayList<ViewOrderCheck>();
		for (int j = 0; j < goodsList.size(); j++) {
			orderGoodsTempList.add(goodsList.get(j));
		}
		for (int k = 0; k < orderGoodsTempList.size(); k++)  //外循环是循环的次数
		{
			for (int j = orderGoodsTempList.size() - 1 ; j > k; j--)  //内循环是 外循环一次比较的次数
			{
				
				if (orderGoodsTempList.get(k).getGId().equals(orderGoodsTempList.get(j).getGId()))
				{
					orderGoodsTempList.remove(j);
				}
				
			}
		}
		for (int j = 0; j < orderGoodsTempList.size(); j++) {
			OrderInfoGoodsDTO goodsDTO = new OrderInfoGoodsDTO();
			for (int k = 0; k < goodsList.size(); k++) {
				if(goodsList.get(k).getGId().toString().equals(orderGoodsTempList.get(j).getGId().toString())){
					goodsDTO.setGoodsId(Integer.valueOf(goodsList.get(k).getGId()));
					goodsDTO.setGoodsName(goodsList.get(k).getGName());
					if(false){
						if(goodsList.get(k).getCarttype().equals("0")){
							goodsDTO.setSimNum(goodsList.get(k).getGNumber());
							goodsDTO.setSimPrice(goodsList.get(k).getGPrice());
							Double num = Double.valueOf(goodsList.get(k).getGNumber());
							Double price = Double.valueOf(goodsList.get(k).getGPrice());
							goodsDTO.setSimPrice(div(price.toString(), num.toString(), 2)+"");
							goodsDTO.setSimPriceCount(price.toString());
							goodsDTO.setAllCountNum(Integer.valueOf(num.intValue())+goodsDTO.getAllCountNum());
							goodsDTO.setAllPrice(add(price.toString(), goodsDTO.getAllPrice())+"");
						} else {
							goodsDTO.setParNum(goodsList.get(k).getParnumber()/1000+"");
							goodsDTO.setParPrice(goodsList.get(k).getParprice()+"");
							Double num = Double.valueOf(goodsList.get(k).getParnumber());
							Double price = Double.valueOf(goodsList.get(k).getParprice().toString());
							goodsDTO.setParPriceCount(price.toString());
							goodsDTO.setParPrice(div(price.toString(), div(num.toString(), 1000+"", 2)+"", 2)+"");
							goodsDTO.setAllCountNum(Integer.valueOf(num.intValue())+goodsDTO.getAllCountNum());
							goodsDTO.setAllPrice(add(price.toString(), goodsDTO.getAllPrice())+"");
						}
					} else {
						if(goodsList.get(k).getCarttype().equals("1")){
							System.out.println(k);
							goodsDTO.setSimNum(goodsList.get(k).getGNumber());
							goodsDTO.setSimPrice(goodsList.get(k).getGPrice());
							Double num = Double.valueOf(goodsList.get(k).getGNumber());
							Double price = Double.valueOf(goodsList.get(k).getGPrice());
							goodsDTO.setSimPrice(price / num+"");
							goodsDTO.setSimPriceCount(price.toString());
							goodsDTO.setAllCountNum(Integer.valueOf(num.intValue())+goodsDTO.getAllCountNum());
							goodsDTO.setAllPrice(add(price.toString(), goodsDTO.getAllPrice())+"");
						} else {
							goodsDTO.setParNum(goodsList.get(k).getParnumber()/1000+"");
							goodsDTO.setParPrice(add(goodsList.get(k).getParprice(),"0")+"");
							Double num = Double.valueOf(goodsList.get(k).getParnumber());
							Double price = Double.valueOf(goodsList.get(k).getParprice().toString());
							goodsDTO.setParPriceCount(add(price.toString(),"0")+"");
							goodsDTO.setParPrice(price / (num/  1000)+"");
							goodsDTO.setAllCountNum(Integer.valueOf(num.intValue())+goodsDTO.getAllCountNum());
							goodsDTO.setAllPrice(add(price.toString(), goodsDTO.getAllPrice())+"");
						}
					}
				}
			}
			orderInfoGoodsDTOList.add(goodsDTO);
		}
//		Integer allNum = 0;
//		String allPrice = "0.00";
//		for (int j = 0; j < orderInfoGoodsDTOList.size(); j++) {
//			allNum = Integer.valueOf(orderInfoGoodsDTOList.get(j).getSimNum()) + Integer.valueOf(orderInfoGoodsDTOList.get(j).getParNum())*1000;
//			orderInfoGoodsDTOList.get(j).setAllCountNum(allNum);
//			BigDecimal b1=new BigDecimal(orderInfoGoodsDTOList.get(j).getSimPriceCount());
//			BigDecimal b2=new BigDecimal(orderInfoGoodsDTOList.get(j).getParPriceCount());
//			BigDecimal b3=new BigDecimal(allPrice);
//			allPrice = b3.add(b1.add(b2)).toString();
//		}
//		BigDecimal b1=new BigDecimal(orderListDTO.getGoodsAllPrice());
//		BigDecimal b2=new BigDecimal(allPrice);
//		orderListDTO.setGoodsAllPrice(b1.add(b2).toString());
//		orderListDTO.setOrderInfoGoodsDTOList(orderInfoGoodsDTOList);
//		orderListDTO.setOrderId(orderList.get(i).getOrderId());
//		orderListDTO.setOrderSn(orderList.get(i).getOrderSn());
//		orderListDTO.setOrderStatus(Integer.valueOf(orderList.get(i).getOrderStatus()));
//		orderListDTO.setTotal(orderList.get(i).getTotal());
//		orderListDTO.setOrderTime(orderList.get(i).getOrderTime());
//		
//		orderListDTO.setRetTime(orderList.get(i).getRetTime());
//		orderListDTO.setRetStatus(orderList.get(i).getRetStatus());
//		orderListDTO.setEvaId(orderList.get(i).getEvaId() == null ? null : orderList.get(i).getEvaId().intValue());
//		orderListDTO.setEvaDesc(orderList.get(i).getEvaDesc());
//		orderListDTO.setEvaTime(orderList.get(i).getEvaTime() == null ? null : orderList.get(i).getEvaTime().split(" ")[0]);
//		
//		if(orderList.get(i).getOrderStatus().equals("1") || orderList.get(i).getOrderStatus().equals("3")|| orderList.get(i).getOrderStatus().equals("4")|| orderList.get(i).getOrderStatus().equals("5")|| orderList.get(i).getOrderStatus().equals("8")){
//			String trueTotal = orderManager.findPayTotalByOrderSn(orderList.get(i).getOrderSn());
//			if(trueTotal != null){
//				orderListDTO.setTrueTotal(trueTotal);
//			}
//		}
//		orderListDTO.setFreight(orderList.get(i).getFreight());
//		if(orderList.get(i).getShippingType().equals("2") || orderList.get(i).getShippingType().equals("3")){
//			if(orderList.get(i).getShoppingTypeCollect()){
//				orderListDTO.setFreight("0");
//				orderListDTO.setFerightDaoFu("到付");
//			} else {
//				orderListDTO.setFreight(orderList.get(i).getFreight());
//			}
//		}
//		orderListDTO.setGoodsSize(orderListDTO.getOrderInfoGoodsDTOList().size());
//		orderListDTOList.add(orderListDTO);
	}
	
	private ViewOrderCheckDTO setViewOrderCheckDTO(DecimalFormat df, int i) {
		ViewOrderCheckDTO dto = new ViewOrderCheckDTO();
		dto.setCartId(ocList.get(i).getCartId());
		dto.setCarttype(ocList.get(i).getCarttype());
		dto.setGId(ocList.get(i).getGId());
		dto.setGName(ocList.get(i).getGName());
		dto.setGNumber(ocList.get(i).getGNumber());
		dto.setGPrice(ocList.get(i).getGPrice());
		dto.setGUnit(ocList.get(i).getGUnit());
		dto.setPack(ocList.get(i).getPack());
		dto.setPack1(ocList.get(i).getPack1());
		dto.setPack1Num(ocList.get(i).getPack1Num());
		dto.setPack2(ocList.get(i).getPack2());
		dto.setPack2Num(ocList.get(i).getPack2Num());
		dto.setPack3(ocList.get(i).getPack3());
		dto.setPack3Num(ocList.get(i).getPack3Num());
		dto.setPack4(ocList.get(i).getPack4());
		dto.setPack4Num(ocList.get(i).getPack4Num());
		
		Double parnumnew = Double.valueOf(ocList.get(i).getParnumber()==null ? "0" : ocList.get(i).getParnumber().toString()) / 1000;
		dto.setParnumber(new DecimalFormat("#.####").format(parnumnew));
		
		dto.setParprice(ocList.get(i).getParprice());
		dto.setPartial(ocList.get(i).getPartial());
		dto.setPromotePrice(ocList.get(i).getPromotePrice());
		dto.setPUrl(ocList.get(i).getPUrl());
		dto.setSample(ocList.get(i).getSample());
		dto.setShopPrice(ocList.get(i).getShopPrice());
		dto.setUId(ocList.get(i).getUId());
		return dto;
	}

	public String getGidss() {
		return gidss;
	}

	public void setGidss(String gidss) {
		this.gidss = gidss;
	}

	private void quchongCoupons(List<CtCouponDetail> coupons) {
		for (int k = 0; k < coupons.size(); k++)  //外循环是循环的次数
		{
		    for (int j = coupons.size() - 1 ; j > k; j--)  //内循环是 外循环一次比较的次数
		    {

		        if (coupons.get(k).getCtCoupon().getAmount().equals(coupons.get(j).getCtCoupon().getAmount()) && coupons.get(k).getCtCoupon().getDiscount().equals(coupons.get(j).getCtCoupon().getDiscount()))
		        {
		        	coupons.remove(j);
		        }

		    }
		}
	}
	private CtBom bomInfo = new CtBom();
	
	// bom跳转确认订单
	public CtBom getBomInfo() {
		return bomInfo;
	}

	public void setBomInfo(CtBom bomInfo) {
		this.bomInfo = bomInfo;
	}

	public String orderCheckByBOM() throws IllegalAccessException {
		String res = getBomIsGoods();
		boolean ison = true;
		int isyN =0;
		boolean isfirst=false;//判断用户是否首单
		if(res.equals("goOrderChear")){
			return res;
		}
		DecimalFormat df = new DecimalFormat("0.00####");
		this.cateList = basicManager.queryAllCate();
		this.catenumList = basicManager.cateCount();
		this.subcatenumList = basicManager.subcateCount();
		this.couponDetails = coupondetailManager.getAllNotUse();
		Long uid = null;
		if (this.session.get("userinfosession") != null) {
			CtUser cu = (CtUser) this.session.get("userinfosession");
			uid = cu.getUId();
			// 取收货地址
			this.addressList = orderManager.getUserAddress(uid);
			if (addressList != null) {
				for (int c = 0; c < this.addressList.size(); c++) {
					Integer def = Integer.parseInt(this.addressList.get(c)
							.getIsdefault().trim());
					if (def == 1) {
						String address = this.addressList.get(c).getCountry()
								+ this.addressList.get(c).getProvince()
								+ this.addressList.get(c).getCity()
								+ this.addressList.get(c).getDistrict()
								+ this.addressList.get(c).getAddress();
						String name = this.addressList.get(c).getAConsignee();
						String mp = this.addressList.get(c).getMb();
						this.request.put("addid", this.addressList.get(c)
								.getAId());
						this.request.put("address", address);
						this.request.put("name", name);
						this.request.put("mp", mp);
					}
				}
				int add = addressList.size();
				this.request.put("addsize", add);
			}
			// 取商品列表
			Integer bomid = orderDTO.getBomid();
			 bomid = orderDTO.getMid();
			//System.out.println(orderDTO.getBomid());
			List<ViewOrderBom> bglist = orderManager.getCheckByBom(bomid);
			bomInfo = bomManager.getCtBomByBomId(bomid, uid);
			//ViewOrderBom vob = new ViewOrderBom();
			String gids = "";
			for (int i = 0; i < bglist.size(); i++) {
				gids += bglist.get(i).getGId() + ",";
				ViewOrderCheck voc = new ViewOrderCheck();
				List<CtCouponDetail> coupons = this.coupondetailManager.getCtCouponsByGid(uid,Long.valueOf(bglist.get(i).getGId().toString()));
				quchongCoupons(coupons);
				voc.setGId(bglist.get(i).getGId().toString());
				voc.setGName(bglist.get(i).getGName());
				Integer sumCount = bglist.get(i).getGoodsNum() * bomInfo.getNum();
				Integer simCount = 0;
				Integer parCount = 0;
				Double simPrice = 0D;
				Double parPrice = 0D;
				Double tempSimCount = 0D;
				List<CtRangePrice>  renList = basicManager.getAllRanPrice(bglist.get(i).getGId());
				Integer p = 0;
				for (int j = 0; j < renList.size(); j++) {
					if(renList.get(j).getParIncrease() != null){
						p = renList.get(j).getParIncrease();
					}
				}
				if(sumCount >= (p*1000)){
					Integer xiaoSum = sumCount / 1000;
					Double isZheng = Double.valueOf(xiaoSum) / Double.valueOf(p);
					if(isZheng.toString().indexOf(".")>0){
						String[] chaiZheng = isZheng.toString().split("\\.");
						if(!chaiZheng[1].equals("0")){
							chaiZheng[1] = "0." + chaiZheng[1];
							tempSimCount = Double.valueOf(chaiZheng[1]) * Double.valueOf(p);
							simCount = (int) (tempSimCount * 1000);
							parCount = Integer.valueOf(chaiZheng[0]) * p;
							parCount *= 1000;
						} else {
							if(xiaoSum.toString().indexOf(".") > 0){
								String[] chai = xiaoSum.toString().split(".");
								chai[1] = "0."+chai[1];
								String geWei = chai[0].substring(chai[0].length()-1, chai[0].length());
								Integer yuShu = Integer.valueOf(geWei) % p;
								if(yuShu > 0){
									Integer sim = p*2;
									parCount = sim + Integer.valueOf(chai[0]);
								}
								simCount = Integer.valueOf(chai[1])*1000;
							} else {
								parCount = xiaoSum;
							}
						}
					}
				} else {
					simCount = sumCount;
				}
				for (int j = 0; j < renList.size(); j++) {
					if(renList.get(renList.size()-1).getSimENum() == null){
						renList.get(renList.size()-1).setSimENum(0);
					}
					if(renList.get(renList.size()-1).getParENum() == null){
						renList.get(renList.size()-1).setParENum(0);
					}
					if((Integer) simCount < renList.get(0).getSimSNum()){
						simPrice = renList.get(0).getSimRPrice();
					}
					if((Integer) simCount >= renList.get(renList.size()-1).getSimSNum() && renList.get(renList.size()-1).getSimENum() == 0){
						simPrice = renList.get(renList.size()-1).getSimRPrice();
					}
					if((Integer) simCount >= renList.get(j).getSimSNum() && (Integer) simCount <= renList.get(j).getSimENum()){
						simPrice = renList.get(j).getSimRPrice();
					}
					System.out.println(renList.get(renList.size()-1).getParSNum());
					System.out.println(renList.get(renList.size()-1).getParENum());
					if(renList.get(renList.size()-1).getParSNum() != null && (Integer) parCount >= renList.get(renList.size()-1).getParSNum() && (renList.get(renList.size()-1).getParENum() == null || renList.get(renList.size()-1).getParENum() == 0)){
						parPrice = renList.get(renList.size()-1).getParRPrice();
					}
					if(renList.get(j).getParSNum()!= null && (Integer) parCount >= renList.get(j).getParSNum() && (Integer) parCount <= renList.get(j).getParENum()){
						parPrice = renList.get(j).getParRPrice();
					}
					
				}
				if(simCount == 0 && parCount == 0){
					return ERROR;
				}
				parPrice = parPrice * 1000;
				String pack = "";
				if(simCount != 0 && parCount == 0){
					Double countPriceSim = simPrice * simCount;
					voc.setGNumber(simCount.toString());
					voc.setGPrice(countPriceSim.toString());
					voc.setCarttype("1");
					pack = "1-0";
					managerMergth(df, uid, bomid, bglist, i, voc, coupons, pack);
				}
				if(simCount == 0 && parCount != 0){
					Double countPrice = parPrice * parCount;
					voc.setParnumber(Long.valueOf(parCount));
					voc.setParprice(countPrice.toString());
					voc.setCarttype("0");
					pack = "0-1";
					managerMergth(df, uid, bomid, bglist, i, voc, coupons, pack);
				}
				if(simCount != 0 && parCount != 0){
					//样品
					voc = new ViewOrderCheck();
					voc.setGId(bglist.get(i).getGId().toString());
					voc.setGName(bglist.get(i).getGName());
					Double countPriceSim = simPrice * simCount;
					voc.setGNumber(simCount.toString());
					voc.setGPrice(countPriceSim.toString());
					voc.setCarttype("1");
					pack = "1-0";
					managerMergth(df, uid, bomid, bglist, i, voc, coupons, pack);
					//批量
					
					voc = new ViewOrderCheck();
					voc.setGId(bglist.get(i).getGId().toString());
					voc.setGName(bglist.get(i).getGName());
					Double countPrice = parPrice * parCount;
					voc.setParnumber(Long.valueOf(parCount));
					voc.setParprice(countPrice.toString());
					voc.setCarttype("0");
					pack = "0-1";
					managerMergth(df, uid, bomid, bglist, i, voc, coupons, pack);
				}
				//managerMergth(df, uid, bomid, bglist, i, voc, coupons, pack);
//				
//				String[] pack = vob.getPack().split("K");
//				Double pa = Double.parseDouble(pack[0]);
//				Double num = vob.getGoodsNum().doubleValue();
//				Double pp = vob.getPromotePrice().doubleValue();
//				Double sub = 0.0;
//				if (pp != 9999) {
//					sub = pa * num * pp;
//				} else {
//					sub = vob.getShopPrice().doubleValue() * num * pa;
//				}
//				vob.setGPrice(sub.toString());
//				this.obList.add(vob);
				
//				voc.setGId(orderGoodslist.get(i).getGId().toString());
//				voc.setGName(orderGoodslist.get(i).getCtGoods().getGName());
//				if(orderGoodslist.get(i).getIsParOrSim().equals("1")){
//					voc.setGNumber(orderGoodslist.get(i).getGNumber());
//					voc.setGPrice(orderGoodslist.get(i).getGSubtotal());
//					voc.setCarttype("1");
//				} else if (orderGoodslist.get(i).getIsParOrSim().equals("0")){
//					Long num = orderGoodslist.get(i).getGParNumber();
//					String par = orderGoodslist.get(i).getGParPrice();
//					Double price = Double.valueOf(par) * Double.valueOf(num);
//					String temp = "";
//					temp = df.format(price);
//					voc.setParnumber(orderGoodslist.get(i).getGParNumber());
//					voc.setParprice(temp);
//					voc.setCarttype("0");
//				}
//				voc.setGUnit(orderGoodslist.get(i).getCtGoods().getGUnit());
//				voc.setPack(orderGoodslist.get(i).getPack());
//				if(orderGoodslist.get(i).getCtGoods().getPromotePrice() != null){
//					voc.setPromotePrice(new java.math.BigDecimal(orderGoodslist.get(i).getCtGoods().getPromotePrice()));
//				} else {
//					voc.setPromotePrice(new java.math.BigDecimal(9999));
//				}
//				voc.setPUrl("nourl");
//				voc.setShopPrice(new java.math.BigDecimal(orderGoodslist.get(i).getCtGoods().getShopPrice()));
//				voc.setUId(uid.toString());
//				this.ocList.add(voc);
				
			}
			/*
			 * 用bomid查bomgoods表 视图中g_price为空，后台查价格写入list
			 */
			Double samTotal=(double) 0;
			Double money = 0.0;
			CtGoods good = new CtGoods();
			for (int j = 0; j < this.ocList.size(); j++) {
				if(ocList.get(j).getCarttype().equals("0")){
					Long number = ocList.get(j).getParnumber();
					String price = ocList.get(j).getParprice();
					if(price.indexOf(".") == 0){
						price = "0" + price;
						ocList.get(j).setParprice(price);
					}
					Double danPrice = Double.valueOf(price) / Double.valueOf(number);
					ocList.get(j).setPack4(df.format(danPrice));
					money = money
							+ Double.parseDouble(this.ocList.get(j).getParprice());
				} else if (ocList.get(j).getCarttype().equals("1")){
					isyN++;
					String number = ocList.get(j).getGNumber();
					String price = ocList.get(j).getGPrice();
					Double danPrice = Double.valueOf(price) / Double.valueOf(number);
					ocList.get(j).setPack4(df.format(danPrice));
					money = money
							+ Double.parseDouble(this.ocList.get(j).getGPrice());
					boolean a = fgoodsManager.isSmall(Long.valueOf(ocList.get(j).getGId()), Integer.valueOf(ocList.get(j).getGNumber()));
					good = fgoodsManager.getGood(Long.valueOf(ocList.get(j).getGId()));
					if (a && good.getIsSample().equals("1") && good.getIsPartial().equals("0")){
						samTotal += Double.valueOf(price);
					}
				}
			}
			
			if (isyN>=10 || !coupondetailManager.isDUse(uid) || samTotal==0d){
				ison = false;
			}else{
				
				 //判断是否都是最小包装
				
			}
			orderGoodslist = new ArrayList<CtOrderGoods>();
			orderInfoGoodsDTOList = new ArrayList<OrderInfoGoodsDTO>();
			newOrderCheckDTOData(ocList, 0);
			Double allPrice = 0D;
			for (int i = 0; i < orderInfoGoodsDTOList.size(); i++) {
				allPrice = add(orderInfoGoodsDTOList.get(i).getAllPrice(), allPrice.toString());
			}
			this.request.put("newAllGoodsPrice", allPrice);
			//quchongCoupons(couponsAll);
			for (int i = 0; i < ocList.size(); i++) {
				ViewOrderCheckDTO dto = setViewOrderCheckDTO(df, i);
				ocListDTO.add(dto);
			}
			if(ocList.size() <= 20){
				session.put("zititime", 1);
			}
			if(ocList.size() > 20 && ocList.size() <= 40){
				session.put("zititime", 2);
			}
			if(ocList.size() > 40 && ocList.size() <= 70){
				session.put("zititime", 3);
			}
			if(ocList.size() > 70 && ocList.size() <= 100){
				session.put("zititime", 4);
			}
			if(ocList.size() >= 100){
				session.put("zititime", 5);
			}
			this.request.put("gids", gids);
			this.request.put("total", money);
			this.request.put("samTotal", samTotal);
			this.request.put("bomid", bomid);
			this.request.put("total", money);
			session.put("orderGoodslist", goodsList);
			this.request.put("gids", gids);
	
		} else {
			return "error";
		}
	
		this.regions = regionManager.queryById(0l);
		int temp = 0;
		int temp1 = 0;
		
		if (ison==true){
			for (int j = 0; j < couponsAll.size(); j++) {
				for (int j2 = 0; j2 < couponsAll.get(j).size(); j2++) {
					if(couponsAll.get(j).get(j2).getCtCoupon().getCouponType().equals("1") || couponsAll.get(j).get(j2).getCtCoupon().getCouponType().equals("9")){
						if(couponsAll.get(j).get(j2).getCtCoupon().getCouponType().equals("9") && (isyN>0 && isyN<10)){
							ccdListTypeNate.add(couponsAll.get(j).get(j2));
						}
						temp++;
					} else {
						temp1++;
					}
				}
			}
		}else{
			ccdListTypeNate = new ArrayList<CtCouponDetail>();
			
		}
			
		
		
		

		if(temp1 == 0){
			if(couponsAll.size() > 1){
				for (int i = 1; i < couponsAll.size(); i++) {
					couponsAll.remove(i);
				}
			}
			
		} else {
			List<CtCouponDetail> ccdListNew = new ArrayList<CtCouponDetail>();
			Map<Integer, List<CtCouponDetail>> mapTemp = new HashMap<Integer, List<CtCouponDetail>>();
			for (int j = 0; j < couponsAll.size(); j++) {
				ccdListNew = new ArrayList<CtCouponDetail>();
				for (int j2 = 0; j2 < couponsAll.get(j).size(); j2++) {
					ccdListNew.add(couponsAll.get(j).get(j2));
				}
				mapTemp.put(j, ccdListNew);
			}
			List<CtCouponDetail> ccdListNew1 = new ArrayList<CtCouponDetail>();
			for (List<CtCouponDetail> ctCouponDetail : mapTemp.values()) {
				for (int i = 0; i < ctCouponDetail.size(); i++) {
					ccdListNew1.add(ctCouponDetail.get(i));
				}
			}
			quchongCoupons(ccdListNew1);
			couponsAll.add(ccdListNew1);
		}
		for (int k = 0; k < couponsAll.size(); k++)  //外循环是循环的次数
		{
		    for (int j = couponsAll.size() - 1 ; j > k; j--)  //内循环是 外循环一次比较的次数
		    {

		        if (couponsAll.get(k).get(0).getCouponId().equals(couponsAll.get(j).get(0).getCouponId()))
		        {
		        	couponsAll.remove(j);
		        }

		    }
		}
		shopList = orderManager.getAllShop();
		
		//判断是否当前有过订单
		this.request.put("isfirst", isfirst);
		
		
		
		return SUCCESS;
	}

	public CtPayInterface getPayInterface() {
		return payInterface;
	}

	public void setPayInterface(CtPayInterface payInterface) {
		this.payInterface = payInterface;
	}

	private void managerMergth(DecimalFormat df, Long uid, Integer bomid,
			List<ViewOrderBom> bglist, int i, ViewOrderCheck voc,
			List<CtCouponDetail> coupons, String pack) {
		ViewOrderBom vob;
		voc.setGUnit(bglist.get(i).getGUnit());
		voc.setPack(pack);
		
		if(bglist.get(i).getPromotePrice() != null){
			voc.setPromotePrice(bglist.get(i).getPromotePrice());
		} else {
			voc.setPromotePrice(new java.math.BigDecimal(9999));
		}
		vob = bglist.get(i);
		String pr = this.basicManager.getPrice(uid, vob.getGId());
		if (pr != null) {
			vob.setPromotePrice(new java.math.BigDecimal(pr));
		}
		voc.setPUrl("nourl");
		voc.setShopPrice(bglist.get(i).getShopPrice());
		voc.setUId(uid.toString());
		this.ocList.add(voc);
		
		CtOrderGoods cogoods = new CtOrderGoods();
		cogoods.setOrderId(Long.valueOf(bomid));
		cogoods.setGId(bglist.get(i).getGId());
		String price = basicManager.getPrice(uid,
				Long.parseLong(bglist.get(i).getGId().toString()));
		if (price == null) {
			price = voc.getPromotePrice().toString();
			if (price.equals("9999")) {
				price = voc.getShopPrice().toString();
			}
		}
		if(voc.getCarttype().equals("1")){
			Double danPrice =  Double.valueOf(voc.getGPrice()) / Double.valueOf(voc.getGNumber()) ;
			cogoods.setGPrice(df.format(danPrice));
			cogoods.setGNumber(voc.getGNumber());
			cogoods.setCartType("1");
			cogoods.setIsParOrSim("1");
			cogoods.setGSubtotal(voc.getGPrice());
		} else if (voc.getCarttype().equals("0")) {
			Double danPrice =  Double.valueOf(voc.getParprice()) / Double.valueOf(voc.getParnumber());
			cogoods.setGParPrice(df.format(danPrice));
			cogoods.setGParNumber(voc.getParnumber());
			cogoods.setCartType("0");
			cogoods.setIsParOrSim("0");
			cogoods.setGSubtotal(voc.getParprice());
		}
		cogoods.setPack(pack);
		if(orderDTO.getCupon() != null){
			cogoods.setCouponId(Long.valueOf(orderDTO.getCupon().toString()));
		} else {
			cogoods.setCouponId(null);
		}
		
		goodsList.add(cogoods);
		if(coupons.size() != 0){
			this.couponsAll.add(coupons);
		}
	}

	private CtBomDTO bomDTO = new CtBomDTO();
	private List<CtBomGoods> bomGoodsList;
	
	//检索bom是否有商品
			public String getBomIsGoods(){
				Integer bomId = bomDTO.getBomId();
				bomGoodsList = bomManager.getCtBomGoodsByBomId(bomId);
				if(bomGoodsList != null && bomGoodsList.size() > 0){
					result = "yes";
					return "success";
				} else {
					result = "您的bom中还么有添加商品呢，请添加后尝试";
				}
				return "goOrderChear";
			}
	private Double couponPrice;
	
	public Double getCouponPrice() {
		return couponPrice;
	}

	public void setCouponPrice(Double couponPrice) {
		this.couponPrice = couponPrice;
	}
	private CtPayDTO payDTOForZhi = new CtPayDTO();
	
	// 下订单
	public CtPayDTO getPayDTOForZhi() {
		return payDTOForZhi;
	}

	public void setPayDTOForZhi(CtPayDTO payDTOForZhi) {
		this.payDTOForZhi = payDTOForZhi;
	}
	
	/**
	 * 递归去除可能重复的订单号
	 * @param orderSn
	 * @param orderMan 
	 * @param uid
	 * @return
	 */
	private static String checkOrderSn(String orderSn, OrderManager orderMan, Long uid){
		CtOrderInfo o = orderMan.getOrderInfoByOrderSn(orderSn);
		if(o != null){
			Date dt = new Date();
			Long time = dt.getTime();
			Long osn = time * uid;
			String orderSN = "CT" + new SimpleDateFormat("yyyyMMdd").format(new Date())
					+ osn.toString().substring(osn.toString().length() - 4,
							osn.toString().length());
			return checkOrderSn(orderSN, orderMan, uid);
		} else {
			return orderSn;
		}
	}
	
	
	String gidsTest = "";
	public String orderFinish() {
		try {
			this.cateList = basicManager.queryAllCate();
			this.catenumList = basicManager.cateCount();
			this.subcatenumList = basicManager.subcateCount();
			DecimalFormat df = new DecimalFormat("0.00####");
			Long uid = null;
			Double s =0d;
			if (this.session.get("userinfosession") != null) {
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();

				CtOrderInfo odinfo = new CtOrderInfo();
				// 生成订单号SN
				Date dt = new Date();
				Long time = dt.getTime();
				Long osn = time * uid;
				String orderSN = "CT" + new SimpleDateFormat("yyyyMMdd").format(new Date())
						+ osn.toString().substring(osn.toString().length() - 4,
								osn.toString().length());
				//去除重复订单号
				orderSN = checkOrderSn(orderSN, orderManager, uid);
				odinfo.setOrderSn(orderSN);
				odinfo.setUId(uid.toString());
				odinfo.setOrderStatus("0");
				// 订单时间
				SimpleDateFormat formatter = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");
				String ordertime = formatter.format(dt);
				String addid = orderDTO.getAddid();
				CtUserAddress address = new CtUserAddress();
				if(addid == null){
					return SUCCESS;
				}
				address = orderManager.getAddress(Long.parseLong(addid));

				
				
				// set订单
				odinfo.setConsignee(address.getAConsignee());
				odinfo.setCountry(new Long(address.getCountry()).intValue());
				odinfo.setProvince(new Long(address.getProvince()).intValue());
				odinfo.setCity(new Long(address.getCity()).intValue());
				odinfo.setDistrict(new Long(address.getDistrict()).intValue());
				odinfo.setAddress(address.getAddress());
				odinfo.setZipcode(address.getZipcode());
				
				if(((address.getMb() != null && !address.getMb().trim().equals("")) && (address.getTel() != null && !address.getTel().trim().equals("")))||(address.getMb() == null || address.getMb().trim().equals(""))){
					odinfo.setTel(address.getTel());
				} else {
					odinfo.setTel(address.getMb());
				}
				odinfo.setShippingType(orderDTO.getSptype());
				odinfo.setInvoiceType(orderDTO.getBilltype());
				odinfo.setInvoice(orderDTO.getBill());
				String[] temp = orderDTO.getPay().split(",");
				odinfo.setPay(temp[0]);
				odinfo.setShippingType(orderDTO.getShippingType().equals("1")?"2":orderDTO.getShippingType());
				String shoppingType = orderDTO.getShippingType();
				if(shoppingType.equals("4")){
					odinfo.setSId(orderDTO.getSId());
				}
				if(orderDTO.getShoppingTypeCollect()){
					System.out.println(orderDTO.getShoppingTypeCollect());
					odinfo.setKfDsc("到付 ");
				}
				odinfo.setShoppingTypeCollect(orderDTO.getShoppingTypeCollect());
				
				odinfo.setCouponId(null);
				odinfo.setDiscount(null);
				System.out.println(orderDTO.getYouhuiquanId());
				if (orderDTO.getCupon() != null && orderDTO.getSam() !=null &&  orderDTO.getYouhuiquanId()!=null && !orderDTO.getSam().equals("") && !orderDTO.getYouhuiquanId().equals("")){
					CtCouponDetail cc = coupondetailManager.findById(Long.valueOf(orderDTO.getYouhuiquanId()));
					CtCoupon ccp = coupondetailManager.findByCouponById(cc.getCouponId());
					
					odinfo.setCouponId(orderDTO.getCupon().toString()+","+orderDTO.getSam() + "," + orderDTO.getYouhuiquanId());
					Double allDis = Double.valueOf(orderDTO.getDiscount())+ccp.getDiscount();
					odinfo.setDiscount(allDis.toString());
				} else if(orderDTO.getCupon() != null && orderDTO.getSam() !=null && !orderDTO.getSam().equals("") && (orderDTO.getYouhuiquanId() == null || orderDTO.getYouhuiquanId().equals(""))){
					odinfo.setCouponId(orderDTO.getCupon().toString()+","+orderDTO.getSam());
					Double allDis = Double.valueOf(orderDTO.getDiscount());
					odinfo.setDiscount(allDis.toString());
				} else if(orderDTO.getCupon() == null && orderDTO.getSam() !=null && !orderDTO.getSam().equals("") && orderDTO.getYouhuiquanId() != null && !orderDTO.getYouhuiquanId().equals("")){
					CtCouponDetail cc = coupondetailManager.findById(Long.valueOf(orderDTO.getYouhuiquanId()));
					CtCoupon ccp = coupondetailManager.findByCouponById(cc.getCouponId());
					
					odinfo.setCouponId(orderDTO.getSam()+","+orderDTO.getYouhuiquanId());
					Double allDis = Double.valueOf(orderDTO.getSamTotal())+ccp.getDiscount();
					odinfo.setDiscount(allDis.toString());
				} else if(orderDTO.getCupon() != null && (orderDTO.getSam() ==null || orderDTO.getSam().equals("")) && orderDTO.getYouhuiquanId() != null && !orderDTO.getYouhuiquanId().equals("")){
					CtCouponDetail cc = coupondetailManager.findById(Long.valueOf(orderDTO.getYouhuiquanId()));
					CtCoupon ccp = coupondetailManager.findByCouponById(cc.getCouponId());
					
					odinfo.setCouponId(orderDTO.getCupon().toString()+","+orderDTO.getYouhuiquanId());
					Double allDis = Double.valueOf(orderDTO.getDiscount())+ccp.getDiscount();
					odinfo.setDiscount(allDis.toString());
				} else if (orderDTO.getCupon() != null && (orderDTO.getSam() ==null||orderDTO.getSam().equals("")) && (orderDTO.getYouhuiquanId() == null || orderDTO.getYouhuiquanId().equals(""))){
					odinfo.setCouponId(orderDTO.getCupon().toString());
					odinfo.setDiscount(orderDTO.getDiscount().toString());
				} else if (orderDTO.getCupon() == null && orderDTO.getSam() !=null && !orderDTO.getSam().equals("") && (orderDTO.getYouhuiquanId() == null || orderDTO.getYouhuiquanId().equals(""))){
					odinfo.setCouponId(orderDTO.getSam());
					odinfo.setDiscount(orderDTO.getSamTotal().toString());
				} else if (orderDTO.getCupon() == null && (orderDTO.getSam() ==null || orderDTO.getSam().equals("")) && orderDTO.getYouhuiquanId() != null && !orderDTO.getYouhuiquanId().equals("")){
					CtCouponDetail cc = coupondetailManager.findById(Long.valueOf(orderDTO.getYouhuiquanId()));
					CtCoupon ccp = coupondetailManager.findByCouponById(cc.getCouponId());
					
					odinfo.setCouponId(orderDTO.getYouhuiquanId());
					odinfo.setDiscount(ccp.getDiscount().toString());
				} else  if (orderDTO.getCupon() == null && (orderDTO.getSam() ==null || orderDTO.getSam().equals("")) && (orderDTO.getYouhuiquanId() == null || orderDTO.getYouhuiquanId().equals(""))){
					odinfo.setCouponId(null);
					odinfo.setDiscount(null);
				}
				
				
				
				Double stotal = Double.valueOf(orderDTO.getTotal());
				
				//运费计算，首单免运费,满100免运费
				Double spost = 0d;
				
				
				if (orderDTO.getIsfirst()!=null && (orderDTO.getIsfirst().equals("true") || stotal>999999)){
					
				}else{
					if (orderDTO.getSh1().toString().equals("2")){
						spost = 10.00;
					}
					if (orderDTO.getSh1().toString().equals("3")){
						spost = 10.00;
					}
					
				}
				
				s = stotal;
				odinfo.setFreight(spost.toString());
				odinfo.setTotal(s.toString());
				
				
			    if(s==0.00){
			        odinfo.setOrderStatus("1");
			    }

				
				if (orderDTO.getRemark().equals("限45个字")){
					odinfo.setDsc(" ");
				}else{
					odinfo.setDsc(orderDTO.getRemark());
				}
				
				odinfo.setOrderTime(ordertime);
				// 获取CT_ORDER_INFO新增记录的ID
				HttpServletRequest req = ServletActionContext.getRequest();
				String Ip =UtilDate.getIpAddr(req);
				odinfo.setOrderIp(Ip);
				
				
				//保存订单信息
				String infoID = orderManager.saveOrderInfo(odinfo);	
				
				if(infoID.equals("Exerror")){
					String url = orderDTO.getUrl();
					String errorTotle = "抱歉，提交异常";
					String errorMess = "含非法字符";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", url);
					return ERROR;
				}
				orderInfo = orderManager.getOrderByOrderId(Long.valueOf(infoID));
				//是否使用优惠券
				if(orderDTO.getCupon() != null && !orderDTO.getCupon().equals("")){
					CtCouponDetail ccd = new CtCouponDetail();
					ccd.setCouponId(Integer.valueOf(orderDTO.getCupon().toString()));
					ccd.setUId(uid);
					ccd.setUseTime(new Date().toString());
					ccd.setOrderId(Long.valueOf(infoID));
					ccd.setPrePrice(couponPrice);
					String res=orderManager.updateCoupon(ccd);
					if(res.equals("Exerror")){
						String url = orderDTO.getUrl();
						String errorTotle = "抱歉，提交异常";
						String errorMess = "含非法字符";
						session.put("errorMess", errorMess);
						session.put("errorTotle", errorTotle);
						session.put("errorBackUrl", url);
						return ERROR;
					}
				}
				//是否使用推广卷
				if(orderDTO.getYouhuiquanId() != null && !orderDTO.getYouhuiquanId().equals("")){
					CtCouponDetail cc = coupondetailManager.findById(Long.valueOf(orderDTO.getYouhuiquanId()));
					CtCoupon ccp = coupondetailManager.findByCouponById(cc.getCouponId());
					
					CtCouponDetail ccd = new CtCouponDetail();
					ccd.setCouponId(Integer.valueOf(orderDTO.getYouhuiquanId().toString()));
					ccd.setUId(uid);
					ccd.setUseTime(new Date().toString());
					ccd.setOrderId(Long.valueOf(infoID));
					ccd.setPrePrice((double) ccp.getDiscount());
					String res=orderManager.updateCoupon(ccd);
					if(res.equals("Exerror")){
						String url = orderDTO.getUrl();
						String errorTotle = "抱歉，提交异常";
						String errorMess = "含非法字符";
						session.put("errorMess", errorMess);
						session.put("errorTotle", errorTotle);
						session.put("errorBackUrl", url);
						return ERROR;
					}
				}
				//是否使用优惠券
				if(orderDTO.getSam() != null && !orderDTO.getSam().equals("")){
					CtCouponDetail ccd = new CtCouponDetail();
					ccd.setCouponId(Integer.valueOf(orderDTO.getSam().toString()));
					ccd.setUId(uid);
					ccd.setUseTime(new Date().toString());
					ccd.setOrderId(Long.valueOf(infoID));
					ccd.setPrePrice(Double.valueOf(orderDTO.getSamTotal()));
					String res=orderManager.updateCoupon(ccd);
					if(res.equals("Exerror")){
						String url = orderDTO.getUrl();
						//System.out.println(url);
						String errorTotle = "抱歉，提交异常";
						String errorMess = "含非法字符";
						session.put("errorMess", errorMess);
						session.put("errorTotle", errorTotle);
						session.put("errorBackUrl", url);
						return ERROR;
					}
				}
				// 获取商品列表，存入CT_ORDER_GOODS
				String gids = orderDTO.getGids();
				Integer isBim = orderDTO.getBomid();
				if(isBim != null && isBim.toString().length() > 0 && !gids.equals("")){
					String[] gid = gids.split(",");
					if(gid == null || gid.length == 0){
						return "goodsNo";
					}
					List<CtOrderGoods> listOrderGoods = (List<CtOrderGoods>)session.get("orderGoodslist");
					for (int i = 0; i < listOrderGoods.size(); i++) {
						if(orderDTO.getCupon() != null){
							listOrderGoods.get(i).setCouponId(Long.valueOf(orderDTO.getCupon().toString()));
						} else {
							listOrderGoods.get(i).setCouponId(null);
						}
						listOrderGoods.get(i).setOrderId(Long.valueOf(infoID));
						orderManager.saveOrderGoods(listOrderGoods.get(i));
						gidsTest = listOrderGoods.get(i).getGId().toString();
					}
				} else if(!gids.equals("")){
					String[] gid = gids.split(",");
					if(gid == null || gid.length == 0){
						return "goodsNo";
					}
					for (int i = 1; i < gid.length; i++) {
						List<ViewOrderCheck> oclist = orderManager.getOrderCheck(uid,
								Long.parseLong(gid[i]),"GId");
						ViewOrderCheck ovc = new ViewOrderCheck();
						for (int j = 0; j < oclist.size(); j++) {
							ovc = oclist.get(j);
							CtOrderGoods cogoods = new CtOrderGoods();
							cogoods.setOrderId(Long.valueOf(infoID));
							cogoods.setGId(Long.parseLong(ovc.getGId()));
							String price = basicManager.getPrice(uid,
									Long.parseLong(ovc.getGId()));
							if (price == null) {
								price = ovc.getPromotePrice().toString();
								if (price.equals("9999")) {
									price = ovc.getShopPrice().toString();
								}
							}
							if(ovc.getCarttype().equals("1")){
								Double danPrice =  Double.valueOf(ovc.getGPrice())/  Double.valueOf(ovc.getGNumber()) ;
								cogoods.setGPrice(df.format(danPrice));
								cogoods.setGNumber(ovc.getGNumber());
								cogoods.setCartType("1");
								cogoods.setIsParOrSim("1");
								cogoods.setGSubtotal(new DecimalFormat("0.00####").format(Double.valueOf(ovc.getGPrice())));
							} else if (ovc.getCarttype().equals("0")) {
								Double danPrice =  Double.valueOf(ovc.getParprice())/ Double.valueOf(ovc.getParnumber());
								cogoods.setGParPrice(df.format(danPrice));
								cogoods.setGParNumber(ovc.getParnumber());
								cogoods.setCartType("0");
								cogoods.setIsParOrSim("0");
								cogoods.setGSubtotal(new DecimalFormat("0.00####").format(Double.valueOf(ovc.getParprice())));
							}
							cogoods.setPack("1--1");
							if(orderDTO.getCupon() != null){
								cogoods.setCouponId(Long.valueOf(orderDTO.getCupon().toString()));
							} else {
								cogoods.setCouponId(null);
							}
							//cogoods.setGSubtotal(new DecimalFormat("0.00####").format(Double.valueOf(ovc.getGPrice())));
							orderManager.saveOrderGoods(cogoods);
							gidsTest = cogoods.getGId().toString();
						}
					}
					for (int k = 1; k < gid.length; k++) {
						orderManager.delCartGoodsByUidAndGid(uid, Long.parseLong(gid[k]));
					}
				} else  {
					@SuppressWarnings("unchecked")
					List<CtOrderGoods> listOrderGoods = (List<CtOrderGoods>)session.get("orderGoodslist");
					if(listOrderGoods != null){
						for (int i = 0; i < listOrderGoods.size(); i++) {
							if(orderDTO.getCupon() != null){
								listOrderGoods.get(i).setCouponId(Long.valueOf(orderDTO.getCupon().toString()));
							} else {
								listOrderGoods.get(i).setCouponId(null);
							}
							listOrderGoods.get(i).setOrderId(Long.valueOf(infoID));
							listOrderGoods.get(i).setIsRefund("0");
							orderManager.saveOrderGoods(listOrderGoods.get(i));
							gidsTest = listOrderGoods.get(i).getGId().toString();
						}
					}
				}
				Long cartcount = basicManager.getCartGoodsNum(uid);
				this.session.put("islgn", cartcount);
			} else {
				return "error";
			}
			session.remove("gids");
			HttpServletRequest req = ServletActionContext.getRequest();
			String path = req.getContextPath();
			String url = req.getScheme()+"://"+req.getServerName()+":"+req.getServerPort()+path+"/";
			url += "goods_detail?gid=" + gidsTest;
			//System.out.println(url);
			payDTOForZhi.setPay_desc(orderInfo.getDsc());
			payDTOForZhi.setOut_trade_no(orderInfo.getOrderSn());
			payDTOForZhi.setSubject("长亭易购"+orderInfo.getOrderSn());
			payDTOForZhi.setTotal_fee(orderInfo.getTotal());
			payDTOForZhi.setGoods_address(url);
			payDTOForZhi.setOrderbody(orderInfo.getDsc());
			payDTOForZhi.setMem_id(orderInfo.getUId());
			
			session.put("payDTOForZhi", payDTOForZhi);
			session.put("orderInfo", orderInfo);
			if(s==0.00){
				//零元单添加支付信息
				payInterface = new CtPayInterface();
				payInterface.setOrderSn(orderInfo.getOrderSn());
				payInterface.setTradeNo("零元单");
				payInterface.setTradeStatus("TRADE_SUCCESS");
				payInterface.setTotalFee("0");
				payInterface.setTradeType("5");
				payInterface.setPayDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				
				if(payInterManager.findPayByOrderSn(orderInfo.getOrderSn()) == null){
					payInterManager.save(payInterface);
				}
			    return "chiFuLing";
			}
		} catch (Exception e) {
			//e.printStackTrace();
			String errorTotle = "抱歉,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			e.printStackTrace();
			return ERROR;
		}

		return SUCCESS;
	}
	public String orderChong(){
		String errorTotle = "抱歉，无法购买";
		String errorMess = "请不要重复提交订单";
		session.put("errorMess", errorMess);
		session.put("errorTotle", errorTotle);
		return SUCCESS;
	}
	// bom下订单
	public String bomFinish() {
		try {
			this.cateList = basicManager.queryAllCate();
			this.catenumList = basicManager.cateCount();
			this.subcatenumList = basicManager.subcateCount();

			Long uid = null;
			if (this.session.get("userinfosession") != null) {
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();

				CtOrderInfo odinfo = new CtOrderInfo();
				// 生成订单号SN
				Date dt = new Date();
				Long time = dt.getTime();
				Long osn = time * uid;
				String orderSN = "CT"+ new SimpleDateFormat("yyyyMMdd").format(new Date())
						+ osn.toString().substring(osn.toString().length() - 10,
								osn.toString().length());
				orderSN = checkOrderSn(orderSN, orderManager, uid);
				odinfo.setOrderSn(orderSN);
				odinfo.setUId(uid.toString());
				odinfo.setOrderStatus("0");
				// 订单时间
				SimpleDateFormat formatter = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");
				String ordertime = formatter.format(dt);
				String addid = orderDTO.getAddid();
				CtUserAddress address = new CtUserAddress();
				address = orderManager.getAddress(Long.parseLong(addid));

				// set订单
				odinfo.setConsignee(address.getAConsignee());
				odinfo.setCountry(new Long(address.getCountry()).intValue());
				odinfo.setProvince(new Long(address.getProvince()).intValue());
				odinfo.setCity(new Long(address.getCity()).intValue());
				odinfo.setDistrict(new Long(address.getDistrict()).intValue());
				odinfo.setAddress(address.getAddress());
				odinfo.setZipcode(address.getZipcode());
				odinfo.setTel(address.getMb());
				odinfo.setShippingType(orderDTO.getSptype());
				odinfo.setInvoiceType(orderDTO.getBilltype());
				odinfo.setInvoice(orderDTO.getBill());
				odinfo.setPay(orderDTO.getPay());
				odinfo.setCouponId(null);
				odinfo.setFreight(orderDTO.getPost());
				odinfo.setTotal(orderDTO.getTotal());
				odinfo.setDiscount(null);
				odinfo.setDsc(orderDTO.getRemark());
				odinfo.setOrderTime(ordertime);
				// 获取CT_ORDER_INFO新增记录的ID
				String infoID = orderManager.saveOrderInfo(odinfo);
				// 获取商品列表，存入CT_ORDER_GOODS
				Integer bomid = orderDTO.getBomid();
				List<ViewOrderBom> oblist = orderManager.getCheckByBom(bomid);
				if(oblist == null || oblist.size() == 0){
					return "goodsNo";
				}
				if (oblist.size() > 0) {
					for (int i = 0; i < oblist.size(); i++) {
						CtOrderGoods cogoods = new CtOrderGoods();
						ViewOrderBom ovb = new ViewOrderBom();
						ovb = oblist.get(i);
						cogoods.setOrderId(Long.valueOf(infoID));
						cogoods.setGId(ovb.getGId());
						String price = basicManager.getPrice(uid, ovb.getGId());
						if (price == null) {
							price = ovb.getPromotePrice().toString();
							if (price.equals("9999")) {
								price = ovb.getShopPrice().toString();
							}
						}
						cogoods.setGPrice(price);
						cogoods.setGNumber(ovb.getGoodsNum().toString());
						cogoods.setPack(ovb.getPack());
						cogoods.setCouponId(null);
						cogoods.setGSubtotal(ovb.getGPrice());
						String res=orderManager.saveOrderGoods(cogoods);
						if(res.equals("Exerror")){
							String url = orderDTO.getUrl();
							////System.out.println(url);
							String errorTotle = "抱歉，提交异常";
							String errorMess = "含非法字符";
							session.put("errorMess", errorMess);
							session.put("errorTotle", errorTotle);
							session.put("errorBackUrl", url);
							return ERROR;
						}
					}
				}
				
				Long cartcount = basicManager.getCartGoodsNum(uid);
				this.session.put("islgn", cartcount);
			} else {
				return "error";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			String errorTotle = "抱歉,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
		return SUCCESS;
	}

	//我的订单页面查询
	public String getFindOrderBy(){
		try {
			this.cateList = basicManager.queryAllCate();
			this.catenumList = basicManager.cateCount();
			this.subcatenumList = basicManager.subcateCount();
			session.put("cateList", cateList);
			String query = orderDTO.getFindStr();
			if(query != null && !query.equals("")){
				orderDTO.setFindStr(query);
			}
			Long uid = null;
			boolean sim = false;
			boolean par = false;
			if (this.session.get("userinfosession") != null) {
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
				page = new Page();
				if (orderDTO.getPage() == 0) {
					orderDTO.setPage(1);
				}
				page.setCurrentPage(orderDTO.getPage());
				if(orderDTO.getStartTime() == null || orderDTO.getStartTime().equals("")){
					orderDTO.setStartTime("2015-01-01");
				}
				if(orderDTO.getEndTime() == null || orderDTO.getEndTime().equals("")){
					orderDTO.setEndTime(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
				}
				this.orderList = orderManager.getFindOrderBy(uid, page,orderDTO);
				Double priceFen = 0D;
				Double priceFenSim = 0D;
				if(orderList != null){
					for (int i = 0; i < orderList.size(); i++) {
						ctEvaluation = orderManager.getEvaByOrderId(orderList.get(i).getOrderId());
						if(ctEvaluation != null){
							orderList.get(i).setEvaDesc(ctEvaluation.getEvaDesc());
							orderList.get(i).setEvaTime(ctEvaluation.getEvaTime());
						}
						if(orderList.get(i).getTotal().indexOf(".") > 0){
							String[] pricess = orderList.get(i).getTotal().split("\\.");
							if(pricess[1].length()>4){
								pricess[1] = pricess[1].substring(0,4);
								orderList.get(i).setTotal(pricess[0] + "." + pricess[1]);
							}
						}
						par = false;
						sim = false;
						goodsList = orderManager.getOrderGoodsByOrderId(orderList.get(i).getOrderId());
						//新版订单数据整合
						newOrderInfoDTOData(i);
						for (int j = 0; j < goodsList.size(); j++) {
							
							if(goodsList.get(j).getIsParOrSim().equals("0")){
								par = true;
								Double goodsPrice = Double.valueOf(goodsList.get(j).getGParPrice()) * Double.valueOf(goodsList.get(j).getGParNumber());
								String price = "";
								if(goodsPrice.toString().indexOf(".") > 0){
									String[] pricess = goodsPrice.toString().split("\\.");
									if(pricess[1].length()>4){
										pricess[1] = pricess[1].substring(0,4);
										price = pricess[0] + "." + pricess[1];
									} else {
										price = goodsPrice.toString();
									}
								} else {
									price = goodsPrice.toString();
								}
								goodsList.get(j).setGSubtotal(price);
							} else {
								sim = true;
								Double goodsPrice = Double.valueOf(goodsList.get(j).getGPrice()) * Double.valueOf(goodsList.get(j).getGNumber());
								String price = "";
								if(goodsPrice.toString().indexOf(".") > 0){
									String[] pricess = goodsPrice.toString().split("\\.");
									if(pricess[1].length()>4){
										pricess[1] = pricess[1].substring(0,4);
										price = pricess[0] + "." + pricess[1];
									} else {
										price = goodsPrice.toString();
									}
								} else {
									price = goodsPrice.toString();
								}
								goodsList.get(j).setGSubtotal(price);
							}
							
							if(goodsList.get(j).getIsParOrSim().equals("0")){
								priceFen += Double.valueOf(goodsList.get(j).getGSubtotal());
								goodsList.get(j).setPriceAllDan(priceFen.toString());
							}
							if(goodsList.get(j).getIsParOrSim().equals("1")){
								priceFenSim += Double.valueOf(goodsList.get(j).getGSubtotal());
								goodsList.get(j).setPriceAllDanSim(priceFenSim.toString());
							}
							listGodsTest.add(goodsList.get(j));
						}
						priceFenSim = 0D;
						priceFen = 0D;
						if(sim && par){
							orderList.get(i).setIsParOrSim("1-1");
						} else if(sim && par == false){
							orderList.get(i).setIsParOrSim("1-0");
						} else if (sim == false && par){
							orderList.get(i).setIsParOrSim("0-1");
						}
						//goodsListAll.add(goodsList);
						//新的订单商品信息
						neworderGoodsDTOList(goodsList);
						goodsListAll.add(goodsList);
					}
				}
//				this.orderGoods = orderManager.getOrderGoods(uid);
			} else {
				return "error";
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			this.request.put("searchKey", orderDTO.getFindStr());
		} catch (Exception e) {
			e.printStackTrace();
			String errorTotle = "抱歉,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
		return SUCCESS;
	}
	//我的订单信用支付页面查询
	public String getFindCreOrderBy(){
		try {
			this.cateList = basicManager.queryAllCate();
			this.catenumList = basicManager.cateCount();
			this.subcatenumList = basicManager.subcateCount();
			session.put("cateList", cateList);
			String query = orderDTO.getFindStr();
			if(query != null && !query.equals("")){
				orderDTO.setFindStr(query);
			}
			Long uid = null;
			boolean sim = false;
			boolean par = false;
			if (this.session.get("userinfosession") != null) {
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
				page = new Page();
				if (orderDTO.getPage() == 0) {
					orderDTO.setPage(1);
				}
				page.setCurrentPage(orderDTO.getPage());
				if(orderDTO.getStartTime() == null || orderDTO.getStartTime().equals("")){
					orderDTO.setStartTime("2015-01-01");
				}
				if(orderDTO.getEndTime() == null || orderDTO.getEndTime().equals("")){
					orderDTO.setEndTime(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
				}
				orderDTO.setPay("ok");
				this.orderList = orderManager.getFindOrderBy(uid, page,orderDTO);
				Double priceFen = 0D;
				Double priceFenSim = 0D;
				if(orderList != null){
					for (int i = 0; i < orderList.size(); i++) {
						ctEvaluation = orderManager.getEvaByOrderId(orderList.get(i).getOrderId());
						if(ctEvaluation != null){
							orderList.get(i).setEvaDesc(ctEvaluation.getEvaDesc());
							orderList.get(i).setEvaTime(ctEvaluation.getEvaTime());
						}
						if(orderList.get(i).getTotal().indexOf(".") > 0){
							String[] pricess = orderList.get(i).getTotal().split("\\.");
							if(pricess[1].length()>4){
								pricess[1] = pricess[1].substring(0,4);
								orderList.get(i).setTotal(pricess[0] + "." + pricess[1]);
							}
						}
						par = false;
						sim = false;
						goodsList = orderManager.getOrderGoodsByOrderId(orderList.get(i).getOrderId());
						//新版订单数据整合
						newOrderInfoDTOData(i);
						for (int j = 0; j < goodsList.size(); j++) {
							
							if(goodsList.get(j).getIsParOrSim().equals("0")){
								par = true;
								Double goodsPrice = Double.valueOf(goodsList.get(j).getGParPrice()) * Double.valueOf(goodsList.get(j).getGParNumber());
								String price = "";
								if(goodsPrice.toString().indexOf(".") > 0){
									String[] pricess = goodsPrice.toString().split("\\.");
									if(pricess[1].length()>4){
										pricess[1] = pricess[1].substring(0,4);
										price = pricess[0] + "." + pricess[1];
									} else {
										price = goodsPrice.toString();
									}
								} else {
									price = goodsPrice.toString();
								}
								goodsList.get(j).setGSubtotal(price);
							} else {
								sim = true;
								Double goodsPrice = Double.valueOf(goodsList.get(j).getGPrice()) * Double.valueOf(goodsList.get(j).getGNumber());
								String price = "";
								if(goodsPrice.toString().indexOf(".") > 0){
									String[] pricess = goodsPrice.toString().split("\\.");
									if(pricess[1].length()>4){
										pricess[1] = pricess[1].substring(0,4);
										price = pricess[0] + "." + pricess[1];
									} else {
										price = goodsPrice.toString();
									}
								} else {
									price = goodsPrice.toString();
								}
								goodsList.get(j).setGSubtotal(price);
							}
							
							if(goodsList.get(j).getIsParOrSim().equals("0")){
								priceFen += Double.valueOf(goodsList.get(j).getGSubtotal());
								goodsList.get(j).setPriceAllDan(priceFen.toString());
							}
							if(goodsList.get(j).getIsParOrSim().equals("1")){
								priceFenSim += Double.valueOf(goodsList.get(j).getGSubtotal());
								goodsList.get(j).setPriceAllDanSim(priceFenSim.toString());
							}
							listGodsTest.add(goodsList.get(j));
						}
						priceFenSim = 0D;
						priceFen = 0D;
						if(sim && par){
							orderList.get(i).setIsParOrSim("1-1");
						} else if(sim && par == false){
							orderList.get(i).setIsParOrSim("1-0");
						} else if (sim == false && par){
							orderList.get(i).setIsParOrSim("0-1");
						}
						//goodsListAll.add(goodsList);
						//新的订单商品信息
						neworderGoodsDTOList(goodsList);
						goodsListAll.add(goodsList);
					}
				}
//				this.orderGoods = orderManager.getOrderGoods(uid);
			} else {
				return "error";
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			this.request.put("searchKey", orderDTO.getFindStr());
		} catch (Exception e) {
			e.printStackTrace();
			String errorTotle = "抱歉,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
		return SUCCESS;
	}
	private List<CtOrderGoods> goodsList = new ArrayList<CtOrderGoods>();
	
	private List<List<CtOrderGoods>> goodsListAll = new ArrayList<List<CtOrderGoods>>();
	private List<List<CtOrderGoodsDTO>> goodsListAllDTO = new ArrayList<List<CtOrderGoodsDTO>>();
	
	
	public List<List<CtOrderGoodsDTO>> getGoodsListAllDTO() {
		return goodsListAllDTO;
	}

	public void setGoodsListAllDTO(List<List<CtOrderGoodsDTO>> goodsListAllDTO) {
		this.goodsListAllDTO = goodsListAllDTO;
	}

	public List<List<CtOrderGoods>> getGoodsListAll() {
		return goodsListAll;
	}

	public void setGoodsListAll(List<List<CtOrderGoods>> goodsListAll) {
		this.goodsListAll = goodsListAll;
	}

	// 订单列表
	public List<CtOrderGoods> getGoodsList() {
		return goodsList;
	}

	public void setGoodsList(List<CtOrderGoods> goodsList) {
		this.goodsList = goodsList;
	}
	private List<CtOrderGoods> listGodsTest = new ArrayList<CtOrderGoods>();
	
	private List<OrderInfoGoodsDTO> orderInfoGoodsDTOList = new ArrayList<OrderInfoGoodsDTO>();
	
	
	public List<OrderInfoGoodsDTO> getOrderInfoGoodsDTOList() {
		return orderInfoGoodsDTOList;
	}

	public void setOrderInfoGoodsDTOList(
			List<OrderInfoGoodsDTO> orderInfoGoodsDTOList) {
		this.orderInfoGoodsDTOList = orderInfoGoodsDTOList;
	}

	public List<CtOrderGoods> getListGodsTest() {
		return listGodsTest;
	}

	public void setListGodsTest(List<CtOrderGoods> listGodsTest) {
		this.listGodsTest = listGodsTest;
	}

	private OrderListDTO orderListDTO = new OrderListDTO();
	
	public OrderListDTO getOrderListDTO() {
		return orderListDTO;
	}

	public void setOrderListDTO(OrderListDTO orderListDTO) {
		this.orderListDTO = orderListDTO;
	}

	private List<OrderListDTO> orderListDTOList = new ArrayList<OrderListDTO>();
	
	public List<OrderListDTO> getOrderListDTOList() {
		return orderListDTOList;
	}

	public void setOrderListDTOList(List<OrderListDTO> orderListDTOList) {
		this.orderListDTOList = orderListDTOList;
	}

	
	//信用支付订单列表
	public String xinyongOrderInfo(){
		try {
			this.cateList = basicManager.queryAllCate();
			this.catenumList = basicManager.cateCount();
			this.subcatenumList = basicManager.subcateCount();
			session.put("cateList", cateList);
			String temp = "0";
			boolean sim = false;
			boolean par = false;
			// 验证登录
			Long uid = null;
			if (this.session.get("userinfosession") != null) {
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
				page = new Page();
				if (orderDTO.getPage() == 0) {
					orderDTO.setPage(1);
				}
				page.setCurrentPage(orderDTO.getPage());
				page.setPageSize(8);
				this.request.put("URemainingAmount", cu.getURemainingAmount());
				String shoppType = orderDTO.getShippingType();
				int yi = 0;
				int wei = 0;
				if(shoppType != null){
					if(shoppType.equals("2")){
						yi = 5;
					}
					if(shoppType.equals("1")){
						yi = 6;
					}
					wei = 0;
				} else {
					yi = 5;
					wei = 6;
				}
				
				this.orderList = orderManager.getOrderListByUIdAndXingYong(uid, page, yi, wei);
				session.put("time", "");
				session.put("status", "all");
				Double priceFen = 0D;
				Double priceFenSim = 0D;
				for (int i = 0; i < orderList.size(); i++) {
					orderInfoGoodsDTOList = new ArrayList<OrderInfoGoodsDTO>();
					ctEvaluation = orderManager.getEvaByOrderId(orderList.get(i).getOrderId());
					orderListDTO = new OrderListDTO();
					if(ctEvaluation != null){
						orderList.get(i).setEvaDesc(ctEvaluation.getEvaDesc());
						orderList.get(i).setEvaTime(ctEvaluation.getEvaTime());
					}
					if(orderList.get(i).getTotal().indexOf(".") > 0){
						String[] pricess = orderList.get(i).getTotal().split("\\.");
						if(pricess[1].length()>4){
							pricess[1] = pricess[1].substring(0,4);
							orderList.get(i).setTotal(pricess[0] + "." + pricess[1]);
						}
					}
					par = false;
					sim = false;
					goodsList = orderManager.getOrderGoodsByOrderId(orderList.get(i).getOrderId());
					//新版订单数据整合
					newOrderInfoDTOData(i);
					for (int j = 0; j < goodsList.size(); j++) {
						
						if(goodsList.get(j).getIsParOrSim().equals("0")){
							par = true;
							//Double goodsPrice = Double.valueOf(goodsList.get(j).getGParPrice()) * Double.valueOf(goodsList.get(j).getGParNumber());
							Double goodsPrice = multiply(Double.valueOf(goodsList.get(j).getGParPrice()),Double.valueOf(goodsList.get(j).getGParNumber()));
							String price = "";
							if(goodsPrice.toString().indexOf(".") > 0){
								String[] pricess = goodsPrice.toString().split("\\.");
								if(pricess[1].length()>4){
									pricess[1] = pricess[1].substring(0,4);
									price = pricess[0] + "." + pricess[1];
								} else {
									price = goodsPrice.toString();
								}
							} else {
								price = goodsPrice.toString();
							}
							goodsList.get(j).setGSubtotal(price);
						} else {
							sim = true;
							//Double goodsPrice = Double.valueOf(goodsList.get(j).getGPrice()) * Double.valueOf(goodsList.get(j).getGNumber());
							Double goodsPrice = multiply(Double.valueOf(goodsList.get(j).getGPrice()),Double.valueOf(goodsList.get(j).getGNumber()));
							String price = "";
							if(goodsPrice.toString().indexOf(".") > 0){
								String[] pricess = goodsPrice.toString().split("\\.");
								if(pricess[1].length()>4){
									pricess[1] = pricess[1].substring(0,4);
									price = pricess[0] + "." + pricess[1];
								} else {
									price = goodsPrice.toString();
								}
							} else {
								price = goodsPrice.toString();
							}
							goodsList.get(j).setGSubtotal(price);
						}
						
						if(goodsList.get(j).getIsParOrSim().equals("0")){
							priceFen += Double.valueOf(goodsList.get(j).getGSubtotal());
							goodsList.get(j).setPriceAllDan(priceFen.toString());
						}
						if(goodsList.get(j).getIsParOrSim().equals("1")){
							priceFenSim += Double.valueOf(goodsList.get(j).getGSubtotal());
							goodsList.get(j).setPriceAllDanSim(priceFenSim.toString());
						}
						
						listGodsTest.add(goodsList.get(j));
					}
					priceFenSim = 0D;
					priceFen = 0D;
					if(sim && par){
						orderList.get(i).setIsParOrSim("1-1");
					} else if(sim && par == false){
						orderList.get(i).setIsParOrSim("1-0");
					} else if (sim == false && par){
						orderList.get(i).setIsParOrSim("0-1");
					}
					//新的订单商品信息
					neworderGoodsDTOList(goodsList);
					goodsListAll.add(goodsList);
				}
				this.orderGoods = orderManager.getOrderGoods(uid);
			} else {
				return "error";
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
		} catch (Exception e) {
			e.printStackTrace();
			String errorTotle = "抱歉,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
		return SUCCESS;
	}
	
	//订单列表
	public String orderList() {
		try {
			this.cateList = basicManager.queryAllCate();
			this.catenumList = basicManager.cateCount();
			this.subcatenumList = basicManager.subcateCount();
			session.put("cateList", cateList);
			String temp = "0";
			boolean sim = false;
			boolean par = false;
			// 验证登录
			Long uid = null;
			if (this.session.get("userinfosession") != null) {
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
				page = new Page();
				if (orderDTO.getPage() == 0) {
					orderDTO.setPage(1);
				}
				page.setCurrentPage(orderDTO.getPage());
				page.setPageSize(8);
				this.orderList = orderManager.getOrderListByUId(uid, page);
				session.put("time", "");
				session.put("status", "all");
				Double priceFen = 0D;
				Double priceFenSim = 0D;
				for (int i = 0; i < orderList.size(); i++) {
					orderInfoGoodsDTOList = new ArrayList<OrderInfoGoodsDTO>();
					ctEvaluation = orderManager.getEvaByOrderId(orderList.get(i).getOrderId());
					orderListDTO = new OrderListDTO();
					if(ctEvaluation != null){
						orderList.get(i).setEvaDesc(ctEvaluation.getEvaDesc());
						orderList.get(i).setEvaTime(ctEvaluation.getEvaTime());
					}
					if(orderList.get(i).getTotal().indexOf(".") > 0){
						String[] pricess = orderList.get(i).getTotal().split("\\.");
						if(pricess[1].length()>4){
							pricess[1] = pricess[1].substring(0,4);
							orderList.get(i).setTotal(pricess[0] + "." + pricess[1]);
						}
					}
					par = false;
					sim = false;
					goodsList = orderManager.getOrderGoodsByOrderId(orderList.get(i).getOrderId());
					//新版订单数据整合
					newOrderInfoDTOData(i);
					for (int j = 0; j < goodsList.size(); j++) {
						
						if(goodsList.get(j).getIsParOrSim().equals("0")){
							par = true;
							//Double goodsPrice = Double.valueOf(goodsList.get(j).getGParPrice()) * Double.valueOf(goodsList.get(j).getGParNumber());
							Double goodsPrice = multiply(Double.valueOf(goodsList.get(j).getGParPrice()),Double.valueOf(goodsList.get(j).getGParNumber()));
							String price = "";
							if(goodsPrice.toString().indexOf(".") > 0){
								String[] pricess = goodsPrice.toString().split("\\.");
								if(pricess[1].length()>4){
									pricess[1] = pricess[1].substring(0,4);
									price = pricess[0] + "." + pricess[1];
								} else {
									price = goodsPrice.toString();
								}
							} else {
								price = goodsPrice.toString();
							}
							goodsList.get(j).setGSubtotal(price);
						} else {
							sim = true;
							//Double goodsPrice = Double.valueOf(goodsList.get(j).getGPrice()) * Double.valueOf(goodsList.get(j).getGNumber());
							Double goodsPrice = multiply(Double.valueOf(goodsList.get(j).getGPrice()),Double.valueOf(goodsList.get(j).getGNumber()));
							String price = "";
							if(goodsPrice.toString().indexOf(".") > 0){
								String[] pricess = goodsPrice.toString().split("\\.");
								if(pricess[1].length()>4){
									pricess[1] = pricess[1].substring(0,4);
									price = pricess[0] + "." + pricess[1];
								} else {
									price = goodsPrice.toString();
								}
							} else {
								price = goodsPrice.toString();
							}
							goodsList.get(j).setGSubtotal(price);
						}
						
						if(goodsList.get(j).getIsParOrSim().equals("0")){
							priceFen += Double.valueOf(goodsList.get(j).getGSubtotal());
							goodsList.get(j).setPriceAllDan(priceFen.toString());
						}
						if(goodsList.get(j).getIsParOrSim().equals("1")){
							priceFenSim += Double.valueOf(goodsList.get(j).getGSubtotal());
							goodsList.get(j).setPriceAllDanSim(priceFenSim.toString());
						}
						
						listGodsTest.add(goodsList.get(j));
					}
					priceFenSim = 0D;
					priceFen = 0D;
					if(sim && par){
						orderList.get(i).setIsParOrSim("1-1");
					} else if(sim && par == false){
						orderList.get(i).setIsParOrSim("1-0");
					} else if (sim == false && par){
						orderList.get(i).setIsParOrSim("0-1");
					}
					//新的订单商品信息
					neworderGoodsDTOList(goodsList);
					goodsListAll.add(goodsList);
				}
				this.orderGoods = orderManager.getOrderGoods(uid);
			} else {
				return "error";
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
		} catch (Exception e) {
			e.printStackTrace();
			String errorTotle = "抱歉,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
		return SUCCESS;
	}

	private void newOrderInfoDTODataDetails(List<CtOrderInfo> orderList,List<CtOrderGoods> goodsList, int i) {
		List<CtOrderGoods> orderGoodsTempList = new ArrayList<CtOrderGoods>();
		orderInfoGoodsDTOList = new ArrayList<OrderInfoGoodsDTO>();
		orderListDTO = new OrderListDTO();
		for (int j = 0; j < goodsList.size(); j++) {
			orderGoodsTempList.add(goodsList.get(j));
		}
		for (int k = 0; k < orderGoodsTempList.size(); k++)  //外循环是循环的次数
		{
			for (int j = orderGoodsTempList.size() - 1 ; j > k; j--)  //内循环是 外循环一次比较的次数
			{
				
				if (orderGoodsTempList.get(k).getGId().equals(orderGoodsTempList.get(j).getGId()))
				{
					orderGoodsTempList.remove(j);
				}
				
			}
		}
		for (int j = 0; j < orderGoodsTempList.size(); j++) {
			OrderInfoGoodsDTO goodsDTO = new OrderInfoGoodsDTO();
			for (int k = 0; k < goodsList.size(); k++) {
				if(goodsList.get(k).getGId().toString().equals(orderGoodsTempList.get(j).getGId().toString())){
					goodsDTO.setGoodsId(goodsList.get(k).getGId().intValue());
					goodsDTO.setGoodsName(goodsList.get(k).getCtGoods().getGName());
					if(goodsList.get(k).getIsParOrSim().equals("1")){
						goodsDTO.setSimNum(goodsList.get(k).getGNumber());
						goodsDTO.setSimPrice(goodsList.get(k).getGPrice());
						goodsDTO.setSimPriceCount(goodsList.get(k).getGSubtotal());
					} else {
						goodsDTO.setParNum(goodsList.get(k).getGParNumber()/1000+"");
						goodsDTO.setParPrice(goodsList.get(k).getGParPrice());
						goodsDTO.setParPriceCount(goodsList.get(k).getGSubtotal());
					}
				}
			}
			
			orderInfoGoodsDTOList.add(goodsDTO);
		}
		Integer allNum = 0;
		String allPrice = "0.00";
		for (int j = 0; j < orderInfoGoodsDTOList.size(); j++) {
			allNum = Integer.valueOf(orderInfoGoodsDTOList.get(j).getSimNum()) + Integer.valueOf(orderInfoGoodsDTOList.get(j).getParNum())*1000;
			orderInfoGoodsDTOList.get(j).setAllCountNum(allNum);
			BigDecimal b1=new BigDecimal(orderInfoGoodsDTOList.get(j).getSimPriceCount());
			BigDecimal b2=new BigDecimal(orderInfoGoodsDTOList.get(j).getParPriceCount());
			BigDecimal b3=new BigDecimal(allPrice);
			allPrice = b3.add(b1.add(b2)).toString();
		}
		BigDecimal b1=new BigDecimal(orderListDTO.getGoodsAllPrice());
		BigDecimal b2=new BigDecimal(allPrice);
		orderListDTO.setGoodsAllPrice(b1.add(b2).toString());
		orderListDTO.setOrderInfoGoodsDTOList(orderInfoGoodsDTOList);
		orderListDTO.setOrderId(orderList.get(i).getOrderId());
		orderListDTO.setOrderSn(orderList.get(i).getOrderSn());
		orderListDTO.setOrderStatus(Integer.valueOf(orderList.get(i).getOrderStatus()));
		orderListDTO.setTotal(orderList.get(i).getTotal());
		orderListDTO.setOrderTime(orderList.get(i).getOrderTime());
		
		orderListDTO.setRetTime(orderList.get(i).getRetTime());
		orderListDTO.setRetStatus(orderList.get(i).getRetStatus());
		orderListDTO.setEvaId(orderList.get(i).getEvaId() == null ? null : orderList.get(i).getEvaId().intValue());
		orderListDTO.setEvaDesc(orderList.get(i).getEvaDesc());
		orderListDTO.setEvaTime(orderList.get(i).getEvaTime() == null ? null : orderList.get(i).getEvaTime().split(" ")[0]);
		
		if(orderList.get(i).getOrderStatus().equals("1") || orderList.get(i).getOrderStatus().equals("3")|| orderList.get(i).getOrderStatus().equals("4")|| orderList.get(i).getOrderStatus().equals("5")|| orderList.get(i).getOrderStatus().equals("8")){
			String trueTotal = orderManager.findPayTotalByOrderSn(orderList.get(i).getOrderSn());
			if(trueTotal != null){
				orderListDTO.setTrueTotal(trueTotal);
			}
		}
		orderListDTO.setFreight(orderList.get(i).getFreight());
		if(orderList.get(i).getShippingType().equals("2") || orderList.get(i).getShippingType().equals("3")){
			if(orderList.get(i).getShoppingTypeCollect()){
				orderListDTO.setFreight("0");
				orderListDTO.setFerightDaoFu("到付");
			} else {
				orderListDTO.setFreight(orderList.get(i).getFreight());
			}
		}
		orderListDTO.setGoodsSize(orderListDTO.getOrderInfoGoodsDTOList().size());
		orderListDTOList.add(orderListDTO);
	}
	private void newOrderInfoDTOData(int i) throws UnsupportedEncodingException {
		List<CtOrderGoods> orderGoodsTempList = new ArrayList<CtOrderGoods>();
		orderInfoGoodsDTOList = new ArrayList<OrderInfoGoodsDTO>();
		orderListDTO = new OrderListDTO();
		for (int j = 0; j < goodsList.size(); j++) {
			orderGoodsTempList.add(goodsList.get(j));
		}
		for (int k = 0; k < orderGoodsTempList.size(); k++)  //外循环是循环的次数
		{
		    for (int j = orderGoodsTempList.size() - 1 ; j > k; j--)  //内循环是 外循环一次比较的次数
		    {

		        if (orderGoodsTempList.get(k).getGId().equals(orderGoodsTempList.get(j).getGId()))
		        {
		        	orderGoodsTempList.remove(j);
		        }

		    }
		}
		for (int j = 0; j < orderGoodsTempList.size(); j++) {
			OrderInfoGoodsDTO goodsDTO = new OrderInfoGoodsDTO();
			for (int k = 0; k < goodsList.size(); k++) {
				if(goodsList.get(k).getGId().toString().equals(orderGoodsTempList.get(j).getGId().toString())){
					goodsDTO.setGoodsId(goodsList.get(k).getGId().intValue());
					goodsDTO.setGoodsName(goodsList.get(k).getCtGoods().getGName());
					if(goodsList.get(k).getIsParOrSim().equals("1")){
						goodsDTO.setSimNum(goodsList.get(k).getGNumber());
						goodsDTO.setSimPrice(goodsList.get(k).getGPrice());
						goodsDTO.setSimPriceCount(goodsList.get(k).getGSubtotal());
						goodsDTO.setIsRefundSim(goodsList.get(k).getIsRefund());
					} else {
						goodsDTO.setParNum(goodsList.get(k).getGParNumber()/1000+"");
						goodsDTO.setParPrice(goodsList.get(k).getGParPrice());
						goodsDTO.setParPriceCount(goodsList.get(k).getGSubtotal());
						goodsDTO.setIsRefundPar(goodsList.get(k).getIsRefund());
					}
				}
			}
			orderInfoGoodsDTOList.add(goodsDTO);
		}
		Integer allNum = 0;
		String allPrice = "0.00";
		for (int j = 0; j < orderInfoGoodsDTOList.size(); j++) {
			allNum = Integer.valueOf(orderInfoGoodsDTOList.get(j).getSimNum()) + Integer.valueOf(orderInfoGoodsDTOList.get(j).getParNum())*1000;
			orderInfoGoodsDTOList.get(j).setAllCountNum(allNum);
			BigDecimal b1=new BigDecimal(orderInfoGoodsDTOList.get(j).getSimPriceCount());
	        BigDecimal b2=new BigDecimal(orderInfoGoodsDTOList.get(j).getParPriceCount());
	        BigDecimal b3=new BigDecimal(allPrice);
	        allPrice = b3.add(b1.add(b2)).toString();
		}
		BigDecimal b1=new BigDecimal(orderListDTO.getGoodsAllPrice());
        BigDecimal b2=new BigDecimal(allPrice);
		orderListDTO.setGoodsAllPrice(b1.add(b2).toString());
		orderListDTO.setOrderInfoGoodsDTOList(orderInfoGoodsDTOList);
		orderListDTO.setOrderId(orderList.get(i).getOrderId());
		orderListDTO.setOrderSn(orderList.get(i).getOrderSn());
		orderListDTO.setOrderStatus(Integer.valueOf(orderList.get(i).getOrderStatus()));
		orderListDTO.setTotal(orderList.get(i).getTotal());
		orderListDTO.setOrderTime(orderList.get(i).getOrderTime());
		
		orderListDTO.setRetTime(orderList.get(i).getRetTime());
		orderListDTO.setRetStatus(orderList.get(i).getRetStatus());
		orderListDTO.setEvaId(orderList.get(i).getEvaId() == null ? null : orderList.get(i).getEvaId().intValue());
		orderListDTO.setEvaDesc(orderList.get(i).getEvaDesc());
		orderListDTO.setEvaTime(orderList.get(i).getEvaTime() == null ? null : orderList.get(i).getEvaTime().split(" ")[0]);
		orderListDTO.setPay(orderList.get(i).getPay());
		if(orderList.get(i).getOrderStatus().equals("1") || orderList.get(i).getOrderStatus().equals("3")|| orderList.get(i).getOrderStatus().equals("4")|| orderList.get(i).getOrderStatus().equals("5")|| orderList.get(i).getOrderStatus().equals("8")){
			String trueTotal = orderManager.findPayTotalByOrderSn(orderList.get(i).getOrderSn());
			if(trueTotal != null){
				orderListDTO.setTrueTotal(trueTotal);
			}
		}
		orderListDTO.setFreight(orderList.get(i).getFreight());
		if(orderList.get(i).getShippingType().equals("2") || orderList.get(i).getShippingType().equals("3")){
			if(orderList.get(i).getShoppingTypeCollect()){
				orderListDTO.setFreight("0");
				orderListDTO.setFerightDaoFu("到付");
			} else {
				orderListDTO.setFreight(orderList.get(i).getFreight());
			}
		}
		orderListDTO.setGoodsSize(orderListDTO.getOrderInfoGoodsDTOList().size());
		List<ExDescInfoDTO> descInfoDTOsList = new ArrayList<ExDescInfoDTO>();
		if(!orderList.get(i).getShippingType().equals("4") && orderList.get(i).getExId() != null){
			express = orderManager.getExpByExId(orderList.get(i).getExId());
			String content = kuaidi100.kuaiDi100New(CODEKEY, express.getExDesc(), orderList.get(i).getExNo(), "3", "1", "desc");
			if(content != null){
				String[] chai = content.split("\n");
				for (int j = 0; j < chai.length; j++) {
					ExDescInfoDTO ex = new ExDescInfoDTO();
					String[] c = chai[j].split(" ");
					if(c.length < 2){
						String time = "";
						String desc = content;
						ex.setDesc(desc);
						ex.setTime(time);
						descInfoDTOsList.add(ex);
					} else {
						String time = c[0]+" "+c[1];
						String desc = chai[j].replaceAll(time, "").trim();
						ex.setDesc(desc);
						ex.setTime(time);
						descInfoDTOsList.add(ex);
					}
				}
			}
			orderListDTO.setExName(express.getExName());
			orderListDTO.setExNo(orderList.get(i).getExNo());
		}
		orderListDTO.setShoptype(orderList.get(i).getShippingType());
		orderListDTO.setDescInfoDTOsList(descInfoDTOsList);
		orderListDTOList.add(orderListDTO);
	}
	
	public static double multiply(double v1, double v2) {  
	    BigDecimal b1 = new BigDecimal(Double.toString(v1));  
	    BigDecimal b2 = new BigDecimal(Double.toString(v2));  
	    return b1.multiply(b2).doubleValue();  
	}
	private void neworderGoodsDTOList(List<CtOrderGoods> goodsList) {
		List<CtOrderGoodsDTO> tempGoodsListDTO = new ArrayList<CtOrderGoodsDTO>();
		for (int k = 0; k < goodsList.size(); k++) {
			CtOrderGoodsDTO dto = setOrderGoodsDTO(k,goodsList);
			tempGoodsListDTO.add(dto);
		}
		goodsListAllDTO.add(tempGoodsListDTO);
	}

	private CtOrderGoodsDTO setOrderGoodsDTO(int k, List<CtOrderGoods> goodsList) {
		CtOrderGoodsDTO dto = new CtOrderGoodsDTO();
		dto.setCartType(goodsList.get(k).getCartType());
		dto.setCouponId(goodsList.get(k).getCouponId());
		dto.setCtGoods(goodsList.get(k).getCtGoods());
		dto.setGId(goodsList.get(k).getGId());
		dto.setGNumber(goodsList.get(k).getGNumber());
		
		Long newParNum = goodsList.get(k).getGParNumber();
		Double price = Double.valueOf(newParNum == null ? "0": newParNum.toString()) / 1000;
		dto.setGParNumber(df.format(price));
		
		dto.setGParPrice(goodsList.get(k).getGParPrice());
		dto.setGPrice(goodsList.get(k).getGPrice());
		dto.setGSubtotal(goodsList.get(k).getGSubtotal());
		dto.setIsParOrSim(goodsList.get(k).getIsParOrSim());
		dto.setIsRefund(goodsList.get(k).getIsRefund());
		dto.setOrderGoodsId(goodsList.get(k).getOrderGoodsId());
		dto.setOrderId(goodsList.get(k).getOrderId());
		dto.setOrderInfo(goodsList.get(k).getOrderInfo());
		dto.setPack(goodsList.get(k).getPack());
		dto.setPriceAllDan(goodsList.get(k).getPriceAllDan());
		dto.setPriceAllDanSim(goodsList.get(k).getPriceAllDanSim());
		return dto;
	}

	// 按时间筛选订单
	public String orderListByTime() {
		try {
			
			this.cateList = basicManager.queryAllCate();
			this.catenumList = basicManager.cateCount();
			this.subcatenumList = basicManager.subcateCount();
			boolean sim = false;
			boolean par = false;
			// 验证登录
			Long uid = null;
			if (this.session.get("userinfosession") != null) {
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
				page = new Page();
				if (orderDTO.getPage() == 0) {
					orderDTO.setPage(1);
				}
				page.setPageSize(8);
				page.setCurrentPage(orderDTO.getPage());
				String time = orderDTO.getTime();
				String status = orderDTO.getStatus();
				
				this.orderList = orderManager.getOrderListByTime(uid, page, time, status);
				session.put("time", time);
				session.put("status", status);
				Double priceFen = 0D;
				Double priceFenSim = 0D;
				for (int i = 0; i < orderList.size(); i++) {
					ctEvaluation = orderManager.getEvaByOrderId(orderList.get(i).getOrderId());
					if(ctEvaluation != null){
						orderList.get(i).setEvaDesc(ctEvaluation.getEvaDesc());
						orderList.get(i).setEvaTime(ctEvaluation.getEvaTime());
					}
					if(orderList.get(i).getTotal().indexOf(".") > 0){
						String[] pricess = orderList.get(i).getTotal().split("\\.");
						if(pricess[1].length()>4){
							pricess[1] = pricess[1].substring(0,4);
							orderList.get(i).setTotal(pricess[0] + "." + pricess[1]);
						}
					}
					par = false;
					sim = false;
					goodsList = orderManager.getOrderGoodsByOrderId(orderList.get(i).getOrderId());
					//新版订单数据整合
					newOrderInfoDTOData(i);
					for (int j = 0; j < goodsList.size(); j++) {
						
						if(goodsList.get(j).getIsParOrSim().equals("0")){
							par = true;
							Double goodsPrice = Double.valueOf(goodsList.get(j).getGParPrice()) * Double.valueOf(goodsList.get(j).getGParNumber());
							String price = "";
							if(goodsPrice.toString().indexOf(".") > 0){
								String[] pricess = goodsPrice.toString().split("\\.");
								if(pricess[1].length()>4){
									pricess[1] = pricess[1].substring(0,4);
									price = pricess[0] + "." + pricess[1];
								} else {
									price = goodsPrice.toString();
								}
							} else {
								price = goodsPrice.toString();
							}
							goodsList.get(j).setGSubtotal(price);
						} else {
							sim = true;
							Double goodsPrice = Double.valueOf(goodsList.get(j).getGPrice()) * Double.valueOf(goodsList.get(j).getGNumber());
							String price = "";
							if(goodsPrice.toString().indexOf(".") > 0){
								String[] pricess = goodsPrice.toString().split("\\.");
								if(pricess[1].length()>4){
									pricess[1] = pricess[1].substring(0,4);
									price = pricess[0] + "." + pricess[1];
								} else {
									price = goodsPrice.toString();
								}
							} else {
								price = goodsPrice.toString();
							}
							goodsList.get(j).setGSubtotal(price);
						}
						
						if(goodsList.get(j).getIsParOrSim().equals("0")){
							priceFen += Double.valueOf(goodsList.get(j).getGSubtotal());
							goodsList.get(j).setPriceAllDan(priceFen.toString());
						}
						if(goodsList.get(j).getIsParOrSim().equals("1")){
							priceFenSim += Double.valueOf(goodsList.get(j).getGSubtotal());
							goodsList.get(j).setPriceAllDanSim(priceFenSim.toString());
						}
						listGodsTest.add(goodsList.get(j));
					}
					priceFenSim = 0D;
					priceFen = 0D;
					if(sim && par){
						orderList.get(i).setIsParOrSim("1-1");
					} else if(sim && par == false){
						orderList.get(i).setIsParOrSim("1-0");
					} else if (sim == false && par){
						orderList.get(i).setIsParOrSim("0-1");
					}
					neworderGoodsDTOList(goodsList);
					goodsListAll.add(goodsList);
				}
				this.orderGoods = orderManager.getOrderGoods(uid);
			} else {
				return "error";
			}
			System.out.println("时间筛选订单");
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
		} catch (Exception e) {
			//e.printStackTrace();
			String errorTotle = "抱歉,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
		return SUCCESS;
	}

	// 按状态筛选订单
	public String orderListByStatus() {
		try {
			this.cateList = basicManager.queryAllCate();
			this.catenumList = basicManager.cateCount();
			this.subcatenumList = basicManager.subcateCount();
			boolean sim = false;
			boolean par = false;
			// 验证登录
			Long uid = null;
			if (this.session.get("userinfosession") != null) {
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
				page = new Page();
				if (orderDTO.getPage() == 0) {
					orderDTO.setPage(1);
				}
				page.setPageSize(8);
				page.setCurrentPage(orderDTO.getPage());
				String status = orderDTO.getStatus();
				String time = orderDTO.getTime();
				this.orderList = orderManager.getOrderListByStatus(uid, page,time,
						status);
				session.put("time", time);
				session.put("status", status);
				Double priceFen = 0D;
				Double priceFenSim = 0D;
				for (int i = 0; i < orderList.size(); i++) {
					ctEvaluation = orderManager.getEvaByOrderId(orderList.get(i).getOrderId());
					if(ctEvaluation != null){
						orderList.get(i).setEvaDesc(ctEvaluation.getEvaDesc());
						orderList.get(i).setEvaTime(ctEvaluation.getEvaTime());
					}
					if(orderList.get(i).getTotal().indexOf(".") > 0){
						String[] pricess = orderList.get(i).getTotal().split("\\.");
						if(pricess[1].length()>2){
							pricess[1] = pricess[1].substring(0,2);
							orderList.get(i).setTotal(pricess[0] + "." + pricess[1]);
						}
					}
					par = false;
					sim = false;
					goodsList = orderManager.getOrderGoodsByOrderId(orderList.get(i).getOrderId());
					//新版订单数据整合
					newOrderInfoDTOData(i);
					for (int j = 0; j < goodsList.size(); j++) {
						
						if(goodsList.get(j).getIsParOrSim().equals("0")){
							par = true;
							Double goodsPrice = Double.valueOf(goodsList.get(j).getGParPrice()) * Double.valueOf(goodsList.get(j).getGParNumber());
							String price = "";
							if(goodsPrice.toString().indexOf(".") > 0){
								String[] pricess = goodsPrice.toString().split("\\.");
								if(pricess[1].length()>2){
									pricess[1] = pricess[1].substring(0,2);
									price = pricess[0] + "." + pricess[1];
								} else {
									price = goodsPrice.toString();
								}
							} else {
								price = goodsPrice.toString();
							}
							goodsList.get(j).setGSubtotal(price);
						} else {
							sim = true;
							Double goodsPrice = Double.valueOf(goodsList.get(j).getGPrice()) * Double.valueOf(goodsList.get(j).getGNumber());
							String price = "";
							if(goodsPrice.toString().indexOf(".") > 0){
								String[] pricess = goodsPrice.toString().split("\\.");
								if(pricess[1].length()>2){
									pricess[1] = pricess[1].substring(0,2);
									price = pricess[0] + "." + pricess[1];
								} else {
									price = goodsPrice.toString();
								}
							} else {
								price = goodsPrice.toString();
							}
							goodsList.get(j).setGSubtotal(price);
						}
						
						if(goodsList.get(j).getIsParOrSim().equals("0")){
							priceFen += Double.valueOf(goodsList.get(j).getGSubtotal());
							goodsList.get(j).setPriceAllDan(priceFen.toString());
						}
						if(goodsList.get(j).getIsParOrSim().equals("1")){
							priceFenSim += Double.valueOf(goodsList.get(j).getGSubtotal());
							goodsList.get(j).setPriceAllDanSim(priceFenSim.toString());
						}
						listGodsTest.add(goodsList.get(j));
					}
					priceFenSim = 0D;
					priceFen = 0D;
					if(sim && par){
						orderList.get(i).setIsParOrSim("1-1");
					} else if(sim && par == false){
						orderList.get(i).setIsParOrSim("1-0");
					} else if (sim == false && par){
						orderList.get(i).setIsParOrSim("0-1");
					}
					neworderGoodsDTOList(goodsList);
					goodsListAll.add(goodsList);
				}
				System.out.println("时间筛选订单1");
				this.orderGoods = orderManager.getOrderGoods(uid);
			} else {
				return "error";
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
		} catch (Exception e) {
			//e.printStackTrace();
			String errorTotle = "抱歉,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
		return SUCCESS;
	}

	// 更改订单状态
	public String changeStatus() {
		Long orderid = orderDTO.getOrderid();
		String status = "6";
		orderInfo = orderManager.getOrderByOrderId(orderid);
		if(orderInfo.getOrderStatus().equals("1") || orderInfo.getOrderStatus().equals("2") || orderInfo.getOrderStatus().equals("3")){
			orderManager.saveRefundCancelOrder(orderInfo);
		}
		if(orderInfo.getCouponId() != null){
			int index = orderInfo.getCouponId().indexOf(",");
			if(index != -1){
				//使用多个优惠券
				String[] couIds = orderInfo.getCouponId().split(",");
				for (int i = 0; i < couIds.length; i++) {
					CtCouponDetail ccd = orderManager.getCcdByCcdId(couIds[i]);
					ccd.setUId(Long.valueOf(orderInfo.getUId()));
					ccd.setUseTime("");
					ccd.setOrderId(null);
					ccd.setPrePrice(null);
					ccd.setIsUse("0");
					orderManager.updateCouponRes(ccd);
				}
			} else {
				//使用一个优惠券
				CtCouponDetail ccd = orderManager.getCcdByCcdId(orderInfo.getCouponId());
				ccd.setUId(Long.valueOf(orderInfo.getUId()));
				ccd.setUseTime("");
				ccd.setOrderId(null);
				ccd.setPrePrice(null);
				ccd.setIsUse("0");
				orderManager.updateCouponRes(ccd);
			}
		}
		orderManager.upChangeStatus(orderid, status);
		
		return SUCCESS;
	}

	
	
	@SuppressWarnings("unchecked")
	public String toPay(){
		if (this.session.get("userinfosession") != null) {
			CtUser cu = (CtUser) this.session.get("userinfosession");
			orderInfo = orderManager.getOrderByOrderSn(orderDTO.getOrderSn(),cu.getUId());
			banks = bankManager.findAll();
		} else {
			return ERROR;
		}
		return SUCCESS;
	}
	public String toPayXingyong(){
		if (this.session.get("userinfosession") != null) {
			CtUser cu = (CtUser) this.session.get("userinfosession");
			
			orderInfo = orderManager.getOrderByOrderSn(orderDTO.getOrderSn(),cu.getUId());
			
			if(orderInfo == null){
				orderInfo = new CtOrderInfo();
				List<CtOrderInfo> listOrderInfo = new ArrayList<CtOrderInfo>();
				Double d = 0D;
				String data = (String) session.get("oneKeyOrderSn");
				if(data != null){
					String orderSns[] = data.split(",");
					for (int i = 0; i < orderSns.length; i++) {
						CtOrderInfo info = orderManager.getOrderStatusByOrderSn(orderSns[i]);
						listOrderInfo.add(info);
						d = add(d.toString(), info.getTotal());
					}
				}
				String orderSn = (String) session.get("hkOrderSn");
				orderInfo.setOrderSn(orderSn);
				orderInfo.setTotal(d.toString());
			}
			
			banks = bankManager.findAll();
		} else {
			return ERROR;
		}
		return SUCCESS;
	}
	private CtPay ctPay = new CtPay();
	
	public CtPay getCtPay() {
		return ctPay;
	}

	public void setCtPay(CtPay ctPay) {
		this.ctPay = ctPay;
	}
	
	public String payUpdateXinyong() throws IOException{
		try {
			CtSystemInfo systemInfo = new CtSystemInfo();
			OSSClient client = new OSSClient(systemInfo.getEndpoint(),systemInfo.getAccess_id(),systemInfo.getAccess_key());
			//System.out.println(ctPay.getUrl1());	
			//fileUpload();
			OSSObjectSample sample = new OSSObjectSample();
			Date date = new Date();
			Long time = date.getTime();
			CtUser cu = (CtUser) this.session.get("userinfosession");
			String fileName = ctPay.getUpFileName();
			String key = systemInfo.getKey() + cu.getUId()+"_"+time+"_"+fileName.substring(fileName.indexOf("."), fileName.length());
			String filePath = ctPay.getFileName().getPath();
			//System.out.println(filePath);
			sample.uploadFile(client, systemInfo.getBucketName(), key, filePath);
			
			ctPay.setPayFile(systemInfo.getCtUFile()+"/"+key);
			ctPay.setPayDate(ctPay.getPayDate() + " "+ new SimpleDateFormat("HH:mm:ss").format(new Date()));
			ctPay.setUpFileName("");
			
			List<CtOrderInfo> listOrderInfo = new ArrayList<CtOrderInfo>();
			if(ctPay.getOrderId() == null){
				String data = (String) session.get("oneKeyOrderSn");
				if(data != null){
					String orderSns[] = data.split(",");
					for (int i = 0; i < orderSns.length; i++) {
						CtOrderInfo info = orderManager.getOrderStatusByOrderSn(orderSns[i]);
						listOrderInfo.add(info);
					}
				}
			} else {
				CtOrderInfo inf = orderManager.getOrderByOrderId(ctPay.getOrderId());
				listOrderInfo.add(inf);
			}
			CtOrderRelation relation = new CtOrderRelation();
			String paid = "";
			for (int i = 0; i < listOrderInfo.size(); i++) {
				CtPay pay = payManager.getPayChong(listOrderInfo.get(i).getOrderId());
				ctPay.setOrderId(listOrderInfo.get(i).getOrderId());
				if(pay == null){
					String res = payManager.saveOrderPay(ctPay);
					paid += res + ",";
					ctPay.setPayId(null);
					if(res.equals("Exerror")){
						String url = ctPay.getUrl();
						//System.out.println(url);
						String errorTotle = "抱歉，提交异常";
						String errorMess = "含非法字符";
						session.put("errorMess", errorMess);
						session.put("errorTotle", errorTotle);
						session.put("errorBackUrl", url);
						return ERROR;
					}
				}
				listOrderInfo.get(i).setPay("7");
				fgoodsManager.merge(listOrderInfo.get(i));
			}
			orderInfo = orderManager.getOrderByOrderId(ctPay.getOrderId());
			relation.setPayId(paid.substring(0, paid.length()-1));
			relation.setPayType("1");
			relation.setUId(Long.valueOf(orderInfo.getUId()));
			fgoodsManager.merge(relation);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			String errorTotle = "抱歉,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
		return SUCCESS;
	}

	public String payUpdate() throws IOException{
		try {
			CtSystemInfo systemInfo = new CtSystemInfo();
			OSSClient client = new OSSClient(systemInfo.getEndpoint(),systemInfo.getAccess_id(),systemInfo.getAccess_key());
			//System.out.println(ctPay.getUrl1());	
			//fileUpload();
			OSSObjectSample sample = new OSSObjectSample();
			Date date = new Date();
			Long time = date.getTime();
			CtUser cu = (CtUser) this.session.get("userinfosession");
			String fileName = ctPay.getUpFileName();
			String key = systemInfo.getKey() + cu.getUId()+"_"+time+"_"+fileName.substring(fileName.indexOf("."), fileName.length());
			String filePath = ctPay.getFileName().getPath();
			//System.out.println(filePath);
			sample.uploadFile(client, systemInfo.getBucketName(), key, filePath);
				ctPay.setPayFile(systemInfo.getCtUFile()+"/"+key);
				ctPay.setPayDate(ctPay.getPayDate() + " "+ new SimpleDateFormat("HH:mm:ss").format(new Date()));
				ctPay.setUpFileName("");
				CtPay pay = payManager.getPayChong(ctPay.getOrderId());
				if(pay == null){
					String res = payManager.saveOrderPay(ctPay);
					if(res.equals("Exerror")){
						String url = ctPay.getUrl();
						//System.out.println(url);
						String errorTotle = "抱歉，提交异常";
						String errorMess = "含非法字符";
						session.put("errorMess", errorMess);
						session.put("errorTotle", errorTotle);
						session.put("errorBackUrl", url);
						return ERROR;
					}
				}
				orderManager.upChangeStatus(ctPay.getOrderId(), "2");
				orderManager.tuPayOneOrderPayType(ctPay.getOrderId());
				orderInfo = orderManager.getOrderInfoByOrderSn(ctPay.getOrderSn());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			String errorTotle = "抱歉,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
		return SUCCESS;
	}
	//填写支付信息文件上传
	private void fileUpload() throws IOException {
		this.savePath = ServletActionContext.getServletContext().getRealPath("/payUpload");
		//System.out.println(savePath);
		BufferedInputStream bis = null;
		BufferedOutputStream bos = null;
		BufferedOutputStream bosWork = null;
		try {
//			File file1 = new File("d:\\update");
//			if(!file1.exists() && !file1.isDirectory()){
//				file1.mkdir();
//			}
			bis = new BufferedInputStream(new FileInputStream(up));
			bos = new BufferedOutputStream(new FileOutputStream(savePath+"//" + ctPay.getCName() + "-" + this.getUpFileName()));
//			bosWork = new BufferedOutputStream(new FileOutputStream("d:\\update"+"\\" + ctPay.getCName() + "-" + this.getUpFileName()));
			byte[] buffer = new byte[(int)up.length()];
			int len = 0;
			while ((len = bis.read(buffer)) != -1) {
				bos.write(buffer, 0, len);
//				bosWork.write(buffer, 0, len);
			}
		} catch (Exception e) {
			//e.printStackTrace();
		} finally{
			if(null != bis){
				bis.close();
			}
			if (null != bos){
				bos.close();
			}
			if (null != bosWork){
				bosWork.close();
			}
		}
	}
	//提交投诉文件上传
	private String fileUploadForComPlain() throws IOException {
		this.savePath = ServletActionContext.getServletContext().getRealPath("/${systemInfo.ctImgUrl }");
		String path = null;
		//System.out.println(savePath);
		BufferedInputStream bis = null;
		BufferedOutputStream bos = null;
		BufferedOutputStream bosWork = null;
		try {
//			File file1 = new File("d:\\updateForCom");
//			if(!file1.exists() && !file1.isDirectory()){
//				file1.mkdir();
//			}
			bis = new BufferedInputStream(new FileInputStream(up));
			path = savePath+"//" + orderInfo.getOrderSn() + "-" + this.getUpFileName();
			bos = new BufferedOutputStream(new FileOutputStream(path));
			//bosWork = new BufferedOutputStream(new FileOutputStream("d:\\updateForCom"+"\\" + orderInfo.getOrderSn() + "-" + this.getUpFileName()));
			byte[] buffer = new byte[(int)up.length()];
			int len = 0;
			while ((len = bis.read(buffer)) != -1) {
				bos.write(buffer, 0, len);
				//bosWork.write(buffer, 0, len);
			}
		} catch (Exception e) {
			//e.printStackTrace();
		} finally{
			if(null != bis){
				bis.close();
			}
			if (null != bos){
				bos.close();
			}
			if (null != bosWork){
				bosWork.close();
			}
		}
		return orderInfo.getOrderSn() + "-" + this.getUpFileName();
	}
	private CtExpress express = new CtExpress();
	public CtExpress getExpress() {
		return express;
	}

	public void setExpress(CtExpress express) {
		this.express = express;
	}
	private double total = 0;
	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}
	private List<CtOrderGoods> orderGoodslist;
	
	public List<CtOrderGoods> getOrderGoodslist() {
		return orderGoodslist;
	}

	public void setOrderGoodslist(List<CtOrderGoods> orderGoodslist) {
		this.orderGoodslist = orderGoodslist;
	}
	//快递查询工具类
	private WuLiu wuLiu = new WuLiu();
	//快递key
	private final static String CODEKEY = "3a4c64496dd5610f";
	public String orderDetails() throws UnsupportedEncodingException{
		try {
			DecimalFormat df =new DecimalFormat("0.00####");
			CtUser cu = (CtUser) this.session.get("userinfosession");
			if(cu == null){
				return "nologin";
			}
			orderInfo = orderManager.getOrderByOrderSn(orderDTO.getOrderSn(),cu.getUId());
			if(orderInfo == null){
				return "nologin";
			}
			if(orderInfo.getShippingType().equals("4")){
				CtShop shop = orderManager.getShopType4ById(orderInfo.getSId());
				session.put("shopType", shop);
			}
			if(orderInfo.getExId() != null && orderInfo.getExId() != 0){
				express = orderManager.getExpByExId(orderInfo.getExId());
				String content = kuaidi100.kuaiDi100(CODEKEY, express.getExDesc(), orderInfo.getExNo(), "2", "1", "desc");
				session.put("WuLiuFind", content);
				content = wuLiu.searchkuaiDiInfo(CODEKEY, express.getExDesc(), orderInfo.getExNo());
				session.put("WuLiuFindUrl", content);
			}
			boolean sim = false;
			boolean par = false;
			Double priceFen = 0D;
			Double priceFenSim = 0D;
			//新版订单数据整合
			orderList = new ArrayList<CtOrderInfo>();
			orderList.add(orderInfo);
			for (int j = 0; j < orderList.size(); j++) {
				goodsList = orderManager.getOrderGoodsByOrderId(orderList.get(j).getOrderId());
				newOrderInfoDTODataDetails(orderList, goodsList, j);
			}
			if(orderListDTOList.size() == 1){
				orderListDTO = orderListDTOList.get(0);
			}
			orderGoodslist = orderManager.getOrderGoodsByOrderId(orderInfo.getOrderId());
			
			//EDB
			//CtPayAction cp = new CtPayAction();
			//cp.edbTradeAdd(orderInfo, orderGoodslist,regionManager,orderManager);
			
			for (int j = 0; j < orderGoodslist.size(); j++) {
				
				if(orderGoodslist.get(j).getIsParOrSim().equals("0")){
					par = true;
					Double goodsPrice = Double.valueOf(orderGoodslist.get(j).getGParPrice()) * Double.valueOf(orderGoodslist.get(j).getGParNumber());
					String price = "";
					if(goodsPrice.toString().indexOf(".") > 0){
						String[] pricess = goodsPrice.toString().split("\\.");
						if(pricess[1].length()>4){
							pricess[1] = pricess[1].substring(0,4);
							price = pricess[0] + "." + pricess[1];
						} else {
							price = goodsPrice.toString();
						}
					} else {
						price = goodsPrice.toString();
					}
					price = df.format(goodsPrice);
					orderGoodslist.get(j).setGSubtotal(price);
				} else {
					sim = true;
					Double goodsPrice = Double.valueOf(orderGoodslist.get(j).getGPrice()) * Double.valueOf(orderGoodslist.get(j).getGNumber());
					String price = "";
					if(goodsPrice.toString().indexOf(".") > 0){
						String[] pricess = goodsPrice.toString().split("\\.");
						if(pricess[1].length()>4){
							pricess[1] = pricess[1].substring(0,4);
							price = pricess[0] + "." + pricess[1];
						} else {
							price = goodsPrice.toString();
						}
					} else {
						price = goodsPrice.toString();
					}
					
					price = df.format(goodsPrice);
					orderGoodslist.get(j).setGSubtotal(price);
				}
				
				if(orderGoodslist.get(j).getIsParOrSim().equals("0")){
					priceFen += Double.valueOf(orderGoodslist.get(j).getGSubtotal());
					orderGoodslist.get(j).setPriceAllDan(priceFen.toString());
				}
				if(orderGoodslist.get(j).getIsParOrSim().equals("1")){
					priceFenSim += Double.valueOf(orderGoodslist.get(j).getGSubtotal());
					orderGoodslist.get(j).setPriceAllDanSim(priceFenSim.toString());
				}
				listGodsTest.add(orderGoodslist.get(j));
			}
			priceFenSim = 0D;
			priceFen = 0D;
			if(sim && par){
				orderInfo.setIsParOrSim("1-1");
			} else if(sim && par == false){
				orderInfo.setIsParOrSim("1-0");
			} else if (sim == false && par){
				orderInfo.setIsParOrSim("0-1");
			}
			session.put("orderGoodslistSize", orderGoodslist.size());
			neworderGoodsDTOList(orderGoodslist);
			goodsListAll.add(orderGoodslist);
			for (int i = 0; i < orderGoodslist.size(); i++) {
				String price = orderGoodslist.get(i).getGSubtotal();
				total += Double.valueOf(price);
				String tot = df.format(total);
				session.put("total", tot);
			}
			String tel = orderInfo.getTel();
			if(tel != null && !(tel.trim().equals(""))){
				String telF = tel.substring(0, 3);
				String telE = tel.substring(tel.length()-3, tel.length());
				//System.out.println(telF + "=====" + telE);
				int lengths = tel.length()-6;
				String str = "";
				for (int i = 0; i < lengths; i++) {
					str += "*";
				}
				orderInfo.setTel(telF + str + telE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			String errorTotle = "抱歉,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
		
		return SUCCESS;
	}
	private CtEvaluation ctEvaluation;
	public CtEvaluation getCtEvaluation() {
		return ctEvaluation;
	}

	public void setCtEvaluation(CtEvaluation ctEvaluation) {
		this.ctEvaluation = ctEvaluation;
	}

	//添加评价
	public String orderPingjia(){
		CtUser cu = (CtUser) this.session.get("userinfosession");
		orderInfo = orderManager.getOrderByOrderSn(orderDTO.getOrderSn(),cu.getUId());
		ctEvaluation.setEvaDesc(ctEvaluation.getEvaDesc());
		ctEvaluation.setOrderId(orderInfo.getOrderId());
		ctEvaluation.setUId(cu.getUId());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		ctEvaluation.setEvaTime(sdf.format(new Date()));
		ctEvaluation = orderManager.saveEvaluation(ctEvaluation);
		session.put("ctEvaluation", ctEvaluation);
		orderManager.updateEvaIdByOrder(orderInfo.getOrderId(),ctEvaluation.getEvaId());
		orderInfo = orderManager.getOrderByOrderSn(orderDTO.getOrderSn(),cu.getUId());
		return SUCCESS;
	}
	
	//更新订单的状态-确认收货
	public String updateOrderStart(){
		
		orderManager.updateOrderStartBySn(orderDTO.getOrderSn());
		result = "确认收货成功";
		return SUCCESS;
	}
	//按照单号查询订单
	public String returnGoods(){
		CtUser cu = (CtUser) this.session.get("userinfosession");
		orderInfo = orderManager.getOrderByOrderSn(orderDTO.getOrderSn(),cu.getUId());
		return SUCCESS;
	}
	public String orderTkUpdate(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		orderInfo.setRetTime(sdf.format(new Date()));
		orderInfo.setRetStatus("提交申请");
		String res=orderManager.updateRetOrderByOrderSn(orderInfo);
		if(res.equals("Exerror")){
			String url = orderDTO.getUrl();
			//System.out.println(url);
			String errorTotle = "抱歉，提交异常";
			String errorMess = "含非法字符";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			session.put("errorBackUrl", url);
			return ERROR;
		}
		result = "提交申请成功";
		return SUCCESS;
	}
	private CtComplain complain;
	private List<CtComplain> complainList = new ArrayList<CtComplain>();
	//投诉- 查询该用户的投诉记录
	public String orderComplain(){
		CtUser cu = (CtUser) this.session.get("userinfosession");
		page = new Page();
		if (orderDTO.getPage() == 0) {
			orderDTO.setPage(1);
		}
		complainList = orderManager.getOrderComplainByUid(cu.getUId(),page);
		Integer totalNo = 0;
		Integer totalOk = 0;
		if(complainList != null && complainList.size() > 0){
			for (int i = 0; i < complainList.size(); i++) {
				if(complainList.get(i).getComStatus().equals("1")){
					totalNo++;
				}
				if(complainList.get(i).getComStatus().equals("2")){
					totalOk++;
				}
			}
		}
		page.setTotalPage(page.getTotalPage());
		this.request.put("pages", page);
		session.put("totalNo", totalNo);
		session.put("totalOk", totalOk);
		return SUCCESS;
	}
	//投诉- 查询是否有此订单 -- 异步
	public String orderIsChong(){
		CtUser cu = (CtUser) this.session.get("userinfosession");
		orderInfo = orderManager.getOrderByOrderSn(orderInfo.getOrderSn(),cu.getUId());
		if(orderInfo == null){
			result = "未找到此订单";
		}
		return SUCCESS;
	}
	//投诉- 添加投诉 
	public String upFileToComplain() throws IOException{
		
		try {
			CtUser cu = (CtUser) this.session.get("userinfosession");
			orderInfo = orderManager.getOrderByOrderSn(complain.getOrderInfo().getOrderSn(),cu.getUId());
			String fileName = fileUploadForComPlain();
			
			CtSystemInfo systemInfo = new CtSystemInfo();
			//创建OSS上传文件链接
			OSSClient client = new OSSClient(systemInfo.getEndpoint(),systemInfo.getAccess_id(),systemInfo.getAccess_key());
			OSSObjectSample sample = new OSSObjectSample();
			String key = systemInfo.getKey()+fileName;
			String filePath = up.getPath();
			
			sample.uploadFile(client, systemInfo.getBucketName(), key, filePath);
			
			complain.setOrderInfo(orderInfo);
			complain.setOrderId(orderInfo.getOrderId());
			complain.setUId(cu.getUId());
			complain.setComFile(fileName);
			complain.setComStatus("1");
			String res=orderManager.updateComplain(complain);
			if(res.equals("Exerror")){
				String url = orderDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
			}
		} catch (Exception e) {
			//e.printStackTrace();
			String errorTotle = "抱歉,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
		return SUCCESS;
	}
	//投诉- 投诉详情 
	public String orderComplainDetails() {
		CtUser cu = (CtUser) this.session.get("userinfosession");
		complain = orderManager.getComplainByComId(complain.getComId());
		complainList = orderManager.getOrderComplainByUid(cu.getUId(),null);
		Integer totalNo = 0;
		Integer totalOk = 0;
		if(complainList != null && complainList.size() > 0){
			for (int i = 0; i < complainList.size(); i++) {
				if(complainList.get(i).getComStatus().equals("1")){
					totalNo++;
				}
				if(complainList.get(i).getComStatus().equals("2")){
					totalOk++;
				}
			}
		}
		session.put("totalNo", totalNo);
		session.put("totalOk", totalOk);
		return SUCCESS;
	}
	
	
	//查看退换货记录
	public String orderReturnList(){
		int total = 0;
		int totals = 0;
		if (this.session.get("userinfosession") != null) {
			CtUser cu = (CtUser) this.session.get("userinfosession");
			page = new Page();
			if (orderDTO.getPage() == 0) {
				orderDTO.setPage(1);
			}
			page.setCurrentPage(orderDTO.getPage());
			orderList = orderManager.getOrderReturnGoods(cu.getUId(),page);
			for (int i = 0; i < orderList.size(); i++) {
				if(orderList.get(i).getRetStatus().equals("退款成功")){
					total++;
				} else {
					totals++;
				}
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			session.put("total", total);
			session.put("totals", totals);
		} else {
			return ERROR;
		}
		return SUCCESS;
	}
	

	
	
	private File up;
	private String upFileName;
	private String upContentType;
	private String savePath;
	
	public List<CtComplain> getComplainList() {
		return complainList;
	}

	public void setComplainList(List<CtComplain> complainList) {
		this.complainList = complainList;
	}

	public CtComplain getComplain() {
		return complain;
	}

	public void setComplain(CtComplain complain) {
		this.complain = complain;
	}

	public File getUp() {
		return up;
	}
	public void setUp(File up) {
		this.up = up;
	}

	public String getUpFileName() {
		return upFileName;
	}

	public void setUpFileName(String upFileName) {
		this.upFileName = upFileName;
	}

	public String getUpContentType() {
		return upContentType;
	}

	public void setUpContentType(String upContentType) {
		this.upContentType = upContentType;
	}

	public String getSavePath() {
		return savePath;
	}

	public void setSavePath(String savePath) {
		this.savePath = savePath;
	}

	
	private PayDTO payDTO = new PayDTO();
	
	public PayDTO getPayDTO() {
		return payDTO;
	}

	public void setPayDTO(PayDTO payDTO) {
		this.payDTO = payDTO;
	}

	public CtCouponDetailManager getCoupondetailManager() {
		return coupondetailManager;
	}

	public void setCoupondetailManager(CtCouponDetailManager coupondetailManager) {
		this.coupondetailManager = coupondetailManager;
	}

	public List<CtCouponDetail> getCouponDetails() {
		return couponDetails;
	}

	public void setCouponDetails(List<CtCouponDetail> couponDetails) {
		this.couponDetails = couponDetails;
	}
	//用户发起退款
	public String orderRefundOrder(){
		System.out.println(orderDTO);
		CtUser cu = (CtUser) this.session.get("userinfosession");
		refund.setRefGoodsType(orderDTO.getSimOrPar().equals("sim") ? "0" : "1");
		refund.setRefGoodsid(orderDTO.getGId());
		orderInfo = orderManager.getOrderByOrderSn(orderDTO.getOrderSn(), cu.getUId());
		refund.setRefOrderid(orderInfo.getOrderId());
		refund.setRefPaytype(orderInfo.getPay());
		refund.setRefStatus("1");
		orderManager.saveRefundAndUpdateOrderAndGoods(refund,orderInfo);
		result = SUCCESS;
//		if(orderInfo){
//			
//		}
		return SUCCESS;
	}
	
	private CtRefund refund = new CtRefund();
	public CtRefund getRefund() {
		return refund;
	}

	public void setRefund(CtRefund refund) {
		this.refund = refund;
	}

	public String orderAgainbuy(){
		CtUser cu = (CtUser) this.session.get("userinfosession");
		if(cu != null){
			Long uid = cu.getUId();
			CtOrderInfo oinfo = orderManager.getOrderByOrderSn(orderDTO.getOrderSn(), uid);
			List<CtOrderGoods> ogoodsList = orderManager.getOrderGoodsByOrderId(oinfo.getOrderId());
			for (int i = 0; i < ogoodsList.size(); i++) {
				List<CtCart> cartList = orderManager.getCartByUIdGIdPack(uid, ogoodsList.get(i).getGId().toString(), "notPack");
				CtCart cart = null;
				for (int j = 0; j < cartList.size(); j++) {
					if(cartList.get(j).getCartType().equals(ogoodsList.get(i).getIsParOrSim())){
						cart = cartList.get(j);
					}
				}
				if(cart == null){
					cart = new CtCart();
					if(ogoodsList.get(i).getIsParOrSim().toString().equals("1")){
						cart.setCartAddTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						cart.setCartType("1");
						cart.setGId(ogoodsList.get(i).getGId().toString());
						cart.setGNumber(Long.valueOf(ogoodsList.get(i).getGNumber()));
						cart.setGParNumber(0L);
						cart.setGParPrice("0.0");
						cart.setGPrice(Double.valueOf(ogoodsList.get(i).getGPrice()) * Long.valueOf(ogoodsList.get(i).getGNumber())+"");
						cart.setPack("K");
						cart.setUId(uid.toString());
						orderManager.updateCart(cart);
					} else {
						cart.setCartAddTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						cart.setCartType("0");
						cart.setGId(ogoodsList.get(i).getGId().toString());
						cart.setGNumber(0L);
						cart.setGParNumber(ogoodsList.get(i).getGParNumber());
						cart.setGParPrice(Double.valueOf(ogoodsList.get(i).getGParPrice()) * ogoodsList.get(i).getGParNumber() +"");
						cart.setGPrice("0.0");
						cart.setPack("K");
						cart.setUId(uid.toString());
						Long num = ogoodsList.get(i).getCtGoods().getGNum();
						num = num * 1000;
						if(num < cart.getGParNumber()){
							orderManager.delCartGoodsByUidAndGid(uid, ogoodsList.get(i).getGId());
							cart = new CtCart();
							cart.setGParNumber(num);
							cart.setGParPrice(Double.valueOf(ogoodsList.get(i).getGParPrice()) * num +"");
							cart.setCartAddTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
							cart.setCartType("0");
							cart.setGId(ogoodsList.get(i).getGId().toString());
							cart.setGNumber(0L);
							cart.setGPrice("0.0");
							cart.setPack("K");
							cart.setUId(uid.toString());
							orderManager.updateCart(cart);
						} else {
							orderManager.updateCart(cart);
						}
					}
				} else {
					List<CtRangePrice>  renList = basicManager.getAllRanPrice(ogoodsList.get(i).getGId());
					Integer num = 0;
					Double simp = 0.0;
					Double parp = 0.0;
					Integer parNum = 0;
					if(ogoodsList.get(i).getIsParOrSim().toString().equals("1")){
						String yuanCartNum = ogoodsList.get(i).getGNumber();
						Long nowCartNum = cart.getGNumber();
						Integer newCartNum = Integer.valueOf(yuanCartNum) + Integer.valueOf(nowCartNum.toString());
						for (int j = 0; j < renList.size(); j++) {
							if(newCartNum >= renList.get(j).getSimSNum() && newCartNum <= renList.get(j).getSimENum()){
								String shang = Double.valueOf(newCartNum) / Double.valueOf(renList.get(j).getSimIncrease()) + "";
								String[] chai = shang.split("\\.");
								if(!chai[1].equals("0")){
									num = Integer.valueOf(chai[0]) * Integer.valueOf(renList.get(j).getSimIncrease());
									chai[1] = "0."+chai[1];
									Integer xiaoshu = Integer.valueOf(chai[1]) * Integer.valueOf(renList.get(j).getSimIncrease());
									num = num + xiaoshu;
									simp = num * renList.get(j).getSimRPrice();
								} else {
									num= Integer.valueOf(chai[0]) * Integer.valueOf(renList.get(j).getSimIncrease());
									simp = num * renList.get(j).getSimRPrice();
								}
								break;
							}
							//数量位于样品第一和第二  第二和第三区间中间的时候
							if(newCartNum > renList.get(j).getSimENum() && newCartNum < renList.get(j == renList.size()-1 ? 0 : j+1).getSimSNum()){
								num = renList.get(j+1).getSimSNum();
								simp = num * renList.get(j+1).getSimRPrice();
								break;
							}
							//超过样品第三区间，但是未超过批量区间的时候
							if(newCartNum > renList.get(renList.size()-1).getSimENum() && newCartNum < (renList.get(j).getParSNum() == null ? 0 : renList.get(j).getParSNum())){
								num = renList.get(renList.size()-1).getSimENum();
								simp = num * renList.get(renList.size()-1).getSimRPrice();
								break;
							}
							//超过样品区间，成为批量区间
							if(newCartNum >= (renList.get(j).getParSNum() == null ? 99999999 : renList.get(j).getParSNum())){
								String shang = Double.valueOf(newCartNum) / renList.get(j).getParSNum() + "";
								String chai[] = shang.split("\\.");
								if(!chai[1].equals("0")){
									parNum = Integer.valueOf(chai[0]) * renList.get(j).getParIncrease();
									parp = parNum * renList.get(j).getParRPrice();
									chai[1] = "0." + chai[1];
									Integer numNow = (int)(Double.valueOf(chai[1]) * renList.get(j).getParIncrease());
									for (int k = 0; k < renList.size(); k++) {
										if(numNow >= renList.get(k).getSimSNum() && numNow <= renList.get(k).getSimENum()){
											String shang1 = Double.valueOf(numNow) / Double.valueOf(renList.get(k).getSimIncrease()) + "";
											String[] chai1 = shang1.split("\\.");
											if(!chai1[1].equals("0")){
												num = Integer.valueOf(chai1[0]) * Integer.valueOf(renList.get(k).getSimIncrease());
												chai1[1] = "0."+chai[1];
												Integer xiaoshu = Integer.valueOf(chai1[1]) * Integer.valueOf(renList.get(k).getSimIncrease());
												num = num + xiaoshu;
												simp = num * renList.get(k).getSimRPrice();
											} else {
												num= Integer.valueOf(chai1[0]) * Integer.valueOf(renList.get(k).getSimIncrease());
												simp = num * renList.get(k).getSimRPrice();
											}
											break;
										}
										//数量位于样品第一和第二  第二和第三区间中间的时候
										if(numNow > renList.get(k).getSimENum() && numNow < renList.get(k == renList.size() ? 0 : k+1).getSimSNum()){
											num = renList.get(k+1).getSimSNum();
											simp = num * renList.get(k+1).getSimRPrice();
											break;
										}
										//超过样品第三区间，但是未超过批量区间的时候
										if(numNow > renList.get(renList.size()-1).getSimENum() && numNow < (renList.get(k).getParSNum() == null ? 0 : renList.get(k).getParSNum())){
											num = renList.get(renList.size()-1).getSimENum();
											simp = num * renList.get(renList.size()-1).getSimRPrice();
											break;
										}
									}
								}
							}
							
						}
						if(num != 0){
							cart.setCartAddTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
							cart.setCartType("1");
							cart.setGId(ogoodsList.get(i).getGId().toString());
							cart.setGNumber(num.longValue());
							cart.setGParNumber(0L);
							cart.setGParPrice("0.0");
							cart.setGPrice(simp.toString());
							cart.setPack("K");
							cart.setUId(uid.toString());
							orderManager.updateCart(cart);
						}
						if(parNum != 0){
							cart = orderManager.getCartByGidAndTypeId(ogoodsList.get(i).getGId().toString(), 0, uid);
							if(cart == null){
								cart = new CtCart();
							} else {
								parNum = cart.getGParNumber().intValue() + parNum;
								parp = Double.valueOf(cart.getGParPrice()) + parp;
							}
							cart.setCartAddTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
							cart.setCartType("0");
							cart.setGId(ogoodsList.get(i).getGId().toString());
							cart.setGNumber(0L);
							cart.setGParNumber(parNum.longValue());
							cart.setGParPrice(parp.toString());
							cart.setGPrice("0.0");
							cart.setPack("K");
							cart.setUId(uid.toString());
							Long num1 = ogoodsList.get(i).getCtGoods().getGNum();
							num1 = num1 * 1000;
							if(num1 < cart.getGParNumber()){
								orderManager.delCartGoodsByUidAndGid(uid, ogoodsList.get(i).getGId());
								cart = new CtCart();
								cart.setGParNumber(num1);
								cart.setGParPrice(Double.valueOf(ogoodsList.get(i).getGParPrice()) * num1 +"");
								cart.setCartAddTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
								cart.setCartType("0");
								cart.setGId(ogoodsList.get(i).getGId().toString());
								cart.setGNumber(0L);
								cart.setGPrice("0.0");
								cart.setPack("K");
								cart.setUId(uid.toString());
								orderManager.updateCart(cart);
							} else {
								orderManager.updateCart(cart);
							}
						}
					} else {
						Integer pnum = ogoodsList.get(i).getGParNumber().intValue();
						Double pprice = Double.valueOf(ogoodsList.get(i).getGParPrice());
						
						pnum += parNum;
						parp += (pprice * pnum);
						cart.setGParNumber(cart.getGParNumber() + pnum);
						cart.setGParPrice(Double.valueOf(cart.getGParPrice()) + parp + "");
						
//						pnum += cart.getGParNumber().intValue();
//						parp += Double.valueOf(cart.getGParPrice());
						cart.setCartAddTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						cart.setCartType("0");
						cart.setGId(ogoodsList.get(i).getGId().toString());
						cart.setGNumber(0L);
//						cart.setGParNumber(pnum.longValue());
//						cart.setGParPrice(parp.toString());
						cart.setGPrice("0.0");
						cart.setPack("K");
						cart.setUId(uid.toString());
						Long num1 = ogoodsList.get(i).getCtGoods().getGNum();
						num1 = num1 * 1000;
						if(num1 < cart.getGParNumber()){
							orderManager.delCartGoodsByUidAndGid(uid, ogoodsList.get(i).getGId());
							cart = new CtCart();
							cart.setGParNumber(num1);
							cart.setGParPrice(Double.valueOf(ogoodsList.get(i).getGParPrice()) * num1 +"");
							cart.setCartAddTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
							cart.setCartType("0");
							cart.setGId(ogoodsList.get(i).getGId().toString());
							cart.setGNumber(0L);
//							cart.setGParNumber(pnum.longValue());
//							cart.setGParPrice(parp.toString());
							cart.setGPrice("0.0");
							cart.setPack("K");
							cart.setUId(uid.toString());
							orderManager.updateCart(cart);
						} else {
							orderManager.updateCart(cart);
						}
					}
				}
			}
		} else {
			return LOGIN;
		}
		return SUCCESS;
	}
	/**
     * 提供精确加法计算的add方法
     * @param value1 被加数
     * @param value2 加数
     * @return 两个参数的和
     */
    public static double add(String value1,String value2){
        BigDecimal b1 = new BigDecimal(value1);
        BigDecimal b2 = new BigDecimal(value2);
        return b1.add(b2).doubleValue();
    }
    
    /**
     * 提供精确减法运算的sub方法
     * @param value1 被减数
     * @param value2 减数
     * @return 两个参数的差
     */
    public static double sub(String value1,String value2){
        BigDecimal b1 = new BigDecimal(value1);
        BigDecimal b2 = new BigDecimal(value2);
        return b1.subtract(b2).doubleValue();
    }
    /**
     * 提供精确乘法运算的mul方法
     * @param value1 被乘数
     * @param value2 乘数
     * @return 两个参数的积
     */
    public static double mul(double value1,double value2){
        BigDecimal b1 = new BigDecimal(value1+"");
        BigDecimal b2 = new BigDecimal(value2+"");
        return b1.multiply(b2).doubleValue();
    }
    
    /**
     * 提供精确的除法运算方法div
     * @param value1 被除数
     * @param value2 除数
     * @param scale 精确范围
     * @return 两个参数的商
     * @throws IllegalAccessException
     */
    public static double div(String value1,String value2,int scale) throws IllegalAccessException{
        //如果精确范围小于0，抛出异常信息
        if(scale<0){         
            throw new IllegalAccessException("精确度不能小于0");
        }
        BigDecimal b1 = new BigDecimal(value1);
        BigDecimal b2 = new BigDecimal(value2);
        return b1.divide(b2, scale).doubleValue();    
    }
	
    public String exportToExcel(){
    	CtUser cu = (CtUser) this.session.get("userinfosession");
    	if(cu == null){
    		return "nologin";
    	}
    	return SUCCESS;
    }
    
    //获得下载文件的内容，可以直接读入一个物理文件或从数据库中获取内容   
    public InputStream getInputStream() throws Exception {   
        //return new FileInputStream("somefile.rar"); 直接下载 somefile.rar   
        //和 Servlet 中不一样，这里我们不需对输出的中文转码为 ISO8859-1   
    	CtUser cu = (CtUser) this.session.get("userinfosession");
    	if(cu != null){
    		this.viewCart = orderManager.getCartByUId(cu.getUId());
			List<ViewCartList> cartLists = new ArrayList<ViewCartList>();
			for (int i = 0; i < viewCart.size(); i++) {
				cartLists.add(viewCart.get(i));
			}
			for (int k = 0; k < cartLists.size(); k++)  //外循环是循环的次数
	        {
	            for (int j = cartLists.size() - 1 ; j > k; j--)  //内循环是 外循环一次比较的次数
	            {
	            	if (cartLists.get(k).getGId().toString().equals(cartLists.get(j).getGId().toString()))
	            	{
	            		cartLists.remove(j);
	            	}

	            }
	        }
			
			
			
			for (int i = 0; i < cartLists.size(); i++) {
				cartDTO = new OrderCartDTO();
				rangePricesList = basicManager.getAllRanPrice(Long.valueOf(cartLists.get(i).getGId()));
				cartDTO.setPrices(rangePricesList);
				for (int j = 0; j < viewCart.size(); j++) {
					if(cartLists.get(i).getGId().equals(viewCart.get(j).getGId())){
						cartDTO.setCartGName(viewCart.get(j).getGName());
						cartDTO.setCartGoodsImgUrl(viewCart.get(j).getPUrl());
						cartDTO.setCartGId(Integer.valueOf(viewCart.get(j).getGId()));
						cartDTO.setIsSample(Integer.valueOf(viewCart.get(j).getSample()));
						if(viewCart.get(j).getCarttype().equals("1")){
							cartDTO.setCartSimNum(viewCart.get(j).getGNumber());
							cartDTO.setCartSimPrice(viewCart.get(j).getGPrice());
							for (int j2 = 0; j2 < rangePricesList.size(); j2++) {
								if((Integer.valueOf(viewCart.get(j).getGNumber()) >= rangePricesList.get(j2).getSimSNum()) && (Integer.valueOf(viewCart.get(j).getGNumber()) <= rangePricesList.get(j2).getSimENum())){
									cartDTO.setCartGoodsSelNum(j2);
								}
							}
						} else {
							cartDTO.setCartParNum((viewCart.get(j).getParnumber() / 1000)+"");
							cartDTO.setCartParPrice(viewCart.get(j).getParprice());
						}
						
					}
				}
				cartDTOList.add(cartDTO);
			}
			String allPrice = "0.00";
			for (int i = 0; i < cartDTOList.size(); i++) {
				cartDTOList.get(i).setCartCountNum((Integer.valueOf(cartDTOList.get(i).getCartSimNum()) + Integer.valueOf(cartDTOList.get(i).getCartParNum().equals("0") ? "0" : (Integer.valueOf(cartDTOList.get(i).getCartParNum()) * 1000)+""))+"");
				BigDecimal b1=new BigDecimal(cartDTOList.get(i).getCartSimPrice());  
		        BigDecimal b2=new BigDecimal(cartDTOList.get(i).getCartParPrice());
		        cartDTOList.get(i).setCartCountPrice(b1.add(b2)+"");
		        b1=new BigDecimal(allPrice);
		        b2=new BigDecimal(cartDTOList.get(i).getCartCountPrice());
		        allPrice = b1.add(b2) + "";
		        cartDTOList.get(i).setGoods(fgoodsManager.getGood(cartDTOList.get(i).getCartGId().longValue()));
			}
    	}
    	
//    	POIExcelDown ex = new POIExcelDown();
    	StringBuffer sb = new StringBuffer();
    	for (int i = 0; i < cartDTOList.size(); i++) {
    		sb.append(cartDTOList.get(i).getGoods().getGSn()+","+cartDTOList.get(i).getGoods().getGName()+","+cartDTOList.get(i).getCartCountNum()+"\n");
    	}
    	String str = "\"请保留1,2行内容，从第3行开始竖排填写型号，如输入数量小于起订量，则按该商品起订量计算\",,\n商品编号,商品名称,数量\n"+sb.toString();
//		try {
//			Map<String[], List<String[]>> map = new HashMap<String[], List<String[]>>();
//			String[] ss = {"商品编号","商品名称","数量"};
//			List<String[]> objList = new ArrayList<String[]>();
//			for (int i = 0; i < cartDTOList.size(); i++) {
//				sb.append(cartDTOList.get(i).getGoods().getGSn()+","+cartDTOList.get(i).getGoods().getGName()+","+cartDTOList.get(i).getCartCountNum()+"\n");
////				String[] objs = {cartDTOList.get(i).getGoods().getGSn(),cartDTOList.get(i).getGoods().getGName(),cartDTOList.get(i).getCartCountNum()};
////				objList.add(objs);
//			}
//			map.put(ss, objList);
//			ByteArrayInputStream excelStream = null;
//			HSSFWorkbook workbook = ex.export("购物车商品数据","可直接导入bom", ss,objList);//调用基类
//			if (workbook != null) {
////				fileName = "测试" + "_" + new SimpleDateFormat("yyyy-MM-dd"); // 设置文件名
//				//this.fileName=new String(fileName.getBytes("gb2312"), "iso8859-1");
//				ByteArrayOutputStream baos = new ByteArrayOutputStream();
//				workbook.write(baos);
//				baos.flush();
//				byte[] aa = baos.toByteArray();
//				excelStream = new ByteArrayInputStream(aa, 0, aa.length);
//				baos.close();
//			}
//			return excelStream;   
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
        return new ByteArrayInputStream(str.getBytes());   
//        return new ByteArrayInputStream(aa, 0, aa.length);;   
    }   
    //对于配置中的 ${fileName}, 获得下载保存时的文件名   
    public String getFileName() {   
        DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");   
        String fileName = df.format(new Date()) + ".csv";   
        try {   
            //中文文件名也是需要转码为 ISO8859-1，否则乱码   
            return new String(fileName.getBytes(), "UTF-8");   
        } catch (UnsupportedEncodingException e) {   
            return "impossible.csv";   
        }   
    }
    
    
    //查询信用额度是否充足
    public String findEDuXionYong(){
    	String orderSn = orderDTO.getOrderSn();
    	CtUser cu = (CtUser) this.session.get("userinfosession");
    	orderInfo = orderManager.getOrderByOrderSn(orderSn, cu.getUId());
    	if(orderInfo != null){
    		if(cu.getURemainingAmount() >= Double.valueOf(orderInfo.getTotal())){
    			result = "1";
    		} else {
    			result = "-1";
    		}
    	} else {
    		result = "-2";
    	}
    	return SUCCESS;
    }
    
    //信用额度充足，信用支付
    public String xinyongpaygo(){
    	String orderSn = orderDTO.getOrderSn();
    	CtUser cu = (CtUser) this.session.get("userinfosession");
    	orderInfo = orderManager.getOrderByOrderSn(orderSn, cu.getUId());
    	if(orderInfo != null){
    		if(cu.getURemainingAmount() >= Double.valueOf(orderInfo.getTotal())){
    			cu.setURemainingAmount(sub(cu.getURemainingAmount().toString(), orderInfo.getTotal()));
    			fgoodsManager.merge(cu);
    			
    			orderInfo.setOrderStatus("1");
    			orderInfo.setPay("5");
    			fgoodsManager.merge(orderInfo);
    			
    			payInterface = new CtPayInterface();
    			payInterface.setOrderSn(orderSn);
    			payInterface.setPayDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
    			payInterface.setTotalFee(orderInfo.getTotal());
    			payInterface.setTradeType("6");
    			
    			if(payInterManager.findPayByOrderSn(orderSn) == null){
					payInterManager.save(payInterface);
				}
    			//CtPayAction.insertEJLOrder(orderInfo, cu, payInterManager);
				//发送下单成功短信
//				if(cu.getUMb() != null){
//					s.paySuccessInfoMess(cu.getUMb(), orderInfo.getOrderSn());
//				}
    			result = "1";
    		} else {
    			result = "-1";
    		}
    	} else {
    		result = "-2";
    	}
    	return SUCCESS;
    }
    private serviceInterface s = new serviceInterface();

	public serviceInterface getS() {
		return s;
	}

	public void setS(serviceInterface s) {
		this.s = s;
	}
	
	//信用支付成功返回
	public String xinyongpayfinish(){
		CtUser cu = (CtUser) this.session.get("userinfosession");
		orderInfo = orderManager.getOrderByOrderSn(orderDTO.getOrderSn(), cu.getUId());
		this.request.put("orderInfo", orderInfo);
		return SUCCESS;
	}
	
	
	
}

package org.ctonline.action.order;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;

import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.ctonline.action.BaseAction;
import org.ctonline.dao.goods.CtBomDao;
import org.ctonline.dto.goods.CtBomDTO;
import org.ctonline.dto.goods.CtBomGoodsDTO;
import org.ctonline.manager.basic.BasicManager;
import org.ctonline.manager.goods.CtBomManager;
import org.ctonline.manager.order.OrderManager;
import org.ctonline.po.goods.CtBom;
import org.ctonline.po.goods.CtBomGoods;
import org.ctonline.po.goods.CtRangePrice;
import org.ctonline.po.order.CtCart;
import org.ctonline.po.user.CtUser;
import org.ctonline.util.Page;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ModelDriven;

public class OrderBomAction extends BaseAction implements RequestAware, ModelDriven<Object>,SessionAware{

	private Map<String, Object> request;
	private CtBomDTO bomDTO = new CtBomDTO();
	private CtBom bom;
	private CtBomManager bomManager;
	private OrderManager orderManager;
	private BasicManager basicManager;
	private Page page;
	private List<CtBom> bomList;
	private List<CtBomGoods> bomGoodsList;
	private CtBomGoodsDTO bomGoodsDTO = new CtBomGoodsDTO();
	private String result;
	private JSONArray resultJson;
	private Map<String, Object> session;
	
	//bom列表
	public String list(){
		try {
			page=new Page();
			if (bomDTO.getPage()==0){
				bomDTO.setPage(1);
			}
			page.setCurrentPage(bomDTO.getPage());
			this.bomList = bomManager.loadAll(page);
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "list";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	
	public OrderManager getOrderManager() {
		return orderManager;
	}

	public void setOrderManager(OrderManager orderManager) {
		this.orderManager = orderManager;
	}

	//Bom中心列表
	public String hotList(){
		try {
			page=new Page();
			if (bomDTO.getPage()==0){
				bomDTO.setPage(1);
			}
			page.setCurrentPage(bomDTO.getPage());
			if(this.session.get("userinfosession")==null){
				bomList = bomManager.loadAllHot(page);
			}else {
				CtUser cu = (CtUser)session.get("userinfosession");
				this.bomList = bomManager.loadAllHotLogin(page);
				List<CtBom> boms = bomManager.getCtBomsByUId(cu.getUId());
				for(int j=0;j<boms.size();j++){
					for(int i=0;i<bomList.size();i++){
						if(bomList.get(i).getBomTitle().equals(boms.get(j).getBomTitle())){
							bomList.remove(i);
						}
					}
				}
			}
			for(int i=0;i<bomList.size();i++){
				if(bomList.get(i).getNum() == null){
					bomList.get(i).setNum(0);
				}
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "hotList";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	
	//新增bom
	public String add(){
		try {
			return "add";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	
	//保存bom
	public String saveAdd(){
		List<String> l = new ArrayList<String>();
		try {
			Map session = ActionContext.getContext().getSession();
			CtUser cu = (CtUser)session.get("userinfosession");
			Long UId = cu.getUId();
			String bomTitle = this.bomDTO.getBomTitle();
			String bomDesc = this.bomDTO.getBomDesc();
			Date date = new Date();
			SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			CtBom cb = new CtBom();
			cb.setBomTitle(bomTitle);
			cb.setBomDesc(bomDesc);
			cb.setBomTime(siFormat.format(date));
			cb.setUId(UId);
			cb.setIsHot("0");
			cb.setNum(0);
			String res = this.bomManager.save(cb);
			if(res.equals("Exerror")){
				String url = bomDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
			}
			if(res != null){
				l.add("success");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;	
			}else {
				l.add("error");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			l.add("error");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		}
	}
	
	//修改Bom
	public String editBom(){
		List<String> l = new ArrayList<String>();
		try {
			//获取登录的用户ID
			Long uid = null;
			if(this.session.get("userinfosession")!=null){
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
			}
			Integer[] bomId = this.bomDTO.getMid();
			CtBom bom = this.bomManager.getCtBomByBomId(bomId[0], uid);
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(bom);
			resultJson = json;
			return SUCCESS;	
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			l.add("error");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		}
	}
	//保存修改Bom
	public String saveUpdate(){
		List<String> l = new ArrayList<String>();
		try {
			//获取登录的用户ID
			Long uid = null;
			if(this.session.get("userinfosession")!=null){
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
			}
			Integer bomId = this.bomDTO.getBomId();
			String bomTitle = this.bomDTO.getBomTitle();
			String bomDesc = this.bomDTO.getBomDesc();
			CtBom cb = this.bomManager.getCtBomByBomId(bomId, uid);
			cb.setBomTitle(bomTitle);
			cb.setBomDesc(bomDesc);
			String res=this.bomManager.update(cb);
			if(res.equals("Exerror")){
				String url = bomDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
			}
			l.add("success");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;	
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			l.add("error");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		}
	}
	//删除Bom
	public String del(){
		try {
			for(Integer b:bomDTO.getMid()){
				this.bomManager.delBomGoodsByBomId(b);
				this.bomManager.delete(b);
			}
			return "del";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	//搜素
	public String search(){
		if(bomDTO.getKeyword() != null){
			Page page=new Page();
			if (bomDTO.getPage()==0){
				bomDTO.setPage(1);
			}
			page.setCurrentPage(bomDTO.getPage());
			if (!"".equals(bomDTO.getKeyword())){
				this.bomList= bomManager.findAll(bomDTO.getKeyword(),page);
			}else{
				this.bomList= bomManager.loadAll(page);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "search";
		}else {
			return "golist";
		}
		
	}
	//搜索Bom中心
	public String searchBC(){
		try {
			Page page=new Page();
			if (bomDTO.getPage()==0){
				bomDTO.setPage(1);
			}
			page.setCurrentPage(bomDTO.getPage());
			if(this.session.get("userinfosession")==null){
				if (!"".equals(bomDTO.getKeyword())){
					this.bomList = bomManager.findByKeyWord(bomDTO.getKeyword(), page);
				}else {
					this.bomList= bomManager.loadAllHot(page);
				}
			}else {
				if (!"".equals(bomDTO.getKeyword())){
					CtUser cu = (CtUser)session.get("userinfosession");
					this.bomList = this.bomManager.findByKeyWordLogin(bomDTO.getKeyword(), page);
					List<CtBom> boms = bomManager.getCtBomsByUId(cu.getUId());
					for(int j=0;j<boms.size();j++){
						for(int i=0;i<bomList.size();i++){
							if(bomList.get(i).getBomTitle().equals(boms.get(j).getBomTitle())){
								bomList.remove(i);
							}
						}
					}
				}else {
					CtUser cu = (CtUser)session.get("userinfosession");
					this.bomList = bomManager.loadAllHotLogin(page);
					List<CtBom> boms = bomManager.getCtBomsByUId(cu.getUId());
					for(int j=0;j<boms.size();j++){
						for(int i=0;i<bomList.size();i++){
							if(bomList.get(i).getBomTitle().equals(boms.get(j).getBomTitle())){
								bomList.remove(i);
							}
						}
					}
				}
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "searchBC";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	
	//复制Bom
	public String copyBom(){
		try {
			//获取登录的用户ID
			Long uid = null;
			if(this.session.get("userinfosession")!=null){
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
			}
			for(Integer b:bomDTO.getMid()){
				//this.bomManager.delete(b);
				CtBom cb = this.bomManager.getCtBomByBomId(b, uid);
				CtBom cbcopy = new CtBom();
				String bomTitleCopy = cb.getBomTitle()+"副本";
				Date date = new Date();
				SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				cbcopy.setBomTitle(bomTitleCopy);
				cbcopy.setBomTime(siFormat.format(date));
				cbcopy.setBomDesc(cb.getBomDesc());
				cbcopy.setUId(cb.getUId());
				cbcopy.setIsHot(cb.getIsHot());
				String res=this.bomManager.save(cbcopy);
				if(res.equals("Exerror")){
					String url = bomDTO.getUrl();
					//System.out.println(url);
					String errorTotle = "抱歉，提交异常";
					String errorMess = "含非法字符";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", url);
					return ERROR;
				}
				//复制相应的子表数据
				CtBom cb2 = this.bomManager.getCtBomByBomTitle(bomTitleCopy,"");
				this.bomGoodsList = bomManager.getCtBomGoodsByBomId(cb.getBomId());
				for(int i =0 ;i<bomGoodsList.size();i++){
					CtBomGoods cbg = new CtBomGoods();
					cbg.setBomId(cb2.getBomId());
					cbg.setBomGoodsId(bomGoodsList.get(i).getBomGoodsId());
					cbg.setGId(bomGoodsList.get(i).getGId());
					cbg.setGoodsNum(bomGoodsList.get(i).getGoodsNum());
					cbg.setPack(bomGoodsList.get(i).getPack());
					String re=this.bomManager.saveCtBomGoods(cbg);
					if(re.equals("Exerror")){
						String url = bomDTO.getUrl();
						//System.out.println(url);
						String errorTotle = "抱歉，提交异常";
						String errorMess = "含非法字符";
						session.put("errorMess", errorMess);
						session.put("errorTotle", errorTotle);
						session.put("errorBackUrl", url);
						return ERROR;
					}
				}
			}
			return "copyBom";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	
	//加入我的Bom
	public String addToMyBom(){
		try {
			//获取登录的用户ID
			Long uid = null;
			if(this.session.get("userinfosession")!=null){
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
			}
			Integer bomId = this.bomDTO.getBomId();
			this.bom = this.bomManager.getCtBomByBomId(bomId, uid);
			bom.setNum(bom.getNum()+1);
			this.bomManager.update(bom);
			CtUser cu = (CtUser)session.get("userinfosession");
			Date date = new Date();
			SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			bom.setUId(cu.getUId());
			bom.setBomTime(siFormat.format(date));
			String res=this.bomManager.save(bom);
			if(res.equals("Exerror")){
				String url = bomDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
			}
			//复制相应的子表数据
			this.bomList = this.bomManager.getCtBomsByUId(cu.getUId());
			CtBom cb2 = bomList.get(0);
//			CtBom cb2 = this.bomManager.getCtBomByBomId(bomId);
			this.bomGoodsList = bomManager.getCtBomGoodsByBomId(bomId);
			for(int i =0 ;i<bomGoodsList.size();i++){
				CtBomGoods cbg = new CtBomGoods();
				cbg.setBomId(cb2.getBomId());
				cbg.setBomGoodsId(bomGoodsList.get(i).getBomGoodsId());
				cbg.setGId(bomGoodsList.get(i).getGId());
				cbg.setGoodsNum(bomGoodsList.get(i).getGoodsNum());
				cbg.setPack(bomGoodsList.get(i).getPack());
			String re=this.bomManager.saveCtBomGoods(cbg);
			if(res.equals("Exerror")){
				String url = bomDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
			}
			}
			return "addToMyBom";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	
	//Bom详细
	public String goBomDetail(){
		try {
			//获取登录的用户ID
			Long uid = null;
			if(this.session.get("userinfosession")!=null){
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
			}
			if(bomDTO.getBomId() != null){
				Integer bomId = this.bomDTO.getBomId();
				this.bom = this.bomManager.getCtBomByBomId(bomId, uid);
				page=new Page();
				if (bomGoodsDTO.getPage()==0){
					bomGoodsDTO.setPage(1);
				}
				page.setCurrentPage(bomGoodsDTO.getPage());
				this.bomGoodsList = bomManager.loadBomDetailAll(page,bomId);
				
				//取价
				if(bomGoodsList.size()>0){
					//循环查询结果替换价格查询结果
					for(int i=0;i<bomGoodsList.size();i++){
						Long gid = bomGoodsList.get(i).getGId();
						String price = this.basicManager.getPrice(uid, gid);
						if(price!=null){
							bomGoodsList.get(i).getGoods().setPromotePrice(Double.parseDouble(price));
						}
					}
				}
				page.setTotalPage(page.getTotalPage());
				this.request.put("pages", page);
				return "goBomDetail";
			}else {
				return "golist";
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	
	//Bom中心详细
	public String goBomCenterDetail(){
		try {
			//获取登录的用户ID
			Long uid = null;
			if(this.session.get("userinfosession")!=null){
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
			}
			Integer bomId = this.bomDTO.getBomId();
			this.bom = this.bomManager.getCtBomByBomId(bomId, uid);
			page=new Page();
			if (bomGoodsDTO.getPage()==0){
				bomGoodsDTO.setPage(1);
			}
			page.setCurrentPage(bomGoodsDTO.getPage());
			this.bomGoodsList = bomManager.loadBomDetailAll(page,bomId);
			
			//取价
			if(bomGoodsList.size()>0){
				//循环查询结果替换价格查询结果
				for(int i=0;i<bomGoodsList.size();i++){
					Long gid = bomGoodsList.get(i).getGId();
					String price = this.basicManager.getPrice(uid, gid);
					if(price!=null){
						bomGoodsList.get(i).getGoods().setPromotePrice(Double.parseDouble(price));
					}
				}
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "goBomCenterDetail";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	
	//删除Bom详情中的某个商品
	public String delGoodsOfBomDetail(){
		try {
			//获取登录的用户ID
			Long uid = null;
			if(this.session.get("userinfosession")!=null){
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
			}
			Integer bomId = this.bomDTO.getBomId();
			this.bom = this.bomManager.getCtBomByBomId(bomId, uid);
			for(Long b:bomGoodsDTO.getMid()){
				this.bomManager.delGoodsOfBomDetail(b);
			}
			page=new Page();
			if (bomGoodsDTO.getPage()==0){
				bomGoodsDTO.setPage(1);
			}
//			page.setCurrentPage(bomGoodsDTO.getPage());
//			this.bomGoodsList = bomManager.loadBomDetailAll(page,bomId);
//			page.setTotalPage(page.getTotalPage());
//			this.request.put("pages", page);
			return "delGoodsOfBomDetail";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	//搜索Bom详情中的商品
	public String searchGoodsOfBomDetail(){
		try {
			//获取登录的用户ID
			Long uid = null;
			if(this.session.get("userinfosession")!=null){
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
			}
			Integer bomId = this.bomGoodsDTO.getBomId();
			this.bom = this.bomManager.getCtBomByBomId(bomId, uid);
			Page page=new Page();
			if (bomGoodsDTO.getPage()==0){
				bomGoodsDTO.setPage(1);
			}
			page.setCurrentPage(bomGoodsDTO.getPage());
			if (!"".equals(bomGoodsDTO.getKeyword()) && !"订单编号/收货人/商品编号/商品名称".equals(bomGoodsDTO.getKeyword())){
				this.bomGoodsList= bomManager.findBomDetailAll(bomGoodsDTO.getKeyword(),page,bomId);
			}else{
				this.bomGoodsList= bomManager.loadBomDetailAll(page,bomId);
			}
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
			return "searchGoodsOfBomDetail";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	
	//保存Pack
	public String savePack(){
		try {
			String pack = this.bomGoodsDTO.getPack();
			Long bomGoodsId = this.bomGoodsDTO.getBomGoodsId();
			CtBomGoods cbg = this.bomManager.getCtBomGoodsBybomGoodsId(bomGoodsId);
			cbg.setPack(pack);
			String res=this.bomManager.updateCtBomGoods(cbg);
			if(res.equals("Exerror")){
				String url = bomDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
			}
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	//更新数量
	public String updateGoodsNum(){
		List<String> l = new ArrayList<String>();
		try {
			Long goodsNum = this.bomGoodsDTO.getGoodsNum();
			Long GId = this.bomGoodsDTO.getGId();
			Integer bomId = this.bomGoodsDTO.getBomId();
			Long bomGoodsId = bomGoodsDTO.getBomGoodsId();
			CtBomGoods cbg = this.bomManager.getCtBomGoodsBybomGoodsId(bomGoodsId);
			cbg.setGoodsNum(goodsNum);
			String res=this.bomManager.updateCtBomGoods(cbg);
			if(res.equals("Exerror")){
				String url = bomDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
			}
			l.add("success");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;	
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			l.add("error");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		}
	}
	private List<CtRangePrice> rangePricesList = new ArrayList<CtRangePrice>();
	
	public List<CtRangePrice> getRangePricesList() {
		return rangePricesList;
	}

	public void setRangePricesList(List<CtRangePrice> rangePricesList) {
		this.rangePricesList = rangePricesList;
	}

	//新增Bom详情中的商品
	public String addGoodsOfBomDetail(){
		try {
			//获取登录的用户ID
			Long uid = null;
			if(this.session.get("userinfosession")!=null){
				CtUser cu = (CtUser) this.session.get("userinfosession");
				uid = cu.getUId();
			}
			Integer bomId = this.bomGoodsDTO.getBomId();
			this.bom = this.bomManager.getCtBomByBomId(bomId, uid);
			Long GId = this.bomGoodsDTO.getGId();
			List<CtBomGoods> cbgs = this.bomManager.getCtBomGoodsByGIdAndBomId(GId, bomId);
			
			//获取基础数据
			rangePricesList = basicManager.getAllRanPrice(GId);
			String[] sims = new String[3];
			String[] sime = new String[3];
			String[] siminc = new String[3];
			String[] simprice = new String[3];
			
			String[] pars = new String[3];
			String[] pare = new String[3];
			String[] parinc = new String[3];
			String[] parprice = new String[3];
			for (int i = 0; i < rangePricesList.size(); i++) {
				sims[i]=rangePricesList.get(i).getSimSNum().toString();
				sime[i]=rangePricesList.get(i).getSimENum().toString();
				siminc[i] = rangePricesList.get(i).getSimIncrease().toString();
				simprice[i] = rangePricesList.get(i).getSimRPrice().toString();
				
				pars[i] = rangePricesList.get(i).getParSNum().toString();
				pare[i] = rangePricesList.get(i).getParENum() == null ? "" : rangePricesList.get(i).getParENum().toString();
				parinc[i] = rangePricesList.get(i).getParIncrease().toString();
				parprice[i] = rangePricesList.get(i).getParRPrice().toString();
			}
			
			if(cbgs != null && cbgs.size()>0){
				for(int i=0;i<cbgs.size();i++){
					if("5K/盘".equals(cbgs.get(i).getPack())){
						nowCount = cbgs.get(i).getGoodsNum().intValue();
						simQuJian(sims,sime,siminc,pars,pare,parinc);
						
						cbgs.get(i).setGoodsNum(nowCount.longValue());
						String res=this.bomManager.updateCtBomGoods(cbgs.get(i));
						if(res.equals("Exerror")){
							String url = bomDTO.getUrl();
							//System.out.println(url);
							String errorTotle = "抱歉，提交异常";
							String errorMess = "含非法字符";
							session.put("errorMess", errorMess);
							session.put("errorTotle", errorTotle);
							session.put("errorBackUrl", url);
							return ERROR;
						}
						break;
					}else {
						CtBomGoods cbg2 = new CtBomGoods();
						cbg2.setBomId(bomId);
						cbg2.setGId(GId);
						nowCount = 1;
						simQuJian(sims,sime,siminc,pars,pare,parinc);
						cbg2.setGoodsNum(nowCount.longValue());
						cbg2.setPack("5K/盘");
						String res=this.bomManager.saveCtBomGoods(cbg2);
						if(res.equals("Exerror")){
							String url = bomDTO.getUrl();
							//System.out.println(url);
							String errorTotle = "抱歉，提交异常";
							String errorMess = "含非法字符";
							session.put("errorMess", errorMess);
							session.put("errorTotle", errorTotle);
							session.put("errorBackUrl", url);
							return ERROR;
						}
					}
				}
			}else {
				CtBomGoods cbg2 = new CtBomGoods();
				cbg2.setBomId(bomId);
				cbg2.setGId(GId);
				nowCount = 1;
				simQuJian(sims,sime,siminc,pars,pare,parinc);
				cbg2.setGoodsNum(nowCount.longValue());
				cbg2.setPack("5K/盘");
				String res=this.bomManager.saveCtBomGoods(cbg2);
				if(res.equals("Exerror")){
					String url = bomDTO.getUrl();
					//System.out.println(url);
					String errorTotle = "抱歉，提交异常";
					String errorMess = "含非法字符";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", url);
					return ERROR;
				}
			}
			return "addGoodsOfBomDetail";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	
	private Integer nowCount;
	
	public Integer getNowCount() {
		return nowCount;
	}

	public void setNowCount(Integer nowCount) {
		this.nowCount = nowCount;
	}

	private int simQuJian(String[] sims, String[] sime,
			String[] siminc, String[] pars, String[] pare, String[] parinc) {
		Integer count = nowCount;
		if(count < Integer.valueOf(sims[0])){
			nowCount = Integer.valueOf(parinc[0]);
			return 3;
		}
		if(count > Integer.valueOf(sime[0]) && count < Integer.valueOf(sims[1])){
			//介于样品第一区间和第二区间之间
			nowCount = Integer.valueOf(parinc[1]);
			return 4;
		}
		if(count > Integer.valueOf(sime[1]) && count < Integer.valueOf(sims[2])){
			nowCount = Integer.valueOf(parinc[2]);
			return 5;
		}
		if(count >= Integer.valueOf(sims[0]) && count <= Integer.valueOf(sime[0])){
			//样品第一区间
			if(count!=0){
				String chu = nowCount / Double.valueOf(siminc[0]) + "";
				String[] chai = chu.split("\\.");
				if(!chai[1].equals("0")){
					Integer zheng = Integer.valueOf(chai[0]) * Integer.valueOf(siminc[0]);
					Integer cha = count - zheng;
					nowCount = count - cha + Integer.valueOf(siminc[0]);
				} else {
					nowCount = count;
				}
			}
			nowCount = Integer.valueOf(parinc[0]);
			return 0;
		}
		if(count >= Integer.valueOf(sims[1]) && count <= Integer.valueOf(sime[1])){
			//样品第二区间
			if(count != 0){
				String chu = nowCount / Double.valueOf(siminc[1]) + "";
				String[] chai = chu.split("\\.");
				if(!chai[1].equals("0")){
					Integer zheng = Integer.valueOf(chai[0]) * Integer.valueOf(siminc[1]);
					Integer cha = count - zheng;
					nowCount = count - cha + Integer.valueOf(siminc[1]);
				} else {
					nowCount = count;
				}
			}
			//nowCount = Integer.valueOf(parinc[1]);
			return 1;
		}
		if(count >= Integer.valueOf(sims[2]) && count <= Integer.valueOf(sime[2])){
			//样品第三区间
			if(count != 0){
				String chu = nowCount / Double.valueOf(siminc[2]) + "";
				String[] chai = chu.split("\\.");
				if(!chai[1].equals("0")){
					Integer zheng = Integer.valueOf(chai[0]) * Integer.valueOf(siminc[2]);
					Integer cha = count - zheng;
					nowCount = count - cha + Integer.valueOf(siminc[2]);
				} else {
					nowCount = count;
				}
			}
			//nowCount = Integer.valueOf(parinc[2]);
			return 2;
		}
		return 0;
	}
	
	//商品详情-加入我的bom两种方式
	public String addToBomFromGoodsTwo(){
		try {
			CtCart cart = new CtCart();
			//把商品加入新建的bom
			String bomTitle = this.bomDTO.getBomTitle();
			Long gid = this.bomDTO.getGId();
			Long cartId = this.bomDTO.getCartId();
			String pack = this.bomDTO.getPack();
			Long goodsNum = this.bomDTO.getGoodsNum(); 
			Map session = ActionContext.getContext().getSession();
			CtUser cu = (CtUser)session.get("userinfosession");
			Long UId = cu.getUId();
			//String bomTitle = this.bomDTO.getBomTitle();
			String bomDesc = this.bomDTO.getBomDesc();
			Date date = new Date();
			SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			CtBom cb = new CtBom();
			cb.setBomTitle(bomTitle);
			cb.setBomDesc(bomTitle);
			cb.setBomTime(siFormat.format(date));
			cb.setUId(UId);
			cb.setIsHot("0");
			cb.setNum(0);
			String res = this.bomManager.save(cb);
			if(res.equals("Exerror")){
				String url = bomDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
			}
			//查询购物车数据
			
			if (cartId!=null){
				cart = orderManager.getCartByCartId(cartId);
			}
			
			
			
			
			
			//查询bom
			bom = this.bomManager.getCtBomsByUId(UId).get(0);
			CtBomGoods cbg = this.bomManager.getCtBomGoodsByGId(gid,bom.getBomId());
			if(cbg != null){
				
				if (cartId!=null){//购物车
					cbg.setGoodsNum(cart.getGNumber() + goodsNum);
					cbg.setPack(cart.getPack());
					cbg.setGPrice(cart.getGPrice());
					
				}else{//单独商品页
					
					if(cbg.getGoodsNum() == null){
						cbg.setGoodsNum((long) 2);
						cbg.setPack(pack);
					}else {
						cbg.setGoodsNum(cbg.getGoodsNum() + goodsNum);
						cbg.setPack(pack);
					}
				}
				
				String re=this.bomManager.updateCtBomGoods(cbg);//update
				if(re.equals("Exerror")){
					String url = bomDTO.getUrl();
					//System.out.println(url);
					String errorTotle = "抱歉，提交异常";
					String errorMess = "含非法字符";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", url);
					return ERROR;
				}
			}else {
				CtBomGoods cbg2 = new CtBomGoods();
				cbg2.setBomId(bom.getBomId());
				cbg2.setGId(gid);
				if (cartId!=null){//购物车
					
					cbg2.setGoodsNum(cart.getGNumber() + goodsNum);
					cbg2.setPack(cart.getPack());
					cbg2.setGPrice(cart.getGPrice());
					
				}else{//单独商品页
					cbg2.setGoodsNum(cbg.getGoodsNum() + goodsNum);
					
					cbg2.setPack(pack);
					//cbg2.setGPrice(cart.getGPrice());
				
				}
				
				
				
				String re=this.bomManager.saveCtBomGoods(cbg2);//insert
				if(re.equals("Exerror")){
					String url = bomDTO.getUrl();
					//System.out.println(url);
					String errorTotle = "抱歉，提交异常";
					String errorMess = "含非法字符";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", url);
					return ERROR;
				}
			}
			//把商品加入已有的bom
			for(Integer b:bomDTO.getMid()){
				CtBomGoods c = this.bomManager.getCtBomGoodsByGId(gid,b);
				if(c != null){
					if(c.getGoodsNum() == null){
						c.setGoodsNum(cart.getGNumber());
						c.setPack(pack);
					}else {
						c.setGoodsNum(c.getGoodsNum() + goodsNum);
						c.setPack(pack);
					}
					String re=this.bomManager.updateCtBomGoods(c);
					if(re.equals("Exerror")){
						String url = bomDTO.getUrl();
						//System.out.println(url);
						String errorTotle = "抱歉，提交异常";
						String errorMess = "含非法字符";
						session.put("errorMess", errorMess);
						session.put("errorTotle", errorTotle);
						session.put("errorBackUrl", url);
						return ERROR;
					}
				}else {
					CtBomGoods cbg2 = new CtBomGoods();
					cbg2.setBomId(b);
					cbg2.setGId(gid);
					cbg2.setGoodsNum(goodsNum);
					cbg2.setPack(pack);
					String re=this.bomManager.saveCtBomGoods(cbg2);
					if(re.equals("Exerror")){
						String url = bomDTO.getUrl();
						//System.out.println(url);
						String errorTotle = "抱歉，提交异常";
						String errorMess = "含非法字符";
						session.put("errorMess", errorMess);
						session.put("errorTotle", errorTotle);
						session.put("errorBackUrl", url);
						return ERROR;
					}
				}
			}
			List<String> l = new ArrayList<String>();
			l.add("success");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			List<String> l = new ArrayList<String>();
			l.add("error");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		}
	}
	//商品详情-加入我的bom，新建
	public String addToBomFromGoodsNew(){
		try {
			//把商品加入新建的bom
			String pack = this.bomDTO.getPack();
			Long goodsNum = this.bomDTO.getGoodsNum();
			String bomTitle = this.bomDTO.getBomTitle();
			Long gid = this.bomDTO.getGId();
			Map session = ActionContext.getContext().getSession();
			CtUser cu = (CtUser)session.get("userinfosession");
			Long UId = cu.getUId();
			//String bomTitle = this.bomDTO.getBomTitle();
			String bomDesc = this.bomDTO.getBomDesc();
			Date date = new Date();
			SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			CtBom cb = new CtBom();
			cb.setBomTitle(bomTitle);
			cb.setBomDesc(bomTitle);
			cb.setBomTime(siFormat.format(date));
			cb.setUId(UId);
			cb.setIsHot("0");
			cb.setNum(0);
			String res= this.bomManager.save(cb);
			if(res.equals("Exerror")){
				String url = bomDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
			}
			//查询bom
			bom = this.bomManager.getCtBomsByUId(UId).get(0);
			CtBomGoods cbg = this.bomManager.getCtBomGoodsByGId(gid,bom.getBomId());
			if(cbg != null){
				if(cbg.getGoodsNum() == null){
					cbg.setGoodsNum((long) 2);
					cbg.setPack(pack);
				}else {
					cbg.setGoodsNum(cbg.getGoodsNum() + goodsNum);
					cbg.setPack(pack);
				}
				String re=this.bomManager.updateCtBomGoods(cbg);
				if(re.equals("Exerror")){
					String url = bomDTO.getUrl();
					//System.out.println(url);
					String errorTotle = "抱歉，提交异常";
					String errorMess = "含非法字符";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", url);
					return ERROR;
				}
			}else {
				CtBomGoods cbg2 = new CtBomGoods();
				cbg2.setBomId(bom.getBomId());
				cbg2.setGId(gid);
				cbg2.setGoodsNum(goodsNum);
				cbg2.setPack(pack);
				String re=this.bomManager.saveCtBomGoods(cbg2);
				if(re.equals("Exerror")){
					String url = bomDTO.getUrl();
					//System.out.println(url);
					String errorTotle = "抱歉，提交异常";
					String errorMess = "含非法字符";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", url);
					return ERROR;
				}
			}
			List<String> l = new ArrayList<String>();
			l.add("success");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			List<String> l = new ArrayList<String>();
			l.add("error");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		}
	}
	
	//把商品加入我的bom 已经存在的bom
	public String addToBomFromGoodsExisting(){
		Long gid = this.bomDTO.getGId();
		String pack = this.bomDTO.getPack();
		Long goodsNum = this.bomDTO.getGoodsNum();
		try {
			for(Integer b:bomDTO.getMid()){
				CtBomGoods c = this.bomManager.getCtBomGoodsByGId(gid,b);
				if(c != null){
					if(c.getGoodsNum() == null){
						c.setGoodsNum((long) 2);
						c.setPack(pack);
					}else {
						c.setGoodsNum(c.getGoodsNum() + goodsNum);
						c.setPack(pack);
					}
					String res=this.bomManager.updateCtBomGoods(c);
					if(res.equals("Exerror")){
						String url = bomDTO.getUrl();
						//System.out.println(url);
						String errorTotle = "抱歉，提交异常";
						String errorMess = "含非法字符";
						session.put("errorMess", errorMess);
						session.put("errorTotle", errorTotle);
						session.put("errorBackUrl", url);
						return ERROR;
					}
				}else {
					CtBomGoods cbg2 = new CtBomGoods();
					cbg2.setBomId(b);
					cbg2.setGId(gid);
					cbg2.setGoodsNum(goodsNum);
					cbg2.setPack(pack);
					String res=this.bomManager.saveCtBomGoods(cbg2);
					if(res.equals("Exerror")){
						String url = bomDTO.getUrl();
						//System.out.println(url);
						String errorTotle = "抱歉，提交异常";
						String errorMess = "含非法字符";
						session.put("errorMess", errorMess);
						session.put("errorTotle", errorTotle);
						session.put("errorBackUrl", url);
						return ERROR;
					}
				}
			}
			List<String> l = new ArrayList<String>();
			l.add("success");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			List<String> l = new ArrayList<String>();
			l.add("error");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		}
	}
	//检验BomTitle是否存在
	public String checkBomTitle(){
		try {
			String bomTitle = bomDTO.getBomTitle();
			CtBom cb = bomManager.getCtBomByBomTitle(bomTitle,"");
			if(cb != null){
				List<String> l = new ArrayList<String>();
				l.add("exists");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
			}
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			List<String> l = new ArrayList<String>();
			l.add("exists");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		}
	}
	
	
	
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.bomDTO;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request; 
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public CtBomDTO getBomDTO() {
		return bomDTO;
	}

	public void setBomDTO(CtBomDTO bomDTO) {
		this.bomDTO = bomDTO;
	}

	public CtBomManager getBomManager() {
		return bomManager;
	}

	public void setBomManager(CtBomManager bomManager) {
		this.bomManager = bomManager;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public List<CtBom> getBomList() {
		return bomList;
	}

	public void setBomList(List<CtBom> bomList) {
		this.bomList = bomList;
	}

	public CtBom getBom() {
		return bom;
	}

	public void setBom(CtBom bom) {
		this.bom = bom;
	}

	public List<CtBomGoods> getBomGoodsList() {
		return bomGoodsList;
	}

	public void setBomGoodsList(List<CtBomGoods> bomGoodsList) {
		this.bomGoodsList = bomGoodsList;
	}

	public CtBomGoodsDTO getBomGoodsDTO() {
		return bomGoodsDTO;
	}

	public void setBomGoodsDTO(CtBomGoodsDTO bomGoodsDTO) {
		this.bomGoodsDTO = bomGoodsDTO;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public JSONArray getResultJson() {
		return resultJson;
	}

	public void setResultJson(JSONArray resultJson) {
		this.resultJson = resultJson;
	}

	public BasicManager getBasicManager() {
		return basicManager;
	}

	public void setBasicManager(BasicManager basicManager) {
		this.basicManager = basicManager;
	}
	
	public Map<String, Object> getSession() {
		return session;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

}

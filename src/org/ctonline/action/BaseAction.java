package org.ctonline.action;

import org.ctonline.common.PageBean;

import com.opensymphony.xwork2.ActionSupport;

public class BaseAction extends ActionSupport {

	private PageBean pagebean = new PageBean();
	private String CtName="长亭易购";

	public String getCtName() {
		return CtName;
	}

	public void setCtName(String ctName) {
		CtName = ctName;
	}

	public PageBean getPagebean() {
		return pagebean;
	}

	public void setPagebean(PageBean pagebean) {
		this.pagebean = pagebean;
	}

}

package org.ctonline.action.user;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.lang.reflect.Field;  


import javax.servlet.ServletInputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;
import org.ctonline.config.AlipayConfig;
import org.ctonline.dto.user.CtSmsDTO;
import org.ctonline.dto.user.CtUserDTO;
import org.ctonline.dto.wxmessage.CoreService;
import org.ctonline.dto.wxmessage.resp.Article;
import org.ctonline.dto.wxmessage.resp.NewsMessage;
import org.ctonline.dto.wxmessage.resp.TextMessage;
import org.ctonline.manager.basic.BasicManager;
import org.ctonline.manager.basic.CtCouponDetailManager;
import org.ctonline.manager.basic.CtTempManager;
import org.ctonline.manager.user.CtUserManager;
import org.ctonline.po.basic.CtFilter;
import org.ctonline.po.basic.CtTemp;
import org.ctonline.po.user.CtDrawDetail;
import org.ctonline.po.user.CtExtension;
import org.ctonline.po.user.CtHongbaoTemp;
import org.ctonline.po.user.CtLoginFailed;
import org.ctonline.po.user.CtSms;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.user.CtHongbao;
import org.ctonline.test.serviceInterface;
import org.ctonline.util.AlipayNotify;
import org.ctonline.util.AlipaySubmit;
import org.ctonline.util.GetWxOrderno;
import org.ctonline.util.MD5;
import org.ctonline.util.MacUtil;
import org.ctonline.util.MialUtil;
import org.ctonline.util.MoneyUtils;
import org.ctonline.util.UtilDate;
import org.ctonline.util.WxMessageUtil;
import org.ctonline.util.wxutil;
import org.ctonline.util.Util;
import org.ctonline.util.http.HttpClientConnectionManager;
import org.ctonline.weibo4j.Account;
import org.ctonline.weibo4j.Users;
import org.ctonline.weibo4j.model.User;
import org.ctonline.weibo4j.model.WeiboException;
import org.ctonline.weibo4j.org.json.JSONException;
import org.ctonline.weibo4j.org.json.JSONObject;
import org.dom4j.DocumentException;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.qq.connect.QQConnectException;
import com.qq.connect.api.OpenID;
import com.qq.connect.api.qzone.PageFans;
import com.qq.connect.api.qzone.UserInfo;
import com.qq.connect.javabeans.AccessToken;
import com.qq.connect.javabeans.qzone.PageFansBean;
import com.qq.connect.javabeans.qzone.UserInfoBean;
import com.qq.connect.javabeans.weibo.Company;
import com.qq.connect.oauth.Oauth;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.core.util.QuickWriter;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;
import com.thoughtworks.xstream.io.xml.XppDriver;

public class LoginAction extends ActionSupport implements RequestAware,
		SessionAware, ServletResponseAware, ServletRequestAware,
		ModelDriven<Object> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CtUserManager userManager;
	private BasicManager basicManager;
	private CtCouponDetailManager couponDetailManager;
	private CtUser ctUser;
	private CtTempManager tempManager;

	public CtTempManager getTempManager() {
		return tempManager;
	}

	public void setTempManager(CtTempManager tempManager) {
		this.tempManager = tempManager;
	}

	public CtCouponDetailManager getCouponDetailManager() {
		return couponDetailManager;
	}

	public void setCouponDetailManager(CtCouponDetailManager couponDetailManager) {
		this.couponDetailManager = couponDetailManager;
	}

	private CtUserDTO userDTO = new CtUserDTO();
	private CtSmsDTO smsDTO = new CtSmsDTO();
	private CtSms ctSms;
	private Long UId;
	private Map<String, Object> request;
	private String result;
	private Map<String, Object> session;
	private HttpServletResponse response;
	private HttpServletRequest _reRequest;
	// private String phoneYzm;
	private String yzm;
	private String isAutoLogin;
	private String message;
	private String uid;
	private String ucode;
	private String email;
	private String referUrl;
	private String pwd;

	private String uname;

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.userDTO;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}

	// 跳转到注册界面
	public String goRegister() {
		return "goRegister";
	}

	// 跳转到找回密码界面
	public String goForgetPwd() {
		return "goForgetPwd";
	}

	// 跳转到登录界面
	public String goLogin() {
		return "login";
	}

	private List<CtFilter> filtersList = new ArrayList<CtFilter>();
	
	public List<CtFilter> getFiltersList() {
		return filtersList;
	}

	public void setFiltersList(List<CtFilter> filtersList) {
		this.filtersList = filtersList;
	}

	// ��֤�û����Ƿ����
	public String checkName() {
		String UUserid = ctUser.getUUserid();
		CtUser ctUser = userManager.getCtUserByUUserid(UUserid);
		if (ctUser == null) {
			filtersList = userManager.getFilter(UUserid);
			if(filtersList != null){
				String str = StringFilter1(UUserid);
				for (int i = 0; i < filtersList.size(); i++) {
					int isok = str.indexOf(filtersList.get(i).getFname());
					if(isok != -1){
						this.result = "input";
						return SUCCESS;
					}
				}
			} 
			result = "success";
			this.result = "success";
		} else {
			this.result = "error";
		}
		return SUCCESS;
	}
//	// 过滤特殊字符  
//    public static   String StringFilter(String   str) throws   PatternSyntaxException   {     
//                // 只允许字母和数字       
//                // String   regEx  =  "[^a-zA-Z0-9]";                     
//                   // 清除掉所有特殊字符  
//          String regEx="[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";  
//          Pattern   p   =   Pattern.compile(regEx);     
//          Matcher   m   =   p.matcher(str);     
//          return   m.replaceAll("").trim();     
//    } 
	// 检测邮箱是否被注册
	public String checkEmail() {
		String UEmail = ctUser.getUEmail();
		CtUser ctUser = userManager.getCtUserByUEmail(UEmail);
		if (ctUser == null) {
			result = "success";
			this.result = "success";
		} else {
			this.result = "error";
		}
		return SUCCESS;
	}

	// ��֤�ֻ���Ƿ�ע��
	public String checkPhone() {
		String UMb = ctUser.getUMb();
		CtUser ctUser = userManager.getCtUserByUMb(UMb);

		if (ctUser == null) {
			result = "success";
			this.result = "success";
		} else {
			this.result = "error";
		}
		return SUCCESS;
	}

	// ��֤�ֻ�ʱ������֤��Ϣ
	public String savePhoneYzm() throws HttpException, IOException {
		ActionContext ac = ActionContext.getContext().getActionInvocation()
				.getInvocationContext();
		HttpServletRequest httpServletRequest = (HttpServletRequest) ac
				.get(ServletActionContext.HTTP_REQUEST);
		CtSms cs = new CtSms();
		String ip = httpServletRequest.getRemoteAddr();
		UId = userDTO.getUId();
		Date date = new Date();
		SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		String base = "0123456789";
		int length = base.length();
		String sRand = "";
		for (int i = 0; i < 4; i++) {
			Random random = new Random();
			int start = random.nextInt(length);
			String rand = base.substring(start, start + 1);
			sRand += rand;
		}

		String UMb = ctUser.getUMb();
		String yzm = sRand;
		cs.setSms(yzm);
		cs.setSmsIp(ip);
		cs.setSmsTime(siFormat.format(date));
		cs.setUMb(UMb);
		Long id = this.userManager.saveCtSms(cs);
		String testSms = ctSms.getSms();
		if (!testSms.equals("2222")) {
			serviceInterface test = new serviceInterface();
			test.getPhone(UMb, yzm);
		}
		if (id != null) {
			this.result = "success";
		} else {
			this.result = "error";
		}
		return SUCCESS;
	}

	// ��֤�������֤���Ƿ���ȷ
	public String checkYzmIsEqual() throws java.text.ParseException {
		String UMb = ctUser.getUMb();
		String yzm = ctSms.getSms();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < yzm.length(); i++) {
			char c = yzm.charAt(i);
			if (Character.isLowerCase(c)) {
				sb.append(Character.toUpperCase(c));
			} else {
				sb.append(c);
			}
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		CtSms cs = this.userManager.getCtSmsByUMb(UMb);
		if (cs != null) {
				String time = cs.getSmsTime();
				Long a = sdf.parse(time).getTime();
				Long diff = sdf.parse(sdf.format(new Date())).getTime() - sdf.parse(time).getTime();
				long nd = 1000 * 24 * 60 * 60;// 一天的毫秒数    
				long nh = 1000 * 60 * 60;// 一小时的毫秒数    
				long nm = 1000 * 60;// 一分钟的毫秒数    
				long ns = 1000;// 一秒钟的毫秒数    
				long day = 0;
				Long min = diff % nd % nh / nm + day * 24 * 60;// 计算差多少分钟    
				if(min < 5){
					String yzm2 = cs.getSms();
					if (sb.toString().equals(yzm2)) {
						this.result = "success";
					} else {
						this.result = "error";
					}
				} else {
					this.result = "chaoshi";
				}
		} else {
			this.result = "error";
		}

		return SUCCESS;
	}
	
	
	//QQ登录返回
	public String afterLogin(){
		HttpServletRequest requ = ServletActionContext.getRequest();

       try {
           AccessToken accessTokenObj = (new Oauth()).getAccessTokenByRequest(requ);

           String accessToken   = null,
                  openID        = null;
           long tokenExpireIn = 0L;




           if (accessTokenObj.getAccessToken().equals("")) {
//               我们的网站被CSRF攻击了或者用户取消了授权
//               做一些数据统计工作
               System.out.print("没有获取到响应参数");
           } else {
               accessToken = accessTokenObj.getAccessToken();
               tokenExpireIn = accessTokenObj.getExpireIn();

               session.put("demo_access_token", accessToken);
               session.put("demo_token_expirein", String.valueOf(tokenExpireIn));

               // 利用获取到的accessToken 去获取当前用的openid -------- start
               OpenID openIDObj =  new OpenID(accessToken);
               openID = openIDObj.getUserOpenID();

               ////out.println("欢迎你，代号为 " + openID + " 的用户!");
               session.put("demo_openid", openID);
               ////out.println("<a href=" + "/shuoshuoDemo.html" +  " target=\"_blank\">去看看发表说说的demo吧</a>");
               // 利用获取到的accessToken 去获取当前用户的openid --------- end


               ////out.println("<p> start -----------------------------------利用获取到的accessToken,openid 去获取用户在Qzone的昵称等信息 ---------------------------- start </p>");
               UserInfo qzoneUserInfo = new UserInfo(accessToken, openID);
               UserInfoBean userInfoBean = qzoneUserInfo.getUserInfo();
               ////out.println("<br/>");
               if (userInfoBean.getRet() == 0) {
                  //验证是否使用QQ登录过
            	   CtUser aliUser = userManager.findQQUserId(openID);
       			if (aliUser != null) {
       				aliUser.setULastTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
       						.format((new Date())));
       				HttpServletRequest req = ServletActionContext.getRequest();
       				String Ip =UtilDate.getIpAddr(req);
       				aliUser.setULastIp(Ip);
       				userManager.update(aliUser);
       				this.session.remove("userinfosession");
       				this.session.put("userinfosession", aliUser);
       			} else {
       				CtUser ct = new CtUser();
       				ct.setUUserid("CT_"
       						+ userInfoBean.getNickname()
       						+ "_"
       						+ new SimpleDateFormat("yyyy-MM-ddHH:mm:ss")
       								.format(new Date()));
       				ct.setUUsername("CT_" + userInfoBean.getNickname());
       				ct.setUCode(UUID.randomUUID().toString());
       				String passwd = AlipaySubmit.query_timestamp();
       				String passwdorg = "CT_" + passwd;
       				String encryptPwd = MD5.sign(passwdorg, "CT", "utf-8");
       				ct.setUPassword(encryptPwd);
       				ct.setUState("1");
       				ct.setURestype("2");// 设置注册方式
       				ct.setUQqloginUserid(openID);
       				// ct.setRId((long) 0);
       				Long id = this.userManager.save(ct);

       				// 关联优惠券
       				// 样品券 7 *10
       				// 注册50-10 8 *10
       				// 注册100-20 9 *10
       				// 注册250-50 10 *4
       				// 注册500-100 11 *3
       				// 注册1000-200 12 *1
       				//couponDetailManager.udpateCouponDetails(7, id, 10);
       				//couponDetailManager.udpateCouponDetails(8, id, 10);
       				//couponDetailManager.udpateCouponDetails(9, id, 10);
       				//couponDetailManager.udpateCouponDetails(10, id, 4);
       				//couponDetailManager.udpateCouponDetails(11, id, 3);
       				//couponDetailManager.udpateCouponDetails(12, id, 1);
       				//couponDetailManager.udpateCouponDetails(61, id, 2);//周年庆优惠券
//    				if(new SimpleDateFormat("yyyy-MM-dd").format(new Date()).equals("2016-11-11")){
//    					couponDetailManager.udpateCouponDetails(125, id, 1);//注册15优惠券
//    				}
    				couponDetailManager.udpateCouponDetails(125, id, 1);//注册15优惠券
       				ct.setULastTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
       						.format((new Date())));
       				HttpServletRequest req = ServletActionContext.getRequest();
       				String Ip =UtilDate.getIpAddr(req);
       				ct.setULastIp(Ip);
       				ct.setURegTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format((new Date())));
       				userManager.update(ct);
       				this.session.remove("userinfosession");
       				this.session.put("userinfosession", ct);
       			}
            	   
            	   
            	   
               } else {
                   ////out.println("很抱歉，我们没能正确获取到您的信息，原因是： " + userInfoBean.getMsg());
               }
               PageFans pageFansObj = new PageFans(accessToken, openID);
               PageFansBean pageFansBean = pageFansObj.checkPageFans("97700000");
               if (pageFansBean.getRet() == 0) {
                   ////out.println("<p>验证您" + (pageFansBean.isFans() ? "是" : "不是")  + "QQ空间97700000官方认证空间的粉丝</p>");
               } else {
               }
               com.qq.connect.api.weibo.UserInfo weiboUserInfo = new com.qq.connect.api.weibo.UserInfo(accessToken, openID);
               com.qq.connect.javabeans.weibo.UserInfoBean weiboUserInfoBean = weiboUserInfo.getUserInfo();
               if (weiboUserInfoBean.getRet() == 0) {

                   StringBuffer sb = new StringBuffer();
                   sb.append("<p>所在地:" + weiboUserInfoBean.getCountryCode() + "-" + weiboUserInfoBean.getProvinceCode() + "-" + weiboUserInfoBean.getCityCode()
                            + weiboUserInfoBean.getLocation());

                   ArrayList<Company> companies = weiboUserInfoBean.getCompanies();
                   if (companies.size() > 0) {
                       for (int i=0, j=companies.size(); i<j; i++) {
                           sb.append("<p>曾服役过的公司：公司ID-" + companies.get(i).getID() + " 名称-" +
                           companies.get(i).getCompanyName() + " 部门名称-" + companies.get(i).getDepartmentName() + " 开始工作年-" +
                           companies.get(i).getBeginYear() + " 结束工作年-" + companies.get(i).getEndYear());
                       }
                   } else {
                   }

               } else {
               }


           }
       } catch (QQConnectException e) {
    	   e.printStackTrace();
       } catch (MalformedURLException e) {
		e.printStackTrace();
	} catch (DocumentException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
		return SUCCESS;
	}
	
	
	

	// 阿里快捷登录返回处理业务
	public String aliLoginResult() throws MalformedURLException,
			DocumentException, IOException {
		// 获取支付宝GET过来反馈信息
		HttpServletRequest requ = ServletActionContext.getRequest();
		Map<String, String> params = new HashMap<String, String>();
		Map requestParams = requ.getParameterMap();
		for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
			String name = (String) iter.next();
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i]
						: valueStr + values[i] + ",";
			}
			// 乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
			params.put(name, valueStr);
		}

		// 获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以下仅供参考)//
		// 支付宝用户号

		String user_id = new String(requ.getParameter("user_id").getBytes(
				"ISO-8859-1"), "UTF-8");
		// out.println("user_id:"+user_id);
		// out.println();
		// 授权令牌
		String token = new String(requ.getParameter("token").getBytes(
				"ISO-8859-1"), "UTF-8");
		// out.println("token:"+token);
		// out.println();
		// email
		String real_name = requ.getParameter("real_name");
		// out.println("email:"+email);
		// out.println();
		// 获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以上仅供参考)//

		// 计算得出通知验证结果
		boolean verify_result = AlipayNotify.verify(params);

		if (verify_result) {// 验证成功
			// ////////////////////////////////////////////////////////////////////////////////////////
			// 请在这里加上商户的业务逻辑程序代码
			// ——请根据您的业务逻辑来编写程序（以下代码仅作参考）——

			// 判断是否在商户网站中已经做过了这次通知返回的处理
			// 如果没有做过处理，那么执行商户的业务程序
			// 如果有做过处理，那么不执行商户的业务程序

			// 查看是否用阿里账号登录过
			CtUser aliUser = userManager.findAliUserId(user_id);
			if (aliUser != null) {
				aliUser.setULastTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
						.format((new Date())));
				HttpServletRequest req = ServletActionContext.getRequest();
				String Ip =UtilDate.getIpAddr(req);
				aliUser.setULastIp(Ip);
				userManager.update(aliUser);
				this.session.remove("userinfosession");
				this.session.put("userinfosession", aliUser);
			} else {
				CtUser ct = new CtUser();
				if(real_name == null){
					real_name = "易购用户";
				}
				ct.setUUserid("CT_"
						+ real_name
						+ "_"
						+ new SimpleDateFormat("yyyy-MM-ddHH:mm:ss")
								.format(new Date()));
				ct.setUUsername("CT_" + real_name);
				ct.setUCode(UUID.randomUUID().toString());
				String passwd = AlipaySubmit.query_timestamp();
				String passwdorg = "CT_" + passwd;
				String encryptPwd = MD5.sign(passwdorg, "CT", "utf-8");
				ct.setUPassword(encryptPwd);
				ct.setUState("1");
				ct.setURestype("3");// 设置注册方式
				ct.setUAliloginUserid(user_id);
				// ct.setRId((long) 0);
				Long id = this.userManager.save(ct);

				// 关联优惠券
				// 样品券 7 *10
				// 注册50-10 8 *10
				// 注册100-20 9 *10
				// 注册250-50 10 *4
				// 注册500-100 11 *3
				// 注册1000-200 12 *1
				//couponDetailManager.udpateCouponDetails(7, id, 10);
				//couponDetailManager.udpateCouponDetails(8, id, 10);
				//couponDetailManager.udpateCouponDetails(9, id, 10);
				//couponDetailManager.udpateCouponDetails(10, id, 4);
				//couponDetailManager.udpateCouponDetails(11, id, 3);
				//couponDetailManager.udpateCouponDetails(12, id, 1);
				//couponDetailManager.udpateCouponDetails(41, id, 1);//注册送十元无限制优惠券
				//couponDetailManager.udpateCouponDetails(61, id, 2);//周年庆优惠券
//				if(new SimpleDateFormat("yyyy-MM-dd").format(new Date()).equals("2016-11-11")){
//					couponDetailManager.udpateCouponDetails(125, id, 1);//注册15优惠券
//				}
				couponDetailManager.udpateCouponDetails(125, id, 1);//注册15优惠券
				ct.setULastTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
						.format((new Date())));
				HttpServletRequest req = ServletActionContext.getRequest();
				String Ip =UtilDate.getIpAddr(req);
				ct.setULastIp(Ip);
				ct.setURegTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format((new Date())));
				userManager.update(ct);
				this.session.remove("userinfosession");
				this.session.put("userinfosession", ct);
			}

			// 该页面可做页面美工编辑
			// out.println("验证成功");

			// ——请根据您的业务逻辑来编写程序（以上代码仅作参考）——

			// ////////////////////////////////////////////////////////////////////////////////////////
		} else {
			// 该页面可做页面美工编辑
			// out.println("验证失败");
		}
		return SUCCESS;
	}
	private String exTenUserId;
	
	public String getExTenUserId() {
		return exTenUserId;
	}

	public void setExTenUserId(String exTenUserId) {
		this.exTenUserId = exTenUserId;
	}

	private CtExtension extension = new CtExtension();
	
	public CtExtension getExtension() {
		return extension;
	}

	public void setExtension(CtExtension extension) {
		this.extension = extension;
	}

	// ע���û�
	public String register() throws Exception {
		try {
			if (ctUser != null) {
				CtUser ct = new CtUser();
				ct.setUUserid(ctUser.getUUserid());
				ct.setUUsername(ctUser.getUUserid());
				ct.setUCode(UUID.randomUUID().toString());
				String passwd = ctUser.getUPassword();
				String passwdorg = "CT" + ctUser.getUUserid() + passwd;
				String encryptPwd = MD5.sign(passwdorg, "CT", "utf-8");
				ct.setUPassword(encryptPwd);
				ct.setUMb(ctUser.getUMb());
				ct.setUState("1");
				ct.setURestype("0");
				// ct.setRId((long) 0);
				Long id = this.userManager.save(ct);

				// 关联优惠券
				// 样品券 7 *10
				// 注册50-10 8 *10
				// 注册100-20 9 *10
				// 注册250-50 10 *4
				// 注册500-100 11 *3
				// 注册1000-200 12 *1
				//couponDetailManager.udpateCouponDetails(7, id, 10);
				//couponDetailManager.udpateCouponDetails(8, id, 10);
				//couponDetailManager.udpateCouponDetails(9, id, 10);
				//couponDetailManager.udpateCouponDetails(10, id, 4);
				//couponDetailManager.udpateCouponDetails(11, id, 3);
				//couponDetailManager.udpateCouponDetails(12, id, 1);
				//couponDetailManager.udpateCouponDetails(41, id, 1);//注册送十元无限制优惠券
				////couponDetailManager.udpateCouponDetails(61, id, 2);//周年庆优惠券
//				if(new SimpleDateFormat("yyyy-MM-dd").format(new Date()).equals("2016-11-11")){
//					couponDetailManager.udpateCouponDetails(125, id, 1);//注册15优惠券
//				}
				couponDetailManager.udpateCouponDetails(125, id, 1);//注册15优惠券
				if(ctUser.getExTenUserId() != null && !ctUser.getExTenUserId().equals("")){
					CtUser tuiGuangMa = userManager.findExTeById(ctUser.getExTenUserId());
					if(tuiGuangMa != null){
						extension.setExtensionUserid(tuiGuangMa.getUId());
						extension.setExtensionName(ctUser.getUUserid());
						extension.setExtensionTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						extension.setExtensionDesc("通过推荐用户 "+ctUser.getUUserid()+" 获得优惠卷");
						extension.setExtensionMoney("10");
						userManager.updateExTen(extension);
						couponDetailManager.udpateCouponDetails(41, id, 1);
						couponDetailManager.udpateCouponDetails(41, tuiGuangMa.getUId(), 1);
					}
				}
				this.session.remove("userinfosession");
				this.session.put("userinfosession", ct);
				ct.setULastTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
						.format((new Date())));
				HttpServletRequest req = ServletActionContext.getRequest();
				String Ip =UtilDate.getIpAddr(req);
				ct.setULastIp(Ip);
				ct.setURegTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format((new Date())));
				userManager.update(ct);
				if(ct.getUMb() != null){
					//发送注册成功短信通知
					s.regMess(ct.getUMb(), ct.getUUserid());
				}
				return "register";
			} else {
				return "error";
			}
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = "注册失败,请重新注册!";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
	}
	private serviceInterface s = new serviceInterface();
	
	public serviceInterface getS() {
		return s;
	}

	public void setS(serviceInterface s) {
		this.s = s;
	}

	public String registerWeix() throws Exception {
		try {
			if (ctUser != null) {
            	CtUser ct = new CtUser();
   				ct.setUUserid(ctUser.getUUserid());
   				ct.setUUsername(ctUser.getUUserid());
   				ct.setUCode(UUID.randomUUID().toString());
   				String passwd = ctUser.getUPassword();
   				String passwdorg = "CT"+ctUser.getUUserid() + passwd;
   				String encryptPwd = MD5.sign(passwdorg, "CT", "utf-8");
   				ct.setUPassword(encryptPwd);
   				ct.setUMb(ctUser.getUMb());
   				ct.setUState("1");
   				ct.setURestype("4");// 设置注册方式
   				ct.setUWeixinloginUserid(ctUser.getUWeixinloginUserid());
   				// ct.setRId((long) 0);
   				Long id = this.userManager.save(ct);

   				// 关联优惠券
   				// 样品券 7 *10
   				// 注册50-10 8 *10
   				// 注册100-20 9 *10
   				// 注册250-50 10 *4
   				// 注册500-100 11 *3
   				// 注册1000-200 12 *1
   				//couponDetailManager.udpateCouponDetails(7, id, 10);
   				//couponDetailManager.udpateCouponDetails(8, id, 10);
   				//couponDetailManager.udpateCouponDetails(9, id, 10);
   				//couponDetailManager.udpateCouponDetails(10, id, 4);
   				//couponDetailManager.udpateCouponDetails(11, id, 3);
   				//couponDetailManager.udpateCouponDetails(12, id, 1);
   				//couponDetailManager.udpateCouponDetails(41, id, 1);//注册送十元无限制优惠券
   				//couponDetailManager.udpateCouponDetails(61, id, 2);//周年庆优惠券
//				if(new SimpleDateFormat("yyyy-MM-dd").format(new Date()).equals("2016-11-11")){
//					couponDetailManager.udpateCouponDetails(125, id, 1);//注册15优惠券
//				}
   				couponDetailManager.udpateCouponDetails(125, id, 1);//注册15优惠券
   				ct.setULastTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
   						.format((new Date())));
   				HttpServletRequest req = ServletActionContext.getRequest();
   				String Ip =UtilDate.getIpAddr(req);
   				ct.setULastIp(Ip);
   				ct.setURegTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format((new Date())));
   				userManager.update(ct);
   				this.session.remove("userinfosession");
   				this.session.put("userinfosession", ct);
//				CtUser ct = new CtUser();
//				ct.setUUserid(ctUser.getUUserid());
//				ct.setUUsername(ctUser.getUUserid());
//				ct.setUCode(UUID.randomUUID().toString());
//				String passwd = ctUser.getUPassword();
//				String passwdorg = "CT" + ctUser.getUUserid() + passwd;
//				String encryptPwd = MD5.sign(passwdorg, "CT", "utf-8");
//				ct.setUPassword(encryptPwd);
//				ct.setUMb(ctUser.getUMb());
//				ct.setUState("1");
//				ct.setURestype("0");
//				// ct.setRId((long) 0);
//				Long id = this.userManager.save(ct);
//				
//				// 关联优惠券
//				// 样品券 7 *10
//				// 注册50-10 8 *10
//				// 注册100-20 9 *10
//				// 注册250-50 10 *4
//				// 注册500-100 11 *3
//				// 注册1000-200 12 *1
//				couponDetailManager.udpateCouponDetails(7, id, 10);
//				couponDetailManager.udpateCouponDetails(8, id, 10);
//				couponDetailManager.udpateCouponDetails(9, id, 10);
//				couponDetailManager.udpateCouponDetails(10, id, 4);
//				couponDetailManager.udpateCouponDetails(11, id, 3);
//				couponDetailManager.udpateCouponDetails(12, id, 1);
//				if(ctUser.getExTenUserId() != null && !ctUser.getExTenUserId().equals("")){
//					CtUser tuiGuangMa = userManager.findExTeById(ctUser.getExTenUserId());
//					if(tuiGuangMa != null){
//						extension.setExtensionUserid(tuiGuangMa.getUId());
//						extension.setExtensionName(ctUser.getUUserid());
//						extension.setExtensionTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
//						extension.setExtensionDesc("通过推荐用户 "+ctUser.getUUserid()+" 获得优惠卷");
//						extension.setExtensionMoney("10");
//						userManager.updateExTen(extension);
//						couponDetailManager.udpateCouponDetails(41, id, 1);
//						couponDetailManager.udpateCouponDetails(41, tuiGuangMa.getUId(), 1);
//					}
//				}
//				this.session.put("userinfosession", ct);
//				ct.setULastTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
//				.format((new Date())));
//				userManager.update(ct);
//				
				
				
				
				return "register";
			} else {
				return "error";
			}
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = "注册失败,请重新注册!";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
	}
	// 过滤特殊字符  
	public static String StringFilter(String str) throws PatternSyntaxException   {     
	     // 只允许字母和数字       
	     // String   regEx  =  "[^a-zA-Z0-9]";                     
	     // 清除掉所有特殊字符  
	     String regEx="[`~!@#$^&*()+=|{}':;',\\[\\].<>/?~！@#￥……&*（）——+|{}【】‘；：”“’。，、？]";
	     Pattern p = Pattern.compile(regEx);
	     Matcher m = p.matcher(str);
	     return m.replaceAll("").trim();
	}
	// 过滤特殊字符  
	public static String StringFilter1(String str) throws PatternSyntaxException   {     
		// 只允许字母和数字       
		// String   regEx  =  "[^a-zA-Z0-9]";                     
		// 清除掉所有特殊字符  
		String regEx="[`~!@#$^&*()+=|{}':;',\\[\\].<>/?~！@#￥……&*（） ——+|{}【】‘；：”“’。，、？]";
		Pattern p = Pattern.compile(regEx);
		Matcher m = p.matcher(str);
		return m.replaceAll("").trim();
	}
    /**
     * 验证邮箱
     * @param email
     * @return
     */
    public static boolean checkEmail(String email){
        boolean flag = false;
        try{
                String check = "^([a-z0-9A-Z]+[-|_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
                Pattern regex = Pattern.compile(check);
                Matcher matcher = regex.matcher(email);
                flag = matcher.matches();
            }catch(Exception e){
                flag = false;
            }
        return flag;
    }
	// 邮箱注册
	public String registerEmail() throws Exception {

		if (ctUser != null) {
			try {
				CtUser ct = new CtUser();
				ct.setUUserid(ctUser.getUUserid());
				this.uid = ctUser.getUUserid();
				this.email = ctUser.getUEmail();
				if(!checkEmail(email)){
					String errorTotle = "检查输入完整性";
					String errorMess = "请检查邮箱地址";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", "login_goRegister");
					return ERROR;
				}
				CtUser cc = userManager.getCtUserByUEmail(email);
				if (cc != null) {
					String errorTotle = "完善输入完整性";
					String errorMess = "邮箱已存在";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", "login_goRegister");
					return ERROR;
				}
				ct.setUUsername(ctUser.getUUserid());
				ct.setUEmail(ctUser.getUEmail());
				CtUser cu = new CtUser();
				cu = userManager.getCtUserByUUserid(ct.getUUserid());
				HttpServletRequest requ = ServletActionContext.getRequest();
				if (cu == null) {
					ct.setUState("0");
					ct.setUCode(UUID.randomUUID().toString());
					String strBackUrl = "http://"
							+ requ.getServerName() // 服务器地址
							+ ":"
							+ requ.getServerPort() // 端口号
							+ requ.getContextPath() // 项目名称
							+ "/login_activate?uid=" + uid + "&ucode="
							+ ct.getUCode(); // 请求页面或其他地址
					//System.out.println(strBackUrl);
					//System.out.println(ct.getUCode());
					String passwd = ctUser.getUPassword();
					String passwdorg = "CT" + ctUser.getUUserid() + passwd;
					String encryptPwd = MD5.sign(passwdorg, "CT", "utf-8");
					ct.setUPassword(encryptPwd);
					ct.setURestype("1");
					Long id = this.userManager.save(ct);
					if (id != null) {
						StringBuffer sb = new StringBuffer();
						sb.append("<table bgcolor=\"#ffffff\" border=\"0\"  align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"700\">");
						sb.append("<tbody>");
						sb.append("<tr>");
						sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\" height=\"30\">&nbsp;</td>");
						sb.append("<td width=\"698\">&nbsp;</td>");
						sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\">&nbsp;</td>");
						sb.append("</tr>");
						sb.append("<tr>");
						sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\" height=\"55\">&nbsp;</td>");
						sb.append("<td width=\"698\" align=\"left\">");
						sb.append("<table>");
						sb.append("<tbody>");
						sb.append("<tr>");
						sb.append("<td valign=\"top\">");
						sb.append("<div style=\"margin:5px 50px 0px 44px; font-family: Myriad Pro, Lucida Grande, Helvetica, Geneva, Arial, Verdana, sans-serif; color:#000000;font-size:30px;line-height:1.3em; \">注册确认</div>");
						sb.append("</td>");
						sb.append("</tr>");
						sb.append("</tbody>");
						sb.append("</table>");
						sb.append("</td>");
						sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\">&nbsp;</td>");
						sb.append("</tr>");
						sb.append("<tr>");
						sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\" height=\"30\">&nbsp;</td>");
						sb.append("<td width=\"698\">&nbsp;</td>");
						sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\">&nbsp;</td>");
						sb.append("</tr>");
						sb.append("<tr>");
						sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\" height=\"96\">&nbsp;</td>");
						sb.append("<td width=\"698\" align=\"center\">");
						sb.append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"698\">");
						sb.append("<tbody>");
						sb.append("<tr>");
						sb.append("<td width=\"44\">&nbsp;</td>");
						sb.append("<td width=\"610\" align=\"left\">");
						sb.append("<div style=\"margin:10px 5px 0px 2px; font-family: Lucida Grande, Arial, Helvetica, Geneva, Verdana, sans-serif; color:#000000;font-size:14px;line-height:1.5em; \">");
						sb.append("<p>亲爱的<a target=\"_blank\" href=\"mailto:"
								+ this.email + "\">" + this.email + "</a>,</p>");
						sb.append("<p>欢迎使用长亭易购，为了方便以后通过安全邮箱来找回密码，请点击以下链接完成邮箱验证：</p>");
						sb.append("<p><a target=\"_blank\" href=\""
								+ strBackUrl + "\">点击验证</a></p>");
						sb.append("<p>（如果上面的链接无法直接点击，请复制链接到浏览器访问）</p>");
						sb.append("<p>如果验证链接已经失效，可在长亭易购的个人设置中; 重发验证邮件：）</p>");
						sb.append("<p>此为系统邮件，请勿回复！<br>");
						sb.append("<br>");
						sb.append("</div>");
						sb.append("</td>");
						sb.append("<td width=\"44\">&nbsp;</td>");
						sb.append("</tr>");
						sb.append("</tbody>");
						sb.append("</table>");
						sb.append("</td>");
						sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\">&nbsp;</td>");
						sb.append("</tr>");
						sb.append("<tr>");
						sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\" height=\"40\">&nbsp;</td>");
						sb.append("<td width=\"698\">&nbsp;</td>");
						sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\">&nbsp;</td>");
						sb.append("</tr>");
						sb.append("</tbody>");
						sb.append("</table>");
						sb.append("");
						String mail = sb.toString();
						MialUtil.sendMail(mail, email);
					} else {
						return "error";
					}
				} else {
					this.result = "error";
				}

			} catch (Exception e) {
				// TODO: handle exception
				//e.printStackTrace();
				String errorTotle = "糟糕,页面丢失了";
				String errorMess = " ";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				return ERROR;
			}
			return "registeremail";
		} else {
			return "error";
		}

	}

	// 再次发送邮件
	public String sendMailAgain() {
		if (ctUser != null) {
			String UUserid = ctUser.getUUserid();
			CtUser cu = userManager.getCtUserByUUserid(UUserid);
			this.email = cu.getUEmail();
			String code = cu.getUCode();
			HttpServletRequest requ = ServletActionContext.getRequest();
			String strBackUrl = "http://" + requ.getServerName() // 服务器地址
					+ ":" + requ.getServerPort() // 端口号
					+ requ.getContextPath() // 项目名称
					+ "/login_activate?uid=" + UUserid + "&ucode=" + code; // 请求页面或其他地址
			//System.out.println(strBackUrl);
			StringBuffer sb = new StringBuffer();
			sb.append("<table bgcolor=\"#ffffff\" border=\"0\"  align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"700\">");
			sb.append("<tbody>");
			sb.append("<tr>");
			sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\" height=\"30\">&nbsp;</td>");
			sb.append("<td width=\"698\">&nbsp;</td>");
			sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\">&nbsp;</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\" height=\"55\">&nbsp;</td>");
			sb.append("<td width=\"698\" align=\"left\">");
			sb.append("<table>");
			sb.append("<tbody>");
			sb.append("<tr>");
			sb.append("<td valign=\"top\">");
			sb.append("<div style=\"margin:5px 50px 0px 44px; font-family: Myriad Pro, Lucida Grande, Helvetica, Geneva, Arial, Verdana, sans-serif; color:#000000;font-size:30px;line-height:1.3em; \">注册确认</div>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</tbody>");
			sb.append("</table>");
			sb.append("</td>");
			sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\">&nbsp;</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\" height=\"30\">&nbsp;</td>");
			sb.append("<td width=\"698\">&nbsp;</td>");
			sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\">&nbsp;</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\" height=\"96\">&nbsp;</td>");
			sb.append("<td width=\"698\" align=\"center\">");
			sb.append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"698\">");
			sb.append("<tbody>");
			sb.append("<tr>");
			sb.append("<td width=\"44\">&nbsp;</td>");
			sb.append("<td width=\"610\" align=\"left\">");
			sb.append("<div style=\"margin:10px 5px 0px 2px; font-family: Lucida Grande, Arial, Helvetica, Geneva, Verdana, sans-serif; color:#000000;font-size:14px;line-height:1.5em; \">");
			sb.append("<p>亲爱的<a target=\"_blank\" href=\"mailto:" + this.email
					+ "\">" + this.email + "</a>,</p>");
			sb.append("<p>欢迎使用长亭易购，为了方便以后通过安全邮箱来找回密码，请点击以下链接完成邮箱验证：</p>");
			sb.append("<p><a target=\"_blank\" href=\"" + strBackUrl
					+ "\">点击验证</a></p>");
			sb.append("<p>（如果上面的链接无法直接点击，请复制链接到浏览器访问）</p>");
			sb.append("<p>如果验证链接已经失效，可在长亭易购的个人设置中; 重发验证邮件：）</p>");
			sb.append("<p>此为系统邮件，请勿回复！<br>");
			sb.append("<br>");
			sb.append("</div>");
			sb.append("</td>");
			sb.append("<td width=\"44\">&nbsp;</td>");
			sb.append("</tr>");
			sb.append("</tbody>");
			sb.append("</table>");
			sb.append("</td>");
			sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\">&nbsp;</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\" height=\"40\">&nbsp;</td>");
			sb.append("<td width=\"698\">&nbsp;</td>");
			sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\">&nbsp;</td>");
			sb.append("</tr>");
			sb.append("</tbody>");
			sb.append("</table>");
			sb.append("");
			String mail = sb.toString();
			MialUtil.sendMail(mail, email);
			this.result = "success";
			return SUCCESS;
		} else {
			this.result = "error";
			return "error";
		}

	}

	// 邮箱注册后激活
	public String activate() {
		try {
			String UUserid = this.getUid();
			String UCode = this.getUcode();
			CtUser cu = userManager.getCtuserByUUseridAndUCode(UUserid, UCode);
			if (cu != null) {
				CtUser cu2 = userManager.getCtUserByUUserid(UUserid);
				cu2.setUState("1");
				cu.setULastTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
						.format((new Date())));
				HttpServletRequest req = ServletActionContext.getRequest();
				String Ip =UtilDate.getIpAddr(req);
				cu2.setULastIp(Ip);
				cu.setURegTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format((new Date())));
				
				userManager.update(cu);
				userManager.update(cu2);
				Long id = cu2.getUId();
				this.session.remove("userinfosession");
				this.session.put("userinfosession", cu2);
				// 关联优惠券
				// 样品券 7 *10
				// 注册50-10 8 *10
				// 注册100-20 9 *10
				// 注册250-50 10 *4
				// 注册500-100 11 *3
				// 注册1000-200 12 *1
				//couponDetailManager.udpateCouponDetails(7, id, 10);
				//couponDetailManager.udpateCouponDetails(8, id, 10);
				//couponDetailManager.udpateCouponDetails(9, id, 10);
				//couponDetailManager.udpateCouponDetails(10, id, 4);
				//couponDetailManager.udpateCouponDetails(11, id, 3);
				//couponDetailManager.udpateCouponDetails(12, id, 1);
				//couponDetailManager.udpateCouponDetails(41, id, 1);//注册送十元无限制优惠券
				//couponDetailManager.udpateCouponDetails(61, id, 2);//周年庆优惠券
//				if(new SimpleDateFormat("yyyy-MM-dd").format(new Date()).equals("2016-11-11")){
//					couponDetailManager.udpateCouponDetails(125, id, 1);//注册15优惠券
//				}
				couponDetailManager.udpateCouponDetails(125, id, 1);//注册15优惠券
				if(cu2.getExTenUserId() != null && !cu2.getExTenUserId().equals("")){
					CtUser tuiGuangMa = userManager.findExTeById(cu2.getExTenUserId());
					if(tuiGuangMa != null){
						extension.setExtensionUserid(tuiGuangMa.getUId());
						extension.setExtensionName(cu2.getUUserid());
						extension.setExtensionTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						extension.setExtensionDesc(cu2.getUUserid());
						userManager.updateExTen(extension);
						couponDetailManager.udpateCouponDetails(41, id, 1);
						couponDetailManager.udpateCouponDetails(41, tuiGuangMa.getUId(), 1);
					}
				}

			} else {
				return "unActivate";
			}

			return "activate";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "unActivate";
		}

	}

	// ����û�������ֻ���Ƿ����
	public String checkUserName() {
		String UUserid = ctUser.getUUserid();
		CtUser ctUser = userManager.getCtUserByUUserid(UUserid);
		if (ctUser == null) {
			CtUser ctUser2 = userManager.getCtUserByUMb(UUserid);
			if (ctUser2 == null) {
				this.result = "error";
			} else {
				this.result = "success";
			}
		} else {
			this.result = "success";
		}

		return SUCCESS;
	}

	public String checkLogin() throws Exception {

		//判断当前网卡是否可登陆
		Boolean isCanLogin = userManager.canLoginByMac(MacUtil.getMacAddress());
		if(!isCanLogin){
			this.result = "error_EN";
			return SUCCESS;
		}
		
		String uname = this.getUname();
		CtUser ctUser = userManager.getCtuserByUname(uname);
		if (ctUser == null) {
			//记录登陆失败
			loginFailed = new CtLoginFailed();
			loginFailed.setFlginName(uname);
			loginFailed.setFloginTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			loginFailed.setFnetworkMac(MacUtil.getMacAddress());
			loginFailed.setFnameOrPass(false);
			userManager.saveLoginFailed(loginFailed);
			this.result = "error";
		} else {
			this.result = "success";
		}
		return SUCCESS;
	}

	public void removeLoginSession(){
		System.out.println("登录session已记录");
		Timer myTimer = new Timer();  
		
		myTimer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				//定时解锁
				System.err.println("登录超时");
				session.remove("userinfosession");
				System.gc();
			}
		}, 1000 * 60 * 30);
	}
	
	public String checkpwd() throws Exception {

		//判断当前网卡是否可登陆
		Boolean isCanLogin = userManager.canLoginByMac(MacUtil.getMacAddress());
		if(!isCanLogin){
			this.result = "error_EN";
			return SUCCESS;
		}
		
		//判断当前用户是否可以登陆
		
		
		String uname = this.getUname();
		String pwd = this.getPwd();
		
		final CtUser c = userManager.getCtuserByUname(uname);
		//是否可以登录
		if(c.getUIsLock() == null || c.getUIsLock().booleanValue() == false){
			Boolean whenLogin = userManager.getPassErrorSumByName(uname);
			if(!whenLogin){
				this.result = "error_NN";
				//加锁
				c.setUIsLock(true);
				userManager.update(c);
				
				
				Timer myTimer = new Timer();  
				
				myTimer.schedule(new TimerTask() {
					
					@Override
					public void run() {
						//定时解锁
						c.setUIsLock(false);
						userManager.update(c);
					}
				}, 1000 * 60 * 30);
				
				return SUCCESS;
			}
			CtUser ctUserp = userManager.getCtuserByPwd(uname, pwd);
			if (ctUserp != null) {
				this.result = "success";
			} else {
				//记录登陆失败
				loginFailed = new CtLoginFailed();
				loginFailed.setFlginName(uname);
				loginFailed.setFloginTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				loginFailed.setFnetworkMac(MacUtil.getMacAddress());
				loginFailed.setFnameOrPass(true);
				userManager.saveLoginFailed(loginFailed);
				this.result = "error";
			}
		} 
//		this.result = "error_NN";
		return SUCCESS;
	}

	// ��֤�һ�����ʱ�������֤���Ƿ���ȷ
	public String checkYzm() {
		String yzm = this.getYzm();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < yzm.length(); i++) {
			char c = yzm.charAt(i);
			if (Character.isLowerCase(c)) {
				sb.append(Character.toUpperCase(c));
			} else {
				sb.append(c);
			}
		}
		String realyzm = (String) this.getSession().get("rand");
		if (sb.toString().equals(realyzm)) {
			this.result = "success";
		} else {
			this.result = "error";
		}
		return SUCCESS;
	}

	// �һ�����1
	public String findpwd1() {
		try {
			String UUserid = ctUser.getUUserid();
			this.ctUser = userManager.getCtUserByUUnameOrUMb(UUserid);
			return "findpwd1";
		} catch (Exception e) {
			//e.printStackTrace();
			String errorTotle = "404··页面丢失了";
			String errorMess = "页面丢失了";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
		}
		return ERROR;
	}

	// �һ�����1
	public String gofindpwd3() {
		try {
			String UUserid = ctUser.getUUserid();
			this.ctUser = userManager.getCtUserByUUserid(UUserid);
			return "gofindpwd3";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			String errorTotle = "404··页面丢失了";
			String errorMess = "页面丢失了";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
		}
		return ERROR;
	}

	// 通过邮箱找回密码
	public String gofindpwdByEmail() {
		try {
			String UUserid = ctUser.getUUserid();
			this.ctUser = userManager.getCtUserByUUserid(UUserid);
			String code = ctUser.getUCode();
			this.email = ctUser.getUEmail();
			HttpServletRequest requ = ServletActionContext.getRequest();
			String strBackUrl = "http://"
					+ requ.getServerName() // 服务器地址
					+ ":"
					+ requ.getServerPort() // 端口号
					+ requ.getContextPath() // 项目名称
					+ "/login_gofindpwd4?ctUser.UUserid=" + UUserid + "&ucode="
					+ code; // 请求页面或其他地址
			//System.out.println(strBackUrl);
			StringBuffer sb = new StringBuffer();
			sb.append("<table bgcolor=\"#ffffff\" border=\"0\"  align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"700\">");
			sb.append("<tbody>");
			sb.append("<tr>");
			sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\" height=\"30\">&nbsp;</td>");
			sb.append("<td width=\"698\">&nbsp;</td>");
			sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\">&nbsp;</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\" height=\"55\">&nbsp;</td>");
			sb.append("<td width=\"698\" align=\"left\">");
			sb.append("<table>");
			sb.append("<tbody>");
			sb.append("<tr>");
			sb.append("<td valign=\"top\">");
			sb.append("<div style=\"margin:5px 50px 0px 44px; font-family: Myriad Pro, Lucida Grande, Helvetica, Geneva, Arial, Verdana, sans-serif; color:#000000;font-size:30px;line-height:1.3em; \">找回密码</div>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</tbody>");
			sb.append("</table>");
			sb.append("</td>");
			sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\">&nbsp;</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\" height=\"30\">&nbsp;</td>");
			sb.append("<td width=\"698\">&nbsp;</td>");
			sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\">&nbsp;</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\" height=\"96\">&nbsp;</td>");
			sb.append("<td width=\"698\" align=\"center\">");
			sb.append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"698\">");
			sb.append("<tbody>");
			sb.append("<tr>");
			sb.append("<td width=\"44\">&nbsp;</td>");
			sb.append("<td width=\"610\" align=\"left\">");
			sb.append("<div style=\"margin:10px 5px 0px 2px; font-family: Lucida Grande, Arial, Helvetica, Geneva, Verdana, sans-serif; color:#000000;font-size:14px;line-height:1.5em; \">");
			sb.append("<p>亲爱的<a target=\"_blank\" href=\"mailto:" + this.email
					+ "\">" + this.email + "</a>,</p>");
			sb.append("<p>请点击一下链接找回密码：</p>");
			sb.append("<p><a target=\"_blank\" href=\"" + strBackUrl
					+ "\">找回密码</a></p>");
			sb.append("<p>（如果上面的链接无法直接点击，请复制链接到浏览器访问）</p>");
			sb.append("<p>如果验证链接已经失效，可在长亭易购的个人设置中; 重发验证邮件：）</p>");
			sb.append("<p>此为系统邮件，请勿回复！<br>");
			sb.append("<br>");
			sb.append("</div>");
			sb.append("</td>");
			sb.append("<td width=\"44\">&nbsp;</td>");
			sb.append("</tr>");
			sb.append("</tbody>");
			sb.append("</table>");
			sb.append("</td>");
			sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\">&nbsp;</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\" height=\"40\">&nbsp;</td>");
			sb.append("<td width=\"698\">&nbsp;</td>");
			sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\">&nbsp;</td>");
			sb.append("</tr>");
			sb.append("</tbody>");
			sb.append("</table>");
			sb.append("");
			String mail = sb.toString();
			MialUtil.sendMail(mail, email);
			return "gofindpwdByEmail";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
	}

	//
	public String gofindpwd4() {
		try {
			String UUserid = ctUser.getUUserid();
			this.ctUser = userManager.getCtUserByUUserid(UUserid);
			return "gofindpwd4";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			String errorTotle = "404··页面丢失了";
			String errorMess = "页面丢失了";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
		}
		return ERROR;
	}

	// �޸�����
	public String updatePasswd() throws Exception {

		String UUserid = ctUser.getUUserid();
		CtUser cu = userManager.getCtUserByUUserid(UUserid);
		String passwd = ctUser.getUPassword();
		String passwdorg = "CT" + ctUser.getUUserid() + passwd;
		String encryptPwd = MD5.sign(passwdorg, "CT", "utf-8");
		cu.setUPassword(encryptPwd);
		userManager.update(cu);
		this.session.remove("userinfosession");
		this.session.put("userinfosession", cu);
		return "updatePasswd";
	}

	private CtLoginFailed loginFailed = new CtLoginFailed();
	
	public CtLoginFailed getLoginFailed() {
		return loginFailed;
	}

	public void setLoginFailed(CtLoginFailed loginFailed) {
		this.loginFailed = loginFailed;
	}

	// ��ͨ�û���¼
	public String login() throws Exception {


		try {
			
			
			String isAutoLogin = this.getIsAutoLogin();
			if (ctUser != null) {
				String uname2 = this.ctUser.getUUserid();
				String password = this.ctUser.getUPassword();
				if ("autoLogin".equals(isAutoLogin)) {
					// 自动登录
					if (uname2.indexOf("@") != -1) {
						// 通过邮箱登录
						CtUser cu = userManager.affimByEmailAndPwd(uname2,
								password);
						if (cu != null) {
							if ("1".equals(cu.getUState())) {
								this.session.remove("userinfosession");
								this.session.put("userinfosession", cu);
								Cookie cookie = new Cookie("usercookie", uname2
										+ "==" + password);
								cookie.setMaxAge(24 * 60 * 60);
								response.addCookie(cookie);
								cu.setULastTime(new SimpleDateFormat(
										"yyyy-MM-dd HH:mm:ss")
										.format((new Date())));
								HttpServletRequest req = ServletActionContext.getRequest();
								String Ip =UtilDate.getIpAddr(req);
								cu.setULastIp(Ip);
								userManager.update(cu);
								Long cartcount = basicManager
										.getCartGoodsNum(cu.getUId());
								this.session.put("islgn", cartcount);
								this.session.remove("userinfosession");
								this.session.put("userinfosession", cu);
								String activity = (String) session.get("activitynologin");
								if(activity != null){
									session.remove("activitynologin");
									return "goactive";
								}
								return "login_user";
							} else {
								// 邮箱没激活
								this.setMessage("邮箱没激活");
								session.put("message", message);
								return "login";
							}
						} else {
							// 用户名错
							this.setMessage("用户名或者密码错误");
							return "login";
						}
					} else {
						// 通过手机号或者用户名登录
						CtUser cu = userManager.affirm(uname2, password);
						if (cu != null) {
							if ("1".equals(cu.getUState())) {
								cu.setULastTime(new SimpleDateFormat(
										"yyyy-MM-dd HH:mm:ss")
										.format((new Date())));
								HttpServletRequest req = ServletActionContext.getRequest();
								String Ip =UtilDate.getIpAddr(req);
								cu.setULastIp(Ip);
								userManager.update(cu);
								Cookie cookie = new Cookie("usercookie", uname2
										+ "==" + password);
								cookie.setMaxAge(24 * 60 * 60);
								response.addCookie(cookie);
								Long cartcount = basicManager
										.getCartGoodsNum(cu.getUId());
								this.session.put("islgn", cartcount);
								this.session.remove("userinfosession");
								this.session.put("userinfosession", cu);
								String activity = (String) session.get("activitynologin");
								if(activity != null){
									session.remove("activitynologin");
									return "goactive";
								}
								return "login_user";
							} else {
								// 非法登录
								this.setMessage("该用户名不是通过手机注册的，请使用手机注册或者通过邮箱注册");
								return "login";
							}
						} else {
							this.setMessage("用户名或者密码错误");
							return "login";
						}
					}
				} else {
					// 没有选择自动登录
					if (uname2.indexOf("@") != -1) {
						// 通过邮箱登录
						CtUser cu = userManager.affimByEmailAndPwd(uname2,
								password);
						if (cu != null) {
							if ("1".equals(cu.getUState())) {
								this.session.remove("userinfosession");
								this.session.put("userinfosession", cu);
								if (this.session.get("referUrl") == null) {
									Long cartcount = basicManager
											.getCartGoodsNum(cu.getUId());
									this.session.put("islgn", cartcount);

									return "bom";
								} else {
									if (!"login_login.action"
											.equals(this.session
													.get("referUrl"))) {
										this.referUrl = (String) this.session
												.get("referUrl");
										this.session.remove("referUrl");
										Long cartcount = basicManager
												.getCartGoodsNum(cu.getUId());
										this.session.put("islgn", cartcount);
										return "referUrl";
									} else {
										Long cartcount = basicManager
												.getCartGoodsNum(cu.getUId());
										this.session.put("islgn", cartcount);
										return "bom";
									}
								}
							} else {
								// 邮箱没激活
								this.setMessage("邮箱没激活");
								String mess = message;
								session.put("message", mess);
								return "login";
							}
						} else {
							// 用户名错
							this.setMessage("用户名或者密码错误");
							return "login";
						}
					} else {
						// 通过手机号或者用户名登录
						CtUser cu = userManager.affirm(uname2, password);
						this.ctUser.setUUserid(uname2);
						this.ctUser.setUPassword(password);
						if (cu != null) {
							if ("1".equals(cu.getUState())) {
								this.session.remove("userinfosession");
								this.session.put("userinfosession", cu);
								if (this.session.get("referUrl") == null) {
									Long cartcount = basicManager
											.getCartGoodsNum(cu.getUId());
									this.session.put("islgn", cartcount);
									cu.setULastTime(new SimpleDateFormat(
											"yyyy-MM-dd HH:mm:ss")
											.format((new Date())));
									HttpServletRequest req = ServletActionContext.getRequest();
									String Ip =UtilDate.getIpAddr(req);
									cu.setULastIp(Ip);
									userManager.update(cu);
									this.session.remove("userinfosession");
									this.session.put("userinfosession", cu);
									String activity = (String) session.get("activitynologin");
									if(activity != null){
										session.remove("activitynologin");
										return "goactive";
									}
									return "bom";
								} else {
									if (!"login_login.action"
											.equals(this.session
													.get("referUrl"))) {
										this.referUrl = (String) this.session
												.get("referUrl");
										cu.setULastTime(new SimpleDateFormat(
												"yyyy-MM-dd HH:mm:ss")
												.format((new Date())));
										HttpServletRequest req = ServletActionContext.getRequest();
										String Ip =UtilDate.getIpAddr(req);
										cu.setULastIp(Ip);
										userManager.update(cu);
										this.session.remove("referUrl");
										Long cartcount = basicManager
												.getCartGoodsNum(cu.getUId());
										this.session.put("islgn", cartcount);
										this.session.remove("userinfosession");
										this.session.put("userinfosession", cu);
										String activity = (String) session.get("activitynologin");
										if(activity != null){
											session.remove("activitynologin");
											return "goactive";
										}
										return "referUrl";
									} else {
										Long cartcount = basicManager
												.getCartGoodsNum(cu.getUId());
										this.session.put("islgn", cartcount);
										cu.setULastTime(new SimpleDateFormat(
												"yyyy-MM-dd HH:mm:ss")
												.format((new Date())));
										HttpServletRequest req = ServletActionContext.getRequest();
										String Ip =UtilDate.getIpAddr(req);
										cu.setULastIp(Ip);
										userManager.update(cu);
										this.session.remove("userinfosession");
										this.session.put("userinfosession", cu);
										String activity = (String) session.get("activitynologin");
										if(activity != null){
											session.remove("activitynologin");
											return "goactive";
										}
										return "bom";
									}
								}
							} else {
								// 非法登录
								this.setMessage("该用户名不是通过手机注册的，请使用手机注册或者通过邮箱注册");
								return "login";
							}
						} else {
							this.setMessage("用户名或者密码错误");
							
							return "login";
						}
					}
				}
			} else {
				return "login";
			}
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}

	}

	// 判断用户是否登录
	public String isLogin() {
		Map session = this.getSession();
		if (session.get("userinfosession") == null) {
			this.result = "error";
		} else {
			this.result = "success";
		}
		return SUCCESS;
	}

	// 退出登录
	public String logout() {
		this.session.remove("userinfosession");
		this.session.remove("islgn");
		return "logout";
	}

	// 通过层登录
	public String loginByLayer() {
		try {
			// CtUser cu = userManager.affirm(userDTO.getUUserid(),
			// userDTO.getUPassword());
			// if(cu != null){
			// this.session.put("userinfosession", cu);
			// this.result = "success";
			// }else {
			// this.result = "error";
			// }
			String uname2 = userDTO.getUUserid();
			String password = userDTO.getUPassword();
			if (uname2.indexOf("@") != -1) {
				// 通过邮箱登录
				CtUser cu = userManager.affimByEmailAndPwd(uname2, password);
				if (cu != null) {
					if ("1".equals(cu.getUState())) {
						cu.setULastTime(new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss").format((new Date())));
						HttpServletRequest req = ServletActionContext.getRequest();
						String Ip =UtilDate.getIpAddr(req);
						cu.setULastIp(Ip);
						userManager.update(cu);
						this.session.remove("userinfosession");
						this.session.put("userinfosession", cu);
						this.result = "success";
					} else {
						// 邮箱没激活
						this.result = "mailnotactive";
					}
				} else {
					// 用户名错
					this.result = "error";
				}
			} else {
				// 通过手机号或者用户名登录
				CtUser cu = userManager.affirm(uname2, password);
				if (cu != null) {
					if ("1".equals(cu.getUState())) {
						cu.setULastTime(new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss").format((new Date())));
						HttpServletRequest req = ServletActionContext.getRequest();
						String Ip =UtilDate.getIpAddr(req);
						cu.setULastIp(Ip);
						userManager.update(cu);
						this.session.remove("userinfosession");
						this.session.put("userinfosession", cu);
						this.result = "success";
					} else {
						// 非法登录
						this.result = "illegal";
					}
				} else {
					this.result = "error";
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
		return SUCCESS;
	}

	public CtUserManager getUserManager() {
		return userManager;
	}

	public void setUserManager(CtUserManager userManager) {
		this.userManager = userManager;
	}

	public CtUser getCtUser() {
		return ctUser;
	}

	public void setCtUser(CtUser ctUser) {
		this.ctUser = ctUser;
	}

	public CtUserDTO getUserDTO() {
		return userDTO;
	}

	public void setUserDTO(CtUserDTO userDTO) {
		this.userDTO = userDTO;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public CtSmsDTO getSmsDTO() {
		return smsDTO;
	}

	public void setSmsDTO(CtSmsDTO smsDTO) {
		this.smsDTO = smsDTO;
	}

	public CtSms getCtSms() {
		return ctSms;
	}

	public void setCtSms(CtSms ctSms) {
		this.ctSms = ctSms;
	}

	public Long getUId() {
		return UId;
	}

	public void setUId(Long uId) {
		UId = uId;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public String getYzm() {
		return yzm;
	}

	public void setYzm(String yzm) {
		this.yzm = yzm;
	}

	public String getIsAutoLogin() {
		return isAutoLogin;
	}

	public void setIsAutoLogin(String isAutoLogin) {
		this.isAutoLogin = isAutoLogin;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	// @Override
	// public void setServletResponse(HttpServletResponse arg0) {
	// // TODO Auto-generated method stub
	//
	// }

	public void setServletResponse(HttpServletResponse response) {
		this.response = response;
		this.response.setCharacterEncoding("UTF-8");
		this.response.setContentType("text/html");
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	@Override
	public void setServletRequest(HttpServletRequest arg0) {
		// TODO Auto-generated method stub
		this._reRequest = arg0;
	}

	public HttpServletRequest get_reRequest() {
		return _reRequest;
	}

	public void set_reRequest(HttpServletRequest _reRequest) {
		this._reRequest = _reRequest;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getUcode() {
		return ucode;
	}

	public void setUcode(String ucode) {
		this.ucode = ucode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getReferUrl() {
		return referUrl;
	}

	public void setReferUrl(String referUrl) {
		this.referUrl = referUrl;
	}

	public BasicManager getBasicManager() {
		return basicManager;
	}

	public void setBasicManager(BasicManager basicManager) {
		this.basicManager = basicManager;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	
	
	//w微博登录，获取url地址
	public String getWeiboUrl() throws WeiboException{
		org.ctonline.weibo4j.Oauth oauth = new org.ctonline.weibo4j.Oauth();
		String url = oauth.authorize("code");
		session.put("len", url);
		return SUCCESS;
	}
	private String code;
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	//微博登录成功回馈
	public String weiboafterlogin(){
		
		
		String accessToken = null ;
        String uid = null ;
        String screenName = null ;        
        String username = null ;
        org.ctonline.weibo4j.http.AccessToken accessTokenObj = null ;
        org.ctonline.weibo4j.Oauth oauth2 = new org.ctonline.weibo4j.Oauth();
        try {
            accessTokenObj = oauth2.getAccessTokenByCode(code) ;
            accessToken = accessTokenObj.getAccessToken() ;
            Account account = new Account(accessToken) ;
            JSONObject uidJson = account.getUid() ;
            uid = uidJson.getString("uid") ;
            Users users = new Users(accessToken) ;
            User weiboUser = users.showUserById(uid) ;
            username = weiboUser.getName() ;
            screenName = weiboUser.getScreenName() ;
            //检测是否使用微博登录过
            CtUser aliUser = userManager.findWeiBoUid(uid);
            if(aliUser != null){
            	aliUser.setULastTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
					.format((new Date())));
        		HttpServletRequest req = ServletActionContext.getRequest();
        		String Ip =UtilDate.getIpAddr(req);
        		aliUser.setULastIp(Ip);
				userManager.update(aliUser);
				this.session.remove("userinfosession");
				this.session.put("userinfosession", aliUser);
            } else {
            	CtUser ct = new CtUser();
   				ct.setUUserid("CT_"
   						+ screenName
   						+ "_"
   						+ new SimpleDateFormat("yyyy-MM-ddHH:mm:ss")
   								.format(new Date()));
   				ct.setUUsername("CT_" + screenName);
   				ct.setUCode(UUID.randomUUID().toString());
   				String passwd = AlipaySubmit.query_timestamp();
   				String passwdorg = "CT_" + passwd;
   				String encryptPwd = MD5.sign(passwdorg, "CT", "utf-8");
   				ct.setUPassword(encryptPwd);
   				ct.setUState("1");
   				ct.setURestype("5");// 设置注册方式
   				ct.setUWeibologinUserid(uid);
   				// ct.setRId((long) 0);
   				Long id = this.userManager.save(ct);

   				// 关联优惠券
   				// 样品券 7 *10
   				// 注册50-10 8 *10
   				// 注册100-20 9 *10
   				// 注册250-50 10 *4
   				// 注册500-100 11 *3
   				// 注册1000-200 12 *1
   				//couponDetailManager.udpateCouponDetails(7, id, 10);
   				//couponDetailManager.udpateCouponDetails(8, id, 10);
   				//couponDetailManager.udpateCouponDetails(9, id, 10);
   				//couponDetailManager.udpateCouponDetails(10, id, 4);
   				//couponDetailManager.udpateCouponDetails(11, id, 3);
   				//couponDetailManager.udpateCouponDetails(12, id, 1);
   				//couponDetailManager.udpateCouponDetails(41, id, 1);//注册送十元无限制优惠券
   				//couponDetailManager.udpateCouponDetails(61, id, 2);//周年庆优惠券
//				if(new SimpleDateFormat("yyyy-MM-dd").format(new Date()).equals("2016-11-11")){
//					couponDetailManager.udpateCouponDetails(125, id, 1);//注册15优惠券
//				}
   				couponDetailManager.udpateCouponDetails(125, id, 1);//注册15优惠券
   				ct.setULastTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
   						.format((new Date())));
   				HttpServletRequest req = ServletActionContext.getRequest();
   				String Ip =UtilDate.getIpAddr(req);
   				ct.setULastIp(Ip);
   				ct.setURegTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format((new Date())));
   				userManager.update(ct);
   				this.session.remove("userinfosession");
   				this.session.put("userinfosession", ct);
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        } catch (JSONException e) {
			//e.printStackTrace();
		}
        //out.println("微博访问Token_Info：" + accessTokenObj + "\t");
       // out.println("微博访问Token：" + accessToken + "\t");
        //out.println("微博用户-Uid：" + uid + "\t");
       // out.println("微博用户-名称：" + screenName + "\t");
 catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		
		//System.out.println(res);
		return SUCCESS;
	}
	
	// public String getPhoneYzm() {
	// return phoneYzm;
	// }
	//
	// public void setPhoneYzm(String phoneYzm) {
	// this.phoneYzm = phoneYzm;
	// }

	/*===============================以下为微信登录======================================*/
	//获取用户授权code
	public String weixinLogin(){
		Long time = new Date().getTime();
		String state = MD5.sign(time.toString(), "CT", "utf-8");
		String url = "https://open.weixin.qq.com/connect/qrconnect?appid="+AlipayConfig.weixinAppId+"&redirect_uri="+AlipayConfig.weixinResUrl+"&response_type=code&scope=snsapi_login&state="+state+"#wechat_redirect";
		session.put("len", url);
		return SUCCESS;
	}
	
	public String guanlianWeix(){
		String UUserid = ctUser.getUUserid();
		if(UUserid == null){
			String errorTotle = "数据异常";
			String errorMess = "请重新进行操作";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
		CtUser cu = userManager.getCtUserByUUserid(UUserid);
		String openid = ctUser.getUWeixinloginUserid();
		cu.setUWeixinloginUserid(openid);
		cu.setULastTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		
		HttpServletRequest req = ServletActionContext.getRequest();
		String Ip =UtilDate.getIpAddr(req);
		cu.setULastIp(Ip);
		
		userManager.update(cu);
		this.session.remove("userinfosession");
		this.session.put("userinfosession", cu);
		return SUCCESS;
	}
	public String weixinRegChong(){
		String errorTotle = "请勿重复提交";
		String errorMess = "请重新登录";
		session.put("errorMess", errorMess);
		session.put("errorTotle", errorTotle);
		return SUCCESS;
	}
	public String checkGuanLianWei(){
		String uname = this.getUname();
		CtUser ctUser = userManager.getCtuserByUname(uname);
		if(ctUser.getUWeixinloginUserid() == null){
			this.result = "success";
		} else {
			this.result = "error";
		}
		return SUCCESS;
	}
	
	public String goGuanlian(){
		
		return SUCCESS;
	}
	
	public String weixinReslogin() {
		try {
			HttpServletRequest req = ServletActionContext.getRequest();
			String query = req.getQueryString();
			if(query == null){
				return "nickNull";
			}
			String[] fen = query.split("&");
			String code1 = fen[0].substring(5);
			String url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid="+AlipayConfig.weixinAppId+"&secret="
					+AlipayConfig.weixinAppSecret+"&code="+code1+"&grant_type=authorization_code";
			
			HttpPost httpost= HttpClientConnectionManager.getPostMethod(url);
			HttpResponse response = GetWxOrderno.httpclient.execute(httpost);
			String jsonStr = EntityUtils.toString(response.getEntity(), "UTF-8");
			JSONObject map = new JSONObject(jsonStr);  
			String access_token = (String) map.get("access_token");
			//String access_token = "ax4g58r2hW1sh6K69d_K4AuFMoe_zXwM7NuBiL3iB1ShXEMG-4So8mZQ6uLIaFeClu3CTL7yAhdxkDqIETCYG465Lu_rzLYqo_7ZDd52MO4LLKhABAHHG";
			String openid = (String) map.get("openid");
			
			
			url = "https://api.weixin.qq.com/sns/userinfo?access_token="+access_token+"&openid="+openid;
			httpost= HttpClientConnectionManager.getPostMethod(url);
			response = GetWxOrderno.httpclient.execute(httpost);
			jsonStr = EntityUtils.toString(response.getEntity(), "UTF-8");
			map = new JSONObject(jsonStr);  
			String nickname = (String) map.get("nickname");
			System.out.println(nickname);
			if(nickname == null){
				String errorTotle = "请求超时";
				String errorMess = "请重新登录";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				return "nickNull";
			}
			session.put("openid", openid);
			session.put("nickname", "CT_"+nickname);
			CtUser weiXinUser = userManager.findWeiXinOpenId(openid);
			if(weiXinUser != null){
				weiXinUser.setULastTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
					.format((new Date())));
				String Ip =UtilDate.getIpAddr(req);
				weiXinUser.setULastIp(Ip);
				userManager.update(weiXinUser);
				this.session.remove("userinfosession");
				this.session.put("userinfosession", weiXinUser);
				return "oldUserWeiXin";
            } else {
//            	CtUser ct = new CtUser();
//   				ct.setUUserid("CT_"
//   						+ nickname
//   						+ "_"
//   						+ new SimpleDateFormat("yyyy-MM-ddHH:mm:ss")
//   								.format(new Date()));
//   				ct.setUUsername("CT_" + nickname);
//   				ct.setUCode(UUID.randomUUID().toString());
//   				String passwd = AlipaySubmit.query_timestamp();
//   				String passwdorg = "CT_" + passwd;
//   				String encryptPwd = MD5.sign(passwdorg, "CT", "utf-8");
//   				ct.setUPassword(encryptPwd);
//   				ct.setUState("1");
//   				ct.setURestype("4");// 设置注册方式
//   				ct.setUWeixinloginUserid(openid);
//   				// ct.setRId((long) 0);
//   				Long id = this.userManager.save(ct);
//
//   				// 关联优惠券
//   				// 样品券 7 *10
//   				// 注册50-10 8 *10
//   				// 注册100-20 9 *10
//   				// 注册250-50 10 *4
//   				// 注册500-100 11 *3
//   				// 注册1000-200 12 *1
//   				couponDetailManager.udpateCouponDetails(7, id, 10);
//   				couponDetailManager.udpateCouponDetails(8, id, 10);
//   				couponDetailManager.udpateCouponDetails(9, id, 10);
//   				couponDetailManager.udpateCouponDetails(10, id, 4);
//   				couponDetailManager.udpateCouponDetails(11, id, 3);
//   				couponDetailManager.udpateCouponDetails(12, id, 1);
//
//   				ct.setULastTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
//   						.format((new Date())));
//   				userManager.update(ct);
//   				this.session.put("userinfosession", ct);
            }
			
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}
	
	public static String getJsonString(String urlPath) throws Exception {  
        URL url = new URL(urlPath);  
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();  
        connection.connect();  
        InputStream inputStream = connection.getInputStream();  
        //对应的字符编码转换  
        Reader reader = new InputStreamReader(inputStream, "UTF-8");  
        BufferedReader bufferedReader = new BufferedReader(reader);  
        String str = null;  
        StringBuffer sb = new StringBuffer();  
        while ((str = bufferedReader.readLine()) != null) {  
            sb.append(str);  
        }  
        reader.close();  
        connection.disconnect();  
        return sb.toString();  
    } 
	

	
	
	public String wxRegister(){
		

		code = userDTO.getCode();

		userDTO.setCode(code);
		
		if (code == null || code.equals("")){
			return "error";
		}
		

		try{

					
			//String urlwx= "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wx20d092d7dac249d5&secret=cac931764ee679a4b00abe35095cd7ec";
			//JSONObject jsonObject = new JSONObject(getJsonString(urlwx));  
			String access_token =getToken();  
			if (access_token.equals("")){
				access_token="GByQVidcbWLGuB-aV3BazMfnzabZiu2cVE5gSEBDM6dxDvqCEAMKiNhWRv8mdFGfKxXS_STeVM5uTh4XgXQivORqAhK9HWUnFZWmwknNpUkQEAcACAUIE";
			}
			
			
			
			
			String urlwx = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=wx20d092d7dac249d5&secret=cac931764ee679a4b00abe35095cd7ec&code="+code+"&grant_type=authorization_code";  
			JSONObject jsonObject = new JSONObject(getJsonString(urlwx));  
			//String access_token = jsonObject.getString("access_token");  
			String openid = jsonObject.getString("openid");
			
			
			//获取用户基本信息
			urlwx = "https://api.weixin.qq.com/cgi-bin/user/info?access_token="+access_token+"&openid="+openid+"&lang=zh_CN";
			jsonObject = new JSONObject(getJsonString(urlwx));  
			Long subscribe_time = jsonObject.getLong("subscribe_time");
			String nickname = jsonObject.getString("nickname");
			int subscribe = jsonObject.getInt("subscribe");//1为关注了，0为没关注
			int sex = jsonObject.getInt("sex");
					
			
			//this.request.put("code", code);
			this.request.put("openid", openid);
			this.request.put("subscribe_time", subscribe_time);
			this.request.put("nickname", nickname);
			this.request.put("subscribe", subscribe);
			this.request.put("sex", sex);
			this.request.put("access_token", getToken());
			return "wxRegister";
			
		}catch(Exception e){
			e.printStackTrace();
			return "error";
		}
	}
	
	public String wxhongbao(String openid,String desc,int rint) {
		try{
		
	//	this.request.put("openid", openid);
		//取100-500随机数
		Random r = new Random();
		//int rint = (int)(100+Math.random()*(300-100+1));
		//rint = 1000
				
			System.out.println(openid);	
		
		//红包记录
	/*
		CtHongbao hongbao = new CtHongbao();
		hongbao.setHongbaoUserid(id);
		hongbao.setHongbaoMoney(String.valueOf(rint));
		hongbao.setHongbaoOpenid(openid);
		hongbao.setHongbaoTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		hongbao.setHongbaoDesc(desc);
		userManager.saveHongBao(hongbao);
		*/
		String url = "https://api.mch.weixin.qq.com/mmpaymkttransfers/sendredpack";
		String orderNNo =  MoneyUtils.getOrderNo() ; 
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("nonce_str", MoneyUtils.buildRandom());//随机字符串
		map.put("mch_billno", orderNNo);//商户订单
		map.put("mch_id", "1251545501");//商户号
		map.put("wxappid", "wx20d092d7dac249d5");//商户appid
		map.put("nick_name", "长亭易购");//提供方名称
		map.put("send_name", "长亭易购");//用户名
		map.put("re_openid", openid);//用户openid
		map.put("total_amount", rint);//付款金额
	//map.put("min_value", 1);//最小红包
		//map.put("max_value", 5);//最大红包
		map.put("total_num", 1);//红包发送总人数
		map.put("wishing", desc);//红包祝福语
		map.put("client_ip", "127.0.0.1");//ip地址
		map.put("act_name", "16走来,17同行");//活动名称
		map.put("remark", "16走来,17同行");//备注
		map.put("sign", MoneyUtils.createSign(map));//签名
		
		String result = "";
			try {
				result = MoneyUtils.doSendMoney(url, MoneyUtils.createXML(map));
			} catch (Exception e) {
				e.printStackTrace();
			}
		System.out.println("result:"+result);
		// userDTO.setCode("result:"+openid);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return "wxhongbao";
	}
	
	public String register_wx() throws Exception {
		try {
			if (userDTO != null) {
				CtUser ct = new CtUser();
				ct.setUUserid(userDTO.getUMb());
				ct.setUUsername(userDTO.getUUsername());//微信昵称
				ct.setUCode(UUID.randomUUID().toString());
				ct.setUWeixinloginUserid(userDTO.getUWeixinloginUserid());//opesnid
				String passwd = userDTO.getUPassword();
				String passwdorg = "CT" + userDTO.getUMb() + passwd;
				String encryptPwd = MD5.sign(passwdorg, "CT", "utf-8");
				ct.setUPassword(encryptPwd);
				ct.setUMb(userDTO.getUMb());
				ct.setUState("1");
				ct.setURestype("4");
				if (userDTO.getUSex().equals("2")){
					ct.setUSex("女");
				}else{
					ct.setUSex("男");
				}
				
				
				
				//微信关注 公众号 时间

				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String date = sdf.format(new Date(Long.valueOf(userDTO.getUWeixingztime())*1000));
				ct.setUWeixingztime(date);
				// ct.setRId((long) 0);
				
				
				
				//String ran = Util.getRandom();
				
				Long id = this.userManager.save(ct);

				// 关联优惠券
				// 样品券 7 *10
				// 注册50-10 8 *10
				// 注册100-20 9 *10
				// 注册250-50 10 *4
				// 注册500-100 11 *3
				// 注册1000-200 12 *1
				//couponDetailManager.udpateCouponDetails(7, id, 10);
				//couponDetailManager.udpateCouponDetails(8, id, 10);
				//couponDetailManager.udpateCouponDetails(9, id, 10);
				//couponDetailManager.udpateCouponDetails(10, id, 4);
				//couponDetailManager.udpateCouponDetails(11, id, 3);
				//couponDetailManager.udpateCouponDetails(12, id, 1);
				//couponDetailManager.udpateCouponDetails(41, id, 1);//注册送十元无限制优惠券
				//couponDetailManager.udpateCouponDetails(61, id, 2);//周年庆优惠券
//				if(new SimpleDateFormat("yyyy-MM-dd").format(new Date()).equals("2016-11-11")){
//					couponDetailManager.udpateCouponDetails(125, id, 1);//注册15优惠券
//				}
				couponDetailManager.udpateCouponDetails(125, id, 1);//注册15优惠券
				this.session.remove("userinfosession");
				this.session.put("userinfosession", ct);
				ct.setULastTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
						.format((new Date())));
				HttpServletRequest req = ServletActionContext.getRequest();
				String Ip =UtilDate.getIpAddr(req);
				ct.setULastIp(Ip);
				//生成唯一码
				
				Random rand = new Random();
				int []ret=new int[4];
				String suiji = "";
				for(int i=0;i<ret.length;i++){
				   ret[i]=rand.nextInt(10);
				   suiji += ret[i];
				}
				
				ct.setUExtensionid(id+suiji);
				ct.setURegTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format((new Date())));
				userManager.update(ct);
				
				this.request.put("wym", "CT"+id+suiji);
				
				//发放红包
				//subscribe=1为已关注  subscribe=0未关注
			/*
				int subscribe = userDTO.getSubscribe();
				if (subscribe==1){
					
					wxhongbao(userDTO.getUWeixinloginUserid(),id);
				}
				
				*/
				return "register_wx";
			} else {
				return "error";
			}
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = "注册失败,请重新注册!";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
	}

	public String register_wx(CtUser ct) throws Exception {
		try {
			if (userDTO != null) {
				ct.setUUserid(userDTO.getUMb());
				ct.setUUsername(userDTO.getUUsername());//微信昵称
				ct.setUCode(UUID.randomUUID().toString());
				ct.setUWeixinloginUserid(userDTO.getUWeixinloginUserid());//opesnid
				String passwd = userDTO.getUPassword();
				String passwdorg = "CT" + userDTO.getUMb() + passwd;
				String encryptPwd = MD5.sign(passwdorg, "CT", "utf-8");
				ct.setUPassword(encryptPwd);
				ct.setUMb(userDTO.getUMb());
				ct.setUState("1");
				ct.setURestype("4");
				if (userDTO.getUSex().equals("2")){
					ct.setUSex("女");
				}else{
					ct.setUSex("男");
				}
				
				
				
				//微信关注 公众号 时间

				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String date = sdf.format(new Date(Long.valueOf(userDTO.getUWeixingztime())*1000));
				ct.setUWeixingztime(date);
				// ct.setRId((long) 0);
				
				
				
				//String ran = Util.getRandom();
				
				Long id = this.userManager.save(ct);

				// 关联优惠券
				// 样品券 7 *10
				// 注册50-10 8 *10
				// 注册100-20 9 *10
				// 注册250-50 10 *4
				// 注册500-100 11 *3
				// 注册1000-200 12 *1
				//couponDetailManager.udpateCouponDetails(7, id, 10);
				//couponDetailManager.udpateCouponDetails(8, id, 10);
				//couponDetailManager.udpateCouponDetails(9, id, 10);
				//couponDetailManager.udpateCouponDetails(10, id, 4);
				//couponDetailManager.udpateCouponDetails(11, id, 3);
				//couponDetailManager.udpateCouponDetails(12, id, 1);
				//couponDetailManager.udpateCouponDetails(41, id, 1);//注册送十元无限制优惠券
				//couponDetailManager.udpateCouponDetails(61, id, 2);//周年庆优惠券
//				if(new SimpleDateFormat("yyyy-MM-dd").format(new Date()).equals("2016-11-11")){
//					couponDetailManager.udpateCouponDetails(125, id, 1);//注册15优惠券
//				}
				couponDetailManager.udpateCouponDetails(125, id, 1);//注册15优惠券
				this.session.remove("userinfosession");
				this.session.put("userinfosession", ct);
				ct.setULastTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
						.format((new Date())));
				HttpServletRequest req = ServletActionContext.getRequest();
				String Ip =UtilDate.getIpAddr(req);
				ct.setULastIp(Ip);
				//生成唯一码
				
				Random rand = new Random();
				int []ret=new int[4];
				String suiji = "";
				for(int i=0;i<ret.length;i++){
				   ret[i]=rand.nextInt(10);
				   suiji += ret[i];
				}
				
				ct.setUExtensionid(id+suiji);
				ct.setURegTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format((new Date())));
				userManager.update(ct);
				
				this.request.put("wym", "CT"+id+suiji);
				
				
				//发放红包
				//subscribe=1为已关注  subscribe=0未关注
			/*
				int subscribe = userDTO.getSubscribe();
				if (subscribe==1){
					
					wxhongbao(userDTO.getUWeixinloginUserid(),id);
				}
				
				*/
				return "register_wx";
			} else {
				return "error";
			}
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = "注册失败,请重新注册!";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
	}
	
	/*
	public static void main(String[] args) {
		LoginAction action = new LoginAction();
		action.aa();
	}
	public void aa(){
		wxhongbao("o7ihfs9f88SD4fMZscap8-Yi-l8Q",0L);
	}
	*/
	
	public String wxToken() throws IOException{
		HttpServletRequest requ = ServletActionContext.getRequest();
		
		boolean isGet = requ.getMethod().toLowerCase().equals("get");  
        System.out.println("获得微信请求:" + requ.getMethod() + " 方式");  
		
        
        if (isGet) {  
			String signature = requ.getParameter("signature");  
	        // 时间戳  
	        String timestamp = requ.getParameter("timestamp");  
	        // 随机数  
	        String nonce = requ.getParameter("nonce");  
	        // 随机字符串  
	        String echostr = requ.getParameter("echostr");  
	  
	       // PrintWriter out = response.getWriter();  
	        
	        
	        this.request.put("signature", signature);
	        this.request.put("timestamp", timestamp);
	        this.request.put("nonce", nonce);
	        this.request.put("echostr", echostr);
	        
	        PrintWriter out = response.getWriter();  
	        // 通过检验signature对请求进行校验，若校验成功则原样返回echostr，表示接入成功，否则接入失败  
	        if (wxutil.checkSignature(signature, timestamp, nonce)) {  
	            out.print(echostr);  
	        }  
	        out.close();  
	        out = null;  
	       // return "wxToken";
	        return null; 
        } else { 

        	requ.setCharacterEncoding("UTF-8");  
	        response.setCharacterEncoding("UTF-8");  
	  
	        // 调用核心业务类接收消息、处理消息  
	        String respMessage = processRequest(requ);  
	       //String respMessage = "长亭易购感谢您的支持！";
	       
    
	        // 响应消息  
	        PrintWriter out = response.getWriter();  
	        out.print(respMessage);  
	        out.close(); 		        
	       // return null; 
        }
       
     // 响应消息 
        /*
        String respMessage = "长亭易购感谢您的支持！kkk ";
        respMessage = processRequest(requ); 
        PrintWriter out = response.getWriter();  
        out.print(respMessage);  
        out.close(); 
        */
        return "wxToken";
	}
	
	
	
	public String sendhongbao() throws IOException{
		
	//	CtTemp temp = new CtTemp();
	//	temp = userManager.getTemp(content,fromUserName);
		
		//wxhongbao("o7ihfs4Xl54ZfFTFgbfYm7d8nkvk","风雨长亭，感恩有你!",200);
		
		List<CtTemp> temps = null;
		temps = userManager.getTempList();
		
		if (temps !=null){
			for(int i=0;i<temps.size();i++){
				CtTemp temp = new CtTemp();
				temp = temps.get(i);
				if (Integer.valueOf(temp.gettNum())>=800){
				wxhongbao(temp.gettN(),"风雨长亭，感恩有你!",200*100);
				}
				/*
				if (Integer.valueOf(temp.gettNum())>200){
    				if (Integer.valueOf(temp.gettNum())==300){
        				wxhongbao(temp.gettN(),"风雨长亭，感恩有你!",200*100);
        				wxhongbao(temp.gettN(),"风雨长亭，感恩有你!",100*100);
        			}
        			if (Integer.valueOf(temp.gettNum())==500){
        				wxhongbao(temp.gettN(),"风雨长亭，感恩有你!",200*100);
        				wxhongbao(temp.gettN(),"风雨长亭，感恩有你!",200*100);
        				wxhongbao(temp.gettN(),"风雨长亭，感恩有你!",100*100);
        			}
        			if (Integer.valueOf(temp.gettNum())==800){
        				wxhongbao(temp.gettN(),"风雨长亭，感恩有你!",200*100);
        				wxhongbao(temp.gettN(),"风雨长亭，感恩有你!",200*100);
        				wxhongbao(temp.gettN(),"风雨长亭，感恩有你!",200*100);
        				wxhongbao(temp.gettN(),"风雨长亭，感恩有你!",200*100);
        			}
    				
    			}else{
    				wxhongbao(temp.gettN(),"风雨长亭，感恩有你!",Integer.valueOf(temp.gettNum())*100);
    			}
    			*/
				
				//CtTemp temp1 = new CtTemp();
				
				
				//String temp1 = userManager.updateTemp(temp.gettId().toString());
				temp = userManager.getTemp(temp.gettSN(),temp.gettN(),"T_T","gx");
				//temp = userManager.getTemp(content,fromUserName,"NULL");
				 // 响应消息  
		        //PrintWriter out = response.getWriter();  
		       // out.print(temp.gettT());  
		        //out.close();
		       
			}
			
		}
	
		
        return "wxToken";
	}
	
	/*
	private static void setOutputMsgInfo(TextMessage oms, TextMessage msg) throws Exception {  
        // 设置发送信息  
        Class<?> outMsg = oms.getClass().getSuperclass();  
        Field CreateTime = outMsg.getDeclaredField("CreateTime");  
        Field ToUserName = outMsg.getDeclaredField("ToUserName");  
        Field FromUserName = outMsg.getDeclaredField("FromUserName");  
  
        ToUserName.setAccessible(true);  
        CreateTime.setAccessible(true);  
        FromUserName.setAccessible(true);  
  
        CreateTime.set(oms, new Date().getTime());  
        ToUserName.set(oms, msg.getFromUserName());  
        FromUserName.set(oms, msg.getToUserName());  
    }  
	

	
	private static StringBuffer getXmlInfo() {
		StringBuffer buff = new StringBuffer();

		

		
		buff.append("<xml>");
		buff.append("<Content><![CDATA[ddddddddd]]></Content>");
		buff.append("<ToUserName><![CDATA[kecyshao]]></ToUserName>");
		buff.append("<FromUserName><![CDATA[o7ihfs4Xl54ZfFTFgbfYm7d8nkvk]]></FromUserName>");
		buff.append("<CreateTime>1449631437864</CreateTime>");
		buff.append("<MsgType><![CDATA[text]]></MsgType>");
		buff.append("<MsgId>1234567890abcdef</MsgId>");
		buff.append("</xml>");
		return buff;
		}
	
	
	 public static void main(String[] args) {  
	        String url = "http://127.0.0.1/CT_Web/login_wxToken";  
	        testPost(url);  
	    } 
	 public static void testPost (String urlStr){
		        try {  
		            URL url = new URL(urlStr);  
		            URLConnection con = url.openConnection();  
		            con.setDoOutput(true);  
		            con.setRequestProperty("Pragma:", "no-cache");  
		            con.setRequestProperty("Cache-Control", "no-cache");  
		            con.setRequestProperty("Content-Type", "text/xml");  
		  
		            OutputStreamWriter out = new OutputStreamWriter(con  
		                    .getOutputStream());      
		            String xmlInfo = getXmlInfo().toString();  
		            System.out.println("urlStr=" + urlStr);  
		            System.out.println("xmlInfo=" + xmlInfo);  
		            out.write(new String(xmlInfo.getBytes("ISO-8859-1")));  
		            out.flush();  
		            out.close();  
		            BufferedReader br = new BufferedReader(new InputStreamReader(con  
		                    .getInputStream()));  
		            String line = "";  
		            for (line = br.readLine(); line != null; line = br.readLine()) {  
		                System.out.println(line);  
		            }  
		        } catch (MalformedURLException e) {  
		            e.printStackTrace();  
		        } catch (IOException e) {  
		            e.printStackTrace();  
		        }  

	 }
	 */
	 
	 
	 public String processRequest(HttpServletRequest request) {  
	        String respMessage = null;  
	        try {     
	            // 默认返回的文本消息内容  
	            String respContent = "长亭易购感谢您的支持！";  
	         
	           
	            // xml请求解析  
	            Map<String, String> requestMap = WxMessageUtil.parseXml(request);  
	  
	            // 发送方帐号（open_id）  
	            String fromUserName = requestMap.get("FromUserName");  
	            // 公众帐号  
	            String toUserName = requestMap.get("ToUserName");  
	            // 消息类型  
	            String msgType = requestMap.get("MsgType");  
	            String content = requestMap.get("Content");
	            
         
	            
	            
	            
	            
	  
	            // 回复文本消息  
	            TextMessage textMessage = new TextMessage();  
	            textMessage.setToUserName(fromUserName);  
	            textMessage.setFromUserName(toUserName);  
	            textMessage.setCreateTime(new Date().getTime());  
	            textMessage.setMsgType(WxMessageUtil.RESP_MESSAGE_TYPE_TEXT);  
	          //  textMessage.setFuncFlag(0);  
	  
	            // 文本消息  
	            if (msgType.equals(WxMessageUtil.REQ_MESSAGE_TYPE_TEXT)) {  
	            	try {
						if(content.substring(0, 5).toUpperCase().equals("CTEGO")){
							CtDrawDetail drawDetail = userManager.findDrawDetailByDsn(content.toUpperCase());
							if(drawDetail != null){
								if(drawDetail.getDGTime() == null){
									CtDrawDetail drawDetailOld = userManager.findDrawByOpenId(fromUserName);
									if(drawDetailOld == null || drawDetailOld.getUId().toString().equals(drawDetail.getUId().toString())){
										respContent = "感谢您对长亭易购的支持，一份薄礼请笑纳，持续关注收获更多惊喜！";
										drawDetail.setDGTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
										drawDetail.setDOpenid(fromUserName);
										userManager.updateDrawDetail(drawDetail);
										
//										wxhongbao(fromUserName,"风雨长亭，感恩有你!",drawDetail.getDSum()*100);
										wxhongbao(fromUserName,"16走来,17同行",drawDetail.getDSum()*100);
//										wxhongbao(fromUserName,"16走来,17同行",100);
										
										CtUser user = userManager.getUserByUId(Long.valueOf(drawDetail.getUId().toString()));
										if(user.getUWeixinloginUserid() == null){
											user.setUWeixinloginUserid(fromUserName);
											userManager.update(user);
										}
									} else {
										respContent = "不要偷领别人的红包哦！";
									}
								} else {
									respContent = "您已经兑换过啦，偷偷告诉你分享至朋友圈还有抽奖机会哦！";
								}
							} else {
								respContent = "兑换码迷失在外太空咯，再仔细核对一下~";
							}
						} else {
							respContent = "感谢您的支持";
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
	            	
	            	
//	            	if (content.equals("赵其国") || content.equals("汪萍") || 
//	            			content.equals("汪杰") || content.equals("汪彬昺") ||
//	            			content.equals("林玲") || content.equals("夏振华") || 
//	            			content.equals("黄荣太") || content.equals("罗素华") || 
//	            			content.equals("戴石招") || content.equals("胡骏") || 
//	            			content.equals("黄露") || content.equals("马骏") || 
//	            			content.equals("张德蕊") || content.equals("黄碧红") || 
//	            			content.equals("林莉") || content.equals("赵桂萍") || 
//	            			content.equals("陈墨") || content.equals("丁燕琼") || 
//	            			content.equals("付亚芳")){
//	            		CtTemp temp = new CtTemp();
//	            		temp = userManager.getTemp(content,fromUserName,"NULL","hq");
//	            		if (temp !=null){
//	            			respContent = "亲爱的"+temp.gettName()+"，长亭感恩有你，祝您新年快乐，工作顺利，财源广进，心想事成哦~！";
//	            			wxhongbao(fromUserName,"风雨长亭，感恩有你!",Integer.valueOf(temp.gettNum())*100);
//	            			//userManager.updateTemp(temp.gettId().toString());
//	            			temp = userManager.getTemp(content,fromUserName,"T_T","gx");
//	            		}
//	            	}
//	            	
//	            	if (content.equals("赵总很帅")){
//	            		CtTemp temp = new CtTemp();
//	            		temp = userManager.getTempC(fromUserName, "tZ");
//	            		
//	            		if (temp !=null){
//	            			respContent = "亲爱的小伙伴，你说对了，嘿嘿~！";
//	            			wxhongbao(fromUserName,"风雨长亭，感恩有你!",288);
//	            			
//	            			temp = userManager.getTemp(temp.gettSN(),temp.gettN(),"T_Z","gx");
//	            		}
//	            	}
//	            	if (content.equals("丁哥很酷")){
//	            		CtTemp temp = new CtTemp();
//	            		temp = userManager.getTempC(fromUserName,"tD");
//	            		
//	            		if (temp !=null){
//	            			respContent = "亲爱的小伙伴，你说对了，嘿嘿~！";
//	            			wxhongbao(fromUserName,"风雨长亭，感恩有你!",266);
//	            			
//	            			temp = userManager.getTemp(temp.gettSN(),temp.gettN(),"T_D","gx");
//	            		}
//	            	}
//	            	if (content.equals("张哥很有型")){
//	            		CtTemp temp = new CtTemp();
//	            		temp = userManager.getTempC(fromUserName,"tZH");
//	            		//String at = "Y";
//	            		if (temp !=null){
//	            			respContent = "亲爱的小伙伴，你说对了，嘿嘿~！";
//	            			wxhongbao(fromUserName,"风雨长亭，感恩有你!",200);
//	            			
//	            			temp = userManager.getTemp(temp.gettSN(),temp.gettN(),"T_ZH","gx");
//	            		}
//	            	}
//	            	
//	            	
//	            	if (content.length()==18){
//	            	//	respContent = "<a href=''>点我注册~！</a>"; 	
//	            		//CtUser user = new CtUser();
//	            		//user = userManager.findWeiXinOpenId(fromUserName);
//	            	
//	            		CtTemp temp = new CtTemp();
//	            		temp = userManager.getTemp(content,fromUserName,"NULL","hq");
//	            		//respContent = temp.gettName();
//	            	
//	            		//respContent ="亲爱的"+ userManager.updateTemp(content);
//	            		//respContent = "aaa";
//	            		if (temp !=null){
//	            			
//	            			respContent = "亲爱的"+temp.gettName()+"，您的账号已绑定成功，长亭感恩有你，祝您新年快乐，工作顺利，财源广进，心想事成哦~！";
//	            			
//	            			/*
//	            			//判断是否注册过
//	            			CtUser user = new CtUser();
//		            		user = userManager.findWeiXinOpenId(fromUserName);
//		            		if (user!=null){//有此用户，更新U_emp=1
//		            			
//		            			
//		            		}else{//注册
//		            			//获取用户基本信息
//		            			String access_token =getToken();  
//		            			String urlwx = "https://api.weixin.qq.com/cgi-bin/user/info?access_token="+access_token+"&openid="+fromUserName+"&lang=zh_CN";
//		            			JSONObject jsonObject = new JSONObject(getJsonString(urlwx));  
//		            			Long subscribe_time = jsonObject.getLong("subscribe_time");
//		            			String nickname = jsonObject.getString("nickname");
//		            			int subscribe = jsonObject.getInt("subscribe");//1为关注了，0为没关注
//		            			int sex = jsonObject.getInt("sex");
//		            			user.setUUserid(fromUserName);
//		            	
//		            			//ct.setUUserid(userDTO.getUMb());
//		            			user.setUUsername(nickname);//微信昵称
//		            			user.setUCode(UUID.randomUUID().toString());
//		            			user.setUWeixinloginUserid(fromUserName);//opesnid
//		        				String passwd = userDTO.getUPassword();
//		        				String passwdorg = "CT" + userDTO.getUMb() + passwd;
//		        				String encryptPwd = MD5.sign(passwdorg, "CT", "utf-8");
//		        				user.setUPassword(encryptPwd);
//		        				user.setUMb(userDTO.getUMb());
//		        				user.setUState("1");
//		        				user.setURestype("4");
//		        				if (sex==2){
//		        					user.setUSex("女");
//		        				}else{
//		        					user.setUSex("男");
//		        				}
//		            			
//		            			
//		            		}
//		            		if (Integer.valueOf(temp.gettNum())>200){
//	            				
//	            				for (int i=200;i<Integer.valueOf(temp.gettNum());i=i+200){
//	            					
//	            				}
//	            			}
//		            		*/
//	            			/*
//	            			if (Integer.valueOf(temp.gettNum())>200){
//	            				if (Integer.valueOf(temp.gettNum())==300){
//		            				wxhongbao(fromUserName,"风雨长亭，感恩有你!",200*100);
//		            				wxhongbao(fromUserName,"风雨长亭，感恩有你!",100*100);
//		            			}
//		            			if (Integer.valueOf(temp.gettNum())==500){
//		            				wxhongbao(fromUserName,"风雨长亭，感恩有你!",200*100);
//		            				wxhongbao(fromUserName,"风雨长亭，感恩有你!",200*100);
//		            				wxhongbao(fromUserName,"风雨长亭，感恩有你!",100*100);
//		            			}
//		            			if (Integer.valueOf(temp.gettNum())==800){
//		            				wxhongbao(fromUserName,"风雨长亭，感恩有你!",200*100);
//		            				wxhongbao(fromUserName,"风雨长亭，感恩有你!",200*100);
//		            				wxhongbao(fromUserName,"风雨长亭，感恩有你!",200*100);
//		            				wxhongbao(fromUserName,"风雨长亭，感恩有你!",200*100);
//		            			}
//	            				
//	            			}else{
//	            				wxhongbao(fromUserName,"风雨长亭，感恩有你!",Integer.valueOf(temp.gettNum())*100);
//	            			}
//	            			*/
//	            			
//	            			
//	            		}else{
//	            			respContent = "亲爱的朋友，长亭感恩有你，新年快乐哦~！";
//	            		}
//	
//	            		
//	            		
//            			
//            			
//	            	}else if (content.substring(0, 2).equals("CT") && content.length()<13){//唯一码校验
//	            		//根据openid取出该用户的信息
//	            		
//	            		if (userManager.getHongBaototalByDesc(" where hongbaoDesc='注册有礼!'")<1000){//判断是否满额
//	            		
//	            			CtUser user = new CtUser();
//		            		user = userManager.findWeiXinOpenId(fromUserName);
//		            		
//		            		//判断用户是否存在
//		            		
//		            		
//		            		if (user != null && content.substring(2, content.length()).equals(user.getUExtensionid())){//如果相同，随机推送红包
//		            			
//		            			if (userManager.getHongBaoByOpenid(fromUserName, "注册有礼!") != null){//判断是否已经获得本轮红包
//		            				respContent = "您已获得本轮红包，把机会留给其他小伙伴吧~！"; 
//		            			}else{
//		            				
//		            				//判断当天是否已经抽奖
//		            				if (userManager.getHongBaoTempByOpenidDate(fromUserName, new SimpleDateFormat("yyyy-MM-dd").format(new Date())) != null){
//		            					respContent = "您今天未抽中红包，记得明天继续来抽哦~！"; 	
//		            				}else{
//		            					//随机,如果6位数+月+日+时+分加起来偶数，中奖
//		            					
//				            			int connum=0;
//				            			for (int i=2;i<content.length();i++){
//				            				 connum = connum + Integer.valueOf(content.substring(i,i+1));
//				            			 }
//
//				            			Calendar c = Calendar.getInstance();
//				            			connum = connum + c.get(Calendar.YEAR)+c.get(Calendar.MONTH)+1+c.get(Calendar.DAY_OF_MONTH)+c.get(Calendar.HOUR_OF_DAY)+c.get(Calendar.MINUTE);
//				            			
//				            			
//				            			//抽取红包记录
//				            			CtHongbaoTemp hongbaot = new CtHongbaoTemp();
//			            				hongbaot.setHtdate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
//			            				hongbaot.setHtopenid(fromUserName);
//			            				hongbaot.setHtwym(content);
//			            				userManager.saveHongBaoTemp(hongbaot);
//			            				
//				            			if ((connum)%15==0){
//				            				respContent = "恭喜抽中本轮微信红包，感谢支持，快将好消息告诉小伙伴们哦~！";
//					            		//	wxhongbao(fromUserName,user.getUId(),"注册有礼!");
//				            			}else{
//				            				
//				            				respContent = "您今天未抽中红包，记得明天继续来抽哦~！"; 	
//				            				//respContent = "抱歉！您未抽中本轮微信红包，满额红包已发放至您账户，并且获得了下次微信红包发放的优先资格，请至长亭易购(ctego.com)官网查看！";
//				            			}
//				            			
//		            				}//判断当天是否已经抽奖结束
//		            				
//		            			}//判断是否已经获得本轮红包结束
//		            			
//
//		            		}////判断是否是该用户结束
//	            			
//	            		}
//	            		
//	                	
//	            		
//	                }
//	            	
//	                
	            }  
	            // 图片消息  
	            else if (msgType.equals(WxMessageUtil.REQ_MESSAGE_TYPE_IMAGE)) {  
	                respContent = "长亭易购感谢您的支持！";  
	            }  
	            // 地理位置消息  
	            else if (msgType.equals(WxMessageUtil.REQ_MESSAGE_TYPE_LOCATION)) {  
	                respContent = "长亭易购感谢您的支持！";  
	            }  
	            // 链接消息  
	            else if (msgType.equals(WxMessageUtil.REQ_MESSAGE_TYPE_LINK)) {  
	                respContent = "长亭易购感谢您的支持！";  
	            }  
	            // 音频消息  
	            else if (msgType.equals(WxMessageUtil.REQ_MESSAGE_TYPE_VOICE)) {  
	                respContent = "长亭易购感谢您的支持！";  
	            }  
	            // 事件推送  
	            else if (msgType.equals(WxMessageUtil.REQ_MESSAGE_TYPE_EVENT)) {  
	                // 事件类型  
	                String eventType =  requestMap.get("Event");
	                // 订阅  
	                if (eventType.equals(WxMessageUtil.EVENT_TYPE_SUBSCRIBE)) {  
	                    respContent = "终于等到你！长亭易购感谢您的支持！如果您是专业采购员，长亭给您最大优惠，" +
	                    		"如果您是创客达人，长亭给您方便快捷，如果您是吐槽达人，我们愿意聆听，采纳后必有重谢！" +
	                    		"海量现货正品库存，专业资源下载，方便快捷服务，长亭易购给您一站式元器件购物享受！"; 
	                    
	                    /*
	                    NewsMessage newsMessage = new NewsMessage();  
	                    newsMessage.setToUserName(fromUserName);  
	                    newsMessage.setFromUserName(toUserName);  
	                    newsMessage.setCreateTime(new Date().getTime());  
	                    newsMessage.setMsgType(WxMessageUtil.RESP_MESSAGE_TYPE_TEXT);  
	                    newsMessage.setArticleCount(1);
	                    List<Article> articles = null;
	                    Article ar = new Article();
	                    ar.setTitle(title);
	                    
	                    newsMessage.setArticles(articles);
	                    
	                    
	                    respMessage = WxMessageUtil.newsMessageToXml(newsMessage);  
	                    */
	                }  
	                // 取消订阅  
	                else if (eventType.equals(WxMessageUtil.EVENT_TYPE_UNSUBSCRIBE)) {  
	                    // TODO 取消订阅后用户再收不到公众号发送的消息，因此不需要回复消息  
	                }  
	                // 自定义菜单点击事件  
	                else if (eventType.equals(WxMessageUtil.EVENT_TYPE_CLICK)) {  
	                    // TODO 自定义菜单权没有开放，暂不处理该类消息  
	                }  
	            }  
	  
	            textMessage.setContent(respContent);  
	            respMessage = WxMessageUtil.textMessageToXml(textMessage);  
	           
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        }  
	  
	        return respMessage;  
	    }  
	 
	 
	// public static void main(String[] args){


	 public static void main(String[] args) {
		 String content = "ctego123456789";
		 boolean flag = content.substring(0, 5).toUpperCase().equals("CTEGO");
		 System.out.println(flag);
	}
	 public String getToken(){//判断是否超时,获取token
		 DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 String token="";
		 try
		  
		 {
		  //读取时间
			 
			 String file = "//usr//token.txt";
			 int i =1;
			 BufferedReader bre = null;
			 String str = "";
			 String ttime = "";
			
			 bre = new BufferedReader(new FileReader(file));//此时获取到的bre就是整个文件的缓存流
			 while ((str = bre.readLine())!= null) // 判断最后一行不存在，为空结束循环
			{
					if(i==1){//时间
						ttime = str;//原样输出读到的内容
					}
					if(i==2){//token
						token = str;//原样输出读到的内容
					}
					i++;
			}
			 
			 bre.close();
		   Date d1 = df.parse(ttime);
		  
		   Date d2 = new Date();
		   long diff = d2.getTime() - d1.getTime();//这样得到的差值是微秒级别
		   long days = diff / (1000 * 60 * 60 * 24);
		  
		   long hours = (diff-days*(1000 * 60 * 60 * 24))/(1000* 60 * 60);
		   if (hours>=2){//重新获取token
			   String urlwx= "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wx20d092d7dac249d5&secret=cac931764ee679a4b00abe35095cd7ec";
			   JSONObject jsonObject = new JSONObject(getJsonString(urlwx));  
			   //token="aaaa";
			   token = jsonObject.getString("access_token"); 
			   //写文件
			   FileOutputStream out=new FileOutputStream(file);
			   PrintStream p=new PrintStream(out);
			   SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	           p.println(df.format(new Date()));
	           p.println(token);
	           
	           p.close();
	           out.close();
		   }
		   //long minutes = (diff-days*(1000 * 60 * 60 * 24)-hours*(1000* 60 * 60))/(1000* 60);
		  // System.out.println(""+days+"天"+hours+"小时"+minutes+"分");
		  
		 }
		 catch (Exception e)
		 {
		 }finally{
			 
		 }
		 return token;
	 }
	 public String wxAbout()
	  {
	    return "wxAbout";
	  }
	  
	  public String wxContact()
	  {
	    return "wxContact";
	  }
}


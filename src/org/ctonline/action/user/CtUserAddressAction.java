package org.ctonline.action.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;

import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.ctonline.dto.basic.CtRegionDTO;
import org.ctonline.dto.user.CtUserAddressDTO;
import org.ctonline.manager.basic.CtRegionManager;
import org.ctonline.manager.user.CtUserAddressManager;
import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.user.CtUserAddress;
import org.ctonline.util.Page;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class CtUserAddressAction extends ActionSupport implements RequestAware,
		ModelDriven<Object>, SessionAware {
	private String result;
	private List<CtUserAddress> addresses;
	private Map<String, Object> session;
	private String AConsignee2;
	private String address2;
	
	
	public String getAConsignee2() {
		return AConsignee2;
	}

	public void setAConsignee2(String aConsignee2) {
		AConsignee2 = aConsignee2;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public CtRegionManager getRegionManager() {
		return regionManager;
	}

	public void setRegionManager(CtRegionManager regionManager) {
		this.regionManager = regionManager;
	}

	private CtUserAddressManager addressManager;
	private List<CtRegion> regions;

	public List<CtRegion> getRegions() {
		return regions;
	}

	public void setRegions(List<CtRegion> regions) {
		this.regions = regions;
	}

	private CtRegionManager regionManager;

	private Map<String, Object> request;
	private Page page;
	private CtUserAddressDTO addressDTO = new CtUserAddressDTO();
	private CtUserAddress address;
	private CtRegionDTO regionDTO = new CtRegionDTO();
	private Long AId;
	private JSONArray resultJson;

	public List<CtUserAddress> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<CtUserAddress> addresses) {
		this.addresses = addresses;
	}

	public CtUserAddressManager getAddressManager() {
		return addressManager;
	}

	public void setAddressManager(CtUserAddressManager addressManager) {
		this.addressManager = addressManager;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public void setRequest(Map<String, Object> request) {
		this.request = request;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public CtUserAddress getAddress() {
		return address;
	}

	public void setAddress(CtUserAddress address) {
		this.address = address;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	@Override
	public Object getModel() {

		return this.addressDTO;
	}

	// ҳ����ʾ�û���
	public String list() {
		CtUser cu = (CtUser) this.session.get("userinfosession");
		Long id = regionDTO.getRegionId();
		Long pid;
		if (id == null || "".equals(id)) {
			pid = (long) 0;

		} else {
			pid = id;
		}
		this.regions = regionManager.queryById(pid);

		page = new Page();
		if (addressDTO.getPage() == 0) {
			addressDTO.setPage(1);
		}
		page.setCurrentPage(addressDTO.getPage());
		this.addresses = addressManager.loadAll(page, cu);
		page.setTotalPage(page.getTotalPage());
		this.request.put("pages", page);
		return "list";
	}

	// ���չؼ��ֲ�ѯ�û���
	public String search() {

		Page page = new Page();
		if (addressDTO.getPage() == 0) {
			addressDTO.setPage(1);
		}
		page.setCurrentPage(addressDTO.getPage());
		if (addressDTO.getKeyword() != null
				&& !addressDTO.getKeyword().equals("��������Ҫ��Ĺؼ��")) {
			this.addresses = addressManager.findAll(addressDTO.getKeyword(),
					page);
		} else {
			// this.addresses = addressManager.loadAll(page);
		}
		page.setTotalPage(page.getTotalPage());
		this.request.put("pages", page);

		return SUCCESS;
	}

	// ����û���
	public String add() {

		CtUserAddress m = new CtUserAddress();
		CtUser cu = (CtUser) this.session.get("userinfosession");
		m.setUId(cu.getUId());
		m.setAConsignee(addressDTO.getAConsignee());
		m.setAddress(addressDTO.getAddress());
		m.setCity(addressDTO.getCity());
		m.setCountry(addressDTO.getCountry());
		m.setProvince(addressDTO.getProvince());
		if(addressDTO.getDistrict()==-1){
			m.setDistrict(addressDTO.getCity());
		}else{
			m.setDistrict(addressDTO.getDistrict());
		}
		m.setMb(addressDTO.getMb()+" ");
		m.setAdesc(addressDTO.getAdesc()+" ");
		m.setTel(addressDTO.getTel()+" ");
		m.setZipcode(addressDTO.getZipcode());
		
		Long ucount = addressManager.totalCount(" where U_ID="+cu.getUId());
		if (ucount==Long.valueOf(0))
			m.setIsdefault("0");
		else
			m.setIsdefault("1");
		
		String res=this.addressManager.save(m);
		if(res.equals("Exerror")){
			String url = addressDTO.getUrl();
			//System.out.println(url);
			String errorTotle = "抱歉，提交异常";
			String errorMess = "含非法字符";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			session.put("errorBackUrl", url);
			return ERROR;
		}

		return "add";
	}
	public String checkaddress(){	
		String AConsignee2=this.getAConsignee2();
		String address2=this.getAddress2();
		CtUser cu = (CtUser) this.session.get("userinfosession");
		CtUserAddress ma=addressManager.findByNameandAddress(AConsignee2,address2,cu);
		if(ma==null){
		this.result="success";
		}else{
		this.result="error";
		}
		return "checkaddress";
	}
	

	public String addForOrder() {

		CtUserAddress m = new CtUserAddress();
		CtUser cu = (CtUser) this.session.get("userinfosession");
		m.setUId(cu.getUId());
		m.setAConsignee(addressDTO.getAConsignee());
		m.setAddress(addressDTO.getAddress());
		m.setCity(addressDTO.getCity());
		m.setCountry(addressDTO.getCountry());
		m.setProvince(addressDTO.getProvince());
		if(addressDTO.getDistrict()==-1){
			m.setDistrict(addressDTO.getCity());
		}else{
			m.setDistrict(addressDTO.getDistrict());
		}
		m.setMb(addressDTO.getMb()+" ");
		m.setTel(addressDTO.getTel()+" ");
		m.setZipcode(addressDTO.getZipcode()+" ");
		m.setAdesc(addressDTO.getAdesc()+" ");
		
		//第一个为默认地址
		Long ucount = addressManager.totalCount(" where U_ID="+cu.getUId());
		if (ucount==Long.valueOf(0))
			m.setIsdefault("0");
		else
			m.setIsdefault("1");

		String res=this.addressManager.save(m);
		if(res.equals("Exerror")){
			String url = addressDTO.getUrl();
			//System.out.println(url);
			String errorTotle = "抱歉，提交异常";
			String errorMess = "含非法字符";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			session.put("errorBackUrl", url);
			return ERROR;
		}
		return "addForOrder";
	}

	// ɾ���û���
	public String del() {
		Long AId = addressDTO.getAId();
		this.addressManager.delete(AId);
		return "del";
	}

	// 把地址设为默认收货地址
	public String setAsDeafult() {
		try {
			Long AId = addressDTO.getAId();
			CtUser cu = (CtUser) this.session.get("userinfosession");
			String res=this.addressManager.setAsDeafult(AId, cu);
			if(res.equals("Exerror")){
				String url = addressDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
			}
			return "setAsDeafult";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}

	// �޸��û���
	public String update() {

		Long AId = addressDTO.getAId();
		CtUserAddress m = this.addressManager.findById(AId);
		m.setAConsignee(addressDTO.getAConsignee());
		m.setAddress(addressDTO.getAddress());
		m.setCity(addressDTO.getCity());
		m.setCountry(addressDTO.getCountry());
		m.setProvince(addressDTO.getProvince());
		m.setDistrict(addressDTO.getDistrict());
		m.setMb(addressDTO.getMb()+" ");
		m.setTel(addressDTO.getTel()+" ");
		m.setZipcode(addressDTO.getZipcode());
		m.setAdesc(addressDTO.getAdesc()+" ");
		String res=addressManager.update(m);
		if(res.equals("Exerror")){
			String url = addressDTO.getUrl();
			//System.out.println(url);
			String errorTotle = "抱歉，提交异常";
			String errorMess = "含非法字符";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			session.put("errorBackUrl", url);
			return ERROR;
		}
		return "update";
	}

	public String find() {
		Long pid = (long) 0;
		this.regions = regionManager.queryById(pid);

		Long id = addressDTO.getId();
		address = new CtUserAddress();
		address.setAId(id);
		this.address = this.addressManager.findById(id);
		addressDTO.setAId(address.getAId());
		addressDTO.setUId(address.getUId());
		addressDTO.setAConsignee(address.getAConsignee());
		addressDTO.setAddress(address.getAddress());
		addressDTO.setCity(address.getCity());
		addressDTO.setCountry(address.getCountry());
		addressDTO.setDistrict(address.getDistrict());
		addressDTO.setProvince(address.getProvince());
		addressDTO.setTel(address.getTel());
		addressDTO.setMb(address.getMb());
		addressDTO.setZipcode(address.getZipcode());

		return SUCCESS;

	}

	public Long getAId() {
		return AId;
	}

	public void setAId(Long aId) {
		AId = aId;
	}

	// 根据收货地址id获取一个收货地址
	public String getAddressByAid() {
		try {
			Long id = addressDTO.getAId();
			CtUserAddress address = this.addressManager.findById(id);
			List<String> l = new ArrayList<String>();
			l.add(address.getRegionCountry().getRegionName());
			l.add(address.getRegionCountry().getRegionId().toString());
			l.add(address.getRegionProvince().getRegionName());
			l.add(address.getRegionProvince().getRegionId().toString());
			l.add(address.getRegionCity().getRegionName());
			l.add(address.getRegionCity().getRegionId().toString());
			l.add(address.getRegionDistrict().getRegionName());
			l.add(address.getRegionDistrict().getRegionId().toString());
			l.add(address.getAddress());
			if (address.getAdesc() == null) {
				l.add("");
			} else {
				l.add(address.getAdesc().trim());
			}
			if (address.getZipcode() == null) {
				l.add("");
			} else {
				l.add(address.getZipcode().trim());
			}
			l.add(address.getAConsignee());
			if (address.getTel() == null) {
				l.add("");
			} else {
				l.add(address.getTel().trim());
			}
			if (address.getMb() == null) {
				l.add("");
			} else {
				l.add(address.getMb().trim());
			}
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			List<String> l = new ArrayList<String>();
			l.add("error");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		}
	}

	public CtUserAddressDTO getAddressDTO() {
		return addressDTO;
	}

	public void setAddressDTO(CtUserAddressDTO addressDTO) {
		this.addressDTO = addressDTO;
	}

	public CtRegionDTO getRegionDTO() {
		return regionDTO;
	}

	public void setRegionDTO(CtRegionDTO regionDTO) {
		this.regionDTO = regionDTO;
	}

	public JSONArray getResultJson() {
		return resultJson;
	}

	public void setResultJson(JSONArray resultJson) {
		this.resultJson = resultJson;
	}
	public String errorpage(){
		return "errorpage";
	}
}

package org.ctonline.action.user;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.ctonline.dto.order.OrderDTO;
import org.ctonline.dto.user.CtUserDTO;
import org.ctonline.manager.basic.CtCouponDetailManager;
import org.ctonline.manager.goods.CtBomManager;
import org.ctonline.manager.goods.F_GoodsManager;
import org.ctonline.manager.order.OrderManager;
import org.ctonline.manager.user.CtUserAddressManager;
import org.ctonline.manager.user.CtUserManager;
import org.ctonline.po.basic.CtNotice;
import org.ctonline.po.basic.CtSuggestion;
import org.ctonline.po.goods.CtBom;
import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.po.order.CtEvaluation;
import org.ctonline.po.order.CtOrderGoods;
import org.ctonline.po.order.CtOrderInfo;
import org.ctonline.po.user.CtContact;
import org.ctonline.po.user.CtDraw;
import org.ctonline.po.user.CtDrawDetail;
import org.ctonline.po.user.CtDrawPacket;
import org.ctonline.po.user.CtExtension;
import org.ctonline.po.user.CtSms;
import org.ctonline.po.user.CtUesrClient;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.user.CtUserAddress;
import org.ctonline.test.serviceInterface;
import org.ctonline.util.AlipaySubmit;
import org.ctonline.util.ListSortUtil;
import org.ctonline.util.MD5;
import org.ctonline.util.MialUtil;
import org.ctonline.util.Page;
import org.ctonline.util.UtilDate;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.qq.connect.QQConnectException;
import com.qq.connect.api.OpenID;
import com.qq.connect.api.qzone.PageFans;
import com.qq.connect.api.qzone.Topic;
import com.qq.connect.api.qzone.UserInfo;
import com.qq.connect.javabeans.AccessToken;
import com.qq.connect.javabeans.GeneralResultBean;
import com.qq.connect.javabeans.qzone.PageFansBean;
import com.qq.connect.javabeans.qzone.UserInfoBean;
import com.qq.connect.javabeans.weibo.Company;
import com.qq.connect.oauth.Oauth;

public class CtUserAction extends ActionSupport implements RequestAware,ModelDriven<Object>,SessionAware{

	private List<CtUser> users;
	private List<CtOrderInfo> orderList;
	private CtUser ctUser;
	private CtEvaluation ctEvaluation;
	private CtUserDTO userDTO = new CtUserDTO();
	private CtUserManager userManager;
	private CtBomManager bomManager;
	private OrderManager orderManager;
	private F_GoodsManager fgoodsManager;
	private Map<String, Object> request;
	private Map<String, Object> session;
	private Page page;
	private String result;
	private String uid;
	private String ucode;
	private List<CtBom> bomList;
	private Long cout_all;
	private Long cout_admit;
	private Long cout_count;
	private Long dfcount;
	private Long dscount;
	private Long dfhcount;
	private Long dshcount;
	private Long dpj;
	private Long ordernum;
	private List<CtOrderGoods> goodsList = new ArrayList<CtOrderGoods>();
	private List<CtOrderGoods> listGodsTest = new ArrayList<CtOrderGoods>();
	private List<List<CtOrderGoods>> goodsListAll = new ArrayList<List<CtOrderGoods>>();
	private List<?> orderGoods;
	private JSONArray resultJson;
	private String name;
	private String em;
	private String mb;
	private String nr;
	private int Ipcount;
	

	public Long getOrdernum() {
		return ordernum;
	}

	public void setOrdernum(Long ordernum) {
		this.ordernum = ordernum;
	}

	public int getIpcount() {
		return Ipcount;
	}

	public void setIpcount(int ipcount) {
		Ipcount = ipcount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEm() {
		return em;
	}

	public void setEm(String em) {
		this.em = em;
	}

	public String getMb() {
		return mb;
	}

	public void setMb(String mb) {
		this.mb = mb;
	}

	public String getNr() {
		return nr;
	}

	public void setNr(String nr) {
		this.nr = nr;
	}

	public JSONArray getResultJson() {
		return resultJson;
	}

	public void setResultJson(JSONArray resultJson) {
		this.resultJson = resultJson;
	}

	public Long getDpj() {
		return dpj;
	}

	public void setDpj(Long dpj) {
		this.dpj = dpj;
	}

	public Long getDfcount() {
		return dfcount;
	}

	public void setDfcount(Long dfcount) {
		this.dfcount = dfcount;
	}

	public Long getDscount() {
		return dscount;
	}

	public void setDscount(Long dscount) {
		this.dscount = dscount;
	}

	public Long getDfhcount() {
		return dfhcount;
	}

	public void setDfhcount(Long dfhcount) {
		this.dfhcount = dfhcount;
	}

	public Long getDshcount() {
		return dshcount;
	}

	public void setDshcount(Long dshcount) {
		this.dshcount = dshcount;
	}

	public List<?> getOrderGoods() {
		return orderGoods;
	}

	public void setOrderGoods(List<?> orderGoods) {
		this.orderGoods = orderGoods;
	}

	public List<List<CtOrderGoods>> getGoodsListAll() {
		return goodsListAll;
	}

	public void setGoodsListAll(List<List<CtOrderGoods>> goodsListAll) {
		this.goodsListAll = goodsListAll;
	}

	public List<CtOrderGoods> getListGodsTest() {
		return listGodsTest;
	}

	public void setListGodsTest(List<CtOrderGoods> listGodsTest) {
		this.listGodsTest = listGodsTest;
	}

	public List<CtOrderGoods> getGoodsList() {
		return goodsList;
	}

	public void setGoodsList(List<CtOrderGoods> goodsList) {
		this.goodsList = goodsList;
	}

	public CtEvaluation getCtEvaluation() {
		return ctEvaluation;
	}

	public void setCtEvaluation(CtEvaluation ctEvaluation) {
		this.ctEvaluation = ctEvaluation;
	}

	public List<CtOrderInfo> getOrderList() {
		return orderList;
	}

	public void setOrderList(List<CtOrderInfo> orderList) {
		this.orderList = orderList;
	}

	public Long getCout_count() {
		return cout_count;
	}

	public void setCout_count(Long cout_count) {
		this.cout_count = cout_count;
	}

	private CtCouponDetailManager coupondetailManager;
	
	public CtCouponDetailManager getCoupondetailManager() {
		return coupondetailManager;
	}

	public void setCoupondetailManager(CtCouponDetailManager coupondetailManager) {
		this.coupondetailManager = coupondetailManager;
	}

	public Long getCout_all() {
		return cout_all;
	}

	public void setCout_all(Long cout_all) {
		this.cout_all = cout_all;
	}

	public Long getCout_admit() {
		return cout_admit;
	}

	public void setCout_admit(Long cout_admit) {
		this.cout_admit = cout_admit;
	}

	public List<CtBom> getBomList() {
		return bomList;
	}

	public void setBomList(List<CtBom> bomList) {
		this.bomList = bomList;
	}

	public CtBomManager getBomManager() {
		return bomManager;
	}

	public void setBomManager(CtBomManager bomManager) {
		this.bomManager = bomManager;
	}

	public OrderManager getOrderManager() {
		return orderManager;
	}

	public void setOrderManager(OrderManager orderManager) {
		this.orderManager = orderManager;
	}

	public F_GoodsManager getFgoodsManager() {
		return fgoodsManager;
	}

	public void setFgoodsManager(F_GoodsManager fgoodsManager) {
		this.fgoodsManager = fgoodsManager;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.getUserDTO();
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}
	//用户列表
	public String list(){
		page = new Page();
		if (userDTO.getPage() == 0) {
			userDTO.setPage(1);
		}
		page.setCurrentPage(userDTO.getPage());
		users = this.userManager.loadAll(page);
		this.request.put("pages", page);
		this.request.put("level", 2);
		return "list";
	}
	
	//修改跳转
	public String editUser(){
		
		CtUser cu = (CtUser) this.session.get("userinfosession");
		ctUser = userManager.getCtUserByUUserid(cu.getUUserid());
		return "editUser";
	}
	
	//保存用户
	public String save(){
		try {
			CtUser cu = (CtUser) this.session.get("userinfosession");
			CtUser cu2 = userManager.getUserByUId(cu.getUId());
			cu2.setUUsername(ctUser.getUUsername());
			cu2.setUQq(ctUser.getUQq());
			cu2.setUWeibo(ctUser.getUWeibo());
			cu2.setUTb(ctUser.getUTb());
			cu2.setUSex(ctUser.getUSex());
			cu2.setUBirthday(ctUser.getUBirthday());
			String res=userManager.update(cu2);
			this.session.put("userinfosession", cu2);
			if(res.equals("Exerror")){
				String url =userDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
			}
		} catch (Exception e) {
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
		return "user";
	}
	
	//跳转到安全中心
	public String goSecurity(){
		CtUser cu = (CtUser) this.session.get("userinfosession");
		ctUser = userManager.getCtUserByUUserid(cu.getUUserid());
		return "goSecurity";
	}
	
	//验证用户输入的密码是否正确
	public String checkloginPwd(){
		try {
			String pwd = ctUser.getUPassword();
			CtUser cu = (CtUser) this.session.get("userinfosession");
			String passwdorg = "CT"+cu.getUUserid()+pwd;
			String encryptPwd = MD5.sign(passwdorg, "CT", "utf-8");
			if(encryptPwd.equals(cu.getUPassword())){
				this.result = "success";
			}else {
				this.result = "error";
			}
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
	}
	//跳转到修改密码
	public String goEditPwd(){
		try {
			CtUser cu = (CtUser) this.session.get("userinfosession");
			ctUser = userManager.getCtUserByUUserid(cu.getUUserid());
			return "goEditPwd";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	//修改密码2
	public String editPwd2(){
		try {
			CtUser cu = (CtUser) this.session.get("userinfosession");
			ctUser = userManager.getCtUserByUUserid(cu.getUUserid());
			return "editPwd2";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	//保存通过手机/邮箱修改的密码
	public String editPwd3(){
		try {
			CtUser cu = (CtUser) this.session.get("userinfosession");
			cu = userManager.getCtUserByUUserid(cu.getUUserid());
			String UPassword = ctUser.getUPassword();
			String passwdorg = "CT"+ctUser.getUUserid()+UPassword;
			String encryptPwd = MD5.sign(passwdorg, "CT", "utf-8");
			cu.setUPassword(encryptPwd);
			String res=userManager.update(cu);
			this.session.put("userinfosession", cu);
			if(res.equals("Exerror")){
				String url = userDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
			}
			return "editPwd3";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
	}
	
	//通过邮箱修改密码
	public String editPwdByEmail(){
		try {
			CtUser cu = (CtUser) this.session.get("userinfosession");
			ctUser = userManager.getCtUserByUUserid(cu.getUUserid());
			if(ctUser.getUEmail() != null){
				new Thread(new Runnable() {
					@Override
					public void run() {
						// TODO Auto-generated method stub
						try {
							String mail = "";
							//MialUtil.sendMail(mail);
						} catch (Exception e) {
							// TODO: handle exception
							//e.printStackTrace();
						}
					}
				}).start();
				return "editPwdByEmail";
			}else{
				return "error";
			}
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
		
	}
	//点击邮箱中的链接进入密码修改界面
	public String goEditPwdByEmail(){
		try {
			String UUserid = this.getUid();
			String UCode = this.getUcode();
			CtUser cu = userManager.getCtuserByUUseridAndUCode(UUserid,UCode);
			if(cu != null){
				return "editPwd2";
			}else {
				return "error";
			}
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
	}
	//跳转到绑定邮箱时身份验证
	public String goConfirmIdentity(){
		try {
			CtUser cu = (CtUser) this.session.get("userinfosession");
			ctUser = userManager.getCtUserByUUserid(cu.getUUserid());
			return "goConfirmIdentity";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
	}
	//绑定邮箱时验证身份
	public String confirmMail(){
		try {
			return "confirmMail";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
	}
	
	//发送绑定邮箱的邮件
		public String sendedMail(){
			try {
				if(ctUser.getUEmail() != null){
					  CtUser cu = (CtUser) this.session.get("userinfosession");
					  CtUser cu2 = userManager.getCtUserByUUserid(cu.getUUserid());
					  cu2.setUCode(UUID.randomUUID().toString());
					  String res=userManager.update(cu2);
					  if(res.equals("Exerror")){
							String url = userDTO.getUrl();
							//System.out.println(url);
							String errorTotle = "抱歉，提交异常";
							String errorMess = "含非法字符";
							session.put("errorMess", errorMess);
							session.put("errorTotle", errorTotle);
							session.put("errorBackUrl", url);
							return ERROR;
					  }
					  String email = ctUser.getUEmail();
					  HttpServletRequest requ = ServletActionContext.getRequest();
						String strBackUrl = "http://" + requ.getServerName() //服务器地址  
								+ ":"   
								+ requ.getServerPort()           //端口号  
								+ requ.getContextPath()      //项目名称  
								+ "/security_bindEmail?ctUser.UUserid="+cu2.getUUserid() + "&ucode="+cu2.getUCode() + "&ctUser.UEmail=" + email;     //请求页面或其他地址  
						//System.out.println(strBackUrl);
						StringBuffer sb = new StringBuffer();
						sb.append("<table bgcolor=\"#ffffff\" border=\"0\"  align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"700\">");
						sb.append("<tbody>");
						sb.append("<tr>");
						sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\" height=\"30\">&nbsp;</td>");
						sb.append("<td width=\"698\">&nbsp;</td>");
						sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\">&nbsp;</td>");
						sb.append("</tr>");
						sb.append("<tr>");
						sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\" height=\"55\">&nbsp;</td>");
						sb.append("<td width=\"698\" align=\"left\">");
						sb.append("<table>");
						sb.append("<tbody>");
						sb.append("<tr>");
						sb.append("<td valign=\"top\">");
						sb.append("<div style=\"margin:5px 50px 0px 44px; font-family: Myriad Pro, Lucida Grande, Helvetica, Geneva, Arial, Verdana, sans-serif; color:#000000;font-size:30px;line-height:1.3em; \">注册确认</div>");
						sb.append("</td>");
						sb.append("</tr>");
						sb.append("</tbody>");
						sb.append("</table>");
						sb.append("</td>");
						sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\">&nbsp;</td>");
						sb.append("</tr>");
						sb.append("<tr>");
						sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\" height=\"30\">&nbsp;</td>");
						sb.append("<td width=\"698\">&nbsp;</td>");
						sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\">&nbsp;</td>");
						sb.append("</tr>");
						sb.append("<tr>");
						sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\" height=\"96\">&nbsp;</td>");
						sb.append("<td width=\"698\" align=\"center\">");
						sb.append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"698\">");
						sb.append("<tbody>");
						sb.append("<tr>");
						sb.append("<td width=\"44\">&nbsp;</td>");
						sb.append("<td width=\"610\" align=\"left\">");
						sb.append("<div style=\"margin:10px 5px 0px 2px; font-family: Lucida Grande, Arial, Helvetica, Geneva, Verdana, sans-serif; color:#000000;font-size:14px;line-height:1.5em; \">");
						sb.append("<p>亲爱的<a target=\"_blank\" href=\"mailto:"+email+"\">"+email+"</a>,</p>");
						sb.append("<p>欢迎使用长亭易购，为了方便以后通过安全邮箱来找回密码，请点击以下链接完成邮箱验证：</p>");
						sb.append("<p><a target=\"_blank\" href=\""+strBackUrl+"\">点击验证</a></p>");
						sb.append("<p>（如果上面的链接无法直接点击，请复制链接到浏览器访问）</p>");
						sb.append("<p>如果验证链接已经失效，可在长亭易购的个人设置中; 重发验证邮件：）</p>");
						sb.append("<p>此为系统邮件，请勿回复！<br>");
						sb.append("<br>");
						sb.append("</div>");
						sb.append("</td>");
						sb.append("<td width=\"44\">&nbsp;</td>");
						sb.append("</tr>");
						sb.append("</tbody>");
						sb.append("</table>");
						sb.append("</td>");
						sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\">&nbsp;</td>");
						sb.append("</tr>");
						sb.append("<tr>");
						sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\" height=\"40\">&nbsp;</td>");
						sb.append("<td width=\"698\">&nbsp;</td>");
						sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\">&nbsp;</td>");
						sb.append("</tr>");
						sb.append("</tbody>");
						sb.append("</table>");
						sb.append("");
						String mail =sb.toString();
						MialUtil.sendMail(mail,email);
					  
					return "sendedMail";
				}else{
					return "error";
				}
			} catch (Exception e) {
				// TODO: handle exception
				//e.printStackTrace();
				String errorTotle = "糟糕,页面丢失了";
				String errorMess = " ";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				return ERROR;
			}
			
		}
	//确认绑定
	public String bindEmail(){
		
		try {
			String UCode = this.getUcode();
			String UEmail = ctUser.getUEmail();
			CtUser cu = userManager.getCtUserByUcode(UCode);
			if(cu != null){
				cu.setUEmail(UEmail);
				String res=userManager.update(cu);
				this.session.put("userinfosession", cu);
				if(res.equals("Exerror")){
					String url = userDTO.getUrl();
					//System.out.println(url);
					String errorTotle = "抱歉，提交异常";
					String errorMess = "含非法字符";
					session.put("errorMess", errorMess);
					session.put("errorTotle", errorTotle);
					session.put("errorBackUrl", url);
					return ERROR;
			  }
				return "bindEmail";
			}else {
				return "error";
			}
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
	}
	
	//跳转到修改绑定的邮箱
	public String goUpdateEmail(){
		
		try {
			CtUser cu = (CtUser) this.session.get("userinfosession");
			this.ctUser = userManager.getCtUserByUUserid(cu.getUUserid());
			return "goUpdateEmail";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
	}
	//修改绑定邮箱时手机的验证
	public String updateMail2(){
		try {
			return "updateMail2";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}
	//保存修改绑定的邮箱
	public String saveUpdateMail(){
		try {
			CtUser cu = (CtUser) this.session.get("userinfosession");
			CtUser cu2 = userManager.getCtUserByUUserid(cu.getUUserid());
			String UEmail = ctUser.getUEmail();
			cu2.setUEmail(UEmail);
			String res=userManager.update(cu2);
			if(res.equals("Exerror")){
				String url = userDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
		  }
			return "saveUpdateMail";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
	}
	
	//通过邮箱修改绑定邮箱
	public String updateEmailByEmail(){
		try {
			String email = ctUser.getUEmail();
			CtUser cu = (CtUser) this.session.get("userinfosession");
			ctUser = userManager.getCtUserByUUserid(cu.getUUserid());
			if(ctUser.getUEmail() != null){
				ctUser.setUEmail(email);
				HttpServletRequest requ = ServletActionContext.getRequest();
				String strBackUrl = "http://" + requ.getServerName() //服务器地址  
						+ ":"   
						+ requ.getServerPort()           //端口号  
						+ requ.getContextPath()      //项目名称  
						+ "/security_saveUpdateMail?ctUser.UEmail=" + email +"&ucode="+ctUser.getUCode();     //请求页面或其他地址  
				//System.out.println(strBackUrl);
				StringBuffer sb = new StringBuffer();
				sb.append("<table bgcolor=\"#ffffff\" border=\"0\"  align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"700\">");
				sb.append("<tbody>");
				sb.append("<tr>");
				sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\" height=\"30\">&nbsp;</td>");
				sb.append("<td width=\"698\">&nbsp;</td>");
				sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\">&nbsp;</td>");
				sb.append("</tr>");
				sb.append("<tr>");
				sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\" height=\"55\">&nbsp;</td>");
				sb.append("<td width=\"698\" align=\"left\">");
				sb.append("<table>");
				sb.append("<tbody>");
				sb.append("<tr>");
				sb.append("<td valign=\"top\">");
				sb.append("<div style=\"margin:5px 50px 0px 44px; font-family: Myriad Pro, Lucida Grande, Helvetica, Geneva, Arial, Verdana, sans-serif; color:#000000;font-size:30px;line-height:1.3em; \">修改邮箱</div>");
				sb.append("</td>");
				sb.append("</tr>");
				sb.append("</tbody>");
				sb.append("</table>");
				sb.append("</td>");
				sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\">&nbsp;</td>");
				sb.append("</tr>");
				sb.append("<tr>");
				sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\" height=\"30\">&nbsp;</td>");
				sb.append("<td width=\"698\">&nbsp;</td>");
				sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\">&nbsp;</td>");
				sb.append("</tr>");
				sb.append("<tr>");
				sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\" height=\"96\">&nbsp;</td>");
				sb.append("<td width=\"698\" align=\"center\">");
				sb.append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"698\">");
				sb.append("<tbody>");
				sb.append("<tr>");
				sb.append("<td width=\"44\">&nbsp;</td>");
				sb.append("<td width=\"610\" align=\"left\">");
				sb.append("<div style=\"margin:10px 5px 0px 2px; font-family: Lucida Grande, Arial, Helvetica, Geneva, Verdana, sans-serif; color:#000000;font-size:14px;line-height:1.5em; \">");
				sb.append("<p>亲爱的<a target=\"_blank\" href=\"mailto:"+email+"\">"+email+"</a>,</p>");
				sb.append("<p>请点击一下链接修改邮箱地址：</p>");
				sb.append("<p><a target=\"_blank\" href=\""+strBackUrl+"\">修改邮箱</a></p>");
				sb.append("<p>（如果上面的链接无法直接点击，请复制链接到浏览器访问）</p>");
				sb.append("<p>如果验证链接已经失效，可在长亭易购的个人设置中; 重发验证邮件：）</p>");
				sb.append("<p>此为系统邮件，请勿回复！<br>");
				sb.append("<br>");
				sb.append("</div>");
				sb.append("</td>");
				sb.append("<td width=\"44\">&nbsp;</td>");
				sb.append("</tr>");
				sb.append("</tbody>");
				sb.append("</table>");
				sb.append("</td>");
				sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\">&nbsp;</td>");
				sb.append("</tr>");
				sb.append("<tr>");
				sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\" height=\"40\">&nbsp;</td>");
				sb.append("<td width=\"698\">&nbsp;</td>");
				sb.append("<td style=\"background-color:#dadada\" bgcolor=\"#dadada\" width=\"1\">&nbsp;</td>");
				sb.append("</tr>");
				sb.append("</tbody>");
				sb.append("</table>");
				sb.append("");
				String mail =sb.toString();
				MialUtil.sendMail(mail,email);
				
				
				
				
				return "updateEmailByEmail";
			}else{
				return "error";
			}
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
		
	}
	
	//修改手机验证
	public String goEditPhone(){
		try {
			CtUser cu = (CtUser) this.session.get("userinfosession");
			this.ctUser = userManager.getCtUserByUUserid(cu.getUUserid());
			return "goEditPhone";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
	}
	//验证新手机
	public String confirmPhone(){
		try {
			CtUser cu = (CtUser) this.session.get("userinfosession");
			this.ctUser = userManager.getCtUserByUUserid(cu.getUUserid());
			return "confirmPhone";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
	}
	
	//保存新的绑定手机号
	public String saveNewPhone(){
		try {
			CtUser cu = (CtUser) this.session.get("userinfosession");
			CtUser cu2 = userManager.getCtUserByUUserid(cu.getUUserid());
			String UMb = ctUser.getUMb();
			cu2.setUMb(UMb);
			String res=userManager.update(cu2);
			if(res.equals("Exerror")){
				String url = userDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
		  }
			return "saveNewPhone";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
	}
	
	//跳转到绑定手机号界面
	public String goBindPhone(){
		try {
			return "goBindPhone";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
	}
	
	//保存绑定手机号
	public String saveBindPhone(){
		try {
			CtUser cu = (CtUser) this.session.get("userinfosession");
			CtUser cu2 = userManager.getCtUserByUUserid(cu.getUUserid());
			String UMb = ctUser.getUMb();
			cu2.setUMb(UMb);
			String res=userManager.update(cu2);
			if(res.equals("Exerror")){
				String url = userDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
		  }
			return "saveBindPhone";
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
	}
 
	public List<CtUser> getUsers() {
		return users;
	}

	public void setUsers(List<CtUser> users) {
		this.users = users;
	}

	public CtUserDTO getUserDTO() {
		return userDTO;
	}

	public void setUserDTO(CtUserDTO userDTO) {
		this.userDTO = userDTO;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public CtUser getCtUser() {
		return ctUser;
	}

	public void setCtUser(CtUser ctUser) {
		this.ctUser = ctUser;
	}
	
	public void setUser(List<?> user) {
		this.user = user;
	}

	private List<?> user;
	public Page getPage() {
		return page;
	}
	public void setPage(Page page) {
		this.page = page;
	}
	public CtUserManager getUserManager() {
		return userManager;
	}
	public void setUserManager(CtUserManager userManager) {
		this.userManager = userManager;
	}
	public List<?> getUser() {
		return user;
	}
	public Map<String, Object> getRequest() {
		return request;
	}
	
	//���չؼ��������û�
	public String search(){
		Long UGId=userDTO.getUGId();
		this.user=this.userManager.selectAll(UGId);
		if (userDTO.getKeyword() != null
				&& !"".equals(userDTO.getKeyword())) {
			this.users = userManager.findAll(userDTO.getKeyword());
		} else {
			this.users=null;
		}
		this.request.put("UGId", UGId);
		
		return SUCCESS;
	}
	private Map<String, Integer> orderGoodsCount = new HashMap<String, Integer>();
	
	public Map<String, Integer> getOrderGoodsCount() {
		return orderGoodsCount;
	}

	public void setOrderGoodsCount(Map<String, Integer> orderGoodsCount) {
		this.orderGoodsCount = orderGoodsCount;
	}

	public String contact(){
		List<String> l = new ArrayList<String>();
		try {
			if(this.session.get("userinfosession")!=null){
			CtUser cu = (CtUser)this.session.get("userinfosession");
			}
			String em=this.getEm();
			String mb=this.getMb();
			String nr=this.getNr();
			Date date = new Date();
			SimpleDateFormat siFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			SimpleDateFormat Format = new SimpleDateFormat("yyyy-MM-dd");
			HttpServletRequest req = ServletActionContext.getRequest();
			String Ip =UtilDate.getIpAddr(req);
			String da=Format.format(date);
			this.Ipcount=this.userManager.getIpcount(Ip,da);
			if(Ipcount<=3){
			CtContact con=new CtContact();
			con.setCemail(em);
			con.setCtel(mb);
			con.setCdesc(nr);
			con.setCIp(Ip);
			con.setCtime(siFormat.format(date));
			String res = this.userManager.savecontact(con);
			if(res.equals("Exerror")){
				String url = userDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
		  }
			if(res != null){
				l.add("success");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;	
			}else {
				l.add("error");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;
			}
			}else{
				l.add("exits");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;
			}
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			l.add("error");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		}
	}

	public String loadcoup() {
		try {
				if(this.session.get("userinfosession") != null){
					this.cout_all = coupondetailManager.getCoutAll();
					JSONArray json = new JSONArray();
					json = JSONArray.fromObject(cout_all);
					resultJson = json;
					return SUCCESS;
				}else{
					this.cout_all = -1l;
					JSONArray json = new JSONArray();
					json = JSONArray.fromObject(cout_all);
					resultJson = json;
					return SUCCESS;
				}		
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}

	}
	
	public String loadorder() {
		try {
			if(this.session.get("userinfosession") != null){
			this.dfcount = orderManager.dfCount();
			//this.dscount = orderManager.dscount();
			this.dfhcount = orderManager.dfhcount();
			this.dshcount = orderManager.dshcount();
			this.dpj = orderManager.dpjcount();
			this.ordernum=dfcount+dfhcount+dshcount+dpj;
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(ordernum);
			resultJson = json;
			return SUCCESS;
			}else{
				this.ordernum=-1l;
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(ordernum);
				resultJson = json;
				return SUCCESS;
			}
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
	}
	private OrderDTO orderDTO = new OrderDTO();
	
	public OrderDTO getOrderDTO() {
		return orderDTO;
	}

	public void setOrderDTO(OrderDTO orderDTO) {
		this.orderDTO = orderDTO;
	}

	public String userinfo(){
		try {
			CtUser cu = (CtUser) this.session.get("userinfosession");
			ctUser = userManager.getCtUserByUUserid(cu.getUUserid());

			String temp = "0";
			boolean sim = false;
			boolean par = false;
			// 验证登录
			Long uid = null;
			if (this.session.get("userinfosession") != null) {
				uid = cu.getUId();
				page = new Page();
				if (userDTO.getPage() == 0) {
					userDTO.setPage(1);
				}
				//处理电话号码加密
				String umb = cu.getUMb();
				if(umb != null && !umb.equals("")){
					String s = umb.substring(0, 3);
					String e = umb.substring(umb.length()-4);
					umb = s+"****"+e;
					this.request.put("jiamiUmb", umb);
					//ctUser.setUMb(umb);
				}
				page.setPageSize(6);
				this.bomList = bomManager.findSome();
				this.cout_all = coupondetailManager.getCoutAll();
				//this.cout_admit = coupondetailManager.getCoutAdmit();
				this.cout_count=cout_all;
				this.dfcount=orderManager.dfCount();
				this.dscount=orderManager.dscount();
				this.dfhcount=orderManager.dfhcount();
				this.dshcount=orderManager.dshcount();
				this.dpj=orderManager.dpjcount();
				page.setCurrentPage(userDTO.getPage());
				this.orderList = orderManager.getSomeOrderListByUId1(uid,page);
				Double priceFen = 0D;
				Double priceFenSim = 0D;
				for (int i = 0; i < orderList.size(); i++) {
					ctEvaluation = orderManager.getEvaByOrderId(orderList.get(i).getOrderId());
					if(ctEvaluation != null){
						orderList.get(i).setEvaDesc(ctEvaluation.getEvaDesc());
						orderList.get(i).setEvaTime(ctEvaluation.getEvaTime());
					}
					if(orderList.get(i).getTotal().indexOf(".") > 0){
						String[] pricess = orderList.get(i).getTotal().split("\\.");
						if(pricess[1].length()>2){
							pricess[1] = pricess[1].substring(0,2);
							orderList.get(i).setTotal(pricess[0] + "." + pricess[1]);
						}


					}
					par = false;
					sim = false;
					goodsList = orderManager.getOrderGoodsByOrderId(orderList.get(i).getOrderId());
					for (int j = 0; j < goodsList.size(); j++) {
						
						if(goodsList.get(j).getIsParOrSim().equals("0")){
							par = true;
							Double goodsPrice = Double.valueOf(goodsList.get(j).getGParPrice()) * Double.valueOf(goodsList.get(j).getGParNumber());
							String price = "";
							if(goodsPrice.toString().indexOf(".") > 0){
								String[] pricess = goodsPrice.toString().split("\\.");
								if(pricess[1].length()>2){
									pricess[1] = pricess[1].substring(0,2);
									price = pricess[0] + "." + pricess[1];
								} else {
									price = goodsPrice.toString();
								}
							} else {
								price = goodsPrice.toString();
							}
							goodsList.get(j).setGSubtotal(price);
						} else {
							sim = true;
							Double goodsPrice = Double.valueOf(goodsList.get(j).getGPrice()) * Double.valueOf(goodsList.get(j).getGNumber());
							String price = "";
							if(goodsPrice.toString().indexOf(".") > 0){
								String[] pricess = goodsPrice.toString().split("\\.");
								if(pricess[1].length()>2){
									pricess[1] = pricess[1].substring(0,2);
									price = pricess[0] + "." + pricess[1];
								} else {
									price = goodsPrice.toString();
								}
							} else {
								price = goodsPrice.toString();
							}
							goodsList.get(j).setGSubtotal(price);
						}
						
						if(goodsList.get(j).getIsParOrSim().equals("0")){
							priceFen += Double.valueOf(goodsList.get(j).getGSubtotal());
							goodsList.get(j).setPriceAllDan(priceFen.toString());
						}
						if(goodsList.get(j).getIsParOrSim().equals("1")){
							priceFenSim += Double.valueOf(goodsList.get(j).getGSubtotal());
							goodsList.get(j).setPriceAllDanSim(priceFenSim.toString());
						}
						listGodsTest.add(goodsList.get(j));
					}
					priceFenSim = 0D;
					priceFen = 0D;
					if(sim && par){
						orderList.get(i).setIsParOrSim("1-1");
					} else if(sim && par == false){
						orderList.get(i).setIsParOrSim("1-0");
					} else if (sim == false && par){
						orderList.get(i).setIsParOrSim("0-1");
					}
					goodsListAll.add(goodsList);
					orderGoodsCount.put(orderList.get(i).getOrderId().toString(), Integer.valueOf(goodsList.size()));
				}
				this.orderGoods = orderManager.getOrderGoods(uid);
			} else {
				return "error";
			}
			int tempk = 0;
			int tempk1 = 0;
			List<List<CtOrderGoods>> listnew  = new ArrayList<List<CtOrderGoods>>();
			List<CtOrderGoods> listNeiNew = new ArrayList<CtOrderGoods>();
			for (int k = 0; k < goodsListAll.size(); k++) {
				for (int k2 = 0; k2 < goodsListAll.get(k).size(); k2++) {
					if(goodsListAll.get(k).get(k2).getIsParOrSim().equals("0")){
						if(tempk >= 4){
							//goodsListAll.get(k).remove(k2);
						} else {
							listNeiNew.add(goodsListAll.get(k).get(k2));
						}
						tempk++;
					}
					if(goodsListAll.get(k).get(k2).getIsParOrSim().equals("1")){
						if(tempk1 >= 4){
							//goodsListAll.get(k).remove(k2);
						} else {
							listNeiNew.add(goodsListAll.get(k).get(k2));
						}
						tempk1++;
					}
				}
				listnew.add(listNeiNew);
				listNeiNew = new ArrayList<CtOrderGoods>();
				tempk = 0;
				tempk1 = 0;
			}
			goodsListAll = listnew;
			page.setTotalPage(page.getTotalPage());
			this.request.put("pages", page);
		} catch (Exception e) {
			//e.printStackTrace();
			String errorTotle = "糟糕,页面丢失了";
			String errorMess = " ";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			return ERROR;
		}
		return SUCCESS;
	}

	private CtNotice notice = new CtNotice();
	private List<CtNotice> noticesList = new ArrayList<CtNotice>();
	
	public CtNotice getNotice() {
		return notice;
	}

	public void setNotice(CtNotice notice) {
		this.notice = notice;
	}

	public List<CtNotice> getNoticesList() {
		return noticesList;
	}

	public void setNoticesList(List<CtNotice> noticesList) {
		this.noticesList = noticesList;
	}
	private CtGoodsCategory goodsCategory = new CtGoodsCategory();
	private List<CtGoodsCategory> goodsCategoriesList = new ArrayList<CtGoodsCategory>();
	
	public CtGoodsCategory getGoodsCategory() {
		return goodsCategory;
	}

	public void setGoodsCategory(CtGoodsCategory goodsCategory) {
		this.goodsCategory = goodsCategory;
	}

	public List<CtGoodsCategory> getGoodsCategoriesList() {
		return goodsCategoriesList;
	}

	public void setGoodsCategoriesList(List<CtGoodsCategory> goodsCategoriesList) {
		this.goodsCategoriesList = goodsCategoriesList;
	}

	
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getUcode() {
		return ucode;
	}

	public void setUcode(String ucode) {
		this.ucode = ucode;
	}
	public String addadvice(){
		List<String> l = new ArrayList<String>();
		try {
			String name=this.getName();
			String em=this.getEm();
			String mb=this.getMb();
			String nr=this.getNr();
			CtSuggestion advice=new CtSuggestion();
			advice.setGtel(mb);
			advice.setGemail(em);
			advice.setGdesc(nr);
			String res = this.userManager.addadvice(advice);
			if(res.equals("Exerror")){
				String url = userDTO.getUrl();
				//System.out.println(url);
				String errorTotle = "抱歉，提交异常";
				String errorMess = "含非法字符";
				session.put("errorMess", errorMess);
				session.put("errorTotle", errorTotle);
				session.put("errorBackUrl", url);
				return ERROR;
		  }
			if(res != null){
				l.add("success");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;	
			}else {
				l.add("error");
				JSONArray json = new JSONArray();
				json = JSONArray.fromObject(l);
				resultJson = json;
				return SUCCESS;
			}
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			l.add("error");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		}
	}
	
	public String qqLoginAndReg(){
		 try {
			HttpServletRequest requ = ServletActionContext.getRequest();
			String len = new Oauth().getAuthorizeURL(requ);
			session.put("len", len);
		} catch (QQConnectException e) {
			//e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String shuoshuo(){
		try {
			HttpServletRequest requ = ServletActionContext.getRequest();
			HttpServletRequest respo = ServletActionContext.getRequest();
			
			requ.setCharacterEncoding("utf-8");
			    String con = requ.getParameter("con");
			    HttpSession session = requ.getSession();
			    String accessToken = (String)session.getAttribute("demo_access_token");
			    String openID = (String)session.getAttribute("demo_openid");
			    //System.out.println(accessToken);
			    //System.out.println(openID);
			    //请开发者自行校验获取的con值是否有效
			    if (con != "") {
			        Topic topic = new Topic(accessToken, openID);
			        try {
			            GeneralResultBean grb = topic.addTopic(con);
			            if (grb.getRet() == 0) {
			            	//respo.getWriter().println("<a href=\"http://www.qzone.com\" target=\"_blank\">您的说说已发表成功，请登录Qzone查看</a>");
			            } else {
			            	//respo.getWriter().println("很遗憾的通知您，发表说说失败！原因： " + grb.getMsg());
			            }
			        } catch (QQConnectException e) {
			            //System.out.println("抛异常了？");
			        }
			    } else {
			        //System.out.println("获取到的值为空？");
			    }
		} catch (UnsupportedEncodingException e) {
			//e.printStackTrace();
		}
		return SUCCESS;
	}
	private List<CtExtension> extensions = new ArrayList<CtExtension>();
	
	public List<CtExtension> getExtensions() {
		return extensions;
	}

	public void setExtensions(List<CtExtension> extensions) {
		this.extensions = extensions;
	}

	public String getDiscountInfo(){
		CtUser cu = (CtUser) this.session.get("userinfosession");
		ctUser = userManager.getCtUserByUUserid(cu.getUUserid());
		if(ctUser.getUExtensionid() == null){
			Random rand = new Random();
			int []ret=new int[4];
			String suiji = "";
			for(int i=0;i<ret.length;i++){
			   ret[i]=rand.nextInt(10);
			   suiji += ret[i];
			}
			ctUser.setUExtensionid(ctUser.getUId()+suiji);
			userManager.update(ctUser);
		}
		HttpServletRequest requ = ServletActionContext.getRequest();
		String url = requ.getRequestURL().toString();
		int lastIndex = url.lastIndexOf("/");
		url = url.substring(0, lastIndex+1) + "extension";
		
		url += "?exTenUserId="+ctUser.getUExtensionid();
		extensions = userManager.findExTenByUserId(ctUser.getUId());
		request.put("tuigangUrl", url);
		return SUCCESS;
	}
	
	public String errorpage(){
		return "errorpage";
	}
	private CtDrawPacket packet = new CtDrawPacket();
	private List<CtDrawPacket> packetsList = new ArrayList<CtDrawPacket>();
	
	public CtDrawPacket getPacket() {
		return packet;
	}

	public void setPacket(CtDrawPacket packet) {
		this.packet = packet;
	}

	public List<CtDrawPacket> getPacketsList() {
		return packetsList;
	}

	public void setPacketsList(List<CtDrawPacket> packetsList) {
		this.packetsList = packetsList;
	}
	private CtDrawDetail drawDetail = new CtDrawDetail();
	private List<CtDrawDetail> drawDetailsList = new ArrayList<CtDrawDetail>();
	

	public CtDrawDetail getDrawDetail() {
		return drawDetail;
	}

	public void setDrawDetail(CtDrawDetail drawDetail) {
		this.drawDetail = drawDetail;
	}

	public List<CtDrawDetail> getDrawDetailsList() {
		return drawDetailsList;
	}

	public void setDrawDetailsList(List<CtDrawDetail> drawDetailsList) {
		this.drawDetailsList = drawDetailsList;
	}
	private CtDraw draw = new CtDraw();
	private List<CtDraw> drawsList = new ArrayList<CtDraw>();
	
	
	public CtDraw getDraw() {
		return draw;
	}

	public void setDraw(CtDraw draw) {
		this.draw = draw;
	}

	public List<CtDraw> getDrawsList() {
		return drawsList;
	}

	public void setDrawsList(List<CtDraw> drawsList) {
		this.drawsList = drawsList;
	}

	//跳转抽奖页面
	public String goActiv() throws ParseException{
		Long nowD = new Date().getTime();
		//获取活动时间
		Long t = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2017-01-16 00:00:00").getTime();
		
		if(nowD < t){
			request.put("myDrawNum", 0);
			request.put("nowCount", 900);
			return SUCCESS;
		}
		
		CtUser cu = (CtUser) this.session.get("userinfosession");
		drawDetailsList = userManager.findDrawDetailListTop20();
		if(drawDetailsList != null){
			for (int i = 0; i < drawDetailsList.size(); i++) {
				String uname = drawDetailsList.get(i).getDUsername();
				if(uname.length() > 2){
					String name = uname.replaceAll("CT_", "");
					drawDetailsList.get(i).setMdName(name.substring(0, 1) + "***" + name.substring(uname.length()-1, name.length()));
				} else {
					drawDetailsList.get(i).setMdName(uname.substring(0, 1) + "***");
				}
			}
		}
		int allHongCount = userManager.findAllHongCount();
		request.put("drawDetailsList", drawDetailsList);
		if(cu != null){
			draw = userManager.findDrawByUid(cu.getUId());
			if(draw == null){
				draw = new CtDraw();
				draw.setUId(cu.getUId().intValue());
				draw.setDrNum(1);
				userManager.updateDraw(draw);
			}
			request.put("myDrawNum", draw.getDrNum());
			
			drawDetailsList = userManager.findMyDrawInfo(cu.getUId());
			request.put("myDrawDetailsList", drawDetailsList);
		} else {
			request.put("myDrawNum", 0);
			session.put("activitynologin", "goActiv");
		}
		request.put("nowCount", allHongCount);
		return SUCCESS;
	}
	
	//查询剩余抽奖次数
	public String ajaxFindMyDrawNum() throws ParseException{
		Long nowD = new Date().getTime();
		//获取活动时间
		Long t = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2017-01-16 00:00:00").getTime();
		
		if(nowD < t){
			result = "-20";
			return SUCCESS;
		}
		
		CtUser cu = (CtUser) this.session.get("userinfosession");
		if(cu != null){
			draw = userManager.findDrawByUid(cu.getUId());
			result = draw.getDrNum().toString();
		} else {
			result = "-10";
		}
		return SUCCESS;
	}
	
	//查询剩余红包数
	public String ajaxFindAllCountHong(){
		CtUser cu = (CtUser) this.session.get("userinfosession");
		if(cu != null){
			int allHongCount = userManager.findAllHongCount();
			result = allHongCount + "";
		} else {
			result = "-1";
		}
		return SUCCESS;
	}
	
	//更新页面奖池信息
	public String ajaxDrawInfoU(){
		CtUser cu = (CtUser) this.session.get("userinfosession");
		String str = "";
		if(cu != null){
			//获取自己最新抽奖记录
			drawDetailsList = userManager.findMyDrawInfo(cu.getUId());
			for (int i = 0; i < drawDetailsList.size(); i++) {
				str += "<li>";
				str += "<span>"+drawDetailsList.get(i).getDTime()+"</span>";
				str += "<span>"+drawDetailsList.get(i).getDSn()+"</span>";
				str += "</li>";
			}
			result = str;
		}
//		<li>
//            <span class="s1">${list.mdName }</span>
//            <span class="s2">${list.DTime }</span>
//            <span class="s3">${list.DSum }元微信红包</span>
//        </li>
		str = "";
		drawDetailsList = userManager.findDrawDetailListTop20();
		for (int i = 0; i < drawDetailsList.size(); i++) {
			String uname = drawDetailsList.get(i).getDUsername();
			if(uname.length() > 2){
				String name = uname.replaceAll("CT_", "");
				drawDetailsList.get(i).setMdName(name.substring(0, 1) + "***" + name.substring(uname.length()-1, name.length()));
			} else {
				drawDetailsList.get(i).setMdName(uname.substring(0, 1) + "***");
			}
			str += "<li>";
			str += "<span class=\"s1\">"+drawDetailsList.get(i).getMdName()+"</span>";
			str += "<span class=\"s2\">"+drawDetailsList.get(i).getDTime()+"</span>";
			str += "<span class=\"s3\">"+drawDetailsList.get(i).getDSum()+"元微信红包</span>";
			str += "</li>";
		}
		result = result + "|" + str;
		return SUCCESS;
	}
	
	//查询当前可中奖的金额
	public String ajaxFindDrawNum() throws ParseException{
		//检查是否在活动时间
		Long nowD = new Date().getTime();
		//获取活动时间
		Long t = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2017-01-16 00:00:00").getTime();
		
		if(nowD < t){
			result = "-2";
			return SUCCESS;
		}
		
		//验证请求是否合法
		String timesp = userDTO.getTimespace();
		Long nowDateTime = new Date().getTime();
		if(timesp != null){
//			String timeStr = nowDateTime.toString().substring(0, 8);
			Long fiveTime = 12000L;
			Long timeWeb = Long.valueOf(timesp);
			
//			String webTimeStr  = timesp.substring(0,8);
			if((nowDateTime-fiveTime)<timeWeb){
				CtUser cu = (CtUser) this.session.get("userinfosession");
				if(cu != null){
					Random ra =new Random();
					packetsList = userManager.findPacketsByNowHong();
					int isZhong = ra.nextInt(2);
					CtDrawDetail drawDetailNow = userManager.findDrawDetailByUid(cu.getUId());
					if(drawDetailNow != null && drawDetailNow.getDNum() >=2){
						isZhong = 0;
					}
					if(isZhong == 1){
						if(packetsList != null && packetsList.size() > 0){
							//随机选出一个中奖对象
							int drawNum = 0;
							List<Integer> dnList = new ArrayList<Integer>(); 
							//按照比例生成中奖概率 
							for (int i = 0; i < packetsList.size(); i++) {
								if(packetsList.get(i).getRMon().toString().equals("5")){
									dnList.add(0);
									dnList.add(0);
									dnList.add(0);
									dnList.add(0);
								}
								if(packetsList.get(i).getRMon().toString().equals("10")){
									dnList.add(1);
									dnList.add(1);
									dnList.add(1);
								}
								if(packetsList.get(i).getRMon().toString().equals("20")){
									dnList.add(2);
									dnList.add(2);
								}
								if(packetsList.get(i).getRMon().toString().equals("50")){
									dnList.add(3);
								}
							}
							//查询是否已经抽奖
							CtDrawDetail drawDetailLow = userManager.findDrawDetailByUid(cu.getUId());
							drawDetail = new CtDrawDetail();
							if(drawDetailLow == null ){
								drawDetail.setDNum(1);
							} else {
								drawDetail.setDNum(drawDetailLow.getDNum() + 1);
							}
							int dsum = 0;
							drawNum = dnList.get(ra.nextInt(dnList.size()));
							
							//根据指定号码生成指定中奖金额
							switch (drawNum) {
							case 0:
								dsum = 5;
								break;
							case 1:
								dsum = 10;
								break;
							case 2:
								dsum = 20;
								break;
							case 3:
								dsum = 50;
								break;
								
							default:
								break;
							}
							
							drawDetail.setDSum(dsum);
							//生成领取码
							Date dt = new Date();
							Long time = dt.getTime();
							Long osn = time * ra.nextInt(5000)+1;
							String dsn = "CTEGO"
									+ osn.toString().substring(osn.toString().length() - 10,
											osn.toString().length());
							drawDetail.setDSn(dsn);
							drawDetail.setUId(cu.getUId().intValue());
//						//如果名字包含CT_，去除
//						String userName = cu.getUUsername().replaceAll("CT_", "");
							drawDetail.setDUsername(cu.getUUsername());
							drawDetail.setDTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
							
							userManager.updateDrawDetail(drawDetail);
							
							for (int i = 0; i < packetsList.size(); i++) {
								if(packetsList.get(i).getRMon() == dsum){
									packet = packetsList.get(i);
								}
							}
							//更新当前奖池数量
							packet.setRRNum(packet.getRRNum() - 1);
							packet.setRJNum(packet.getRJNum() - 1);
							userManager.updatePacket(packet);
							int allHongCount = userManager.findAllHongCount();
							
							result = drawNum+"__"+allHongCount+"__"+dsn + "";
						} else {
							result = "4";
						}
					} else {
						result = "4";
					}
				} else {
					result = "-1";
				}
				//更新用户抽奖次数
				draw = userManager.findDrawByUid(cu.getUId());
				if(draw != null){
					draw.setDrNum(draw.getDrNum() - 1);
					userManager.updateDraw(draw);
				}
			} else {
				result="error";
			}
		}
		
		return SUCCESS;
	}

	public String noPassLoginOrReg(){
		String phone = userDTO.getUserPhone();
		String yzm = userDTO.getYzmNoPass();
		if(phone != null && !phone.equals("")){
			CtSms sms = (CtSms) fgoodsManager.findByTypeName(phone+","+yzm, "UMb,sms", "CtSms", 0);
			if(sms != null){
				CtUser user = (CtUser) fgoodsManager.findByTypeName(phone, "UMb","CtUser",0);
				//判断是否需要注册
				if(user == null){
					user = new CtUser();
					user.setUUserid(phone);
					user.setUUsername(phone);
					user.setUCode(UUID.randomUUID().toString());
					Long t = new Date().getTime();
					String passwd = new Random().nextInt(Integer.valueOf(t.toString().substring(1, 5)).intValue()) + "";
					String passwdorg = "CT" + phone + passwd;
					String encryptPwd = MD5.sign(passwdorg, "CT", "utf-8");
					user.setUPassword(encryptPwd);
					user.setUMb(phone);
					user.setUState("1");
					user.setURestype("0");
					// ct.setRId((long) 0);
					Long id = this.userManager.save(user);
					
					this.session.put("userinfosession", user);
					user.setULastTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
							.format((new Date())));
					HttpServletRequest req = ServletActionContext.getRequest();
					String Ip =UtilDate.getIpAddr(req);
					user.setULastIp(Ip);
					user.setURegTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format((new Date())));
					userManager.update(user);
					if(user.getUMb() != null){
						//发送注册成功短信通知
						s.regMess(user.getUMb(), user.getUUserid());
					}
					result="success";
				} else {
					user.setULastTime(new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss")
							.format((new Date())));
					HttpServletRequest req = ServletActionContext.getRequest();
					String Ip =UtilDate.getIpAddr(req);
					user.setULastIp(Ip);
					userManager.update(user);
					this.session.put("userinfosession", user);
					result="success";
				}
			} else {
				result="您输入的验证码与手机号不匹配";
			}
		}
		return SUCCESS;
	}
	private serviceInterface s = new serviceInterface();


	public serviceInterface getS() {
		return s;
	}

	public void setS(serviceInterface s) {
		this.s = s;
	}
	private CtUserAddressManager addressManager;
	
	public CtUserAddressManager getAddressManager() {
		return addressManager;
	}

	public void setAddressManager(CtUserAddressManager addressManager) {
		this.addressManager = addressManager;
	}
	private CtUserAddress userAddress;
	private List<CtUserAddress> userAddressesList = new ArrayList<CtUserAddress>();
	
	public CtUserAddress getUserAddress() {
		return userAddress;
	}

	public void setUserAddress(CtUserAddress userAddress) {
		this.userAddress = userAddress;
	}

	public List<CtUserAddress> getUserAddressesList() {
		return userAddressesList;
	}

	public void setUserAddressesList(List<CtUserAddress> userAddressesList) {
		this.userAddressesList = userAddressesList;
	}
	private List<CtUesrClient> uesrClientsList = new ArrayList<CtUesrClient>();
	
	private CtUesrClient uesrClient = new CtUesrClient();
	
	
	public List<CtUesrClient> getUesrClientsList() {
		return uesrClientsList;
	}

	public void setUesrClientsList(List<CtUesrClient> uesrClientsList) {
		this.uesrClientsList = uesrClientsList;
	}

	public CtUesrClient getUesrClient() {
		return uesrClient;
	}

	public void setUesrClient(CtUesrClient uesrClient) {
		this.uesrClient = uesrClient;
	}

	//跳转客户列表
	public String addCusAddress(){
		session.remove("isd");
		page = new Page();
		if (userDTO.getPage() == 0) {
			userDTO.setPage(1);
		}
		page.setCurrentPage(userDTO.getPage());
		
		CtUser us = (CtUser) session.get("userinfosession");
		Long uid = us.getUId();
		if(uid != null){
//			userAddressesList = userManager.findAllAddressByUid(uid, page);
			uesrClientsList = userManager.findCusMent(uid, page);
			session.put("cusUid", uid);
		} else {
			session.put("cusUid", userDTO.getUId());
//			userAddressesList = userManager.findAllAddressByUid(userDTO.getUId(), page);
			uesrClientsList = userManager.findCusMent(userDTO.getUId(), page);
		}
//		String isd = "false";
//		if(userAddressesList != null && uesrClientsList != null){
//			for (int j = 0; j < uesrClientsList.size(); j++) {
//				for (int i = 0; i < userAddressesList.size(); i++) {
//					if(userAddressesList.get(i).getCid() != null && userAddressesList.get(i).getACustomer() != null && userAddressesList.get(i).getCid().toString().equals(uesrClientsList.get(j).getCid().toString()) && userAddressesList.get(i).getACustomer().equals("2")){
//						uesrClientsList.get(j).setIsDefault("1");
//						isd = "true";
//						break;
//					}
//				}
//			}
//		}
//		session.put("isd", isd);
//		ListSortUtil<CtUesrClient> l = new ListSortUtil<CtUesrClient>();
//		l.sort(uesrClientsList, "isDefault", "desc");
//		
		page.setTotalPage(page.getTotalPage());
		
		
//		regionsList = regionManager.queryById(0L);
		
		this.request.put("pages", page);
		return SUCCESS;
	}
	
	
	//设置下单客户
	public String updateCusAddress(){
		Long cid = userDTO.getId();
		CtUser us = (CtUser) session.get("userinfosession");
		userManager.updateCusDefultByAid(cid, us.getUId());
		
		System.out.println(cid);
		return SUCCESS;
	}
	
}

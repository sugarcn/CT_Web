package org.ctonline.action.user;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.ctonline.dto.user.CtAddTicketDTO;
import org.ctonline.dto.user.CtUserDTO;
import org.ctonline.manager.user.CtAddTicketManager;
import org.ctonline.po.basic.CtSystemInfo;
import org.ctonline.po.user.CtAddTicket;
import org.ctonline.po.user.CtUser;
import org.ctonline.util.ImageUtil;
import org.ctonline.util.OSSObjectSample;
import org.ctonline.util.Page;

import com.aliyun.oss.OSSClient;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class CtAddTicketAction extends ActionSupport implements RequestAware,
		ModelDriven<Object>,SessionAware {
	private JSONArray resultJson;
	private List<CtAddTicket> addTickets;
	private CtAddTicketManager addTicketManager;
	private Map<String, Object> request;
	private Map<String, Object> session;
	private Page page;
	private CtAddTicketDTO addTicketDTO = new CtAddTicketDTO();
	private CtAddTicket addTicket;
	private CtUser ctUser;
	private CtUserDTO userDTO = new CtUserDTO();

	private List<File> file; // 上传文件集合
	private List<String> fileFileName; // 上传文件名集合
	private List<String> fileContentType; // 上传文件内容集合
	private String level;
	private String type;
	private String result;

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.addTicketDTO;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		// TODO Auto-generated method stub
		this.request = request;
	}

	public String list() {
		
		CtUser cu = (CtUser) this.session.get("userinfosession");
		
		// if("2".endsWith(level)){
		page = new Page();
		if (addTicketDTO.getPage() == 0) {
			addTicketDTO.setPage(1);
		}
		page.setCurrentPage(addTicketDTO.getPage());
		this.addTickets = addTicketManager.loadAll(page,cu);
		page.setTotalPage(page.getTotalPage());
		this.request.put("pages", page);
		this.request.put("level", 2);
		return "list";
		// }else if ("1".endsWith(level))d {
		// Long Uid = 1l;
		// page = new Page();
		// if (addTicketDTO.getPage() == 0) {
		// addTicketDTO.setPage(1);
		// }
		// page.setCurrentPage(addTicketDTO.getPage());
		// this.addTickets = addTicketManager.loadOne(page, Uid);
		// page.setTotalPage(page.getTotalPage());
		// this.request.put("pages", page);
		// return SUCCESS;
		// }
		// return null;
	}
	
	//根据UId查询增票
	public String getOneTicket(){
		CtUser cu = (CtUser) this.session.get("userinfosession");
		
		page = new Page();
		if (addTicketDTO.getPage() == 0) {
			addTicketDTO.setPage(1);
		}
		page.setCurrentPage(addTicketDTO.getPage());
		this.addTickets = addTicketManager.loadAll(page,cu);
		page.setTotalPage(page.getTotalPage());
		this.request.put("pages", page);
		this.request.put("level", 2);
		
		return "showticket";
	}

	// 新增票跳转
	public String add() {

			return "add";
	}
	
	//根据UId删除增票
	public String del(){
		Long uId = this.addTicketDTO.getATId();
		int d = addTicketManager.delete(uId);
		return "del";
	}

	// 审核
	public String shenhe() {
		//System.out.println(type);
		//System.out.println(addTicket.getATId());
		Long ATId = addTicket.getATId();
		CtAddTicket ctAddTicket = this.addTicketManager.findById(ATId);
		if("pass".equals(type)){
			ctAddTicket.setIsPass("1");
		}else{
			ctAddTicket.setIsPass("0");
		}
		this.addTicketManager.update(ctAddTicket);
		return "shenhe";
	}
	
	/*
	 *跟新增票信息
	 */
	public String update(){
		Long id = this.addTicketDTO.getATId();
		
		CtAddTicket tic = addTicketManager.findById(id);
		tic.setCompanyName(addTicketDTO.getCompanyName());
		tic.setCompanySn(addTicketDTO.getCompanySn());
		tic.setRegisteAddress(addTicketDTO.getRegisteAddress());
		tic.setRegisteTel(addTicketDTO.getRegisteTel());
		tic.setDepositBank(addTicketDTO.getDepositBank());
		tic.setBankAccount(addTicketDTO.getBankAccount());
		
		String res=addTicketManager.update(tic);
		if(res.equals("Exerror")){
			String url = addTicketDTO.getUrl();
			//System.out.println(url);
			String errorTotle = "抱歉，提交异常";
			String errorMess = "含非法字符";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			session.put("errorBackUrl", url);
			return ERROR;
		}
		return "update";
	}
	
	/*
	 * 修改增票默认地址
	 */
	public String setTehDeafult(){
		try {
			
			Long Tid = addTicketDTO.getATId();
			CtUser cu = (CtUser) this.session.get("userinfosession");
			this.addTicketManager.setTehDeafult(Tid, cu);
			
			return "setTehDeafult";
		} catch (Exception e) {
			return "error";
		}
		
	}
	
	//查询单条
	public String getFindByATId(){
		
		try {
			Long id = addTicketDTO.getATId();
			List<String> l = new ArrayList<String>();
			CtAddTicket tic = this.addTicketManager.findById(id);
			l.add(tic.getATId().toString());
			l.add(tic.getCtUser().getUId().toString());
			l.add(tic.getCompanyName());
			l.add(tic.getCompanySn());
			l.add(tic.getRegisteAddress());
			l.add(tic.getRegisteTel());
			l.add(tic.getDepositBank());
			l.add(tic.getBankAccount());
			l.add(tic.getLisence());
			l.add(tic.getTaxCertifi());
			l.add(tic.getGeneralCertifi());
			l.add(tic.getBankLisence());
			l.add(tic.getIsPass());
			l.add(tic.getLisenceSmall());
			l.add(tic.getTaxCertifiSmall());
			l.add(tic.getGeneralCertifiSmall());
			l.add(tic.getBankLisenceSmall());
			
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
			
		} catch (Exception e) {
			
			//e.printStackTrace();
			List<String> l = new ArrayList<String>();
			l.add("error");
			JSONArray json = new JSONArray();
			json = JSONArray.fromObject(l);
			resultJson = json;
			return SUCCESS;
		}
	}
	private String[] fileUpdate;
	
	public String[] getFileUpdate() {
		return fileUpdate;
	}

	public void setFileUpdate(String[] fileUpdate) {
		this.fileUpdate = fileUpdate;
	}

	// 上传图片和保存其他信息
	public String saveTicket() {
		CtUser cu = (CtUser) this.session.get("userinfosession");
		//上传目录
		String dir="";
		//时间戳
//		String date = new Date().getTime()+"";
		//带时间戳的文件名
		String timename="";
		//缩略图存储路径
		String imgSmalldir="";
		//上传图片
		if(file == null){
//			return INPUT;
		}
		//System.out.println(addTicket.getFileNames());
		CtAddTicket ticket = new CtAddTicket();
		for(int i=0;i<addTicket.getFileNames().length;i++){
			try {
				String UPLOADDIR = "/uploadmessage";
				dir = ServletActionContext.getRequest().getRealPath(UPLOADDIR);
				String time = new Date().getTime()+"";
				String fileNanes = "";
				if(i == 0){
					fileNanes = addTicket.getLisenceName();
					timename = time+"_"+cu.getUUsername() + "_" + fileNanes;
				} else if (i == 1) {
					fileNanes = addTicket.getTaxCertifiName();
					timename = time+"_"+cu.getUUsername() + "_" + fileNanes;
				} else if (i == 2) {
					fileNanes = addTicket.getGeneralCertifiName();
					timename = time+"_"+cu.getUUsername() + "_" + fileNanes;
				} else if (i == 3) {
					fileNanes = addTicket.getBankLisenceName();
					timename = time+"_"+cu.getUUsername() + "_" + fileNanes;
				}
				String url = timename;
				String date = new Date().getTime()+"";
				imgSmalldir = dir+"/"+date+ "_" + fileNanes + "_Small";
				dir = dir.replaceAll("\\\\","/");
				imgSmalldir = imgSmalldir.replaceAll("\\\\", "/");
				String urlSmall = date+ "_" + fileNanes + "_Small.jpg";
				dir = ServletActionContext.getRequest().getRealPath(UPLOADDIR);
				//System.out.println("dir="+dir);
				
				CtSystemInfo systemInfo = new CtSystemInfo();
				//创建OSS上传文件链接
				OSSClient client = new OSSClient(systemInfo.getEndpoint(),systemInfo.getAccess_id(),systemInfo.getAccess_key());
				OSSObjectSample sample = new OSSObjectSample();
				String key = systemInfo.getKey()+time+"_"+cu.getUUsername() + "_" + fileNanes;
				String filePath = addTicket.getFileNames()[i].getPath();
				//System.out.println(key);
				//System.out.println(filePath);
				//文件开始上传
				sample.uploadFile(client, systemInfo.getBucketName(), key, filePath);
				if(i==0){
					ticket.setLisence(url);
					ticket.setLisenceSmall(urlSmall);
				}else if (i==1) {
					ticket.setTaxCertifi(url);
					ticket.setTaxCertifiSmall(urlSmall);
				}else if (i==2) {
					ticket.setGeneralCertifi(url);
					ticket.setGeneralCertifiSmall(urlSmall);
				}else if (i==3) {
					ticket.setBankLisence(url);
					ticket.setBankLisenceSmall(urlSmall);
				}
				//ImageUtil.saveImageAsJpg("http://ctufile.oss-cn-shenzhen.aliyuncs.com"+"\\pay\\"+timename, imgSmalldir+".jpg", 50, 50);
			} catch (Exception e) {
				// TODO: handle exception
				//e.printStackTrace();
				//System.out.println("上传失败！");
			}
		}
		Long uId = cu.getUId();
		ticket.setATId(addTicketDTO.getATId());
		ticket.setUId(uId);
		ticket.setCompanyName(addTicket.getCompanyName());
		ticket.setCompanySn(addTicket.getCompanySn());
		ticket.setRegisteAddress(addTicket.getRegisteAddress());
		ticket.setRegisteTel(addTicket.getRegisteTel());
		ticket.setDepositBank(addTicket.getDepositBank());
		ticket.setBankAccount(addTicket.getBankAccount());
		ticket.setTheDefault("1");
		ticket.setIsPass("0");
		String res = addTicketManager.save(ticket);
		if(res.equals("Exerror")){
			String url = addTicketDTO.getUrl();
			//System.out.println(url);
			String errorTotle = "抱歉，提交异常";
			String errorMess = "含非法字符";
			session.put("errorMess", errorMess);
			session.put("errorTotle", errorTotle);
			session.put("errorBackUrl", url);
			return ERROR;
		}
		return "saveAdd";
	}
	
	//判断增票是否存在
	public String isExists(){
		try {
			if(this.session.get("userinfosession") != null){
				CtUser cu = (CtUser) this.session.get("userinfosession");
				Long UId = null;
					 UId = cu.getUId();
					 CtAddTicket cat = addTicketManager.isExists(UId);
					 if(cat !=null){
						this.result = "exists";
					 }else {
						this.result = "notExists";
					}
			}
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return "error";
		}
	}

	private static String getExtention(String fileName) {
		int pos = fileName.lastIndexOf(".");
		return fileName.substring(pos);
	}

	public List<CtAddTicket> getAddTickets() {
		return addTickets;
	}

	public void setAddTickets(List<CtAddTicket> addTickets) {
		this.addTickets = addTickets;
	}

	public CtAddTicketManager getAddTicketManager() {
		return addTicketManager;
	}

	public void setAddTicketManager(CtAddTicketManager addTicketManager) {
		this.addTicketManager = addTicketManager;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public CtAddTicketDTO getAddTicketDTO() {
		return addTicketDTO;
	}

	public void setAddTicketDTO(CtAddTicketDTO addTicketDTO) {
		this.addTicketDTO = addTicketDTO;
	}

	public CtAddTicket getAddTicket() {
		return addTicket;
	}

	public void setAddTicket(CtAddTicket addTicket) {
		this.addTicket = addTicket;
	}

	public Map<String, Object> getRequest() {
		return request;
	}

	public CtUser getCtUser() {
		return ctUser;
	}

	public void setCtUser(CtUser ctUser) {
		this.ctUser = ctUser;
	}

	public CtUserDTO getUserDTO() {
		return userDTO;
	}

	public void setUserDTO(CtUserDTO userDTO) {
		this.userDTO = userDTO;
	}

	public List<File> getFile() {
		return file;
	}

	public void setFile(List<File> file) {
		this.file = file;
	}

	public List<String> getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(List<String> fileFileName) {
		this.fileFileName = fileFileName;
	}

	public List<String> getFileContentType() {
		return fileContentType;
	}

	public void setFileContentType(List<String> fileContentType) {
		this.fileContentType = fileContentType;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public JSONArray getResultJson() {
		return resultJson;
	}

	public void setResultJson(JSONArray resultJson) {
		this.resultJson = resultJson;
	}
	public String errorpage(){
		return "errorpage";
	}
}

package org.ctonline.analysis;

public class Sample {

	private Realm1 realm1;
	private Realm2 realm3;
	private Realm3 realm2;
	public Realm1 getRealm1() {
		return realm1;
	}
	public void setRealm1(Realm1 realm1) {
		this.realm1 = realm1;
	}
	public Realm2 getRealm3() {
		return realm3;
	}
	public void setRealm3(Realm2 realm3) {
		this.realm3 = realm3;
	}
	public Realm3 getRealm2() {
		return realm2;
	}
	public void setRealm2(Realm3 realm2) {
		this.realm2 = realm2;
	}
	
}

package org.ctonline.analysis;

import java.util.Map;

public class JsonGoods {
	private String category_name;
	private String search_key;
	private String created;
	//private Integer sku_id;
	private Integer sale_price;
	private Integer promotion_price;
	private String unit_name;
	private String short_desc;
	private String seller_outer_no;
	private String modified;
	private Integer market_price;
	private String seller_nick;
	
	//private Integer changed;
	
	
//	public Integer getChanged() {
//		return changed;
//	}
//	public void setChanged(Integer changed) {
//		this.changed = changed;
//	}
	//private String brand_name;
	
	private Sample sample;
	private Batch batch;
	private Map<String, String> custom_props;
	private Map<String, Integer> sku_unit;
	
//	public String getBrand_name() {
//		return brand_name;
//	}
//	public void setBrand_name(String brand_name) {
//		this.brand_name = brand_name;
//	}
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}
	public String getSearch_key() {
		return search_key;
	}
	public void setSearch_key(String search_key) {
		this.search_key = search_key;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
//	public Integer getSku_id() {
//		return sku_id;
//	}
//	public void setSku_id(Integer sku_id) {
//		this.sku_id = sku_id;
//	}
	public Integer getSale_price() {
		return sale_price;
	}
	public void setSale_price(Integer sale_price) {
		this.sale_price = sale_price;
	}
	public Integer getPromotion_price() {
		return promotion_price;
	}
	public void setPromotion_price(Integer promotion_price) {
		this.promotion_price = promotion_price;
	}
	public String getShort_desc() {
		return short_desc;
	}
	public void setShort_desc(String short_desc) {
		this.short_desc = short_desc;
	}
	public String getSeller_outer_no() {
		return seller_outer_no;
	}
	public void setSeller_outer_no(String seller_outer_no) {
		this.seller_outer_no = seller_outer_no;
	}
	public String getModified() {
		return modified;
	}
	public void setModified(String modified) {
		this.modified = modified;
	}
	public Integer getMarket_price() {
		return market_price;
	}
	public void setMarket_price(Integer market_price) {
		this.market_price = market_price;
	}
	public String getSeller_nick() {
		return seller_nick;
	}
	public void setSeller_nick(String seller_nick) {
		this.seller_nick = seller_nick;
	}
	public Sample getSample() {
		return sample;
	}
	public void setSample(Sample sample) {
		this.sample = sample;
	}
	public Batch getBatch() {
		return batch;
	}
	public void setBatch(Batch batch) {
		this.batch = batch;
	}
	public Map<String, String> getCustom_props() {
		return custom_props;
	}
	public void setCustom_props(Map<String, String> custom_props) {
		this.custom_props = custom_props;
	}
	public Map<String, Integer> getSku_unit() {
		return sku_unit;
	}
	public void setSku_unit(Map<String, Integer> sku_unit) {
		this.sku_unit = sku_unit;
	}
	public String getUnit_name() {
		return unit_name;
	}
	public void setUnit_name(String unit_name) {
		this.unit_name = unit_name;
	}
	
}

package org.ctonline.analysis;

public class Batch {

	private Realm1 realm1;
	private Realm2 realm2;
	private Realm3 realm3;
	public Realm1 getRealm1() {
		return realm1;
	}
	public void setRealm1(Realm1 realm1) {
		this.realm1 = realm1;
	}
	public Realm2 getRealm2() {
		return realm2;
	}
	public void setRealm2(Realm2 realm2) {
		this.realm2 = realm2;
	}
	public Realm3 getRealm3() {
		return realm3;
	}
	public void setRealm3(Realm3 realm3) {
		this.realm3 = realm3;
	}
	
}

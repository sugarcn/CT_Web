package org.ctonline.manager.basic;

import java.util.List;

import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.basic.CtUserRank;
import org.ctonline.util.Page;

public interface CtUserRankManager {
	public Long save(CtUserRank userRank);//保存
	public CtUserRank findById(Long id);//根据id查找
	public List<CtUserRank> loadAll(Page page);//分页找出所有
	public int delete(Long id);//删除一条记录
//	public Long totalCount(String str);//统计条数
	public void update(CtUserRank userRank);//更新
	public List<CtUserRank> findAll(String keyword,Page page);//根据条件检索
	public String getMaxPoint();
	//获取所有积分
	public List<CtUserRank> getAll();
}

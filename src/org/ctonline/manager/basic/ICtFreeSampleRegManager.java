package org.ctonline.manager.basic;

import org.ctonline.po.basic.CtFreeSampleReg;

public interface ICtFreeSampleRegManager {

	public abstract void saveSamInfo(CtFreeSampleReg reg);

}
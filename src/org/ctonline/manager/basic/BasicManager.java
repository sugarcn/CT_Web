package org.ctonline.manager.basic;

import java.util.List;

import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.po.goods.CtGroupPrice;
import org.ctonline.po.goods.CtRangePrice;
import org.ctonline.po.views.ViewCateNum;
import org.ctonline.po.views.ViewSubcateNum;

public interface BasicManager {
	//查询所有商品分类
	public List<CtGoodsCategory> queryAllCate();
	//查询分类总数
	public List<ViewCateNum> cateCount();
	//子分类总数
	public List<ViewSubcateNum> subcateCount();
	//查询价格
	public String getPrice(Long uid,Long gid);
	//购物车中商品数
	public Long getCartGoodsNum(Long uid);
	//根据商品编号的商品的价格区间
	public List<CtRangePrice> getAllRanPrice(Long gid);
	//查询商品组的价格区间
	public CtGroupPrice getAllGroupPrice(Long gid);
}

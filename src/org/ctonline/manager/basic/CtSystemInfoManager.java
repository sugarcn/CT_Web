package org.ctonline.manager.basic;

import java.util.List;

import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.basic.CtSystemInfo;
import org.ctonline.util.Page;

public interface CtSystemInfoManager {
	public void update(CtSystemInfo systemInfo);//
	public CtSystemInfo getInfo();//
}

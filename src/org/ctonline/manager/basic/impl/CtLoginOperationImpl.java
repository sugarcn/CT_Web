package org.ctonline.manager.basic.impl;

import org.ctonline.dao.basic.CtLoginOperationDAO;
import org.ctonline.manager.basic.CtLoginOperationManager;
import org.ctonline.po.basic.CtLoginOperation;

public class CtLoginOperationImpl
  implements CtLoginOperationManager
{
  private CtLoginOperationDAO loginOperationDAO;
  
  public void insertLog(CtLoginOperation loginOperation)
  {
    this.loginOperationDAO.insertLog(loginOperation);
  }
  



public CtLoginOperationDAO getLoginOperationDAO() {
	return loginOperationDAO;
}




public void setLoginOperationDAO(CtLoginOperationDAO loginOperationDAO) {
	this.loginOperationDAO = loginOperationDAO;
}




@Override
public Boolean findDescEs(String opContent) {
	CtLoginOperation oper = loginOperationDAO.findDescEs(opContent);
	if(oper != null){
		return false;
	}
	return true;
}
}

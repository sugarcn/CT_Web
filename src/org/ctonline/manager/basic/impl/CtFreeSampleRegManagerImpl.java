package org.ctonline.manager.basic.impl;

import org.ctonline.dao.basic.ICtFreeSampleRegDAO;
import org.ctonline.manager.basic.ICtFreeSampleRegManager;
import org.ctonline.po.basic.CtFreeSampleReg;

public class CtFreeSampleRegManagerImpl implements ICtFreeSampleRegManager {

	private ICtFreeSampleRegDAO sampleRegDAO;


	public ICtFreeSampleRegDAO getSampleRegDAO() {
		return sampleRegDAO;
	}


	public void setSampleRegDAO(ICtFreeSampleRegDAO sampleRegDAO) {
		this.sampleRegDAO = sampleRegDAO;
	}


	/* (non-Javadoc)
	 * @see org.ctonline.manager.basic.impl.ICtFreeSampleRegManager#insertSamInfo(org.ctonline.po.basic.CtFreeSampleReg)
	 */
	@Override
	public void saveSamInfo(CtFreeSampleReg reg) {
		sampleRegDAO.saveSamInfo(reg);
	}
	
}

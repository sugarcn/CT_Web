package org.ctonline.manager.basic.impl;

import java.util.List;

import org.ctonline.dao.basic.BasicDAO;
import org.ctonline.manager.basic.BasicManager;
import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.po.goods.CtGroupPrice;
import org.ctonline.po.goods.CtRangePrice;
import org.ctonline.po.views.ViewCateNum;
import org.ctonline.po.views.ViewSubcateNum;

public class BasicManagerImpl implements BasicManager{

	private BasicDAO basicDao;

	
	
	public BasicDAO getBasicDao() {
		return basicDao;
	}

	public void setBasicDao(BasicDAO basicDao) {
		this.basicDao = basicDao;
	}

	@Override
	public List<CtGoodsCategory> queryAllCate() {
		// TODO Auto-generated method stub
		return basicDao.queryAllCate();
	}

	@Override
	public List<ViewCateNum> cateCount() {
		// TODO Auto-generated method stub
		return basicDao.cateCount();
	}

	@Override
	public List<ViewSubcateNum> subcateCount() {
		// TODO Auto-generated method stub
		return basicDao.subcateCount();
	}

	@Override
	public String getPrice(Long uid, Long gid) {
		// TODO Auto-generated method stub
		return basicDao.getPrice(uid, gid);
	}

	@Override
	public Long getCartGoodsNum(Long uid) {
		// TODO Auto-generated method stub
		return basicDao.getCartGoodsNum(uid);
	}

	@Override
	public List<CtRangePrice> getAllRanPrice(Long gid) {
		return basicDao.getAllRanPrice(gid);
	}

	@Override
	public CtGroupPrice getAllGroupPrice(Long gid) {
		return basicDao.getAllGroupPrice(gid);
	}
}

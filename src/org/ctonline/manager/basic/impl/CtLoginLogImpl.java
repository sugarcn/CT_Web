package org.ctonline.manager.basic.impl;

import org.ctonline.manager.basic.CtLoginLogManager;
import org.ctonline.po.basic.CtLoginLog;
import org.ctonline.dao.basic.CtLoginLogDAO;

/**
 * 日志
 * @author ZWT
 *
 */
public class CtLoginLogImpl implements CtLoginLogManager {
	
	private CtLoginLogDAO loginLogDao;
	
	
	@Override
	public void inse(CtLoginLog log) {
		loginLogDao.save(log);
	}

	public CtLoginLogDAO getLoginLogDao() {
		return loginLogDao;
	}

	public void setLoginLogDao(CtLoginLogDAO loginLogDao) {
		this.loginLogDao = loginLogDao;
	}
	
}

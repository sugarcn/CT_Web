package org.ctonline.manager.basic.impl;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.ctonline.dao.basic.CtCouponDetailDAO;
import org.ctonline.manager.basic.CtCouponDetailManager;
import org.ctonline.po.basic.CtCoupon;
import org.ctonline.po.basic.CtCouponDetail;
import org.ctonline.util.Logg;
import org.ctonline.util.Page;

public class CtCouponDetailMangerImpl implements CtCouponDetailManager{
	CtCouponDetailDAO coupondetailDao;
	
	

	public CtCouponDetailDAO getCoupondetailDao() {
		return coupondetailDao;
	}

	public void setCoupondetailDao(CtCouponDetailDAO coupondetailDao) {
		this.coupondetailDao = coupondetailDao;
	}

	public CtCouponDetailDAO getCouponDetailDao() {
		return coupondetailDao;
	}

	public void setCouponDetailDao(CtCouponDetailDAO coupondetailDao) {
		this.coupondetailDao = coupondetailDao;
	}

	public Long save(CtCouponDetail coupondetail)
	{
		coupondetailDao.save(coupondetail);
		return 1l;
	}
	
	public CtCouponDetail findById(Long id)
	{
		return coupondetailDao.findById(id);
	}
	//public List<CtCoupon> loadAll(Page page);//��ҳ�ҳ�����
	public int delete(Long id)
	{
		return coupondetailDao.delete(id);
	}

	//public Long totalCount(String str);//ͳ������
	public String update(CtCouponDetail coupondetail)
	{
		return coupondetailDao.update(coupondetail);
	}
	public List<CtCouponDetail> findAll(String keyword,Page page)
	{
		return coupondetailDao.findAll(keyword, page);
	}
	public List<CtCouponDetail> findByCouponId(int id)
	{
		return coupondetailDao.findByCouponId(id);
	}
	//public Long getID();
	public List<CtCouponDetail> queryAll()
	{
		return coupondetailDao.queryAll();
	}

	@Override
	public List<CtCouponDetail> loadAll(Page page,String type1,String type2) {
		// TODO Auto-generated method stub
		return coupondetailDao.loadAll(page,type1,type2);
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long getID() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<CtCouponDetail> getAdmitGoods(Page page,String type1,String type2) {
		// TODO Auto-generated method stub
		return this.coupondetailDao.getAdmitGoods(page,type1,type2);
	}
	
	@Override
	public List<CtCouponDetail> getSamGoods(Page page,String type1,String type2) {
		// TODO Auto-generated method stub
		return this.coupondetailDao.getSamGoods(page,type1,type2);
	}

	@Override
	public CtCouponDetail getCtCouponDetailByCouponNum(String couponNum) {
		// TODO Auto-generated method stub
		return this.coupondetailDao.getCtCouponDetailByCouponNum(couponNum);
	}

	@Override
	public void activateByCouponNum(CtCouponDetail ccd) {
		// TODO Auto-generated method stub
		this.coupondetailDao.activateByCouponNum(ccd);
	}

	@Override
	public Long getCoutAll() {
		// TODO Auto-generated method stub
		return this.coupondetailDao.getCoutAll();
	}

	@Override
	public Long getCoutOwn() {
		// TODO Auto-generated method stub
		return this.coupondetailDao.getCoutOwn();
	}
	
	@Override
	public Long getCoutSam() {
		// TODO Auto-generated method stub
		return this.coupondetailDao.getCoutSam();
	}

	@Override
	public Long getCoutAdmit() {
		// TODO Auto-generated method stub
		return this.coupondetailDao.getCoutAdmit();
	}

	@Override
	public List<CtCouponDetail> getCtCouponsByGid(Long uid,Long GId) {
		// TODO Auto-generated method stub
		List<CtCouponDetail> couponDetails = this.coupondetailDao.getCtCouponsByGid(uid,GId);
		return couponDetails;
	}

	@Override
	public List<CtCouponDetail> isGetCoupom(Integer couponId) {
		// TODO Auto-generated method stub
		return this.coupondetailDao.isGetCoupom(couponId);
	}

	@Override
	public List<CtCouponDetail> getCouponDetails(Integer couponId) {
		// TODO Auto-generated method stub
		return this.coupondetailDao.getCouponDetails(couponId);
	}

	@Override
	public List<CtCouponDetail> getAllNotUse() {
		// TODO Auto-generated method stub
		return this.coupondetailDao.getAllNotUse();
	}
	
	
	@Logg(operationType="update", operationName="更新优惠券信息给用户")
	//随机对某优惠券类型，给用户分配N个优惠券
	public void udpateCouponDetails(Integer couponId,Long uid,Integer n){
		
		coupondetailDao.udpateCouponDetails(couponId,uid,n);
	}
	
	//随机对某优惠券类型，给用户分配N个优惠券
	public  boolean isDUse(Long uid){
		
		return coupondetailDao.isDUse(uid);
	}

	@Override
	public CtCoupon findByCouponById(Integer couponId) {
		return coupondetailDao.findByCouponById(couponId);
	}
	
	

}

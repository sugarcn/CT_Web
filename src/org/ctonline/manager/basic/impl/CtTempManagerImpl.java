package org.ctonline.manager.basic.impl;

import java.util.List;

import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.basic.CtTemp;
import org.ctonline.po.basic.CtUserRank;
import org.ctonline.util.Page;
import org.ctonline.dao.basic.CtRegionDAO;
import org.ctonline.dao.basic.CtTempDAO;
import org.ctonline.dao.basic.CtUserRankDAO;

public class CtTempManagerImpl implements org.ctonline.manager.basic.CtTempManager {
	private CtTempDAO tempDao;

	public CtTempDAO getTempDao() {
		return tempDao;
	}

	public void setTempDao(CtTempDAO tempDao) {
		this.tempDao = tempDao;
	}


	@Override
	public CtTemp findBySn(String sn) {
		// TODO Auto-generated method stub
		return tempDao.findBySn(sn);
	}


}

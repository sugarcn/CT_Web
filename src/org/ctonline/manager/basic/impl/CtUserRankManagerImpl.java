package org.ctonline.manager.basic.impl;

import java.util.List;

import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.basic.CtUserRank;
import org.ctonline.util.Page;
import org.ctonline.dao.basic.CtRegionDAO;
import org.ctonline.dao.basic.CtUserRankDAO;

public class CtUserRankManagerImpl implements org.ctonline.manager.basic.CtUserRankManager {
	private CtUserRankDAO userRankDao;

	public CtUserRankDAO getUserRankDao() {
		return userRankDao;
	}

	public void setUserRankDao(CtUserRankDAO userRankDao) {
		this.userRankDao = userRankDao;
	}

	@Override
	public List<CtUserRank> loadAll(Page page) {
		// TODO Auto-generated method stub
		return userRankDao.loadAll(page);
	}

	@Override
	public Long save(CtUserRank userRank) {
		// TODO Auto-generated method stub
		return userRankDao.save(userRank);
	}

	@Override
	public String getMaxPoint() {
		// TODO Auto-generated method stub
		return userRankDao.getMaxPoint();
	}

	@Override
	public CtUserRank findById(Long id) {
		// TODO Auto-generated method stub
		return userRankDao.findById(id);
	}

	//修改等级积分
	@Override
	public void update(CtUserRank userRank) {
		// TODO Auto-generated method stub
		this.userRankDao.update(userRank);
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		return userRankDao.delete(id);
	}

	@Override
	public List<CtUserRank> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		return userRankDao.findAll(keyword, page);
	}

	@Override
	public List<CtUserRank> getAll() {
		// TODO Auto-generated method stub
		return this.userRankDao.getAll();
	}

}

package org.ctonline.manager.basic.impl;


import org.ctonline.po.basic.CtSystemInfo;
import org.ctonline.dao.basic.CtSystemInfoDAO;

public class CtSystemInfoManagerImpl implements org.ctonline.manager.basic.CtSystemInfoManager {
	private CtSystemInfoDAO systemInfoDao;

	public CtSystemInfoDAO getSystemInfoDao() {
		return systemInfoDao;
	}

	public void setSystemInfoDao(CtSystemInfoDAO systemInfoDao) {
		this.systemInfoDao = systemInfoDao;
	}



	@Override
	public CtSystemInfo getInfo() {
		// TODO Auto-generated method stub
		return systemInfoDao.getInfo();
	}

	//修改等级积分
	@Override
	public void update(CtSystemInfo SystemInfo) {
		// TODO Auto-generated method stub
		this.systemInfoDao.update(SystemInfo);
	}



}

package org.ctonline.manager.basic.impl;

import java.util.List;

import org.ctonline.dao.basic.CtNoticeDAO;
import org.ctonline.manager.basic.CtNoticeManager;
import org.ctonline.po.basic.CtNotice;
import org.ctonline.util.Page;

public class CtNoticeManagerImpl implements CtNoticeManager{
	private CtNoticeDAO noticeDAO;

	public CtNoticeDAO getNoticeDAO() {
		return noticeDAO;
	}

	public void setNoticeDAO(CtNoticeDAO noticeDAO) {
		this.noticeDAO = noticeDAO;
	}

	@Override
	public List<CtNotice> loadAll(Page page, Integer typeId) {
		// TODO Auto-generated method stub
		return noticeDAO.loadAll(page, typeId);
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		return noticeDAO.totalCount(str);
	}

	@Override
	public CtNotice findById(int id) {
		// TODO Auto-generated method stub
		return noticeDAO.findById(id);
	}

	@Override
	public List<CtNotice> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		return noticeDAO.findAll(keyword, page);
	}

}

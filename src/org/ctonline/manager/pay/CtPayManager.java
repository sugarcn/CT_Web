package org.ctonline.manager.pay;

import org.ctonline.po.pay.CtPayInterface;


public interface CtPayManager {
	public Long save(CtPayInterface payInterface);//保存

	public String findPayByOrderSn(String orderSn);

	public String findXianXiaPayTimeByOrderSn(Long orderId);

	public CtPayInterface findPayInfoByOrderSn(String orderSn);
}

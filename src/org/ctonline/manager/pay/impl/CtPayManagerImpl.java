package org.ctonline.manager.pay.impl;

import java.util.List;


import org.ctonline.po.pay.CtPayInterface;
import org.ctonline.util.Logg;
import org.ctonline.util.Page;
import org.ctonline.dao.pay.CtPayDAO;

public class CtPayManagerImpl implements org.ctonline.manager.pay.CtPayManager {
	public CtPayDAO getPayDao() {
		return payDao;
	}

	public void setPayDao(CtPayDAO payDao) {
		this.payDao = payDao;
	}

	private CtPayDAO payDao;

	@Logg(operationType="save", operationName="新增支付信息")
	public Long save(CtPayInterface payInterface) {
		// TODO Auto-generated method stub
		return payDao.save(payInterface);
	}

	@Override
	public String findPayByOrderSn(String orderSn) {
		return payDao.findPayByOrderSn(orderSn);
	}

	@Override
	public String findXianXiaPayTimeByOrderSn(Long orderId) {
		return payDao.findXianXiaPayTimeByOrderSn(orderId);
	}

	@Override
	public CtPayInterface findPayInfoByOrderSn(String orderSn) {
		return payDao.findPayInfoByOrderSn(orderSn);
	}

}

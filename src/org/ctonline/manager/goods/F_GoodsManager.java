package org.ctonline.manager.goods;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.ctonline.po.basic.CtAd;
import org.ctonline.po.basic.CtCoupon;
import org.ctonline.po.basic.CtCouponDetail;
import org.ctonline.po.basic.CtNotice;
import org.ctonline.po.basic.CtReplace;
import org.ctonline.po.basic.CtResource;
import org.ctonline.po.basic.CtSearchKeyword;
import org.ctonline.po.basic.CtXwStock;
import org.ctonline.po.goods.CtBom;
import org.ctonline.po.goods.CtCollect;
import org.ctonline.po.goods.CtCollectGoods;
import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.goods.CtGoodsAttribute;
import org.ctonline.po.goods.CtGoodsAttributeRelation;
import org.ctonline.po.goods.CtGoodsBrand;
import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.po.goods.CtGoodsDetail;
import org.ctonline.po.goods.CtGoodsImg;
import org.ctonline.po.goods.CtGoodsResource;
import org.ctonline.po.goods.CtGoodsType;
import org.ctonline.po.order.CtCart;
import org.ctonline.po.views.ViewCateNum;
import org.ctonline.po.views.ViewGoodsList;
import org.ctonline.po.views.ViewSubcateNum;
import org.ctonline.util.Page;

public interface F_GoodsManager {
	//查询商品分类
	public List<CtGoodsCategory> queryAllCate();
	//分类总数
	public List<ViewCateNum> cateCount();
	//子分类总数
	public List<ViewSubcateNum> subcateCount();
	//查询品牌
	public List<CtGoodsBrand> queryBrand();
	//商品列表
	public List<ViewGoodsList> getGoodsList(Page page);
	//搜索商品
	public List<?> searchGoodsList(Long pid,String kword,Page page,String price1, String price2);
	//按品牌搜索
	public List<?> searchGoodsByBrand(Long bid,Page page);
	//获取商品
	public List<?> getGoods(Long gid);
	public CtGoods getGood(Long gid);
	//获取分类中所有商品品牌
	public List<?> callProBrand(Long pid,String kword,String price1, String price2) throws SQLException;
	//获取分类中所有商品子分类
	public List<CtGoodsCategory> getSubCate(Long pid);
	//获取分类中所有商品类型
	public List<?> callProType(Long pid,String kword,String price1, String price2);
	//获取分类中所有商品属性
	public List<?> callProAttr(Long pid,String kword,String price1, String price2);
	//获取分类中所有商品属性值
	public List<?> callProAttrVal(Long pid,String kword,String price1, String price2);
	//获取商品图片
	public List<CtGoodsImg> getImg(Long gid);
	//根据Gname查询CtGoods
	public List<CtGoods> getGoodsByGname(String Gname);
	//根据Gname查询一个CtGoods
	public Object getOneGoodsByGname(String Gname);
	//获取商品数量
	public Integer getGNum(Long gid);
	//商品介绍
	public List<CtGoodsDetail> getGoodsIntroduce(Long id);
	//替代商品
	public List<?> getGoodsReplaces(Long gid);
	//关联商品
	public List<?> getGoodsLink(Long gid);
	//按CID搜索商品
	public List<?> searchGoodsByCId(Long cid,Page page);
	//获取CID下所有品牌
	public List<?> getBrandByCId(Long cid);
	//获取CID下所有类型
	public List<?> getTypeByCId(Long cid);
	//获取CID下所有属性
	public List<?> getAttrByCId(Long cid);
	//获取CID下所有属性值
	public List<?> getAttrValByCId(Long cid);
	//获取品牌下所有类型
	public List<?> getTypeByBId(Long bid);
	//获取品牌下所有属性
	public List<?> getAttrByBId(Long bid);
	//获取品牌下所有属性值
	public List<?> getAttrValByBId(Long bid);
	
	public List<?> choseGoodsmod1(Long pid,String kword,String mod,String cont,Page page);
	public List<?> choseGoodsmod2(Long cid,String mod,String cont,Page page,String price1,String price2);
	public List<?> choseGoodsmod3(Long bid,String mod,String cont,Page page);
	//收藏商品
	public String save(CtCollectGoods ccg);
	//根据uid和Gid查询一个商品是否被收藏
	public CtCollectGoods getCtCollectGoodsByUIdAndGid(Long UId,Long GId);
	//查询收藏夹
	public List<CtCollectGoods> loadAll(Long UId);
	//商品收藏列表
	public List<?> collectList(Long UId,Page page,Long collid);
	//根据uid和Gid删除一个收藏的商品
	public Integer delGoodsByByUIdAndGid(Long UId,Long GId,Long collid);
	public List<CtGoodsCategory> queryAllCateToType(String typeNameToCid);
	public List<CtCouponDetail> getCouponByGid(Long gid);
	public List<CtCouponDetail> getCouponByCouponId(Integer couponId);
	public List<ViewGoodsList> callGoodsByPrice(String price1, String price2);
	public String getGoodsTypeByGid(Long gid);
	public Long getGoodsExTypeByTypeId(String typeId);
	public List<CtGoodsCategory> getCateByEx(Long exType);
	public List<CtCoupon> getCouponByGidNew(Long gid);
	public List<CtGoodsCategory> queryAll();
	//收藏夹分类
	public List<CtCollect> findAll(Page page);
	public String save(CtCollect collect);
	//根据bomTitle查询Bom
	public CtCollect getCtCollByCollTitle(String collTitle,String collid);
	//根据collId删除coll详情中的商品
	public void delCollGoodsByCollId(Long collid);
	//批量删除Bom
	public void delete(Long collMid);
	//根据collId查询收藏夹
	public CtCollect getCtCollByCollId(Long CollId);
	//修改收藏夹
	public String updateColl(CtCollect collect);
	//判断当前采集数量是否是最小数量
	public boolean isSmall(Long gId,Integer sn);
	//检索
	//检索
	public List<CtCollect> search(String keyword, Page page);
	//加载收藏夹里的商品详情
	public List<CtCollectGoods> loadCollDetailAll(Page page,Long collid);
	//根据GId查询CtCollectGoods
	public CtCollectGoods getCtCollGoodsByGId(Long GId,Long collid);
	//根据UId查询Coll
	public List<CtCollect> getCtCollByUId(Long UId);
	public List<CtGoodsCategory> queryAllByPid(Long pid);
	public CtGoodsCategory queryAllByCName(String cname);
	public CtGoodsCategory queryByCName(String cname);
	public CtGoodsCategory getCNameByCid(Long cid);
	public List<CtNotice> findAllNotices();
	public List<CtGoodsCategory> getSanjiFenLei();
	public CtGoodsBrand findById(Long id);
	public List<CtGoodsCategory> getSanjiFenLeiDianRong();
	public Map<Integer, String> getCateFenLeiAll(Long gId);
	public CtCart getcartBycartid(Long cartId);
	public List<CtGoods> getSubHade();
	public List<CtGoodsResource> getResultIdByGid(Long gid);
	public CtResource getResByResId(Long resourceId);
	public List<CtGoodsCategory> getCateForFirst();
	public List<CtGoodsCategory> getCateSer(List<CtGoodsCategory> cateList);
	public List<CtGoodsCategory> getCateThree(List<CtGoodsCategory> cateListSer);
	public List<CtGoodsAttributeRelation> getAttrRelByGid(Long gid);
	public CtGoodsAttribute getattrByAttrId(Long attrId);
	public List<CtGoodsAttributeRelation> getAttrRelByAttrId(Long attrId,Long gid);
	public List<CtGoodsAttribute> getAttrByGTIdOrder(Long gtId);
	public List<CtGoods> getGoodsByGid(Long gid);
	public List<CtGoods> loadGoods();
	public List<CtGoods> getGoodsByBrandId(Long bId);
	public String getCateNameByCateId(Long cId);
	public void addGoodsKey(CtSearchKeyword searchKeyWord);
	public CtSearchKeyword findByKeyWord(String kword);
	public List<CtSearchKeyword> findKeyWordOrderTop4();
	public void GoodsKeyNew(CtSearchKeyword searchKeyWord);
	public Object merge(Object ctGoods);
	public List<CtGoods> findByGSn(String seller_outer_no);
	/**
	 * 
	 * @param string 要查询的指定值，多个按逗号隔开
	 * @param string2 要查询的列，和值对应，逗号隔开
	 * @param string3 查询的表
	 * @param i 查询返回的结果 1为集合 0 为一个对象
	 * @return
	 */
	public Object findByTypeName(String string, String string2,
			String string3, int i);
	public CtXwStock findByXWStock();
	public void deleteRanPriceByGid(Long gId);
	public List<CtResource> findResouresByTopSex();
	public List<CtResource> findResoursByPage(Page page);
	public CtReplace findReplaceByKey(String key);
	public List<CtAd> findAdByIsUpAndType(int i, String string);
}

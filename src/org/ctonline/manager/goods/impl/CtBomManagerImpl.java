package org.ctonline.manager.goods.impl;

import java.util.List;

import org.ctonline.dao.goods.CtBomDao;
import org.ctonline.manager.goods.CtBomManager;
import org.ctonline.po.goods.CtBom;
import org.ctonline.po.goods.CtBomGoods;
import org.ctonline.po.goods.CtRangePrice;
import org.ctonline.po.order.CtCart;
import org.ctonline.util.Page;

public class CtBomManagerImpl implements CtBomManager {
	private CtBomDao bomDao;

	public CtBomDao getBomDao() {
		return bomDao;
	}

	public void setBomDao(CtBomDao bomDao) {
		this.bomDao = bomDao;
	}

	// 获取Bom数量
	public Integer countBom(Integer id) {
		return this.bomDao.countBom(id);
	}

	@Override
	public List<CtBom> loadAll(Page page) {
		// TODO Auto-generated method stub
		return this.bomDao.loadAll(page);
	}

	@Override
	public String save(CtBom ctBom) {
		// TODO Auto-generated method stub
		return this.bomDao.save(ctBom);
	}

	@Override
	public CtBom getCtBomByBomId(Integer BomId, Long uid) {
		// TODO Auto-generated method stub
		return this.bomDao.getCtBomByBomId(BomId, uid);
	}

	@Override
	public String update(CtBom ctBom) {
		// TODO Auto-generated method stub
		String res = this.bomDao.update(ctBom);
		return res;
	}

	@Override
	public void delete(Integer bomMid) {
		// TODO Auto-generated method stub
		this.bomDao.delete(bomMid);
	}

	@Override
	public List<CtBom> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		return this.bomDao.findAll(keyword, page);
	}

	@Override
	public List<CtBomGoods> loadBomDetailAll(Page page, Integer bomId) {
		// TODO Auto-generated method stub
		return this.bomDao.loadBomDetailAll(page, bomId);
	}

	@Override
	public void delGoodsOfBomDetail(Long bomGoodsId) {
		// TODO Auto-generated method stub
		this.bomDao.delGoodsOfBomDetail(bomGoodsId);
	}

	@Override
	public List<CtBomGoods> findBomDetailAll(String keyword, Page page,
			Integer bomId) {
		// TODO Auto-generated method stub
		return this.bomDao.findBomDetailAll(keyword, page, bomId);
	}

	@Override
	public CtBomGoods getCtBomGoodsBybomGoodsId(Long bomGoodsId) {
		// TODO Auto-generated method stub
		return this.bomDao.getCtBomGoodsBybomGoodsId(bomGoodsId);
	}

	@Override
	public String saveCtBomGoods(CtBomGoods cbg) {
		// TODO Auto-generated method stub
		return this.bomDao.saveCtBomGoods(cbg);
	}

	@Override
	public String updateCtBomGoods(CtBomGoods cbg) {
		// TODO Auto-generated method stub
		return this.bomDao.updateCtBomGoods(cbg);
	}

	@Override
	public CtBomGoods getCtBomGoodsByGId(Long GId, Integer bomId) {
		// TODO Auto-generated method stub
		return this.bomDao.getCtBomGoodsByGId(GId, bomId);
	}

	@Override
	public CtBomGoods getCtBomGoodsByGIdandPack(Long GId, Integer bomId,
			String pack) {
		// TODO Auto-generated method stub
		return this.bomDao.getCtBomGoodsByGIdandPack(GId, bomId, pack);
	}

	@Override
	public CtBom getCtBomByBomTitle(String bomTitle,String bomId) {
		// TODO Auto-generated method stub
		return this.bomDao.getCtBomByBomTitle(bomTitle,bomId);
	}

	@Override
	public List<CtBomGoods> getCtBomGoodsByBomId(Integer bomId) {
		// TODO Auto-generated method stub
		return this.bomDao.getCtBomGoodsByBomId(bomId);
	}

	@Override
	public List<CtBom> loadAllHot(Page page) {
		// TODO Auto-generated method stub
		return this.bomDao.loadAllHot(page);
	}

	@Override
	public List<CtBom> loadAllHotLogin(Page page) {
		// TODO Auto-generated method stub
		return this.bomDao.loadAllHotLogin(page);
	}

	@Override
	public List<CtBom> getCtBomsByUId(Long UId) {
		// TODO Auto-generated method stub
		return this.bomDao.getCtBomsByUId(UId);
	}

	@Override
	public List<CtBom> findByKeyWord(String keyword, Page page) {
		// TODO Auto-generated method stub
		return this.bomDao.findByKeyWord(keyword, page);
	}

	@Override
	public List<CtBom> findByKeyWordLogin(String keyword, Page page) {
		// TODO Auto-generated method stub
		return this.bomDao.findByKeyWordLogin(keyword, page);
	}

	@Override
	public void delBomGoodsByBomId(Integer bomId) {
		// TODO Auto-generated method stub
		this.bomDao.delBomGoodsByBomId(bomId);
	}

	@Override
	public List<CtBom> loadAll() {
		// TODO Auto-generated method stub
		return this.bomDao.loadAll();
	}

	@Override
	public List<CtBomGoods> getCtBomGoodsByGIdAndBomId(Long GId, Integer bomId) {
		// TODO Auto-generated method stub
		return this.bomDao.getCtBomGoodsByGIdAndBomId(GId, bomId);
	}

	@Override
	public CtBomGoods getCtBomGoodsByGIdandPack(Long gid, String pack,
			Integer bomid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CtCart getcartBycartid(Long cartId) {
		// TODO Auto-generated method stub
		return this.bomDao.getcartBycartid(cartId);
	}

	@Override
	public CtBomGoods getCtBomGoodsByGId(Long GId, Long bomId) {
		// TODO Auto-generated method stub
		return this.bomDao.getCtBomGoodsByGId(GId, bomId);
	}

	@Override
	public List<CtBom> findSome() {
		// TODO Auto-generated method stub
		return this.bomDao.findSome();
	}
	@Override
	public String updateCtBomNum(Long bomId,Long bomNum) {
		// TODO Auto-generated method stub
		return this.bomDao.updateCtBomNum(bomId,bomNum);
	}

	@Override
	public List<CtRangePrice> findRanByBGid(String gid) {
		return this.bomDao.findRanByBGid(gid);
	}


}

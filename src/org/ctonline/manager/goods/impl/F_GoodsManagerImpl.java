package org.ctonline.manager.goods.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.ctonline.dao.goods.F_GoodsDAO;
import org.ctonline.manager.goods.F_GoodsManager;
import org.ctonline.po.basic.CtAd;
import org.ctonline.po.basic.CtCoupon;
import org.ctonline.po.basic.CtCouponDetail;
import org.ctonline.po.basic.CtNotice;
import org.ctonline.po.basic.CtReplace;
import org.ctonline.po.basic.CtResource;
import org.ctonline.po.basic.CtSearchKeyword;
import org.ctonline.po.basic.CtXwStock;
import org.ctonline.po.goods.CtBom;
import org.ctonline.po.goods.CtCollect;
import org.ctonline.po.goods.CtCollectGoods;
import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.goods.CtGoodsAttribute;
import org.ctonline.po.goods.CtGoodsAttributeRelation;
import org.ctonline.po.goods.CtGoodsBrand;
import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.po.goods.CtGoodsDetail;
import org.ctonline.po.goods.CtGoodsImg;
import org.ctonline.po.goods.CtGoodsResource;
import org.ctonline.po.goods.CtGoodsType;
import org.ctonline.po.order.CtCart;
import org.ctonline.po.views.ViewCateNum;
import org.ctonline.po.views.ViewGoodsList;
import org.ctonline.po.views.ViewSubcateNum;
import org.ctonline.util.Page;
import org.hibernate.Query;

public class F_GoodsManagerImpl implements F_GoodsManager{

	private F_GoodsDAO fgoodsDao;

	public F_GoodsDAO getFgoodsDao() {
		return fgoodsDao;
	}

	public void setFgoodsDao(F_GoodsDAO fgoodsDao) {
		this.fgoodsDao = fgoodsDao;
	}

	@Override
	public List<CtGoodsCategory> queryAllCate() {
		// TODO Auto-generated method stub
		return fgoodsDao.queryAllCate();
	}

	@Override
	public List<ViewCateNum> cateCount() {
		// TODO Auto-generated method stub
		return fgoodsDao.cateCount();
	}
	public CtGoods getGood(Long gid){
		return fgoodsDao.getGood(gid);
	}

	@Override
	public List<ViewSubcateNum> subcateCount() {
		// TODO Auto-generated method stub
		return fgoodsDao.subcateCount();
	}

	@Override
	public List<CtGoodsBrand> queryBrand() {
		// TODO Auto-generated method stub
		return fgoodsDao.queryBrand();
	}

	@Override
	public List<ViewGoodsList> getGoodsList(Page page) {
		// TODO Auto-generated method stub
		return fgoodsDao.getGoodsList(page);
	}

	@Override
	public List<?> callProBrand(Long pid,String kword,String price1, String price2) throws SQLException {
		// TODO Auto-generated method stub
		return fgoodsDao.callProBrand(pid, kword,price1,price2);
	}

	@Override
	public List<CtGoodsCategory> getSubCate(Long pid) {
		// TODO Auto-generated method stub
		return fgoodsDao.getSubCate(pid);
	}

	@Override
	public List<?> callProType(Long pid, String kword,String price1, String price2) {
		// TODO Auto-generated method stub
		return fgoodsDao.callProType(pid, kword,price1,price2);
	}

	@Override
	public List<?> callProAttr(Long pid, String kword,String price1, String price2) {
		// TODO Auto-generated method stub
		return fgoodsDao.callProAttr(pid, kword,price1,price2);
	}

	@Override
	public List<?> callProAttrVal(Long pid, String kword,String price1, String price2) {
		// TODO Auto-generated method stub
		return fgoodsDao.callProAttrVal(pid, kword,price1,price2);
	}

	@Override
	public List<?> searchGoodsList(Long pid, String kword, Page page,String price1, String price2) {
		// TODO Auto-generated method stub
		return fgoodsDao.searchGoodsList(pid, kword, page,price1, price2);
	}

	public List<CtGoods> getGoodsByGname(String Gname) {
		// TODO Auto-generated method stub
		return this.fgoodsDao.getGoodsByGname(Gname);
	}

	@Override
	public Object getOneGoodsByGname(String Gname) {
		// TODO Auto-generated method stub
		return this.fgoodsDao.getOneGoodsByGname(Gname);

	}

	@Override
	public List<?> getGoods(Long gid) {
		// TODO Auto-generated method stub
		return fgoodsDao.getGoods(gid);
	}

	@Override
	public List<CtGoodsImg> getImg(Long gid) {
		// TODO Auto-generated method stub
		return fgoodsDao.getImg(gid);
	}

	@Override
	public Integer getGNum(Long gid) {
		// TODO Auto-generated method stub
		return fgoodsDao.getGNum(gid);
	}

	@Override
	public List<CtGoodsDetail> getGoodsIntroduce(Long id) {
		// TODO Auto-generated method stub
		return fgoodsDao.getGoodsIntroduce(id);
	}

	@Override
	public List<?> getGoodsReplaces(Long gid) {
		// TODO Auto-generated method stub
		return fgoodsDao.getGoodsReplaces(gid);
	}

	@Override
	public List<?> getGoodsLink(Long gid) {
		// TODO Auto-generated method stub
		return fgoodsDao.getGoodsLink(gid);
	}

	@Override
	public List<?> searchGoodsByCId(Long cid, Page page) {
		// TODO Auto-generated method stub
		return fgoodsDao.searchGoodsByCId(cid, page);
	}

	@Override
	public List<?> getBrandByCId(Long cid) {
		// TODO Auto-generated method stub
		return fgoodsDao.getBrandByCId(cid);
	}

	@Override
	public List<?> getTypeByCId(Long cid) {
		// TODO Auto-generated method stub
		return fgoodsDao.getTypeByCId(cid);
	}

	@Override
	public List<?> getAttrByCId(Long cid) {
		// TODO Auto-generated method stub
		return fgoodsDao.getAttrByCId(cid);
	}

	@Override
	public List<?> getAttrValByCId(Long cid) {
		// TODO Auto-generated method stub
		return fgoodsDao.getAttrValByCId(cid);
	}

	@Override
	public List<?> searchGoodsByBrand(Long bid, Page page) {
		// TODO Auto-generated method stub
		return fgoodsDao.searchGoodsByBrand(bid, page);
	}

	@Override
	public List<?> getTypeByBId(Long bid) {
		// TODO Auto-generated method stub
		return fgoodsDao.getTypeByBId(bid);
	}

	@Override
	public List<?> getAttrByBId(Long bid) {
		// TODO Auto-generated method stub
		return fgoodsDao.getAttrByBId(bid);
	}

	@Override
	public List<?> getAttrValByBId(Long bid) {
		// TODO Auto-generated method stub
		return fgoodsDao.getAttrValByBId(bid);
	}

	@Override
	public List<?> choseGoodsmod1(Long pid, String kword, String mod,String cont, Page page) {
		// TODO Auto-generated method stub
		return fgoodsDao.choseGoodsmod1(pid, kword, mod,cont, page);
	}

	@Override
	public List<?> choseGoodsmod2(Long cid, String mod,String cont, Page page,String price1, String price2) {
		// TODO Auto-generated method stub
		return fgoodsDao.choseGoodsmod2(cid, mod,cont, page,price1,price2);
	}

	@Override
	public List<?> choseGoodsmod3(Long bid, String mod,String cont, Page page) {
		// TODO Auto-generated method stub
		return fgoodsDao.choseGoodsmod3(bid, mod,cont, page);
	}

	@Override
	public String save(CtCollectGoods ccg) {
		// TODO Auto-generated method stub
		return this.fgoodsDao.save(ccg);
	}

	@Override
	public CtCollectGoods getCtCollectGoodsByUIdAndGid(Long UId, Long GId) {
		// TODO Auto-generated method stub
		return this.fgoodsDao.getCtCollectGoodsByUIdAndGid(UId, GId);
	}

	@Override
	public List<?> collectList(Long UId, Page page,Long collid) {
		// TODO Auto-generated method stub
		return this.fgoodsDao.collectList(UId, page , collid);
	}

	@Override
	public Integer delGoodsByByUIdAndGid(Long UId, Long GId,Long collid) {
		// TODO Auto-generated method stub
		return this.fgoodsDao.delGoodsByByUIdAndGid(UId, GId,collid);
	}

	@Override
	public List<CtGoodsCategory> queryAllCateToType(String typeNameToCid) {
		return this.fgoodsDao.queryAllCateToType(typeNameToCid);
	}

	@Override
	public List<CtCouponDetail> getCouponByGid(Long gid) {
		
		return this.fgoodsDao.getCouponByGid(gid);
	}

	@Override
	public List<CtCouponDetail> getCouponByCouponId(Integer couponId) {
		return this.fgoodsDao.getCouponByCouponId(couponId);
	}

	@Override
	public List<ViewGoodsList> callGoodsByPrice(String price1, String price2) {
		return this.fgoodsDao.callGoodsByPrice(price1,price2);
	}

	@Override
	public String getGoodsTypeByGid(Long gid) {
		
		return this.fgoodsDao.getGoodsTypeByGid(gid);
	}

	@Override
	public Long getGoodsExTypeByTypeId(String typeId) {
		return this.fgoodsDao.getGoodsExTypeByTypeId(typeId);
	}

	@Override
	public List<CtGoodsCategory> getCateByEx(Long exType) {
		return this.fgoodsDao.getCateByEx(exType);
	}

	@Override
	public List<CtCoupon> getCouponByGidNew(Long gid) {
		return this.fgoodsDao.getCouponByGidNew(gid);
	}

	@Override
	public List<CtCollectGoods> loadAll(Long UId) {
		// TODO Auto-generated method stub
		return this.fgoodsDao.loadAll(UId);
	}

	@Override
	public List<CtCollect> findAll(Page page) {
		// TODO Auto-generated method stub
		return this.fgoodsDao.findAll(page);
	}

	@Override
	public String save(CtCollect collect) {
		// TODO Auto-generated method stub
		return this.fgoodsDao.save(collect);
	}

	@Override
	public CtCollect getCtCollByCollTitle(String collTitle,String collid) {
		// TODO Auto-generated method stub
		return this.fgoodsDao.getCtCollByCollTitle(collTitle,collid);
	}

	@Override
	public void delCollGoodsByCollId(Long collid) {
		// TODO Auto-generated method stub
		this.fgoodsDao.delCollGoodsByCollId(collid);
	}

	@Override
	public void delete(Long collMid) {
		// TODO Auto-generated method stub
		this.fgoodsDao.delete(collMid);
	}

	@Override
	public CtCollect getCtCollByCollId(Long CollId) {
		// TODO Auto-generated method stub
		return this.fgoodsDao.getCtCollByCollId(CollId);
	}

	@Override
	public String updateColl(CtCollect collect) {
		// TODO Auto-generated method stub
		String res =  this.fgoodsDao.updateColl(collect);
		return res;
	}
	@Override
	public boolean isSmall(Long gId,Integer sn) {
		// TODO Auto-generated method stub
		boolean res =  this.fgoodsDao.isSmall(gId,sn);
		return res;
	}

	@Override
	public List<CtCollect> search(String keyword, Page page) {
		// TODO Auto-generated method stub
		return this.fgoodsDao.search(keyword, page);
	}

	@Override
	public List<CtCollectGoods> loadCollDetailAll(Page page, Long collid) {
		// TODO Auto-generated method stub
		return this.fgoodsDao.loadCollDetailAll(page, collid);
	}

	@Override
	public CtCollectGoods getCtCollGoodsByGId(Long GId, Long collid) {
		// TODO Auto-generated method stub
		return this.fgoodsDao.getCtCollGoodsByGId(GId, collid);
	}

	@Override
	public List<CtCollect> getCtCollByUId(Long UId) {
		return this.fgoodsDao.getCtCollByUId(UId);
	}

	@Override
	public List<CtGoodsCategory> queryAll() {
		// TODO Auto-generated method stub
		return this.fgoodsDao.queryAll();
	}

	@Override
	public  List<CtGoodsCategory> queryAllByPid(Long pid) {
		// TODO Auto-generated method stub
		return this.fgoodsDao.queryAllByPid(pid);
	}

	@Override
	public CtGoodsCategory queryAllByCName(String cname) {
		// TODO Auto-generated method stub
		return this.fgoodsDao.queryAllByCName(cname);
	}

	@Override
	public CtGoodsCategory queryByCName(String cname) {
		// TODO Auto-generated method stub
		return this.fgoodsDao.queryByCName(cname);
	}

	@Override
	public CtGoodsCategory getCNameByCid(Long cid) {
		// TODO Auto-generated method stub
		return this.fgoodsDao.getCNameByCid(cid);
	}

	@Override
	public List<CtNotice> findAllNotices() {
		return this.fgoodsDao.findAllNotices();
	}

	@Override
	public List<CtGoodsCategory> getSanjiFenLei() {
		return this.fgoodsDao.getSanjiFenLei();
	}

	@Override
	public CtGoodsBrand findById(Long id) {
		// TODO Auto-generated method stub
		return this.fgoodsDao.findById(id);
	}

	@Override
	public List<CtGoodsCategory> getSanjiFenLeiDianRong() {
		return this.fgoodsDao.getSanjiFenLeiDianRong();
	}

	@Override
	public Map<Integer, String> getCateFenLeiAll(Long gId) {
		return this.fgoodsDao.getCateFenLeiAll(gId);
	}

	@Override
	public CtCart getcartBycartid(Long cartId) {
		// TODO Auto-generated method stub
		return this.fgoodsDao.getcartBycartid(cartId);
	}

	@Override
	public List<CtGoods> getSubHade() {
		return this.fgoodsDao.getSubHade();
	}

	@Override
	public List<CtGoodsResource> getResultIdByGid(Long gid) {
		return this.fgoodsDao.getResultIdByGid(gid);
	}

	@Override
	public CtResource getResByResId(Long resourceId) {
		return this.fgoodsDao.getResByResId(resourceId);
	}

	@Override
	public List<CtGoodsCategory> getCateForFirst() {
		return this.fgoodsDao.getCateForFirst();
	}

	@Override
	public List<CtGoodsCategory> getCateSer(List<CtGoodsCategory> cateList) {
		return this.fgoodsDao.getCateSer(cateList);
	}

	@Override
	public List<CtGoodsCategory> getCateThree(List<CtGoodsCategory> cateListSer) {
		return this.fgoodsDao.getCateThree(cateListSer);
	}

	@Override
	public List<CtGoodsAttributeRelation> getAttrRelByGid(Long gid) {
		return this.fgoodsDao.getAttrRelByGid(gid);
	}

	@Override
	public CtGoodsAttribute getattrByAttrId(Long attrId) {
		return this.fgoodsDao.getattrByAttrId(attrId);
	}

	@Override
	public List<CtGoodsAttributeRelation> getAttrRelByAttrId(Long attrId,Long gid) {
		return this.fgoodsDao.getAttrRelByAttrId(attrId,gid);
	}

	@Override
	public List<CtGoodsAttribute> getAttrByGTIdOrder(Long gtId) {
		return this.fgoodsDao.getAttrByGTIdOrder(gtId);
	}

	@Override
	public List<CtGoods> getGoodsByGid(Long gid) {
		return this.fgoodsDao.getGoodsByGid(gid);
	}

	@Override
	public List<CtGoods> loadGoods() {
		// TODO Auto-generated method stub
		return this.fgoodsDao.loadGoods();
	}

	@Override
	public List<CtGoods> getGoodsByBrandId(Long bId) {
		return fgoodsDao.getGoodsByBrandId(bId);
	}

	@Override
	public String getCateNameByCateId(Long cId) {
		return fgoodsDao.getCateNameByCateId(cId);
	}

	@Override
	public void addGoodsKey(CtSearchKeyword searchKeyWord) {
		fgoodsDao.addGoodsKey(searchKeyWord);
	}

	@Override
	public CtSearchKeyword findByKeyWord(String kword) {
		return fgoodsDao.findByKeyWord(kword);
	}

	@Override
	public List<CtSearchKeyword> findKeyWordOrderTop4() {
		return fgoodsDao.findKeyWordOrderTop4();
	}

	@Override
	public void GoodsKeyNew(CtSearchKeyword searchKeyWord) {
		fgoodsDao.GoodsKeyNew(searchKeyWord);
	}

	@Override
	public Object merge(Object ctGoods) {
		return fgoodsDao.merge(ctGoods);
	}

	@Override
	public List<CtGoods> findByGSn(String seller_outer_no) {
		return fgoodsDao.findByGSn(seller_outer_no);
	}

	@Override
	public Object findByTypeName(String string, String string2,
			String string3, int i) {
		return fgoodsDao.findByTypeName(string,string2,string3,i);
	}

	@Override
	public CtXwStock findByXWStock() {
		return fgoodsDao.findByXWStock();
	}

	@Override
	public void deleteRanPriceByGid(Long gId) {
		fgoodsDao.deleteRanPriceByGid(gId);
	}

	@Override
	public List<CtResource> findResouresByTopSex() {
		return fgoodsDao.findResouresByTopSex();
	}

	@Override
	public List<CtResource> findResoursByPage(Page page) {
		return fgoodsDao.findResoursByPage(page);
	}

	@Override
	public CtReplace findReplaceByKey(String key) {
		return fgoodsDao.findReplaceByKey(key);
	}

	@Override
	public List<CtAd> findAdByIsUpAndType(int i, String str) {
		return fgoodsDao.findAdByIsUpAndType(i, str);
	}
	
}

package org.ctonline.manager.user;

import java.util.List;

import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.user.CtUserAddress;
import org.ctonline.util.Page;

public interface CtUserAddressManager {
	
	public String save(CtUserAddress address);//保存
	public CtUserAddress findById(Long id);//根据id查找
	public CtUserAddress findByNameandAddress(String name,String address,CtUser cu);
	public List<CtUserAddress> loadAll(Page page,CtUser cu);//分页找出所有
	public int delete(Long id);//删除一条记录
	public Long totalCount(String str);//统计条数
	public String update(CtUserAddress address);//更新
	public List<CtUserAddress> findAll(String keyword,Page page);//根据条件检索
	//根据id查找区域
	public List<CtRegion> queryById(Long id);
	//把地址设为默认收货地址
	public String setAsDeafult(Long Aid,CtUser cu);
}

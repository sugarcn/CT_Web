package org.ctonline.manager.user;

import java.util.List;

import org.ctonline.po.basic.CtFilter;
import org.ctonline.po.basic.CtNotice;
import org.ctonline.po.basic.CtSuggestion;
import org.ctonline.po.basic.CtTemp;
import org.ctonline.po.user.CtContact;
import org.ctonline.po.user.CtDraw;
import org.ctonline.po.user.CtDrawDetail;
import org.ctonline.po.user.CtDrawPacket;
import org.ctonline.po.user.CtExtension;
import org.ctonline.po.user.CtHongbaoTemp;
import org.ctonline.po.user.CtLoginFailed;
import org.ctonline.po.user.CtSms;
import org.ctonline.po.user.CtUesrClient;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.user.CtHongbao;
import org.ctonline.po.user.CtUserAddress;
import org.ctonline.util.Page;

public interface CtUserManager {
	//通过用户名获取用户
	public CtUser getCtUserByUUserid(String UUsername);
	//保存短信验证码
	public Long saveCtSms(CtSms ctSms);
	//通过手机号查找用户
	public CtUser getCtUserByUMb(String UMb);
	//通过手机号查询短信验证码
	public CtSms getCtSmsByUMb(String UMb);
	//保存用户
	public Long save(CtUser ctUser);
	//通过用户名或者手机号查询用户
	public CtUser getCtUserByUUnameOrUMb(String arg0);
	//更新用户
	public String update(CtUser ctUser);
	//验证用户的登录
	public CtUser affirm(String uname,String password) throws Exception;
	//修改用户
	public String editCtuser(CtUser ctUser);
	//用户搜索
	public  List<CtUser> findAll(String keyword);
	//根据UGId搜索
	public List<?>  selectAll(Long UGId);
	//查询所有用户
	public List<CtUser> loadAll(Page page);
	//根据UEmail获取一个用户
	public CtUser getCtUserByUEmail(String UEmail);
	//通过UUserid和Ucode查询一个用户
	public CtUser getCtuserByUUseridAndUCode(String UUserid,String UCode);
	//通过邮件地址和密码验证登录
	public CtUser affimByEmailAndPwd(String email,String password)throws Exception;
	//绑定邮箱的验证
	public CtUser getCtUserByUcode(String UCode);
	//根据输入的内容查找一个用户
	public CtUser getCtuserByUname(String name);
	//根据输入的密码查找一个用户
	public CtUser getCtuserByPwd(String name,String pwd)throws Exception;
	public List<CtNotice> findAllNotices();
	public String addadvice(CtSuggestion advice);
	public String savecontact(CtContact contcat);
	public int getIpcount(String IP,String date);
	public CtUser getUserByUId(Long uId);
	public CtUser findAliUserId(String user_id);
	public CtUser findQQUserId(String openID);
	public CtUser findWeiBoUid(String uid);
	public CtUser findWeiXinOpenId(String openid);
	public CtUser findExTeById(String _userId);
	public void updateExTen(CtExtension extension);
	public List<CtExtension> findExTenByUserId(Long uId);
	public void saveHongBao(CtHongbao hongbao);
	public CtHongbao getHongBaoByOpenid(String openid,String desc);//根据openid获取红包记录
	public Long getHongBaototalByDesc(String desc);//根据desc获取红包条数
	public void saveHongBaoTemp(CtHongbaoTemp hongbaot);
	public CtHongbaoTemp getHongBaoTempByOpenidDate(String openid,String date);//根据openid及日期获取记录
	
	public List<CtTemp> getTempList();//获取所有未发红包且已关联用户
	public CtTemp getTemp(String sn,String openid,String tc,String gx);//根据openid及日期获取记录
	
	public String updateTemp(String tid);//根据openid及日期获取记录
	public CtTemp getTempC(String openid,String tZ);//根据openid及日期获取记录
	public List<CtFilter> getFilter(String userName);
	public List<CtDrawPacket> findPacketsByNowHong();
	public void updateDrawDetail(CtDrawDetail drawDetail);
	public CtDrawDetail findDrawDetailByUid(Long uId);
	public void updatePacket(CtDrawPacket packet);
	public CtDraw findDrawByUid(Long uId);
	public void updateDraw(CtDraw draw);
	public List<CtDrawDetail> findDrawDetailListTop20();
	public int findAllHongCount();
	public List<CtDrawDetail> findMyDrawInfo(Long uId);
	public CtDrawDetail findDrawDetailByDsn(String content);
	public CtDrawDetail findDrawByOpenId(String fromUserName);
	public Boolean canLoginByMac(String macAddress);
	public void saveLoginFailed(CtLoginFailed loginFailed);
	public Boolean getPassErrorSumByName(String uname);
	public List<CtUserAddress> findAllAddressByUid(Long uid, Page page);
	public void updateCusDefultByAid(Long aid, Long uid);
	public List<CtUesrClient> findCusMent(Long uId, Page page);
	
	
}

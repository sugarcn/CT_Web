package org.ctonline.manager.user.impl;

import java.util.List;

import org.ctonline.po.user.CtAddTicket;
import org.ctonline.po.user.CtUser;
import org.ctonline.util.Logg;
import org.ctonline.util.Page;
import org.ctonline.dao.user.CtAddTicketDAO;

public class CtAddTicketManagerImpl implements org.ctonline.manager.user.CtAddTicketManager {

	private CtAddTicketDAO addTicketDao;

	public CtAddTicketDAO getAddTicketDao() {
		return addTicketDao;
	}

	public void setAddTicketDao(CtAddTicketDAO addTicketDao) {
		this.addTicketDao = addTicketDao;
	}

	@Override
	public List<CtAddTicket> loadAll(Page page,CtUser cu) {
		// TODO Auto-generated method stub
		return this.addTicketDao.loadAll(page, cu);
	}

	@Override
	public CtAddTicket findById(Long id) {
		// TODO Auto-generated method stub
		return this.addTicketDao.findById(id);
	}

	@Override
	public List<CtAddTicket> loadOne(Long id) {
		// TODO Auto-generated method stub
		return this.addTicketDao.loadOne(id);
	}

	@Logg(operationType="update", operationName="更新增票信息")
	@Override
	public String update(CtAddTicket addTicket) {
		// TODO Auto-generated method stub
		return this.addTicketDao.update(addTicket);
	}

	@Logg(operationType="save", operationName="新增增票信息")
	@Override
	public String save(CtAddTicket addTicket) {
		// TODO Auto-generated method stub
		return this.addTicketDao.save(addTicket);
	}

	@Override
	public CtAddTicket getTicketByUId(Long UId) {
		// TODO Auto-generated method stub
		return this.addTicketDao.getTicketByUId(UId);
	}

	@Logg(operationType="delete", operationName="删除增票信息")
	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		return this.addTicketDao.delete(id);
	}

	@Override
	public CtAddTicket isExists(Long UId) {
		// TODO Auto-generated method stub
		return this.addTicketDao.isExists(UId);
	}

	@Logg(operationType="update", operationName="设置增票默认")
	@Override
	public void setTehDeafult(Long Tid, CtUser cu) {
		this.addTicketDao.setTehDeafult(Tid, cu);
		
	}

}

package org.ctonline.manager.user.impl;

import java.util.List;

import org.ctonline.dao.basic.CtRegionDAO;
import org.ctonline.dao.user.CtUserAddressDAO;
import org.ctonline.manager.user.CtUserAddressManager;
import org.ctonline.po.basic.CtRegion;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.user.CtUserAddress;
import org.ctonline.util.Logg;
import org.ctonline.util.Page;

public class CtUserAddressManagerImpl implements  CtUserAddressManager {
	
	public CtUserAddressDAO getAddressDao() {
		return addressDao;
	}

	public void setAddressDao(CtUserAddressDAO addressDao) {
		this.addressDao = addressDao;
	}

	private CtUserAddressDAO addressDao;
	private CtRegionDAO regionDao;
	
	public CtRegionDAO getRegionDao() {
		return regionDao;
	}

	public void setRegionDao(CtRegionDAO regionDao) {
		this.regionDao = regionDao;
	}

	@Logg(operationType="save", operationName="新增收货地址")
	@Override
	public String save(CtUserAddress address) {
		// TODO Auto-generated method stub
		return addressDao.save(address);
	}

	@Override
	public CtUserAddress findById(Long id) {
		// TODO Auto-generated method stub
		return addressDao.findById(id);
	}

	@Override
	public List<CtUserAddress> loadAll(Page page,CtUser cu) {
		// TODO Auto-generated method stub
		return addressDao.loadAll(page,cu);
	}

	@Logg(operationType="delete", operationName="删除一个收货地址")
	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		return addressDao.delete(id);
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		return addressDao.totalCount(str);
	}

	@Logg(operationType="update", operationName="更新收货地址")
	@Override
	public String update(CtUserAddress address) {
		// TODO Auto-generated method stub
		return addressDao.update(address);
	}

	@Override
	public List<CtUserAddress> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		return addressDao.findAll(keyword, page);
	}

	@Override
	public List<CtRegion> queryById(Long id){
		
		return regionDao.queryById(id);
	}

	@Override
	public String setAsDeafult(Long Aid, CtUser cu) {
		// TODO Auto-generated method stub
		return this.addressDao.setAsDeafult(Aid, cu);
	}

	@Override
	public CtUserAddress findByNameandAddress(String name, String address,CtUser cu) {
		// TODO Auto-generated method stub
		return addressDao.findByNameandAddress(name, address,cu);
	}


}

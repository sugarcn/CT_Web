package org.ctonline.manager.user.impl;

import java.util.List;

import org.ctonline.dao.user.CtUserDao;
import org.ctonline.manager.user.CtUserManager;
import org.ctonline.po.basic.CtFilter;
import org.ctonline.po.basic.CtNotice;
import org.ctonline.po.basic.CtSuggestion;
import org.ctonline.po.basic.CtTemp;
import org.ctonline.po.user.CtContact;
import org.ctonline.po.user.CtDraw;
import org.ctonline.po.user.CtDrawDetail;
import org.ctonline.po.user.CtDrawPacket;
import org.ctonline.po.user.CtExtension;
import org.ctonline.po.user.CtHongbaoTemp;
import org.ctonline.po.user.CtLoginFailed;
import org.ctonline.po.user.CtSms;
import org.ctonline.po.user.CtUesrClient;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.user.CtHongbao;
import org.ctonline.po.user.CtUserAddress;
import org.ctonline.util.Logg;
import org.ctonline.util.Page;

public class CtUserManagerImpl implements CtUserManager {
	private CtUserDao userDao;

	public CtUserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(CtUserDao userDao) {
		this.userDao = userDao;
	}

	@Override
	public CtUser getCtUserByUUserid(String UUserid) {
		// TODO Auto-generated method stub
		return this.userDao.getCtUserByUUserid(UUserid);
	}

	@Override
	public Long saveCtSms(CtSms ctSms) {
		// TODO Auto-generated method stub
		return this.userDao.saveCtSms(ctSms);
	}

	@Override
	public CtUser getCtUserByUMb(String UMb) {
		// TODO Auto-generated method stub
		return this.userDao.getCtUserByUMb(UMb);
	}

	@Override
	public CtSms getCtSmsByUMb(String UMb) {
		// TODO Auto-generated method stub
		return this.userDao.getCtSmsByUMb(UMb);
	}

	@Logg(operationType="save", operationName="新增用户注册信息")
	@Override
	public Long save(CtUser ctUser) {
		// TODO Auto-generated method stub
		return this.userDao.save(ctUser);
	}

	@Override
	public CtUser getCtUserByUUnameOrUMb(String arg0) {
		// TODO Auto-generated method stub
		return this.userDao.getCtUserByUUnameOrUMb(arg0);
	}

	@Logg(operationType="update", operationName="更新用户信息")
	@Override
	public String update(CtUser ctUser) {
		// TODO Auto-generated method stub
		return this.userDao.update(ctUser);
	}

	@Override
	public CtUser affirm(String uname, String password) throws Exception {
		// TODO Auto-generated method stub
		return this.userDao.affirm(uname, password);
	}
	
	@Override
		public  List<CtUser> findAll(String keyword){
		
		return this.userDao.findAll(keyword);
				
	}
	
	@Override
	public List<?>  selectAll(Long UGId){
		
		
		return this.userDao.selectAll(UGId);
	}

	@Override
	public String editCtuser(CtUser ctUser) {
		// TODO Auto-generated method stub
		return this.userDao.editCtuser(ctUser);
	}

	@Override
	public List<CtUser> loadAll(Page page) {
		// TODO Auto-generated method stub
		return this.userDao.loadAll(page);
	}

	@Override
	public CtUser getCtUserByUEmail(String UEmail) {
		// TODO Auto-generated method stub
		return this.userDao.getCtUserByUEmail(UEmail);
	}

	@Override
	public CtUser getCtuserByUUseridAndUCode(String UUserid, String UCode) {
		// TODO Auto-generated method stub
		return this.userDao.getCtuserByUUseridAndUCode(UUserid, UCode);
	}

	@Override
	public CtUser affimByEmailAndPwd(String email, String password)
			throws Exception {
		// TODO Auto-generated method stub
		return this.userDao.affimByEmailAndPwd(email, password);
	}

	@Override
	public CtUser getCtUserByUcode(String UCode) {
		// TODO Auto-generated method stub
		return this.userDao.getCtUserByUcode(UCode);
	}

	@Override
	public CtUser getCtuserByUname(String name) {
		// TODO Auto-generated method stub
		return this.userDao.getCtuserByUname(name);
	}

	@Override
	public CtUser getCtuserByPwd(String name,String pwd) throws Exception {
		// TODO Auto-generated method stub
		return this.userDao.getCtuserByPwd(name,pwd);
	}

	@Override
	public List<CtNotice> findAllNotices() {
		return this.userDao.findAllNotices();
	}

	@Logg(operationType="save", operationName="意见反馈")
	@Override
	public String addadvice(CtSuggestion advice) {
		// TODO Auto-generated method stub
		return this.userDao.saveadvice(advice);
	}

	@Override
	public String savecontact(CtContact contcat) {
		// TODO Auto-generated method stub
		return this.userDao.savecontact(contcat);
	}

	@Override
	public int getIpcount(String Ip,String date) {
		// TODO Auto-generated method stub
		return this.userDao.getIpcount(Ip,date);
	}

	@Override
	public CtUser getUserByUId(Long uId) {
		return this.userDao.getUserByUId(uId);
	}

	@Override
	public CtUser findAliUserId(String user_id) {
		return this.userDao.findAliUserId(user_id);
	}

	@Override
	public CtUser findQQUserId(String openID) {
		return this.userDao.findQQUserId(openID);
	}

	@Override
	public CtUser findWeiBoUid(String uid) {
		return this.userDao.findWeiBoUid(uid);
	}

	@Override
	public CtUser findWeiXinOpenId(String openid) {
		return this.userDao.findWeiXinOpenId(openid);
	}

	@Override
	public CtUser findExTeById(String _userId) {
		return userDao.findExTeById(_userId);
	}

	@Override
	public void updateExTen(CtExtension extension) {
		userDao.updateExTen(extension);
	}

	@Override
	public List<CtExtension> findExTenByUserId(Long uId) {
		return userDao.findExTenByUserId(uId);
	}

	@Override
	public void saveHongBao(CtHongbao hongbao) {
		userDao.saveHongBao(hongbao);
	}
	
	@Override
	public CtHongbao getHongBaoByOpenid(String openid,String desc) {
		return this.userDao.getHongBaoByOpenid(openid,desc);
	}

	@Override
	public Long getHongBaototalByDesc(String desc) {
		// TODO Auto-generated method stub
		return this.userDao.getHongBaototalByDesc(desc);
	}

	@Override
	public void saveHongBaoTemp(CtHongbaoTemp hongbaot) {
		userDao.saveHongBaoTemp(hongbaot);
	}
	@Override
	public CtHongbaoTemp getHongBaoTempByOpenidDate(String openid,String date) {
		return this.userDao.getHongBaoTempByOpenidDate(openid,date);
	}
	
	@Override
	public CtTemp getTemp(String sn,String openid,String tc,String gx) {
		return this.userDao.getTemp(sn,openid,tc,gx);
	}	
	
	
	@Override
	public String updateTemp(String tid) {
		return this.userDao.updateTemp(tid);
		
	}
	
	@Override
	public List<CtTemp> getTempList() {
		return this.userDao.getTempList();
	}
	
	@Override
	public CtTemp getTempC(String openid,String tZ) {
		return this.userDao.getTempC(openid,tZ);
	}

	@Override
	public List<CtFilter> getFilter(String userName) {
		return userDao.getFilter(userName);
	}

	@Override
	public List<CtDrawPacket> findPacketsByNowHong() {
		return userDao.findPacketsByNowHong();
	}

	@Override
	public void updateDrawDetail(CtDrawDetail drawDetail) {
		userDao.updateDrawDetail(drawDetail);
	}

	@Override
	public CtDrawDetail findDrawDetailByUid(Long uId) {
		return userDao.findDrawDetailByUid(uId);
	}

	@Override
	public void updatePacket(CtDrawPacket packet) {
		userDao.updatePacket(packet);
	}

	@Override
	public CtDraw findDrawByUid(Long uId) {
		return userDao.findDrawByUid(uId);
	}

	@Override
	public void updateDraw(CtDraw draw) {
		userDao.updateDraw(draw);
	}

	@Override
	public List<CtDrawDetail> findDrawDetailListTop20() {
		return userDao.findDrawDetailListTop20();
	}

	@Override
	public int findAllHongCount() {
		return userDao.findAllHongCount();
	}

	@Override
	public List<CtDrawDetail> findMyDrawInfo(Long uId) {
		// TODO Auto-generated method stub
		return userDao.findMyDrawInfo(uId);
	}

	@Override
	public CtDrawDetail findDrawDetailByDsn(String content) {
		return userDao.findDrawDetailByDsn(content);
	}

	@Override
	public CtDrawDetail findDrawByOpenId(String fromUserName) {
		return userDao.findDrawByOpenId(fromUserName);
	}

	@Override
	public Boolean canLoginByMac(String macAddress) {
		List<CtLoginFailed> loginFailedList = userDao.findLoginFailedByMac(macAddress);
		if(loginFailedList != null && loginFailedList.size() >= 5){
			return false;
		} else {
			return true;
		}
	}

	@Logg(operationType="save", operationName="记录登陆失败")
	@Override
	public void saveLoginFailed(CtLoginFailed loginFailed) {
		userDao.saveLoginFailed(loginFailed);
	}

	@Override
	public Boolean getPassErrorSumByName(String uname) {
		List<CtLoginFailed> loginFailedList = userDao.findLoginFailedByUname(uname);
		if(loginFailedList != null && loginFailedList.size() >= 10){
			return false;
		} else {
			return true;
		}
	}

	@Override
	public List<CtUserAddress> findAllAddressByUid(Long uid, Page page) {
		return userDao.findAllAddressByUid(uid, page);
	}

	@Logg(operationType="update", operationName="设置客服下单地址")
	@Override
	public void updateCusDefultByAid(Long aid, Long uid) {
		// TODO Auto-generated method stub
		userDao.updateCusDefultByAid(aid, uid);
	}

	@Override
	public List<CtUesrClient> findCusMent(Long uId, Page page) {
		
		return userDao.findCusMent(uId, page);
	}
}

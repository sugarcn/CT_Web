package org.ctonline.manager.help.impl;

import java.util.List;



import org.ctonline.dao.help.CtHelpTypeDAO;
import org.ctonline.manager.help.CtHelpTypeManager;
import org.ctonline.po.help.CtHelpType;
import org.ctonline.util.Page;

public class CtHelpTypeManagerImpl implements CtHelpTypeManager {
	private CtHelpTypeDAO helpTypeDao;

	public CtHelpTypeDAO getHelpTypeDao() {
		return helpTypeDao;
	}

	public void setHelpTypeDao(CtHelpTypeDAO helpTypeDao) {
		this.helpTypeDao = helpTypeDao;
	}

	@Override
	public String save(CtHelpType helpType) {
		// TODO Auto-generated method stub
		return helpTypeDao.save(helpType);
	}

	@Override
	public CtHelpType findById(int id) {
		// TODO Auto-generated method stub
		return helpTypeDao.findById(id);
	}

	@Override
	public List<CtHelpType> loadAll(Page page) {
		// TODO Auto-generated method stub
		return helpTypeDao.loadAll(page);
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return helpTypeDao.delete(id);
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		return helpTypeDao.totalCount(str);
	}

	@Override
	public String update(CtHelpType helpType) {
		// TODO Auto-generated method stub
		return this.helpTypeDao.update(helpType);
	}

	@Override
	public List<CtHelpType> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		return helpTypeDao.findAll(keyword, page);
	}

	@Override
	public List<CtHelpType> queryType() {
		// TODO Auto-generated method stub
		return helpTypeDao.queryType();
	}

}

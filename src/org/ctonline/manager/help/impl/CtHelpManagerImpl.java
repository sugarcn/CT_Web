package org.ctonline.manager.help.impl;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.ctonline.dao.help.CtHelpDAO;
import org.ctonline.manager.help.CtHelpManager;
import org.ctonline.po.basic.CtNotice;
import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.po.goods.CtGoodsImg;
import org.ctonline.po.help.CtHelp;
import org.ctonline.po.help.CtHelpType;
import org.ctonline.util.Page;

public class CtHelpManagerImpl implements CtHelpManager{

	@Override
	public List<CtHelp> loadOne(int id) {
		// TODO Auto-generated method stub
		return this.helpDAO.loadOne(id);
	}

	private CtHelpDAO helpDAO;


	public CtHelpDAO getHelpDAO() {
		return helpDAO;
	}

	public void setHelpDAO(CtHelpDAO helpDAO) {
		this.helpDAO = helpDAO;
	}

	@Override
	public CtHelp findById(int id) {
		// TODO Auto-generated method stub
		return this.helpDAO.findById(id);
	}

	@Override
	public String save(CtHelp help) {
		// TODO Auto-generated method stub
		return this.helpDAO.save(help);
	}

	@Override
	public Long totalCount(String str) {
		// TODO Auto-generated method stub
		return this.helpDAO.totalCount(str);
	}


	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return this.helpDAO.delete(id);
	}

	@Override
	public CtHelp getHelpByHId(int Uid) {
		// TODO Auto-generated method stub
		return this.helpDAO.getHelpByHId(Uid);
	}

	@Override
	public List<CtHelp> loadAll(Page page) {
		// TODO Auto-generated method stub
		return this.helpDAO.loadAll(page);
	}

	@Override
	public String update(CtHelp help) {
		// TODO Auto-generated method stub
		return this.helpDAO.update(help);
	}

	@Override
	public List<CtHelp> findAll(String keyword, Page page) {
		// TODO Auto-generated method stub
		return this.helpDAO.findAll(keyword, page);
	}

	@Override
	public List<CtHelpType> queryType() {
		// TODO Auto-generated method stub
		return this.helpDAO.queryType();
	}

	@Override
	public CtHelp findByTitle(String title) {
		// TODO Auto-generated method stub
		return this.helpDAO.findByTitle(title);
	}

	@Override
	public List<CtNotice> findAllNotices(int typeId) {
		return this.helpDAO.findAllNotices(typeId);
	}

	@Override
	public List<CtGoods> getDianZuGoods() {
		List<CtGoods> list = this.helpDAO.getDianZuGoods();
		List<CtGoods> listNew = new ArrayList<CtGoods>();
		if(list!=null && list.size() > 13){
			for (int i = 0; i < 13; i++) {
				listNew.add(list.get(i));
			}
		} else {
			listNew = list;
		}
		return listNew;
	}

	@Override
	public List<CtGoods> getDianRongGoods() {
		List<CtGoods> list = this.helpDAO.getDianRongGoods();
		List<CtGoods> listNew = new ArrayList<CtGoods>();
		if(list!=null && list.size() > 13){
			for (int i = 0; i < 13; i++) {
				listNew.add(list.get(i));
			}
		} else {
			listNew = list;
		}
		return listNew;
	}

	@Override
	public List<CtGoodsImg> getImg(Long gId) {
		return this.helpDAO.getImg(gId);
	}

	@Override
	public Map<Long,String> getErjiName() {
		return this.helpDAO.getErjiName();
	}

	@Override
	public List<CtGoodsCategory> getCateListByCid(Long cid) {
		return this.helpDAO.getCateListByCid(cid);
	}
}

package org.ctonline.manager.help;


import java.util.List;
import java.util.Map;

import org.ctonline.po.basic.CtNotice;
import org.ctonline.po.basic.CtResourceType;
import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.po.goods.CtGoodsImg;
import org.ctonline.po.help.CtHelp;
import org.ctonline.po.help.CtHelpType;
import org.ctonline.util.Page;

public interface CtHelpManager {
	public CtHelp findById(int id);
	public List<CtHelp> loadAll(Page page);
	public String save(CtHelp help);
	public int delete(int m);
	public Long totalCount(String str);
	public String update(CtHelp help);
	public List<CtHelp> loadOne(int id);
	public CtHelp getHelpByHId(int Uid);
	public List<CtHelp> findAll(String keyword,Page page);
	public List<CtHelpType> queryType();
	public CtHelp findByTitle(String title);
	public List<CtNotice> findAllNotices(int typeId);
	public List<CtGoods> getDianZuGoods();
	public List<CtGoods> getDianRongGoods();
	public List<CtGoodsImg> getImg(Long gId);
	public Map<Long,String> getErjiName();
	public List<CtGoodsCategory> getCateListByCid(Long cid);
}

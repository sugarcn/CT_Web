package org.ctonline.manager.help;

import java.util.List;

import org.ctonline.po.basic.CtResourceType;
import org.ctonline.po.help.CtHelpType;
import org.ctonline.util.Page;

public interface CtHelpTypeManager {
	public String save(CtHelpType helpType);
	public CtHelpType findById(int id);
	public List<CtHelpType> loadAll(Page page);
	public int delete(int m);
	public Long totalCount(String str);
	public String update(CtHelpType helpType);
	public List<CtHelpType> findAll(String keyword,Page page);
	public List<CtHelpType> queryType();
}

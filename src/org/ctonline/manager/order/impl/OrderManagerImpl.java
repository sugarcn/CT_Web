package org.ctonline.manager.order.impl;

import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.input.BOMInputStream;
import org.ctonline.dao.goods.CtBomDao;
import org.ctonline.dao.order.OrderDAO;
import org.ctonline.dto.order.OrderDTO;
import org.ctonline.manager.order.OrderManager;
import org.ctonline.po.basic.CtCashCoupon;
import org.ctonline.po.basic.CtCouponDetail;
import org.ctonline.po.basic.CtPaymentWx;
import org.ctonline.po.basic.CtTransaction;
import org.ctonline.po.goods.CtBom;
import org.ctonline.po.goods.CtBomGoods;
import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.goods.CtGoodsBrand;
import org.ctonline.po.order.CtCart;
import org.ctonline.po.order.CtComplain;
import org.ctonline.po.order.CtEvaluation;
import org.ctonline.po.order.CtExpress;
import org.ctonline.po.order.CtOrderGoods;
import org.ctonline.po.order.CtOrderInfo;
import org.ctonline.po.order.CtRefund;
import org.ctonline.po.order.CtShop;
import org.ctonline.po.user.CtUser;
import org.ctonline.po.user.CtUserAddress;
import org.ctonline.po.views.ViewCartList;
import org.ctonline.po.views.ViewOrderBom;
import org.ctonline.po.views.ViewOrderCheck;
import org.ctonline.po.views.ViewUserAddress;
import org.ctonline.util.Logg;
import org.ctonline.util.Page;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class OrderManagerImpl implements OrderManager{

	private OrderDAO orderDao;
	
	private CtBomDao bomDao;
	

	public SessionFactory getSessionFactory() {
		return bomDao.getSessionFactory();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		bomDao.setSessionFactory(sessionFactory);
	}

	public Session begin() {
		return bomDao.begin();
	}

	public Session getSession() {
		return bomDao.getSession();
	}

	public List<CtBom> loadAll(Page page) {
		return bomDao.loadAll(page);
	}

	public Session getCurrentSession() {
		return bomDao.getCurrentSession();
	}

	public List<CtBom> loadAll() {
		return bomDao.loadAll();
	}

	public void clossSession() {
		bomDao.clossSession();
	}

	public void commit() {
		bomDao.commit();
	}

	public Long totalCount(String str) {
		return bomDao.totalCount(str);
	}

	public String save(CtBom ctBom) {
		return bomDao.save(ctBom);
	}

	public CtBom getCtBomByBomId(Integer BomId, Long uid) {
		return bomDao.getCtBomByBomId(BomId, uid);
	}

	public void update(CtBom ctBom) {
		bomDao.update(ctBom);
	}

	public void delete(Integer bomMid) {
		bomDao.delete(bomMid);
	}

	public List<CtBom> findAll(String keyword, Page page) {
		return bomDao.findAll(keyword, page);
	}

	public List<CtBomGoods> loadBomDetailAll(Page page, Integer bomId) {
		return bomDao.loadBomDetailAll(page, bomId);
	}

	public void delGoodsOfBomDetail(Long bomGoodsId) {
		bomDao.delGoodsOfBomDetail(bomGoodsId);
	}

	public List<CtBomGoods> findBomDetailAll(String keyword, Page page,
			Integer bomId) {
		return bomDao.findBomDetailAll(keyword, page, bomId);
	}

	public CtBomGoods getCtBomGoodsBybomGoodsId(Long bomGoodsId) {
		return bomDao.getCtBomGoodsBybomGoodsId(bomGoodsId);
	}

	public String saveCtBomGoods(CtBomGoods cbg) {
		return bomDao.saveCtBomGoods(cbg);
	}

	public void updateCtBomGoods(CtBomGoods cbg) {
		bomDao.updateCtBomGoods(cbg);
	}

	public CtBomGoods getCtBomGoodsByGId(Long GId, Integer bomId) {
		return bomDao.getCtBomGoodsByGId(GId, bomId);
	}

	public CtBom getCtBomByBomTitle(String bomTitle,String bomId) {
		return bomDao.getCtBomByBomTitle(bomTitle,bomId);
	}

	public List<CtBomGoods> getCtBomGoodsByBomId(Integer bomId) {
		return bomDao.getCtBomGoodsByBomId(bomId);
	}

	public List<CtBom> loadAllHot(Page page) {
		return bomDao.loadAllHot(page);
	}

	public List<CtBom> loadAllHotLogin(Page page) {
		return bomDao.loadAllHotLogin(page);
	}

	public List<CtBom> getCtBomsByUId(Long UId) {
		return bomDao.getCtBomsByUId(UId);
	}

	public List<CtBom> findByKeyWord(String keyword, Page page) {
		return bomDao.findByKeyWord(keyword, page);
	}

	public List<CtBom> findByKeyWordLogin(String keyword, Page page) {
		return bomDao.findByKeyWordLogin(keyword, page);
	}

	public void delBomGoodsByBomId(Integer bomId) {
		bomDao.delBomGoodsByBomId(bomId);
	}

	public List<CtBomGoods> getCtBomGoodsByGIdAndBomId(Long GId, Integer bomId) {
		return bomDao.getCtBomGoodsByGIdAndBomId(GId, bomId);
	}

	public OrderDAO getOrderDao() {
		return orderDao;
	}

	public void setOrderDao(OrderDAO orderDao) {
		this.orderDao = orderDao;
	}

	@Override
	public List<ViewCartList> getCartByUId(Long UId) {
		// TODO Auto-generated method stub
		return orderDao.getCartByUId(UId);
	}

	@Logg(operationType="save", operationName="新增购物车信息")
	@Override
	public String addCart(CtCart cart) {
		// TODO Auto-generated method stub
		return orderDao.addCart(cart);
	}

	@Override
	public List<CtGoods> getGoodsByGId(Long gid) {
		// TODO Auto-generated method stub
		return orderDao.getGoodsByGId(gid);
	}

	@Override
	public List<ViewUserAddress> getUserAddress(Long uid) {
		// TODO Auto-generated method stub
		return orderDao.getUserAddress(uid);
	}

	@Logg(operationType="update", operationName="更新购物车信息")
	@Override
	public String updateCart(CtCart cart) {
		// TODO Auto-generated method stub
		return orderDao.updateCart(cart);
	}

	@Override
	public CtCart getCartByUIdGId(Long uid, Long gid) {
		// TODO Auto-generated method stub
		return orderDao.getCartByUIdGId(uid, gid);
	}

	@Override
	public List<ViewOrderCheck> getOrderCheck(Long uid, Long gid, String queryType) {
		// TODO Auto-generated method stub
		return orderDao.getOrderCheck(uid, gid, queryType);
	}

	@Override
	public CtUserAddress getAddress(Long addid) {
		// TODO Auto-generated method stub
		return orderDao.getAddress(addid);
	}

	@Logg(operationType="save", operationName="保存订单信息")
	@Override
	public String saveOrderInfo(CtOrderInfo odinfo) {
		// TODO Auto-generated method stub
		return orderDao.saveOrderInfo(odinfo);
	}

	@Logg(operationType="save", operationName="保存订单商品数据")
	@Override
	public String saveOrderGoods(CtOrderGoods odgoods) {
		// TODO Auto-generated method stub
		return orderDao.saveOrderGoods(odgoods);
	}

	@Logg(operationType="delete", operationName="删除购物车商品")
	@Override
	public String delCartGoods(Long uid, Long gid) {
		// TODO Auto-generated method stub
		return orderDao.delCartGoods(uid, gid);
	}

	@Logg(operationType="delete", operationName="清空用户购物车商品")
	@Override
	public String delCart(Long uid) {
		// TODO Auto-generated method stub
		return orderDao.delCart(uid);
	}

	@Override
	public List<CtOrderInfo> getOrderListByUId(Long uid,Page page) {
		// TODO Auto-generated method stub
		return orderDao.getOrderListByUId(uid,page);
	}

	@Override
	public List<?> getOrderGoods(Long uid) {
		// TODO Auto-generated method stub
		return orderDao.getOrderGoods(uid);
	}

	@Override
	public List<CtOrderInfo> getOrderListByTime(Long uid, Page page, String time, String status) {
		// TODO Auto-generated method stub
		return orderDao.getOrderListByTime(uid, page, time, status);
	}

	@Override
	public List<CtOrderInfo> getOrderListByStatus(Long uid, Page page,String time,
			String status) {
		// TODO Auto-generated method stub
		return orderDao.getOrderListByStatus(uid, page,time, status);
	}

	@Logg(operationType="update", operationName="更新订单状态")
	@Override
	public void upChangeStatus(Long orderId, String status) {
		// TODO Auto-generated method stub
		orderDao.upChangeStatus(orderId, status);
	}

	@Override
	public List<CtBomGoods> getGoodsByBom(Integer bomid) {
		// TODO Auto-generated method stub
		return orderDao.getGoodsByBom(bomid);
	}

	@Override
	public List<ViewOrderBom> getCheckByBom(Integer bomid) {
		// TODO Auto-generated method stub
		return orderDao.getCheckByBom(bomid);
	}

	@Override
	public List<CtCart> getCartByUIdGIdPack(Long uid, String gid, String pack) {
		// TODO Auto-generated method stub
		return orderDao.getCartByUIdGIdPack(uid,gid,pack);
	}

	@Override
	public String updateCartNum(CtCart cart) {
		// TODO Auto-generated method stub
		return orderDao.updateCartNum(cart);
	}

	@Logg(operationType="update", operationName="更新用户使用优惠券信息")
	@Override
	public String updateCoupon(CtCouponDetail ccd) {
		return orderDao.updateCoupon(ccd);
	}
	
	@Override
	public CtCart getCartByCartId(Long cartId){//根据CartID查出当前购物车值
		return orderDao.getCartByCartId(cartId);
	}
//	@Override
//	public CtCart getCartByCartIdandpack(Long cartId,Long bomId,String pack){//根据CartID查出当前购物车值
//		return orderDao.getCartByCartIdandpack(cartId,bomId,pack);
//	}

	@Override
	public List<CtGoods> getGoodsBycartId(String string) {
		return orderDao.getGoodsBycartId(string);
	}

	@Override
	public CtOrderInfo getOrderByOrderSn(String orderSn,Long uid) {
		return orderDao.getOrderByOrderSn(orderSn,uid);
	}

	@Logg(operationType="update", operationName="更新订单信息-确认收货")
	@Override
	public void updateOrderStartBySn(String orderSn) {
		orderDao.updateOrderStartBySn(orderSn);
	}

	@Override
	public CtOrderInfo getOrderByOrderId(Long infoID) {
		return orderDao.getOrderByOrderId(infoID);
	}

	@Override
	public List<CtOrderInfo> getIsDis() {
		return orderDao.getIsDis();
	}

	@Override
	public int updateStartByOrderId(String orderId) {
		return orderDao.updateStartByOrderId(orderId);
	}

	@Override
	public CtExpress getExpByExId(Byte exId) {
		return orderDao.getExpByExId(exId);
	}

	@Override
	public List<CtOrderGoods> getOrderGoodsByOrderId(Long orderId) {
		return orderDao.getOrderGoodsByOrderId(orderId);
	}

	@Override
	public CtEvaluation saveEvaluation(CtEvaluation ctEvaluation) {
		return orderDao.saveEvaluation(ctEvaluation);
	}

	@Override
	public void updateEvaIdByOrder(Long orderId, Long evaId) {
		orderDao.updateEvaIdByOrder(orderId,evaId);
	}

	@Override
	public CtEvaluation getEvaByOrderId(Long orderId) {
		return orderDao.getEvaByOrderId(orderId);
	}

	@Logg(operationType="update", operationName="提交订单退款信息状态")
	@Override
	public String updateRetOrderByOrderSn(CtOrderInfo orderInfo) {
		return orderDao.updateRetOrderByOrderSn(orderInfo);
	}

	@Override
	public List<CtComplain> getOrderComplainByUid(Long uId,Page page) {
		return orderDao.getOrderComplainByUid(uId, page);
	}

	@Override
	public String updateComplain(CtComplain complain) {
		return orderDao.updateComplain(complain);
	}

	@Override
	public CtComplain getComplainByComId(Integer comId) {
		return orderDao.getComplainByComId(comId);
	}

	@Override
	public List<CtOrderInfo> getOrderReturnGoods(Long uId, Page page) {
		return orderDao.getOrderReturnGoods(uId,page);
	}

	@Override
	public List<CtOrderInfo> getFindOrderBy(Long uid, Page page, OrderDTO orderDTO) {
		Pattern pattern = Pattern.compile("[0-9]*");
		Matcher isNum = pattern.matcher(orderDTO.getFindStr());
		Boolean isGoods =false;
		if(!isNum.matches()){
			isGoods = false;
		} else {
			isGoods = true;
		}
		return orderDao.getFindOrderBy(uid,page,orderDTO,isGoods);
	}

	@Override
	public CtCart getCartByFCartId(Long cartId) {
		// TODO Auto-generated method stub
		return orderDao.getCartByFCartId(cartId);
	}

	@Override
	public List<CtShop> getAllShop() {
		return orderDao.getAllShop();
	}

	@Override
	public String updateCartUtil(String uid) {
		return orderDao.updateCartUtil(uid);
	}

	@Override
	public String getIsPanOrK(Long uid) {
		return orderDao.getIsPanOrK(uid);
	}

	@Override
	public List<CtOrderInfo> getSomeOrderListByUId(Long uid) {
		// TODO Auto-generated method stub
		return orderDao.getSomeOrderListByUId(uid);
	}
	@Override
	public List<CtOrderInfo> getSomeOrderListByUId1(Long uid,Page page) {
		// TODO Auto-generated method stub
		return orderDao.getSomeOrderListByUId1(uid,page);
	}

	@Override
	public Long dfCount() {
		// TODO Auto-generated method stub
		return orderDao.dfCount();
	}

	@Override
	public Long dscount() {
		// TODO Auto-generated method stub
		return orderDao.dscount();
	}

	@Override
	public Long dfhcount() {
		// TODO Auto-generated method stub
		return orderDao.dfhcount();
	}

	@Override
	public Long dshcount() {
		// TODO Auto-generated method stub
		return orderDao.dshcount();
	}

	@Override
	public Long dpjcount() {
		// TODO Auto-generated method stub
		return orderDao.dpjcount();
	}

	@Override
	public CtEvaluation getEvaByEvaId(Long evaId) {
		// TODO Auto-generated method stub
		return orderDao.getEvaByEvaId(evaId);
	}

	public CtOrderInfo getOrderStatusByOrderSn(String orderSn) {
		return orderDao.getOrderStatusByOrderSn(orderSn);
	}

	@Override
	public List<CtEvaluation> getEvaAll() {
		// TODO Auto-generated method stub
		return orderDao.getEvaAll();
	}

	@Override
	public CtOrderInfo getOrderInfoByOrderSn(String orderSn) {
		return orderDao.getOrderInfoByOrderSn(orderSn);
	}

	@Override
	public void tuPayOneOrderPayType(Long orderId) {
		orderDao.tuPayOneOrderPayType(orderId);
	}

	@Logg(operationType="update", operationName="更新用户积分信息")
	@Override
	public void updateUserCredit(CtUser cu) {
		orderDao.updateUserCredit(cu);
	}

	@Override
	public CtCouponDetail getCcdByCcdId(String couponId) {
		return orderDao.getCcdByCcdId(couponId);
	}

	@Logg(operationType="update", operationName="取消订单退还优惠券信息")
	@Override
	public void updateCouponRes(CtCouponDetail ccd) {
		orderDao.updateCouponRes(ccd);
	}

	@Override
	public String getUserIdByUid(String uId) {
		return orderDao.getUserIdByUid(uId);
	}

	@Override
	public void updatePayByOrder(String out_trade_no, String type) {
		orderDao.updatePayByOrder(out_trade_no,type);
	}

	@Override
	public List<CtGoodsBrand> getGoodsBrandTopSix() {
		return orderDao.getGoodsBrandTopSix();
	}

	@Override
	public String getCateSimParNumByUserId(Long uId) {
		return orderDao.getCateSimParNumByUserId(uId);
	}

	@Override
	public void saveTransaction(CtTransaction transaction) {
		orderDao.saveTransaction(transaction);
	}

	@Override
	public List<CtOrderInfo> getorderToday() {
		return orderDao.getorderToday();
	}

	@Override
	public CtTransaction getTranInfoByOrderSn(String orderSn) {
		return orderDao.getTranInfoByOrderSn(orderSn);
	}

	@Override
	public List<CtTransaction> findTranByTop30() {
		return orderDao.findTranByTop30();
	}

	@Override
	public int findTramByCountList(int i) {
		return orderDao.findTramByCountList(i);
	}

	@Override
	public List<CtTransaction> findTramByIsFalseData() {
		return orderDao.findTramByIsFalseData();
	}

	@Logg(operationType="save", operationName="发起退款信息")
	@Override
	public void saveRefundAndUpdateOrderAndGoods(CtRefund refund,
			CtOrderInfo orderInfo) {
		orderDao.saveRefundAndUpdateOrderAndGoods(refund, orderInfo);
	}

	@Logg(operationType="save", operationName="保存用户去下订单退款信息")
	@Override
	public void saveRefundCancelOrder(CtOrderInfo orderInfo) {
		orderDao.saveRefundCancelOrder(orderInfo);
	}

	@Override
	public void saveWxPay(CtPaymentWx wx) {
		orderDao.saveWxPay(wx);
	}

	@Override
	public CtPaymentWx getWxPayByOrderSn(String string) {
		return orderDao.getWxPayByOrderSn(string);
	}

	@Override
	public CtPaymentWx findPayWxByOpenId(String openid) {
		return orderDao.findPayWxByOpenId(openid);
	}

	@Override
	public CtShop getShopType4ById(Short sId) {
		return orderDao.getShopType4ById(sId);
	}

	@Override
	public List<CtOrderInfo> findOrderListByOrderStart(String string) {
		return orderDao.findOrderListByOrderStart(string);
	}

	@Override
	public void updateOrderByOrderInfo(CtOrderInfo orderInfo) {
		orderDao.updateOrderByOrderInfo(orderInfo);
	}

	@Override
	public CtCart getCartByGidAndTypeId(String gid, int i, Long uid) {
		return orderDao.getCartByGidAndTypeId(gid,i, uid);
	}

	@Logg(operationType="delete", operationName="删除购物车的商品")
	@Override
	public void delCartGoodsByUidAndGid(Long uid, Long ctid) {
		orderDao.delCartGoodsByUidAndGid(uid, ctid);
	}

	@Override
	public String findPayTotalByOrderSn(String orderSn) {
		return orderDao.findPayTotalByOrderSn(orderSn);
	}

	@Override
	public String getOrderTotalByTime(String time, CtUser cu) {
		return orderDao.getOrderTotalByTime(time, cu);
	}

	@Override
	public List<CtCashCoupon> findCashCouponByOrderSn(String orderSn) {
		return orderDao.findCashCouponByOrderSn(orderSn);
	}

	@Override
	public CtCashCoupon getCashInfoByCashId(Integer cashId) {
		return orderDao.getCashInfoByCashId(cashId);
	}

	@Logg(operationType="update", operationName="用户使用现金券")
	@Override
	public void updateCashCouponById(CtCashCoupon cashCoupon) {
		orderDao.updateCashCouponById(cashCoupon);
	}

	@Override
	public List<CtOrderInfo> getOrderListByUIdAndXingYong(Long uid, Page page,
			int i, int j) {
		return orderDao.getOrderListByUIdAndXingYong(uid, page, i, j);
	}

	@Override
	public CtUser getUserByIUid(Long uid) {
		return orderDao.getUserByIUid(uid);
	}

	@Override
	public void updateUser(CtUser cu) {
		orderDao.updateUser(cu);
	}

}

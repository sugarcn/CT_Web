package org.ctonline.manager.order.impl;

import java.util.List;

import javax.annotation.Resource;

import org.ctonline.dao.order.ICtPayDAO;
import org.ctonline.dto.order.PayDTO;
import org.ctonline.manager.order.ICtPayManager;
import org.ctonline.po.order.CtPay;
import org.springframework.stereotype.Service;

public class CtPayManagerImpl implements ICtPayManager {
	
	private ICtPayDAO dao;

	public ICtPayDAO getDao() {
		return dao;
	}

	public List findAll() {
		return dao.findAll();
	}

	public void setDao(ICtPayDAO dao) {
		this.dao = dao;
	}

	@Override
	public String saveOrderPay(CtPay ctPay) {
		String res = dao.saveOrderPay(ctPay);
		return res;
	}

	@Override
	public CtPay getPayChong(Long orderId) {
		return dao.getPayChong(orderId);
	}
	
}

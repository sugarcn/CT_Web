package org.ctonline.manager.order.impl;

import java.util.List;

import javax.annotation.Resource;

import org.ctonline.dao.order.ICtBankDAO;
import org.ctonline.manager.order.ICtBankManager;
import org.ctonline.po.order.CtBank;
import org.springframework.stereotype.Service;

public class CtBankManagerImpl implements ICtBankManager {

	private ICtBankDAO dao;

	public ICtBankDAO getDao() {
		return dao;
	}

	public void setDao(ICtBankDAO dao) {
		this.dao = dao;
	}

	public List findAll() {
		return dao.findAll();
	}
}

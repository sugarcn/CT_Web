package org.ctonline.manager.order;

import java.util.List;

import org.ctonline.dto.order.PayDTO;
import org.ctonline.po.order.CtPay;

public interface ICtPayManager {
	public abstract List findAll();

	public abstract String saveOrderPay(CtPay payDTO);

	public abstract CtPay getPayChong(Long orderId);

}
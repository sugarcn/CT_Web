package org.ctonline.test;

import java.math.BigDecimal;
import java.util.Random;

import org.ctonline.po.basic.CtXwStock;
import org.ctonline.po.basic.CtXwSubStockinfo;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class StockNumInfo  extends RanInit implements Job  {
//	public static void main(String[] args) throws Exception {
//		//总库存
//		 allStockNum = 5200000000L;
//		//总金额
//		Long allPriceNum = 42000000L;
//		//单天总库存
//		Long stockNum = 0L;
//		//单天总价格
//		Double priceSinAll = 0D;
//		int s = 0;
//		//模拟总数480分钟
//		int max=800000;
//		int min=600000;
//		Random random = new Random();
//		
//		//随机的数量范围区间
//		s = random.nextInt(max)%(max-min+1) + min;
//		Double danci = div(s, 480, 2);
//		System.out.println("总金额："+s);
//		for (int j = 0; j < 480; j++) {
//			
//			max = 40;
//			min = 10;
//			s = random.nextInt(max)%(max-min+1) + min;
//			
//			Double temp = sub(danci, s) + random.nextInt(30)%(30-15+1) + 15;
//			int dan = (int) div(temp, 123, 0);
//			stockNum += dan;
//			System.out.println(dan);
//			priceSinAll = add(priceSinAll, temp);
//			System.out.println(temp);
//		}
//		
//		System.out.println("今天减去的总库存：" + stockNum);
//		System.out.println("今天减去的总金额:" + priceSinAll);
//		
//		allStockNum -= stockNum;
//		String allPriceNum1 = Double.valueOf(allPriceNum.toString())-priceSinAll + "";
//		System.out.println("剩余库存："+allStockNum);
//		System.out.println("剩余库存金额："+ new BigDecimal(allPriceNum1));
//	}
	
	/**
     * 提供精确加法计算的add方法
     * @param value1 被加数
     * @param value2 加数
     * @return 两个参数的和
     */
    public static double add(double value1,double value2){
        BigDecimal b1 = new BigDecimal(Double.valueOf(value1));
        BigDecimal b2 = new BigDecimal(Double.valueOf(value2));
        return b1.add(b2).doubleValue();
    }
    
    /**
     * 提供精确减法运算的sub方法
     * @param value1 被减数
     * @param value2 减数
     * @return 两个参数的差
     */
    public static double sub(double value1,double value2){
        BigDecimal b1 = new BigDecimal(Double.valueOf(value1));
        BigDecimal b2 = new BigDecimal(Double.valueOf(value2));
        return b1.subtract(b2).doubleValue();
    }
    
    /**
     * 提供精确乘法运算的mul方法
     * @param value1 被乘数
     * @param value2 乘数
     * @return 两个参数的积
     */
    public static double mul(double value1,double value2){
        BigDecimal b1 = new BigDecimal(Double.valueOf(value1));
        BigDecimal b2 = new BigDecimal(Double.valueOf(value2));
        return b1.multiply(b2).doubleValue();
    }
    
    /**
     * 提供精确的除法运算方法div
     * @param value1 被除数
     * @param value2 除数
     * @param scale 精确范围
     * @return 两个参数的商
     * @throws IllegalAccessException
     */
    public static double div(double value1,double value2,int scale) throws IllegalAccessException{
        //如果精确范围小于0，抛出异常信息
        if(scale<0){         
            throw new IllegalAccessException("精确度不能小于0");
        }
        BigDecimal b1 = new BigDecimal(Double.valueOf(value1));
        BigDecimal b2 = new BigDecimal(Double.valueOf(value2));
        return b1.divide(b2, scale).doubleValue();    
    }

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {
			Long stockNum = 0L;
			Double priceSinAll = 0D;
			int max = 0;
			int min = 0;
			Random random = new Random();
			Double danci = 0D;
			System.out.println("zhouqi:"+zhouqi);
			if(zhouqi == 540){
				//模拟总数480分钟
				max=800000;
				min=600000;
				//随机的数量范围区间
				s = random.nextInt(max)%(max-min+1) + min;
				
				System.out.println("总金额："+s);
				logger.info("总金额："+s);
			}
			if(zhouqi != 0){
				zhouqi--;
			} else {
				zhouqi = 540;
				s = 0;
			}
			danci = div(s, 540, 2);
//			SYSTEM.OUT.PRINTLN("S:"+S);
//			SYSTem.out.println("danci:"+danci);
			max = 800;
			min = 450;
			int ss = random.nextInt(max)%(max-min+1) + min;
			Double temp = sub(danci, ss) + random.nextInt(800)%(800-120+1) + 120;
			int dan = (int) div(temp, random.nextInt(180)%(180-130+1) + 130, 0);
			stockNum += dan;
			//System.out.println(dan);
			priceSinAll = add(priceSinAll, temp);
			
			System.out.println("zhouqi:"+zhouqi);
			System.out.println("减去的总库存：" + stockNum);
			logger.info("减去的总库存：" + stockNum);
			System.out.println("减去的总金额:" + priceSinAll);
			logger.info("减去的总金额:" + priceSinAll);
			
			allStockNum -= stockNum;
			allPriceNum = (Double.valueOf(allPriceNum.toString())-priceSinAll);
			CtXwStock stock = goodsManager.findByXWStock();
			stock.setStockNum(allStockNum);
			stock.setStockAllprice(allPriceNum);
			goodsManager.merge(stock);
			
			CtXwSubStockinfo stockinfo = new CtXwSubStockinfo();
			stockinfo.setSubStockNum(stockNum.intValue());
			stockinfo.setSubStockPrice(priceSinAll);
			goodsManager.merge(stockinfo);
			System.out.println("剩余库存："+allStockNum);
			logger.info("剩余库存："+allStockNum);
			System.out.println("剩余库存金额："+ new BigDecimal(allPriceNum));
			logger.info("剩余库存金额："+ new BigDecimal(allPriceNum));
		} catch (NumberFormatException e) {
			e.printStackTrace();
			logger.info(e.getMessage());
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			logger.info(e.getMessage());
		}
	}

}

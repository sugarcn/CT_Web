package org.ctonline.test;

import java.util.Date;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

/**
 * 调用任务的类
 * @author lxk
 *
 */
public class SchedulerTest {
   public static void main(String[] args) {
   
	 //通过schedulerFactory获取一个调度器
	   SchedulerFactory schedulerfactory=new StdSchedulerFactory();
	   Scheduler scheduler=null;
	   SchedulerFactory schedulerfactory1=new StdSchedulerFactory();
	   Scheduler scheduler1=null;
	   try{
//		通过schedulerFactory获取一个调度器
		   scheduler=schedulerfactory.getScheduler();
		   scheduler1=schedulerfactory1.getScheduler();
		   
//		 创建jobDetail实例，绑定Job实现类
//		 指明job的名称，所在组的名称，以及绑定job类
		   JobDetail job=JobBuilder.newJob(RanPayOrder.class).withIdentity("job1", "jgroup1").build();
		   JobDetail job1=JobBuilder.newJob(RanPayOrder.class).withIdentity("job2", "jgroup2").build();
		 
		   
//		 定义调度触发规则
		   
//		使用simpleTrigger规则
//		  Trigger trigger=TriggerBuilder.newTrigger().withIdentity("simpleTrigger", "triggerGroup")
//				          .withSchedule(SimpleScheduleBuilder.repeatSecondlyForever(1).withRepeatCount(8))
//				          .startNow().build();
//		使用cornTrigger规则 
			  Trigger trigger=TriggerBuilder.newTrigger().withIdentity("simpleTrigger", "triggerGroup")
	          .withSchedule(CronScheduleBuilder.cronSchedule("0/10 0-59 10,11,12,13,14,15,16,17,18,19,20 * * ? *"))
	          .startNow().build(); 
			  Trigger trigger1=TriggerBuilder.newTrigger().withIdentity("simpleTrigger1", "triggerGroup1")
					  .withSchedule(CronScheduleBuilder.cronSchedule("0/5 27 10 * * ? *"))
					  .startNow().build(); 
		   //      0 0/1 14,18 * * ?  在每天下午2点到2:55期间和下午6点到6:55期间的每5分钟触发 
//		 把作业和触发器注册到任务调度中
		   scheduler.scheduleJob(job, trigger);
		   //scheduler1.scheduleJob(job1, trigger1);
		   
//		 启动调度
		   scheduler.start();
		   //scheduler1.start();
		   
		   
	   }catch(Exception e){
		   e.printStackTrace();
	   }
	   
}
}

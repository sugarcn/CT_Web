package org.ctonline.test;

import java.util.List;

import org.ctonline.po.order.CtOrderInfo;
import org.ctonline.po.user.CtUser;
import org.ctonline.service.JdpCtegoTrade;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class UpdateEjlOrder  extends RanInit implements Job {
	public static void main(String[] args) {
		List<CtOrderInfo> listOrder = orderManager.getorderToday();
		for (int i = 0; i < listOrder.size(); i++) {
			System.out.println(listOrder.get(i).getOrderSn() + "___" + listOrder.get(i).getOrderTime());
		}
	}

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {
			List<CtOrderInfo> listOrder = orderManager.getorderToday();
			System.out.println("开始遍历");
			//JdpCtegoTrade jct = delegate.getAllTradeByOrderId(1284L);
			logger.info("今日订单数" + listOrder.size());
			System.out.println("今日订单数" + listOrder.size());
			for (int i = 0; i < listOrder.size(); i++) {
				if(listOrder.get(i).getOrderStatus().equals("1") || listOrder.get(i).getOrderStatus().equals("3")){
					logger.info("付款" + i);
					System.out.println("付款");
					JdpCtegoTrade jct = delegate.getAllTradeByOrderId(listOrder.get(i).getOrderId());
					if(jct == null){
						logger.info("未到EJL");
						System.out.println("未到EJL");
						CtUser user = userManager.getUserByUId(Long.valueOf(listOrder.get(i).getUId()));
						insertEJLOrder(listOrder.get(i),user,payManager);
					}
					logger.info("结束");
					System.out.println(jct);
				}
			}
		} catch (Exception e1) {
			logger.info("EJL订单异常:"+e1.getMessage());
			e1.printStackTrace();
		}
	}

	
}

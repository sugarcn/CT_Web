package org.ctonline.test;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;

import com.cloopen.rest.sdk.CCPRestSDK;

public class serviceInterface {
	public static void main(String[] args)throws Exception
	{
		
		 //Double price = Double.valueOf((Double.valueOf("100") / 100));
		//System.out.println(price);
//		paySuccessWxNew("13400051102", "刘晓康", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "100000000");
		regMess("18566785567", "刘晓康");
	}
	/**
	 * 注册成功短信通知
	 * @param phone 目标手机号
	 * @param name 用户姓名
	 */
	public static void regMess(String phone,String name) {
		HashMap<String, Object> result = null;
		
		CCPRestSDK restAPI = new CCPRestSDK();
		restAPI.init("sandboxapp.cloopen.com", "8883");// 初始化服务器地址和端口，格式如下，服务器地址不需要写https://
		restAPI.setAccount("8a48b5514f73ea32014f9d9be8fa5029", "6f3378d732b847729fbbe29b9c8692d7");// 初始化主帐号和主帐号TOKEN
		restAPI.setAppId("8a216da854e1a37a0154e1e8e28900b3");// 初始化应用ID
		result = restAPI.sendTemplateSMS(phone,"145571" ,new String[]{name});
		
		System.out.println("SDKTestSendTemplateSMS result=" + result);
		if("000000".equals(result.get("statusCode"))){
			//正常返回输出data包体信息（map）
			HashMap<String,Object> data = (HashMap<String, Object>) result.get("data");
			Set<String> keySet = data.keySet();
			for(String key:keySet){
				Object object = data.get(key);
				System.out.println(key +" = "+object);
			}
		}else{
			//异常返回输出错误码和错误信息
			System.out.println("错误码=" + result.get("statusCode") +" 错误信息= "+result.get("statusMsg"));
		}
	}
	/**
	 * 付款成功短信通知
	 * @param phone 目标手机号
	 * @param name 订单编号
	 */
	public static void paySuccessInfoMess(String phone,String name) {
		HashMap<String, Object> result = null;
		
		CCPRestSDK restAPI = new CCPRestSDK();
		restAPI.init("sandboxapp.cloopen.com", "8883");// 初始化服务器地址和端口，格式如下，服务器地址不需要写https://
		restAPI.setAccount("8a48b5514f73ea32014f9d9be8fa5029", "6f3378d732b847729fbbe29b9c8692d7");// 初始化主帐号和主帐号TOKEN
		restAPI.setAppId("8a216da854e1a37a0154e1e8e28900b3");// 初始化应用ID
		result = restAPI.sendTemplateSMS(phone,"145572" ,new String[]{name});
		
		System.out.println("SDKTestSendTemplateSMS result=" + result);
		if("000000".equals(result.get("statusCode"))){
			//正常返回输出data包体信息（map）
			HashMap<String,Object> data = (HashMap<String, Object>) result.get("data");
			Set<String> keySet = data.keySet();
			for(String key:keySet){
				Object object = data.get(key);
				System.out.println(key +" = "+object);
			}
		}else{
			//异常返回输出错误码和错误信息
			System.out.println("错误码=" + result.get("statusCode") +" 错误信息= "+result.get("statusMsg"));
		}
	}
	
	
	public static void paySuccessWx(String phone,String name, String dateStr, String price) throws HttpException, IOException{
		HttpClient client = new HttpClient();
		PostMethod post = new PostMethod("http://gbk.sms.webchinese.cn/"); 
		post.addRequestHeader("Content-Type","application/x-www-form-urlencoded;charset=gbk");//在头文件中设置转码																		//您好，您本次验证码为1041，请在页面填写验证码完成验证，请勿泄露给他人哦~！【长亭易购】
		NameValuePair[] data ={ new NameValuePair("Uid", "ctdz"),
				new NameValuePair("Key", "4a9ff4c77a2688b334c5"),
				new NameValuePair("smsMob",""+phone+""),
				new NameValuePair("smsText",name + "于"+dateStr+"向您微信成功支付"+price+"元【长亭易购】")
		};
		post.setRequestBody(data);

		client.executeMethod(post);
		Header[] headers = post.getResponseHeaders();
		int statusCode = post.getStatusCode();
		System.out.println(statusCode);
		//System.out.println("statusCode:"+statusCode);
		for(Header h : headers)
		{
		//System.out.println(h.toString());
		}
		String result = new String(post.getResponseBodyAsString().getBytes("gbk")); 
		System.out.println(result);
		//System.out.println(result); //打印返回消息状态
		post.releaseConnection();
		
	}
	
	
	public void getPhone1(String phone,String code) throws HttpException, IOException{
		HttpClient client = new HttpClient();
		PostMethod post = new PostMethod("http://gbk.sms.webchinese.cn/"); 
		post.addRequestHeader("Content-Type","application/x-www-form-urlencoded;charset=gbk");//在头文件中设置转码																		//您好，您本次验证码为1041，请在页面填写验证码完成验证，请勿泄露给他人哦~！【长亭易购】
		NameValuePair[] data ={ new NameValuePair("Uid", "ctdz"),new NameValuePair("Key", "4a9ff4c77a2688b334c5"),new NameValuePair("smsMob",""+phone+""),new NameValuePair("smsText","您好，您本次验证码为"+code+"，请在页面填写验证码完成验证，请勿泄露给他人哦~！【长亭易购】")};
		post.setRequestBody(data);

		client.executeMethod(post);
		Header[] headers = post.getResponseHeaders();
		int statusCode = post.getStatusCode();
		//System.out.println("statusCode:"+statusCode);
		for(Header h : headers)
		{
		//System.out.println(h.toString());
		}
		String result = new String(post.getResponseBodyAsString().getBytes("gbk")); 
		//System.out.println(result); //打印返回消息状态
		post.releaseConnection();
		
	}
	
	public void getPhone(String phone,String code) {
		HashMap<String, Object> result = null;

		CCPRestSDK restAPI = new CCPRestSDK();
		restAPI.init("sandboxapp.cloopen.com", "8883");// 初始化服务器地址和端口，格式如下，服务器地址不需要写https://
		restAPI.setAccount("8a48b5514f73ea32014f9d9be8fa5029", "6f3378d732b847729fbbe29b9c8692d7");// 初始化主帐号和主帐号TOKEN
		restAPI.setAppId("aaf98f89512446e201513858d4703d1c");// 初始化应用ID
		result = restAPI.sendTemplateSMS(phone,"51083" ,new String[]{code,"5"});
   
		System.out.println("SDKTestSendTemplateSMS result=" + result);
		if("000000".equals(result.get("statusCode"))){
			//正常返回输出data包体信息（map）
			HashMap<String,Object> data = (HashMap<String, Object>) result.get("data");
			Set<String> keySet = data.keySet();
			for(String key:keySet){
				Object object = data.get(key);
				System.out.println(key +" = "+object);
			}
		}else{
			//异常返回输出错误码和错误信息
			System.out.println("错误码=" + result.get("statusCode") +" 错误信息= "+result.get("statusMsg"));
		}
	}
	public static void paySuccessWxNew(String phone,String name, String dateStr, String price) {
		HashMap<String, Object> result = null;
		
		CCPRestSDK restAPI = new CCPRestSDK();
		restAPI.init("sandboxapp.cloopen.com", "8883");// 初始化服务器地址和端口，格式如下，服务器地址不需要写https://
		restAPI.setAccount("8a48b5514f73ea32014f9d9be8fa5029", "6f3378d732b847729fbbe29b9c8692d7");// 初始化主帐号和主帐号TOKEN
		restAPI.setAppId("8a216da854e1a37a0154e1e8e28900b3");// 初始化应用ID
		result = restAPI.sendTemplateSMS(phone,"88695" ,new String[]{name,dateStr,price});
		
		System.out.println("SDKTestSendTemplateSMS result=" + result);
		if("000000".equals(result.get("statusCode"))){
			//正常返回输出data包体信息（map）
			HashMap<String,Object> data = (HashMap<String, Object>) result.get("data");
			Set<String> keySet = data.keySet();
			for(String key:keySet){
				Object object = data.get(key);
				System.out.println(key +" = "+object);
			}
		}else{
			//异常返回输出错误码和错误信息
			System.out.println("错误码=" + result.get("statusCode") +" 错误信息= "+result.get("statusMsg"));
		}
	}

}

package org.ctonline.test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import net.sf.json.JSONArray;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

import org.apache.log4j.Logger;
import org.ctonline.manager.goods.F_GoodsManager;
import org.ctonline.manager.order.OrderManager;
import org.ctonline.manager.pay.CtPayManager;
import org.ctonline.manager.user.CtUserManager;
import org.ctonline.po.basic.CtXwStock;
import org.ctonline.po.order.CtOrderInfo;
import org.ctonline.po.user.CtUser;
import org.ctonline.service.EJLInfoServiceImplDelegate;
import org.ctonline.service.EJLInfoServiceImplService;
import org.ctonline.service.JdpCtegoTrade;
import org.ctonline.util.FormatJson;
import org.ctonline.util.JsonOrderInfo;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class RanInit {
	public static OrderManager orderManager;
	public static CtUserManager userManager;
	public static CtPayManager payManager;
	public static F_GoodsManager goodsManager;
	
	public static String[] nameXins;//姓氏
	public static String[] xiaoshu;//金额的小数部分
	public static String[] startPay;//交易状态
	public static String[] orderStart;//订单状态
	public static String[] jiageQuJian;
	public static Logger logger;
	public static Long allStockNum;
	public static Double allPriceNum;
	public static EJLInfoServiceImplDelegate delegate;
	public static int zhouqi;
	public static int zhouqiJia;
	public static int s;
	//初始化原始属性
	static{
		try {
			zhouqi = 540;
			zhouqiJia = 540;
			s = 0;
			logger = Logger.getLogger("OperateLog");
			delegate = new EJLInfoServiceImplService().getEJLInfoServiceImplPort();
			System.out.println("客户名称\t订单编号\t下单日期\t金额\t付款状态\t订单状态");
			//初始化百家姓
			nameXins = new String[]{"赵","钱","孙","李","周","吴","郑","王","冯","陈","蒋","沈","韩","杨","朱","秦","许","何","吕","施","张","孔","曹","严","华","金","魏","陶","谢","邹","喻","柏","水","苏","潘","葛","范","彭","鲁","马","苗","袁","柳","史","薛","雷","贺","倪","汤","滕","殷","罗","毕","郝","于","皮","卞","齐","余","顾","孟","平","萧","尹","邵"};
			//初始化金额的小数部分，.00占50% .50占20% .25占20% .75占10%
			xiaoshu = new String[]{".00",".25",".50",".75",".00",".25",".00",".00",".00",".50"};
			//初始化交易状态
			startPay = new String[]{"已付款","未付款","已付款","已付款","已付款","未付款","已付款"};
			//初始化订单状态
			orderStart = new String[]{"已备货","已发货","待发货"};
			//初始化价格区间
			jiageQuJian = new String[]{"1-35","36-100",
										"101-150","151-2000",
										"1-35","1-35","1-35",
										"1-35","1-35","1-35",
										"1-35","1-35","1-35",
										"1-35","1-35","1-35",
										"1-35","101-150","36-100",
										"36-100"};
			ApplicationContext ctx = new ClassPathXmlApplicationContext(new String[]{"applicationContext.xml",
					 																	"spring/applicationContext-basic.xml",
					 																	"spring/applicationContext-goods.xml",
					 																	"spring/applicationContext-help.xml",
					 																	"spring/applicationContext-pay.xml",
					 																	"spring/applicationContext-role.xml",
					 																	"spring/applicationContext-user.xml",
					 																	"spring/applicationContext-order.xml"
					 																	});
			 orderManager = (OrderManager) ctx.getBean("orderManager");
			 userManager = (CtUserManager) ctx.getBean("userManager");
			 payManager = (CtPayManager) ctx.getBean("payManager");
			 goodsManager = (F_GoodsManager) ctx.getBean("fgoodsManager");
			 CtXwStock cxs = goodsManager.findByXWStock();
				//总库存
			allStockNum = cxs.getStockNum();
				//总金额
			allPriceNum = cxs.getStockAllprice();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
public static void insertEJLOrder(CtOrderInfo orderInfo, CtUser user, CtPayManager payManager) {
		
		orderInfo.setOrderStatus(orderInfo.getOrderStatus());
		EJLInfoServiceImplDelegate delegate = new EJLInfoServiceImplService().getEJLInfoServiceImplPort();
		//List<JdpCtegoTrade> list = delegate.getAllTrade();
		JdpCtegoTrade trade = new JdpCtegoTrade();
		trade = new JdpCtegoTrade();
		trade.setOrderId(orderInfo.getOrderId());
		trade.setStatus(orderInfo.getOrderStatus());
		trade.setSellerNick("长亭易购");
		trade.setType("fixed");
		trade.setUserId(Long.valueOf(orderInfo.getUId()));
		trade.setBuyerNick(user.getUUsername());
		trade.setCreated(orderInfo.getOrderTime());
		trade.setModified(orderInfo.getOrderTime());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = sdf.format(new Date());
		trade.setJdpCreated(date);
		trade.setJdpModified(date);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
		String payTime = payManager.findPayByOrderSn(orderInfo.getOrderSn());
		
		JsonOrderInfo info = FormatJson.formatJsonOrder(orderInfo,payTime);
		
		JSONArray jsonArray = JSONArray.fromObject(info);
		String jsonStr = jsonArray.toString().substring(1, jsonArray.toString().length()-1);
		jsonStr = "{\"trade_fullinfo_get_response\": { \"trade\": " + jsonStr + " } }";
		trade.setJdpResponse(jsonStr);
		
		String mess = delegate.jdpInsertInfo(trade);
		System.out.println(mess);
		
		logger.info("成功新增一条记录+++++++++++++++++++++++++++");
		//System.out.println(jsonArray);
		
	}
}

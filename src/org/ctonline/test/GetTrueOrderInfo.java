package org.ctonline.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.ctonline.po.basic.CtTransaction;
import org.ctonline.po.order.CtOrderInfo;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class GetTrueOrderInfo extends RanInit implements Job {

	private List<CtOrderInfo> orderInfoList = new ArrayList<CtOrderInfo>();
	private CtTransaction transaction = new CtTransaction();
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		orderInfoList = orderManager.getorderToday();
		
		if(orderInfoList != null){
			for (int i = 0; i < orderInfoList.size(); i++) {
				transaction = orderManager.getTranInfoByOrderSn(orderInfoList.get(i).getOrderSn());
				if(transaction == null){
					transaction = new CtTransaction();
				}
				if(orderInfoList.get(i).getOrderStatus().equals("0")){
					transaction.setTranOrderStart("待付款");
					transaction.setTranPay("未付款");
				}
				if(orderInfoList.get(i).getOrderStatus().equals("1")){
					transaction.setTranOrderStart("已付款");
					transaction.setTranPay("已付款");
				}
				if(orderInfoList.get(i).getOrderStatus().equals("2")){
					transaction.setTranOrderStart("已付款");
					transaction.setTranPay("已付款");
				}
				if(orderInfoList.get(i).getOrderStatus().equals("3")){
					transaction.setTranOrderStart("已备货");
					transaction.setTranPay("已付款");
				}
				if(orderInfoList.get(i).getOrderStatus().equals("4")){
					transaction.setTranOrderStart("已发货");
					transaction.setTranPay("已付款");
				}
				if(orderInfoList.get(i).getOrderStatus().equals("5")){
					transaction.setTranOrderStart("已发货");
					transaction.setTranPay("已付款");
				}
				if(orderInfoList.get(i).getOrderStatus().equals("6")){
					transaction.setTranOrderStart("已发货");
					transaction.setTranPay("已付款");
				}
				if(orderInfoList.get(i).getOrderStatus().equals("7")){
					transaction.setTranOrderStart("已发货");
					transaction.setTranPay("已付款");
				}
				int start = orderInfoList.get(i).getOrderTime().indexOf("-");
				String date = orderInfoList.get(i).getOrderTime().substring(start+1, orderInfoList.get(i).getOrderTime().length());
				transaction.setOrderSn(orderInfoList.get(i).getOrderSn());
				String[] chai = orderInfoList.get(i).getTotal().split("\\.");
				String totle = "";
				if(chai.length == 2){
					if(chai[1].length() == 1){
						chai[1] = chai[1] + "0";
					}
					totle = chai[0] + "." + chai[1];
				} else {
					totle = orderInfoList.get(i).getTotal() + ".00";
				}
				transaction.setTranCount(totle);
				transaction.setTranTime(date);
				transaction.setTranUsername(orderInfoList.get(i).getConsignee().substring(0,1)+"**");
				transaction.setTranIsZhenOrJia("1");
				try {
					orderManager.saveTransaction(transaction);
					logger.info("insert true ok");
				} catch (Exception e) {
					e.printStackTrace();
					logger.info("no insert true");
				}
			}
		} else {
			logger.info("未获取到订单");
		}
		
	}

}

package org.ctonline.test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.ctonline.po.basic.CtTransaction;
import org.ctonline.po.order.CtOrderInfo;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class UpdateOrderInfoStauts extends RanInit implements Job {
	List<CtOrderInfo> orderInfosList = new ArrayList<CtOrderInfo>();
	CtOrderInfo orderInfo = new CtOrderInfo();
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		//获取七天之前发货的订单
		orderInfosList = orderManager.findOrderListByOrderStart("4");
		System.out.println(orderInfosList.size());
		logger.info("等待自动确认收货订单数： "+ orderInfosList.size() );
		for (int i = 0; i < orderInfosList.size(); i++) {
			orderInfo = orderInfosList.get(i);
			orderInfo.setOrderStatus("5");
			orderInfo.setIsOkTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			orderManager.updateOrderByOrderInfo(orderInfo);
			System.out.println("确认收货成功");
			logger.info("订单"+orderInfo.getOrderSn()+"确认收货成功");
		}
	}
}

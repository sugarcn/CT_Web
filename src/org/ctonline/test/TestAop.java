package org.ctonline.test;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Pointcut;

public class TestAop implements MethodInterceptor {

	@Override
	public Object invoke(MethodInvocation Invocation) throws Throwable {
		 //System.out.println("============================饭前洗手================================");
	        Object result=Invocation.proceed();//通过反射调用目标对象的方法    
	        //System.out.println(result);
	        //System.out.println("================================吃饭后刷牙============================");
	        return result;
	}
	
	public static void main(String[] args) {
		System.out.println(2<<1);
	}
	
	/**
	 * 新增业务逻辑切方法入点
	 */
	@Pointcut("execution(* org.ctonline.dao.*.impl.*.*(..))")
	public void saveFilmCall(){ }
	

	/**
	 * 修改业务逻辑切方法入点
	 */
	@Pointcut("execution(* org.ctonline.manager.*.impl.*.update*(..))")
	public void updateFilmCall(){ }
	
	
	@Around("saveFilmCall()")
	public void saveVildate(ProceedingJoinPoint joinpoint) throws Throwable{
		//System.out.println("前");
		joinpoint.proceed();
		//System.out.println("后");
	}
	@Around("updateFilmCall()")
	public void updateVildate(ProceedingJoinPoint joinpoint){
		
	}
}

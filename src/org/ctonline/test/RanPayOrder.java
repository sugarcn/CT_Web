package org.ctonline.test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.ctonline.manager.user.CtUserManager;
import org.ctonline.po.basic.CtTransaction;
import org.ctonline.po.order.CtOrderInfo;
import org.ctonline.po.user.CtUser;
import org.ctonline.service.JdpCtegoTrade;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class RanPayOrder extends RanInit implements Job {
//	public static Logger logger;
//	private static OrderManager orderManager;
//	private static String[] nameXins;//姓氏
//	private static String[] xiaoshu;//金额的小数部分
//	private static String[] startPay;//交易状态
//	private static String[] orderStart;//订单状态
//	private static String[] jiageQuJian;
//	//初始化原始属性
//	static{
//		try {
//			System.out.println("客户名称\t订单编号\t下单日期\t金额\t付款状态\t订单状态");
//			//初始化百家姓
//			nameXins = new String[]{"赵","钱","孙","李","周","吴","郑","王","冯","陈","楮","卫","蒋","沈","韩","杨","朱","秦","尤","许","何","吕","施","张","孔","曹","严","华","金","魏","陶","谢","邹","喻","柏","水","窦","苏","潘","葛","奚","范","彭","郎","鲁","韦","昌","马","苗","凤","花","任","袁","柳","酆","鲍","史","廉","岑","薛","雷","贺","倪","汤","滕","殷","罗","毕","郝","邬","安","于","时","傅","皮","卞","齐","余","元","卜","顾","孟","平","穆","萧","尹","姚","邵"};
//			//初始化金额的小数部分，.00占40% .50占20% .25占20% .75占10%
//			xiaoshu = new String[]{".00",".25",".50",".75",".00",".25",".00",".00",".50",".50"};
//			//初始化交易状态
//			startPay = new String[]{"已付款","未付款"};
//			//初始化订单状态
//			orderStart = new String[]{"已备货","已发货","待发货"};
//			//初始化价格区间
//			jiageQuJian = new String[]{"1-35","36-100",
//										"101-150","151-5000",
//										"1-35","1-35","1-35",
//										"1-35","1-35","1-35",
//										"1-35","1-35","1-35",
//										"1-35","1-35","1-35",
//										"1-35","101-150","36-100",
//										"36-100"};
//			ApplicationContext ctx = new ClassPathXmlApplicationContext(new String[]{"applicationContext.xml",
//					 																	"spring/applicationContext-basic.xml",
//					 																	"spring/applicationContext-goods.xml",
//					 																	"spring/applicationContext-help.xml",
//					 																	"spring/applicationContext-pay.xml",
//					 																	"spring/applicationContext-role.xml",
//					 																	"spring/applicationContext-user.xml",
//					 																	"spring/applicationContext-order.xml"
//					 																	});
//			 orderManager = (OrderManager) ctx.getBean("orderManager");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
	
	private CtTransaction transaction = new CtTransaction();
	
	
	
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		
		
//		try {
//			List<CtOrderInfo> listOrder = orderManager.getorderToday();
//			System.out.println("开始遍历");
//			//JdpCtegoTrade jct = delegate.getAllTradeByOrderId(1284L);
//			logger.info("今日订单数" + listOrder.size());
//			System.out.println("今日订单数" + listOrder.size());
//			for (int i = 0; i < listOrder.size(); i++) {
//				if(listOrder.get(i).getOrderStatus().equals("1") || listOrder.get(i).getOrderStatus().equals("3")){
//					logger.info("付款" + i);
//					System.out.println("付款");
//					JdpCtegoTrade jct = delegate.getAllTradeByOrderId(listOrder.get(i).getOrderId());
//					if(jct == null){
//						logger.info("未到EJL");
//						System.out.println("未到EJL");
//						CtUser user = userManager.getUserByUId(Long.valueOf(listOrder.get(i).getUId()));
//						insertEJLOrder(listOrder.get(i),user,payManager);
//					}
//					logger.info("结束");
//					System.out.println(jct);
//				}
//			}
//		} catch (Exception e1) {
//			logger.info("EJL订单异常:"+e1.getMessage());
//			e1.printStackTrace();
//		}
		
		
		//System.out.println(i);
		//System.out.println(orderManager);
		System.out.println("ok");
		// 生成订单号SN
		Random ra =new Random();
		
		int isOk = ra.nextInt(10);
		if(isOk == 1 || isOk == 2 || isOk == 3 || isOk == 0||isOk==4||isOk==8||isOk==9 ||isOk==10){
			Date dt = new Date();
			Long time = dt.getTime();
			
			Long osn = time * ra.nextInt(5000)+1;
			String orderSN = "CT"
					+ osn.toString().substring(osn.toString().length() - 10,
							osn.toString().length());
			
			//随机一个姓氏
			String name = nameXins[ra.nextInt(nameXins.length)];
			//随机出是三个字的姓名还是两个的 最终姓名格式   姓氏+** 或者  姓氏+*
			for (int i = 0; i < ra.nextInt(2)+1; i++) {
				name+="*";
			}
			SimpleDateFormat sdf = new SimpleDateFormat("HH");
			String xiaoshi = sdf.format(dt);
			int start = 0;
			int end = 0;
			String qujian = jiageQuJian[ra.nextInt(jiageQuJian.length)];
			start = Integer.valueOf(qujian.split("-")[0]);
			end = Integer.valueOf(qujian.split("-")[1]);
			System.out.println(start + "----" + end);
			//随机价格
			int zhengJia = ra.nextInt(end)%(end-start+1) + start;
			String jia = String.valueOf(zhengJia) + xiaoshu[ra.nextInt(xiaoshu.length)];
			
			
			//随机出付款状态
			int a = ra.nextInt(startPay.length);
			//System.out.println(a);
			String payStart = startPay[a];
			
			
			//随机出订单状态
			String orderSta = "";
			//只有在已付款的订单中才会出现发货信息
			if(payStart.equals("已付款")){
				orderSta = orderStart[ra.nextInt(orderStart.length)];
			} else {
				orderSta = "待付款";
			}
			sdf = new SimpleDateFormat("mm");
			int m = 0;
			if(Integer.valueOf(sdf.format(dt))>5){
				m = Integer.valueOf(sdf.format(dt));
			} else {
				m=0;
			}
			String min = (m - ra.nextInt(5))+"";
			if(Integer.valueOf(min) < 10){
				if(Integer.valueOf(min) >= 0){
					min = "0"+min;
				} else {
					min = "0"+min.toString().substring(1, min.length());
				}
			}
			
			String sce = ra.nextInt(59)+"";
			if(Integer.valueOf(sce) < 10){
				sce = "0"+sce;
			}
			sdf = new SimpleDateFormat("HH");
			String dateStr = sdf.format(dt) + ":"+min + ":" + sce;
			sdf = new SimpleDateFormat("MM-dd");
			dateStr = sdf.format(dt) + " " + dateStr;
			System.out.println(name +  "\t" + orderSN +"\t" + dateStr + "\t" + jia + "\t" + payStart + "\t" + orderSta);
			transaction.setOrderSn(orderSN);
			transaction.setTranCount(jia);
			transaction.setTranOrderStart(orderSta);
			transaction.setTranPay(payStart);
			transaction.setTranTime(dateStr);
			transaction.setTranUsername(name);
			transaction.setTranIsZhenOrJia("0");
			try {
				logger.info("orderManager:"+orderManager);
				logger.info("transaction:"+transaction);
				orderManager.saveTransaction(transaction);
				logger.info("insert:ok");
			} catch (Exception e) {
				e.printStackTrace();
				logger.info(e.getMessage());
			}
		} else {
			logger.info("no insert");
			System.out.println(false);
		}
		
	}

	
}

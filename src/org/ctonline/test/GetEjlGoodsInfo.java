package org.ctonline.test;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.json.JSONObject;

import org.ctonline.analysis.Batch;
import org.ctonline.analysis.Sample;
import org.ctonline.common.Solr;
import org.ctonline.po.goods.CtGoods;
import org.ctonline.po.goods.CtGoodsAttribute;
import org.ctonline.po.goods.CtGoodsAttributeRelation;
import org.ctonline.po.goods.CtGoodsBrand;
import org.ctonline.po.goods.CtGoodsCategory;
import org.ctonline.po.goods.CtGoodsType;
import org.ctonline.po.goods.CtRangePrice;
import org.ctonline.service.EJLInfoServiceImplDelegate;
import org.ctonline.service.EJLInfoServiceImplService;
import org.ctonline.service.JdpCtegoItem;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class GetEjlGoodsInfo extends RanInit implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		logger.info("读取数据开始");
		EJLInfoServiceImplDelegate delegate = new EJLInfoServiceImplService().getEJLInfoServiceImplPort();
		List<JdpCtegoItem> list = delegate.findJdpItemByType(0);
		for (int i = 0; i < list.size(); i++) {
			String jsonStr = list.get(i).getJdpResponse();
			jsonStr = analysisJson(jsonStr);
			//list.get(i).setJdpResponse(jsonStr);
			//list.get(i).setModified(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			list.get(i).setChanged(2);
			delegate.updateJdpChanged(list.get(i));
		}
		logger.info("数据结束");
	}
//	public static void main(String[] args) throws Exception {
////		CtGoods ctGoods = goodsManager.getGood(10414L);
////		ctGoods.setGNum(10L);
////		Solr solr = new Solr();
////		goodsManager.merge(ctGoods);
////		solr.SolrjUpdate();
//		
////		EJLInfoServiceImplDelegate delegate = new EJLInfoServiceImplService().getEJLInfoServiceImplPort();
////		List<JdpCtegoItem> list = delegate.findJdpItemByType(2);
////		for (int i = 0; i < list.size(); i++) {
////			String jsonStr = list.get(i).getJdpResponse();
////			jsonStr = analysisJson(jsonStr);
////			//list.get(i).setJdpResponse(jsonStr);
////			//list.get(i).setModified(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
////			list.get(i).setChanged(1);
////			delegate.updateJdpChanged(list.get(i));
////		}
//		String j = "{\"category_name\":\"电感/继电器|贴片电感\",\"search_key\":\"68uH ±20999999 贴片电感 4030 电感 SWPA4030S681MT 绕线电感 电感/继电器\",\"created\":\"2016-08-11 01:43:37\",\"batch\":{\"realm1\":{\"min\":2000,\"max\":\" \",\"step\":2000,\"value\":0.253}},\"sale_price\":0,\"sample\":{\"realm3\":{\"min\":1100,\"max\":1900,\"step\":100,\"value\":0.276},\"realm2\":{\"min\":150,\"max\":1000,\"step\":50,\"value\":0.308},\"realm1\":{\"min\":20,\"max\":100,\"step\":20,\"value\":0.46}},\"promotion_price\":0,\"unit_name\":\"PCS\",\"seller_outer_no\":\"SWPA4030S681MT\",\"modified\":\"2016-08-11 01:43:37\",\"market_price\":0,\"short_desc\":\"68uH 4030 ±20% 贴片\",\"seller_nick\":\"长亭易购\",\"sku_unit\":{\"盒\":2000},\"custom_props\":{\"电感量\":\"68uH\",\"品牌ID\":\"顺络Sunlord\",\"系列\":\"SWPA\",\"材质\":\"绕线\",\"精度\":\"±20%\",\"安装方式\":\"贴片\",\"是否偏料\":0,\"封装规格\":\"4030\",\"工作温度范围\":\"-40℃~+125℃\",\"是否样品\":1,\"容差\":\"±20%\"}}";
//////		String j = "{\"category_name\":\"电容|贴片电容MLCC\",\"search_key\":\"  高压 150 1000V -55~+125ºC 村田Murata 15pF 1206 否 贴片 1206(3216公制) U2J 是 ±5%\",\"created\":\"2016-10-01 03:52:54\",\"sale_price\":0,\"promotion_price\":0,\"unit_name\":\"pcs\",\"seller_outer_no\":\"GCM31A7U3A150JX01J\",\"market_price\":0,\"modified\":\"2016-10-01 03:52:54\",\"shortDesc\":\"1206 15pF U2J ±5% 1000V\",\"custom_props\":{\"特性\":\"高压\",\"容值代码\":\"150\",\"耐压值\":\"1000V\",\"工作温度范围\":\"-55~+125ºC\",\"品牌ID\":\"村田Murata\",\"标称容值\":\"15pF\",\"系列\":\"1206\",\"是否偏料\":\"否\",\"安装方式\":\"贴片\",\"封装规格\":\"1206(3216公制)\",\"成分\":\"U2J\",\"是否样品\":\"是\",\"容差\":\"±5%\"},\"sku_unit\":{\"盘\":\"10000\"},\"seller_nick\":\"长亭易购\"}";
//		analysisJson(j);
//	}
	public static String analysisJson(String json) {
		try {
//			String jsonStr = "{\"category_name\":\"保险丝|贴片保险丝\",\"search_key\":\"   5 x 20 mm 可更换 5 x 20 mm 250V 1.25A 力特 样品 \",\"created\":\"2016-08-09 11:15:31.0\",\"batch\":{\"realm1\":{\"min\":1000,\"max\":\" \",\"step\":1,\"value\":\"1.01\"}},\"sample\":{\"realm3\":{\"min\":120,\"max\":980,\"step\":20,\"value\":\"1.09\"},\"realm2\":{\"min\":20,\"max\":100,\"step\":10,\"value\":\"1.34\"},\"realm1\":{\"min\":1,\"max\":10,\"step\":1,\"value\":\"2.1\"}},\"sale_price\":0,\"promotion_price\":0,\"unit_name\":\"PCS\",\"sku_no\":\"02181.25MXBP\",\"seller_outer_no\":\"02181.25MXBP\",\"modified\":\"2016-08-09 11:22:34\",\"market_price\":0,\"seller_nick\":\"长亭易购\",\"custom_props\":{\"电流\":\"1.25A\",\"品牌ID\":\"力特\",\"系列\":\"5 x 20 mm\",\"电压\":\"250V\",\"安装方式\":\"可更换\",\"是否偏料\":0,\"封装规格\":\"5 x 20 mm\",\"是否样品\":1},\"sku_unit\":{\"袋\":1000},\"changed\":1}";
//			String jsonStr = "{\"category_name\":\"电感/继电器|贴片电感\",\"search_key\":\"3.3uH ±20999999 贴片电感 5020 电感 SWPA5020S3R3MT 绕线电感 电感/继电器\",\"created\":\"2016-08-10 16:37:54\",\"batch\":{\"realm1\":{\"min\":3000,\"max\":\" \",\"step\":3000,\"value\":\"0.33\"}},\"sample\":{\"realm3\":{\"min\":1100,\"max\":2400,\"step\":100,\"value\":\"0.36\"},\"realm2\":{\"min\":150,\"max\":1000,\"step\":50,\"value\":\"0.402\"},\"realm1\":{\"min\":10,\"max\":100,\"step\":10,\"value\":\"0.6\"}},\"sale_price\":0,\"promotion_price\":0,\"unit_name\":\"PCS1\",\"seller_outer_no\":\"SWPA5020S3R3MT\",\"modified\":\"2016-08-10 16:37:54\",\"market_price\":0,\"short_desc\":\"3.3uH 5020 ±20% 贴片\",\"seller_nick\":\"长亭易购\",\"custom_props\":{\"电感量\":\"3.3uH\",\"品牌ID\":\"顺络Sunlord\",\"系列\":\"SWPA\",\"材质\":\"绕线\",\"精度\":\"±20%\",\"安装方式\":\"贴片\",\"是否偏料\":0,\"封装规格\":\"5020\",\"工作温度范围\":\"-40℃~+125℃\",\"是否样品\":1,\"容差\":\"±20%\"},\"sku_unit\":{\"盘\":2500},\"changed\":0}";
			String jsonStr = json;
			JSONObject jsonObject = JSONObject.fromObject(jsonStr);
			org.ctonline.analysis.JsonGoods goods = (org.ctonline.analysis.JsonGoods) JSONObject.toBean(jsonObject, org.ctonline.analysis.JsonGoods.class);
			
			CtGoods ctGoods = new CtGoods();
			if(true){
				//ctGoods = new CtGoods();
				//是否是修改操作
				if(true){
					//修改操作查询一条已经存在的数据
					List<CtGoods> g = goodsManager.findByGSn(goods.getSeller_outer_no());
					if(g != null && g.size() > 0){
						ctGoods = g.get(0);
					} else {
						ctGoods = new CtGoods();
						ctGoods.setGNum(0L);
					}
				}
				//ctGoods.setAlw(alw);
				String[] cidsName = goods.getCategory_name().split("\\|");
				CtGoodsCategory listc = (CtGoodsCategory) goodsManager.findByTypeName(cidsName[1],"CName","CtGoodsCategory",0);
				if(listc != null){
					ctGoods.setCId(listc.getCId());
				}
				ctGoods.setGName(goods.getShort_desc());
				
				ctGoods.setGSn(goods.getSeller_outer_no());
				ctGoods.setTname(goods.getSearch_key().replaceAll("　", " ").trim());
				ctGoods.setGUnit(goods.getUnit_name());
				//测试 -- 存放自定义属性
				ctGoods.setIsPartial("0");
				ctGoods.setIsSample("1");
				//测试结束
				ctGoods.setMarketPrice(Double.valueOf(goods.getMarket_price()));
				//ctGoods.setPack1(pack1)
				ctGoods.setPromotePrice(Double.valueOf(goods.getPromotion_price()));
				//ctGoods.setSection("");
				ctGoods.setShopPrice(Double.valueOf(goods.getSale_price()));
				
				Map<String, Integer> mapSort = goods.getSku_unit();
				
				//这里将map.entrySet()转换成list
				List<Map.Entry<String,Integer>> list = new ArrayList<Map.Entry<String,Integer>>(mapSort.entrySet());
				//然后通过比较器来实现排序
				Collections.sort(list,new Comparator<Map.Entry<String,Integer>>() {
					//升序排序
					public int compare(Entry<String, Integer> o1,
							Entry<String, Integer> o2) {
						return o1.getValue().compareTo(o2.getValue());
					}
					
				});
				
				
				mapSort = sortMapByValue(mapSort);
				List<String> paiXuMap = new ArrayList<String>();
				for (String unitName : mapSort.keySet()) {
					paiXuMap.add(unitName+"__" + mapSort.get(unitName));
				}
				if(paiXuMap.size() > 0){
					if(paiXuMap.size() == 2){
						ctGoods.setPack2(paiXuMap.get(0).split("__")[0]);
						ctGoods.setPack2Num(Integer.valueOf(paiXuMap.get(0).split("__")[1]));
						ctGoods.setPack1(paiXuMap.get(1).split("__")[0]);
						ctGoods.setPack1Num(Integer.valueOf(paiXuMap.get(1).split("__")[1]));
					}
					if(paiXuMap.size() == 3){
						ctGoods.setPack2(paiXuMap.get(1).split("__")[0]);
						ctGoods.setPack2Num(Integer.valueOf(paiXuMap.get(1).split("__")[1]));
						ctGoods.setPack3(paiXuMap.get(0).split("__")[0]);
						ctGoods.setPack3Num(Integer.valueOf(paiXuMap.get(0).split("__")[1]));
						ctGoods.setPack1(paiXuMap.get(2).split("__")[0]);
						ctGoods.setPack1Num(Integer.valueOf(paiXuMap.get(2).split("__")[1]));
					}
					if(paiXuMap.size() == 1){
						ctGoods.setPack1(paiXuMap.get(0).split("__")[0]);
						ctGoods.setPack1Num(Integer.valueOf(paiXuMap.get(0).split("__")[1]));
					}
				}
				for (int i = 0; i < paiXuMap.size(); i++) {
					System.out.println(paiXuMap.get(i));
				}
				ctGoods.setIsDel("0");
				ctGoods.setAlw(" ");
				ctGoods.setSection(" ");
				ctGoods.setSeries(" ");
				
				ctGoods.setTokenCredit(0L);
				ctGoods.setGiveCredit(0L);
				ctGoods.setUName(goods.getShort_desc());
				ctGoods.setIsOnSale("1");
				ctGoods = (CtGoods)goodsManager.merge(ctGoods);
				//查询分类类别
				List<CtGoodsType> listt = (List<CtGoodsType>) goodsManager.findByTypeName("435","GTId","CtGoodsType",1);
				CtGoodsAttributeRelation attributeRelation = new CtGoodsAttributeRelation();
				for (String zidingValue : goods.getCustom_props().keySet()) {
					
					
					List<CtGoodsAttribute> listattr = (List<CtGoodsAttribute>) goodsManager.findByTypeName(zidingValue,"attrName","CtGoodsAttribute",1);
					if(listattr.size() > 0){
						if(true){
							Integer gtid  = 0;
							if(listt.size() > 0){
								gtid = listt.get(0).getGTId().intValue();
							} else {
								gtid=435;
							}
							boolean isZiDingyiOk = false;
							
							for (int i = 0; i < listattr.size(); i++) {
								//判断是否有已经存在的attrid
								if(listattr.get(i).getGTId().equals(gtid.longValue())){
									attributeRelation = new CtGoodsAttributeRelation();
									Integer attrId = listattr.get(i).getAttrId().intValue();
									//是否是已经添加过的属性
									String values= attrId + ","+ctGoods.getGId();
									String keys = "attrId,GId";
									attributeRelation = (CtGoodsAttributeRelation) goodsManager.findByTypeName(values,keys,"CtGoodsAttributeRelation",0);
									if(attributeRelation == null){
										attributeRelation = new CtGoodsAttributeRelation();
									}
									if(goods.getCustom_props().get(zidingValue).equals(attributeRelation.getAttrValue())){
										isZiDingyiOk = true;
										continue;
									}
									String vv = goods.getCustom_props().get(zidingValue);
									if(zidingValue.equals("工作温度范围")){
										vv = vv.replaceAll("º", "°");
									}
									attributeRelation.setAttrId(attrId.longValue());
									attributeRelation.setAttrValue(vv);
									attributeRelation.setGId(ctGoods.getGId());
									attributeRelation.setTag("0");
									goodsManager.merge(attributeRelation);
									isZiDingyiOk = true;
								}
							}
							//如果没有在已经存在的属性中，就新增一个这个属性
							if(!isZiDingyiOk){
								List<CtGoodsAttribute> listattrs = (List<CtGoodsAttribute>) goodsManager.findByTypeName(gtid.toString(),"GTId","CtGoodsAttribute",1);
								int sors = listattrs.size() + 1;
								CtGoodsAttribute attribute = new CtGoodsAttribute();
								attribute.setGTId(gtid.longValue());
								attribute.setAttrName(zidingValue);
								attribute.setSortOrder(Short.valueOf(sors+""));
								attribute = (CtGoodsAttribute) goodsManager.merge(attribute);
								
								attributeRelation = new CtGoodsAttributeRelation();
								Integer attrId = attribute.getAttrId().intValue();
								attributeRelation.setAttrId(attrId.longValue());
								attributeRelation.setAttrValue(goods.getCustom_props().get(zidingValue));
								attributeRelation.setGId(ctGoods.getGId());
								attributeRelation.setTag("0");
								goodsManager.merge(attributeRelation);
							}
						} else {
							CtGoodsType goodsType = new CtGoodsType();
							List<CtGoodsType> l = (List<CtGoodsType>) goodsManager.findByTypeName(cidsName[0],"typeName","CtGoodsType",1);
							goodsType = l.size() > 0 ? l.get(0): new CtGoodsType();
							goodsType.setTypeName(cidsName[0]);
							goodsType.setEnabled("1");
							CtGoodsType goodsTypeNew = (CtGoodsType) goodsManager.merge(goodsType);
							
							List<CtGoodsAttribute> listattrs = (List<CtGoodsAttribute>) goodsManager.findByTypeName(goodsTypeNew.getGTId().toString(), "GTId","CtGoodsAttribute",1);
							//得到最新的排序数字
							int sors = listattrs.size() + 1;
							CtGoodsAttribute attribute = new CtGoodsAttribute();
							attribute.setGTId(goodsTypeNew.getGTId());
							attribute.setAttrName(zidingValue);
							attribute.setSortOrder(Short.valueOf(sors+""));
							attribute = (CtGoodsAttribute) goodsManager.merge(attribute);
							
							
							attributeRelation = new CtGoodsAttributeRelation();
							Integer attrId = attribute.getAttrId().intValue();
							attributeRelation.setAttrId(attrId.longValue());
							attributeRelation.setAttrValue(goods.getCustom_props().get(zidingValue));
							attributeRelation.setGId(ctGoods.getGId());
							attributeRelation.setTag("0");
							
							goodsManager.merge(attributeRelation);
//							for (int i = 0; i < listattr.size(); i++) {
//								if(listattr.get(i).getGTId().equals(goodsTypeNew.getGTId())){
//								}
//							}
						}
					} else {
						//判断是否是自定义属性之外的属性
						if(zidingValue.equals("品牌ID") || zidingValue.equals("是否偏料") || zidingValue.equals("是否样品") || zidingValue.equals("容差") || zidingValue.equals("分段计价") || zidingValue.equals("系列") || zidingValue.equals("通用名称")){
							if(zidingValue.equals("容差")){
								ctGoods.setAlw(goods.getCustom_props().get(zidingValue));
							}
							if(zidingValue.equals("通用名称")){
								ctGoods.setUName(goods.getCustom_props().get(zidingValue));
							}
							if(zidingValue.equals("分段计价")){
								ctGoods.setSection(goods.getCustom_props().get(zidingValue));
							}
							if(zidingValue.equals("系列")){
								ctGoods.setSeries(goods.getCustom_props().get(zidingValue));
							}
							if(zidingValue.equals("品牌ID")){
								List<CtGoodsBrand> listb = (List<CtGoodsBrand>) goodsManager.findByTypeName(goods.getCustom_props().get(zidingValue),"BName","CtGoodsBrand",1);
								if(listb.size() > 0){
									ctGoods.setBId(listb.get(0).getBId());
								} else {
									CtGoodsBrand brand = new CtGoodsBrand();
									brand.setBLogo("/0913/logo1.png");
									brand.setBName(goods.getCustom_props().get(zidingValue) == null ? "品牌名称丢失" : goods.getCustom_props().get(zidingValue));
									brand.setBUrl("www.uniohm.com");
									brand.setIsShow(false);
									brand.setSortOrder(99);
									brand = (CtGoodsBrand) goodsManager.merge(brand);
									ctGoods.setBId(brand.getBId());
								}
								//ctGoods.setSeries(goods.getCustom_props().get(zidingValue));
							}
							if(zidingValue.equals("是否样品")){
								//ctGoods.setSeries(goods.getCustom_props().get(zidingValue));
								Object a = goods.getCustom_props().get(zidingValue);
								System.out.println(a);
								ctGoods.setIsSample(a.toString());
							}
							if(zidingValue.equals("是否偏料")){
								Object a = goods.getCustom_props().get(zidingValue);
								System.out.println(a);
								ctGoods.setIsPartial(a.toString());
//								ctGoods.setSeries(goods.getCustom_props().get(zidingValue));
							}
						} else {
							if(true){
								Integer gtid  = 0;
								if(listt.size() > 0){
									gtid = listt.get(0).getGTId().intValue();
								} else {
									gtid=435;
								}
								
								List<CtGoodsAttribute> listattrs = (List<CtGoodsAttribute>) goodsManager.findByTypeName(gtid.toString(), "GTId","CtGoodsAttribute",1);
								//得到最新的排序数字
								int sors = listattrs.size() + 1;
								CtGoodsAttribute attribute = new CtGoodsAttribute();
								attribute.setGTId(gtid.longValue());
								attribute.setAttrName(zidingValue);
								attribute.setSortOrder(Short.valueOf(sors+""));
								attribute = (CtGoodsAttribute) goodsManager.merge(attribute);
								
								attributeRelation = new CtGoodsAttributeRelation();
								Integer attrId = attribute.getAttrId().intValue();
								attributeRelation.setAttrId(attrId.longValue());
								attributeRelation.setAttrValue(goods.getCustom_props().get(zidingValue));
								attributeRelation.setGId(ctGoods.getGId());
								attributeRelation.setTag("0");
								goodsManager.merge(attributeRelation);
							} else {
								//在没有分类的时候进行新增类别
								CtGoodsType goodsType = new CtGoodsType();
								List<CtGoodsType> l = (List<CtGoodsType>) goodsManager.findByTypeName(cidsName[0],"typeName","CtGoodsType",1);
								goodsType = l.size() > 0 ? l.get(0): new CtGoodsType();
								goodsType.setTypeName(cidsName[0]);
								goodsType.setEnabled("1");
								CtGoodsType goodsTypeNew = (CtGoodsType) goodsManager.merge(goodsType);
								
								List<CtGoodsAttribute> listattrs = (List<CtGoodsAttribute>) goodsManager.findByTypeName(goodsTypeNew.getGTId().toString(), "GTId","CtGoodsAttribute",1);
								int sors = listattrs.size() + 1;
								CtGoodsAttribute attribute = new CtGoodsAttribute();
								attribute.setGTId(goodsTypeNew.getGTId());
								attribute.setAttrName(zidingValue);
								attribute.setSortOrder(Short.valueOf(sors+""));
								attribute = (CtGoodsAttribute) goodsManager.merge(attribute);
								
								attributeRelation = new CtGoodsAttributeRelation();
								Integer attrId = attribute.getAttrId().intValue();
								attributeRelation.setAttrId(attrId.longValue());
								attributeRelation.setAttrValue(goods.getCustom_props().get(zidingValue));
								attributeRelation.setGId(ctGoods.getGId());
								attributeRelation.setTag("0");
								goodsManager.merge(attributeRelation);
							}
						}
					}
					System.out.println(zidingValue);
				}
				
				
				ctGoods = (CtGoods) goodsManager.merge(ctGoods);
				//添加价格区间
				List<CtRangePrice> listRan = new ArrayList<CtRangePrice>();
				List<CtRangePrice> listRanYuan = (List<CtRangePrice>) goodsManager.findByTypeName(ctGoods.getGId().toString(),"GId","CtRangePrice",1);
				CtRangePrice rangePrice = new CtRangePrice();
				CtRangePrice rangePrice1 = new CtRangePrice();
				CtRangePrice rangePrice2 = new CtRangePrice();
				
				Sample sample = goods.getSample();
				Batch batch = goods.getBatch();
				if(sample != null && batch != null){
//					if(listRanYuan != null && listRanYuan.size() > 0){
//						for (int i = 0; i < listRanYuan.size(); i++) {
//							
//						}
//					}
					goodsManager.deleteRanPriceByGid(ctGoods.getGId());
					
					rangePrice.setGId(ctGoods.getGId());
						if(sample.getRealm1() != null || batch.getRealm1() != null){
							//样品
							rangePrice.setInsTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
							rangePrice.setSimENum(isnull(sample.getRealm1()) ? sample.getRealm1().getMax(): null);
							rangePrice.setSimIncrease(isnull(sample.getRealm1()) ? sample.getRealm1().getStep():null);
							rangePrice.setSimRPrice(isnull(sample.getRealm1()) ? Double.valueOf(sample.getRealm1().getValue()) : null);
							rangePrice.setSimSNum(isnull(sample.getRealm1()) ? sample.getRealm1().getMin() : null);
							
							//批量
							rangePrice.setParENum(isnull(batch.getRealm1())? batch.getRealm1().getMax() : null);
							rangePrice.setParIncrease(isnull(batch.getRealm1()) ? batch.getRealm1().getStep():null);
							rangePrice.setParRPrice(isnull(batch.getRealm1()) ? Double.valueOf(batch.getRealm1().getValue()) : null);
							rangePrice.setParSNum(isnull(batch.getRealm1()) ? batch.getRealm1().getMin() : null);
							
							listRan.add(rangePrice);
						}
						
						if(sample.getRealm2() != null || batch.getRealm2() != null){
							//样品
							
							rangePrice1.setGId(ctGoods.getGId());
							rangePrice1.setInsTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
							rangePrice1.setSimENum(isnull(sample.getRealm2()) ? sample.getRealm2().getMax(): null);
							rangePrice1.setSimIncrease(isnull(sample.getRealm2()) ? sample.getRealm2().getStep():null);
							rangePrice1.setSimRPrice(isnull(sample.getRealm2()) ? Double.valueOf(sample.getRealm2().getValue()) : null);
							rangePrice1.setSimSNum(isnull(sample.getRealm2()) ? sample.getRealm2().getMin() : null);
							
							//批量
							rangePrice1.setParENum(isnull(batch.getRealm2())? batch.getRealm2().getMax() : null);
							rangePrice1.setParIncrease(isnull(batch.getRealm2()) ? batch.getRealm2().getStep():null);
							rangePrice1.setParRPrice(isnull(batch.getRealm2()) ? Double.valueOf(batch.getRealm2().getValue()) : null);
							rangePrice1.setParSNum(isnull(batch.getRealm2()) ? batch.getRealm2().getMin() : null);
							listRan.add(rangePrice1);
						}
						
						if(sample.getRealm3() != null || batch.getRealm3() != null){
							//样品
							
							rangePrice2.setGId(ctGoods.getGId());
							rangePrice2.setInsTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
							rangePrice2.setSimENum(isnull(sample.getRealm3()) ? sample.getRealm3().getMax(): null);
							rangePrice2.setSimIncrease(isnull(sample.getRealm3()) ? sample.getRealm3().getStep():null);
							rangePrice2.setSimRPrice(isnull(sample.getRealm3()) ? Double.valueOf(sample.getRealm3().getValue()) : null);
							rangePrice2.setSimSNum(isnull(sample.getRealm3()) ? sample.getRealm3().getMin() : null);
							
							//批量
							rangePrice2.setParENum(isnull(batch.getRealm3())? batch.getRealm3().getMax() : null);
							rangePrice2.setParIncrease(isnull(batch.getRealm3()) ? batch.getRealm3().getStep():null);
							rangePrice2.setParRPrice(isnull(batch.getRealm3()) ? Double.valueOf(batch.getRealm3().getValue()) : null);
							rangePrice2.setParSNum(isnull(batch.getRealm3()) ? batch.getRealm3().getMin() : null);
							listRan.add(rangePrice2);
						}
						
						for (int i = 0; i < listRan.size(); i++) {
							goodsManager.merge(listRan.get(i));
						}
				}
				System.out.println(goods);
//				jsonObject = JSONObject.fromObject(goods);
//				
//				JSONObject object = jsonObject.getJSONObject("batch");
//				System.out.println(object.get("realm2"));
//				if(object.get("realm2").toString().equals("null")){
//					object.remove("realm2");
//				}
//				if(object.get("realm3").toString().equals("null")){
//					object.remove("realm3");
//				}
//				
//				JSONObject o1 = object.getJSONObject("realm1");
//				if(!o1.toString().equals("null") && o1.get("max").toString().equals("0")){
//					o1.put("max", " ");
//				}
//				object.containsValue(o1);
//				JSONObject o2 = object.getJSONObject("realm2");
//				if(!o2.toString().equals("null") &&o2.get("max").toString().equals("0")){
//					o2.put("max", " ");
//				}
//				object.containsValue(o2);
//				JSONObject o3 = object.getJSONObject("realm3");
//				if(!o3.toString().equals("null") &&o3.get("max").toString().equals("0")){
//					o3.put("max", " ");
//				}
//				object.containsValue(o3);
//				
//				//修改json时间
//				//jsonObject.put("modified", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
//				jsonObject.containsValue(object);
//				System.out.println();
//				System.out.println();
//				System.out.println();
//				System.out.println(jsonObject);
				return jsonObject.toString();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static boolean isnull(Object obj){
		if(obj == null || obj.equals(" ")){
			return false;
		} 
		return true;
	}
	
	public static Map<String, Integer> sortMapByValue(Map<String, Integer> oriMap) {  
	    Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();  
	    if (oriMap != null && !oriMap.isEmpty()) {  
	        List<Map.Entry<String, Integer>> entryList = new ArrayList<Map.Entry<String, Integer>>(oriMap.entrySet());  
	        Collections.sort(entryList,  
	                new Comparator<Map.Entry<String, Integer>>() {  
	                    public int compare(Entry<String, Integer> entry1,  
	                            Entry<String, Integer> entry2) {  
	                        int value1 = 0, value2 = 0;  
	                        try {  
	                            value1 = getInt(entry1.getValue().toString());  
	                            value2 = getInt(entry2.getValue().toString());  
	                        } catch (NumberFormatException e) {  
	                            value1 = 0;  
	                            value2 = 0;  
	                        }  
	                        return value2 - value1;  
	                    }  
	                });  
	        Iterator<Map.Entry<String, Integer>> iter = entryList.iterator();  
	        Map.Entry<String, Integer> tmpEntry = null;  
	        while (iter.hasNext()) {  
	            tmpEntry = iter.next();  
	            sortedMap.put(tmpEntry.getKey(), tmpEntry.getValue());  
	        }  
	    }  
	    return sortedMap;  
	}  
	private static int getInt(String str) {  
	    int i = 0;  
	    try {  
	        Pattern p = Pattern.compile("^\\d+");  
	        Matcher m = p.matcher(str);  
	        if (m.find()) {  
	            i = Integer.valueOf(m.group());  
	        }  
	    } catch (NumberFormatException e) {  
	        e.printStackTrace();  
	    }  
	    return i;  
	} 
}

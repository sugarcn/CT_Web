<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="common/basePath.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<script type="text/javascript" src="js/jquery.lazyload.js"></script>

<script type="text/javascript">
$(function(){ 

	$("#searchSelected").click(function(){ 
		$("#searchTab").show();
		$(this).addClass("searchOpen");
	}); 

	$("#searchTab li").hover(function(){
		$(this).addClass("selected");
	},function(){
		$(this).removeClass("selected");
	});
	 
	$("#searchTab li").click(function(){
		$("#searchSelected").html($(this).html());
		$("#searchTab").hide();
		$("#searchSelected").removeClass("searchOpen");
	});
});
</script>
<script type="text/javascript">

		//$(function(){
		//	$('#marquee4').kxbdMarquee({direction:'up',isEqual:false});
		//});
			$(function(){
		$(window).scroll(function(){
			if($(window).scrollTop()>100){
				$("#side-bar .gotop").fadeIn();	
			}
			else{
				$("#side-bar .gotop").hide();
			}
		});
		$("#side-bar .gotop").click(function(){
			$('html,body').animate({'scrollTop':0},500);
		});
	});
	function selectFrom(lowerValue, upperValue){ 

//取值范围总数 
	var choices = upperValue - lowerValue + 1; 
	return Math.floor(Math.random() * choices + lowerValue); 
	}

	</script>
<script src="js/scriptindexlist.js" type="text/javascript"></script>
</head>
<body>

<jsp:include page="common/headold.jsp"></jsp:include>
<div class="index_ct">
	<ul class="tin_left">
        <li><a href="<%=basePath%>goods_brandDetail?fgoodsDTO.BId=1" target="_blank"><img src="<%=imgurl %>/bg/ct_pp1.gif"></a></li>
        <li><a href="<%=basePath%>goods_brandDetail?fgoodsDTO.BId=3" target="_blank"><img src="<%=imgurl %>/bg/ct_pp2.gif"></a></li>
        <li><a href="<%=basePath%>goods_brandDetail?fgoodsDTO.BId=2" target="_blank"><img src="<%=imgurl %>/bg/ct_pp3.gif"></a></li>
<div class="notice">
<c:if test="${userinfosession.UUserid != null}">
	<div class="yesloginpos">您好！${userinfosession.UUsername }<span>进入<a href="<%=basePath %>user_userinfo">个人中心</a></span></div>
</c:if>
<c:if test="${userinfosession.UUserid == null}">
	<div class="yeslogin"><a href="login_login" class="login-a">登录</a><a href="login_goRegister" target="_blank">注册</a></div>
</c:if>
    <dl class="tin_th">
<dt id=gonggao><h3><a href="list_notice" target="_blank">特别公告</a></h3></dt>
<c:forEach items="${noticesList }" var="list">
	<dd><a href="find_notice?noticeDTO.noId=${list.noId }" target="_blank">${list.noTitle }</a></dd>
</c:forEach> 
    </dl>
    <ul class="ulgua">
<li class="ulguaa"><span>专业配送</span></li>
<li class="ulguab"><span>安全支付</span></li>
<li class="ulguac"><span>品质保证</span></li>
    </ul>
	<div class="tin_mid">
        <div class="tin_tu">
           	<div class="box">	
		<div class="pic">
			<ul>
				<li style="background:url(<%=imgurl %>/bg1.jpg) center">
					<a href="<%=basePath%>login_goRegister"><img class="img1" src="<%=imgurl %>/text1.png" width="684" height="223" />
					<img class="img2" src="<%=imgurl %>/con1.png"  width="684" height="223"/></a>
				</li>
				<li style="background:url(<%=imgurl %>/bg2.jpg) center">
					<a href="<%=basePath%>sx.jsp" target="_blank"><img class="img1" src="<%=imgurl %>/text2.png"  width="684" height="223" />
					<img class="img2" src="<%=imgurl %>/con2.png"  width="684" height="223"/></a>
				</li>
				<li style="background:url(<%=imgurl %>/bg3.jpg) center">
					<a href="<%=basePath%>sx.jsp" target="_blank"><img class="img1" src="<%=imgurl %>/text3.png"  width="684" height="223" />
					<img class="img2" src="<%=imgurl %>/con3.png"  width="684" height="223"/></a>
				</li>				
			</ul>
		</div>
		<div class="nave">
			<ul>
				<li class="bg"></li>
				<li></li>
				<li></li>				
			</ul>
		</div>
	</div>
    	</div>
        <c:forEach items="${bodyCareMap }" var="list">
	        <dl class="tin_th">
	            <dt><i>[<a href="javascript:;" onclick="getQuEncode('${list.key }')">更多</a>]</i><h3>${list.key }</h3></dt>
	            <dd>
	            <c:forEach items="${list.value }" var="list1">
		            <a href="javascript:;" onclick="getQuEncode('${list1.CName }')">${list1.CName }</a><span class="ct_line_t"></span>
	            </c:forEach>
	    	</dl>
        </c:forEach>
    </div>
	<div class="tin_right">
        
         
         <c:if test="${userinfosession.UUserid != null}">
	         <div class="in_dengluhou"><span>进入<a href="<%=basePath %>user_userinfo">个人中心</a></span>您好！${userinfosession.UUsername }</div>
         </c:if>
         <c:if test="${userinfosession.UUserid == null}">
	    	 <ul class="in_denglu">
	        <li class="de_nglu"><a href="login_login">登录</a></li>
	        <li><a href="login_goRegister">注册</a></li>
	        </ul>
         </c:if>
         
         
         
        <dl class="tin_th">
            <dt id=gonggao><i>[<a href="list_notice" target="_blank">更多</a>]</i><h3>特别公告</h3></dt>
            <c:forEach items="${noticesList }" var="list">
	            <dd><a href="find_notice?noticeDTO.noId=${list.noId }" target="_blank">${list.noTitle }</a></dd>
            </c:forEach>
    	</dl>
        <dl class="tin_tr">
            <dt><h3>晒单专区</h3></dt>
            <dd>
                <div class="sdlunbo">
                   <div id="marquee4">
            <ul>
            	<c:forEach items="${evaList }" var="list">
	                <li><a href="goods_detail?gid=${list.orderId }" target="_blank">${list.ctUser.nameTemp } ${list.evaDesc }</a></li>
            	</c:forEach>
            	
            </ul>
        </div>
                </div>           
            </dd>
    	</dl>
    </div>
<script type="text/javascript">
//随机Q
		var qqs = ["2880608370","2881112887","2881112904"]; 
	var qq = qqs[selectFrom(0, qqs.length-1)]; 
	$(".qqrandom").html("<a target=blank href='tencent://message/?uin="+qq+"&Site=qq&Menu=yes'><img src='<%=imgurl %>/ct_qq.png'></a>");
	function loadSanjiFenlei(){
		$.post(
			"getSanJiFenLei",
			function(data,varstart){
				var str = "";
				for(var i = 0; i<data.length; i++){
					str += "<li><a href='javascript:;' onclick=\"getQuEncode('"+data[i].CName+"')\" >"+data[i].CName+"</a></li>";
				}
				$("#dianzu").append(str);
			}
		);
	}
	loadSanjiFenlei()
</script>
<div class="ct_dian">
    <div class="ct_d_tit"><i>[<a href="javascript:;" onclick="getQuEncode('电阻')">更多</a>]</i><h2>电阻</h2></div>
    <ul class="ct_siderl" id="dianzu">
    </ul>
    <div class="ct_siderr">
    <div class="ct_shp_shuang">
<div class="pro-switch">
		<div class="slider">
			<div class="flexslider">
				<ul class="slides">
					<li>
						<div class="img"><a href="goods_detail?gid=223" target="_blank"><img src="<%=imgurl %>/dz14.gif" height="219" width"203" alt="" /></a></div>
					</li>
					<li>
						<div class="img"><a href="goods_detail?gid=225" target="_blank"><img src="<%=imgurl %>/dz15.gif" height="219" width"203" alt="" /></a></div>
					</li>
					<li>
						<div class="img"><a href="goods_detail?gid=245" target="_blank"><img src="<%=imgurl %>/dz16.gif" height="219" width"203" alt="" /></a></div>
					</li>
				</ul>
			</div>
		</div>
	</div>
        </div>
        <c:forEach items="${goodsListZu }" var="list">
	    	<div class="ct_shp">
				<dl onmouseover="MM_over(this)" onmouseout="MM_out(this)">
					<dd class="r_pic"><a href="goods_detail?gid=${list.GId }" target="_blank"><img src="${list.goodsImg.PUrl }" /></a>
					<span><div style="display:none; "><a href="goods_detail?gid=${list.GId }" target="_blank" >购买</a></div></span></dd>
	            	<dt><a href="goods_detail?gid=${list.GId }" target="_blank">${list.GName }</a></dt>
	                <dd class="l_gou">
	                	<ul>
	                	<li>原厂料号：</li>
	                    <li>${list.GSn }</li>
	                    </ul>
					</dd>
	                <dd class="l_gou">   
	                	<ul>
	                	<li>品牌：${list.pack4 }</li>
	                    </ul>	                    
					</dd>
	            </dl>
	        </div>
        </c:forEach>
    </div>
</div>
<script type="text/javascript">
	function loadSanjiFenleiDianRong(){
		$.post("loadSanjiFenleiDianRong",
				function(data,varstart){
				var str = "";
				for(var i = 0; i<data.length; i++){
					str += "<li><a href='javascript:;' onclick=\"getQuEncode('"+data[i].CName+"')\">"+data[i].CName+"</a></li>";
				}
				$("#dianrong").append(str);
			}
		);
	}
	loadSanjiFenleiDianRong();
</script>
<div class="ct_dian">
    <div class="ct_r_tit"><i>[<a href="javascript:;" onclick="getQuEncode('电容')">更多</a>]</i><h2>电容</h2></div>
    <ul class="ct_siderl ct_siderl_r" id="dianrong">
    </ul>
    <div class="ct_siderr">
            <div class="ct_shp_shuang">
    <div class="pro-switch">
            <div class="slider">
                <div class="flexslider">
                    <ul class="slides">
                        <li>
                            <div class="img"><a href="/" target="_blank"><img src="<%=imgurl %>/dr14.gif" height="219" width"203" alt="" /></a></div>
                        </li>
                        <li>
                            <div class="img"><a href="/" target="_blank"><img src="<%=imgurl %>/dr15.gif" height="219" width"203" alt="" /></a></div>
                        </li>
                        <li>
                            <div class="img"><a href="/" target="_blank"><img src="<%=imgurl %>/dr16.gif" height="219" width"203" alt="" /></a></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
            </div>
    	 <c:forEach items="${goodsListRong }" var="list">
	    	<div class="ct_shp">
				<dl onmouseover="MM_over(this)" onmouseout="MM_out(this)">
				<dd class="r_pic"><a href="goods_detail?gid=${list.GId }" target="_blank"><img src="${list.goodsImg.PUrl }" /></a>
				<span><div style="display:none; "><a href="goods_detail?gid=${list.GId }" target="_blank" >购买</a></div></span></dd>
	            	<dt><a href="goods_detail?gid=${list.GId }" target="_blank">${list.GName }</a></dt>
	                <dd class="l_gou">
	                	<ul>
	                	<li>原厂料号：</li>
	                    <li>${list.GSn }</li>
	                    <li><div style="display:none; "><a href="goods_detail?gid=${list.GId }" target="_blank" >购买</a></div></li>
	                    </ul>
					</dd>
					 <dd class="l_gou">   
	                	<ul>
	                	<li>品牌：${list.pack4 }</li>
	                    </ul>	                    
					</dd>
	            </dl>
	        </div>
        </c:forEach>
    </div>
</div>
<div class="clear"></div>
</div>
<jsp:include page="common/foot.jsp"></jsp:include>
	<script type="text/javascript">
		var i = -1;
		var time=0;
		junmper();
		function junmper(){
			i++;
			if(i>4)
			i=0;
			$(".nave ul li").eq(i).addClass("bg").siblings().removeClass("bg");
				$(".pic ul li").eq(i).fadeIn(100).siblings().fadeOut(100);
				$(".pic ul li").eq(i).find(".img1").css({left:"-760px"});
				$(".pic ul li").eq(i).find(".img2").css({display:"none",left:"-15px"});
				$(".pic ul li").eq(i).find(".img1").animate({left:"0px"},500,function(){
					$(".pic ul li").eq(i).find(".img2").css("display","block");
					$(".pic ul li").eq(i).find(".img2").animate({left:"0px"},500);
				});
	
		}
		time=setInterval("junmper()",3700);
		$(".nave ul li").click(function(){
			i=$(this).index();
			$(".nave ul li").eq(i).addClass("bg").siblings().removeClass("bg");
				$(".pic ul li").eq(i).fadeIn(100).siblings().fadeOut(100);
				$(".pic ul li").eq(i).find(".img1").css({left:"-760px"});
				$(".pic ul li").eq(i).find(".img2").css({display:"none",left:"-15px"});
				$(".pic ul li").eq(i).find(".img1").animate({left:"0px"},500,function(){
					$(".pic ul li").eq(i).find(".img2").css("display","block");
					$(".pic ul li").eq(i).find(".img2").animate({left:"0px"},500);
				});
	
		})
	$(".nave ul li").hover(function(){
		clearInterval(time);
	},function(){
		time=setInterval("junmper()",3700);
	})
	</script>
<script defer src="js/slider.js"></script> 
<script type="text/javascript">
    $(function(){
      $('.flexslider').flexslider({
        animation: "slide",
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
  </script>
</body>
</html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <base href="<%=basePath%>">
    
   <title>${systemInfo.ctTitle }</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	 <script type="text/javascript" src="<%=basePath %>js/qrcode.js"></script>
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
  </head>
  
  <body>
  
  <jsp:include page="../common/head.jsp"></jsp:include>
  <div style="width: 100%; text-align: center;" align="center" id="qrcode" class="weixin">
	  
	  <div class="wx_mid">
		  <dl>
		  <dd><span>应还款金额：<i>${orderInfowx.total }元</i></span>订单号：${orderInfowx.orderSn }</dd>
		  <dt>请您及时付款，以便订单尽快处理！</dt>
		  </dl>
			  <div class="wx_tu">
				  <div style="width: 100%; text-align: center;" align="center" id="img" class="wx_pic"></div>
				  <div class="wx_sm">
				  <div class="wx_sm_b">
					  <p>请用微信二维码扫一扫</p>
					  <p>扫描二维码支付</p>
				  </div>
				  </div>
				  <p class="wx_fan"><a href="javascript:history.go(-1);">选择其他支付方式</a><p>
			  </div>
	  </div>
	  <div class="clear"></div>
</div>
  
  
  <input type="hidden" id="weixinpayUrl" value="${weixinPayurl }" />
  <input type="hidden" id="weixinorderSn" value="${weixinorderSn }" />
<jsp:include page="../common/foot.jsp"></jsp:include>
  <script type="text/javascript">
  
  var jishu = 0;

	var wait=0; 
	getErweima();
	function getErweima(){
	var url = $("#weixinpayUrl").val();
	
	//参数1表示图像大小，取值范围1-10；参数2表示质量，取值范围'L','M','Q','H'
	var qr = qrcode(10, 'L');
	qr.addData(url);
	qr.make();
	var dom=document.getElementById("img");
	dom.innerHTML = qr.createImgTag();
	time();
}
	//time();
function time() { 
	if(jishu <= 100){
		if (wait == 3) {
		$.post(
			"FindOrderWeiXinIsOk",
			{"out_trade_no":$("#weixinorderSn").val()},
			function(data,varstart){
				if(data == "SUCCESS"){
					location.href="wxreturnOk_cre?out_trade_no="+$("#weixinorderSn").val();
					wait = 6; 
				} else {
					jishu++;
					wait = 0; 
					time();
				}
			}
		);
		} else { 
			wait++; 
			setTimeout(function() { 
			time();
			}, 
		1000) 
		} 
	} else {
		jishu=0;
		$.post(
			"againwxEr",
			{"out_trade_no":$("#weixinorderSn").val()},
			function(data,varstart){
				var url = data;
				
				//参数1表示图像大小，取值范围1-10；参数2表示质量，取值范围'L','M','Q','H'
				var qr = qrcode(10, 'L');
				qr.addData(url);
				qr.make();
				var dom=document.getElementById("img");
				dom.innerHTML = qr.createImgTag();
				time();
			}
		);
		
	}
} 
</script>
  </body>
</html>

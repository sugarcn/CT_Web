<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = "//"+request.getServerName()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'tenPayTest.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  	<form action="pay_ten" method="post">
	  	<table align="center">
	  		<tr>
	  			<th>商品价格</th>
	  			<td><input type="text" name="payDTO.total_fee" /></td>
	  		</tr>
	  		<tr>
	  			<th>订单编号</th>
	  			<td><input type="text" name="payDTO.out_trade_no" /></td>
	  		</tr>
	  		<tr>
	  			<th>订单名称</th>
	  			<td><input type="text" name="payDTO.subject" /></td>
	  		</tr>
	  		<tr>
	  			<th>备注信息</th>
	  			<td><input type="text" name="payDTO.pay_desc" /></td>
	  		</tr>
	  		<tr>
	  			<td colspan="2"><input type="submit" value="提交" /></td>
	  		</tr>
	  	</table>
	  	
  	</form>
  </body>
</html>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="frm" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <base href="<%=basePath%>">
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  
  <jsp:include page="../common/head.jsp"></jsp:include>
  <script type="text/javascript">
var type = 1;
function pay(){
	var total = $("#oldtotal").val();
	var cashId = $("#cashId").val();
	var orderSn = $("#orderSn").val();
	if(total != "0.00"){
		$.post(
			"findTimeOk",
			{"cashId":cashId,"out_trade_no":orderSn},
			function(data, varstart){
				if(data == "ok"){
					if(type == 1){
						updatePay(2);
						document.forms["alipaysubmit"].submit();
					} else if (type == 2){
						updatePay(5);
						var ten = $("#tenUrl").val();
						location.href=ten;
					} else if (type == 3){
						updatePay(3);
						$("#toweixin").submit();
					} else if (type == 4){
						updatePay(4);
						document.forms["chinapay"].submit();
					} else if (type == 5){
						//信用支付
						$.post(
							"findEDuXionYong",
							{"orderSn":orderSn},
							function(data, varstart){
								if(data == "1"){
									//余额充足
									$.post(
										"xinyongpaygo",
										{"orderSn":orderSn},
										function(data, varstart){
											if(data == "1"){
												//支付成功
												location.href="xinyongpayfinish?orderSn="+orderSn;
											}
										}
									);
								} else if(data == "-1") {
									//余额不足
									alert("您的信用额度余额不足，无法完成支付");
								} else {
									//登录信息不完整
								}
							}
						);
					} else if (type == 6){
						//线下支付
						location.href="order_toPay?orderDTO.orderSn="+orderSn;
					} else {
						updatePay(2);
						document.forms["alipaysubmit"].submit();
					}
				} else {
					alert("现金劵已过期");
					location.reload();
				}
			}
		);
	}
	//location.href=ten;
	
}
function updatePay(payType){
	var orderSn = $("#orderSn").val();
	$.post("updatePayZhuang",{"payDTO.payType":payType,"payDTO.out_trade_no":orderSn});
}
$(function(){
	$("#xy_zhifu").click(function(){
		$(this).addClass("hover").parent().parent().siblings("div").find("li").removeClass("hover");
	});
	$(".fu_xianxia ul li").click(function(){
		$(this).addClass("hover").parent().parent().siblings("div").find("li").removeClass("hover");
	});
	$(".fu_jishi ul li").click(function(){
		$(this).addClass("hover").parent().parent().siblings("div").find("li").removeClass("hover");
	    $(this).siblings("li").removeClass("hover");
	});
});

$(function(){
	$("#payTypeZhi").click(function(){
		type = 1;
		$("img").parent().removeClass("hover");
		$(this).parent().addClass("hover");
	});
	$("#payTypeTen").click(function(){
		type = 2;
		$("img").parent().removeClass("hover");
		$(this).parent().addClass("hover");
	});
	$("#payTypeWeixin").click(function(){
		type = 3;
		$("img").parent().removeClass("hover");
		$(this).parent().addClass("hover");
	});
	$("#payTypeyingLian").click(function(){
		type = 4;
		$("img").parent().removeClass("hover");
		$(this).parent().addClass("hover");
	});
	$("#xinyongPay").click(function(){
		type = 5;
		$("img").parent().removeClass("hover");
		$(this).parent().addClass("hover");
	});
	$("#xianxiapay").click(function(){
		type = 6;
		$("img").parent().removeClass("hover");
		$(this).parent().addClass("hover");
	});
	$("input[name=cashCoupon]").click(function(){
		var va = this.value;
		var chai = va.split("_");
		var total = $("#oldtotal").val();
		//满减卷
		if(chai[2] == "1"){
			$("#totalCount").text((Number(total) - Number(chai[1])).toFixed(2));

			//无限制卷
		} else if (chai[2] == "2"){
			if(Number(total) < Number(chai[1])){
				$("#totalCount").text("0.00");
			} else {
				$("#totalCount").text((Number(total) - Number(chai[1])).toFixed(2));
			}
		}
		//alert(chai[0]+"______"+chai[1]);
	});
	$("input[name=cashCoupon]").mouseover(function(){
		var va = this.value;
		var chai = va.split("_");
		var total = $("#oldtotal").val();
		if(chai[2] == "1"){
			if(Number(total) < Number(chai[0])){
				$(this).attr("disabled",true);
			}
		}
	});
});
//if(window.name!="hasLoad"){    
//    location.reload();    
//    window.name = "hasLoad";    
//}else{    
//    window.name="";    
//}    
</script>

  <div class="kuan_fu">
  <form action="getWeiXinImgUrl" id="toweixin" method="post">
		<input type="hidden" id="orderSn" name="out_trade_no" value="${payDTO.out_trade_no }" />
  </form>
	<div class="fu_min">
    	<div class="fu_zong">
        <div class="fu_dingdan">订单号：<i>${payDTO.out_trade_no }</i></div>
        <input type="hidden" id="oldtotal" value="${payDTO.total_fee }" />
        <div class="fu_zj">此次交易金额为：<i id="totalCount"><frm:formatNumber pattern="0.00">${payDTO.total_fee }</frm:formatNumber></i>元</div>
        </div>
        <c:if test="${payDTO.coupon != null }">
	        <div class="xjj">
	            <h3>现金券使用</h3>
			        <p>
			        	<input type="hidden" id="cashId" value="${payDTO.coupon.cashId }" />
			            <input type="radio" checked="checked" name="cashCoupon">
			            	<c:if test="${payDTO.coupon.cashType == 1 }">
			            		<span>满减卷</span><a>满${payDTO.coupon.amount }减${payDTO.coupon.discount }</a>
			            	</c:if>
			            	<c:if test="${payDTO.coupon.cashType == 2 }">
			            		<span>无限制</span><a>抵${payDTO.coupon.discount }</a>
			            	</c:if>
			            <span class="s_date">有效期至 <a>
			            	<frm:formatDate value="${payDTO.coupon.sdatetime }" pattern="yyyy-MM-dd　　HH:mm:ss" />
			            </a></span>
			        </p>
	        </div>
        </c:if>
        <h3>使用即时到账交易</h3>
    	<div class="fu_jishi">
            <ul>
            <div id="zhifubao">
            	${sHtmlText }
            </div>
            <div style="display: none">
				${chinaPayHtmlText }
            </div>
            <li class="hover"><img id="payTypeZhi" src="<%=imgurl %>/bg/zhifufs_1.png"></li>
            
            <li><img id="payTypeWeixin" src="<%=imgurl %>/bg/zhifufs_5.png"></li> 
            <li><img id="payTypeTen" src="<%=imgurl %>/bg/zhifufs_2.png"></li>
            <input type="hidden" id="tenUrl"  value="${requestUrl }"/>
             <input type="hidden" id="tenUrl"  value="${requestUrl }"/>
            <!-- <li><img id="payTypeyingLian" src="<%=imgurl %>/bg/zhifufs_3.png"></li>  -->
            </ul>
            <!--<div class="an_fu"><input type="button" id="zhiFu" onclick="pay();" value="立即支付"></div>  -->
            <p>注：便捷结算,安全有保证。</p>
        </div>
        <c:if test="${payDTO.user.UCreditLimit != 0 }">
	        <h3>信用支付 </h3>
	    	<div class="xinyong" id="xinyongPay">
	            <ul>
	            	<li id="xy_zhifu"><div><p>您的信用余额为：<span>&yen;${payDTO.user.URemainingAmount }
	            		    	<c:if test="${payDTO.user.URemainingAmount < payDTO.total_fee }">
						    		 余额不足
						    	</c:if>
	            	</span></p></div></li>
	            </ul>
	        </div>
        </c:if>
        <h3>线下支付</h3>
    	<div class="fu_xianxia">
            <ul>
            <!--<li onclick="javascript:location.href='order_toPay?orderDTO.orderSn=${payDTO.out_trade_no }'"><img src="<%=imgurl %>/bg/zhifufs_4.png"></li>  -->
            <li><img id="xianxiapay" src="<%=imgurl %>/bg/zhifufs_4.png"></li>
            
            </ul>
            <p>注：请前往<a href="order_toPay?orderDTO.orderSn=${payDTO.out_trade_no }" target="_blank">填写支付信息页面</a>。</p>
        </div>
    
        <div class="an_fu"><input type="button" id="zhiFu" onclick="pay();" value="立即支付"></div>
<div class="clear"></div>
    </div>

<div class="clear"></div>
</div>

<jsp:include page="../common/foot.jsp"></jsp:include>

  </body>
</html>

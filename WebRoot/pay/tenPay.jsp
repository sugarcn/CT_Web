<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = "//"+request.getServerName()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'tenPay.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	
<script src="<%=basePath %>js/jqueryt.js" type="text/javascript"></script>
  
  </head>
  
  <body>
  	<input type="hidden" id="ResUrl" value="${requestUrl }" />
   	<script type="text/javascript">
   		function loadTempUrl(){
			location.href=$("#ResUrl").val();
   		}
   		loadTempUrl();
	</script>
  </body>
</html>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"s://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html lang="zh-cmn-Hans">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <title>长亭易购-货款支付</title>
	<link rel="stylesheet" href="/style/weui.css"/>
    <link rel="stylesheet" href="/style/example.css"/>
    <script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
</head>
<body>
<!-- 
    <div class="weui_cells_title"></div>
	<div class="weui_center">
      
    </div>
	 -->

    <img alt="长按识别二维码" width="100%" height="100%" src="payWx.jpg">
    <!-- 
    <div class="weui_cells weui_cells_form">
        <div class="weui_cell">
            <div class="weui_cell_hd"><label class="weui_label">姓名</label></div>
            <div class="weui_cell_bd weui_cell_primary">
                <input class="weui_input" type="text" value="${wxName }" id="payName" placeholder="请输入您的姓名"/>
            </div>
        </div>
        <div class="weui_cell">
            <div class="weui_cell_hd"><label class="weui_label">付款金额</label></div>
            <div class="weui_cell_bd weui_cell_primary">
                  <input class="weui_input" type="number" onKeyUp="checkPrice(this);" id="total" name="orderDTO.total" pattern="[0-9]+\.[0-9]{1,2}" placeholder="请输入付款金额"/>
            </div>

        </div>
    </div>
 -->
    <!-- 
    <div class="weui_cells_tips"></div>
    <div class="weui_btn_area">
        <a class="weui_btn weui_btn_primary" href="javascript:void(0);" onClick="onBridgeReady();return false;" id="showTooltips">确定</a>
    </div>



<div class="weui_center_bottom">
       <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/img/logo_a.png" />
    </div>
 -->
   <!--  <script src="/style/zepto.min.js"></script> -->
  
  <script type="text/javascript">
  function checkPrice(me){
	if(!(/^(?:\d+|\d+\.\d{0,2})$/.test(me.value))){
		me.value = me.value.replace(/^(\d*\.\d{0,2}|\d+).*$/,'$1');
	}
  }
  function onBridgeReady(){
	  var total = $("#total").val();
	  var payName = $("#payName").val();
	  if(payName == ""){
		  alert("请输入您的姓名");
		  return;
	  }
	  if(total == ""){
		  alert("金额不能为空");
		  return;
	  }
	  //var url = window.location.href;
	  //var code = url.substring(url.indexOf("code=")+5, url.indexOf("&"));
	$.post(
		"weixinpaysc/pay",
		{"orderDTO.total":total,"orderDTO.payName":payName},
		function(data){
			//alert(data);
			data = eval('(' + data + ')'); 
			  if(parseInt(data.agent)<5){
				 alert("您的微信版本低于5.0无法使用微信支付");
				 return;
				}  
				WeixinJSBridge.invoke(
					       'getBrandWCPayRequest', {
					           "appId":data.appId,     //公众号名称，由商户传入     
					           "timeStamp":data.timeStamp,         //时间戳，自1970年以来的秒数     
					           "nonceStr": data.nonceStr, //随机串     
					           "package": data.packageValue,     
					           "signType":data.signType,         //微信签名方式：     
					           "paySign": data.paySign //微信签名 
					       },
					       function(res){  
					    	   //alert(res.err_msg);
					    	   //alert(res.err_code + "__1__" + res.err_desc + "__2__" + res.err_msg);
					           if(res.err_msg == "get_brand_wcpay_request:ok" ) {
					        	    //alert(data.sendUrl);
					        	   window.location.href=data.sendUrl;
					           }     // 使用以上方式判断前端返回,微信团队郑重提示：res.err_msg将在用户支付成功后返回    ok，但并不保证它绝对可靠。
					           if(res.err_msg == "get_brand_wcpay_request:cancel" ) {
					        	   location.reload();
					           }
					           if(res.err_msg == "get_brand_wcpay_request:fail" ) {
					        	   location.reload();
					           }
					       }
					   );
		}
	);
	}
	
  </script>
</body>
</html>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="common/basePath.jsp"%>
 
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="Keywords" content="${systemInfo.ctKey }" />
	<meta name="Description" content="${systemInfo.ctTitle }" />
	<meta name="360-site-verification" content="c3ebbc6b2d3afc308afcca9047faeed2" />
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
	<title>幸运大转盘活动</title>
	<link rel="stylesheet" href="css/activity.css">
  </head>
  
  <body>
            <div class="tanchu" style="display:block;">
  	  	        <div class="details">
  	  	            <p><span>活动已结束~</span> <!--<b class="close">  --></b></p>
  	  	            <div>
  	  	                <!--<p class="nth1">获得微信红包<span>50</span>元</p>  -->
  	  	                <p class="nth2">扫一扫关注长亭易购微信公众号<br>时刻关注后续活动哦！</p>
  	  	                <div></div>
  	  	                <!--<h1 id="osn">领奖码：CT123456</h1>  -->
  	  	            </div>
  	  	        </div>
  	  	    </div>
  	  	<jsp:include page="common/head.jsp"></jsp:include>
  	  	<div class="back" style="float:left;position:absolute;top:15px;right:69%;font-size:14px;">
  	  	    <a href="<%=basePath%>">返回首页</a>
  	  	</div>
  	  	<div class="banner"> 
  	  	    <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/2017/huodong/banner.png">
  	  	</div>
  	  	<div class="activety">
  	  	    <div class="content">
  	  	        <p>
  	  	        <a href="#div1"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/2017/huodong/btn1.png"></a>
  	  	        <a href="#div2"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/2017/huodong/btn2.png"></a>
  	  	        </p>
  	  	        <div class="div1" id="div1">
  	  	        <b></b>
  	  	        <span class="span1"></span><span class="span2"></span><span class="span3"></span>
  	  	            <h1><b></b><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/2017/huodong/wenzi1.png"></h1>
  	  	            <div class="bg">
  	  	                <b><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/2017/huodong/shadow.png"></b>
  	  	                <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/2017/huodong/dial.png">
  	  	            </div>
  	  	            <div class="tran">
  	  	                <button id="start_btn"></button>
  	  	            </div>
  	  	            <p class="p1">剩余抽奖次数：<span>${myDrawNum }</span></p>
  	  	            <div class="txt">
  	  	                <h3>活动规则：</h3>
  	  	                <p>1、红包抽奖仅限于长亭易购注册用户。</p>
  	  	                <p>2、登入本司官网，进入活动页面可进行抽奖，将抽奖后所获得领奖码回复长亭易购微信公众号，即可领取红包。（每日可获得一次抽奖机会）</p>
  	  	                <p>3、在微信朋友圈晒出平台下单成功的订单截图，上传至长亭易购微信公众号，经审核通过后可再获一次红包抽奖机会。</p>
  	  	                <p>4、转载长亭易购微信公账号任意文章，并截图上传至长亭易购微信公账号，经审核通过后再获一次红包抽奖机会。</p>
  	  	                <p>5、每个用户最多获得三次抽奖机会（每日）。</p>
  	  	                <p>6、抽奖时间：1月16日至1月18日，每天放出300个红包（总金额10000元）</p>
  	  	                <p>7、每个平台账号只能绑定一个微信号，如发现多账号一个微信即为作弊行为，将取消抽奖资格！</p>
  	  	                <p>8、活动最终解释权归长亭易购所有。</p>
  	  	                <p class="last_p">
  	  	                    <span>当前剩余红包数量：</span><b id="countHongBao">${nowCount }个</b>
  	  	                </p>
  	  	            </div>
  	  	            <div class="bulletin">
  	  	                <div class="record">
  	  	                    <h3>我的抽奖记录</h3>
  	  	                    <p>
  	  	                        <span>抽奖时间</span>
  	  	                        <span>红包兑换码</span>
  	  	                    </p>
  	  	                        <ul id="mydraw">
  	  	                        	<c:forEach items="${myDrawDetailsList }" var="list">
		  	  	                        <li>
		  	  	                            <span>${list.DTime }</span>
		  	  	                            <span>${list.DSn }</span>
		  	  	                        </li>
	  	  	                    	</c:forEach>
  	  	                        </ul>
  	  	                </div>
  	  	                <div class="list">
  	  	                    <h3>最新中奖名单</h3>
  	  	                    <div>
  	  	                    <ul id="darwInfo">
	  	  	                    <c:forEach items="${drawDetailsList }" var="list">
	  	  	                        <li>
	  	  	                            <span class="s1">${list.mdName }</span>
	  	  	                            <span class="s2">${list.DTime }</span>
	  	  	                            <span class="s3">${list.DSum }元微信红包</span>
	  	  	                        </li>
	  	  	                    </c:forEach>
  	  	                    </ul>
  	  	                    </div>
  	  	                </div>
  	  	            </div>
  	  	        </div>
  	  	        
  	  	        <div class="div2" id="div2">
  	  	        <span class="span1"></span><span class="span2"></span><b></b>
  	  	            <h1><b></b><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/2017/huodong/wenzi2.png"></h1>
  	  	            <div class="txt">
  	  	                <h3>活动规则</h3>
  	  	                <p>1、支付宝最新AR红包，长亭易购将会在本司华强北的线下体验店藏200个红包，总金额5000元。</p>
  	  	                <p>2、打开支付宝AR红包，点击找红包，拍摄本司线下体验店招牌就能获得长亭易购送出的新年祝福红包。</p>
  	  	                <p>3、红包数量有限，先到先得。</p>
  	  	                <p>4、活动最终解释权归长亭易购所有。</p>
  	  	                <p class="last_p">
  	  	                    <span>活动时间：</span>
  	  	                    <b>1月16日至1月18日</b>
  	  	                </p>
  	  	            </div>
  	  	            <div class="phone">
  	  	                <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/2017/huodong/phone.png">
  	  	            </div>
  	  	            <div class="site">
  	  	                <p>长亭易购线下体验店地址：</p>
  	  	                <p>深圳市华强北新亚洲电子市场1期1F046</p>
  	  	            </div>
  	  	        </div>
  	  	    </div>
  	  	</div>
			
  	  	<script src="<%=basePath %>js/jquery-1.11.3.js"></script>
  	  	<script type="text/javascript">
  	  eval(function(p,a,c,k,e,r){e=function(c){return(c<62?'':e(parseInt(c/62)))+((c=c%62)>35?String.fromCharCode(c+29):c.toString(36))};if('0'.replace(0,e)==0){while(c--)r[e(c)]=k[c];k=[function(e){return r[e]||e}];e=function(){return'([379a-hj-mq-zA-Z]|1\\w)'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('$(document).ready(b(){b 1j($1k,m){$1k.v({\'12\':\'G(\'+m+\'m)\',\'-moz-12\':\'G(\'+m+\'m)\',\'-o-12\':\'G(\'+m+\'m)\'})}b 1l(){c 1m=1n.floor(1n.1l()*4);13 1m}b G(){$.L("ajaxFindAllCountHong",b(g,M){f(g>0){$("#14").unbind("N");$.L("ajaxFindDrawNum",{"timespace":(H I()).valueOf()},b(g,M){c C=15;f(g=="error"){$(".3").q();$(".3  .7>p>d").h("很遗憾~");$(".3  .7>e .r").j("您的请求被拒绝");$(".3  .7>e t").u();C=O}f(g=="-2"){$(".3").q();$(".3  .7>p>d").h("亲~活动还未开始呢");$(".3  .7>e .r").j("");$(".3  .7>e t").u();$(".k .J d").h(0);C=O}f(C==15){c P=g.1o("__");c Q="";f(g!=4&&g!=-1){Q=P[0]}l{Q=4}c $1p=$(".bg>img"),i,R=100,D=0,17=0,S=[],T=Q,1q=18-(0.36*T),1r=["5","10","20","50","谢谢参与",];S[1]=[0.2,0.4,0.6,0.8,1,1.2,1.4,1.6,1.8,1.4];S[2]=[1.8,1.6,1.4,1.2,1,1,0.8,0.6,0.4,0.2];for(i=0;i<1s;i++){E(b(){c m=1q*(S[1t(R).1u(0,1)][1t(R).1u(1,1)]);1j($1p,m+17);17+=m;R++;D++;f(D>=1s){E(b(){f(T==4){$(".3").q();$(".3  .7>p>d").h("很遗憾~");$(".3  .7>e .r").j("您没有抽中奖品");$(".3  .7>e t").u()}l{c 1v=1r[T];$(".3").q();$(".3  .7>p>d").h("恭喜您");$(".3  .7>e .r").j("获得微信红包<d></d>元");$(".3  .7>e .r d").h(1v);$(".3  .7>e t").q();f(g!=4&&g!=-1){$("#countHongBao").j(P[1]+"个");$("#osn").j("领奖码："+P[2])}$.L("ajaxDrawInfoU",b(g,M){c 19=g.1o("|");$("#1w s").1a();$("#1w").j(19[0]);$("#U s").1a();$("#U").j(19[1])})}$("#14").N(b(){1b()})},1000)}},i*30)}}})}l{f(g==-1){1y.1z.1A="1B"}l{$(".3").q();$(".3  .7>p>d").h("很遗憾~");$(".3  .7>e .r").j("今天的奖池已经没有了，请明天再来吧");$(".3  .7>e t").u()}}})}$("#14").N(b(){1b()});$(".7 .close").N(b(){$(".3").u()});b 1b(){c C=1C();f(C==O){$(".3").q();$(".3  .7>p>d").h("亲~活动还未开始呢");$(".3  .7>e .r").j("");$(".3  .7>e t").u();$(".k .J d").h(0)}l{$.L("ajaxFindMyDrawNum",b(g,M){c n=g;f(n>0){n--;$(".k .J d").h(n);G()}l{f(n==-10){1y.1z.1A="1B"}l{f(n==-20){$(".3").q();$(".3  .7>p>d").h("亲~活动还未开始呢");$(".3  .7>e .r").j("");$(".3  .7>e t").u();$(".k .J d").h(0)}l{$(".3").q();$(".3  .7>p>d").h("很遗憾~");$(".3  .7>e .r").j("您的抽奖次数已用完");$(".3  .7>e t").u();$(".k .J d").h(0)}}}})}}b 1C(){c 1D=H I().V();c W=(H I("1E/01/16 F:F:F")).V();f(1D>W){13 15}l{13 O}}$.9.a.1G();1H()});b 1H(){c 1I=H I().V();c W=(H I("1E/01/16 F:F:F")).V();f(1I>W){$("#U").v("1J","block")}l{$("#U").v("1J","none")}}$.9.a={1c:0,1K:300,X:30,Y:0,D:0,Z:1d,1e:1d,11:0,1f:2000,1G:b(){w.Y=w.1K/w.X;w.1c=1L($(".x .y .k .z .A B s d").v("height"));w.1e=E(w.1g,w.1f)},1g:b(){$.9.a.D=$.9.a.1c/$.9.a.X;$.9.a.Z=E($.9.a.1h,$.9.a.Y)},1h:b(){c K=1L($(".x .y .k .z .A B").v("1i-K"));c 1M=K-$.9.a.D+"px";$(".x .y .k .z .A B").v("1i-K",1M);$.9.a.11++;f($.9.a.11<$.9.a.X){$.9.a.Z=E($.9.a.1h,$.9.a.Y)}l{$.9.a.Z=1d;$.9.a.11=0;$.9.a.1e=E($.9.a.1g,$.9.a.1f);c s=$(".x .y .k .z .A B s:1N-1O").j();$(".x .y .k .z .A B s:1N-1O").1a();$(".x .y .k .z .A B").v("1i-K",0);$(".x .y .k .z .A B").append("<s>"+s+"</s>")}}}',[],113,'|||tanchu||||details||fn|record|function|var|span|div|if|data|text||html|div1|else|deg||||show|nth1|li|h1|hide|css|this|activety|content|bulletin|list|ul|isStart|step|setTimeout|00|rotate|new|Date|p1|top|post|varstauts|click|false|offse|nowDate|cnt|ratio|offset|darwInfo|getTime|startTime|STEPS|interval|timer||moved|transform|return|start_btn|true||total||chai|remove|pan|LIHEIGHT|null|timer2|WAIT|move|moveStep|margin|setDegree|obj|random|num|Math|split|tar|amount|result|200|String|substr|res|mydraw||window|location|href|login_login|date|now|2017||init|showt|showTime|display|DURATION|parseFloat|m_top|first|child'.split('|'),0,{}))
  	  	</script>
  </body>
</html>

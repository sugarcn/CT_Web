<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>${systemInfo.ctTitle }</title>
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=6Pg8rFGKRTZqxGPsbvKD87DSqaQpuc00"></script>
  </head>
  
  <body>
  <jsp:include page="act_head.jsp"></jsp:include>
  <div class="top_banner">
    <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/act_banner2.png">
</div>
<div class="contactUs">
    <h2>
        <b>联系我们</b>
        <span>CONTACT US</span>
    </h2>
    <div class="content">
        <h1>中航中心总部</h1>
        <div class="div_1">
            <p>深圳市福田区华富路1018号2308室</p>
            <p>媒体合作：s@ctego.com</p>
            <p>服务热线：0755-33561467</p>
            <p>投诉电话：13662258574</p>
            <p>客服QQ：2880700694</p>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2881112904</p>
        </div>
        <div class="div_3">
            <p>微信公众号：ctego</p>
            <div>
            <p>长亭易购<br>官方微信</p>
            <ul>
                <li>品类齐全</li>
                <li>自建仓库</li>
                <li>品牌保证</li>
                <li>拆零服务</li>
            </ul>
            </div>
            <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/weixin.png">
        </div>
        <ul>
            <li class="li_1">
                <h1>产品运营部</h1>
                <p>地址：深圳市福田区振兴路109号华康大厦1栋306</p>
                <p>产品合作：0755-33561467</p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dyj@ctego.com</p>
            </li>
            <li class="li_2">
                <h1>线下提货点</h1>
                <p>地址：深圳市福田区新亚洲电子市场一期一楼1F046室</p>
                <p>联系方式：13794469606</p>
            </li>
        </ul>
        <div id="container">
        </div>
        <p class="myMap">长亭易购线下提货点</p>
    </div>
</div>
<script type="text/javascript">
    var map = new BMap.Map("container");          // 创建地图实例
    var point = new BMap.Point(114.091398,22.548415);  // 创建点坐标
    map.centerAndZoom(point, 15);                 // 初始化地图，设置中心点坐标和地图级别
    map.addControl(new BMap.NavigationControl());//地图平移缩放控件
    map.addControl(new BMap.ScaleControl());//比例尺控件
    map.addControl(new BMap.OverviewMapControl());//缩略地图控件
    map.addControl(new BMap.MapTypeControl());//地图类型控件
    var marker = new BMap.Marker(point);        // 创建标注
    map.addOverlay(marker);
    map.enableScrollWheelZoom();//启动鼠标滚轮缩放地图
    //标注跳动的动画
    marker.setAnimation(BMAP_ANIMATION_BOUNCE);
    //创建信息窗口
    var sContent="<div class='myclass'><span>深圳前海长亭易购线下提货点</span><br>深圳市福田区新亚洲电子市场一期一楼1F046室<br><img src='https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/aboutUs_4.png'></div>"
    //创建信息窗口对象
    var infoWindow=new BMap.InfoWindow(sContent);
    //map.openInfoWindow(infoWindow, map.getCenter());  //显示信息窗口
    map.centerAndZoom(point,15);
    //标注点添加到页面
    map.addOverlay(marker);
    //点击事件监听
    marker.addEventListener("click",function(){
        this.openInfoWindow(infoWindow);
        document.getElementById("imgDemo").onload=function(){
            infoWindow.redraw();
        }
    });
</script>
  <jsp:include page="act_foot.jsp"></jsp:include>
  </body>
</html>

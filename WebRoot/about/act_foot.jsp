<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <meta charset="UTF-8">
    <style>
        #act_foot{
            width:100%;
            overflow:hidden;
            margin-top:20px;
        }
        #act_foot .nav{
            width:100%;
            height:50px;
            background:#333;
        }
        #act_foot .nav ul{
            width:600px;
            height:50px;
            margin:0 auto;
        }
        #act_foot .nav ul li{
            float:left;
            width:25%;
            height:50px;
            text-align:center;
            line-height:50px;
            position:relative;
        }
        #act_foot .nav ul li a{
            color:#fff;
            font-size:16px;
        }
        #act_foot .nav ul li b{
            float:right;
            height:30px;
            position:absolute;
            top:10px;right:0;
            border-right:1px solid #fff;
        }
        #act_foot .foot_copy{
            width:500px;
            height:100px;
            margin:10px auto;
        }
        #act_foot .foot_copy>p{
            width:100%;
            font-size:13px;
            text-align:center;
            overflow:hidden;
        }
        #act_foot .foot_copy>p.foot_img{
            margin-top:10px;
        }
        #act_foot .foot_copy>p>a{
            float:left;
            text-align:center;
        }
        #act_foot .foot_copy>p>a>img{
            height:40px;
            margin-left:23px;
        }
        #act_foot .foot_copy>p>a:first-child>img{
            margin-left:0;
        }
    </style>
  </head>
  
  <body>
  <div id="act_foot">
    <div class="nav">
        <ul>
            <li><b></b><a href="/">网站首页</a></li>
            <li><b></b><a href="about/about_ct.jsp">关于长亭</a></li>
            <li><b></b><a href="about/contact_us.jsp">联系我们</a></li>
            <li><a href="about/join_us.jsp">加入我们</a></li>
        </ul>
    </div>
    <div class="foot_copy">
        <p>长亭易购 版权所有 &copy;2014-2017 CTEGO CORPORATION All Rights Reserved.</p>
        <p>粤ICP备16050358号-1
            <script type="text/javascript">var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");document.write(unescape("%3Cspan id='cnzz_stat_icon_1255740681'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s95.cnzz.com/z_stat.php%3Fid%3D1255740681%26show%3Dpic' type='text/javascript'%3E%3C/script%3E"));</script>
        </p>
        <p class="foot_img">
            <script id="ebsgovicon" src="http://szcert.ebs.org.cn/govicon.js?id=54c6c1f0-0dd6-4625-ab6e-8e34ad47d7ec&width=34&height=60&type=1" type="text/javascript" charset="utf-8"></script>

            <a href="http://webscan.360.cn/index/checkwebsite/url/www.ctego.com" style="height:50px; padding-left:10px;"><img style="height:40px; padding-top:0; margin-left:25px" border="0" src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/bot360.png"/></a>
            <a id="_pingansec_bottomimagelarge_shiming" href="http://si.trustutn.org/info?sn=608150928017490081680&certType=1">
                <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/rzlm.jpg"/></a>
            <a id="_pinganTrust" target="_blank" href="http://c.trustutn.org/s/ctego.com">
            <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/paxy.jpg"/></a>
        </p>
    </div>
</div>
  </body>
</html>

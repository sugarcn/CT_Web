<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="css/about_ct.css">
    <style>
        #act_head{
            width:100%;
            height:140px;
            border-bottom:20px solid #F5F5F5;
            position:relative;
        }
        #act_head>a>img{
            float:left;
            width:190px;
            position:absolute;
            top:34px;
            left:200px;
            z-index:50;
        }
        #act_head>ul{
            height:120px;
            line-height:120px;
            float:right;
            position:absolute;
            top:0;right:20%;
        }
        #act_head>ul li{
            float:left;
            height:120px;
            overflow:hidden;
            width:140px;
            text-align:center;
        }
        #act_head>ul li a{
            float:left;
            color:#333;
            font-size:22px;
            height:120px;
            width:90px;
            text-align:center;
            position:relative;
        }
        #act_head>ul li a b{
            float:left;
            position:absolute;
            left:0;
            bottom:0;
            width:100%;
            height:8px;
            background:#2387F9;
            display:none;
        }
        #act_head>ul li a:hover{
            color:#2387F9;
        }
        #act_head>ul li a:hover b{
            display:block;
        }
    </style>
  </head>
  
  <body>
  <div id="act_head">
        <a href="/"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/act_logo.png"></a>
        <ul>
            <li><a href="about/about_ct.jsp"><b></b>关于长亭</a></li>
            <li><a href="about/contact_us.jsp"><b></b>联系我们</a></li>
            <li><a href="about/join_us.jsp"><b></b>加入我们</a></li>
        </ul>
</div>
  </body>
</html>

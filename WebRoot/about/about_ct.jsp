<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>${systemInfo.ctTitle }</title>
    
  </head>
  
  <body>
  <jsp:include page="act_head.jsp"></jsp:include>
  <div class="top_banner">
    <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/act_banner1.png">
</div>
<div id="act_main">
    <!--关于我们-->
    <div class="aboutUs">
        <h2>
            <b>关于我们</b>
            <span>ABOUT US</span>
        </h2>
        <p>长亭易购(www.ctego.com)隶属于深圳前海长亭易购电子商务有限公司，是国内首家电子元器件样品服务平台，</p>
        <p>平台于2014年10月筹备上线，总部位于深圳，拥有专业的行业和互联网人才团队，迄今已获得多轮风险投资，</p>
        <p>为解决电子元器件样品采购难的问题，公司自营库存，提供拆零服务，</p>
        <p>通过自建平台，自建作业系统，为用户提供更高效的采购方式。</p>
        <ul>
            <li class="li_1"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/aboutUs_1.png"></li>
            <li class="li_2"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/aboutUs_2.png"></li>
            <li class="li_3"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/aboutUs_3.png"></li>
            <li class="li_4"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/aboutUs_4.png"></li>
            <li class="li_5"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/aboutUs_5.png"></li>
            <li class="li_6"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/aboutUs_6.png"></li>
        </ul>
    </div>
    <!--服务宗旨-->
    <div class="serviceTenet">
        <b></b>
        <h2>
            <b>服务宗旨</b>
            <span>SERVICE TENET</span>
        </h2>
        <div class="content">
            <ul class="txt">
                <li class="yuanjing">
                    <h2>愿景</h2>
                    <b class="b1"></b>
                    <b class="b2"></b>
                    <p>成为</p>
                    <p>现货<span>最齐全</span>的电子元器件</p>
                    <p class="last"><span>样品</span>服务平台</p>
                </li>
                <li class="shiming">
                    <h2>使命</h2>
                    <b class="b1"></b>
                    <b class="b2"></b>
                    <p><span>让</span></p>
                    <p>电子元器件<span>采购</span></p>
                    <p class="last">更<span>简单</span></p>
                </li>
            </ul>
            <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/service.png">
            <ul class="service_ul">
                <li>
                    <p>全</p>
                    <h1>品类齐全 一站采购</h1>
                    <span>长亭易购经营155170条国内外库存数据，库存品种目前超过30000种，库存种类不断增加！</span>
                </li>
                <li>
                    <p>快</p>
                    <h1>自建系统 方便快捷</h1>
                    <span>长亭易购自建平台，为您提供高度透明价格，oms订单系统，wms仓储系统，快速响应订单，当天订单，当天发货。</span>
                </li>
                <li>
                    <p>正</p>
                    <h1>品牌保证 售后无忧</h1>
                    <span>长亭易购与国内外元器件厂家和代理商合作，为确保每颗物料来自原厂，我们选择自营模式，目前仓库面积2300平米。</span>
                </li>
                <li>
                    <p>省</p>
                    <h1>拆零服务 省时省心</h1>
                    <span>长亭易购独有的元器件拆零服务，bom表匹配，为您解决电子元器件样品采购、小批量采购的难题！</span>
                </li>
            </ul>
        </div>
    </div>
    <!--企业文化-->
    <div class="company">
        <b></b>
        <h2>
            <b>企业文化</b>
            <span>COMOPANY CULTURE</span>
        </h2>
        <div class="content">
            <p>价值观</p>
            <ul>
                <li class="li_1">
                    <h1>细节</h1>
                    <span>NO DETAILS NO SUCCESS</span>
                    <p>【决定成败】</p>
                </li>
                <li class="li_2">
                    <h1>目标</h1>
                    <span>AIMS DEFINE DIRECTION</span>
                    <p>【决定方向】</p>
                </li>
                <li class="li_3">
                    <h1>行动</h1>
                    <span>IF IS BETTER TO DO</span>
                    <p>【成就未来】</p>
                </li>
            </ul>
            <p class="p2">企业宣传视频</p>
            <div class="embed"><video src="http://ctufile.oss-cn-shenzhen.aliyuncs.com/ctego.mp4" width="914px" height="515px" controls="controls" poster="http://ctufile.oss-cn-shenzhen.aliyuncs.com/fengmian.png" preload="none"></video></div>
        </div>
        <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/bg_1.png" class="img1">
        <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/bg_2.png" class="img2">
    </div>
    <!--荣誉资质-->
    <div class="honor">
        <b></b>
        <h2>
            <b>荣誉资质</b>
            <span>HONOR</span>
        </h2>
        <ul>
            <li class="li_1"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/honor_1.png"></li>
            <li class="li_2"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/honor_2.png"></li>
            <li class="li_3"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/honor_3.png"></li>
            <li class="li_4"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/honor_4.png"></li>
            <li class="li_5"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/honor_5.png"></li>
            <li class="li_6"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/honor_6.png"></li>
        </ul>
    </div>
</div>
<a class="act_fixed" onclick="javascript:window.scrollTo( 0, 0 )"; style="cursor:pointer;"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/back.png"></a>
<jsp:include page="act_foot.jsp"></jsp:include>
  </body>
</html>

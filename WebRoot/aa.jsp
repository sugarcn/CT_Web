<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Export to Excel - Demo</title>
</head>
<body>
    <%
        String exportToExcel = request.getParameter("exportToExcel");
        if (exportToExcel != null
                && exportToExcel.toString().equalsIgnoreCase("YES")) {
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "inline; filename="
                    + "excel.xls");
 
        }
    %>
    <table align="left" border="2">
        <thead>
            <tr bgcolor="lightgreen">
                <th>Sr. No.</th>
                <th>Text Data</th>
                <th>Number Data</th>
            </tr>
        </thead>
        <tbody>
            <%
                for (int i = 0; i < 10; i++) {
            %>
            <tr bgcolor="lightblue">
                <td align="center"><%=i + 1%></td>
                <td align="center">This is text data <%=i%></td>
                <td align="center"><%=i * i%></td>
            </tr>
            <%
                }
            %>
        </tbody>
    </table>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
            
    <%
        if (exportToExcel == null) {
    %>
    <a href="aa.jsp?exportToExcel=YES">Export to Excel</a>
    <%
        }
    %>
    <script type="text/javascript">
    function myPrint(tableId) { 
    	try { 
    	//alert( 'a '); 
    	var elTable = document.getElementById(tableId); 
    	//alert( 'b '); 
    	var oRangeRef = document.body.createTextRange(); 
    	oRangeRef.moveToElementText(elTable); 
    	//alert( 'c '); 
    	oRangeRef.execCommand( "Copy "); 
    	//alert( 'd '); 
    	var oXL = new ActiveXObject( "Excel.Application "); 
    	//alert( "e "); 
    	var oWB = oXL.Workbooks.Add; 
    	var oSheet = oWB.ActiveSheet; 
    	oSheet.Paste(); 

    	oXL.Selection.ColumnWidth = 5; 
    	oXL.Selection.RowHeight = 35;// 行高 
    	//oXL.Selection.Rows( " <%=rows%> : <%=rows%> ").RowHeight = 35; //动态设定单行高度 
    	oXL.Selection.Columns(3).ColumnWidth = 7; //单列设定宽度 
    	oXL.Selection.Columns(5).ColumnWidth = 8; //单列设定宽度 
    	oXL.Visible = true; 
    	oSheet = null; 
    	oWB = null; 
    	appExcel = null; 
    	} 
    	catch (e) { 
    	alert(e.description); 
    	} 
    </script>
</body>
</html>
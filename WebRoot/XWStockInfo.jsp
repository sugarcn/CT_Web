<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>碧联供应链第三方库存监控系统</title>
    <style>
        body{
             width:100%;
            height:100%;
            background:url("img/xwzn.jpg") no-repeat  center center fixed !important;
            background-size:cover !important;
        }
        #xw{
            width:600px;
            height:220px;
            position:absolute;
            top:50%;
            left:50%;
            margin-left:-300px;
            margin-top:-110px;
            text-align:center;
        }
         #xw p{
            margin:0;
            font-size:40px;
            color:#fff;
            line-height:60px;
        }
        #xw p.p_time{
            font-size:34px;
            line-height:34px;
            margin-top:10px;
            margin-bottom:20px;
        }
        .clear{
            clear:both; height: 100%; width:100%; font-size: 0;
        }
    </style>
</head>
<body>
<div id="xw">
    <p class="p_time">至 <span id="time"></span> 止</p>
    <p>实时库存数量: <span>${stock.stockNum }</span></p>
    <p>实时库存金额: <span>${XWStockPrice }</span></p>
</div>
<script type="text/javascript">
var timer=setInterval(function(){
    var time=new Date();
    var span=document.getElementById("time");
    var M=time.getMonth()+1;
    M<10?M="0"+M:M=M;
    var d=time.getDate();
    d<10?d="0"+d:d=d;
    var h=time.getHours();
    h<10?h="0"+h:h=h;
    var m=time.getMinutes();
    m<10?m="0"+m:m=m;
    var date=time.getFullYear()+"-"+M+"-"+d+" "+h+":"+m;
    span.innerHTML=date;
},1000)

var t=60;//设定跳转的时间 
setInterval("refer()",1000); //启动1秒定时 
function refer(){
	if(t==0){
		window.location.reload();
	}
	t--;
}
</script>
</body>
</html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<base href="<%=basePath%>">

 <meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
</head>

<body>

	<jsp:include page="../common/head.jsp"></jsp:include>
	<div class="contant_e">

		<div class="dizhilian_e">
			<a href="<%=basePath %>" target="_blank">首页</a>><a>客服中心</a>><span>我的投诉</span>
		</div>
		<jsp:include page="../common/left.jsp"></jsp:include>
		<div class="right_k">


			<div class="zpshzher">
				<div class="biao">我的投诉</div>

				<div class="ts_zong">
					<span class="wsl">待处理<i>${totalNo }</i>个</span><span
						class="wchl">处理完成<i>${totalOk }</i>个</span>
				</div>

				<div class="ts_tit">
					<h2>我的投诉</h2>
				</div>

				<div class="ts_zhu">
					<ul class="tszhu_ul">
						<li class="ts_zhu_1">投诉类别</li>
						<li class="ts_zhu_2">涉及订单</li>
						<li class="ts_zhu_3">投诉内容</li>
						<li class="ts_zhu_4">回复状态</li>
					</ul>
					<c:forEach items="${complainList }" var="list">
						<dl class="tszhu_dl">
							<dd class="ts_zhu_1">${list.comType }</dd>
							<dd class="ts_zhu_2">${list.orderInfo.orderSn }</dd>
							<dd class="ts_zhu_3">
								<a href="order_complain_Details?complain.comId=${list.comId }" target="_blank">${list.comContent }</a>
							</dd>
							<dd class="ts_zhu_4">
								<c:if test="${list.comStatus == 1 }">
									<span class="ts_ch_d">
										待处理
									</span>
								</c:if>
								<c:if test="${list.comStatus == 2 }">
									<span class="ts_ch_w">处理完成</span> 
								</c:if>
							</dd>
						</dl>
					</c:forEach>
					
					${pages.comStr }

				</div>
				<div class="ts_tit">
					<h2>提交我的投诉</h2>
				</div>
				<form action="upFileToComplain" id="myform" method="post" enctype="multipart/form-data">
					<div class="ts_zhu">
						<dl class="ts_neir">
							<dd>
								<div class="tx_tt">投诉类别：</div>
								<div class="tx_nei">
									<select name="complain.comType" id="comType"
										>
										<option value="0">请选择类别</option>
										<option value='产品方面'>产品方面</option>
										<option value='价格方面'>价格方面</option>
										<option value='服务方面'>服务方面</option>
										<option value='物流方面'>物流方面</option>
									</select>
								</div>
							</dd>
							<dd>
								<div class="tx_tt">涉及订单：</div>
								<div class="tx_nei">
									<input id="orderId" maxlength="14" onblur="javascript:getOrderIsChong();" name="complain.orderInfo.orderSn" type="text">
								</div>
							</dd>
							<dd>
								<div class="tx_tt tx_ttb">投诉内容：</div>
								<div class="tx_wei">
									<textarea maxlength="200" id="comContent" name="complain.comContent">限200字内！</textarea>
								</div>
							</dd>
							<dd>
								<div class="tx_tt">联系手机：</div>
								<div class="tx_nei">
									<input name="complain.UTel" id="UTel" maxlength="11"
										class="lcsc_xxfk3"
										type="text">
								</div>
							</dd>
							<dd>
								<div class="tx_tt">上传图片：</div>
								<div class="tx_nei tx_neiqu">
									<input name="up" id="myFile" type="file" class="tx_wenjian" />
								</div>
								<div class="tx_zhushi">(只支持图片形式，如png，gif ，jpeg 文件大小不能大于5M。)</div>
							</dd>
							<dd>
								<div class="tx_tt tx_ttb">备注：</div>
								<div class="tx_wei">
									<textarea id="comDesc" maxlength="200" name="complain.comDesc">限200字内！</textarea>
								</div>
							</dd>
							<dd>
								<div class="tx_que tx_qux">
									<a href="javascript:upFileForComplain();">确认</a><a href="javascript:;" onclick="chongzhi();" class="tx_chzh">重置</a>
								</div>
						</dl>
						<script type="text/javascript">
							function getOrderIsChong(){
								var $orderId = $("#orderId").val();
								if($orderId.length =="" && $orderId != null){
									return;
								}
								$.post(
									"${pageContext.request.contextPath}/order_IsChong",
									{"orderInfo.orderSn":$orderId},
									function(data,varstart){
										if(data == "未找到此订单"){
											alert(data);
											$("#orderId").val("");
										}
									}
								);
							}
							function chongzhi(){
								var $comType = $("#comType").val("");
								var $orderId = $("#orderId").val("");
								var $comContent = $("#comContent").val("");
								var $UTel = $("#UTel").val("");
								var $myFile = $("#myFile").val("");
								var $comDesc = $("#comDesc").val("");
							}
							function upFileForComplain(){
								var $comType = $("#comType").val();
								var $orderId = $("#orderId").val();
								var $comContent = $("#comContent").val();
								var $UTel = $("#UTel").val();
								var $myFile = $("#myFile").val();
								var $comDesc = $("#comDesc").val();
								if(!checkSpe($comContent)){
									alert("投诉的内容不能含有非法字符");
									return;
								}
								if(!checkSpe($comDesc)){
									alert("备注信息不能含有非法字符");
									return;
								}
								if($comType == '0'){
									alert("请选择投诉类型！");
									return;
								}
								if($orderId.length == 0){
									alert("请输入需要投诉的订单");
									return;
								}
								if($comContent.length == 0){
									alert("请输入需要投诉内容");
									return;
								}
								if($UTel.length == 0){
									alert("请输入电话！");
									return;
								}
								if(!/^1[3|5|7|8|9][0-9]\d{8}$/.test($UTel)){
									alert("电话号码不合法！");
									return;
								}
								if($myFile.length == 0){
									alert("请选择图片！");
									return;
								}
								if($comDesc.length == 0){
									alert("请输入备注信息！");
									return;
								}
								document.getElementById('myform').submit();
							}
						</script>
					</div>
				</form>

			</div>
		</div>

		<div class="clear"></div>
	</div>
	<jsp:include page="../common/foot.jsp"></jsp:include>

</body>
</html>

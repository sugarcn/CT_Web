﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <base href="<%=basePath%>">
    
     <meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<script type="text/javascript">
function subnum(num){
	var goodsNum = $("#"+num).val();
	if(goodsNum < 2){
		alert("数量不能小于1");
		return;
	}
	var newgoodsNum = Number(goodsNum) - 1;
	$("#"+num).val(newgoodsNum);
	var pp = document.getElementById(num+"pp").value;
	var obj = document.getElementById(num+"se");
	var index = obj.selectedIndex;
	var pn = obj.options[index].value;
	var money = pp*pn*newgoodsNum;
	document.getElementById(num+"sp").innerText = money+"元";
}
function addnum(num){
	var goodsNum = $("#"+num).val();
	var newgoodsNum = Number(goodsNum) + 1;
	$("#"+num).val(newgoodsNum);
	var pp = document.getElementById(num+"pp").value;
	var obj = document.getElementById(num+"se");
	var index = obj.selectedIndex;
	var pn = obj.options[index].value;
	var money = pp*pn*newgoodsNum;
	document.getElementById(num+"sp").innerText = money+"元";
}
</script>
<style>
    .zfcg_g>.coupon{
        width:400px;
        height:100px;
        margin:0 auto;
        overflow:hidden;
        position:relative;
        padding-top:80px;
    }
    .zfcg_g>.coupon p{
        width:100%;
        text-align:center;
        font-size:24px;
        color:#ff8900;
        font-weigth:bold;
    }
    .zfcg_g>.coupon a{
        float:left;
        width:100px;
        height:30px;
        background:#ff0066;
        color:#fff;
        text-align:center;
        line-height:30px;
        border-radius:5px;
        position:absolute;
        bottom:20px;
        left:50%;
        margin-left:-60px;
        cursor:pointer;
        font-size:14px;
        font-weight:bold;
    }
</style>
  </head>
  
  <body>
  
  <jsp:include page="../common/head.jsp"></jsp:include>
  

<div class="zfcg_g">
<!-- 
<div class="coupon">
    <p>小主，优惠券君已飞到您的账户啦!!</p>
    <a href="coupon_list?coupondetailDTO.type1=1&coupondetailDTO.type2=1">点击查看</a>
</div> -->
<dl class="wf_s">
<dt><img src="<%=imgurl%>/bg/duide_bg.gif"> 支付成功!!您的订单已经完成付款，订单号为 <font color='red'>${orderInfo.orderSn }</font>，正在为您配货。</dt>
<dd><a href="<%=basePath %>order_list">查看订单状态</a><span>|</span><a href="<%=basePath %>order_list">我的订单</a></dd>
</dl>

<ul>
<li>安全提醒：长亭易购不会以付款异常、卡单、系统升级为由要求您退款，谨防诈骗！</li>
<li>保管好个人账户信息，不要将<span>银行卡、密码、手机验证码</span>提供他人。</li>
</ul>


<div class="clear"></div>
</div>

<jsp:include page="../common/foot.jsp"></jsp:include>

  </body>
</html>

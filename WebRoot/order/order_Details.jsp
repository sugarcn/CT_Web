﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<link rel="stylesheet" href="css/orderdetail.css">
<title>${systemInfo.ctTitle }</title>
<style type="text/css">
	.addBack{margin-left: 23px; }
</style>
<script type="text/javascript">
	$(function() {

		$("#searchSelected").click(function() {
			$("#searchTab").show();
			$(this).addClass("searchOpen");
		});

		$("#searchTab li").hover(function() {
			$(this).addClass("selected");
		}, function() {
			$(this).removeClass("selected");
		});

		$("#searchTab li").click(function() {
			$("#searchSelected").html($(this).html());
			$("#searchTab").hide();
			$("#searchSelected").removeClass("searchOpen");
		});
	});
</script>
<link type="text/css" rel="stylesheet" href="css/style.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>

</head>

<body>
<script type="text/javascript">  
//===========================点击展开关闭效果====================================  
function openShutManager(oSourceObj,oTargetObj,shutAble,oOpenTip,oShutTip){  
var sourceObj = typeof oSourceObj == "string" ? document.getElementByName(oSourceObj) : oSourceObj;  
var targetObj = $(sourceObj).parent().siblings();
var targetObj1 = targetObj[1];
var targetObj2 = $(targetObj1).children("div")[0];
var openTip = oOpenTip || "";  
var shutTip = oShutTip || "";  
if(targetObj2.style.display!="none"){  
   if(shutAble) return;  
   targetObj2.style.display="none";  
   if(openTip  &&  shutTip){  
    sourceObj.innerHTML = shutTip;
   }
} else {  
   targetObj2.style.display="block";  
   if(openTip  &&  shutTip){  
    sourceObj.innerHTML = openTip;   
   }  
}  

}
//===========================点击展开关闭效果====================================  
function openShutManager1(oSourceObj,oTargetObj,shutAble,oOpenTip,oShutTip){  
var sourceObj = typeof oSourceObj == "string" ? document.getElementByName(oSourceObj) : oSourceObj;  
var targetObj = $(sourceObj).parent().siblings();
var targetObj1 = targetObj[1];
if(targetObj.length == 5){
	targetObj1 = targetObj[4];
} else {
	targetObj1 = targetObj[1];
}
var targetObj2 = $(targetObj1).children("div")[0];
var openTip = oOpenTip || "";  
var shutTip = oShutTip || "";  
if(targetObj2.style.display!="none"){  
   if(shutAble) return;  
   targetObj2.style.display="none";  
   if(openTip  &&  shutTip){  
    sourceObj.innerHTML = shutTip;
   }
} else {  
   targetObj2.style.display="block";  
   if(openTip  &&  shutTip){  
    sourceObj.innerHTML = openTip;   
   }  
}  

}
</script> 
  <jsp:include page="../common/head.jsp"></jsp:include>
	<div class="contant_e">
<input type="hidden" id="zhuangtai" value="${orderInfo.orderStatus }" />
    <div class="dizhilian_e"><a href="<%=basePath %>" target="_blank">首页</a>><a target="_blank">订单中心</a>><span>我的订单</span></div>
  <jsp:include page="../common/left.jsp"></jsp:include> 
    
		<div class="right_e">
			<div class="dingdanhao_p">
				订单号：${orderInfo.orderSn }<span>状态：<i>
					 <c:if test="${orderInfo.orderStatus==0}">待付款</c:if>
					 <c:if test="${orderInfo.orderStatus==1}">已付款</c:if>
					 <c:if test="${orderInfo.orderStatus==2}">待审核</c:if>
					 <c:if test="${orderInfo.orderStatus==3}">配货中</c:if>
					 <c:if test="${orderInfo.orderStatus==4}">已发货</c:if>
					 <c:if test="${orderInfo.orderStatus==5}">已完成</c:if>
					 <c:if test="${orderInfo.orderStatus==6}">取消</c:if>
					 <c:if test="${orderInfo.orderStatus==7}">无效    </c:if>
					 <c:if test="${orderInfo.orderStatus==8}">已备货   </c:if>
				</i>
				</span>
			</div>

			<div class="diangdanzht_p">
				<p>
					 <c:if test="${orderInfo.orderStatus==0}">亲，您的订单已经生成，请您尽快付款</c:if>
					 <c:if test="${orderInfo.orderStatus==1}">正在帮您审核订单，请耐心等待</c:if>
					 <c:if test="${orderInfo.orderStatus==2}">正在帮您审核订单，请耐心等待</c:if>
					 <c:if test="${orderInfo.orderStatus==3}">正在配货</c:if>
					 <c:if test="${orderInfo.orderStatus==4}">订单已经发货，请即时注意查收</c:if>
					 <c:if test="${orderInfo.orderStatus==5}">已完成</c:if>
					 <c:if test="${orderInfo.orderStatus==6}">您取消的这笔订单</c:if>
					 <c:if test="${orderInfo.orderStatus==7}">打款不足，订单无效 </c:if>
					 <c:if test="${orderInfo.orderStatus==8}">您的订单已经备货完成 </c:if>
				</p>
				<ul class="ztbj">
					<c:if test="${orderInfo.orderStatus==0}">
						<li><img src="<%=imgurl %>/bg/dingdanzht1.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht3.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht3.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht3.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht3.gif">
						</li>
					</c:if>
					 <c:if test="${orderInfo.orderStatus==1}">
					 	<li><img src="<%=imgurl %>/bg/dingdanzht1.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht2.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht3.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht3.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht3.gif">
						</li>
					 </c:if>
					 <c:if test="${orderInfo.orderStatus==2}">
					 	<li><img src="<%=imgurl %>/bg/dingdanzht1.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht2.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht3.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht3.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht3.gif">
						</li>
					 </c:if>
					 <c:if test="${orderInfo.orderStatus==3}">
					 	<li><img src="<%=imgurl %>/bg/dingdanzht1.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht2.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht2.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht3.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht3.gif">
						</li>
					 </c:if>
					 <c:if test="${orderInfo.orderStatus==4}">
					 	<li><img src="<%=imgurl %>/bg/dingdanzht1.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht2.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht2.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht2.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht3.gif">
						</li>
					 </c:if>
					 <c:if test="${orderInfo.orderStatus==5}">
					 	<li><img src="<%=imgurl %>/bg/dingdanzht1.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht2.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht2.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht2.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht2.gif">
						</li>
					 </c:if>
					 <c:if test="${orderInfo.orderStatus==6}">
					 	<li><img src="<%=imgurl %>/bg/dingdanzht1.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht3.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht3.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht3.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht3.gif">
						</li>
					 </c:if>
					 <c:if test="${orderInfo.orderStatus==7}">
					 	<li><img src="<%=imgurl %>/bg/dingdanzht1.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht2.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht3.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht3.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht3.gif">
						</li>
					 </c:if>
					 <c:if test="${orderInfo.orderStatus==8}">
					 	<li><img src="<%=imgurl %>/bg/dingdanzht1.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht2.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht2.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht3.gif">
						</li>
						<li><img src="<%=imgurl %>/bg/dingdanzht3.gif">
						</li>
					 </c:if>
					
				</ul>
				<ul class="ztwz">
					<li>提交订单</li>
					<li>付款成功</li>
					<li>商品出库</li>
					<li>等待收货</li>
					<li>完成</li>
				</ul>
			</div>

			<div class="ddfk">
				<div class="chooes">
					<div class="xiangmu_tiao">
						<ul>
							<li id="xmlistb1" onclick="setTab('xmlistb',1,2)" class="hover"><a
								href="" onclick="return false" >订单跟踪</a>
							</li>
							<li id="xmlistb2" onclick="setTab('xmlistb',2,2)"><a href=""
								onclick="return false">付款信息</a>
							</li>
						</ul>
					</div>
					
					<script type="text/javascript">
						var temp=false;
						function lookWuliu()
						{
							var ziti = $("#ziti").val();
							if(ziti == "1"){
								return;
							}
							var wuliu = $("#htmlTest1").val();
							var zhuangtai = $("#zhuangtai").val();
							if(zhuangtai == 4 || zhuangtai == 5){
								$("#rem").remove();
								$("#tab").remove();
								$("table[cellspacing=0]").remove();
								if(wuliu.indexOf("div")>0){
									var url = $("#htmlTest").val();
									var str = "<div class='dingdangenzong' id='rem'>"
											+"<iframe style='margin-top: 20px; margin-left: -12px;' name='kuaidi100' id='kuaidi100' src='' width='600' height='380' marginwidth='0' marginheight='0'"
											+"hspace='0' vspace='0'frameborder='0' scrolling='no'></iframe>"
											+"<span id='tab' style='margin-left:23px;'>数据由快递100提供</span>"
											+"</div>";
									$("#findWuLiu").after(str);
									//给iframe的src赋值。
									document.getElementById("kuaidi100").src = url;
								} else {
									$("#findWuLiu").after(wuliu);
									$("table[cellspacing=0]").addClass("addBack");
									$("table[cellspacing=0]").after("<span id='tab' style='margin-left:23px;'>数据由快递100提供</span>");
								}
								
								temp=true;
							} else {
								$("#rem").remove();
								$("table[cellspacing=0]").remove();
								$("#tab").remove();
								temp=false;
							}
						}
						//lookWuliu();
					</script>
					<div class="pir_right">
						<div id="con_xmlistb_1" class="con_all">
							<input type="hidden" id="htmlTest" value="${WuLiuFindUrl }" />
							<input type="hidden" id="htmlTest1" value="${WuLiuFind }" />
							<div class="dingdangenzong" id="findWuLiu">
								<c:if test="${orderInfo.exNo != null && orderInfo.exNo != '0' }">
									<c:if test="${orderInfo.shippingType == 4 }">
										<input type="hidden" id="ziti" value="1" />
									</c:if>
									<c:if test="${orderInfo.shippingType != 4 }">
										<input type="hidden" id="ziti" value="0" />
									</c:if>
									<ul class="songhuo">
										<li class="li1">送货方式：${express.exName }</li>
										<li class="li2">承运人：${express.exDesc }</li>
										<li class="li3">运单号：${orderInfo.exNo } |  快递网址： ${express.exUrl }</li>
										<li><a class="wuliu jbGre2 shadowAll" href="javascript:void()">查看物流</a></li>
									</ul>
								</c:if>
								<c:if test="${orderInfo.orderStatus == 1 || orderInfo.orderStatus == 6 }">
									<c:if test="${orderInfo.orderStatus == 6 }">
										<ul class="songhuo">
											<li class="li11">订单已经取消</li>
											<li class="li22">您取消的这笔订单</li>
											<li class="li33"></li>
											<li></li>
										</ul>
									</c:if>
									<ul class="songhuo">
										<li class="li11">提交时间：${orderInfo.orderTime }</li>
										<li class="li22">您提交了订单，订单进入华南仓准备备货</li>
										<li class="li33"></li>
										<li></li>
									</ul>
								</c:if>
								<c:if test="${orderInfo.orderStatus == 8 }">
									<ul class="songhuo">
										<li class="li11">备货时间：${orderInfo.stokingTime }</li>
										<li class="li22">您的订单已经备货完成</li>
										<li class="li33"></li>
										<li></li>
									</ul>
									<ul class="songhuo">
										<li class="li11">提交时间：${orderInfo.orderTime }</li>
										<li class="li22">您提交了订单，订单进入华南仓准备备货</li>
										<li class="li33"></li>
										<li></li>
									</ul>
								</c:if>
								<c:if test="${orderInfo.orderStatus == 4 }">
									<ul class="songhuo">
										<li class="li11">发货时间：${orderInfo.deliverTime }</li>
										<c:if test="${orderInfo.shippingType == 4 }">
											<li class="li22">包裹已出库，发货完成，预计
											<c:if test="${orderGoodslistSize <= 20 }">2</c:if>
											<c:if test="${orderGoodslistSize > 20 && orderGoodslistSize <= 60 }">3</c:if>
											<c:if test="${orderGoodslistSize > 60 && orderGoodslistSize <= 100 }">4</c:if>
											<c:if test="${orderGoodslistSize > 100 }">5</c:if>
											小时到达 ${shopType.shopName }</li>
										</c:if>
										<c:if test="${orderInfo.shippingType != 4 }">
											<li class="li22">包裹已出库，发货完成</li>
										</c:if>
										
										<li class="li33"></li>
										<li></li>
									</ul>
									<ul class="songhuo">
										<li class="li11">备货时间：${orderInfo.stokingTime }</li>
										<li class="li22">您的订单已经备货完成</li>
										<li class="li33"></li>
										<li></li>
									</ul>
									<ul class="songhuo">
										<li class="li11">提交时间：${orderInfo.orderTime }</li>
										<li class="li22">您提交了订单，订单进入华南仓准备备货</li>
										<li class="li33"></li>
										<li></li>
									</ul>
								</c:if>
								<c:if test="${orderInfo.orderStatus == 5 }">
									<ul class="songhuo">
										<li class="li11">完成时间：${orderInfo.isOkTime }</li>
										<li class="li22">订单完成</li>
										<li class="li33"></li>
										<li></li>
									</ul>
									<ul class="songhuo">
										<li class="li11">发货时间：${orderInfo.deliverTime }</li>
										<c:if test="${orderInfo.shippingType == 4 }">
											<li class="li22">包裹已出库，发货完成，预计
											<c:if test="${orderGoodslistSize <= 20 }">2</c:if>
											<c:if test="${orderGoodslistSize > 20 && orderGoodslistSize <= 60 }">3</c:if>
											<c:if test="${orderGoodslistSize > 60 && orderGoodslistSize <= 100 }">4</c:if>
											<c:if test="${orderGoodslistSize > 100 }">5</c:if>
											小时到达 ${shopType.shopName }</li>
										</c:if>
										<c:if test="${orderInfo.shippingType != 4 }">
											<li class="li22">包裹已出库，发货完成</li>
										</c:if>
										
										<li class="li33"></li>
										<li></li>
									</ul>
									<ul class="songhuo">
										<li class="li11">备货时间：${orderInfo.stokingTime }</li>
										<li class="li22">您的订单已经备货完成</li>
										<li class="li33"></li>
										<li></li>
									</ul>
									<ul class="songhuo">
										<li class="li11">提交时间：${orderInfo.orderTime }</li>
										<li class="li22">您提交了订单，订单进入华南仓准备备货</li>
										<li class="li33"></li>
										<li></li>
									</ul>
								</c:if>
							</div>
						</div>
						<div id="con_xmlistb_2" class="con_all" style="display:none;">
							<dl class="fukxinxi">
								<dd>付款方式：
									<c:if test="${orderInfo.pay == 1 }">
										线下支付
									</c:if>
									<c:if test="${orderInfo.pay == 2 }">
										支付宝支付
									</c:if>
									<c:if test="${orderInfo.pay == 3 }">
										微信支付
									</c:if>
								</dd>
								<dd>商品付款：￥${total }</dd>
								<dd>
									<span>运费金额：￥${orderInfo.freight }</span>&nbsp;&nbsp;&nbsp;<span>实际运费：￥${orderInfo.freight }</span>
								</dd>
								<dd>优惠总金额：￥
									<c:if test="${orderInfo.couponId == null }">
										0.00
									</c:if>
									<c:if test="${orderInfo.couponId != null }">
										${total-orderInfo.total }
									</c:if>
								</dd>
								<dd>应支付金额：￥${orderInfo.total }</dd>
								<dd>下单时间：${orderInfo.orderTime }</dd>
							</dl>
						</div>
					</div>
				</div>
			</div>

			<div class="dingdanxinxi_p">
				<p class="p1">订单信息</p>
				<ul>
					<li><span>收货人信息</span>
					</li>
					<li>收 货 人：${orderInfo.consignee }</li>
					<li>地 址：${orderInfo.address }</li>
					<li>手机号码：<i>${orderInfo.tel }</i>
					</li>
				</ul>
				<ul>
					<li><span>支付及配送方式</span>
					</li>
					<li>支付方式：
									<c:if test="${orderInfo.pay == 1 }">
										线下支付
									</c:if>
									<c:if test="${orderInfo.pay == 2 }">
										支付宝支付
									</c:if>
									<c:if test="${orderInfo.pay == 3 }">
										微信支付
									</c:if>
					</li>
					<li>运 费：<i>￥${orderInfo.freight }
						<c:if test="${ orderInfo.shoppingTypeCollect == true}">
							&nbsp;运费到付
						</c:if>
					</i>
					</li>
					<li>送货日期：工作日、双休日与假日均可送货</li>
				</ul>
<!-- 备注信息 -->				
				<ul>
					<li><span>备注</span>
					</li>
					<li><p>${orderInfo.dsc }</p></i>
					</li>
				</ul>

				
				<!-- 
				<ul>
					<li><span>发票信息</span>
					</li>
					<li>发票类型：
						<c:if test="${orderInfo.invoiceType == 1 }">
							不开票
						</c:if>
						<c:if test="${orderInfo.invoiceType == 2 }">
							普通发票
						</c:if>
						<c:if test="${orderInfo.invoiceType == 3 }">
							增值税发票
						</c:if>
					</li>
					<li>发票抬头：${orderInfo.consignee }</li>
					<li>发票内容：明细</li>
				</ul>
				 -->
				 <c:forEach items="${listGodsTest }" var="oglist" varStatus="status">
	                	<c:if test="${oglist.orderId == orderInfo.orderId }">
	                	<input type="hidden" name="orderIds" value="${oglist.orderId }" />
	                	<c:if test="${oglist.isParOrSim == '1'}">
	                	<input type="hidden" name="${oglist.orderId }simForPriceDan" value="${oglist.GSubtotal }" />
	                	</c:if>
	                	<c:if test="${oglist.isParOrSim == '0'}">
	                	<input type="hidden" name="${oglist.orderId }parForPriceDan" value="${oglist.GSubtotal }" />
	                	</c:if>
	                	</c:if>
	       </c:forEach>
	       <div class="new_details">
    <ul class="theader">
        <li class="li_1">
            <b>商品信息</b>
            <a href="javascript:;" class="zhankai">展开</a>
            <b>小计(元)</b>
            <b>采购总数</b>
        </li>
        <li class="li_2">金额(元)</li>
        <li class="li_3">实付款(元)</li>
    </ul>
    <table class="details_tab">
        <tbody>
            <tr>
                <td class="products">
                	<c:forEach items="${orderListDTO.orderInfoGoodsDTOList }" var="orderGoodsInfo" varStatus="staindex">
	                    <div class="div_${staindex.index+1 }">
	                        <div class="product_d1">
	                            <h5><a href="goods_detail?gid=${orderGoodsInfo.goodsId }" target="_blank">${orderGoodsInfo.goodsName }</a></h5>
	                            <p class="pp1">采购总数：</p>
	                            <p class="pp2">
									<c:if test="${orderGoodsInfo.simNum != 0 && orderGoodsInfo.parNum == 0 }">
				                    	<span>${orderGoodsInfo.simNum }(pcs) × ${orderGoodsInfo.simPrice }</span>
				                    </c:if>
				                    <c:if test="${orderGoodsInfo.simNum == 0 && orderGoodsInfo.parNum != 0 }">
				                    	<span>${orderGoodsInfo.parNum }(kpcs) × ${orderGoodsInfo.parPrice }</span>
				                    </c:if>
				                    <c:if test="${orderGoodsInfo.simNum != 0 && orderGoodsInfo.parNum != 0 }">
				                    	<span>${orderGoodsInfo.simNum }(pcs) × ${orderGoodsInfo.simPrice }</span><br>
				                    	<span>${orderGoodsInfo.parNum }(kpcs) × ${orderGoodsInfo.parPrice }</span>
				                    </c:if>
	                            </p>
	                            <p class="pp3">
	                            <c:if test="${orderGoodsInfo.simNum != 0 && orderGoodsInfo.parNum == 0 }">
				                	<span>&yen;${orderGoodsInfo.simPriceCount }</span>
				                </c:if>
				                <c:if test="${orderGoodsInfo.simNum == 0 && orderGoodsInfo.parNum != 0 }">
				                	<span>&yen;${orderGoodsInfo.parPriceCount }</span>
				                </c:if>
				                <c:if test="${orderGoodsInfo.simNum != 0 && orderGoodsInfo.parNum != 0 }">
				                	<span>&yen;${orderGoodsInfo.simPriceCount }</span><br>
				                	<span>&yen;${orderGoodsInfo.parPriceCount }</span>
				                </c:if>
	                            </p>
	                        </div>
	                        <div class="product_d2">${orderGoodsInfo.allCountNum }(pcs)</div>
	                    </div>
                		
                	</c:forEach>
                </td>
                <td class="price">&yen;${orderListDTO.goodsAllPrice }</td>
                <td class="pay">
                    <p>
	                    &yen; <span>${orderListDTO.total }</span> <br>
				        <c:if test="${orderListDTO.ferightDaoFu != '-1' }">
				            <a>(不含运费 &yen; <span>${orderListDTO.freight }</span>)<br/>到付</a>
				        </c:if>
				        <c:if test="${orderListDTO.ferightDaoFu == '-1' }">
				            <a>(含运费 &yen; <span>${orderListDTO.freight }</span>)</a>
				        </c:if>
			            <c:if test="${orderListDTO.orderStatus == 1 || orderListDTO.orderStatus == 3 || orderListDTO.orderStatus == 4 || orderListDTO.orderStatus == 5 || orderListDTO.orderStatus == 8 }">
			            </c:if>
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
				<div class="ct_dingdanlied" style="display:none">
	<div class="dingdanbianhao"><span>商品信息</span><span class="xiaoji">小计</span><span class="zongji">总计</span></div>

    <div class="ctlei_left">
	   <c:if test="${orderInfo.isParOrSim == '1-1' }">
        <div class="ct_pl_yp"><div class="line_l_pl">样片商品</div><span onclick="openShutManager(this,'box',false,'关闭','展开')">展开</span></div> 
        
            <div class="jine" name="${orderInfo.orderId }simZongPrice">
          
            </div>
                    
            <div class="caigoushu" name="123">
            	<c:forEach items="${goodsListAllDTO }" var="list">
	                <c:forEach items="${list }" var="oglist" varStatus="status">
	                	<c:if test="${oglist.orderId == orderInfo.orderId }">
		                	<c:if test="${oglist.isParOrSim == '1' }">
					                <dl name="shouxiansim${oglist.orderId }">
						                <dd>
						                	<p class="pt"><a href="goods_detail?gid=${oglist.ctGoods.GId }" target="_blank">${oglist.ctGoods.GName }</a></p>
						                	<p class="p2"><span class="s1">采购数量:</span><span class="s2">${oglist.GNumber } （PCS） &nbsp;&nbsp;&nbsp;<font style="color: black;">单价：</font>￥${oglist.GPrice }</span></p>
						                </dd>
					                </dl>
		                	</c:if>
	                	</c:if>
	            	</c:forEach>
            	</c:forEach>
                
                <div class="111" name="box" style="display:none">
                <c:forEach items="${goodsListAllDTO }" var="list">
	                <c:forEach items="${list }" var="oglist" varStatus="status">
	                	<c:if test="${oglist.orderId == orderInfo.orderId }">
		            		<c:if test="${oglist.isParOrSim == '1' }">
					                <dl name="houxiansim${oglist.orderId }">
						                <dd>
						                	<p class="pt"><a href="goods_detail?gid=${oglist.ctGoods.GId }" target="_blank">${oglist.ctGoods.GName }</a></p>
						                	<p class="p2"><span class="s1">采购数量:</span><span class="s2">${oglist.GNumber } （PCS） &nbsp;&nbsp;&nbsp;<font style="color: black;">单价：</font>￥${oglist.GPrice }</span></p>
						                </dd>
					                </dl>
		                	</c:if>
	                	</c:if>
	            	</c:forEach>
            	</c:forEach>
                </div>
                
                
                
            </div>
            <div class="ct_pl_pl"><div class="line_l_pl">批量商品</div><span onclick="openShutManager1(this,'box2',false,'关闭','展开')">展开</span></div>
            <div class="jine" name="${orderInfo.orderId }parZongPrice">
            </div>
            <!--  && sss.index+1 == fn:length(goodsListAll) -->
            <div class="caigoushu">
            	<c:forEach items="${goodsListAllDTO }" var="list">
	                <c:forEach items="${list }" var="oglist" varStatus="status">
	                	<c:if test="${oglist.orderId == orderInfo.orderId }">
		            		<c:if test="${oglist.isParOrSim == '0' }">
					                <dl name="shouxianpar${oglist.orderId }">
						                <dd>
						                	<p class="pt"><a href="goods_detail?gid=${oglist.ctGoods.GId }" target="_blank">${oglist.ctGoods.GName }</a></p>
						                	<p class="p2"><span class="s1">采购数量:</span><span class="s2">${oglist.GParNumber }（KPCS） &nbsp;&nbsp;&nbsp;<font style="color: black;">单价：</font>￥${oglist.GParPrice }</span></p>
						                </dd>
					                </dl>
		                	</c:if>
	                	</c:if>
	            	</c:forEach>
            	</c:forEach>
                <div class="" name="box2" style="display:none">
                 <c:forEach items="${goodsListAllDTO }" var="list">
	                <c:forEach items="${list }" var="oglist" varStatus="status">
	                	<c:if test="${oglist.orderId == orderInfo.orderId }">
		            		<c:if test="${oglist.isParOrSim == '0' }">
					                <dl name="houxianpar${oglist.orderId }">
						                <dd>
						                	<p class="pt"><a href="goods_detail?gid=${oglist.ctGoods.GId }" target="_blank">${oglist.ctGoods.GName }</a></p>
						                	<p class="p2"><span class="s1">采购数量:</span><span class="s2">${oglist.GParNumber } （KPCS） &nbsp;&nbsp;&nbsp;<font style="color: black;">单价：</font>￥${oglist.GParPrice }</span></p>
						                </dd>
					                </dl>
		                	</c:if>
	                	</c:if>
	            	</c:forEach>
            	</c:forEach>
                </div>
            </div>
</c:if>
        <c:if test="${orderInfo.isParOrSim == '0-1' }">
        <div class="ct_pl_pl"><div class="line_l_pl">批量商品</div><span onclick="openShutManager1(this,'box2',false,'关闭','展开')">展开</span></div>
            <div class="jine" name="${orderInfo.orderId }parZongPrice">
            </div>
            
            <div class="caigoushu">
            	<c:forEach items="${goodsListAllDTO }" var="list">
	                <c:forEach items="${list }" var="oglist" varStatus="status">
	                	<c:if test="${oglist.orderId == orderInfo.orderId }">
		            		<c:if test="${oglist.isParOrSim == '0' }">
					                <dl name="shouxianpar${oglist.orderId }">
						                <dd>
						                	<p class="pt"><a href="goods_detail?gid=${oglist.ctGoods.GId }" target="_blank">${oglist.ctGoods.GName }</a></p>
						                	<p class="p2"><span class="s1">采购数量:</span><span class="s2">${oglist.GParNumber } （KPCS） &nbsp;&nbsp;&nbsp;<font style="color: black;">单价：</font>￥${oglist.GParPrice }</span></p>
						                </dd>
					                </dl>
		                	</c:if>
	                	</c:if>
	            	</c:forEach>
            	</c:forEach>
                <div class="" name="box2" style="display:none">
                 <c:forEach items="${goodsListAllDTO }" var="list">
	                <c:forEach items="${list }" var="oglist" varStatus="status">
	                	<c:if test="${oglist.orderId == orderInfo.orderId }">
		            		<c:if test="${oglist.isParOrSim == '0' }">
					                <dl name="houxianpar${oglist.orderId }">
						                <dd>
						                	<p class="pt"><a href="goods_detail?gid=${oglist.ctGoods.GId }" target="_blank">${oglist.ctGoods.GName }</a></p>
						                	<p class="p2"><span class="s1">采购数量:</span><span class="s2">${oglist.GParNumber } （KPCS） &nbsp;&nbsp;&nbsp;<font style="color: black;">单价：</font>￥${oglist.GParPrice }</span></p>
						                </dd>
					                </dl>
		                	</c:if>
	                	</c:if>
	            	</c:forEach>
            	</c:forEach>
                </div>
            </div>
</c:if>
<c:if test="${orderInfo.isParOrSim == '1-0' }">
<div class="ct_pl_yp"><div class="line_l_pl">样片商品</div><span onclick="openShutManager(this,'box',false,'关闭','展开')">展开</span></div> 
        
            <div class="jine" name="${orderInfo.orderId }simZongPrice">
            </div>
                    
            <div class="caigoushu">
            	<c:forEach items="${goodsListAllDTO }" var="list">
	                <c:forEach items="${list }" var="oglist" varStatus="status">
	                	<c:if test="${oglist.orderId == orderInfo.orderId }">
		                	<c:if test="${oglist.isParOrSim == '1' }">
					                <dl name="shouxiansim${oglist.orderId }">
						                <dd>
						                	<p class="pt"><a href="goods_detail?gid=${oglist.ctGoods.GId }" target="_blank">${oglist.ctGoods.GName }</a></p>
						                	<p class="p2"><span class="s1">采购数量:</span><span class="s2">${oglist.GNumber } （PCS） &nbsp;&nbsp;&nbsp;<font style="color: black;">单价：</font>￥${oglist.GPrice }</span></p>
						                </dd>
					                </dl>
		                	</c:if>
	                	</c:if>
	            	</c:forEach>
            	</c:forEach>
                
                <div class="" name="box" style="display:none">
                <c:forEach items="${goodsListAllDTO }" var="list">
	                <c:forEach items="${list }" var="oglist" varStatus="status">
	                	<c:if test="${oglist.orderId == orderInfo.orderId }">
		            		<c:if test="${oglist.isParOrSim == '1' }">
					                <dl name="houxiansim${oglist.orderId }">
						                <dd>
						                	<p class="pt"><a href="goods_detail?gid=${oglist.ctGoods.GId }" target="_blank">${oglist.ctGoods.GName }</a></p>
						                	<p class="p2"><span class="s1">采购数量:</span><span class="s2">${oglist.GNumber } （PCS） &nbsp;&nbsp;&nbsp;<font style="color: black;">单价：</font>￥${oglist.GPrice }</span></p>
						                </dd>
					                </dl>
		                	</c:if>
	                	</c:if>
	            	</c:forEach>
            	</c:forEach>
                </div>
            </div>
</c:if>
    </div>
      <div class="ctlei_right" style="background:#fff;">
        <div class="jine">
        ¥${total }
        </div>

    </div>  
    

</div>   

        
    
			</div>


			<div class="zongjine_p">
				<ul class="jiafei_f">
					<li><p>运费：</p>
						<span><i>￥${orderInfo.freight }
						<c:if test="${ orderInfo.shoppingTypeCollect == true}">
							&nbsp;到付
						</c:if></i>
					</span>
					</li>
					<li><p>总商品金额：</p>
						<span><i>￥${total }</i>
					</span>
					</li>
					<li><p>优惠：</p>
						<span><i>-￥
							<c:if test="${orderInfo.couponId == null }">
										0.00
									</c:if>
									<c:if test="${orderInfo.couponId != null }">
										${orderInfo.discount }
									</c:if>
						</i>
					</span>
					</li>
				</ul>
				<ul class="zongfei_f">
					<li class="shifu_f"><p>实付款：</p>
						<span><i>￥${orderInfo.total }</i>
					</span>
					</li>
				</ul>
			</div>
<input type="hidden" name="orderIdss" value="${orderInfo.orderId }" />
		</div>
<script type="text/javascript">
	function getParPrice(){
	   		var orderIds = $("input[name=orderIds]");
		   		var orderIdTemp = 0;
		   		var orderIdsTwo = [];
		   		for(var i = 0,len = orderIds.length;i < len;i++){
		   			! RegExp(orderIds[i].value,"g").test(orderIdsTwo.join(",")) && (orderIdsTwo.push(orderIds[i].value));
		   			}
		   		
	   		for(var j = 0; j< orderIdsTwo.length; j++){
		   		var priceAll = 0;
		   		var priceAllsim = 0;
		   		var prices = document.getElementsByName(orderIdsTwo[j]+"parForPriceDan");
		   		var pricesSim = document.getElementsByName(orderIdsTwo[j]+"simForPriceDan");
		   		for(var i = 0; i < prices.length; i++){
		   			priceAll += Number(prices[i].value);
		   		}
		   		for(var i = 0; i < pricesSim.length; i++){
		   			priceAllsim += Number(pricesSim[i].value);
		   		}
		   		priceAll = Math.round(priceAll*1000)/1000;
		   		priceAllsim = Math.round(priceAllsim*1000)/1000;
		   		$("div[name="+orderIdsTwo[j]+"parZongPrice]").html("￥" + priceAll);
		   		$("div[name="+orderIdsTwo[j]+"simZongPrice]").html("￥" + priceAllsim);
	   		}
	   	}
	   	getParPrice();
	   	function kongZhiShouHou(){
	   		var orderIds = $("input[name=orderIdss]");
	   		for(var j = 0 ; j < orderIds.length; j++){
		   		var shousim = $("dl[name=shouxiansim"+orderIds[j].value+"]");
		   		var housim = $("dl[name=houxiansim"+orderIds[j].value+"]")
		   		var shoupar = $("dl[name=shouxianpar"+orderIds[j].value+"]");
		   		var houpar = $("dl[name=houxianpar"+orderIds[j].value+"]");
		   		if(shousim.length >= 3){
		   			for(var i = 3; i<shousim.length; i++){
		   				$("dl[name=shouxiansim"+orderIds[j].value+"]")[i].style.display="none";
		   			}
		   			for(var i = 0; i <= 2; i++){
		   				$("dl[name=houxiansim"+orderIds[j].value+"]")[i].style.display="none";
		   			}
		   		} else {
		   			for(var i = 0; i < housim.length; i++){
		   				$("dl[name=houxiansim"+orderIds[j].value+"]")[i].style.display="none";
		   			}
		   		}
		   		if(shoupar.length >= 3){
		   			for(var i = 3; i<shoupar.length; i++){
		   				$("dl[name=shouxianpar"+orderIds[j].value+"]")[i].style.display="none";
		   			}
		   			for(var i = 0; i <= 2; i++){
		   				$("dl[name=houxianpar"+orderIds[j].value+"]")[i].style.display="none";
		   			}
		   		} else {
		   			for(var i = 0; i < houpar.length; i++){
		   				$("dl[name=houxianpar"+orderIds[j].value+"]")[i].style.display="none";
		   			}
		   		}
	   		}
	   	}
	   	kongZhiShouHou();
	   	lookWuliu();
	    $(document).ready(function(){
	        var div=$(".details_tab td.products>div");
	        for(var i=0;i<div.length;i++){
	            if(i>=3){
	                $(div[i]).addClass("active");
	            }
	        }
	        $(".zhankai").click(function(){
	            var me=$(this);
	            var html=$(me).html();
	            var a=$(".details_tab td.products>div");
	            if(a.length<3){
		            $(a[a.length-1]).css("border-bottom","0");
		        }
		        if(html=="收起"){
		            html="展开";
		            for(var i=0;i< a.length;i++){
		                if(i>=3){
		                    $(a[i]).addClass("active");
		                }
		                $(a[2]).css("border-bottom","0");
		            }
		        }else if(html=="展开"){
		            html="收起";
		            for(var i=0;i< a.length;i++){
		                if(i>=3){
		                    $(a[i]).removeClass("active");
		                }
		            }
		            $(a[2]).css("border-bottom","1px solid #ddd");
		            $(a[a.length-1]).css("border-bottom","0");
		        }
		        $(me).html(html);
	        })
	    })
	    $.fn.toggle=function(me){
	        var html=$(me).html();
	        var a=$(".details_tab td.products>div");
	        if(a.length<3){
	            $(a[a.length-1]).css("border-bottom","0");
	        }
	        if(html=="收起"){
	            html="展开";
	            for(var i=0;i< a.length;i++){
	                if(i>=3){
	                    $(a[i]).addClass("active");
	                }
	                $(a[2]).css("border-bottom","0");
	            }
	        }else if(html=="展开"){
	            html="收起";
	            for(var i=0;i< a.length;i++){
	                if(i>=3){
	                    $(a[i]).removeClass("active");
	                }
	            }
	            $(a[2]).css("border-bottom","1px solid #ddd");
	            $(a[a.length-1]).css("border-bottom","0");
	        }
	        $(me).html(html);
	    } 
</script>
		<div class="clear"></div>
	</div>
<jsp:include page="../common/foot.jsp"></jsp:include>
		<div class="clear"></div>
	</div>
</body>
</html>


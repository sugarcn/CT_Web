<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<base href="<%=basePath%>">

<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
</head>

<body>

	<jsp:include page="../common/head.jsp"></jsp:include>
	<div class="contant_e">

		<div class="dizhilian_e">
			<a href="<%=basePath%>">首页</a>><a>客服中心</a>><span>我的投诉</span>
		</div>
		<jsp:include page="../common/left.jsp"></jsp:include>
		<div class="right_k">


			<div class="zpshzher">
				<div class="biao">我的退换货申请</div>

				<div class="ts_zong">
					<span class="wsl">待处理<i>${totals }</i>个</span><span
						class="wchl">处理完成<i>${total }</i>个</span>
				</div>

				<div class="ts_tit">
					<h2>退换货记录</h2>
				</div>

				<div class="ts_zhu">
					<ul class="tszhu_ul">
						<li class="ts_zhu_1">当前退货状态</li>
						<li class="ts_zhu_2">退货的订单</li>
						<li class="ts_zhu_3">退款日期</li>
						<li class="ts_zhu_4">退款金额</li>
					</ul>
					<c:forEach items="${orderList }" var="list">
						<dl class="tszhu_dl">
							<dd class="ts_zhu_1">
								${list.retStatus }
							</dd>
							<dd class="ts_zhu_2">${list.orderSn }</dd>
							<dd style="text-align: center;" class="ts_zhu_3">
								${list.retTime }
							</dd>
							<dd class="ts_zhu_4">
								${list.retNumber }
							</dd>
						</dl>
					</c:forEach>
					${pages.comStr }


				</div>

			</div>
		</div>

		<div class="clear"></div>
	</div>
	<jsp:include page="../common/foot.jsp"></jsp:include>

</body>
</html>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = "//"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>确认订单</title>
    <link href="<%=request.getContextPath() %>/css/css.css" type="text/css" rel="stylesheet"/>
	<link href="<%=request.getContextPath() %>/css/css2.css" type="text/css" rel="stylesheet"/>
	<link href="<%=request.getContextPath() %>/css/css3.css" type="text/css" rel="stylesheet"/>
	<link type="text/css" rel="stylesheet" href="<%=request.getContextPath() %>/css/style.css" />
	<script type="text/javascript" src="<%=request.getContextPath() %>/js/jqueryt.js"></script>
	<script src="<%=request.getContextPath() %>/js/script.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath() %>/js/ct.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/js/spxx.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/js/user.js" charset="utf-8"></script>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<script type="text/javascript">
function addcheck(){
	 var addcheck = document.getElementsByName("xh1");
	 for(var i=0;i<addcheck.length;i++){
	 	if(addcheck[i].checked){
       		var addid = addcheck[i].value;
       		document.getElementById("addid").value = addid;
            break;
            }
	 }
}

function checkpost(){
	var postcheck = document.getElementsByName("sh1");
	for(var i=0;i<postcheck.length;i++){
	 	if(postcheck[i].checked){
       		var post = postcheck[i].value;
       		document.getElementById("post").value = post;
       		document.getElementById("cpost").innerText = "￥"+post;
       		var money = document.getElementById("total").value*1;
       		var p = post*1;
       		var sum = money+p;
       		document.getElementById("money").innerText = "￥"+sum;
            break;
            }
	 }
}
function checkbill(){
	var billcheck = document.getElementsByName("fa");
	for(var i=0;i<billcheck.length;i++){
		if(billcheck[i].checked){
			var bill = billcheck[i].value;
			document.getElementById("bill").value = bill;
			break;
		}
	}
}

function showProvince2(regionId){
              $.post("<%=basePath%>queryPro_region?regionId="+regionId,function(json){
              var list=json.plist;
              for(var i=0;i<list.length;i++){
			        $("#province2").append("<option value='"+list[i].regionId+"'>"+list[i].regionName+"</option>");
              }
			},"json");
	}
         
         function showCity2(regionId){
		     $.post("<%=basePath%>queryPro_region?regionId="+regionId,function(json){
		          var list=json.plist;
		          for(var i=0;i<list.length;i++){
				          $("#city2").append("<option value='"+list[i].regionId+"'>"+list[i].regionName+"</option>");
		          }
			},"json");
         } 
         function showDistrict2(regionId){
		          $.post("<%=basePath%>queryPro_region?regionId="+regionId,function(json){
		              var list=json.plist;
		              for(var i=0;i<list.length;i++){
				           $("#district2").append("<option value='"+list[i].regionId+"'>"+list[i].regionName+"</option>");
	              	}
				},"json");
         }
</script>
  </head>
  
  <body>
  
  <jsp:include page="../common/head.jsp"></jsp:include>
  	<div class="contant_f">
<ul class="gou_zhuang_f">
<li class="zhuang_b1">我的购物车</li>
<li class="zhuang_b2">填写核对订单</li>
<li class="zhuang_b3">订单完成</li>
</ul>
<form id="ordercheck" action="" method="post">
<div class="tian_bot_f">

    <div class="tit_t_f">收货地址</div>
    <div class="tit_m_f">
    	<s:iterator value="addressList" var="add">
    	<s:if test="#add.isdefault==0">
    		<dl class="cunzai_f">
            <dt>
            <input name="xh1" type="radio" value="${add.AId }" checked onclick="javascript:addcheck()" /><span class="moren_f">使用默认地址</span>
            <span>收货人： ${add.AConsignee }</span></dt>
            <dd>
            <p><span><i>地址：</i>${add.country }${add.province }${add.city }${add.district }${add.address }</span><span><i>电话：</i>${add.tel }</span><span><i>手机：</i>${add.mb }</span><span><i>邮编：</i>${add.zipcode }</span><span><i>快递备注：</i>${add.adesc}</span></p>
            </dd>
        </dl>
        </s:if>
    	</s:iterator>
    	<s:iterator value="addressList" var="add1">
    		<s:if test="#add1.isdefault==1">
    		<dl class="cunzai_f">
            <dt>
            <input name="xh1" type="radio" value="${add1.AId }" onclick="javascript:addcheck()" />
            <span>收货人： ${add1.AConsignee }</span></dt>
            <dd>
            <p><span><i>地址：</i>${add1.country }${add1.province }${add1.city }${add1.district }${add1.address }</span><span><i>电话：</i>${add1.tel }</span><span><i>手机：</i>${add1.mb }</span><span><i>邮编：</i>${add1.zipcode }</span><span><i>快递备注：</i>${add.adesc}</span></p>
            </dd>
        </dl>
        </s:if>
    	</s:iterator>
        <dl class="cunzaixin_f">
            <dt><a href="javascript:;" id="btn_bm">+ 新增收货地址</a>您已创建<span>${addsize }</span>个收货地址，最多可创建<span>20</span>个</dt>
            <dd class="BMchuangdi_f" style="display:none;" > 	
<div class="lchadi_f"><span class="guan_ff"><img src="<%=imgurl %>/bg/chadiao.gif" /></span></div>
<ul class="shoujizhuce">
<li class="li1"><span class="s1">使用新地址</span><span class="s3">带“<span class="s2">*</span>”为必填项！</span></li>
				<div class="msg-up" style="display: none;"></div>
				<li class="li2">
				  <select id="country2" name="country" onchange="showProvince2(this.value)">
				    <option value="0">---请选择---</option>
				    <s:iterator value="regions" var="list">
				    <option value="${list.regionId}" >${list.regionName}</option>
				    </s:iterator>
				    </select>
				  <select id="province2" name="province" onchange="showCity2(this.value)">
				    <option value="0">---请选择---</option>
				    </select>
				  <select id="city2" name="city" onchange="showDistrict2(this.value)">
				    <option value="0">---请选择---</option>
				    </select>
				     <select id="district2" name="district" >
				    <option value="0">---请选择---</option>
				    </select>
				  <span class="s4">*</span><span class="s5">请选择</span>
				  <span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="region2Tip"></span>
				</li>
				<li class="li3">详细地址：<input id="address2" name="address" type="text" class="jj" onblur="checkName();"/><span class="s6">*</span><span class="s7">请详细填正确地址</span>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="address2Tip"></span>
				</li>
				<li class="li4">邮编：<input id="zipcode2" name="zipcode" type="text" class="jj"/>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="zipcode2Tip"></span>
				</li>
				<li class="li5">收货人姓名：<input id="AConsignee2" name="AConsignee" type="text" class="jj"  onblur="checkAddress();"/><span class="s6">*</span><span class="s7">收货人姓名  先生/女士</span>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="AConsignee2Tip"></span>
				</li>
				<li class="li3">手机号码：<input id="tel2" name="tel" type="text" class="jj"/>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="tel2Tip"></span>
				</li>
				<li class="li3">电话号码：<input id="mb2" name="mb" type="text" class="jj"/><span class="s6">*手机、电话</span><span class="s7">两者至少填一项</span>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="mb2Tip"></span>
				</li>
				<li class="li3">快递备注：<input id="adesc" name="adesc" type="text" class="jj"/>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="descTip"></span>
				</li>
				<li class="li6"><a href="javascript:saveAddressForOrder()">保存收货人地址</a></li>
</ul>

            </dd>          
        </dl>        
    </div>
    
    <div class="tit_t_f">物流配送方式</div>
    <div class="tit_m_f">
        <ul class="mianyou_f">
       <li class="qufei_f"><input name="sh1" type="radio" value="0" checked onclick="checkpost()"/><span>区内（免邮费）</span> <a href="javascript:;" id="btn_by">查看区内范围</a></li> 
       <li><input name="sh1" type="radio" value="80" onclick="checkpost()" /><span>区外（快递费用：<i>80</i>元）</span></li> 
       <li class="BMchuangmian_f tan_mian_f" style="display:none;">
<div class="lchamian_f"><span><img src="<%=imgurl %>/bg/chadiao.gif" /></span></div>
<div class="tizp">福田区、罗湖区、南山区、盐田区、宝安区、龙岗区、光明新区、坪山新区、龙华新区、大鹏新区</div>
       </li>
        </ul>
    </div>
    
    
    <div class="tit_t_f">支付方式</div>
    <div class="tit_m_f">
    <p class="bao_h_f"><span><input name="xh" type="radio" value="xh1" checked />银行转账</span></p>
    </div>
    
    <div class="tit_t_f">商品清单</div>
    <div class="tit_m_f">
    
	<dl class="gou_che">
    <dd class="fanhui_f"><a href="<%=request.getContextPath() %>/goods_cart" >[ 返回修改我的购物车 ]</a></dd>
    <dt><div class="gou_ct_2">商品信息</div><div class="gou_ct_3">单价(元)</div><div class="gou_ct_4">包装</div><div class="gou_ct_5">采集数量</div><div class="gou_ct_6">金额（元）</div><dt>
    <!--循环开始 -->
    <s:iterator value="ocList" var="ocgoods">
        <dd>
    <div class="gou_ct_2">
    	<s:if test="#ocgoods.PUrl=='nourl' ">
    	<img src="<%=request.getContextPath() %>/<%=imgurl %>/ct_s.gif" />
    	</s:if><s:else>
        <img src="<%=request.getContextPath() %>/${ocgoods.PUrl }" />
        </s:else>
        <div class="ping_m_f"><p>${ocgoods.GName }</p></div>
    </div>
    <div class="gou_ct_3">
    	￥<s:if test="#ocgoods.promotePrice!=9999">${ocgoods.promotePrice }</s:if>
    	 <s:else>${ocgoods.shopPrice }</s:else>
    	/${ocgoods.GUnit }
    </div>
    <div class="gou_ct_4">
    <s:if test="#ocgoods.pack1Num != null">
	    <s:if test="#ocgoods.pack==1">${ocgoods.pack1Num }${ocgoods.GUnit }/${ocgoods.pack1 }</s:if>
	    <s:elseif test="#ocgoods.pack==2">${ocgoods.pack2Num }${ocgoods.GUnit }/${ocgoods.pack2 }</s:elseif>
	    <s:elseif test="#ocgoods.pack==3">${ocgoods.pack3Num }${ocgoods.GUnit }/${ocgoods.pack3 }</s:elseif>
	    <s:elseif test="#ocgoods.pack==4">${ocgoods.pack4Num }${ocgoods.GUnit }/${ocgoods.pack4 }</s:elseif>
    </s:if>
    <s:else>${ocgoods.pack }</s:else>
    </div>
    <div class="gou_ct_5">${ocgoods.GNumber }</div>
    <div class="gou_ct_6">${ocgoods.GPrice }元</div>
    <input type="hidden" name="gidPrice" id="${ocgoods.GId }gidPrice" value="${ocgoods.GPrice }|${ocgoods.GId }" />
        </dd>
    </s:iterator>
       
    <!--循环结束 -->
	</dl>
    </div>
    <script type="text/javascript">
    		var cunId = 0;
    		var isOk = false;
    		var jia = 0;
    		var zheKou = 0;
    		var showNum = 0;
    		function getCoupon(){
    			if(showNum==0){
    				showNum++;
    				$("#Coupon").show();
    			} else {
    				showNum=0;
    				cunId = 0;
    				isOk = false;
    				jia = 0;
    				 var totalZhe = $("#totalZhe").val();
    				document.getElementById("totalSum").innerText="￥"+totalZhe;
    				document.getElementById("sale").innerText="￥0";
    				document.getElementById("money").innerText="￥"+totalZhe;
    				$("input[name=syy]:checked").attr("checked",false);
    				$("#Coupon").hide();
    			}
    		}
    		function xuanzhong(){
    			var syy = $("input[name=syy]:checked").attr("value");
    			var syys = syy.split("-");
    			var totalZhe = $("#totalZhe").val();
    			if(syys[1] == 1){
    				isOk = true;
    				radioCoupon(syys[2]);
    					var zheSum = totalZhe - syys[0];
    					var you = totalZhe-zheSum;
    					document.getElementById("totalSum").innerText="￥"+zheSum;
    					document.getElementById("sale").innerText="￥"+you;
    					document.getElementById("money").innerText="￥"+zheSum;
    					jia = you;
    					zheKou = syys[0];
    					$("#total").val(zheSum);
    			}
    			if(syys[1] == 2){
    				isOk = true;
    				radioCoupon(syys[2]);
    				var zheSum = 0;
    				var you =0;
    				if(parseFloat(totalZhe) < parseFloat(syys[0])){
    					zheSum=0;
    					you = totalZhe;
    				} else {
	    				zheSum = totalZhe - syys[0];
	    				you = totalZhe-zheSum;
    				}
					document.getElementById("totalSum").innerText="￥"+zheSum;
					document.getElementById("sale").innerText="￥"+you;
					document.getElementById("money").innerText="￥"+zheSum;
					$("#total").val(zheSum);
					zheKou = syys[0];
					jia = you;
    			}
    			if(syys[1] == 3){
    				var prices = "";
    				$("input[name='gidPrice']").each(function(){
    					prices += $(this).val()+",";
    				});
    				var gids = "";
		    		var priceAndGid = prices.split(",");
    				for(var i=0; i<priceAndGid.length; i++){
    					var fen = priceAndGid[i].split("|");
    					if(fen[1]==syys[2]){
    						isOk = true;
		    				radioCoupon(syys[3]);
		    				var couAndGids = gids.split(",");
		    				var zheSum = fen[0]*(syys[0]/10);
		    				var you = fen[0]-zheSum;
		    				totalZhe -= you;
							document.getElementById("totalSum").innerText="￥"+totalZhe;
							document.getElementById("sale").innerText="￥"+you;
							document.getElementById("money").innerText="￥"+totalZhe;
							$("#total").val(totalZhe);
							jia = you;
							zheKou = syys[0];
							return;
    					}
    				}
    			}
    		}
    	$(function(){
    	})

    	function radioCoupon(id){
    		if (isOk == true){
	    		$("#"+id+"cun").val(id);
	    		cunId = id;
	    		return id;
    		} else {
    			return 0;
    		}
    	}
    	function checkorder(){
    		addcheck();
	   		if(cunId == 0){
	   			document.getElementById("ordercheck").action="order_finish";
	   		} else {
	    		document.getElementById("ordercheck").action="order_finish?orderDTO.cupon="+cunId+"&couponPrice="+jia + "&orderDTO.discount=" + zheKou;
	   		}
	   		document.getElementById("ordercheck").submit();
	    	}
    </script>
    <div class="tit_t_f"><a href="javascript:getCoupon();" id="btn_bysyy"><i>+ </i>使用优惠券</a></div>
    <div class="tit_m_f">
        <div class="BMchuangmian_fsyy tit_m_fsyy" id="Coupon" style="display:none;">
<div class="lchamian_fsyy"></div>
<div class="tizpery">
		<ul class="ul_tit">
		   <li class="li31">选择优惠券</li>
		   <li class="li32">优惠商品</li>
		   <li class="li33">有效期</li>
		 </ul>
		 <c:forEach items="${couponsAll }" var="list">
		 	<c:forEach items="${list }" var="list1">
		 		<input type="hidden" id="${list1.CDetailId}cun" value="" />
		 		<c:if test="${list1.ctCoupon.couponType == 1 }">
					<ul class="ul3">
					   <li class="li31"><input name="syy" onclick="xuanzhong()" type="radio" value="${list1.ctCoupon.discount }-${list1.ctCoupon.couponType}-${list1.CDetailId}"  /><img src="<%=imgurl %>/bg/manjian.gif"/>						   <span>满${list1.ctCoupon.amount }减${list1.ctCoupon.discount }</span></li>
					   <li class="li32">${list1.ctCoupon.goods.GName }</li>
					   <li class="li33">${list1.ctCoupon.etime }</li>
					 </ul>
		 		</c:if>
		 		<c:if test="${list1.ctCoupon.couponType == 2 }">
					<ul class="ul3">
					   <li class="li31"><input name="syy" onclick="xuanzhong()"  type="radio" value="${list1.ctCoupon.discount }-${list1.ctCoupon.couponType}-${list1.CDetailId}"  /><img src="<%=imgurl %>/bg/jiantou.gif"/>
					   <span>抵${list1.ctCoupon.discount }元</span></li>
					   <li class="li32">所有商品</li>
					   <li class="li33">${list1.ctCoupon.etime }</li>
					 </ul>
		 		</c:if>
		 		<c:if test="${list1.ctCoupon.couponType == 3 }">
		 		<input type="hidden" name="couByGid" value="${list1.ctCoupon.GId }" />
					<ul class="ul3">
					   <li class="li31"><input
					   	<c:if test="${list1.ctCoupon.amount > total }">
					   		disabled="disabled"  
					   	</c:if>
					    name="syy" type="radio" onclick="xuanzhong()"  value="${list1.ctCoupon.discount }-${list1.ctCoupon.couponType}-${list1.ctCoupon.GId }-${list1.CDetailId}"  /><img src="<%=imgurl %>/bg/zhe.gif"/>
					   <span>满${list1.ctCoupon.amount }打${list1.ctCoupon.discount }折</span></li>
					   <li class="li32">${list1.ctCoupon.goods.GName }</li>
					   <li class="li33">${list1.ctCoupon.etime }</li>
					 </ul>
		 		</c:if>
		 	</c:forEach>
		 </c:forEach> 
</div>
        </div>
    </div>

</div>



<div class="tian_fot_f">
	<dl class="buchongs_f">
    <dt>补充说明：</dt>
    <dd><input class="textbox1" id="remark" name="remark" maxlength="45" size="15" style="color: rgb(204, 204, 204); padding: 3px;" value="限45个字" onblur="if(this.value==''||this.value=='限45个字'){this.value='限45个字';this.style.color='#cccccc'}" onfocus="if(this.value=='限45个字') {this.value='';};this.style.color='#000000';" type=""></dd>
    </dl>
    <ul class="jiafei_f">
      <li><p>运费：</p><span><i id="cpost">￥0</i>元</span></li>
      <li><p>总商品金额：</p><span><i id="totalSum">￥${total }</i>元</span></li>
      <input type="hidden" id="totalZhe" value="${total }" />
      <li><p>优惠：</p><span><i id="sale">-￥0</i>元</span></li>
    </ul>
    <ul class="zongfei_f">
      <li class="shifu_f"><p>实付款：</p><span><i id="money">￥${total }</i>元</span></li>
      <li>寄送至：${address }</li>
      <li>收货人：${name }</li>
      <li>电　话：${mp }</li>
      <li class="tijiaodd"><a href="javascript:void()" onclick="checkorder()">提交订单</a></li>
    </ul>
	<input type="hidden" name="orderDTO.total" id="total" value="${total }" />
	<input type="hidden" name="addid" id="addid" value="${addid }" />
	<input type="hidden" name="orderDTO.post" id="post" value="0" />
	<input type="hidden" name="orderDTO.bill" id="bill" value="1" />
	<input type="hidden" name="orderDTO.billtype" id="type" value="1" />
	<input type="hidden" name="orderDTO.sptype" id="sptype" value="1" />
	<input type="hidden" name="orderDTO.gids" id="gids" value="${gids }" />
	<input type="hidden" name="orderDTO.pay" id="pay" value="1" />
</div>
</form>
<div class="clear"></div>
</div>
<jsp:include page="../common/foot.jsp"></jsp:include>

  </body>
</html>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
	<link type="text/css" rel="stylesheet" href="<%=request.getContextPath() %>/css/style.css" />
	<script type="text/javascript" src="<%=request.getContextPath() %>/js/jqueryt.js"></script>
	<script src="<%=request.getContextPath() %>/js/script.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath() %>/js/ct.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/js/spxx.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/order/js/order_check.js"></script>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<script type="text/javascript">
function addcheck(){
	 var addcheck = document.getElementsByName("xh1");
	 for(var i=0;i<addcheck.length;i++){
	 	if(addcheck[i].checked){
       		var addid = addcheck[i].value;
       		document.getElementById("addid").value = addid;
            break;
            }
	 }
}
function checkpost(){
	var postcheck = document.getElementsByName("sh1");
	for(var i=0;i<postcheck.length;i++){
	 	if(postcheck[i].checked){
       		var post = postcheck[i].value;
       		document.getElementById("post").value = post;
       		document.getElementById("cpost").innerText = "￥"+post;
       		var money = document.getElementById("total").value*1;
       		var p = post*1;
       		var sum = money+p;
       		document.getElementById("money").innerText = "￥"+sum;
            break;
            }
	 }
}
function checkbill(){
	var billcheck = document.getElementsByName("fa");
	for(var i=0;i<billcheck.length;i++){
		if(billcheck[i].checked){
			var bill = billcheck[i].value;
			document.getElementById("bill").value = bill;
			break;
		}
	}
}
function checkorder(){
	document.getElementById("ordercheck").action="bom_finish";
	document.getElementById("ordercheck").submit();
}
function showProvince2(regionId){
              $.post("<%=basePath%>queryPro_region?regionId="+regionId,function(json){
              var list=json.plist;
              for(var i=0;i<list.length;i++){
              $("#province2").append("<option value='"+list[i].regionId+"'>"+list[i].regionName+"</option>");
              }
			},"json");
         }
         
         function showCity2(regionId){
         $.post("<%=basePath%>queryPro_region?regionId="+regionId,function(json){
              var list=json.plist;
              for(var i=0;i<list.length;i++){
              $("#city2").append("<option value='"+list[i].regionId+"'>"+list[i].regionName+"</option>");
              }
			},"json");
         }
         function showDistrict2(regionId){
          $.post("<%=basePath%>queryPro_region?regionId="+regionId,function(json){
              var list=json.plist;
              for(var i=0;i<list.length;i++){
              $("#district2").append("<option value='"+list[i].regionId+"'>"+list[i].regionName+"</option>");
              }
			},"json");
         }
</script>
  </head>
  
  <body>
  
  <jsp:include page="../common/head.jsp"></jsp:include>
  
  	<div class="contant_f">


<ul class="gou_zhuang_f">
<li class="zhuang_b1">我的购物车</li>
<li class="zhuang_b2">填写核对订单</li>
<li class="zhuang_b3">订单完成</li>
</ul>
<form id="ordercheck" action="" method="post">
<div class="tian_bot_f">

    <div class="tit_t_f">收货地址</div>
    <div class="tit_m_f">
    	<s:iterator value="addressList" var="add">
    	<s:if test="#add.isdefault==1">
    		<dl class="cunzai_f">
            <dt>
            <input name="xh1" type="radio" value="${add.AId }" checked onclick="javascript:addcheck()" /><span class="moren_f">使用默认地址</span>
            <span>收货人： ${add.AConsignee }</span></dt>
            <dd>
            <p><span><i>地址：</i>${add.country }${add.province }${add.city }${add.district }${add.address }</span><span><i>电话：</i>${add.tel }</span><span><i>手机：</i>${add.mb }</span><span><i>邮编：</i>${add.zipcode }</span></p>
            </dd>
        </dl>
        </s:if>
    	</s:iterator>
    	<s:iterator value="addressList" var="add1">
    		<s:if test="#add1.isdefault!=1">
    		<dl class="cunzai_f">
            <dt>
            <input name="xh1" type="radio" value="${add1.AId }" onclick="javascript:addcheck()" />
            <span>收货人： ${add1.AConsignee }</span></dt>
            <dd>
            <p><span><i>地址：</i>${add1.country }${add1.province }${add1.city }${add1.district }${add1.address }</span><span><i>电话：</i>${add1.tel }</span><span><i>手机：</i>${add1.mb }</span><span><i>邮编：</i>${add1.zipcode }</span></p>
            </dd>
        </dl>
        </s:if>
    	</s:iterator>
        <dl class="cunzaixin_f">
            <dt><a href="javascript:;" id="btn_bm">+ 新增收货地址</a>您已创建<span>${addsize }</span>个收货地址，最多可创建<span>20</span>个</dt>
            <dd class="BMchuangdi_f" style="display:none;" >

<div class="lchadi_f"><span class="guan_ff"><img src="<%=imgurl %>/bg/chadiao.gif" /></span></div>
<ul class="shoujizhuce">
<li class="li1"><span class="s1">使用新地址</span><span class="s3">带“<span class="s2">*</span>”为必填项！</span></li>
				<li class="li2">
				  <select id="country2" name="country" onchange="showProvince2(this.value)">
				    <option value="0" >---请选择---</option>
				    <s:iterator value="regions" var="list">
				    <option value="${list.regionId}" >${list.regionName}</option>
				    </s:iterator>
				    </select>
				  <select id="province2" name="province" onchange="showCity2(this.value)">
				    <option value="0">---请选择---</option>
				    </select>
				  <select id="city2" name="city" onchange="showDistrict2(this.value)">
				    <option value="0">---请选择---</option>
				    </select>
				     <select id="district2" name="district" >
				    <option value="0">---请选择---</option>
				    </select>
				  <span class="s4">*</span><span class="s5">请选择</span>
				  <span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="region2Tip"></span>
				</li>
				<li class="li3">详细地址：<input id="address2" name="address" type="text" class="jj"/><span class="s6">*</span><span class="s7">请详细填正确地址</span>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="address2Tip"></span>
				</li>
				<li class="li4">邮编：<input id="zipcode2" name="zipcode" type="text" class="jj"/>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="zipcode2Tip"></span>
				</li>
				<li class="li5">收货人姓名：<input id="AConsignee2" name="AConsignee" type="text" class="jj"/><span class="s6">*</span><span class="s7">收货人姓名  先生/女士</span>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="AConsignee2Tip"></span>
				</li>
				<li class="li3">手机号码：<input id="tel2" name="tel" type="text" class="jj"/>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="tel2Tip"></span>
				</li>
				<li class="li3">电话号码：<input id="mb2" name="mb" type="text" class="jj"/><span class="s6">*手机、电话</span><span class="s7">两者至少填一项</span>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="mb2Tip"></span>
				</li>
				<li class="li6"><a href="javascript:saveAddress()">保存收货人地址</a></li>
</ul>

            </dd>
        </dl>    
    </div>
    
    <div class="tit_t_f">物流配送方式</div>
    <div class="tit_m_f">
        <ul class="mianyou_f">
       <li class="qufei_f"><input name="sh1" type="radio" value="0" checked onclick="checkpost()"/><span>区内（免邮费）</span> <a href="javascript:;" id="btn_by">查看区内范围</a></li> 
       <li><input name="sh1" type="radio" value="80" onclick="checkpost()" /><span>区外（快递费用：<i>80</i>元）</span></li> 
       <li class="BMchuangmian_f tan_mian_f" style="display:none;">
<div class="lchamian_f"><span><img src="<%=imgurl %>/bg/chadiao.gif" /></span></div>
<div class="tizp">福田区、罗湖区、南山区、盐田区、宝安区、龙岗区、光明新区、坪山新区、龙华新区、大鹏新区</div>
       </li>
        </ul>
    </div>
    
    <div class="tit_t_f">开具发票</div>
    <div class="tit_m_f">
    
<div class="fapiaokaij"><p>发票开具方式：</p><p><input name="fa" type="radio" value="1" id="btn_no" checked onclick="checkbill()"/><span>不开发票</span></p><p><input name="fa" type="radio" value="2" id="btn_bp" onclick="checkbill()"/><span>普通发票</span></p><p><input name="fa" type="radio" value="3"  id="btn_bzz" onclick="checkbill()"/><span>增值税发票</span></p></div>

<div class="BMchuangpu_f tan_pu_f" style="display:none;">
    <div class="fapiaokaijnei"><p>发票抬头：</p></div>
    <div class="pufa_f">
        <p><input name="fab" type="radio" value="xh1" id="btn_gr" checked />个人</p>
        <p><input name="fab" type="radio" value="xh1" id="btn_ban"/>单位</p>
        <div class="BMchuangdan_f tan_dan_f" style="display:none;"> <input class="textbox1" id="remarkText" maxlength="45" size="15" style="color: rgb(204, 204, 204); padding: 3px;" value="请输入单位名称" onblur="if(this.value==''||this.value=='请输入单位名称'){this.value='请输入单位名称';this.style.color='#cccccc'}" onfocus="if(this.value=='请输入单位名称') {this.value='';};this.style.color='#000000';" type=""></div>
        <div class="fapiaomxi"><p>发票内容：</p><p><input name="fabm" type="radio" value="xh1" checked />明细</p></div>
    </div>
</div>

<div class="BMchuangzz_f tan_pu_f" style="display:none;">
    <ul class="wenxint_f">
    <li class="titis_f">温馨提示</li>
	<li>如您是首次开具增值税专用发票，请您至 "<span>会员中心->个人中心-><a href="<%=basePath%>ticket_list">增票信息设置</a></span>"中填写纳税人识别号等开票信息，并上传 加盖公章的营业执照副本、税务登记证副本、一般纳税人资格证书及银行开户 许可证扫描件至长亭，收到您的开票资料后，我们会尽快审核。</li>
	<li>注意：有效增值税发票开票资质仅为一个。</li>
    </ul>
    <dl class="zzfapiao_f">
<!-- 循环开始 -->
    <dd><span class="zzname_f">单位名称：</span><span class="zzcheng_f">长亭电子有限公司</span></dd>
    <dd><span class="zzname_f">纳税人识别名：</span><span class="zzcheng_f">Safjloi_984614634156</span></dd>
    <dd><span class="zzname_f">注册地址：</span><span class="zzcheng_f">深圳市福田区上步工业区友谊路万源大厦419室</span></dd>
    <dd><span class="zzname_f">注册电话：</span><span class="zzcheng_f">025-316954984</span></dd>
    <dd><span class="zzname_f">开户银行：</span><span class="zzcheng_f">11224455787976416</span></dd>
    <dd><span class="zzname_f">银行帐户：</span><span class="zzcheng_f">南京银行</span></dd>
<!-- 循环结束 -->
    <dd class="zzname_f"><a href="/" target="_blank">维护增票资质</a></dd>
    </dl>
    
</div>

    </div>
    
    <div class="tit_t_f">支付方式</div>
    <div class="tit_m_f">
    <p class="bao_h_f"><span><input name="xh" type="radio" value="xh1" checked />银行转账</span></p>
    </div>
    
    <div class="tit_t_f">商品清单</div>
    <div class="tit_m_f">
    
	<dl class="gou_che">
    <dd class="fanhui_f"><a href="<%=request.getContextPath() %>/bom_list" >[ 返回修改我的bom ]</a></dd>
    <dt><div class="gou_ct_2">商品信息</div><div class="gou_ct_3">单价(元)</div><div class="gou_ct_4">包装</div><div class="gou_ct_5">采购数量</div><div class="gou_ct_6">金额（元）</div><dt>
    <!--循环开始 -->
        
    <s:iterator value="obList" var="ocgoods">
        <dd>
    <div class="gou_ct_2">
    	<s:if test="#ocgoods.PUrl=='nourl' ">
    	<img src="<%=request.getContextPath() %>/<%=imgurl %>/ct_s.gif" />
    	</s:if><s:else>
        <img src="<%=request.getContextPath() %>/${ocgoods.PUrl }" />
        </s:else>
        <div class="ping_m_f"><p>${ocgoods.GName }</p></div>
    </div>
    <div class="gou_ct_3">
    	￥<s:if test="#ocgoods.promotePrice!=9999">${ocgoods.promotePrice }</s:if>
    	 <s:else>${ocgoods.shopPrice }</s:else>
    	/${ocgoods.GUnit }
    </div>
    <div class="gou_ct_4">
    ${ocgoods.pack }
    </div>
    <div class="gou_ct_5">${ocgoods.goodsNum }</div>
    <div class="gou_ct_6">${ocgoods.GPrice }元</div>
        </dd>
    </s:iterator>
    
    <!--循环结束 -->
	</dl>
    </div>
    
    <div class="tit_t_f"><a href="javascript:;" id="btn_bysyy"><i>+ </i>使用优惠券</a></div>
    <div class="tit_m_f">
        <div class="BMchuangmian_fsyy tit_m_fsyy" style="display:none;">
<div class="lchamian_fsyy"><span><img src="<%=imgurl %>/bg/chadiao.gif" /></span></div>
<div class="tizpery">



		 <ul class="ul2">
		   <li class="li21">优惠券名称</li>
		   <li class="li22">商品名称</li>
		   <li class="li23">有效期</li>
		 </ul>
		 <s:iterator value="couponDetails" var="list">
		 	<ul class="ul3">
		 	<c:if test="${list.ctCoupon.couponType == '1' }">
		 		<li class="li31"><input name="syy" type="radio" value="xh1"  /><img src="<%=imgurl %>/bg/manjian.gif"/><span>满${list.ctCoupon.amount }减${list.ctCoupon.discount }</span></li>
		 	</c:if>
		 	<c:if test="${list.ctCoupon.couponType == '2' }">
		 		<li class="li31"><input name="syy" type="radio" value="xh1"  /><img src="<%=imgurl %>/bg/jiantou.gif"/><span>无限制使用</span></li>
		 	</c:if>
		 	<c:if test="${list.ctCoupon.couponType == '3' }">
		 		<li class="li31"><input name="syy" type="radio" value="xh1"  /><img src="<%=imgurl %>/bg/jiantou.gif"/><span>${list.ctCoupon.discount}折</span></li>
		 	</c:if>
		 	<c:if test="${list.ctCoupon.GId=='0' }">
		 		<li class="li32">适用于全网商品</li>
		 	</c:if>
		 	<c:if test="${list.ctCoupon.GId!='0' }">
		 		<c:if test="${list.ctCoupon.goods.GName == null }">
		 		  <li class="li32">商品不存在</li>
		 		</c:if>
		 		<c:if test="${list.ctCoupon.goods.GName != null }">
		 		 <li class="li32">${list.ctCoupon.goods.GName }</li>
		 		</c:if>
		 		
		 	</c:if>
		 	<li class="li33">${list.ctCoupon.stime }&nbsp至&nbsp${list.ctCoupon.etime }</li>
		 	</ul>
		 </s:iterator>
		<!--  <ul class="ul3">
		   <li class="li31"><input name="syy" type="radio" value="xh1"  /><img src="<%=imgurl %>/bg/manjian.gif"/><span>满1052151250减20</span></li>
		   <li class="li32">二极管</li>
		   <li class="li33">2015-02-25 20:00:00</li>
		 </ul> -->

</div>
        </div>
    </div>

</div>



<div class="tian_fot_f">
	<dl class="buchongs_f">
    <dt>补充说明：</dt>
    <dd><input class="textbox1" id="remark" name="remark" maxlength="45" size="15" style="color: rgb(204, 204, 204); padding: 3px;" value="限45个字" onblur="if(this.value==''||this.value=='限45个字'){this.value='限45个字';this.style.color='#cccccc'}" onfocus="if(this.value=='限45个字') {this.value='';};this.style.color='#000000';" type=""></dd>
    </dl>
    <ul class="jiafei_f">
      <li><p>运费：</p><span><i id="cpost">￥0</i>元</span></li>
      <li><p>总商品金额：</p><span><i>￥${total }</i>元</span></li>
      <li><p>优惠：</p><span><i>-￥0</i>元</span></li>
    </ul>
    <ul class="zongfei_f">
      <li class="shifu_f"><p>实付款：</p><span><i id="money">￥${total }</i>元</span></li>
      <li>寄送至：${address }</li>
      <li>收货人：${name }</li>
      <li>电　话：${mp }</li>
      <li class="tijiaodd"><a href="javascript:void()" onclick="checkorder()">提交订单</a></li>
      
    </ul>
	<input type="hidden" name="total" id="total" value="${total }" />
	<input type="hidden" name="addid" id="addid" value="${addid }" />
	<input type="hidden" name="post" id="post" value="0" />
	<input type="hidden" name="bill" id="bill" value="1" />
	<input type="hidden" name="billtype" id="type" value="1" />
	<input type="hidden" name="sptype" id="sptype" value="1" />
	<input type="hidden" name="bomid" id="bomid" value="${bomid }" />
	<input type="hidden" name="pay" id="pay" value="1" />
</div>
</form>
<jsp:include page="../common/foot.jsp"></jsp:include>

  </body>
</html>

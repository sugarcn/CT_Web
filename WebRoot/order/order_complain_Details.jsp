<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<base href="<%=basePath%>">

 <meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
</head>

<body>

	<jsp:include page="../common/head.jsp"></jsp:include>
	<div class="contant_e">

		<div class="dizhilian_e">
			<a href="<%=basePath %>" target="_blank">首页</a>><a target="_blank">订单中心</a>><span>我的订单</span>
		</div>
		<jsp:include page="../common/left.jsp"></jsp:include>
		<div class="right_k">


			<div class="zpshzher">
				<div class="biao">查看投诉详情</div>

				<div class="ts_zong">
					<span class="wsl">待处理<i>${totalNo }</i>个</span><span
						class="wchl">处理完成<i>${totalOk }</i>个</span>
				</div>

				<div class="ts_tit">
					<h2>查看投诉详情</h2>
				</div>

				<div class="ts_zhu_jie">
					<dl class="ts_neir">
						<dd>
							<div class="tx_tt">投诉类别：</div>
							<div class="tx_nei">
								<p>${complain.comType }</p>
							</div>
						</dd>
						<dd>
							<div class="tx_tt">涉及订单：</div>
							<div class="tx_nei">
								<p>${complain.orderInfo.orderSn }</p>
							</div>
						</dd>
						<dd>
							<div class="tx_tt">投诉内容：</div>
							<div class="tx_nei">
								<p>${complain.comContent }
								</p>
							</div>
						</dd>
						<dd>
							<div class="tx_tt">联系手机：</div>
							<div class="tx_nei">
								<p>${complain.UTel }</p>
							</div>
						</dd>
						<dd>
							<div class="tx_tt">上传图片：</div>
							<div class="tx_nei">
								<p>
									<img width="180px;" height="180px;" src="<%=resUrl %>/${complain.comFile}">
								</p>
							</div>
						</dd>
						<dd>
							<div class="tx_tt ">备注：</div>
							<div class="tx_nei">
								<p>${complain.comDesc }
								</p>
							</div>
						</dd>
						<dd>
							<div class="tx_tt ">回复内容：</div>
							<div class="tx_nei">
								<p style="height: 29px;">${complain.ansContent }</p>
							</div>
						</dd>
						<dd class="tishi_tx">
							<div class="tx_tt ">温馨提示：</div>
							<div class="tx_nei">
								<p style="height: 29px;">${complain.ansTips }</p>
							</div>
						</dd>
					</dl>
				</div>
			</div>
		</div>

		<div class="clear"></div>
	</div>
	<jsp:include page="../common/foot.jsp"></jsp:include>

</body>
</html>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="frm" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
  		<script type="text/javascript" src="<%=basePath %>js/jquery-1.11.3.js" charset="UTF-8"></script> 
    <base href="<%=basePath%>">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
    <meta name="Keywords" content="<%=key%>" />
<meta name="Description" content="<%=title%>" />
<title><%=title%></title>
	<script type="text/javascript" src="<%=basePath %>js/order_list.js" charset="UTF-8"></script> 

	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<script type="text/javascript" src="<%=basePath%>plugins/datepicker/date/WdatePicker.js"></script>
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
  </head>
  
  <body class="orderbody">
  
  <jsp:include page="../common/head.jsp"></jsp:include>
  	<div class="contant_e">

    <div class="dizhilian_e"><a href="<%=basePath %>" target="_blank">首页</a>><a>订单中心</a>><span>我的订单</span></div>
  <jsp:include page="../common/left.jsp"></jsp:include> 
    
	<div class="right_e">
       <div class="jiansuo_e">

       <ul  class="xggrxx">
       		<li class="tim_al"><span class="tim_ct">开始时间：</span>
       		<input type="hidden" value="" id="isChuChange" />
		       <input type="text" id="UStartDate" name="ctUser.UBirthday" value="${orderDTO.startTime }" onclick="WdatePicker()" class="Wdate" src="datepicker/skin/datePicker.gif"/>
       		</li>
       		<li class="tim_al"> <span class="tim_ct">结束时间：</span>
		      <input type="text" id="UEndDate" onchange="isDaThree();" value="${orderDTO.endTime }" name="ctUser.UBirthday" value="" onclick="WdatePicker()" class="Wdate" src="datepicker/skin/datePicker.gif"/>
       		</li>
       </ul>
<a href="javascript:findOrderBy();">检索</a><input name="textfield" type="text" id="textfield" value="${searchKey }" placeholder="订单编号/收货人/商品编号/商品名称"/>         
	   </div>


	  
	  <div class="myorders">
    <ul class="dingdanxinxi_en">
        <li class="nth1">
            <span class="nth1_span1">订单信息</span>
            <span class="nth1_span2">小计</span>
            <span class="nth1_span3">数量(pcs)</span>
        </li>
        <li class="nth2">金额(元)</li>
        <li class="nth3">实付款(元)</li>
        <li class="nth4">
            <select id="odtime" name="odtime" onchange="javascript:bytime()" >
                <option value="">下单时间</option>
                <option value="3">最近三个月</option>
                <option value="12">今年内</option>
            </select>
        </li>
        <li class="nth5">
            <select id="odstatus" name="odstatus" onchange="javascript:bystatus()">
                <option value="">全部状态</option>
                <option value="all">订单状态</option>
                <option value="0">待付款</option>
                <option value="1">已付款</option>
                <option value="2">待审核</option>
                <option value="3">配货中</option>
                <option value="4">已发货</option>
                <option value="5">已完成</option>
                <option value="6">取消</option>
                <option value="7">无效</option>
                <option value="8">已备货</option>
            </select>
        </li>
        <li class="nth6">操作</li>
    </ul>
    
    <c:forEach items="${orderListDTOList }" var="orderInfo">
	    <div class="ct_dingdann">
	        <div class="dingdan_header">
	            订单编号: <a  href="order_details?orderDTO.orderSn=${orderInfo.orderSn }" id="findOrderInfo" target="_blank" class="num">${orderInfo.orderSn }</a>
	            <a href="javascript:;" class="zhankai">收起</a>
	            <b></b>
	        </div>
	        <table class="dingdan_body">
	            <tr>
	                <td class="products">
		                <c:forEach items="${orderInfo.orderInfoGoodsDTOList }" var="orderGoodsInfo" varStatus="staindex">
		                    <div class="div_${staindex.index+1 }">
		                        <div class="product_d1">
		                            <h5><a href="goods_detail?gid=${orderGoodsInfo.goodsId }" target="_blank">${orderGoodsInfo.goodsName }</a></h5>
		                            <p class="p1">采购总数：</p>
			                        <p class="p2">
			                        	<c:if test="${orderGoodsInfo.simNum != 0 && orderGoodsInfo.parNum == 0 }">
			                            	<span>${orderGoodsInfo.simNum }(pcs) × ${orderGoodsInfo.simPrice }
			                            	</span>
			                        	</c:if>
			                        	<c:if test="${orderGoodsInfo.simNum == 0 && orderGoodsInfo.parNum != 0 }">
			                            	<span>${orderGoodsInfo.parNum }(kpcs) × ${orderGoodsInfo.parPrice }
			                            	</span>
			                        	</c:if>
			                        	<c:if test="${orderGoodsInfo.simNum != 0 && orderGoodsInfo.parNum != 0 }">
			                            	<span>${orderGoodsInfo.simNum }(pcs) × ${orderGoodsInfo.simPrice }
			                            	</span><br>
			                            	<span>${orderGoodsInfo.parNum }(kpcs) × ${orderGoodsInfo.parPrice }
			                            	</span>
			                        	</c:if>
			                        </p>
			                        <p class="p3">
			                        	<c:if test="${orderGoodsInfo.simNum != 0 && orderGoodsInfo.parNum == 0 }">
			                            	<span>&yen;${orderGoodsInfo.simPriceCount }
			                            		<c:if test="${orderGoodsInfo.isRefundSim == null || orderGoodsInfo.isRefundSim == 0}">
						                			<c:if test="${ orderInfo.orderStatus == 1 || orderInfo.orderStatus == 2 ||orderInfo.orderStatus == 3 }">
							                			<a href="javacsript:void(0);" onclick="refundGoods('${orderInfo.orderSn }','${orderGoodsInfo.goodsId }','sim');return false;">退</a>
						                			</c:if>
						                		</c:if>
								                <c:if test="${orderGoodsInfo.isRefundSim != null && orderGoodsInfo.isRefundSim != 0 }">
								                	<c:if test="${orderGoodsInfo.isRefundSim == 1 }"><a href="javascript:;">退款已提交</a></c:if>
										            <c:if test="${orderGoodsInfo.isRefundSim == 2 }"><a href="javascript:;">退款成功</a></c:if>
										            <c:if test="${orderGoodsInfo.isRefundSim == 3 }"><a href="javascript:;">退款失败</a></c:if>
								               </c:if>
								               </span>
			                        	</c:if>
			                        	<c:if test="${orderGoodsInfo.simNum == 0 && orderGoodsInfo.parNum != 0 }">
			                            	<span>&yen;${orderGoodsInfo.parPriceCount }
			                            		<c:if test="${orderGoodsInfo.isRefundPar == null || orderGoodsInfo.isRefundPar == 0}">
						                			<c:if test="${ orderInfo.orderStatus == 1 || orderInfo.orderStatus == 2 ||orderInfo.orderStatus == 3 }">
							                			<a href="javacsript:void(0);" onclick="refundGoods('${orderInfo.orderSn }','${orderGoodsInfo.goodsId }','par');return false;">退</a>
						                			</c:if>
						                		</c:if>
								                <c:if test="${orderGoodsInfo.isRefundPar != null && orderGoodsInfo.isRefundPar != 0 }">
								                	<c:if test="${orderGoodsInfo.isRefundPar == 1 }"><a href="javascript:;">退款已提交</a></c:if>
										            <c:if test="${orderGoodsInfo.isRefundPar == 2 }"><a href="javascript:;">退款成功</a></c:if>
										            <c:if test="${orderGoodsInfo.isRefundPar == 3 }"><a href="javascript:;">退款失败</a></c:if>
								               </c:if>
			                            	</span>
			                        	</c:if>
			                        	<c:if test="${orderGoodsInfo.simNum != 0 && orderGoodsInfo.parNum != 0 }">
			                            	<span>&yen;${orderGoodsInfo.simPriceCount }
			                            		<c:if test="${orderGoodsInfo.isRefundSim == null || orderGoodsInfo.isRefundSim == 0}">
						                			<c:if test="${ orderInfo.orderStatus == 1 || orderInfo.orderStatus == 2 ||orderInfo.orderStatus == 3 }">
							                			<a href="javacsript:void(0);" onclick="refundGoods('${orderInfo.orderSn }','${orderGoodsInfo.goodsId }','sim');return false;">退</a>
						                			</c:if>
						                		</c:if>
								                <c:if test="${orderGoodsInfo.isRefundSim != null && orderGoodsInfo.isRefundSim != 0 }">
								                	<c:if test="${orderGoodsInfo.isRefundSim == 1 }"><a href="javascript:;">退款已提交</a></c:if>
										            <c:if test="${orderGoodsInfo.isRefundSim == 2 }"><a href="javascript:;">退款成功</a></c:if>
										            <c:if test="${orderGoodsInfo.isRefundSim == 3 }"><a href="javascript:;">退款失败</a></c:if>
								               </c:if>
			                            	</span><br>
			                            	<span>&yen;${orderGoodsInfo.parPriceCount }
			                            		<c:if test="${orderGoodsInfo.isRefundPar == null || orderGoodsInfo.isRefundPar == 0}">
						                			<c:if test="${ orderInfo.orderStatus == 1 || orderInfo.orderStatus == 2 ||orderInfo.orderStatus == 3 }">
							                			<a href="javacsript:void(0);" onclick="refundGoods('${orderInfo.orderSn }','${orderGoodsInfo.goodsId }','par');return false;">退</a>
						                			</c:if>
						                		</c:if>
								                <c:if test="${orderGoodsInfo.isRefundPar != null && orderGoodsInfo.isRefundPar != 0 }">
								                	<c:if test="${orderGoodsInfo.isRefundPar == 1 }"><a href="javascript:;">退款已提交</a></c:if>
										            <c:if test="${orderGoodsInfo.isRefundPar == 2 }"><a href="javascript:;">退款成功</a></c:if>
										            <c:if test="${orderGoodsInfo.isRefundPar == 3 }"><a href="javascript:;">退款失败</a></c:if>
								               </c:if>
			                            	</span>
			                        	</c:if>
			                        </p>
		                        </div>
		                        <!--<div class="product_d2">${orderGoodsInfo.allCountNum }(pcs)</div>  -->
		                    </div>
		                </c:forEach>
		                    </td>
		                    <td class="products2">
		                     <c:forEach items="${orderInfo.orderInfoGoodsDTOList }" var="orderGoodsInfo" varStatus="staindex">
		                        <div class="div_${staindex.index+1 }">
	                            <div class="product_d2">${orderGoodsInfo.allCountNum }(pcs)</div>
	                            </div>
	                       </c:forEach>
	                       </td>
	                <td class="price">&yen; ${orderInfo.goodsAllPrice }</td>
	                <td class="pay">
	                    <p>
	                        &yen; <span>${orderInfo.total }</span> <br>
			                <c:if test="${orderInfo.ferightDaoFu != '-1' }">
			                    <a>(不含运费 &yen; <span>${orderInfo.freight }</span>)<br/>到付</a>
			                </c:if>
			                <c:if test="${orderInfo.ferightDaoFu == '-1' }">
			                    <a>(含运费 &yen; <span>${orderInfo.freight }</span>)</a>
			                </c:if>
		                    <c:if test="${orderInfo.orderStatus == 1 || orderInfo.orderStatus == 3 || orderInfo.orderStatus == 4 || orderInfo.orderStatus == 5 || orderInfo.orderStatus == 8 }">
		                    </c:if>
	                    </p>
	                </td>
	                <td class="order_date">
	                    ${orderInfo.orderTime }
	                </td>
	                <td class="order_state">
	                	<c:if test="${orderInfo.orderStatus == 0 }">待付款</c:if>
		            	<c:if test="${orderInfo.orderStatus == 1 }">已付款</c:if>
		            	<c:if test="${orderInfo.orderStatus == 2 }">待审核</c:if>
		            	<c:if test="${orderInfo.orderStatus == 3 }">配货中</c:if>
		            	<c:if test="${orderInfo.orderStatus == 4 }">已发货</c:if>
		            	<c:if test="${orderInfo.orderStatus == 5 }">已完成</c:if>
		            	<c:if test="${orderInfo.orderStatus == 6 }">取消</c:if>
		            	<c:if test="${orderInfo.orderStatus == 7 }">无效</c:if>
		            	<c:if test="${orderInfo.orderStatus == 8 }">已备货</c:if>
	                </td>
	                <td class="operation">
		            	<c:if test="${orderInfo.orderStatus == 0 }">
			                <a href="getPayAllUrl?orderDTO.orderSn=${orderInfo.orderSn }" target="_blank">去付款</a> <br>
		            	</c:if>
		            	<c:if test="${orderInfo.pay == 5 }">
			                <a href="goXinyongHuan?orderDTO.orderSn=${orderInfo.orderSn }">去还款</a> <br>
		            	</c:if>
		            	<c:if test="${orderInfo.pay == 6 }">
			                <a href="javascript:;">已结清</a> <br>
		            	</c:if>
		            	<c:if test="${orderInfo.pay == 7 }">
			                <a href="javascript:;">已提交线下支付审核</a> <br>
		            	</c:if>
		            	<c:if test="${orderInfo.pay == 8 }">
			                <a href="javascript:;">已结清</a> <br>
		            	</c:if>
		            	<c:if test="${orderInfo.orderStatus == 4 && orderInfo.retTime == null }">
		            		<a href="javascript:void();" onclick="isOkDis('${orderInfo.orderSn }');">确认收货</a> <br>
		            	</c:if>
	                    <p><a href="<%=basePath%>order_details?orderDTO.orderSn=${orderInfo.orderSn }"  id="findOrderInfo">查看订单</a>
	                    <c:if test="${orderInfo.orderStatus <= 3 }">/<a href="javascript:;" onclick="cancelOrder(${orderInfo.orderId })">取消订单</a> <br></c:if></p>
	                    <c:if test="${orderInfo.shoptype != 4 }" >
		                    <c:if test="${orderInfo.orderStatus == 4 || orderInfo.orderStatus == 5 }">
			                    <div class="logistics" ><a href="javascript:;">查看物流</a>
			                       <div>
			                           <b></b>
			                           <p>${orderInfo.exName }：${orderInfo.exNo }</p>
			                           <ul>
			                           	<c:forEach items="${orderInfo.descInfoDTOsList }" var="ex">
			                               <li>
			                                   <p>${ex.desc }</p>
			                                   <span>${ex.time }</span>
			                               </li>
			                           	</c:forEach>
			                           </ul>
			                       </div>
			                    </div>
		                    </c:if>
	                    </c:if>
	                    <p><button onclick="javascript:location.href='<%=basePath%>orderAgainbuy?orderSn=${orderInfo.orderSn }';">再次购买</button></p>
		                <c:if test="${orderInfo.orderStatus == 5 }">
		                	
		                    <div id="${orderInfo.orderSn }pingqueRen"
		                    <c:if test="${orderInfo.evaId != null }">
		                		 style="display: block;"
		                	</c:if>	
		                     class="evaluate">
		                        <p>我要评价</p>
		                        <textarea cols="14" rows="9" id="pingValue${orderInfo.orderSn }" name="as">限120字</textarea>
		                        <button onclick="javascript:upPingJia('${orderInfo.orderSn }');" id="upPingJia">提交评价</button>
		                    </div>
		                    <div id="${orderInfo.orderSn }pingOk" class="ev_content"
		                     <c:if test="${orderInfo.evaId == null }">
		                		 style="display: none;"
		                	</c:if>	
		                    >
		                        <p class="first_p">评价内容</p>
		                        <p id="okPingJiaNei${orderInfo.orderSn }">${orderInfo.evaDesc }</p>
		                        <p id="okPingJiaTime${orderInfo.orderSn }" class="pp">${orderInfo.evaTime }</p>
		                    </div>
		                </c:if>
	                </td>
	            </tr>
	
	        </table>
	    </div>

    </c:forEach>
</div>
<script type="text/javascript">
    $("table.dingdan_body tr td.operation .logistics").hover(function(){
    	var height=parseFloat($(this).children("div").css("height"));
    	$(this).children("div").css({"display":"block","top":"-"+height-3+"px"});
    },function(){
    	$(this).children("div").css("display","none");
    }
    );
    $(".evaluate textarea").focus(function(){
    	$(this).text("");
    })
    $(".evaluate textarea").blur(function(){
    	$(this).text("限120字");
    })
</script>
	  
	   <ul class="dingdanxinxi_e" style="display: none;">
	     <li class="li1">订单信息</li>
		 <li class="li2">小计</li>
		 <li class="li2">总额</li>
		 <li class="li3">收货人</li>
		 <li class="li4">
		   <select id="odtime" name="odtime" onchange="javascript:bytime()" >
		     <option value="">下单时间</option>
		     <option value="3">最近三个月</option>
			 <option value="12">今年内</option>
		   </select>
		 </li>
		 <li class="li4">
		   <select id="odstatus" name="odstatus" onchange="javascript:bystatus()">
		     <option value="">订单状态</option>
		     <option value="all">全部状态</option>
			 <option value="0">待付款</option>
			 <option value="1">已付款</option>
			 <option value="2">待审核</option>
			 <option value="3">配货中</option>
			 <option value="4">已发货</option>
			 <option value="5">已完成</option>
			 <option value="6">取消</option>
			 <option value="7">无效</option> 
			 <option value="8">已备货</option> 
		   </select>
		 </li>
		 <li class="li5">操作</li>
	   </ul>


	   <!--订单开始-->

	   <s:iterator value="orderList" var="olist" status="text">
	    <c:forEach items="${listGodsTest }" var="oglist" varStatus="status">
	                	<c:if test="${oglist.orderId == olist.orderId }">
	                	<input type="hidden" name="orderIds" value="${oglist.orderId }" />
	                	<c:if test="${oglist.isParOrSim == '1'}">
	                	<input type="hidden" name="${oglist.orderId }simForPriceDan" value="${oglist.GSubtotal }" />
	                	</c:if>
	                	<c:if test="${oglist.isParOrSim == '0'}">
	                	<input type="hidden" name="${oglist.orderId }parForPriceDan" value="${oglist.GSubtotal }" />
	                	</c:if>
	                	</c:if>
	       </c:forEach>
<div class="ct_dingdan" style="display: none;">
	<div class="dingdanbianhao">订单编号：<a  href="order_details?orderDTO.orderSn=${olist.orderSn }" id="findOrderInfo" target="_blank">${olist.orderSn }</a></div>

    <div class="ctlei_left">
	   <c:if test="${olist.isParOrSim == '1-1' }">
        <div class="ct_pl_yp"><div class="line_l_pl">样片商品</div><span onclick="openShutManager(this,'box',false,'关闭','展开')">展开</span></div> 
        
            <div class="jine" name="${olist.orderId }simZongPrice">
          
            </div>
                    
            <div class="caigoushu" name="123">
            <c:set var="simSun" value="1"></c:set>
            	<c:forEach items="${goodsListAllDTO }" var="list">
	                <c:forEach items="${list }" var="oglist" varStatus="status">
	                	<c:if test="${oglist.orderId == olist.orderId }">
		                	<c:if test="${oglist.isParOrSim == '1' }">
					                <dl name="shouxiansim${oglist.orderId }">
						                <dd>
						                	<p class="p1"><a href="goods_detail?gid=${oglist.ctGoods.GId }" target="_blank">${oglist.ctGoods.GName }</a></p>
						                	<p class="p2">
						                		<span class="s1">采购数量:</span>
						                		<span class="s2">${oglist.GNumber }</span>
						                		<c:if test="${oglist.isRefund == null || oglist.isRefund == 0}">
						                			<c:if test="${ olist.orderStatus == 1 || olist.orderStatus == 2 ||olist.orderStatus == 3 }">
							                			<a href="javacsript:void(0);" onclick="refundGoods('${olist.orderSn }','${oglist.ctGoods.GId }','sim');return false;">退</a>
						                			</c:if>
						                		</c:if>
								                <c:if test="${oglist.isRefund != null && oglist.isRefund != 0 }">
								                	<c:if test="${oglist.isRefund == 1 }">退款已提交</c:if>
										            <c:if test="${oglist.isRefund == 2 }">退款成功</c:if>
										            <c:if test="${oglist.isRefund == 3 }">退款失败</c:if>
								               </c:if>
						                	</p>
						                </dd>
					                </dl>
		                	</c:if>
	                	</c:if>
	            	</c:forEach>
            	</c:forEach>
                
                <div class="111" name="box" style="display:none">
                <c:set var="simSun1" value="1"></c:set>
                <c:forEach items="${goodsListAllDTO }" var="list">
	                <c:forEach items="${list }" var="oglist" varStatus="status">
	                	<c:if test="${oglist.orderId == olist.orderId }">
		            		<c:if test="${oglist.isParOrSim == '1' }">
					                <dl name="houxiansim${oglist.orderId }">
						                <dd>
						                	<p class="p1"><a href="goods_detail?gid=${oglist.ctGoods.GId }" target="_blank">${oglist.ctGoods.GName }</a></p>
						                	<p class="p2">
						                		<span class="s1">采购数量:</span>
						                		<span class="s2">${oglist.GNumber }</span>
						                		<c:if test="${oglist.isRefund == null || oglist.isRefund == 0}">
						                			<c:if test="${ olist.orderStatus == 1 || olist.orderStatus == 2 ||olist.orderStatus == 3 }">
							                			<a href="javacsript:void(0);" onclick="refundGoods('${olist.orderSn }','${oglist.ctGoods.GId }','sim');return false;">退</a>
						                			</c:if>
						                		</c:if>
								                		<c:if test="${oglist.isRefund != null && oglist.isRefund != 0 }">
								                			<c:if test="${oglist.isRefund == 1 }">退款已提交</c:if>
										                	<c:if test="${oglist.isRefund == 2 }">退款成功</c:if>
										                	<c:if test="${oglist.isRefund == 3 }">退款失败</c:if>
								                		</c:if>
						                	</p>
						                </dd>
					                </dl>
		                	</c:if>
	                	</c:if>
	            	</c:forEach>
            	</c:forEach>
                </div>
                
                
                
            </div>
            <div class="ct_pl_pl"><div class="line_l_pl">批量商品</div><span onclick="openShutManager1(this,'box2',false,'关闭','展开')">展开</span></div>
            <div class="jine" name="${olist.orderId }parZongPrice">
            </div>
            <!--  && sss.index+1 == fn:length(goodsListAll) -->
		            		<c:set var="parSun" value="1"></c:set>
            <div class="caigoushu">
            	<c:forEach items="${goodsListAllDTO }" var="list">
	                <c:forEach items="${list }" var="oglist" varStatus="status">
	                	<c:if test="${oglist.orderId == olist.orderId }">
		            		<c:if test="${oglist.isParOrSim == '0' }">
					                <dl name="shouxianpar${oglist.orderId }">
						                <dd>
						                	<p class="p1"><a href="goods_detail?gid=${oglist.ctGoods.GId }" target="_blank">${oglist.ctGoods.GName }</a></p>
						                	<p class="p2">
						                		<span class="s1">采购数量:</span>
						                		<span class="s2">${oglist.GParNumber }</span>
						                		<c:if test="${oglist.isRefund == null || oglist.isRefund == 0}">
						                			<c:if test="${ olist.orderStatus == 1 || olist.orderStatus == 2 ||olist.orderStatus == 3 }">
							                			<a href="javacsript:void(0);" onclick="refundGoods('${olist.orderSn }','${oglist.ctGoods.GId }','par');return false;">退</a>
						                			</c:if>
						                		</c:if>
								               <c:if test="${oglist.isRefund != null && oglist.isRefund != 0 }">
								                			<c:if test="${oglist.isRefund == 1 }">退款已提交</c:if>
										                	<c:if test="${oglist.isRefund == 2 }">退款成功</c:if>
										                	<c:if test="${oglist.isRefund == 3 }">退款失败</c:if>
								                		</c:if>
						                	</p>
						                </dd>
					                </dl>
		                	</c:if>
	                	</c:if>
	            	</c:forEach>
            	</c:forEach>
                <div class="" name="box2" style="display:none">
                <c:set var="parSun1" value="1"></c:set>
                 <c:forEach items="${goodsListAllDTO }" var="list">
	                <c:forEach items="${list }" var="oglist" varStatus="status">
	                	<c:if test="${oglist.orderId == olist.orderId }">
		            		<c:if test="${oglist.isParOrSim == '0' }">
					                <dl name="houxianpar${oglist.orderId }">
						                <dd>
						                	<p class="p1"><a href="goods_detail?gid=${oglist.ctGoods.GId }" target="_blank">${oglist.ctGoods.GName }</a></p>
						                	<p class="p2">
						                		<span class="s1">采购数量:</span>
						                		<span class="s2">${oglist.GParNumber }</span>
						                		<c:if test="${oglist.isRefund == null || oglist.isRefund == 0}">
						                			<c:if test="${ olist.orderStatus == 1 || olist.orderStatus == 2 ||olist.orderStatus == 3 }">
							                			<a href="javacsript:void(0);" onclick="refundGoods('${olist.orderSn }','${oglist.ctGoods.GId }','par');return false;">退</a>
						                			</c:if>
						                		</c:if>
								                		<c:if test="${oglist.isRefund != null && oglist.isRefund != 0 }">
								                			<c:if test="${oglist.isRefund == 1 }">退款已提交</c:if>
										                	<c:if test="${oglist.isRefund == 2 }">退款成功</c:if>
										                	<c:if test="${oglist.isRefund == 3 }">退款失败</c:if>
								                		</c:if>
						                	</p>
						                </dd>
					                </dl>
		                	</c:if>
	                	</c:if>
	            	</c:forEach>
            	</c:forEach>
                </div>
            </div>
</c:if>
        <c:if test="${olist.isParOrSim == '0-1' }">
        <div class="ct_pl_pl"><div class="line_l_pl">批量商品</div><span onclick="openShutManager1(this,'box2',false,'关闭','展开')">展开</span></div>
            <div class="jine" name="${olist.orderId }parZongPrice">
            </div>
            
            <div class="caigoushu">
            <c:set var="parSun" value="1"></c:set>
            	<c:forEach items="${goodsListAllDTO }" var="list">
	                <c:forEach items="${list }" var="oglist" varStatus="status">
	                	<c:if test="${oglist.orderId == olist.orderId }">
		            		<c:if test="${oglist.isParOrSim == '0' }">
					                <dl name="shouxianpar${oglist.orderId }">
						                <dd>
						                	<p class="p1"><a href="goods_detail?gid=${oglist.ctGoods.GId }" target="_blank">${oglist.ctGoods.GName }</a></p>
						                	<p class="p2">
						                		<span class="s1">采购数量:</span>
						                		<span class="s2">${oglist.GParNumber }</span>
						                		<c:if test="${oglist.isRefund == null || oglist.isRefund == 0}">
						                			<c:if test="${ olist.orderStatus == 1 || olist.orderStatus == 2 ||olist.orderStatus == 3 }">
							                			<a href="javacsript:void(0);" onclick="refundGoods('${olist.orderSn }','${oglist.ctGoods.GId }','par');return false;">退</a>
						                			</c:if>
						                		</c:if>
								                <c:if test="${oglist.isRefund != null && oglist.isRefund != 0 }">
								                		<c:if test="${oglist.isRefund == 1 }">退款已提交</c:if>
										               <c:if test="${oglist.isRefund == 2 }">退款成功</c:if>
										               <c:if test="${oglist.isRefund == 3 }">退款失败</c:if>
								                </c:if>
						                	</p>
						                </dd>
					                </dl>
		                	</c:if>
	                	</c:if>
	            	</c:forEach>
            	</c:forEach>
                <div class="" name="box2" style="display:none">
                 <c:set var="parSun1" value="1"></c:set>
                 <c:forEach items="${goodsListAllDTO }" var="list">
	                <c:forEach items="${list }" var="oglist" varStatus="status">
	                	<c:if test="${oglist.orderId == olist.orderId }">
		            		<c:if test="${oglist.isParOrSim == '0' }">
					                <dl name="houxianpar${oglist.orderId }">
						                <dd>
						                	<p class="p1"><a href="goods_detail?gid=${oglist.ctGoods.GId }" target="_blank">${oglist.ctGoods.GName }</a></p>
						                	<p class="p2">
						                		<span class="s1">采购数量:</span>
						                		<span class="s2">${oglist.GParNumber }</span>
						                		<c:if test="${oglist.isRefund == null || oglist.isRefund == 0}">
						                			<c:if test="${ olist.orderStatus == 1 || olist.orderStatus == 2 ||olist.orderStatus == 3 }">
							                			<a href="javacsript:void(0);" onclick="refundGoods('${olist.orderSn }','${oglist.ctGoods.GId }','par');return false;">退</a>
						                			</c:if>
						                		</c:if>
								                		<c:if test="${oglist.isRefund != null && oglist.isRefund != 0 }">
								                			<c:if test="${oglist.isRefund == 1 }">退款已提交</c:if>
										                	<c:if test="${oglist.isRefund == 2 }">退款成功</c:if>
										                	<c:if test="${oglist.isRefund == 3 }">退款失败</c:if>
								                		</c:if>
						                	</p>
						                </dd>
					                </dl>
		                	</c:if>
	                	</c:if>
	            	</c:forEach>
            	</c:forEach>
                </div>
            </div>
</c:if>
<c:if test="${olist.isParOrSim == '1-0' }">
<div class="ct_pl_yp"><div class="line_l_pl">样片商品</div><span onclick="openShutManager(this,'box',false,'关闭','展开')">展开</span></div> 
        
            <div class="jine" name="${olist.orderId }simZongPrice">
            </div>
                    
            <div class="caigoushu">
            <c:set var="simSun" value="1"></c:set>
            	<c:forEach items="${goodsListAllDTO }" var="list">
	                <c:forEach items="${list }" var="oglist" varStatus="status">
	                	<c:if test="${oglist.orderId == olist.orderId }">
		                	<c:if test="${oglist.isParOrSim == '1' }">
					                <dl name="shouxiansim${oglist.orderId }">
						                <dd>
						                	<p class="p1"><a href="goods_detail?gid=${oglist.ctGoods.GId }" target="_blank">${oglist.ctGoods.GName }</a></p>
						                	<p class="p2">
						                		<span class="s1">采购数量:</span>
						                		<span class="s2">${oglist.GNumber }</span>
						                		<c:if test="${oglist.isRefund == null || oglist.isRefund == 0}">
						                			<c:if test="${ olist.orderStatus == 1 || olist.orderStatus == 2 ||olist.orderStatus == 3 }">
							                			<a href="javacsript:void(0);" onclick="refundGoods('${olist.orderSn }','${oglist.ctGoods.GId }','sim');return false;">退</a>
						                			</c:if>
						                		</c:if>
								                		<c:if test="${oglist.isRefund != null && oglist.isRefund != 0 }">
								                			<c:if test="${oglist.isRefund == 1 }">退款已提交</c:if>
										                	<c:if test="${oglist.isRefund == 2 }">退款成功</c:if>
										                	<c:if test="${oglist.isRefund == 3 }">退款失败</c:if>
								                		</c:if>
						                	</p>
						                </dd>
					                </dl>
		                	</c:if>
	                	</c:if>
	            	</c:forEach>
            	</c:forEach>
                
                <div class="" name="box" style="display:none">
                 <c:set var="simSun1" value="1"></c:set>
                <c:forEach items="${goodsListAllDTO }" var="list">
	                <c:forEach items="${list }" var="oglist" varStatus="status">
	                	<c:if test="${oglist.orderId == olist.orderId }">
		            		<c:if test="${oglist.isParOrSim == '1' }">
					                <dl name="houxiansim${oglist.orderId }">
						                <dd>
						                	<p class="p1"><a href="goods_detail?gid=${oglist.ctGoods.GId }" target="_blank">${oglist.ctGoods.GName }</a></p>
						                	<p class="p2">
						                		<span class="s1">采购数量:</span>
						                		<span class="s2">${oglist.GNumber }</span>
						                		<c:if test="${oglist.isRefund == null || oglist.isRefund == 0}">
						                			<c:if test="${ olist.orderStatus == 1 || olist.orderStatus == 2 ||olist.orderStatus == 3 }">
							                			<a href="javacsript:void(0);" onclick="refundGoods('${olist.orderSn }','${oglist.ctGoods.GId }','sim');return false;">退</a>
						                			</c:if>
						                		</c:if>
								                		<c:if test="${oglist.isRefund != null && oglist.isRefund != 0 }">
								                			<c:if test="${oglist.isRefund == 1 }">退款已提交</c:if>
								                			<c:if test="${oglist.isRefund == 2 }">退款成功</c:if>
								                			<c:if test="${oglist.isRefund == 3 }">退款失败</c:if>
								                		</c:if>
						                	</p>
						                </dd>
					                </dl>
		                	</c:if>
	                	</c:if>
	            	</c:forEach>
            	</c:forEach>
                </div>
            </div>
</c:if>
    </div>
  
    
    <div class="ctlei_right">
        <div class="jine">
       	￥${olist.total }
        </div>

        <div class="shouhuoren">
        ${olist.consignee }
        </div>

      <div class="xiadanshijian">
		    <p class="p3">${olist.orderTime }</p>
			<p class="p4"> </p>
		 </div>
        
        <div class="dingdanzhuangtai">
		 <s:if test="#olist.orderStatus==0">待付款</s:if>
		 <s:if test="#olist.orderStatus==1">已付款</s:if>
		 <s:if test="#olist.orderStatus==2">待审核</s:if>
		 <s:if test="#olist.orderStatus==3">配货中</s:if>
		 <s:if test="#olist.orderStatus==4">已发货</s:if>
		 <s:if test="#olist.orderStatus==5">已完成</s:if>
		 <s:if test="#olist.orderStatus==6">取消</s:if>
		 <s:if test="#olist.orderStatus==7">无效</s:if>
		 <s:if test="#olist.orderStatus==8">已备货</s:if>
		 </div>

        <div class="caozuo">
		  <ul class="caozuo_ul">
			  <c:if test="${olist.orderStatus == 0 }">
			      <li><a href="getPayAllUrl?orderDTO.orderSn=${olist.orderSn }" target="_blank">去付款</a></li>
			  </c:if>
			  <c:if test="${olist.orderStatus == 4 && olist.retTime == null }">
			      <li><a href="javascript:void();" onclick="isOkDis('${olist.orderSn }');">确认收货</a></li>
			  </c:if>
			<li><a href="<%=basePath%>order_details?orderDTO.orderSn=${olist.orderSn }"  id="findOrderInfo">查看订单</a>
				<s:if test="#olist.orderStatus<=3">/<a href="javascript:;" onclick="cancelOrder(${olist.orderId })" >取消订单</a>
				</s:if>
			</li>
			<c:if test="${olist.orderStatus == 3 || olist.orderStatus == 4 }">
				<c:if test="${olist.retTime != null}">
					<li>当前退货状态：${olist.retStatus }</li>
				</c:if>
				<c:if test="${olist.retStatus == null}">
					<li><a href="<%=basePath%>return_Goods?orderDTO.orderSn=${olist.orderSn }">退换货申请</a></li>
				</c:if>
			</c:if>
			<li class="li6"><a href="javascript:;" onclick="orderAgainBuy(${olist.orderId })">再次购买</a></li>
		  </ul>
		  <c:if test="${olist.orderStatus == 5 }">
			  <c:if test="${olist.evaId == null }">
				  <div id="frmTitle_p${olist.orderSn }" class="pingjia_d" id="addPingJia${olist.orderSn }">
		              <h3 id="tetal${olist.orderSn }">我要评价</h3>
		              <dl>
		              <dt id="okPingJiaNei${olist.orderSn }"><textarea id="pingValue${olist.orderSn }" name="as" >限120字。</textarea></dt>
		              <dd id="okPingJiaTime${olist.orderSn }"><a id="pingBtn${olist.orderSn }" href="javascript:void();" onclick="javascript:upPingJia('${olist.orderSn }');" id="upPingJia" >提交评价</a></dd>
		              </dl>
		            </div>
		      </c:if>
		       <c:if test="${olist.evaId != null }">
						 <div id="frmTitle_p${olist.orderSn }" class="pingjia_d">
			              <h3>评价内容</h3>
			              <dl>
			              <dt>${olist.evaDesc }</dt>
			              <dd>${olist.evaTime }</dd>
			              </dl>
			              </div>
				</c:if>
		  </c:if>
			 </div>
    
    </div>    
      
    

</div>
<!--订单结束-->
	   	

		  <script type="text/javascript">

		  $("#ajaxPing").hide();
		  </script>
	   <input type="hidden" id="orderId" value="${olist.orderSn }" />
	   <input type="hidden" name="orderIdss" value="${olist.orderId }" />
	   </s:iterator>
<input type="hidden" id="timeBack" value="${time }" />
<input type="hidden" id="statusBack" value="${status }" />
	   
	  <script type="text/javascript">
	   	getThreeOrder();

	   	getParPrice();
	   	

	   	kongZhiShouHou();
	   	
	   	defaultDate();
	   	defaultSelect();
	   	function defaultSelect(){
	   		var odstatus = document.getElementById("odstatus").options;
	   		var odtime = document.getElementById("odtime").options;
	   		var timeBack = $("#timeBack").val();
	   		var statusBack = $("#statusBack").val();
	   		for (i=0; i<odstatus.length; i++){
	   			if (odstatus[i].value == statusBack) // 根据option标签的ID来进行判断 测试的代码这里是两个等号
	   			{
	   				odstatus[i].selected = true;
	   			}
	   		}
	   		for (i=0; i<odtime.length; i++){
	   			if (odtime[i].value == timeBack) // 根据option标签的ID来进行判断 测试的代码这里是两个等号
	   			{
	   				odtime[i].selected = true;
	   			}
	   		}
	   	}
	   	
	   	function refundGoods(orderSn, gid, goodsType){
	   		if(confirm("确定要退款这个物料吗？")){
		   		$.post(
		   			"order_refundOrder",
		   			{"orderSn":orderSn,"GId":gid,"simOrPar":goodsType},
		   			function(data, varstart){
		   				if(data=="success"){
		   					alert("申请退款成功");
			   				location.href="order_list";
		   				}
		   			}
		   		);
	   		}
	   		//location.href="order_list";
	   		//$("#orderSnForRefund").val(orderSn);
	   		//$("#gidForRefund").val(gid);
	   		//$("#simOrParForRefund").val(goodsType);
	   		//$("#myFromOrderList").attr("action","order_refundOrder");
	   		//document.getElementById("myFromOrderList").submit();
	   		//window.navigate("webindex"); 
	   		//location.href="webindex";
	   	}
	   	
	  </script>
	  
        ${pages.pageGoodsStr }
       
	
	</div>
	<form action="order_check" method="post" id="myFromOrderList">
		<input type="hidden" id="orderIdByAgainBuy" name="pack" />
		
		<input type="hidden" name="orderSn" id="orderSnForRefund" />
		<input type="hidden" name="GId" id="gidForRefund" />
		<input type="hidden" name="simOrPar" id="simOrParForRefund" />
		
	</form>


<div class="clear"></div>
</div>
<jsp:include page="../common/foot.jsp"></jsp:include>
<script>
$(document).ready(function(){
    var dingdan=$(".ct_dingdann");
    for(var i=0;i<dingdan.length;i++){
        var me=$(dingdan[i]).find(".zhankai");
        $.fn.toggle(me);
    }
    $(".myorders").on("click",".zhankai",function(){
        var me=$(this);
        $.fn.toggle(me);
    });
})
    $.fn.toggle=function(me){
        var html=$(me).html();
        var length=$(me).parent().siblings().find(".products>div").length;
        var a=$(me).parent().siblings(".dingdan_body").find("td.products>div");
        var b=$(me).parent().siblings(".dingdan_body").find("td.products2>div");
        if(html=="收起"){
            html="展开";
	        if(a.length<3){
	            $(a[a.length-1]).css("border-bottom","0");
	            $(b[b.length-1]).css("border-bottom","0");
	        }
            for(var i=0;i< a.length;i++){
                if(i>=3){
                    $(a[i]).addClass("active");
                    $(b[i]).addClass("active");
                }
	        $(a[2]).css("border-bottom","0");
	        $(b[2]).css("border-bottom","0");
            }
        }else if(html=="展开"){
            html="收起";
            for(var i=0;i< a.length;i++){
                if(i>=3){
                    $(a[i]).removeClass("active");
                    $(b[i]).removeClass("active");
                }
            }
	        $(a[2]).css("border-bottom","1px solid #ddd");
	        $(a[a.length-1]).css("border-bottom","0");
	        $(b[2]).css("border-bottom","1px solid #ddd");
	        $(b[b.length-1]).css("border-bottom","0");
        }
        $(me).html(html);
    }


</script>
  </body>
</html>

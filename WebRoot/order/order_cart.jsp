<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="frm" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
	<script type="text/javascript" src="<%=basePath %>js/check.js" charset="utf-8"></script> 

	<script type="text/javascript" src="<%=request.getContextPath()%>/order/js/order_price.js" ></script>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" type="text/css" href="css/jquery.toolbar.css">
<script type="text/javascript">
function a(){
	var res = "${cupon}";
	var ress;
	var gid = "${cart.cartId}"
	if(res.length > 0){
		ress = res.split(",");
		for(var i=0; i<ress.length;i++){
			var resss = ress[i].split("|");
			var pas = $("#"+resss[1]+"cunpon").attr("id");
			if(resss[0] > 0){
				$("#"+resss[1]+"cunpon").show();
			} else {
				$("#"+resss[1]+"cunpon").hide();
			}
		}
	}
}
//sumLoad();
</script>
  </head>
  
  <body>
  <!-- <body onload="javascript:sumLoad();"> -->
  <div id="bgallfk" class="bgallfk" style="display:none;"></div>
  <!-- 登录弹出开始 -->
		<div id="myModal" class="reveal-modal" style="top:220px; display: none; z-index:666">
        
		<h1 class="tle">亲，您尚未登录~<em></em></h1>
		<div class="reveal-modal-cont">
			
			<div class="reveal-tab-meun hd">
				<ul>
					<li class="on">登录</li>
					<li><a href="<%=basePath%>login_goRegister">注册</a></li>
				</ul>
			</div>
				<h1>${cupon }</h1>
			<br class="clear">
			<div class="reveal-tab-cont bd">
				<div class="reveal-tab-item">
					 <s:form action="login_login" id="loginByLayerForm" method="post">
					 <span style="color: red; padding-left:18px; display:block; width:320px; height:20px; line-height:20px; text-align:left; font-size:12px;" id="msg"></span>
					<input type="hidden" value="" id="nexturl"/>
					<ul>
						<li>
							<div class="ipone"><span class="hao_name">用 户 名：</span></div>
							<div class="text"><input id="username" name="ctUser.UUserid" type="text" style="color: rgb(204, 204, 204);" value="用户名/手机/邮箱" onblur="if(this.value==''||this.value=='用户名/手机/邮箱'){this.value='用户名/手机/邮箱';this.style.color='#cccccc'}" onfocus="if(this.value=='用户名/手机/邮箱') {this.value='';};this.style.color='#000000';"/></div>
							<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="usernametip"></span>
						</li>
						<li>
							<div class="ipone"><span class="hao_name">密　　码：</span></div>
							<div class="text"><input type="password" id="passwd" name="ctUser.UPassword"></div>
							<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="passwdtip"></span>
						</li>
						<li>
							<div class="texter">
								<a class="submit-btn" href="javascript:loginByLayer();">登　　录</a>
							</div>
						</li>
						<li class="li-top">
							<div class="text text-radio">
								<span class="fl_f"><input type="checkbox" value="1" name="remember" id="remember">自动登录</span>
								<span class="fr_f"><a href="<%=basePath%>login_goForgetPwd">忘记密码？</a></span>
							</div>
						</li>
						<li class="li-top">
<dl class="disanfang">
<dt>用第三方帐号登录长亭电子官网</dt>
<dd class="taobaod"><a href="/" target="_blank">淘宝</a></dd>
<dd class="weibod"><a href="/" target="_blank">微博</a></dd>
<dd class="tengxund"><a href="/" target="_blank">腾讯</a></dd>
</dl>

						</li>
					</ul>
					<input name="act" type="hidden" value="signin">
					</s:form>
				</div>
			</div>
			
		</div>
		<a class="close-reveal-modal" href="javascript:close()"></a>
	</div><div class="reveal-modal-bg" style="cursor: pointer;display: none"></div>
<!-- 登录弹出结束 -->
  <jsp:include page="../common/head.jsp"></jsp:include>
  
  	<div class="contant_f">

	<ul class="gou_zhuang_f">
	<li class="zhuang_a1">我的购物车</li>
	<li class="zhuang_a2">填写核对订单</li>
	<li class="zhuang_a3">订单完成</li>
	<div class="clear"></div>
	</ul>
  
<div id="shop">
    <div class="gwc_d1">
        <p>购买商品</p>
    </div>
    <ul class="gwc_ul">
        <li class="gwc_li1">
            <input type="checkbox" onclick="selectAllNew(1)" size="4" checked="checked" class="all"> 全选
            <span>商品信息</span>
        </li>
        <li class="gwc_li2">
            <a>价格</a>
            <span>单位</span>
            <span>数量</span>
            <span>小计</span>
        </li>
        <li class="gwc_li3">
            <span>购买数量</span>
            <span>金额</span>
            <span>操作</span>
        </li>
    </ul>
    <c:forEach items="${cartDTOList }" var="cartListDto">
	    <ul name="pro" class="product_ul">
	        <li class="product_li1">
	            <input type="checkbox" checked="checked" size="4" value="${cartListDto.cartGId }" onclick="selectOneNew(2)">
	           <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/${cartListDto.cartGoodsImgUrl }">
	            <div>
	                <span><a href="<%=request.getContextPath()%>/goods_detail?gid=${cartListDto.cartGId}">${cartListDto.cartGName }</a></span>
	                <img src="
		                <c:if test="${cartListDto.isSample == 1 }">
			                https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/zcypq.gif
		                </c:if>
	                ">
	            </div>
	        </li>
	        <li class="product_li2">
	            <div class="sample_price">
	                <div class="price">
	                	<c:forEach items="${cartListDto.prices }" varStatus="priceIndex" var="renPrice">
		                    <p name="${cartListDto.cartGId }${priceIndex.index }isPriceHover"
		                    <c:if test="${priceIndex.index ==  cartListDto.cartGoodsSelNum}">
			                     class="active"
		                    </c:if>
		                     ><span>${renPrice.simSNum }-${renPrice.simENum }：&yen;${renPrice.simRPrice }</span><b></b></p>
	                	</c:forEach>
	                </div>
	                <div>pcs</div>
	                <div class="sample_number">
	                    <span onclick="cartSubCount(${cartListDto.cartGId },3)">-</span>
	                    <input maxlength="4" type="text" onfocus="javascript:if(this.value == 0){this.value=''};" oldvalue="${cartListDto.cartSimNum }" onchange="changesim(${cartListDto.cartGId })" onblur="listsimAjax(${cartListDto.cartGId },this.value)" id="${cartListDto.cartGId }" value="${cartListDto.cartSimNum }">
	                    <span onclick="cartSubCount(${cartListDto.cartGId },4)">+</span>
	                </div>
	                <div id="${cartListDto.cartGId }simPriceQu" class="sample_subtotle">&yen;<frm:formatNumber pattern="0.00">${cartListDto.cartSimPrice }</frm:formatNumber></div>
	            </div>
	
	            <div class="batch_price">
	                <div class="batch_d1">
		                <c:forEach items="${cartListDto.prices }" varStatus="priceIndex" var="renPrice">
		                	<c:if test="${renPrice.parSNum != null }">
			                    <p name="${cartListDto.cartGId }${priceIndex.index }isPriceHoverPar"
			                    	<c:if test="${cartListDto.cartParNum != 0 }">
			                    		 class="active"
			                    	</c:if>
			                   ><span>≥<frm:formatNumber pattern="0">${renPrice.parSNum / 1000 }</frm:formatNumber>：&yen;<frm:formatNumber pattern="0.00">${renPrice.parRPrice * 1000 }</frm:formatNumber></span><b></b></p>
		                	</c:if>
		                	<input type="hidden" name="${cartListDto.cartGId }gidSim" value="${renPrice.simIncrease }"/>
						    <input type="hidden" name="${cartListDto.cartGId }gidENum" value="${renPrice.simENum }"/>
						    <input type="hidden" name="${cartListDto.cartGId }gidSNum" value="${renPrice.simSNum }"/>
						    <input type="hidden" name="${cartListDto.cartGId }gidSPrice" value="${renPrice.simRPrice }"/>
						    
						    <input type="hidden" name="${cartListDto.cartGId }gidPar" value="${renPrice.parIncrease}"/>
						    <input type="hidden" name="${cartListDto.cartGId }gidParENum" value="${renPrice.parENum }"/>
						    <input type="hidden" name="${cartListDto.cartGId }gidParSNum" value="${renPrice.parSNum }"/>
						    <input type="hidden" name="${cartListDto.cartGId }gidParPrice" value="${renPrice.parRPrice * 1000}"/>
		                </c:forEach>
	                </div>
	                <div class="batch_d2">kpcs</div>
	                <div class="batch_d3">
	                    <span onclick="cartSubCount(${cartListDto.cartGId },5)">-</span>
	                    <input maxlength="4" onfocus="javascript:if(this.value == 0){this.value=''};" type="text" oldvalue="${cartListDto.cartParNum }" onchange="changepar(${cartListDto.cartGId })" onblur="listparAjax(${cartListDto.cartGId },this.value)" id="${cartListDto.cartGId }num" value="${cartListDto.cartParNum }">
	                    <span onclick="cartSubCount(${cartListDto.cartGId },6)">+</span>
	                </div>
	                <div id="${cartListDto.cartGId }parPriceQu" class="batch_d4">&yen;<frm:formatNumber pattern="0.00">${cartListDto.cartParPrice }</frm:formatNumber></div>
	            </div>
	        </li>
	        <li class="product_li3">
	            <div class="li3_d1">
	                <span onclick="cartSubCount(${cartListDto.cartGId },1)">-</span>
	                <input type="text"  maxlength="8" onfocus="javascript:if(this.value == 0){this.value=''};" onchange='change(${cartListDto.cartGId })' oldvalue='${cartListDto.cartCountNum }' onblur="listAllCountAjax(${cartListDto.cartGId },this.value)" name="${cartListDto.cartGId }countSum" value="${cartListDto.cartCountNum }">
	                <span onclick="cartSubCount(${cartListDto.cartGId },2)">+</span>
	            </div>
	            <div id="${cartListDto.cartGId }xiaojiePriceTotal" class="li3_d2">
	                &yen;<frm:formatNumber pattern="0.00">${cartListDto.cartCountPrice }</frm:formatNumber>
	            </div>
	            <input type="hidden" id="${cartListDto.cartGId }_goodsPrice" value="<frm:formatNumber pattern="0.00">${cartListDto.cartCountPrice }</frm:formatNumber>" name="goodsCountPrice" />
	            <div class="li3_d3">
	                <p>
	                <a href="javascript:delGoodsAjax(${cartListDto.cartGId })" name="${cartListDto.cartGId }bianJi">删除</a><br>
	                <a href="javascript:addBom(${cartListDto.cartGId });" id="btn_bm">加入我的BOM</a>
	                </p>
	            </div>
	        </li>
	    </ul>
	    <input type="hidden" id="${cartListDto.cartGId }pack1num" value="${cartListDto.goods.pack1Num }" />
	    <input type="hidden" id="${cartListDto.cartGId }_kucun" value="${cartListDto.goods.GNum }" />
    </c:forEach>
</div>
<div class="shop_foot">
        <p><input type="checkbox" onclick="selectAllNew(2)" checked="checked" size="4" class="all_2">全选 </p>
        <p>
            <a href="javascript:delgoods()">删除选中的商品</a>
            <a href="javascript:addBomGoods()">加入我的BOM</a>
            <!-- <a href="javascript:collectGoods();">移至收藏夹</a> -->
        </p>
        <div>
			<!-- <a href="exportToExcel">导出到Excel</a> -->
			
            <a>已选商品 <b id="cartGoodsNum" class="product_number">${allCartNum }</b> 件</a>
            <span>合计(不含运费)：<b id="totalNew" class="total">&yen;<frm:formatNumber pattern="0.00">${allPrice }</frm:formatNumber></b></span>
        </div>
        <button id="checkout">去结算</button>
</div>
<form action="order_check" method="post" id="myfromCart">
	<input type="hidden" id="gidssFrm" name="gids"/>
</form>
  <script type="text/javascript">
  $(function(){
		$("#checkout").click(function(){
			var box = $("input[size=4]");
			var gids = "";
			var gnum = "";
			for (var i=1;i<box.length-1;i++) {
				if(box[i].checked){
					gids = gids + "," + box[i].value;
				}
			}
			if(gids==""){
				alert("请至少选择一个商品！");
			}else{
				$("#gidssFrm").val(gids);
				$("#myfromCart").submit();
			}
		});
	});
  function findSimNull(){
	  var p = $("div[class=price]");
	  var str = "";
	  for(var i = 0; i < p.length; i++){
		  var tempP = $(p[i]);
		  str = tempP.children("p").find("span").html();
		  if(str == "-：¥"){
			  tempP.parent(".sample_price").hide();
		  }
	  }
  }
  findSimNull();
  $(document).ready(function(){
	  var ul=$("#shop>ul");
	  for(var i=0;i<ul.length;i++){
		  if($(ul[i]).find(".sample_price").css("display")=="none"){
			  $(ul[i]).find(".batch_price").addClass("active");
		  }else{
			  $(ul[i]).find(".batch_price").removeClass("active");
		  }
	  }
  });
  </script>
    <!--循环结束 -->
<div class="BMchuang mydiv_jrbom" style="display:none; z-index:666"  id="popDivjrbom">
<div class="lcha"><span><a href="javascript:closeDiv()"><img src="<%=imgurl %>/bg/chadiao.gif" /></a></span></div>
<form id="bom-form" name="bom-form" action="" method="post">
<div class="tizp">
<input type="hidden" value="" id="nexturl-bom"/>
<input type="hidden" name="GId" value="${cartListDto.cartGId }"/>
<!-- <input type="hidden" id="cartId" name="cartId" value="${cart.cartId }"/> -->
<input type="hidden" id="pack" name="pack" />
<input type="hidden" id="goodsNum" name="goodsNum"/>
<div class="xinjianb_f"><span class="shoujia">新建我的BOM：</span><input name="bomTitle" id="bm_name" value=""></div>
<div class="xinjianbline_f">
<span class="shoujia">加入已有的BOM：</span>
    <ul class="gund_f">
    <s:iterator value="boms" var="list">
    	<li><input id="checkbox" name="mid" type="checkbox" value="${list.bomId }" class="xuanze_f" />${list.bomTitle }</li>
    </s:iterator>
    </ul>
</div>
<div class="tijiao"><span><a href="javascript:confirmAdd();" ><img src="<%=imgurl %>/bg/tan_que.gif" /></a></span></div>
</div>
</form>
</div>

<div id="bgshizhong" class="bgshizhong" style="display:none;"></div>


<!-- 收藏夹 -->


<div class="GMchuang" style="display:none;">
<div class="lcha"><span><a href="javascript:closeGM()"><img src="<%=imgurl %>/bg/chadiao.gif" /></a></span></div>
<form id="bom-form" name="bom-form" action="" method="post">
<div class="tizp">
<input type="hidden" value="" id="nexturl-bom"/>
<input type="hidden" name="GId" value="${cartListDto.cartGId }"/>
<input type="hidden" id="cartId" name="cartId" value="${cart.cartId }"/>
<input type="hidden" id="pack" name="pack" />
<input type="hidden" id="goodsNum" name="goodsNum"/>
<div class="xinjianb_f"><span class="shoujia">新建我的BOM：</span><input name="bomTitle" id="bm_bname" value=""></div>
<div class="xinjianbline_f">
<span class="shoujia">加入已有的BOM：</span>
    <ul class="gund_f">
    <s:iterator value="boms" var="list">
    	<li><input id="checkbox" name="mid" type="checkbox" value="${list.bomId }" class="xuanze_f" />${list.bomTitle }</li>
    </s:iterator>
    </ul>
</div>
<div class="tijiao"><span><a href="javascript:addGoodsToMany();" ><img src="<%=imgurl %>/bg/tan_que.gif" /></a></span></div>
</div>
</form>
</div>

<div class="CMchuang" style="display:none; top:55px; left:25%; z-index:666 " >
<div class="lcha"><span><a href="javascript:closeCM()"><img src="<%=imgurl %>/bg/chadiao.gif"/></a></span></div>
<form id="coll-form" name="coll-form" action="" method="post">
<div class="tizp">
<input type="hidden" value="" id="nexturl-coll"/>
<input type="hidden" name="GId" value="${cartListDto.cartGId }"/>
<input type="hidden" id="cartId" name="cartId" value="${cart.cartId }"/>
<input type="hidden" id="cgid" name="GId" value="${cartListDto.cartGId }"/>
<div class="xinjianb_f"><span class="shoujia">新建我的收藏夹：</span><input name="colltitle" id="coll_name" value=""></div>
<div class="xinjianbline_f">
<span class="shoujia">加入已有的收藏夹：</span>
    <ul class="gund_f">
    <s:iterator value="collects" var="list">
    	<li><input id="checkbox" name="mid" type="checkbox" value="${list.collid }" class="xuanze_f" />${list.colltitle }</li>
    </s:iterator>
    </ul>
</div>
<div class="tijiao"><span><a href="javascript:confirmAddColl();" ><img src="<%=imgurl %>/bg/tan_que.gif" /></a></span></div>
</div>
</form>
</div>
<div class="clear"></div>
</div>
<jsp:include page="../common/foot.jsp"></jsp:include>
<script type="text/javascript" src="<%=request.getContextPath()%>/goods/js/newgoods_price.js" ></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/cart.js"></script>
<div id="toolbar-options" class="hidden">
    <a>请输入数量</a>
    <!--<a href="#"><i class="fa fa-car"></i></a>
    <a href="#"><i class="fa fa-bicycle"></i></a>-->
</div>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.toolbar.js"></script>
<script>
$(".product_li3 .li3_d1 input[type='text']").toolbar({
    content: '#toolbar-options',
    position: 'top',
    style: 'info',
});

$(".product_li3 .li3_d1 input[type='text']").on("toolbarShown",function() {
    var timer = setTimeout(function () {
        $(".tool-container.tool-top").css("display", "none").css("opacity", "0")
    }, 3000)
})
</script>
  </body>
</html>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
</head>

<body>

<jsp:include page="../common/head.jsp"></jsp:include>
<div class="contant_e">
    <div class="dizhilian_e"><a href="<%=basePath %>" target="_blank">首页</a>><a href="<%=request.getContextPath() %>/user_userinfo" target="_blank">个人中心</a>><span>填写支付信息</span></div>
	 <jsp:include page="../common/left.jsp"></jsp:include> 
<form action="order_payUpdate" name="Form1" method="post" id="myForm" enctype="multipart/form-data">
<s:token></s:token>
    <input type="hidden" name="ctPay.url"  id="url" />
	<div class="right_k">
	   
	   <div class="zpshzher">
		  <div class="biao">填写支付信息<span>*请务必填写完整准确的打款信息，以便我司快速查账确认。</span></div>
<dl class="txzfxx">
<input type="hidden" name="ctPay.orderId" value="${orderInfo.orderId }" />
<input type="hidden" name="ctPay.orderSn" value="${orderInfo.orderSn }" />
<dd><div class="tx_tt">订单号：</div><div class="tx_nei">${orderInfo.orderSn }</div></dd>
<dd><div class="tx_tt">订单金额：</div><div class="tx_nei">${orderInfo.total }元</div></dd>
<dd><div class="tx_tt">转入我司银行：</div>
<span class="tx_t_biao">转入开户行</span>
<span class="tx_m_biao">转入开户名</span>
<span class="tx_h_biao">转入帐号</span>
</dd>
<c:forEach items="${banks }" var="list">
	<dd class="tx_tou">
	    <ul>
	    <li>
	    <span class="tx_t_biao">
	    <i class="tx_danx">
	    <input name="xh1" type="radio" value="${list.bankId}" checked />
	    </i>
	    ${list.deposit }</span>
	    <span class="tx_m_biao">${list.accountName }</span>
	    <span class="tx_h_biao">${list.accountNumber }</span>
	    </li>
	    </ul>
	</dd>
</c:forEach>
<input type="hidden" id="bankIds" name="ctPay.bankId" value="" />
<script type="text/javascript">
	function xianxiapayClear(){
		$("#payName").val("");
		$("#payTel").val("");
		$("#costName").val("");
		$("#receiveDate").val("");
		$("#year").val("2015");
		$("#month").val("1");
		$("#day").val("1");
		$("#up").val("");
		$("#fillRemark").val("限100字内！");
	}
	function updatePayType(){
		var $payName = $("#payName").val();
		var $payTel = $("#payTel").val();
		var $costName = $("#costName").val();
		var $receiveDate = $("#receiveDate").val();
		//var $year = $("#year").val();
		//var $month = $("#month").val();
		//var $day = $("#day").val();
		var date = $("#dataNow").val();
		var $payDate = $("#payDate").val(date);
		var $up = $("#up").val();
		var valls = $("input[name=xh1]:checked").attr("value");
		$("input[id=bankIds]").attr("value",valls);
		var url = window.location.href;
		$("#url").val(url);
		$("#updateFileUrl").val($("#up"));
		
		if($payName == "" || $payName.length == 0){
			alert("请输入您的姓名");
			return;
		}
		if($payTel == "" || $payTel.length == 0){
			alert("请输入您的联系方式");
			return;
		}

		if($costName == "" || $costName.length == 0){
			alert("请输入您打款账户开户名");
			return;
		}
		if($receiveDate == "" || $receiveDate.length == 0){
			alert("请输入转入金额");
			return;
		}
		if($up == "" || $up.length == 0){
			alert("您还没有输入上传凭证呢");
			return;
		}
		//alert(aa);
		//var file_img=document.getElementById("up"); 
		 //var transImg;
		 //getPath(file_img,this,transImg);
		$("#myForm").submit();
	}
	function getNowFormatDate() { 
	    var date = new Date(); 
	    var seperator1 = "-"; 
	    var seperator2 = ":"; 
	    var month = date.getMonth() + 1; 
	    var strDate = date.getDate(); 
	    if (month >= 1 && month <= 9) { 
	        month = "0" + month; 
	    } 
	    if (strDate >= 0 && strDate <= 9) { 
	        strDate = "0" + strDate; 
	    } 
	    var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate ; 
		$("#dataNow").val(currentdate);
	} 
	
</script>
<dd><div class="tx_tt">您的姓名：</div><div class="tx_nei"><input name="ctPay.CName" id="payName" type="text"></div></dd>
<dd><div class="tx_tt">您的电话：</div><div class="tx_nei"><input name="ctPay.CTel" id="payTel" type="text"></div></dd>
<dd><div class="tx_tt">您的打款账户开户名：</div><div class="tx_nei"><input  onafterpaste="encodeKey(this);" onkeyup="encodeKey(this);" name="ctPay.CAName" id="costName" type="text"></div><div class="tx_zhushi">注意：此处不是银行名称，而是开户名，如：李佳嘉，xxx公司。</div></dd>
<dd><div class="tx_tt">转入金额：</div><div class="tx_nei"><input name="ctPay.payAmount" id="receiveDate" class="lcsc_xxfk3" type="text"></div></dd>
<dd>
<input type="hidden" name="ctPay.payDate" id="payDate" value="" />
<div class="tx_tt">转入日期：</div>
<div class="tx_nei">
<input type="text" id="dataNow" onclick="WdatePicker()" readonly="readonly" class="Wdate" src="datepicker/skin/datePicker.gif"/>
</div>
</dd>
<dd><div class="tx_tt">上传图片：</div><div class="tx_nei"><input name="ctPay.fileName" onchange="test(this)" id="up" type="file" class="tx_wenjian" /></div><div class="tx_zhushi">(仅支持图片格式，文件大小不能大于2M。)</div></dd>
<dd><div class="tx_tt">备注：</div><div class="tx_wei"><textarea name="customerPayInBank.fillRemark" id="fillRemark" >限100字内！</textarea></div></dd>
<dd><div class="tx_que"><a href="javascript:void();" onclick="javascript:updatePayType();">确认</a><a href="javascript:;" onclick="xianxiapayClear();" class="tx_chzh">重置</a></div>
</dd>
</dl>

	   </div>
  </div>
  <input type="hidden" id="updateFileUrl" name="ctPay.fileName"  />
  <input type="hidden" id="updateFileName" name="ctPay.upFileName"  />
  </form>
<script type="text/javascript">
$(function(){
	$("#up").change(function() {
	    $("#updateFileUrl").val(this.files && this.files.length ?
	          this.files[0].name : this.value.replace(/^C:\\fakepath\\/i, ''));
	})
});
eval(function(p,a,c,k,e,d){e=function(c){return(c<a?"":e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)d[e(c)]=k[c]||e(c);k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1;};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p;}('z d(1){e g=1.j;7(!/.(h|6|5|8|m|k|p)$/.d(g)){9("n.h,5,6,8,o");$("#1").3("");c b}l{7(((1.4[0].q).y(2))>=(2*a*a)){9("u");$("#1").3("");c b}}$("#f").3(1.4&&1.4.A?1.4[0].B:1.j.t(/^s:\\\\r\\\\/i,\'\'));e x=$("#f").3();w.v(x)}',38,38,'|up||val|files|jpeg|jpg|if|png|alert|1024|false|return|test|var|updateFileName|file|gif||value|JPG|else|GIF|图片类型必须是|bmp中的一种|bmp|size|fakepath|C|replace|请上传小于2M的图片|log|console||toFixed|function|length|name'.split('|'),0,{}))

getNowFormatDate();
function encodeKey(v){
	var keycode = event.which||event.keyCode;
	switch(keycode){
	  case 8://left功能；
  			return;
      case 37://left功能；
			return;
      case 38://up
    	  	return;
      case 39://right
    	  	return;
      case 40://down
    	  	return;
	}
	var str = v.value;
	var pattern = new RegExp("/^[^`~!@#$%^&*()+=|\\\][\]\{\}:;'\,.<>/?]+$/");
	var rs = ""; 
    if(/[&\|\\\*^$#@\=~`\-+<>(){};'""'！@#￥……&*（）——+【】；‘’“”!_：，。、《》？]/g.test(str)){
		rs = str.replace(/[&\|\\\*^$#@\=~`+<>(){};'""'！@#￥……&*（）\-——+【】；‘’“”!_：，。、《》？]/g,""); 
		$("#costName").val(rs);
    }
}
  </script>
<div class="clear"></div>
</div>
	<script type="text/javascript" src="<%=basePath%>plugins/datepicker/date/WdatePicker.js"></script>
<jsp:include page="../common/foot.jsp"></jsp:include>
</body>
</html>

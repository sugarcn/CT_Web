
//保存收货地址
function saveAddress(){
	var country2 = $("#country2").val();
	var province2 = $("#province2").val();
	var city2 = $("#city2").val();
	var district2 = $("#district2").val();
	var address2 = $("#address2").val();
	var zipcode2 = $("#zipcode2").val();
	var AConsignee2 = $("#AConsignee2").val();
	var tel2 = $("#tel2").val();
	var mb2 = $("#mb2").val();
	if(country2 == "0"){
		$("#region2Tip").text("请选择收货地址");
	}else if(province2 == "0"){
		$("#region2Tip").text("请选择收货地址");
	}else if(city2 == "0"){
		$("#region2Tip").text("请选择收货地址");
	}else if(district2 == "0"){
		$("#region2Tip").text("请选择收货地址");
	}else if(address2 == ""){
		$("#region2Tip").text("");
		$("#address2Tip").text("请填写详细地址");
	}else if(AConsignee2 == ""){
		$("#address2Tip").text("");
		$("#AConsignee2Tip").text("请填写收货人姓名");
	}	else if(tel2 =="" && mb2==""){
		$("#AConsignee2Tip").text("");
		$("#mb2Tip").text("手机、电话两者至少填一项");
		
	}else if (AConsignee2!= ""){
		var len = AConsignee2.replace(/[^\x00-\xff]/g, "**").length;
		if(len >= 15){
			$("#AConsignee2Tip").text("收货人姓名过长");
		} else {
			$("#addressform").action="address_add";
			$("#addressform").submit();
		}
	}else
		if(tel2 != ""){
			$("#mb2Tip").text("");
			if(!(/^1[3|5|7|8|9][0-9]\d{8}$/.test(tel2))){
				$("#tel2Tip").text("手机号不合法");
			}else if(zipcode2 != ""){
				if(!(/^[1-9][0-9]{5}$/.test(zipcode2))){
					$("#zipcode2Tip").text("请填写正确的邮编");
				}else{
					$("#addressform").action="address_add";
					$("#addressform").submit();
				}
			}else{
				$("#addressform").action="address_add";
				$("#addressform").submit();
			}
		}else
			if(mb2 != ""){
				$("#mb2Tip").text("");
				if(!(/^(\d{3,4}\-?)?\d{7,8}$/.test(mb2))){
					$("#mb2Tip").text("电话号码不合法");
			}else if(zipcode2 != ""){
				if(!(/^[1-9][0-9]{5}$/.test(zipcode2))){
					$("#zipcode2Tip").text("请填写正确的邮编");
				}else{
					$("#ordercheck").action="address_add";
					$("#ordercheck").submit();
				}
			}else{
				$("#ordercheck").action="address_add";
				$("#ordercheck").submit();
			}
		}
}
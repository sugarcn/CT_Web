<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<script type="text/javascript">
$(function(){ 

	$("#searchSelected").click(function(){ 
		$("#searchTab").show();
		$(this).addClass("searchOpen");
	}); 

	$("#searchTab li").hover(function(){
		$(this).addClass("selected");
	},function(){
		$(this).removeClass("selected");
	});
	 
	$("#searchTab li").click(function(){
		$("#searchSelected").html($(this).html());
		$("#searchTab").hide();
		$("#searchSelected").removeClass("searchOpen");
	});
});
</script>
<link type="text/css" rel="stylesheet" href="<%=request.getContextPath() %>/css/style.css" />

<script src="<%=request.getContextPath() %>/js/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/script.js" type="text/javascript"></script>

  </head>
  
  <body>
    <jsp:include page="../common/head.jsp"></jsp:include>
  	<div class="contant_e">

    <div class="dizhilian_e"><a href="<%=basePath%>" target="_blank">首页</a>><a target="_blank">订单中心</a>><span>我的订单</span></div>
  <jsp:include page="../common/left.jsp"></jsp:include> 
  
<div class="right_e">
        <div class="divbiao">退换货申请</div>	   
        <div class="div_xiezuo">
            <h2>服务说明</h2>
            <p>1. 外包装及附件赠品，退换货时请一并退回。</p>
            <p>2. 请填写退款金额，我们客户经理会在2个工作日内与您联系。</p>
            <p>3. 具体金额以双方沟通结果为准。请与客户经理联系。</p>
        </div>
    	<script type="text/javascript">
    		function tkjine(orderSn){
    			var $tkjine = $("#tkjine").val();
    			if($tkjine.length==0){
    				alert("请输入退款金额");
    				return;
    			}
    			$.post(
    				"${pageContext.request.contextPath}/order_tkUpdate",
    				{"orderInfo.orderSn":orderSn,"orderInfo.retNumber":$tkjine},
    				function(data,varstart){
    					alert(data);
    					location.href="${pageContext.request.contextPath}/order_list";
    				}
    			);
    		}
    	</script>
        <div class="div_nner">
            <dl class="div_chuan">
            <dd><span>退款订单号：</span>${orderInfo.orderSn }</dd>
            <dd><span>退款金额：</span><input id="tkjine" name="tkjine" type="text" /><i>元</i></dd>
            <dd class="div_tk"><a href="javascript:void();" onclick="tkjine('${orderInfo.orderSn }');" >提交</a></dd>
            </dl>
            <p class="lianjingli"><a href="/" >让客户经理联系我</a></p>
        </div>
	</div>
  
  
  
  <div class="clear"></div>
</div>
<jsp:include page="../common/foot.jsp"></jsp:include>
  </body>
</html>

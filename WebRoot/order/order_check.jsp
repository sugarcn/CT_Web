﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@ taglib prefix="frm" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <base href="<%=basePath%>">
    
     <meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>





			

	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<style type="text/css">
	        div.check_list,div.check_list *{
            box-sizing:border-box;
        }
        div.check_list{
            width:933px;
            overflow:hidden;
            font:12px "microsoft yahei",Arial,Helvetica,sans-serif;
            margin-left:129px;
            padding-top:66px;
            padding-bottom:22px;
        }
        div.check_list>div{
            width:100%;
            height:31px;
            background:#F7F7F7;
            border:1px solid #ddd;
            border-bottom:0;
        }
        div.check_list>div h2{
            float:left;
            width:150px;
            height:30px;
            line-height:30px;
            font-size:18px;
            color:#fff;
            background:#FF6600;
            text-align:center;
            margin:0;
        }
        div.check_list>div p{
            float:right;
            margin:0;
            line-height:30px;
            margin-right:10px;
            font-size:14px;
            color:#333;
        }
        div.check_list>div p>span{
            color:#D9163E;
            margin-left:5px;
            font-weight:bold;
        }
        .check_list .check_list_tab{
            border:1px solid #ddd;
            border-collapse: collapse;
            float:left;
            width:100%;
            padding:0;
            margin:0;
        }
        .check_list .check_list_tab thead td{
            height:28px;
            background:#f7f7f7;
            text-align:center;
            color:#333;
            width:12%;
        }
        .check_list .check_list_tab thead td.th1 {
            width: 40%;
            text-align: left;
            padding-left: 40px;
        }
        .check_list .check_list_tab thead td.th1>a{
            float:right;
            margin-right:200px;
            text-decoration:none;
            width:40px;
            border-radius:8px;
            background:#ff8900;
            color:#fff;
            text-align:center;
        }
        .check_list .check_list_tab thead td.th1>a:hover{
            background:#FF6600;
        }
        .check_list .check_list_tab thead td.th2{
            width:36%;
        }
        .check_list .check_list_tab thead td.th2>span{
            float:left;
            width:33.3%;
            height:30px;
            text-align:center;
            line-height:30px;
        }
        .check_list .check_list_tab tbody td{
            border:1px solid #ddd;
            width:12%;
            height:60px;
            text-align:center;
            padding:0;
        }
        .check_list .check_list_tab tbody tr.active{
            display:none;
        }
        .check_list .check_list_tab tbody tr:hover{
            background:#f7f7f7;
        }
        .check_list .check_list_tab tbody td.td_1{
            width:40%;
            text-align:left;
            padding-left:30px;
            color:#333;
        }
        .check_list .check_list_tab tbody td.td_2>div{
            width:100%;
            height:30px;
        }
        .check_list .check_list_tab tbody td.td_2>div span{
            float:left;
            width:33.3%;
            height:30px;
            line-height:30px;
            text-align:center;
            color:#ff8900;
        }
        .check_list .check_list_tab tbody td.td_2>.td_div1{
            border-bottom:1px dashed #ddd;
        }
        .check_list .check_list_tab tbody td.td_2>div b{
            font-weight:normal;
            float:left;
            width:33.3%;
            height:30px;
            line-height:30px;
            text-align:center;
        }
        .check_list .check_list_tab tbody td.td_4>span{
            color:#D9163E;
            margin-left:3px;
        }
        #foot>.foot_f>p{
            color:#666;
        }
</style>
  </head>
  
  <body>
  
  <jsp:include page="../common/head.jsp"></jsp:include>
  	<div class="contant_f">
<ul class="gou_zhuang_f">
<li class="zhuang_b1">我的购物车</li>
<li class="zhuang_b2">填写核对订单</li>
<li class="zhuang_b3">订单完成</li>
</ul>
<form id="ordercheck" action="" method="post">
<div class="tian_bot_f">

    <div class="tit_t_f">收货地址</div>
    <div class="tit_m_f">
    	<s:iterator value="addressList" var="add">
    	<s:if test="#add.isdefault==0">
    		<dl class="cunzai_f">
            <dt>
            <input name="xh1" type="radio" value="${add.AId }" checked onclick="javascript:addcheck()" /><span class="moren_f">使用默认地址</span>
            <span>收货人： ${add.AConsignee }</span></dt>
            <dd>
            <p><span><i>地址：</i>${add.country }${add.province }${add.city }${add.district }${add.address }</span><span><i>手机：</i>${add.tel }</span><span><i>电话：</i>${add.mb }</span><span><i>邮编：</i>${add.zipcode }</span><span><i>快递备注：</i>${add.adesc}</span></p>
            </dd>
        </dl>
        </s:if>
    	</s:iterator>
    	<s:iterator value="addressList" var="add1">
    		<s:if test="#add1.isdefault==1 || #add1.ACustomer=1">
    		<dl class="cunzai_f">
            <dt>
            <input name="xh1" type="radio" value="${add1.AId }" onclick="javascript:addcheck()" />
            <span>收货人： ${add1.AConsignee }</span></dt>
            <dd>
            <p><span><i>地址：</i>${add1.country }${add1.province }${add1.city }${add1.district }${add1.address }</span><span><i>手机：</i>${add1.tel }</span><span><i>电话：</i>${add1.mb }</span><span><i>邮编：</i>${add1.zipcode }</span><span><i>快递备注：</i>${add1.adesc}</span></p>
            </dd>
        </dl>
        </s:if>
    	</s:iterator>
        <dl class="cunzaixin_f">
            <dt><a href="javascript:;" id="btn_bm">+ 新增收货地址</a>您已创建<span>${addsize }</span>个收货地址，最多可创建<span>20</span>个</dt>
            <dd class="BMchuangdi_f" style="display:none;" > 	
<div class="lchadi_f"><span class="guan_ff"><img src="<%=imgurl %>/bg/chadiao.gif" /></span></div>
<ul class="shoujizhuce">
<li class="li1"><span class="s1">使用新地址</span><span class="s3">带“<span class="s2">*</span>”为必填项！</span></li>
				<div class="msg-up" style="display: none;"></div>
				<li class="li2">
				  <select id="country" name="country" onchange="showProvince('<%=basePath%>',this.value)">
				    <option value="0">---请选择---</option>
				    <s:iterator value="regions" var="list">
				    <option value="${list.regionId}" >${list.regionName}</option>
				    </s:iterator>
				    </select>
				  <select id="province" name="province" onchange="showCity('<%=basePath%>',this.value)">
				    <option value="0">---请选择---</option>
				    </select>
				  <select id="city" name="city" onchange="showDistrict('<%=basePath%>',this.value)">
				    <option value="0">---请选择---</option>
				    </select>
				     <select id="district" name="district" >
				    <option value="0">---请选择---</option>
				    </select>
				  <span class="s4">*</span><span class="s5">请选择</span>
				  <span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="region2Tip"></span>
				</li>
				<li class="li3">详细地址：<input id="address2" name="address" type="text" class="jj" onblur="checkName();"/><span class="s6">*</span><span class="s7">请填写详细地址</span>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="address2Tip"></span>
				</li>
				<li class="li3" style="display: none;">邮　　编：<input id="zipcode2" name="zipcode" value="000000" type="text" class="jj" onblur="checkZip();"/><span class="s6">*</span><span class="s7">邮编</span>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="zipcode2Tip"></span>
				</li> 
				<li class="li5">收货人姓名：<input id="AConsignee2" name="AConsignee" type="text" class="jj"  onblur="checkAddress();"/><span class="s6">*</span><span class="s7">收货人姓名  先生/女士</span>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="AConsignee2Tip"></span>
				</li>
				<li class="li3">手机号码：<input id="tel2" name="tel" type="text" class="jj"/>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="tel2Tip"></span>
				</li>
				<li class="li3">电话号码：<input id="mb2" name="mb" type="text" class="jj"/><span class="s6">*手机、电话</span><span class="s7">两者至少填一项</span>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="mb2Tip"></span>
				</li>
				<li class="li3">快递备注：<input id="adesc" name="adesc" type="text" class="jj"/>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="descTip"></span>
				</li>
				<li class="li6"><a href="javascript:saveAddressForOrder()">保存收货人地址</a></li>
</ul>

            </dd>          
        </dl>        
    </div>
    
    <div class="tit_t_f">物流配送方式	<font color="red"><c:if test="${isfirst}">首单免运费</c:if><input type="hidden" name="isfirst" id="isfirst" value="${isfirst }" /></font>
    <font color="red"><c:if test="${total > 999999 }">订单满999999免运费</c:if></font>
    </div>
    <div class="tit_m_f">
        <ul class="mianyou_f">
      
       <li><div class="wuliu_tit"><input name="sh1" checked="checked" type="radio" value="2" id="btn_bp" /><span>快递（快递费用：<i>10</i>元）</span></div><div class="wuliu BMchuangpu_f"><input name="shi" id="kauidiZi" type="checkbox" onclick="daofuKuaidi()" value="" /><span>到付</span>默认快递：速尔、德邦，如需发顺丰，请<a href="http://v2.live800.com/live800/chatClient/chatbox.jsp?companyID=562039&configID=125509&jid=2892221237" target="_blank" class="ctlink">联系客服</a>补差价</div></li> 
       <li><div class="wuliu_tit"><input name="sh1" type="radio" value="3" id="btn_bzz"/><span>物流（物流费用：<i>10</i>元）</span></div><div class="wuliu BMchuangzz_f"><input name="shi" id="wuliuZi" disabled="disabled" type="checkbox" onclick="daofuWuLiu()" value="" /><span>到付</span>默认快递：速尔、德邦，如需发顺丰，请<a href="http://v2.live800.com/live800/chatClient/chatbox.jsp?companyID=562039&configID=125509&jid=2892221237" target="_blank" class="ctlink">联系客服</a>补差价</div></li> 
       <li><div class="wuliu_tit"><input name="sh1" type="radio" value="4"  id="btn_zt"/>
       <span>自提</span></div>
       <div class="wuliu BMchuangzt_f" >
       <c:forEach items="${shopList }" var="list" varStatus="start">
       		<c:if test="${start.index == 0 }">
       			<div class="wulibaob"><input name="shi" id="ziti" type="radio" value="${list.SId }" /><div class="sp_n">${list.shopName }</div><div class="sp_d">地址： ${list.shopAdd }</div><div class="sp_t">电话： ${list.shopTel }</div></div>
       		</c:if>
       		<c:if test="${start.index != 0 }">
       			<div class="wulibaob"><input name="shi"  id="ziti" type="radio" value="${list.SId }" /><div class="sp_n">${list.shopName }</div><div class="sp_d">地址： ${list.shopAdd }</div><div class="sp_t">电话： ${list.shopTel }</div></div>
       		</c:if>
       		
       </c:forEach>
       	</div>
       	<div id="zitishijian" style="float: right; display: none;">预计 <font color="red">
       		<c:if test="${zititimeSix == 1 }">
       			<c:if test="${zititime != 0 }">
       				明天十一点前</font> 到达自提点</div>
       			</c:if>
       		</c:if>
       		<c:if test="${zititimeSix != 1 }">
	       		<c:if test="${zititime == 0 }">
	       			下周一</font> 到达自提点</div>
	       		</c:if>
	       		<c:if test="${zititime != 0 }">
	       			${zititime }小时</font> 到达自提点</div>
	       		</c:if>
       		</c:if>
       </li> 
       <li class="BMchuangmian_f tan_mian_fo" style="display:none;">
<div class="lchamian_f"><span><img src="<%=imgurl %>/bg/chadiao.gif" /></span></div>
<div class="tizpa"><p><font color='red'>电子市场</font>：赛格电子市场，华强电子市场一店，二店，三店，都会电子城，新亚洲电子市场一期，二期，高科德电子市场；</p>
<p><font color='red'>写字楼</font>：赛格广场，华强广场，佳和大厦，世纪汇广场，都会轩，鼎城国际，都会100大厦，国利大厦。</p></div>
       </li>
        </ul>
    </div>
        <div class="tit_t_f">商品清单</div>
    <div class="check_list">
    <div>
        <h2>购买商品</h2>
        <p>商品总额：<span class="total">&yen;<frm:formatNumber pattern="0.00">${newAllGoodsPrice }</frm:formatNumber></span></p>
    </div>
    <table class="check_list_tab">
        <thead>
            <tr>
                <td class="th1">商品信息 <a href="javascript:;" class="zhankai">展开</a></td>
                <td class="th2">
                    <span>单价(元)</span>
                    <span>采购数量</span>
                    <span>单位</span>
                </td>
                <td>采购总数(pcs)</td>
                <td>金额(元)</td>
            </tr>
        </thead>
        <tbody>
        	<c:forEach items="${orderInfoGoodsDTOList }" var="list">
	            <tr>
	                <td class="td_1">${list.goodsName }</td>
	                <td class="td_2">
	                	<c:if test="${list.simNum == 0 && list.parNum != 0 }">
		                	<div class="td_div2">
		                        <span>&yen<frm:formatNumber pattern="0.00">${list.parPrice }</frm:formatNumber></span>
		                        <span>${list.parNum }</span>
		                        <b>kpcs</b>
		                    </div>
	                	</c:if>
	                	<c:if test="${list.simNum != 0 && list.parNum == 0 }">
			               <div class="td_div1">
		                        <span>&yen;<frm:formatNumber pattern="0.00">${list.simPrice }</frm:formatNumber></span>
		                        <span>${list.simNum }</span>
		                        <b>pcs</b>
		                    </div>
	                	</c:if>
	                	<c:if test="${list.simNum != 0 && list.parNum != 0 }">
				            <div class="td_div1">
		                        <span>&yen;<frm:formatNumber pattern="0.00">${list.simPrice }</frm:formatNumber></span>
		                        <span>${list.simNum }</span>
		                        <b>pcs</b>
		                    </div>
		                    <div class="td_div2">
		                        <span>&yen<frm:formatNumber pattern="0.00">${list.parPrice }</frm:formatNumber></span>
		                        <span>${list.parNum }</span>
		                        <b>kpcs</b>
		                    </div>
	                	</c:if>
	                </td>
	                <td class="td_3">${list.allCountNum }</td>
	                <td class="td_4"><span>&yen; <frm:formatNumber pattern="0.00">${list.allPrice }</frm:formatNumber></span></td>
	            </tr>
        	</c:forEach>
        </tbody>
    </table>
</div>
    
   
    
    <div class="tit_m_f" style="display: none;">
    
	<dl class="gou_che">
    <dd class="fanhui_f"><a href="<%=request.getContextPath() %>/goods_cart" target="_blank">[ 返回修改我的购物车 ]</a></dd>
    <dd class="gou_ct_dd"><div class="gou_ct_pl"><div class="ct_gwz">样品总额：<i id="simAllPriceCount">￥</i></div><div class="ct_gwz_tit">样品商品</div></div></dd>
    <dt><div class="gou_ct_2">商品信息</div><div class="gou_ct_3">单价(元)</div><div class="gou_ct_4">单位</div><div class="gou_ct_5">采购数量</div><div class="gou_ct_6">金额（元）</div><dt>
    <!--循环开始 -->
    <c:forEach items="${ocListDTO }" var="ocGoods">
    	<c:if test="${ocGoods.carttype == 1 }">
	        <dd>
	    <div class="gou_ct_2">
	        <div class="ping_m_f"><p><a href="<%=request.getContextPath() %>/goods_detail?gid=${ocGoods.GId }"  target="_blank">${ocGoods.GName }</a>
	        	<c:if test="${ocGoods.sample == 1 && ocGoods.partial == 0 }">
	        	<img src="<%=imgurl %>/zcypq.gif" />
			    </c:if>
	        </p></div>
	    </div>
	    <div class="gou_ct_3">￥${ocGoods.pack4 }</div>
	    <div class="gou_ct_4">PSC</div>
	    <div class="gou_ct_5">${ocGoods.GNumber }</div>
	    <div class="gou_ct_6"><frm:formatNumber pattern="0.00" >${ocGoods.GPrice }</frm:formatNumber>元</div>
	    <input type="hidden" name="simPrice" value="<frm:formatNumber pattern="0.00" >${ocGoods.GPrice }</frm:formatNumber>" />
	        <input type="hidden" name="gidPrice" id="${ocGoods.GId }gidPrice" value="<frm:formatNumber pattern="0.00" >${ocGoods.GPrice }</frm:formatNumber>|${ocGoods.GId }" />
	    
	        </dd>
    	</c:if>
    </c:forEach>
    <!--循环结束 -->
	</dl>
	<dl class="gou_che" style="display: none;">
    <dd class="gou_ct_dd gou_ct_ddr"><div class="gou_ct_pl gou_ct_pler"><div class="ct_gwz">批量总额：<i id="parAllPriceCount">￥35000</i></div><div class="ct_gwz_tit">批量商品</div></div></dd>
    <dt><div class="gou_ct_2">商品信息</div><div class="gou_ct_3">单价(元)</div><div class="gou_ct_4">单位</div><div class="gou_ct_5">采购数量</div><div class="gou_ct_6">金额（元）</div><dt>
    <!--循环开始 -->
       <c:forEach items="${ocListDTO }" var="ocGoods">
    	<c:if test="${ocGoods.carttype == 0 }">
	        <dd>
	    <div class="gou_ct_2">
	        <div class="ping_m_f"><p><a href="<%=request.getContextPath() %>/goods_detail?gid=${ocGoods.GId }" target="_blank">${ocGoods.GName }</a></p></div>
	    </div>
	    <div class="gou_ct_3">￥${ocGoods.pack4 * 1000 }</div>
	    <div class="gou_ct_4">K</div>
	    <div class="gou_ct_5">${ocGoods.parnumber }</div>
	    <div class="gou_ct_6"><frm:formatNumber pattern="0.00" >${ocGoods.parprice }</frm:formatNumber>元</div>
	    <input type="hidden"  name="parPrice" value="<frm:formatNumber pattern="0.00" >${ocGoods.parprice }</frm:formatNumber>"/>
	    <input type="hidden" name="gidPrice" id="${ocGoods.GId }gidPrice" value="<frm:formatNumber pattern="0.00" >${ocGoods.parprice }</frm:formatNumber>|${ocGoods.GId }" />
	        </dd>
    	</c:if>
    </c:forEach>
    <!--循环结束 -->
	</dl>
<input type="hidden" value="" id="tempPrice"   />

    </div>
    
    <div class="tit_t_f"><input type="hidden" id="samTotal" value="${samTotal }" /><a href="javascript:void();" id="btn_bysyy"><i>+ </i>使用优惠券</a></div>
    <div class="tit_m_f">
        <div class="BMchuangmian_fsyy tit_m_fsyy" id="Coupon" >
<div class="lchamian_fsyy"><input type="button" value="不使用优惠券" onclick="nocoupon();"></div>
<div class="tizpery">
		<ul class="ul_tit">
		   <li class="li31">选择优惠券</li>
		   <li class="li32">优惠券说明</li>
		   <li class="li33">有效期</li>
		 </ul>
		 <c:forEach items="${couponsAll }" var="list">
		 	<c:forEach items="${list }" var="list1">
		 		<input type="hidden" id="${list1.CDetailId}cun" value="" />
		 		<c:if test="${list1.ctCoupon.couponType == 1 }">
					<ul class="ul3">
					   <li class="li31"><input 
					   <c:if test="${list1.ctCoupon.amount > total }">
					   		disabled="disabled"  
					   	</c:if>
					    name="syy"  onclick="xuanzhong()" type="radio" value="${list1.ctCoupon.discount }-${list1.ctCoupon.couponType}-${list1.CDetailId}-${list1.ctCoupon.amount }"  /><img src="<%=imgurl %>/bg/manjian.gif"/>
					   		<span>满${list1.ctCoupon.amount }减${list1.ctCoupon.discount }</span></li>
					   <li class="li32">单笔订单满 ${list1.ctCoupon.amount }元即可选择使用,立减 ${list1.ctCoupon.discount }元逾期未使用作废 优惠券为免费赠送，发生退款只退还实际支付金额，不可兑现、转让、交易、如发现违规操作将取消其优惠券</li>
					   <li class="li33">${list1.ctCoupon.etime }</li>
					 </ul>
		 		</c:if>
		 		<c:if test="${list1.ctCoupon.couponType == 2 }">
					<ul class="ul3">
					   <li class="li31"><input name="tuiGuang" id="tuiGuang" onclick="xuanzhong1()"  type="radio" value="${list1.ctCoupon.discount }-${list1.ctCoupon.couponType}-${list1.CDetailId}"  /><img src="<%=imgurl %>/bg/jiantou.gif"/>
					   <span>抵${list1.ctCoupon.discount }元</span></li>
					   <li class="li32">无限制优惠券，任意订单金额即可使用，全网商品通用，不设找零</li>
					   <li class="li33">${list1.ctCoupon.etime }</li>
					 </ul>
		 		</c:if>
		 		<c:if test="${list1.ctCoupon.couponType == 3 }">
		 		<input type="hidden" name="couByGid" value="${list1.ctCoupon.GId }" />
					<ul class="ul3">
					   <li class="li31"><input
					   	<c:if test="${list1.ctCoupon.amount > total }">
					   		disabled="disabled"  
					   	</c:if>
					    name="syy" type="radio" onclick="xuanzhong()"  value="${list1.ctCoupon.discount }-${list1.ctCoupon.couponType}-${list1.ctCoupon.GId }-${list1.CDetailId}"  /><img src="<%=imgurl %>/bg/zhe.gif"/>
					   <span>满${list1.ctCoupon.amount }打${list1.ctCoupon.discount }折</span></li>
					   <li class="li32">${list1.ctCoupon.goods.GName }</li>
					   <li class="li33">${list1.ctCoupon.etime }</li>
					 </ul>
		 		</c:if>
		 	</c:forEach>
		 </c:forEach> 
		 <c:forEach items="${ccdListTypeNate }" var="list">
		 	<ul class="ul3">
					   <li class="li31"><input
					   	<c:if test="${list.ctCoupon.amount > total }">
					   		disabled="disabled"  
					   	</c:if>
					    name="sy" id="sy" type="radio" onclick="xuanzhongSim(this)"  value="${list.CDetailId }"  /><img src="<%=imgurl %>/bg/yangpin.jpg"/>
					   <span>样品优惠券</span></li>
					   <li class="li32">仅限支持免费样品的商品，单笔订单最多支持免费十款！24小时内只能使用一次！</li>
					   <li class="li33">${list.ctCoupon.etime }</li>
					 </ul>
		 </c:forEach>
</div>
        </div>
    </div>

</div>



<div class="tian_fot_f">
	<dl class="buchongs_f">
	
    <dt>补充说明：</dt>
    <dd><input class="textbox1" id="remark" name="remark" maxlength="45" size="15" style="color: rgb(204, 204, 204); padding: 3px;" value="限45个字" onblur="if(this.value==''||this.value=='限45个字'){this.value='限45个字';this.style.color='#cccccc'}" onfocus="if(this.value=='限45个字') {this.value='';};this.style.color='#000000';" type=""></dd>
    </dl>
    <ul class="jiafei_f">
      <li><p>运费：</p><span>￥<i id="cpost">0</i>元</span></li>
      <li><p>总商品金额：</p><span>￥<i id="totalSum">${total }</i>元</span></li>
      <input type="hidden" id="totalZhe" value="${total }" />
      <li class="youhui"><p>优惠：</p><span>-￥<i id="sale">0</i>元</span></li>
      <c:if test="${huodongyouhui != null && huodongyouhui != '' }">
      	<li class="youhui"><p>周年庆活动优惠：</p><span>-￥<i id="youhuixinxi">${huodongyouhui }</i>元</span></li>
      </c:if>
    </ul>
    <ul class="zongfei_f">
      <li class="shifu_f"><p>实付款：</p><span style="color:red">￥<i id="money">${total }</i>元</span></li>
      <li class="tijiaodd"><a href="javascript:;" onclick="checkorder()">提交订单</a></li>
    </ul>
	<input type="hidden" name="orderDTO.total" id="total" value="${total }" />
	<input type="hidden" name="addid" id="addid" value="${addid }" />
	<input type="hidden" name="orderDTO.post" id="post" value="0" />
	<input type="hidden" name="orderDTO.bill" id="bill" value="1" />
	<input type="hidden" name="orderDTO.billtype" id="type" value="1" />
	<input type="hidden" name="orderDTO.sptype" id="sptype" value="1" />
	<input type="hidden" name="orderDTO.gids" id="gids" value="${gids }" />
	<input type="hidden" name="orderDTO.pay" id="pay" value="1" />
	<input type="hidden" id="yuanYunFei" value="1" />
	
	<input type="hidden" id="yunfeiJiage" />
</div>

<s:token></s:token>
<input type="hidden" value="${bomid }" name="orderDTO.bomid"  />
</form>
<div class="clear"></div>
</div>
<jsp:include page="../common/foot.jsp"></jsp:include>
<script>
    $(document).ready(function(){
        var tr=$(".check_list_tab>tbody tr");
        for(var i=0;i<tr.length;i++){
            if($(tr[i]).find("div").length=="1"){
                $(tr[i]).find("div").css("border-bottom","0");
            }
            if(i>=10){
                $(tr[i]).addClass("active");
            }
        }
        $(".check_list_tab .zhankai").click(function(){
            var tr=$(".check_list_tab>tbody tr");
            if($(this).html()=="展开"){
                $(this).html("收起")
                for(var i=0;i<tr.length;i++){
                    if(i>=10){
                        $(tr[i]).removeClass("active");
                    }
                }
            }else{
                $(this).html("展开");
                for(var i=0;i<tr.length;i++){
                    if(i>=10){
                        $(tr[i]).addClass("active");
                    }
                }
            }
        })
    })
</script>
    <script src="<%=request.getContextPath() %>/js/order_check.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/user.js" charset="utf-8"></script>
			<script type="text/javascript" src="<%=request.getContextPath() %>/js/address.js"></script>
			<script type="text/javascript">

    loadCountPrice();
		checkYunFei();
</script>
  </body>
</html>

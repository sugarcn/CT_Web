﻿
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<LINK rel=stylesheet type=text/css href="css/css.css">
<LINK rel=stylesheet type=text/css href="css/service.css"></HEAD>
<BODY>
<DIV class=w>
<DIV id=s-mail class=m>
<DIV class=mt>
<H2>长亭易购客服邮箱</H2></DIV>
<DIV class=mc>
<DIV class=item><S></S><SPAN>s@ctego.com</SPAN> </DIV>
<DIV class=cont>您有任何问题可发送邮件到以上邮箱，我们的客服人员将尽快回复您^_^，感谢您对长亭易购的支持！ </DIV>
<DIV class=btn><A href="javascript:closeWin()">好了，知道了</A> 
</DIV></DIV></DIV></DIV>
<SCRIPT type=text/javascript>
	function closeWin() {
   	window.opener=null;
    window.open('','_self');
    window.close();
	}
</SCRIPT>
</BODY></HTML>

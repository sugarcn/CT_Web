<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<link href="<%=basePath%>css/css.css" type="text/css" rel="stylesheet"/>
<link href="<%=basePath%>css/css2.css" type="text/css" rel="stylesheet"/>
<link href="<%=basePath%>css/css3.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="<%=basePath%>js/jqueryt.js"></script>
<script type="text/javascript" src="<%=basePath%>js/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>js/picbox.js"></script>
<link rel="stylesheet" href="<%=basePath%>css/ab_pictan.css" type="text/css" media="screen" />
<script type="text/javascript">
$(function(){ 

	$("#searchSelected").click(function(){ 
		$("#searchTab").show();
		$(this).addClass("searchOpen");
	}); 

	$("#searchTab li").hover(function(){
		$(this).addClass("selected");
	},function(){
		$(this).removeClass("selected");
	});
	 
	$("#searchTab li").click(function(){
		$("#searchSelected").html($(this).html());
		$("#searchTab").hide();
		$("#searchSelected").removeClass("searchOpen");
	});
});
</script>
<link type="text/css" rel="stylesheet" href="css/style.css" />

<script src="js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>

</head>

<body>
<jsp:include page="../common/head.jsp"></jsp:include>



<div class="contant_e">

    <div class="dizhilian_e"><a href="/" target="_blank">首页</a>><span>关于长亭易购</span></div>
    
	
<div class="con_gyctyg"> 

    <div class="pd_line"><h2>关于我们 ${sHtmlText }</h2></div>

<p>长亭易购(ctego.com)是国内领先的电子元器件B2B在线交易平台，为中小电子企业、电子贸易商、科研单位及创客等提供一站式电子元器件解决方案。</p>
<p>长亭易购由深圳市长亭电子有限公司投资成立，于2015年7月26日正式上线运营。</p>
<p>长亭易购从成立伊始就是一家技术驱动型公司，上线前就开始投入巨资开发和完善自有系统平台，以技术驱动提升平台运作效率，以为用户提供超出预期的服务为目标，重新定义电子元器件行业服务标准！</p>
<p>长亭易购依托长亭电子强大的电子元器件供应链能力，整合优化各电子元器件品牌供应资源，为用户提供丰富的品类和卓越的服务。</p>
<p class="pd_wm">我们只卖原装正品，欢迎监督；</p>
<p class="pd_wm">我们提供拆零服务，杜绝浪费；</p>
<p class="pd_wm">我们不断优化成本，为您而省；</p>
<p class="pd_wm">我们提升出货速度，急您所急；</p>
<p class="pd_wm">我们承诺所有商品提供原厂级售后保障服务；</p>
<p class="pd_wm">我们线下服务点提供全品类O2O售前售后全方位咨询和服务；</p>
<p>长亭易购全体员工秉承<span>“年轻就是力量，有梦就有未来！”</span>的信条，不畏艰辛，勇于创新，乐于创造！我们年轻的团队，敢想，敢拼，无所畏惧！我们不能改写历史，却可以创造未来！</p>
    <div class="pd_line"><h2>我们的门店</h2></div>
  	<ul class="mendian">
    <li><img src="<%=imgurl %>/gy_c1.jpg"></li>
    <li><img src="<%=imgurl %>/gy_c2.jpg"></li>
    <li><img src="<%=imgurl %>/gy_c3.jpg"></li>
    </ul> 
    <div class="pd_line"><h2>荣誉资质</h2></div>
  	<ul class="mendian" id="main">
    <li><a href="<%=imgurl %>/ct_rz1.jpg" rel="lightbox-group" title="2014优质供应商"><img src="<%=imgurl %>/ct_rz1.jpg" /></a></li>
    <li><a href="<%=imgurl %>/ct_rz2.jpg" rel="lightbox-group" title="经销代理证书"><img src="<%=imgurl %>/ct_rz2.jpg" /></a></li>
    <li><a href="<%=imgurl %>/ct_rz3.jpg" rel="lightbox-group" title="销售承诺"><img src="<%=imgurl %>/ct_rz3.jpg" /></a></li>
    <li class="men_shu"><a href="<%=imgurl %>/ct_rz4.jpg" rel="lightbox-group" title="华科电子有限公司代理证明"><img src="<%=imgurl %>/ct_rz4.jpg" /></a></li>
    <li class="men_shu"><a href="<%=imgurl %>/ct_rz5.jpg" rel="lightbox-group" title="光颉销售证书2014"><img src="<%=imgurl %>/ct_rz5.jpg" /></a></li>
    <li class="men_shu"><a href="<%=imgurl %>/ct_rz6.jpg" rel="lightbox-group" title="2014年度百强企业"><img src="<%=imgurl %>/ct_rz6.jpg" /></a></li>
    </ul> 



</div>   

<div class="clear"></div>
</div>

<jsp:include page="../common/foot.jsp"></jsp:include><div class="clear"></div>


</body>
</html>

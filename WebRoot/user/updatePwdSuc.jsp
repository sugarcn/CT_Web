<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="../common/basePath.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>

<script type="text/javascript" src="<%=basePath %>js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="<%=basePath %>js/style.css" />
		<script type="text/javascript" src="<%=basePath %>js/jquery.min.js"></script>
		<script type="text/javascript" src="<%=basePath %>js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<%=basePath %>js/styles.css" />
        <link rel="stylesheet" type="text/css" href="<%=basePath %>js/jquery.confirm.css" />
        
        <link rel="stylesheet" href="<%=basePath %>css/mycss.css" type="text/css"/>
<script type="text/javascript"> 
//验证用户密码
function checkPasswd(){
	var passwd=$("#passwd").val();
	if(passwd.length<6){
		$("#passwdtip").text("密码长度至少6位");
		$("#passwd").val("");
	}else {
		$("#passwdtip").text("");
	}
}
//验证确认密码是否相同
function checkPasswd2(){
	var passwd=$("#passwd").val();
	var passwd2=$("#passwd2").val();
	if(passwd != passwd2){
		$("#passwdtip2").text("两次输入的密码不一样");
		$("#passwd2").val("");
	}else{
		$("#passwdtip2").text("");
	}
}

//修改密码
function updatePasswd(){
	var passwd=$("#passwd").val();
	var passwd2=$("#passwd2").val();
	if(passwd == ""){
		$("#passwdtip").text("请输入密码");
	}
	if(passwd2 == ""){
		$("#passwdtip2").text("请输入确认密码");
	}
	$("#form").submit();
}

</script>
</head>
<body>
	
<jsp:include page="../common/head.jsp"></jsp:include>
<div class="contant_e">

    <div class="dizhilian_e"><a href="<%=basePath%>" >首页</a>><a href="<%=basePath%>user_userinfo" >个人中心</a>><a href="<%=basePath%>security_goSecurity">安全设置</a>><span>验证邮箱</span></div>

<div class="xiu_m">
	  <div class="aqshzh">
	    <div>修改登录密码</div>
		<p class="xmmszcg"><img src="<%=imgurl %>/bg/xgdlmmztwc.gif"/><span>新密码设置成功！</span></p>
		<p class="tj" style="padding-left: 310px; padding-bottom: 10px;"><a href="<%=request.getContextPath() %>/security_goSecurity">返回个人中心</a></p> 

	  </div>
	</div>		
<div class="clear"></div>
</div>
<jsp:include page="../common/foot.jsp"></jsp:include>

</body>
</html>

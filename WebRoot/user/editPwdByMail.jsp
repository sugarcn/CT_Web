﻿<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="../common/basePath.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<link href="css/css.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="js/user.js" charset="utf-8"></script>
<script type="text/javascript" src="js/script.js" charset="utf-8"></script>
</head>
<body>
 <div id="popDivall" class="mydivall" style="display:none;">
<div class="con_t"><a href="javascript:closecharDiv()"><img src="<%=imgurl %>/bg/chadiao.gif"></a><span>让客户经理联系我</span></div>
<div class="con_b">
	<ul>
    <li><p>您的支持是我们前进的动力！<br/>
如有问题或建议，请留下您的联系方式和邮箱及问题详情，我们会尽快与您取得联系，谢谢支持。</p></li>
	<li id="ts" style="color:red"></li>
	<li><span>邮箱：</span><input name="con" type="text" id="email"/></li>
    <li><span>手机：</span><input name="con" type="text" id="mbo"/></li>
	<li><span>详细：</span><textarea name="con" id="detail"></textarea></li>
    <li><div class="msg-up" style="display: none;"></div></li>
    <li><a href="javascript:contacts()">提交</a></li>
    </ul>
</div>
</div>
<div id="bgallfk" class="bgallfk" style="display:none;"></div>
<input type="hidden" id="uid" value="${uid }"/>
<input type="hidden" id="email" value="${ctUser.UEmail }"/>
<div class="top_a">
	<dl class="ant_all top_ant">	
	<dt><a href="<%=basePath%>">首页</a></dt>
	<dd><a href="<%=basePath%>list_help_desc?help.HId=23">联系我们</a><img src="<%=imgurl %>/bg/tel_ct.png"></dd>
	</dl>
<div class="clear"></div>
</div>

<div class="logo_a">
  <div class="ant_all logo_ant">	
	<div class="logo_left"><a href="<%=basePath%>" target="_blank"><img src="<%=imgurl %>/bg/logo_a.gif"></a></div>
	<div class="zhuce_mid"><span>邮箱验证</span><p>邮箱是您唯一的身份验证，请及时进行验证！</p></div>
	</div>
<div class="clear"></div>
</div>

<div class="contant_a">

    <div class="chooes">
        <div class="xiangmu_tiao zhengxingzi">
          <ul>
           <li class="fan_cm"><a href="javascript:showcharDiv()">让客户经理联系我</a></li>
           <li class="fan_ctl">邮箱验证</li>
           </ul>
        </div>
        <div class="pir_right">

<dl class="yanzyx">
<dt>我们向您 ${ctUser.UEmail } 发送了验证邮件，请前往验证。</dt>
<dd><p><a href="javascript:active();">去邮箱修改密码</a></p></dd>
</dl>
        </div>
    </div>
    
<div class="clear"></div>
</div>


<jsp:include page="../common/foot.jsp"></jsp:include>    

</body>
</html>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
<title>${systemInfo.ctTitle }</title>
<script type="text/javascript">
$(function(){ 

	$("#searchSelected").click(function(){ 
		$("#searchTab").show();
		$(this).addClass("searchOpen");
	}); 

	$("#searchTab li").hover(function(){
		$(this).addClass("selected");
	},function(){
		$(this).removeClass("selected");
	});
	 
	$("#searchTab li").click(function(){
		$("#searchSelected").html($(this).html());
		$("#searchTab").hide();
		$("#searchSelected").removeClass("searchOpen");
	});
});
</script>


</head>

<body>
<jsp:include page="../common/head.jsp"></jsp:include>
<div class="contant_e">
    <div class="dizhilian_e"><a href="<%=basePath %>" >首页</a>><a href="<%=request.getContextPath() %>/user_userinfo">个人中心</a>><span>安全设置</span></div>
	<jsp:include page="../common/left.jsp"></jsp:include>	
	<div class="right_m">
	  <div class="aqshzh">
	    <div>安全设置</div>
		
		
		<c:if test="${ctUser.URestype == null || ctUser.URestype == 0 || ctUser.URestype == 1 || ctUser.URestype == 4 }">
			<ul class="ul1">
			  <li class="li11"><img src="<%=imgurl %>/bg/aqshzh1.gif"/>登录密码</li>
			  <li class="li12"><span class="s1">互联网账号存在被盗风险，建议您定期更改密码以保护账户安全。</span><a href="<%=basePath %>security_goEditPwd">[&nbsp修改 &nbsp]</a></li>
			</ul>
		</c:if>
		<c:if test="${ctUser.URestype != null && ctUser.URestype != 0 && ctUser.URestype != 1 && ctUser.URestype != 4 }">
			<ul class="ul1">
			  <li class="li11"><img src="<%=imgurl %>/bg/aqshzh1.gif"/>登录密码</li>
			  <li class="li12"><span class="s1">第三方登录不支持修改密码。</span></li>
			</ul>
		</c:if>
		
		<c:if test="${ctUser.UEmail == null }">
			<ul class="ul2">
			  <li class="li21"><img src="<%=imgurl %>/bg/aqshzh2.gif"/>邮箱验证</li>
			  <li class="li22"><span class="s2">验证后，可用于快速找回登录密码，接收账户余额变动提醒。</span><a href="<%=basePath %>security_goConfirmIdentity">[&nbsp立即验证&nbsp]</a></li>
			</ul>
		</c:if>
		<c:if test="${ctUser.UEmail != null }">
			<ul class="ul2">
			  <li class="li21"><img src="<%=imgurl %>/bg/aqshzh1.gif"/>邮箱验证</li>
			  <li class="li22"><span class="s2">验证后，可用于快速找回登录密码，接收账户余额变动提醒。</span><a href="<%=basePath %>security_goUpdateEmail">[&nbsp修改&nbsp]</a></li>
			</ul>
		</c:if>
		<c:if test="${ctUser.UMb != null }">
			<ul class="ul2">
			  <li class="li21"><img src="<%=imgurl %>/bg/aqshzh1.gif"/>手机验证</li>
			  <li class="li22"><span class="s2">验证后，可用于快速找回登录密码及支付密码，接收账户余额变动提醒。</span><a href="<%=basePath %>security_goEditPhone">[&nbsp修改&nbsp]</a></li>
			</ul>
		</c:if>
		<c:if test="${ctUser.UMb == null }">
			<ul class="ul2">
			  <li class="li21"><img src="<%=imgurl %>/bg/aqshzh2.gif"/>手机验证</li>
			  <li class="li22"><span class="s2">验证后，可用于快速找回登录密码及支付密码，接收账户余额变动提醒。</span><a href="<%=basePath %>security_goBindPhone">[&nbsp立即验证&nbsp]</a></li>
			</ul>
		</c:if>
	  </div>
	</div>

<div class="clear"></div>
</div>
<jsp:include page="../common/foot.jsp"></jsp:include>	
</body>
</html>

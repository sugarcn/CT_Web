<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="../common/basePath.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<link href="css/css.css" type="text/css" rel="stylesheet"/>
</head>

<body>

<div class="top_a">
	<dl class="ant_all top_ant">	
	<dt><a href="<%=path %>">首页</a></dt>
	<dd><a href="<%=basePath%>list_help_desc?help.HId=23">联系我们</a><img src="<%=imgurl %>/bg/tel_ct.png"></dd>
	</dl>
<div class="clear"></div>
</div>

<div class="logo_a">
  <div class="ant_all logo_ant">	
	<div class="logo_left"><a href="<%=basePath%>"><img src="<%=imgurl %>/bg/logo_a.gif"></a></div>
	<div class="zhuce_mid"><span>邮箱验证</span><p>邮箱是您唯一的身份验证，请及时进行验证！</p></div>
	</div>
<div class="clear"></div>
</div>

<div class="contant_a">

    <div class="chooes">
        <div class="xiangmu_tiao zhengxingzi">
          <ul>
           <li class="fan_cm"><a href="/" target="_blank">让客户经理联系我</a></li>
           <li class="fan_ctl">邮箱验证</li>
           </ul>
        </div>
        <div class="pir_right">

<dl class="yanzyx">
<dt>我们向您 625076943@qq.com 发送了验证邮件，请前往验证。</dt>
<dd>邮箱是您唯一的身份验证，请及时进行验证！</dd>
<dd><p><a href="/" target="_blank">去邮箱验证</a></p></dd>
<dd>没收到？<a href="/" target="_blank">再次发送验证邮件</a>　　<a href="/" target="_blank">以后再说</a></dd>
</dl>

        </div>
    </div>
    
<div class="clear"></div>
</div>

<div class="baozhang">
  <ul class="ant_all baozhang_ant">	
<li class="ct_bz_1">种类齐全 专业配送</li>
<li class="ct_bz_2">线下支付 安全服务</li>
<li class="ct_bz_3">乐享正品 品质保证</li>
	</ul>
<div class="clear"></div>
</div>

<div class="footer">
  <div class="ant_all">	
<div class="fuwu_list">
<dl class="fuwe_5">
	<dt>购物指南</dt>
	<dd><a href="/" target="_blank">购物流程</a></dd>
	<dd><a href="/" target="_blank">会员等级</a></dd>
	<dd><a href="/" target="_blank">联系客服</a></dd>
</dl>
<dl class="fuwe_5">
	<dt>支付与配送</dt>
	<dd><a href="/" target="_blank">线下支付</a></dd>
	<dd><a href="/" target="_blank">物流查询</a></dd>
</dl>
<dl class="fuwe_5">
	<dt>特色服务</dt>
	<dd><a href="/" target="_blank">订制服务</a></dd>
	<dd><a href="/" target="_blank">BOM配单</a></dd>
</dl>
<dl class="fuwe_5">
	<dt>售后服务</dt>
	<dd><a href="/" target="_blank">服务投诉</a></dd>
	<dd><a href="/" target="_blank">退换货流程</a></dd>
	<dd><a href="/" target="_blank">订单查询</a></dd>
</dl>
<dl class="fuwe_5">
	<dt>特别说明</dt>
	<dd><a href="/" target="_blank">服务条款</a></dd>
	<dd><a href="/" target="_blank">隐私保护</a></dd>
	<dd><a href="/" target="_blank">网站条款</a></dd>
	<dd><a href="/" target="_blank">网站声明</a></dd>
</dl>
</div>
<dl class="erweima_a">
	<dt><img src="<%=imgurl %>/bg/erweima.gif"></dt>
	<dd>扫描关注官方微信</dd>
</dl>
<div class="dizhi_a">
<div class="dianhua_a"><p>全国免费服务热线：<span>4008-620-062</span></p></div>
<div class="zdizhi_a"><p>地址 : 深圳市福田区上步工业区友谊路万源大厦419室</p><p>电话 : <span>0755-88358380</span></p></div>
<div class="xinxi_a"><p>版权归长亭电子有限公司所有   苏备京ICP备12011349号</p></div>
</div>

  </div>
<div class="clear"></div>
</div>

</body>
</html>

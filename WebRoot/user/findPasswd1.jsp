<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="../common/basePath.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<link href="css/css.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="js/findpwd.js"></script>
<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
<link rel="shortcut icon" href="<%=basePath %>favicon.ico">
<style type="text/css">
body,td,th {
	font-family: SimSun;
}
</style>
</head>

<body>

<div class="top_a">
	<dl class="ant_all top_ant">	
	<dt><a href="<%=basePath %>">首页</a></dt>
	<dd><a href="<%=path %>/list_help">联系我们</a><img src="<%=imgurl %>/bg/tel_ct.png"></dd>
	</dl>
<div class="clear"></div>
</div>

<div class="logo_a">
  <div class="ant_all logo_ant">	
	<div class="logo_left"><a href="<%=basePath%>" target="_blank"><img src="<%=imgurl %>/bg/logo_a.gif"></a></div>
	<div class="zhuce_mid"><span>找回密码</span></div>
	</div>
<div class="clear"></div>
</div>


<div class="contant_a">
    <ul class="zhmm">
	  <li class="zhmmzt">1.输入账号</li>
	  <li>2.选择找回方式</li>
	  <li>3.进行安全验证</li>
	  <li>4.设置新密码</li>
	</ul>
	<div class="pir_right">
	<div id="con_xmlistb_1" class="con_all">
	<s:form action="login_findpwd1" id="form" method="post">
	<ul class="shoujizhuce">
          <li class="shouji"><span class="hao_name">您的账号：</span><input name="ctUser.UUserid" id="UUserid" type="text" value="用户名/手机" onfocus="if (value =='用户名/手机'){value =''}" onblur="if (value ==''){value='用户名/手机'}" />
          <span  style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="usernametip"></span>
          </li>
          <li class="yanzheng"><span class="hao_name">验 证 码：</span><input id="yzm" name="yzm" type="text" /><img id="realyzm"
					 		src="<%=basePath%>common/image.jsp"/><a
								href="javascript:showYzm()">换一张</a>
			<span  style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="yzmtip"></span>					
		  </li>
          <li class="zhucean"><p><a onclick="javascript:findpwd1()">下一步</a></p></li> 
    </ul>
    </s:form>
	</div>
	</div>		
<div class="clear"></div>
</div>



<jsp:include page="../common/foot.jsp"></jsp:include>
</body>
</html>

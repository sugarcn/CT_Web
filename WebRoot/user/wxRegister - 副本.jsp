﻿<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="../common/basePath.jsp"%>


<html>
<head>
	<meta charset="utf-8">
	<title>长亭易购</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/wxstyle.css">

	<script src="js/jquery.mobile-1.4.2.min.js"></script>
	<script src="js/jquery.validate.js"></script>
	<script src="js/register.js"></script>



<script>
  $.validator.setDefaults({ 
     submitHandler: function(signupForm) {
		 if (checkPhone()){
			 signupForm.submit();
		 }
      
     } 
  });
	var yanzhengOk = true;
	var wait=60; 
function time(o) { 
	if (wait == 0) {
		o.removeAttribute("disabled");	
		//("#spanid").html("csjwang");
		$("#fsyzm").html("<a href='#'>发送手机验证码</a>"); 
		$("#fsyzm").attr("onclick", "getPhoneYzm(this)"); 
		//o.value="　  获取验证码"; 
		wait = 60; 
	} else { 
		//o.setAttribute("disabled", true);
		$("#fsyzm").attr("onclick", ""); 
		$("#fsyzm").html("重新发送" + wait); 
		//("#fsyzm").innerHTML="重新发送(" + wait + ")"; 
		//o.value="　  重新发送(" + wait + ")";
		wait--; 
		setTimeout(function() { 
		time(o);
	}, 
	1000) 
	} 
} 
var huo = 300;
function timeOutYzm(o) { 
	if (huo == 0) { 
		huo = 300; 
		guoqi();
		checkUMbYzmIsEqual2("验证码超时，请重新输入");
		$("#sms").val("");
	} else { 
		huo--; 
		setTimeout(function() { 
		timeOutYzm(o);
	}, 
	1000) 
	} 
}
	function getPhoneYzm(o){
		if(yanzhengOk){
			var phoneNum = $("#UMb").val();
			if(phoneNum=="" || phoneNum == "请输入手机号码"){
				$("#UMb").val("请输入手机号码");
				isGetYzm = false;
				
			}else{
				$.post("login_savePhoneYzm",{"ctSms.sms":"1111","ctUser.UMb":phoneNum});
				isGetYzm = true;
				//$("#sms").removeAttr("disabled");
				time(o);
				timeOutYzm(o);
				huo = 300;
			}
			
			 
			isGuo = true;
		} else {
		//	$("#yzmtip1").text("请输入验证码");
		}
	}

	//验证手机号码是否合法
	function checkPhone(){
		flag = true;
		var phoneNum = $("#UMb").val();
		if(!(/^1[3|5|7|8|9][0-9]\d{8}$/.test(phoneNum))){
			$("#umbtip").text("手机号不合法");
			$("#fsyzm").attr("onclick", ""); 
			flag = false;
		}else{
			$("#umbtip").text("");
			$.ajaxSetup({async:false});
			$.post("login_checkPhone",{"ctUser.UMb":phoneNum},function(data){
				if(data=="success"){
					$("#umbtip").text("");
				}else {
					$("#umbtip").text("手机号已注册");
					$("#fsyzm").attr("onclick", ""); 
					flag = false;
//					$("#umb").val("");
				}
			});
		}
		return flag;
	}	//验证手机号码,获取手机验证码并保存验证
	
		//检测手机验证码是否正确
	function checkUmbYzmIsEqual(){

		var yzm = $("#yzm").val();
		if(yzm.length > 0){
			 checkUmbYzmIsEqual2("验证码错误");
		}
	}
	function checkUmbYzmIsEqual2(str) {
		flag = true;
		var phoneNum = $("#UMb").val();
		var yzm = $("#yzm").val();
		$("#smstip").text("");
		$.ajaxSetup({async:false});
		$.post("login_checkYzmIsEqual",{"ctSms.sms":yzm,"ctUser.UMb":phoneNum},function(data){
			if(data=="success"){
				//$("#wxsub").attr("disabled","false");
				//$("#wxsub").removeattr("disabled");
				$("#wxsub").prop("disabled",false);
			}else {
				$("#smstip").text(str);
				$("#wxsub").attr("disabled","disabled");
				flag = false;
//				$("#sms").val("");
			}
		});
		return flag;
	}
</script>	

</head>
<body>
<div id="page1" data-role="page">
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="logo span4">
                        <h1><img src="<%=imgurl %>/ctegosjzc.png"></h1>
                    </div>
                    
                </div>
            </div>
        </div>


        <div class="register-container container" id="container">
            <div class="row">
                
                <div class="register span6">
                    <form id="signupForm" method="post" action="login_register_wxsss">
                        <h2><span class="red"><strong>注册</strong></span></h2>
                        
<dl class="yhzc">
    <dd><input type="text" id="UMb" name="UMb" placeholder="请输入手机号码"  onBlur="return checkPhone();"><span  style="padding-left:18px; text-align:left; " id="umbtip"></span></dd>
                    
    <dd><span class="yhzlleibie"><input type="text" id="yzm" name="yzm" placeholder="手机验证码" onBlur="return checkUmbYzmIsEqual();"></span><span id="wide"><span id="fsyzm" class="yhzlleibie" onclick="getPhoneYzm(this)"><a href="#">发送手机验证码</a></span></span><span  style="padding-left:18px; text-align:left;" id="smstip"></span></dd>                      

    <dd><input type="password" id="UPassword" name="UPassword" placeholder="请输入您的密码"></dd>
    
    <dd><input type="password" id="confirm_UPassword" name="confirm_UPassword" placeholder="再次确认您的密码"></dd>

	<input type="hidden" id="USex" name="USex" value="2">
	<input type="hidden" id="UWeixinloginUserid" name="UWeixinloginUserid" value="o7ihfs4Xl54ZfFTFgbfYm7d8nkvk">
	<input type="hidden" id="UWeixingztime" name="UWeixingztime" value="1449052344">
	<input type="hidden" id="UUsername" name="UUsername" value="燕飞">
	<input type="hidden" id="subscribe" name="subscribe" value="1">
                     
    <dd><button id="wxsub" type="submit" disabled="disabled">注册</button>

	</dd>
</dl> 
<div class="clear">${sex}/////${openid}/////${subscribe_time}/////${nickname}//////${subscribe}</div>
                    </form>
                </div>
            </div>
        </div>
<div class="footer">长亭易购 版权所有<br/>©2014-2015 CTEGO CORPORATION All Rights Reserved.</div>
        <!-- Javascript -->
		
		<script>
$( "#page1" ).on( "pageinit", function() {
	$( "form" ).validate({
		rules: {
			UMb: {
				required: true,
				minlength: 11
			},
			yzm: {
				required: true,
			},
			UPassword: {
				required: true,
				minlength: 5
			},
			confirm_UPassword: {
				required: true,
				minlength: 5,
				equalTo: "#UPassword"
			}
		},
		
		messages: {
				yzm: "请输入验证码",
				UMb: {
					required: "请输入手机号码",
					minlength: "手机号码必须是11位"
				},
				yzm: {
					required: "请输入验证码",
				},
				UPassword: {
					required: "请输入密码",
					minlength: "密码必须大于5位"
				},
				confirm_UPassword: {
					required: "请再次输入密码",
					minlength: "密码必须大于5位",
					equalTo: "请输入相同的密码"
				}
			}
	});
});
</script>




</body>
</html>

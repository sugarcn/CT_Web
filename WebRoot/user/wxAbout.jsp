﻿<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="../common/basePath.jsp"%>


<html>
<head>
	<meta charset="utf-8">
	<title>长亭易购</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no" />
<link rel="stylesheet" href="css/wxstyle.css">

</head>
<body>

<div id="page1" data-role="page">
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="logo span4">
                        <h1><img src="<%=imgurl %>/ctegosjzc.png"></h1>
                    </div>
                    
                </div>
            </div>
        </div>
		
		
		
        <div class="register-container container" id="container">
            <div class="row">
                
                <div class="register span6">
		
		

<form class="cmxform" id="commentForm" method="post" action="login_register_wx">  

<h2><span class="red"><strong>公司介绍</strong></span></h2>


		  
<div class="content">
		  
<p>长亭易购(<a href="http://www.ctego.com/" target="_blank">ctego.com</a>)是国内领先的电子元器件B2B在线交易平台，为中小电子企业、电子贸易商、科研单位及创客等提供一站式电子元器件解决方案。</p>
<p>深圳前海长亭易购电子商务有限公司由深圳市长亭电子有限公司投资成立，于2015年7月26日正式上线运营。</p>
<p>长亭易购从成立伊始就是以技术为驱动，上线前就开始投入巨资开发和完善自有系统平台。以技术驱动提升平台运作效率，以为用户提供超出预期的服务为目标，重新定义电子元器件行业服务标准！</p>
<p>长亭易购依托长亭电子强大的电子元器件供应链能力，整合优化各电子元器件品牌供应资源，为用户提供丰富的品类和卓越的服务。</p>
<div  class="font-bold">
<p>我们只卖原装正品，欢迎监督；</p>
<p>我们提供拆零服务，杜绝浪费；</p>
<p>我们不断优化成本，为您而省；</p>
<p>我们提升出货速度，急您所急；</p>
<p>我们承诺所有商品提供原厂售后保障服务；</p>
<p>我们线下体验店提供全品类O2O售前售后全方位咨询和服务；</p>
</div>
<p>长亭易购全体员工秉承“ 年轻就是力量，有梦就有未来！”的信条，不畏艰辛，勇于创新，乐于创造！我们年轻的团队，敢想，敢拼，无所畏惧！我们不能改写历史，却可以创造未来！</p>
</div>
<div class="clear"></div>

</form>  
        </div>

<div class="footer">长亭易购 版权所有<br/>©2014-2016 CTEGO CORPORATION All Rights Reserved.</div>

</div></div>
</body>  
</html> 

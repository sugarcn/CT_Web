 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 <%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../../common/basePath.jsp"%>
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>

<jsp:include page="../../common/head.jsp"></jsp:include>
<div class="contant_e">
    <div class="dizhilian_e"><a href="<%=basePath %>">首页</a>><a href="<%=request.getContextPath() %>/user_userinfo">个人中心</a>><span>我的级别</span></div>
<jsp:include page="../../common/left.jsp"></jsp:include>    
    
	<div class="right_ef">
       
<dl>
<dt>我的级别： <span>${userRank.RName }用户 <i>(${ctUser.UCredit }积分)</i></span></dt>
<c:if test="${userRank.RName == '注册会员' }">
<dd><span class="hover">注册会员</span></dd>
<dd><span>V1优先会员</span><img src="<%=imgurl %>/bg/vip1.png"></dd>
<dd><span>V2银牌会员</span><img src="<%=imgurl %>/bg/vip2.png"></dd>
<dd><span>V3金牌会员</span><img src="<%=imgurl %>/bg/vip3.png"></dd>
</c:if>


</dl>

<div class="djshuoming">
<h2>等级说明：</h2>
<ul>
<li>用户分级分为4个档次,按照积分进行分类,每100元得1个积分：</li>
<li>${userRanks[0].RName}用户-不足${userRanks[1].minPoints}积分</li>
<li>${userRanks[1].RName}用户-满${userRanks[1].minPoints}个积分</li>
<li>${userRanks[2].RName}用户-满${userRanks[2].minPoints}个积分</li>
<li>${userRanks[3].RName}用户-满${userRanks[3].minPoints}个积分</li>
<li class="chak"><a href="javascript:rankDetail();">查看详情>></a></li>
</ul>
</div>
	
	</div>

<div class="clear"></div>
</div>
<jsp:include page="../../common/foot.jsp"></jsp:include>    
</body>
</html>

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../common/basePath.jsp"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<link href="css/css.css" type="text/css" rel="stylesheet"/>
<link href="css/css2.css" type="text/css" rel="stylesheet"/>
<link href="css/css3.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="js/jqueryt.js"></script>
<script type="text/javascript" src="js/lrtk.js"></script>
<link type="text/css" rel="stylesheet" href="css/style.css" />

<script src="js/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>
</head>

<body>

<jsp:include page="../common/head.jsp"></jsp:include>

<div class="contant_e">

    <div class="dizhilian_e"><a href="<%=basePath%>">首页</a>><span>个人中心</span></div>
    
	<jsp:include page="../common/left.jsp"></jsp:include>
	<div class="geren">

<div class="gr_jiben">
	<div class="ge_tx"><img src="<%=imgurl %>/touxiang.gif"></div>
	<div class="ge_xx">
        <div class="ge_dj"><span class="de_dj_sh"><i>上次登录时间：${ctUser.ULastTime }</i></span><span class="de_dj_ti">普通用户</span><span class="de_dj_xs">${ctUser.UUsername}，您好！</span></div>
        <ul>
            <li class="ge_t">
                <dl>
                <c:if test="${ctUser.UEmail != null }">
                <dd><span>用户邮箱：</span><div><i>${ctUser.UEmail}</i><a href="<%=basePath %>security_goUpdateEmail">[修改]</a></div></dd>
                </c:if>
                <c:if test="${ctUser.UEmail == null }">
                <dd><span>用户邮箱：</span><div><i><a href="<%=basePath %>security_goConfirmIdentity">[ 立即验证 ]</a></i></div></dd>
                </c:if>
                <c:if test="${ctUser.UMb != null }">
                <dd><span>手机号：</span><div><i>${jiamiUmb }</i><a href="<%=basePath %>security_goEditPhone">[修改]</a></div></dd>
                </c:if>
                <c:if test="${ctUser.UMb == null }">
                <dd><span>手机号：</span><div><i><a href="<%=basePath %>security_goBindPhone">[ 立即验证 ]</a></i></div></dd>
                </c:if>
                </dl>
            </li>
            <li class="ge_t ge_two">
                <dl>
                <dd><span>积分：</span><div><i class="geshu"><c:if test="${ctUser.UCredit == null }">0</c:if><c:if test="${ctUser.UCredit != null }">${ctUser.UCredit }</c:if></i>分</div></dd>
                <dd><span>优惠券：</span><div><i class="geshu">[ ${cout_count} ]</i>个</div></dd>
                <dd><span>信用余额：</span><div><i class="geshu">&yen;${ctUser.URemainingAmount }</i></div></dd>
                </dl>
            </li>

            <li class="ge_b">
                <dl>
                <dd><span>待付款：</span><div><i class="geshu">[ ${dfcount } ]</i>个</div></dd>
                <dd><span>待审核：</span><div><i class="geshu">[ ${dscount } ]</i>个</div></dd>
                <dd><span>待发货：</span><div><i class="geshu">[ ${dfhcount } ]</i>个</div></dd>
                <dd><span>待收货：</span><div><i class="geshu">[ ${dshcount } ]</i>个</div></dd>
                <dd><span>待评价：</span><div><i class="geshu">[ ${dpj } ]</i>个</div></dd>
                </dl>
            </li>
        </ul>
        
    </div>
</div>
    
<div class="gr_ding">
    <div class="gr_tit"><a href="<%=basePath %>order_list" >查看更多订单</a><h3>最近的订单</h3></div>
    <s:iterator value="orderList" var="olist" status="text">
    <c:forEach items="${listGodsTest }" var="oglist" varStatus="status">
	                	<c:if test="${oglist.orderId == olist.orderId }">
	                	<input type="hidden" name="orderIds" value="${oglist.orderId }" />
	                	<input type="hidden" name="orders" value="${oglist.orderId }" />
	                	<c:if test="${oglist.isParOrSim == '1'}">
	                	<input type="hidden" name="${oglist.orderId }simForPriceDan" value="${oglist.GSubtotal }" />
	                	<input type="hidden" name="${oglist.orderId }simForNum" value="${oglist.GNumber}" />
	                	</c:if>
	                	<c:if test="${oglist.isParOrSim == '0'}">
	                	<input type="hidden" name="${oglist.orderId }parForPriceDan" value="${oglist.GSubtotal }" />
	                	<input type="hidden" name="${oglist.orderId }parForNum" value="${oglist.GNumber}" />
	                	</c:if>
	                	</c:if>
	       </c:forEach>
    
    	<c:if test="${olist.isParOrSim == '1-1' }">
	    <dl class="gr_nei">
	    <dt><a href="<%=basePath%>order_details?orderDTO.orderSn=${olist.orderSn}">查看订单>></a><span>订单编号：<i>${olist.orderSn }</i></span>
	    <span>共有<i>[ <c:forEach items="${orderGoodsCount }" var="list">
	    				<c:if test="${list.key == olist.orderId }">
	    					${list.value }
	    				</c:if>
	    			</c:forEach> ]</i>件商品</span></dt>
	    <dd class="gr_n_tit">
	    	<ul>
        <li><div class="gr_n_a"><span class="gr_n_y"></span>样片商品</div><div class="gr_n_b">
       
        <c:forEach items="${goodsListAll }" var="list">
	                <c:forEach items="${list }" var="oglist" varStatus="status">
	                	<c:if test="${oglist.orderId == olist.orderId }">
		                	<c:if test="${oglist.isParOrSim == '1' }">
					    		<a href="goods_detail?gid=${oglist.ctGoods.GId }" target="_blank">${oglist.ctGoods.GName }</a><span class="ct_line_t"></span>	
		                	</c:if>
	                	</c:if>
	            	</c:forEach>
            	</c:forEach>
                
               <!-- <c:forEach items="${goodsListAll }" var="list">
	                <c:forEach items="${list }" var="oglist" varStatus="status">
	                	<c:if test="${oglist.orderId == olist.orderId }">
		            		<c:if test="${oglist.isParOrSim == '1' }">
			            		<c:if test="${status.index > 3 }">
                <div class="111" name="box" style="display:none">
					                <a href="goods_detail?gid=${oglist.ctGoods.GId }" target="_blank">${oglist.ctGoods.GName }</a><span class="ct_line_t"></span>						               
                </div>
			            		</c:if>
		                	</c:if>
	                	</c:if>
	            	</c:forEach>
            	</c:forEach>-->
        </div> <div class="gr_n_c"> <div class="jine" name="${olist.orderId }simZongPrice">
            </div></div></li>
        <li><div class="gr_n_a"><span class="gr_n_p"></span>批量商品</div><div class="gr_n_b">
            <!--  && sss.index+1 == fn:length(goodsListAll) -->
            	<c:forEach items="${goodsListAll }" var="list">
	                <c:forEach items="${list }" var="oglist" varStatus="status">
	                	<c:if test="${oglist.orderId == olist.orderId }">
		            		<c:if test="${oglist.isParOrSim == '0' }">
					                <a href="goods_detail?gid=${oglist.ctGoods.GId }" target="_blank">${oglist.ctGoods.GName }</a><span class="ct_line_t"></span>
		                	</c:if>
	                	</c:if>
	            	</c:forEach>
            	</c:forEach>
            	<!--  <c:forEach items="${goodsListAll }" var="list">
	                <c:forEach items="${list }" var="oglist" varStatus="status">
	                	<c:if test="${oglist.orderId == olist.orderId }">
		            		<c:if test="${oglist.isParOrSim == '0' }">
			            		<c:if test="${status.index > 3 }">
                <div class="" name="box2" style="display:none">
		<a href="goods_detail?gid=${oglist.ctGoods.GId }" target="_blank">${oglist.ctGoods.GName }</a><span class="ct_line_t"></span>			             			             
			            		</div>
			            		</c:if>
		                	</c:if>
	                	</c:if>
	            	</c:forEach>
            	</c:forEach>-->
        </div><div class="gr_n_c"><div class="jine" name="${olist.orderId }parZongPrice">
            </div></div></li>
            </ul>
    </dd>
    <dd>总计：<i>￥${olist.total }</i></dd>
    <dd>订单状态：
    	 <s:if test="#olist.orderStatus==0"><i>【待付款】</i></s:if>
		 <s:if test="#olist.orderStatus==1"><i>【已付款】</i></s:if>
		 <s:if test="#olist.orderStatus==2"><i>【待审核】</i></s:if>
		 <s:if test="#olist.orderStatus==3"><i>【配货中】</i></s:if>
		 <s:if test="#olist.orderStatus==4"><i>【已发货】</i></s:if>
		 <s:if test="#olist.orderStatus==5"><i>【已完成】</i></s:if>
		 <s:if test="#olist.orderStatus==6"><i>【取消】</i></s:if>
		 <s:if test="#olist.orderStatus==7"><i>【无效】</i></s:if>
	</dd>
    </dl>
        </c:if>
        <c:if test="${olist.isParOrSim == '0-1' }">
        <dl class="gr_nei">
	    <dt><a href="<%=basePath%>order_details?orderDTO.orderSn=${olist.orderSn}">查看订单>></a><span>订单编号：<i>${olist.orderSn }</i></span>
	    <span>共有<i>[ <c:forEach items="${orderGoodsCount }" var="list">
	    				<c:if test="${list.key == olist.orderId }">
	    					${list.value }
	    				</c:if>
	    			</c:forEach> ]</i>件商品</span></dt>
	    <dd style="height: 31px;" class="gr_n_tit">
	    	<ul>
            <li><div class="gr_n_a"><span class="gr_n_p"></span>批量商品</div><div class="gr_n_b">                
            	<c:forEach items="${goodsListAll }" var="list">
	                <c:forEach items="${list }" var="oglist" varStatus="status">
	                	<c:if test="${oglist.orderId == olist.orderId }">
		            		<c:if test="${oglist.isParOrSim == '0' }">
					             <a href="goods_detail?gid=${oglist.ctGoods.GId }" target="_blank">${oglist.ctGoods.GName }</a><span class="ct_line_t"></span>	
		                	</c:if>
	                	</c:if>
	            	</c:forEach>
            	</c:forEach>
            	<!-- <c:forEach items="${goodsListAll }" var="list">
	                <c:forEach items="${list }" var="oglist" varStatus="status">
	                	<c:if test="${oglist.orderId == olist.orderId }">
		            		<c:if test="${oglist.isParOrSim == '0' }">
			            		<c:if test="${status.index > 3 }">
                <div class="" name="box2" style="display:none">
					             <a href="goods_detail?gid=${oglist.ctGoods.GId }" target="_blank">${oglist.ctGoods.GName }</a><span class="ct_line_t"></span>
			            		</div>
			            		</c:if>
		                	</c:if>
	                	</c:if>
	            	</c:forEach>
            	</c:forEach> -->
            	</div><div class="gr_n_c"><div class="jine" name="${olist.orderId }parZongPrice">
            </div></div></li>
            </ul>
    </dd>
    <dd style="height: 30px;">总计：<i>￥${olist.total }</i></dd>
    <dd style="height: 30px;">订单状态：
    	 <s:if test="#olist.orderStatus==0"><i>【待付款】</i></s:if>
		 <s:if test="#olist.orderStatus==1"><i>【已付款】</i></s:if>
		 <s:if test="#olist.orderStatus==2"><i>【待审核】</i></s:if>
		 <s:if test="#olist.orderStatus==3"><i>【配货中】</i></s:if>
		 <s:if test="#olist.orderStatus==4"><i>【已发货】</i></s:if>
		 <s:if test="#olist.orderStatus==5"><i>【已完成】</i></s:if>
		 <s:if test="#olist.orderStatus==6"><i>【取消】</i></s:if>
		 <s:if test="#olist.orderStatus==7"><i>【无效】</i></s:if>
	</dd>
    </dl>
        </c:if>
        <c:if test="${olist.isParOrSim == '1-0' }">
        <dl class="gr_nei">
	    <dt><a href="<%=basePath%>order_details?orderDTO.orderSn=${olist.orderSn}">查看订单>></a><span>订单编号：<i>${olist.orderSn }</i></span>
	    <span>共有<i>[ <c:forEach items="${orderGoodsCount }" var="list">
	    				<c:if test="${list.key == olist.orderId }">
	    					${list.value }
	    				</c:if>
	    			</c:forEach> ]</i>件商品</span></dt>
	    <dd style="height: 31px;" class="gr_n_tit">
	    	<ul>
        <li><div class="gr_n_a"><span class="gr_n_y"></span>样片商品</div><div class="gr_n_b">
        <c:forEach items="${goodsListAll }" var="list">
	                <c:forEach items="${list }" var="oglist" varStatus="status">
	                	<c:if test="${oglist.orderId == olist.orderId }">
		                	<c:if test="${oglist.isParOrSim == '1' }">
					    <a href="goods_detail?gid=${oglist.ctGoods.GId }" target="_blank">${oglist.ctGoods.GName }</a><span class="ct_line_t"></span>
		                	</c:if>
	                	</c:if>
	            	</c:forEach>
            	</c:forEach>
                <!-- <c:forEach items="${goodsListAll }" var="list">
	                <c:forEach items="${list }" var="oglist" varStatus="status">
	                	<c:if test="${oglist.orderId == olist.orderId }">
		            		<c:if test="${oglist.isParOrSim == '1' }">
			            		<c:if test="${status.index > 3 }">
                <div class="111" name="box" style="display:none">
					                <a href="goods_detail?gid=${oglist.ctGoods.GId }" target="_blank">${oglist.ctGoods.GName }</a><span class="ct_line_t"></span>						               
                </div>
			            		</c:if>
		                	</c:if>
	                	</c:if>
	            	</c:forEach>
            	</c:forEach> -->
        </div><div class="gr_n_c"> <div class="jine" name="${olist.orderId }simZongPrice">
            </div></div></li>
            </ul>
    </dd>
    <dd style="height: 30px;">总计：<i>￥${olist.total }</i></dd>
    <dd style="height: 30px;">订单状态：
    	 <s:if test="#olist.orderStatus==0"><i>【待付款】</i></s:if>
		 <s:if test="#olist.orderStatus==1"><i>【已付款】</i></s:if>
		 <s:if test="#olist.orderStatus==2"><i>【待审核】</i></s:if>
		 <s:if test="#olist.orderStatus==3"><i>【配货中】</i></s:if>
		 <s:if test="#olist.orderStatus==4"><i>【已发货】</i></s:if>
		 <s:if test="#olist.orderStatus==5"><i>【已完成】</i></s:if>
		 <s:if test="#olist.orderStatus==6"><i>【取消】</i></s:if>
		 <s:if test="#olist.orderStatus==7"><i>【无效】</i></s:if>
	</dd>
    </dl>
        </c:if>
        
    <input type="hidden" id="orderId" value="${olist.orderSn }" />
    </s:iterator>
</div>
${pages.pageGoodsStr }
<script type="text/javascript">
/*
function findOrderInfo(orderSn){
		location.href="${pageContext.request.contextPath}/order_details?orderDTO.orderSn="+orderSn;
	}
	*/
function getThreeOrder(){
		   	 var orderSN = $("input[id=setOrderSnTemp]:gt(2)");
		   	 for(var i = 0; i < orderSN.length; i++){
		   		 var orderSn = orderSN[i].value;
		   		switchPoint.innerText=3;
		   		 switchSysBar(orderSn);
		   	 }
	   	}
	   	getThreeOrder();
	   	function getParPrice(){
	   		var orderIds = $("input[name=orderIds]");
		   		var orderIdTemp = 0;
		   		var orderIdsTwo = [];
		   		for(var i = 0,len = orderIds.length;i < len;i++){
		   			! RegExp(orderIds[i].value,"g").test(orderIdsTwo.join(",")) && (orderIdsTwo.push(orderIds[i].value));
		   			}
		   		
	   		for(var j = 0; j< orderIdsTwo.length; j++){
		   		var priceAll = 0;
		   		var priceAllsim = 0;
		   		var prices = document.getElementsByName(orderIdsTwo[j]+"parForPriceDan");
		   		var pricesSim = document.getElementsByName(orderIdsTwo[j]+"simForPriceDan");
		   		for(var i = 0; i < prices.length; i++){
		   			priceAll += Number(prices[i].value);
		   		}
		   		for(var i = 0; i < pricesSim.length; i++){
		   			priceAllsim += Number(pricesSim[i].value);
		   		}
		   		$("div[name="+orderIdsTwo[j]+"parZongPrice]").html("￥" + priceAll);
		   		$("div[name="+orderIdsTwo[j]+"simZongPrice]").html("￥" + priceAllsim);
	   		}
	   	}
	   	getParPrice();
	   	function getYpNum(){
	   	
	   	}
	  </script>

<div class="gr_ding">
    <div class="gr_tit"><a href="<%=basePath%>bom_list" >查看更多BOM</a><h3>我的BOM</h3></div>

    <dl class="gp_nei">
    <dt><span>BOM名称</span><span>BOM数量</span></dt>
    <c:forEach items="${bomList}" var="blist">
    <dd><span class="gp_sl"><a href="<%=basePath%>bom_goBomDetail?bomDTO.bomId=${blist.bomId }">${blist.bomTitle}</a></span><span><div><i class="geshu">[ ${blist.num} ]</i>个</div></span></dd>
    </c:forEach>
    </dl>
</div>
	
	</div>
<div class="clear"></div>
</div>
<script type="text/javascript">
	/* 分页跳转控制器 */
	function upAndDownPage(sum){
		//截取访问名
		var url = document.URL.toString();
		var i = url.lastIndexOf("/");
		var s = url.lastIndexOf("?");
		if(s == -1){
			
		} else {
			var chai = url.split("?");
			i = chai[0].lastIndexOf("/");
			url = url.substring(i+1, s);
		}
		var strUrl = url+"?page=" + sum;
		strUrl = encodeURI(strUrl);
		window.location.href= strUrl;
	}
	</script>
<jsp:include page="../common/foot.jsp"></jsp:include>

</body>
</html>

﻿<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="../../common/basePath.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<link href="css/css.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="js/user.js" charset="utf-8"></script>
</head>
<body>
<input type="hidden" id="uid" value="${uid }"/>
<input type="hidden" id="email" value="${ctUser.UEmail }"/>
<div class="top_a">
	<dl class="ant_all top_ant">	
	<dt><a href="<%=basePath%>" >首页</a></dt>
	<dd><a href="<%=basePath%>list_help_desc?help.HId=23" >联系我们</a><img src="<%=imgurl %>/bg/tel_ct.png"></dd>
	</dl>
<div class="clear"></div>
</div>

<div class="logo_a">
  <div class="ant_all logo_ant">	
	<div class="logo_left"><a href="<%=basePath%>"><img src="<%=imgurl %>/bg/logo_a.gif"></a></div>
	</div>
<div class="clear"></div>
</div>

<div class="contant_a">

    <div class="chooes">
        <div class="xiangmu_tiao zhengxingzi">
          <ul>
           <li class="fan_cm"><a href="/" target="_blank">让客户经理联系我</a></li>
           <li class="fan_ctl">邮箱验证</li>
           </ul>
        </div>
        <div class="pir_right">

<dl class="yanzyx">
<dt>我们向您 ${ctUser.UEmail } 发送了验证邮件，请前往修改新的邮箱。</dt>
<dd><p><a href="javascript:active();">去邮箱修改新的邮箱</a></p></dd>
</dl>
        </div>
    </div>
    
<div class="clear"></div>
</div>

<jsp:include page="../../common/foot.jsp"></jsp:include>

</body>
</html>

﻿<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="../../common/basePath.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<link href="css/css.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
        <script type="text/javascript" src="js/user.js" charset="utf-8"></script>
</head>

<body>
		<s:form action="security_saveUpdateMail" id="form" method="post">
			<input type="hidden" id="UMb2" name="ctUser.UMb" value="${ctUser.UMb}"/>
			<table>
				<tr>
					<td>我的邮箱:</td>
					<td><input type="text" id="email" name="ctUser.UEmail" onblur="checkEmail();"/></td>
					<td><span  style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="emailtip"></span>
					</td>
				</tr>
				<tr>
				<td>验证码:</td>
				<td>
					<input type="text" id="yzm" name="yzm" onblur="checkYzm();"/><img id="realyzm"
					 		src="<%=basePath%>common/image.jsp"/><a
								href="javascript:showYzm()">换一张</a>
								<span  style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="yzmtip"></span>
				</td>
				</tr>
				<tr>
					<td><input type="button" value="修改绑定邮箱" onclick="saveUpdateMail()"/></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>
			
		</s:form>
</body>
</html>

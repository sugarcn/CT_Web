<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../common/basePath.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<link href="css/css.css" type="text/css" rel="stylesheet"/>
<link rel="shortcut icon" href="<%=basePath %>favicon.ico">
<script type="text/javascript" src="js/findpwd.js"></script>
<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
 <script type="text/javascript" src="<%=basePath %>js/login.js"></script>
 <script type="text/javascript" src="<%=basePath %>js/script.js"></script>
<script type="text/javascript">
$(function(){
		var UMb = $("#UMb").text();
		UMb = UMb.substring(0, 3) + "****" + UMb.substring(7, 11);
		$("#UMb").text(UMb);
		
		var UEmail = $("#UEmail").text();
		UEmail = UEmail.split("@");
		UEmail = UEmail[0].substring(0, 2) + "***"+UEmail[0].substring(UEmail[0].length-2, UEmail[0].length)+"@" + UEmail[1];
		$("#UEmail").text(UEmail);
	});
</script>
<style type="text/css">
body,td,th {
	font-family: SimSun;
}
</style>
</head>

<body>
<div id="popDivall" class="mydivall" style="display:none;">
<div class="con_t"><a href="javascript:closecharDiv()"><img src="<%=imgurl %>/bg/chadiao.gif"></a><span>让客户经理联系我</span></div>
<div class="con_b">
	<ul>
    <li><p>您的支持是我们前进的动力！<br/>
如有问题或建议，请留下您的联系方式和邮箱及问题详情，我们会尽快与您取得联系，谢谢支持。</p></li>
	<li id="ts" style="color:red"></li>
	<li><span>邮箱：</span><input name="con" type="text" id="email"/></li>
    <li><span>手机：</span><input name="con" type="text" id="mbo"/></li>
	<li><span>详细：</span><textarea name="con" id="detail"></textarea></li>
    <li><div class="msg-up" style="display: none;"></div></li>
    <li><a href="javascript:contacts()">提交</a></li>
    </ul>
</div>
</div>
<div id="bgallfk" class="bgallfk" style="display:none;"></div>
  
<div class="top_a">
	<dl class="ant_all top_ant">	
		<dt><a href="<%=basePath %>">首页</a></dt>
	<dd><a href="<%=path %>/list_help" >联系我们</a><img src="<%=imgurl %>/bg/tel_ct.png"></dd>
	</dl>
<div class="clear"></div>
</div>

<div class="logo_a">
  <div class="ant_all logo_ant">	
	<div class="logo_left"><img src="<%=imgurl %>/bg/logo_a.gif"></div>
	<div class="zhuce_mid"><span>找回密码</span></div>
	</div>
<div class="clear"></div>
</div>
<s:form action="login_gofindpwd3" id="form" method="post">
<div class="contant_a">   
	   <input type="hidden" value="${ctUser.UUserid}" name="ctUser.UUserid"/>
	   <div class="zhmmzh"><p>您正在找回密码的账号为<span>${ctUser.UUsername}</span>，<i><a href="javascript:goForgetPwd()">换一个账号</a></i></p></div>
	   <ul class="zhmm">
	     <li>1.输入账号</li>
	     <li class="zhmmzt">2.选择找回方式</li>
	     <li>3.进行安全验证</li>
	     <li>4.设置新密码</li>
	   </ul>
	  
	  <div class="zhmmfs">
	  	<c:if test="${ctUser.URestype != null && ctUser.URestype != 0 && ctUser.URestype != 1 }">
	  		 <p id="text">第三方登录账号无法在此平台修改密码</p>
	  	</c:if>
	  	<c:if test="${ctUser.URestype == null || ctUser.URestype == 0 || ctUser.URestype == 1 }">
	  		 <p id="text">请选择找回密码的方式：</p>
	  	</c:if>
	   
	    <c:if test="${ctUser.UMb != null }">
	    <p><input type="radio" name="choose" value="1" checked="checked"/><span class="s1">选择绑定手机：</span><span id="UMb" class="s1">${ctUser.UMb}</span></p>
     	</c:if>
     	<c:if test="${ctUser.UEmail != null }">
     	<p><input type="radio" name="choose" value="2" checked="checked"/><span class="s1">选择绑定邮箱：</span><span id="UEmail" class="s1">${ctUser.UEmail}</span></p>
     	</c:if>
      </div>
	  

	<div id="con_xmlistb_1" class="con_all">
	<ul class="xyb">
          <li class="xyb1"><p><a id="isOkNext"
          	<c:if test="${ctUser.URestype != null && ctUser.URestype != 0 && ctUser.URestype != 1 && ctUser.URestype != 4 }">
		  		 onclick="javascript:;"
		  	</c:if>
          	<c:if test="${ctUser.URestype == null || ctUser.URestype == 0 || ctUser.URestype == 1 || ctUser.URestype == 4 }">
		  		 onclick="javascript:gofindpwd3();"
		  	</c:if>
          >下一步</a></p></li> 
    </ul>
	</div>

	
	<div class="lxkf">
	  <p>无法通过上面的方法找回密码？</p>
	  <p>您可以进行<span class="lianjingli"><a href="javascript:showcharDiv()">让客户经理联系我</a></span>，客服在核实您的身份后可为您重置密码。</p>
	</div>
	  
<div class="clear"></div>
</div>
</s:form>

<div class="clear"></div>
</div>
<jsp:include page="../common/foot.jsp"></jsp:include>
</body>
</html>

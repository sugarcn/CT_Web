<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="../../common/basePath.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<link href="css/css.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
        <script type="text/javascript" src="js/user.js" charset="utf-8"></script>
<script type="text/javascript">
$(function(){ 

	$("#searchSelected").click(function(){ 
		$("#searchTab").show();
		$(this).addClass("searchOpen");
	}); 

	$("#searchTab li").hover(function(){
		$(this).addClass("selected");
	},function(){
		$(this).removeClass("selected");
	});
	 
	$("#searchTab li").click(function(){
		$("#searchSelected").html($(this).html());
		$("#searchTab").hide();
		$("#searchSelected").removeClass("searchOpen");
	});
	var UEmail = $("#UEmail").text();
		UEmail = UEmail.split("@");
		UEmail = UEmail[0].substring(0, 2) + "****@" + UEmail[1];
		$("#UEmail").text(UEmail);
});
</script>
</head>

<body>
<jsp:include page="../../common/head.jsp"></jsp:include>
<div class="contant_e">

    <div class="dizhilian_e"><a href="<%=basePath%>" >首页</a>><a href="<%=basePath%>user_userinfo">个人中心</a>><a href="<%=basePath%>security_goSecurity">安全设置</a>><span>验证邮箱</span></div>
    
<jsp:include page="../../common/left.jsp"></jsp:include>	
    
	<div class="right_m">
	  <div class="aqshzh">
	    <div>验证邮箱</div>
		
		<ul class="yanzhengyouxiang1">
			<li class="yzyxzt1">1.验证身份</li>
			<li class="yzyxzt2">2.验证邮箱</li>
			<li class="yzyxzt3">3.完成</li>
		</ul>
		<input type="hidden" id="email" value="${ctUser.UEmail }"/>
		
		<p class="yfsyzhyj"><img src="<%=imgurl %>/bg/xgdlmmztwc.gif"/><span >已发送验证邮件至：<span id="UEmail">${ctUser.UEmail }</span></span></p>
		<p class="tishi1">(请立即完成验证，邮箱验证不通过则修改邮箱失败)</p>
		<p class="tishi2">验证邮件24小时内有效，请尽快登录您的邮箱点击验证链接完成验证。</p>
		<p class="qyxck"><a href="javascript:active();">去邮箱查看</a></p> 

	  </div>
	</div>

<div class="clear"></div>
</div>
<jsp:include page="../../common/foot.jsp"></jsp:include>
</body>
</html>

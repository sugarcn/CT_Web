<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="../../common/basePath.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<link href="css/css.css" type="text/css" rel="stylesheet" />
<link href="css/css.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
        <script type="text/javascript" src="js/user.js" charset="utf-8"></script>
<script type="text/javascript">
$(function(){ 

	$("#searchSelected").click(function(){ 
		$("#searchTab").show();
		$(this).addClass("searchOpen");
	}); 

	$("#searchTab li").hover(function(){
		$(this).addClass("selected");
	},function(){
		$(this).removeClass("selected");
	});
	 
	$("#searchTab li").click(function(){
		$("#searchSelected").html($(this).html());
		$("#searchTab").hide();
		$("#searchSelected").removeClass("searchOpen");
	});
	var UMb = $("#UMb").text();
		UMb = UMb.substring(0, 3) + "****" + UMb.substring(7, 11);
		$("#UMb").text(UMb);
});
</script>
</head>

<body>
<jsp:include page="../../common/head.jsp"></jsp:include>
<div class="contant_e">

    <div class="dizhilian_e"><a href="<%=basePath%>" >首页</a>><a href="<%=basePath%>user_userinfo">个人中心</a>><a href="<%=basePath%>security_goSecurity">安全设置</a>><span>验证邮箱</span></div>
    
	<jsp:include page="../../common/left.jsp"></jsp:include>	
   	<div class="right_m">
	  <div class="aqshzh">
	    <div>验证邮箱</div>
		
		<ul class="xgdlmm">
			<li class="xgdlmmzt1">1.验证身份</li>
			<li class="xgdlmmzt2">2.验证邮箱</li>
			<li class="xgdlmmzt2">3.完成</li>
		</ul>
 <s:form action="security_confirmMail" id="form" method="post">
			<input type="hidden" id="UMb2" name="ctUser.UMb" value="${ctUser.UMb}"/>
		<ul class="xgdlmm2">
			<li class="dlmm"><span>请输入登录密码：</span><span><input name="ctUser.UPassword" id="UPassword" type="password"/></span>
			<span  style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="pwdtip"></span>
			</li>
			<li class="yzhm"><span>验证码：</span><input id="yzm" name="yzm" onblur="checkYzm();" type="text" /><img id="realyzm"
					 		src="<%=basePath%>common/image.jsp"/><a href="javascript:showYzm()">看不清，换一张</a>
			<span  style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="yzmtip"></span>		 		
		    </li>
			<li class="tijiao"><p><a href="javascript:tijiao();">提交</a></p></li> 
		</ul>
</s:form>		
	  </div>
	</div>

<div class="clear"></div>
</div>
<jsp:include page="../../common/foot.jsp"></jsp:include>	
</body>
</html>

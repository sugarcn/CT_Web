<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../common/basePath.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
    <script type="text/javascript" src="<%=basePath %>js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="<%=basePath %>js/style.css" />
		<script type="text/javascript" src="<%=basePath %>js/jquery.min.js"></script>
		<script type="text/javascript" src="<%=basePath %>js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<%=basePath %>js/styles.css" />
        <link rel="stylesheet" type="text/css" href="<%=basePath %>js/jquery.confirm.css" />
        <link rel="stylesheet" href="<%=basePath %>css/mycss.css" type="text/css"/>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

<script type="text/javascript">
$(function(){ 

	$("#searchSelected").click(function(){ 
		$("#searchTab").show();
		$(this).addClass("searchOpen");
	}); 

	$("#searchTab li").hover(function(){
		$(this).addClass("selected");
	},function(){
		$(this).removeClass("selected");
	});
	 
	$("#searchTab li").click(function(){
		$("#searchSelected").html($(this).html());
		$("#searchTab").hide();
		$("#searchSelected").removeClass("searchOpen");
	});
});
</script>
</head>

<body>
<jsp:include page="../common/head.jsp"></jsp:include>
<div class="contant_e">

    <div class="dizhilian_e"><a href="<%=basePath%>" >首页</a>><a href="<%=basePath%>user_userinfo">个人中心</a>><a href="<%=basePath%>security_goSecurity" >安全设置</a>><span>验证邮箱</span></div>
    
<jsp:include page="../common/left.jsp"></jsp:include>
<div class="right_m">
	  <div class="aqshzh">
	    <div>修改登录密码</div>
		
		<ul class="xgdlmm5">
			<li class="xgdlmmzt1">1.验证身份</li>
			<li class="xgdlmmzt2">2.修改登录密码</li>
			<li class="xgdlmmzt3">3.完成</li>
		</ul>
		<p class="xmmszcg"><img src="<%=imgurl %>/bg/xgdlmmztwc.gif"/><span>新密码设置成功！</span></p>
		<p class="tj" style="padding-left: 310px; padding-bottom: 10px;"><a href="<%=request.getContextPath() %>/security_goSecurity">返回个人中心</a></p> 

	  </div>
	</div>		
<div class="clear"></div>
</div>
<jsp:include page="../common/foot.jsp"></jsp:include>
</body>
</html>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<link href="<%=request.getContextPath() %>/css/css.css" type="text/css" rel="stylesheet"/>
<link href="<%=request.getContextPath() %>/css/css2.css" type="text/css" rel="stylesheet"/>
<link href="<%=request.getContextPath() %>/css/css3.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jqueryt.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/lrtk.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/spxx.js"></script>
<link href="<%=request.getContextPath() %>/css/style.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/script.js"></script>
<script type="text/javascript" src="js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="js/user.js" charset="utf-8"></script>
<script type="text/javascript">
$(function(){ 

	$("#searchSelected").click(function(){ 
		$("#searchTab").show();
		$(this).addClass("searchOpen");
	}); 

	$("#searchTab li").hover(function(){
		$(this).addClass("selected");
	},function(){
		$(this).removeClass("selected");
	});
	 
	$("#searchTab li").click(function(){
		$("#searchSelected").html($(this).html());
		$("#searchTab").hide();
		$("#searchSelected").removeClass("searchOpen");
	});
});
</script>
</head>

<body>

<jsp:include page="../../common/head.jsp"></jsp:include>
<div class="contant_e">

    <div class="dizhilian_e"><a href="<%=basePath%>" >首页</a>><a href="<%=basePath%>user_userinfo" >个人中心</a>><a href="<%=basePath%>security_goSecurity" >安全设置</a>><span>验证邮箱</span></div>
    <jsp:include page="../../common/left.jsp"></jsp:include>
	<div class="right_m">
	  <div class="aqshzh">
	    <div>验证手机</div>
		
		<ul class="xgdlmm">
			<li class="xgdlmmzt2">1.验证身份</li>
			<li class="xgdlmmzt2">2.修改手机号码</li>
			<li class="xgdlmmzt1">3.完成</li>
		</ul>
		<p class="sjyzcg"><img src="<%=imgurl %>/bg/xgdlmmztwc.gif"/><span>恭喜您，手机修改成功！</span></p>
		<p class="tj2"><a href="<%=request.getContextPath() %>/security_goSecurity">返回个人中心</a></p> 

	  </div>
	</div>

<div class="clear"></div>
</div>
<jsp:include page="../../common/foot.jsp"></jsp:include>
</body>
</html>

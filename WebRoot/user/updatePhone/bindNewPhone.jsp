<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../../common/basePath.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<link href="css/css.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
        <script type="text/javascript" src="js/user.js" charset="utf-8"></script>
<script type="text/javascript">
$(function(){ 

	$("#searchSelected").click(function(){ 
		$("#searchTab").show();
		$(this).addClass("searchOpen");
	}); 

	$("#searchTab li").hover(function(){
		$(this).addClass("selected");
	},function(){
		$(this).removeClass("selected");
	});
	 
	$("#searchTab li").click(function(){
		$("#searchSelected").html($(this).html());
		$("#searchTab").hide();
		$("#searchSelected").removeClass("searchOpen");
	});
	var UMb = $("#UMb").text();
		UMb = UMb.substring(0, 3) + "****" + UMb.substring(7, 11);
		$("#UMb").text(UMb);
});
</script>
</head>

<body>
<jsp:include page="../../common/head.jsp"></jsp:include>
<div class="contant_e">

    <div class="dizhilian_e"><a href="<%=basePath %>">首页</a>><a href="<%=request.getContextPath() %>/user_userinfo">个人中心</a>><a href="<%=request.getContextPath() %>/security_goSecurity" >安全设置</a>><span>验证邮箱</span></div>
    
<jsp:include page="../../common/left.jsp"></jsp:include>
    
	<div class="right_m">
	  <div class="aqshzh">
	    <div>修改登录密码</div>
		
		<ul class="xgdlmm">
			<li class="xgdlmmzt2">1.验证身份</li>
			<li class="xgdlmmzt1">2.修改手机号码</li>
			<li class="xgdlmmzt2">3.完成</li>
		</ul>
<s:form action="security_saveNewPhone" id="form" method="post">
				<ul class="yzhshj2">
			<li class="wdshj"><span>我的手机：</span><span><input type="text" id="UMb2" name="ctUser.UMb" onblur="checkPhone();"/></span>
			<span  style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="umbtip"></span>
			</li>
			<li class="shjjym"><span>手机校验码：</span><input type="text" id="sms" disabled="disabled" onblur="checkUmbYzmIsEqual();"/><input type="button" id="sendsms" maxlength="4"  value="　  获取验证码" onclick="getPhoneYzm(this)" class="sjzc_ct" style="color: #050000; font-size:12px; height: 28px; line-height: 28px; width: 110px; border:1px solid #da082f; cursor:pointer; margin-left: 20px;" />
			<span  style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="smstip"></span>
			</li>
			<li class="yzhm"><span>验证码：</span><input id="yzm" name="yzm" onblur="checkYzm();" type="text" /><img id="realyzm"
					 		src="<%=basePath%>common/image.jsp"/><a href="javascript:showYzm()">看不清，换一张</a>
			<span  style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="yzmtip"></span>		 		
		    </li>
			<li class="tijiao"><p><a href="javascript:saveNewPhone();" >提交</a></p></li> 
		</ul>
</s:form>		
	  </div>
	</div>

<div class="clear"></div>
</div>
<script type="text/javascript">
showYzm();
</script>
<jsp:include page="../../common/foot.jsp"></jsp:include>
</body>
</html>

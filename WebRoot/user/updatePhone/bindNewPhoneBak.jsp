﻿<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="../../common/basePath.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<link href="css/css.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
        <script type="text/javascript" src="js/user.js" charset="utf-8"></script>
</head>

<body>
		<s:form action="security_saveNewPhone" id="form" method="post">
			<table>
				<tr>
				<td>我的手机号:</td>
				<td><input type="text" id="UMb2" name="ctUser.UMb" onblur="checkPhone();"/>
					<span  style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="umbtip"></span>
				</td>
				</tr>
				<tr>
					<td>请填写手机验证码:</td>
					<td><input type="text" id="sms" onblur="checkUmbYzmIsEqual();"/></td>
					<td><input type="button" value="获取短信验证码" id="btn" onclick="getPhoneYzm()"/></td>
					<td colspan="2"><span style="color: red;" id="smstip"></span></td>
				</tr>
				<tr>
				<td>验证码:</td>
				<td>
					<input type="text" id="yzm" name="yzm" onblur="checkYzm();"/><img id="realyzm"
					 		src="<%=basePath%>common/image.jsp"/><a
								href="javascript:showYzm()">换一张</a>
								<span  style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="yzmtip"></span>
				</td>
				</tr>
				<tr>
					<td><input type="button" value="提交" onclick="saveNewPhone()"/></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td rowspan="4">
						<div style="display: none" id="msgtip">
							<span id="msptip2">校验码已发出，请注意查收短信，如果没有收到，你可以在<span style="color: red;">95</span>秒后要求系统重新发送</span>
						</div>
					</td>
				</tr>
			</table>
			
		</s:form>
</body>
</html>

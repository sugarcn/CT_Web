<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../common/basePath.jsp"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>

       <script type="text/javascript" src="<%=basePath%>plugins/datepicker/date/WdatePicker.js"></script>

</head>

<body>
<jsp:include page="../common/head.jsp"></jsp:include>
<div class="contant_e">

    <div class="dizhilian_e"><a href="<%=basePath %>">首页</a>><a href="<%=basePath%>user_userinfo">个人中心</a>><span>修改个人信息</span></div>
    
<jsp:include page="../common/left.jsp"></jsp:include>
    
	<div class="right_g">
	<form action="user_save" id="updaeuserform" method="post">
    		<input type="hidden" value="${ctUser.UId}" name="ctUser.UId"/>
	   <ul class="xggrxx">
           <li class="xingming"><span class="s3">姓   名：</span><input id="UUsername" name="ctUser.UUsername" value="${ctUser.UUsername }" maxlength="10" type="text" class="input_g" onblur="javascript:editName();"/><div id="usernametip"></div></li>
		   <input type="hidden" id="yuanUserName" value="${ctUser.UUsername }" />
		   <c:if test="${ctUser.USex== '男' }">
		   <li class="xingbie">
		   <span class="s3">性   别：</span><input type="radio" id="USex" name="ctUser.USex" value="男" checked="checked"/><span class="s4">男</span>
		   <input type="radio" id="USex" name="ctUser.USex" value="女"/><span class="s4">女</span> 
    	   </li>
		   </c:if>
		   <c:if test="${ctUser.USex== '女' }">
		   <li class="xingbie">
		   <span class="s3">性   别：</span><input type="radio" id="USex" name="ctUser.USex" value="男" /><span class="s4">男</span>
		   <input type="radio" id="USex" name="ctUser.USex" value="女" checked="checked"/><span class="s4">女</span> 
    	   </li>
		   </c:if>
		   
		   <li class="shengri">
		   <span class="s3">生   日：</span>
		  	<input type="text" id="UBirthday" name="ctUser.UBirthday" value="${ctUser.UBirthday }" 	onclick="WdatePicker()"  class="Wdate" src="datepicker/skin/datePicker.gif"/>
		   </li>
		   <li class="qq"><span class="s3">Q   Q：</span><input id="UQq" name="ctUser.UQq"  value="${ctUser.UQq }" maxlength="11" type="text" class="input_g"/></li>
		   <li class="weibo"><span class="s3">微   博：</span><input id="UWeibo" name="ctUser.UWeibo"  value="${ctUser.UWeibo }" maxlength="15" type="text" class="input_g"/></li>	
		    <li class="taobao"><span class="s3">淘宝账号：</span><input id="UTb" name="ctUser.UTb"  value="${ctUser.UTb }" maxlength="18" type="text" class="input_g"/></li>	   
           <li class="login"><a href="javascript:updateUser();">提　　交</a></li>
        </ul>
	</form>	
	</div>

<div class="clear"></div>
</div>
<jsp:include page="../common/foot.jsp"></jsp:include>
</body>
</html>

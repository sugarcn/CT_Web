<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../../common/basePath.jsp"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
	<meta name="Keywords" content="${systemInfo.ctKey }" />
	<meta name="Description" content="${systemInfo.ctTitle }" />
	<title>${systemInfo.ctTitle }</title>
	<script src="<%=basePath %>js/jquery-1.11.3.js"></script>
  </head>
  
  <body>
  	<jsp:include page="../../common/head.jsp"></jsp:include>
	  	<div class="contant_e" style="width:1190px;margin:0 auto;overflow:hidden;">
		<input type="hidden" id="isd" value="isd" />
	    <div class="dizhilian_e"><a href=<%=basePath%> >首页</a>><a href="<%=basePath%>user_userinfo">个人中心</a>><span>客服客户管理</span></div>
	    
	  	<jsp:include page="../../common/left.jsp"></jsp:include>
	  	
	  	
	  	<div class="shouhuodizhi_j">
	     <div class="div1">客户列表</div>
	     <s:iterator value="uesrClientsList" var="list">
	     	<s:if test="#list.cname != '原账户地址'">
		     <s:if test="#list.cdefult==1">
		     	<ul class="ul1_j">
			   <li class="li1_j">
			   <div class="div2">客户姓名：<span class="span1">${list.cname }</span></div><div class="div3">当前下单客户</div><div class="div4"><span class="span2"></div>
			   </li>
			 </ul>
		     </s:if>
		     <s:if test="#list.cdefult!=1">
		      <ul class="ul1_j" style="margin-top:20px;position:relative;">
			   <li class="li1_j">
			   <div class="div2">客户姓名：<span class="span1">${list.cname }</span></div>
			   <div class="div4" style="position:absolute;bottom:20px;right:20px;">
			   	<a href="setAsCusAddress?id=${list.cid }">设为当前下单客户</a>
			   </div>
			   </li>
			 </ul>
		     </s:if>
	     	</s:if>
	     	<s:if test="#list.cname != '原账户地址'">
	     	<s:if test="#list.cdefult==1">
	     	<ul class="ul1_j">
		   <li class="li1_j">
		   <div class="div2">客户姓名：<span class="span1">${list.cname }</span></div><div class="div3">当前下单客户</div><div class="div4"><span class="span2"></div>
		   </li>
		 </ul>
	     </s:if>
	     <s:if test="#list.cdefult!=1">
	      <ul class="ul1_j" style="margin-top:20px;position:relative;">
		   <li class="li1_j">
		   <div class="div2">客户姓名：<span class="span1">${list.cname }</span></div>
		   <div class="div4" style="position:absolute;bottom:20px;right:20px;">
		   	<a href="setAsCusAddress?id=${list.cid }">设为当前下单客户</a>
		   </div>
		   </li>
		 </ul>
	     </s:if>
	     </s:if>
		 </s:iterator>
	   </div>
	  	
	  	<div class="clear"></div>
	  	        ${pages.pageGoodsStr }
	  	        <script type="text/javascript">
	  	        	function upAndDownPage(num){
	  	        		location.href="add_Cus_Address?page=" + num;
	  	        	}
	  	        	function loadPage(){
		  	        	$(".pager_d").attr("style","margin-left:415px;");
		  	        	var isd = $("#isd").val();
		  	        	if(isd == "false"){
		  	        		alert("当前客户地址为空，设置失败");
		  	        	}
	  	        	}
	  	        	loadPage();
	  	        	
	  	        </script>
  	</div>
  	<jsp:include page="../../common/foot.jsp"></jsp:include>
    <script type="text/javascript" src="<%=basePath%>js/user.js" charset="utf-8"></script>
	<script type="text/javascript" src="<%=basePath%>js/address.js"></script>
  </body>
</html>

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../../common/basePath.jsp"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<script src="<%=basePath %>js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="<%=basePath%>plugins/datepicker/date/WdatePicker.js"></script>




<style type="text/css">
#BgDiv{background-color:#000; position:fixed; z-index:999; left:0; top:0; display:none; width:100%; height:1000px;opacity:0.65;filter: alpha(opacity=40);-moz-opacity: 0.4;}

</style>
</head>

<body>
<div id="BgDiv"></div>
<jsp:include page="../../common/head.jsp"></jsp:include>
<div class="contant_e" style="width:1190px;margin:0 auto;overflow:hidden;">

    <div class="dizhilian_e"><a href=<%=basePath%> >首页</a>><a href="<%=basePath%>user_userinfo">个人中心</a>><span>收货地址管理</span></div>
    
<jsp:include page="../../common/left.jsp"></jsp:include>
<div class="right_j">
	
	   <div class="shouhuodizhi_j">
	     <div class="div1">收货地址</div>
	     <div class="BMchuangxzer" style="display:none;">
<s:form method="post" id="updateaddressform" action="address_update">	 
	        <div class="xzshhdz">
			  <div class="lchaxz"><span><img src="<%=imgurl %>/bg/chadiao.gif" /></span></div>
			  <ul>
			   <input type="hidden" id="AId" name="AId"/> 
			    <li class="li1"><span class="s1">使用新地址</span><span class="s3">带“<span class="s2">*</span>”为必填项！</span></li>
				<li class="li2">
				  <select id="country" name="country" onchange="showProvince('<%=basePath%>',this.value)">
				    <option value="0" >---请选择---</option>
				    <s:iterator value="regions" var="list">
				    <option value="${list.regionId}" >${list.regionName}</option>
				    </s:iterator>
				    </select>
				  <select id="province" name="province" onchange="showCity('<%=basePath%>',this.value)">
				    <option value="0">---请选择---</option>
				    </select>
				  <select id="city" name="city" onchange="showDistrict('<%=basePath%>',this.value)">
				    <option value="0">---请选择---</option>
				    </select>
				  <select id="district" name="district">
				    <option value="0">---请选择---</option>
				  </select>
				  <span class="s4">*</span><span class="s5">请选择</span>
				  <span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="regionTip"></span>
				</li>
				<li class="li3">详细地址：<input id="address" name="address" type="text" class="jj"/><span class="s6">*</span><span class="s7">请详细填正确地址</span>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="addressTip"></span>
				</li>
				<li class="li3">邮&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;编：<input id="zipcode" name="zipcode" type="text" class="jj"/><span class="s6">*</span><span class="s7">邮编</span>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="zipcodeTip"></span>
				</li>
				<li class="li5">收货人姓名：<input id="AConsignee" name="AConsignee" type="text" class="jj"/><span class="s6">*</span><span class="s7">收货人姓名  先生/女士</span>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="AConsigneeTip"></span>
				</li>
				<li class="li3">手机号码：<input id="tel" name="tel" type="text" class="jj"/>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="telTip"></span>
				</li>
				<li class="li3">电话号码：<input id="bm" name="mb" type="text" class="jj"/><span class="s6">*手机、电话</span><span class="s7">两者至少填一项</span>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="mbTip"></span>
				</li>
				<li class="li3">快递备注：<input id="adesc" name="adesc" type="text" class="jj"/>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="descTip"></span>
				</li>
				<li class="li6"><a href="javascript:updateAddress()">修改收货人地址</a></li>
			  </ul>
			</div>
</s:form>
         </div>
	     <s:iterator value="addresses" var="list">
	     <s:if test="#list.isdefault==0">
	     	<ul class="ul1_j">
		   <li class="li1_j">
		   <div class="div2">收货人：<span class="span1">${list.AConsignee }</span></div><div class="div3">默认地址</div><div class="div4"><a href="javascript:del(${list.AId });">删除</a><span class="span2">|</span><a href="javascript:updateAddress1('<%=basePath%>',${list.AId });" id="btn_bxzer">修改</a><span class="span2"></div>
		   </li>
		   <c:if test="${list.regionCity.regionName==list.regionDistrict.regionName}">
		   <li>地址：${list.regionProvince.regionName }${list.regionCity.regionName}${list.address }</li>
		   </c:if>
		   <c:if test="${list.regionCity.regionName!=list.regionDistrict.regionName}">
		   <li>地址：${list.regionProvince.regionName }${list.regionCity.regionName}${list.regionDistrict.regionName}${list.address }</li>
		   </c:if>
		   <li>邮编：${list.zipcode }</li>
		   <li>手机：${list.tel }</li>
		   <li>电话：${list.mb }</li>
		   <li>快递备注：${list.adesc }</li>
		 </ul>
	     </s:if>
	     <s:if test="#list.isdefault==1">
	      <ul class="ul1_j">
		   <li class="li1_j">
		   <div class="div2">收货人：<span class="span1">${list.AConsignee }</span></div><div class="div4"><a href="javascript:del(${list.AId });">删除</a><span class="span2">|</span><a href="javascript:updateAddress1('<%=basePath%>',${list.AId });" id="btn_bxzer">修改</a><span class="span2">|</span><a href="address_setAsDeafult?AId=${list.AId }">设为默认</a></div>
		   </li>
		   <c:if test="${list.regionCity.regionName==list.regionDistrict.regionName}">
		   <li>地址：${list.regionProvince.regionName }${list.regionCity.regionName}${list.address }</li>
		   </c:if>
		   <c:if test="${list.regionCity.regionName!=list.regionDistrict.regionName}">
		   <li>地址：${list.regionProvince.regionName }${list.regionCity.regionName}${list.regionDistrict.regionName}${list.address }</li>
		   </c:if>
		   <li>邮编：${list.zipcode }</li>
		   <li>手机：${list.tel }</li>
		   <li>电话：${list.mb }</li>
		   <li>快递备注：${list.adesc }</li>
		 </ul>
	     </s:if>
		 </s:iterator>
		 <ul class="ul1_j">
		 	<li class="li2_j"><a href="javascript:;" id="btn_bxz" >+新增收货地址</a></li>
		 </ul>
<s:form method="post" id="addressform" action="address_add">	 
		 <div class="BMchuangxz" style="display:none;">
	        
	        <div class="xzshhdz">
			  <div class="lchaxz"><span><img src="<%=imgurl %>/bg/chadiao.gif" /></span></div>
			  <ul>
			    <li class="li1"><span class="s1">使用新地址</span><span class="s3">带“<span class="s2">*</span>”为必填项！</span></li>
			     <div class="msg-up" style="display: none;"></div>
				<li class="li2">
				  <select id="country2" name="country" onchange="showProvince2('<%=basePath%>',this.value)">
				    <option value="0" >---请选择---</option>
				    <s:iterator value="regions" var="list">
				    <option value="${list.regionId}" >${list.regionName}</option>
				    </s:iterator>
				    </select>
				  <select id="province2" name="province" onchange="showCity2('<%=basePath%>',this.value)">
				    <option value="0">---请选择---</option>
				    </select>
				  <select id="city2" name="city" onchange="showDistrict2('<%=basePath%>',this.value)">
				    <option value="0">---请选择---</option>
				    </select>
				     <select id="district2" name="district" >
				    <option value="0">---请选择---</option>
				    </select>
				  <span class="s4">*</span><span class="s5">请选择</span>
				  <span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="region2Tip"></span>
				</li>
				<li class="li3">详细地址：<input id="address2" name="address" type="text" class="jj" onblur="checkName();"/><span class="s6">*</span><span class="s7">请详细填正确地址</span>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="address2Tip"></span>
				</li>
				<li class="li3">邮&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;编：<input id="zipcode2" name="zipcode" type="text" class="jj"/><span class="s6">*</span><span class="s7">邮编</span>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="zipcode2Tip"></span>
				</li>
				<li class="li5">收货人姓名：<input id="AConsignee2" name="AConsignee" type="text" class="jj" onblur="checkAddress();"/><span class="s6">*</span><span class="s7">收货人姓名  先生/女士</span>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="AConsignee2Tip"></span>
				</li>
				<li class="li3">手机号码：<input id="tel2" name="tel" type="text" class="jj"/>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="tel2Tip"></span>
				</li>
				<li class="li3">电话号码：<input id="mb2" name="mb" type="text" class="jj"/><span class="s6">*手机、电话</span><span class="s7">两者至少填一项</span>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="mb2Tip"></span>
				</li>
				<li class="li3">快递备注：<input id="desc" name="adesc" type="text" class="jj"/>
				<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="descTip"></span>
				</li>
				<li class="li6"><a href="javascript:saveadd()">保存收货人地址</a></li>
			  </ul>
			</div>
         </div>
</s:form>			 
	   </div>
	</div>
	<div id="dele" class="BMchuang BMchuangsmall " style="top:50%; left:50%; z-index:1000; display:none;" >
		<div class="lcha" id="btnClose" onclick="closeDiv('DialogDiv')"><span><img src="<%=imgurl %>/bg/chadiao.gif" /></span></div>
    	<div class="tizp_de">
        	<p>是否删除？</p>
    
	        <div class="jiarugoux_dd">
	        <span class="jiaruche_d"><a id="deladdress" href="javascript:;">确定</a></span>
	        <span class="lcha"><a class="jiaruche_d1" id="btnClose" onclick="closeDiv('DialogDiv')">取消</a></span>
	</div>
    </div>
</div>
<div class="clear"></div>
</div>

<jsp:include page="../../common/foot.jsp"></jsp:include>
       <script type="text/javascript" src="<%=basePath%>js/user.js" charset="utf-8"></script>
		<script type="text/javascript" src="<%=basePath%>js/address.js"></script>
</body>
</html>

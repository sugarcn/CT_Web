<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../../common/basePath.jsp"%>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/coupon.js"></script>
 <%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<%@include file="../../common/head.jsp" %>
<div class="contant_e">

    <div class="dizhilian_e"><a href="<%=basePath %>">首页</a>><a href="<%=request.getContextPath() %>/user_userinfo">个人中心</a>><span>我的优惠券</span></div>
<%@include file="../../common/left.jsp" %> 
<div class="jihuo_n">
<a href="javascript:;" class="jihuoy" id="btn_bxz" >激活优惠券</a>

		 <div class="BMchuangxz" style="display:none;">
	        
	        <div class="xzshhdzo">
			  <div class="lchaxzo lchaxz"><span><img src="<%=imgurl %>/bg/chadiao.gif" /></span></div>
<div class="shuru"><span>输入优惠券号：</span><span class="shusou"><input name="textfielder" id="couponNum" type="text" maxlength="19" style="color: rgb(204, 204, 204);" value="请输入19位优惠券号" onblur="if(this.value==''||this.value=='请输入19位优惠券号'){this.value='请输入19位优惠券号';this.style.color='#cccccc'}" onfocus="if(this.value=='请输入19位优惠券号') {this.value='';};this.style.color='#000000';"/></span><span class="shusou"><a href="javascript:activate()">激活</a></span></div>
			<span  style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="yhq"></span>
			</div>
         </div>
</div>    
<div class="right_n">
	   <div class="youhuiquan">
	     <div class="div1">我的优惠券</div>
		 
		 <ul class="ul1">
		   <li id="xmlistb1" onclick="setTab('xmlistb',1,2)" class="hover"><a href="" onclick="return false" ><span>全网商品</span><span class="shuzhi_l">${cout_all }</span></a></li>
           <li id="xmlistb2" onclick="setTab('xmlistb',2,2)"><a href="" onclick="return false"><span>限定商品</span><span class="shuzhi_l">${cout_admit }</span></a></li>
		 </ul>

		 
		 
            <div id="con_xmlistb_1" class="con_all">
            <div class="sanlei_n"><a class="hover">未使用</a><a>已使用</a><a>已过期</a></div> 
		 <ul class="ul2">
		   <li class="li21">优惠券号</li>
		   <li class="li22">优惠券名称</li>
		   <li class="li23">有效期</li>
		 </ul>
		 
		 <s:iterator value="couponDetailsList" var="list">
		 	<ul class="ul3">
		 	<li class="li31">${list.couponNumber }</li>
		 	<c:if test="${list.ctCoupon.couponType == '1' }">
		 		<li class="li32"><img src="<%=imgurl %>/bg/manjian.gif"/><span>满${list.ctCoupon.amount }减${list.ctCoupon.discount }</span></li>
		 	</c:if>
		 	<c:if test="${list.ctCoupon.couponType == '2' }">
		 		<li class="li32"><img src="<%=imgurl %>/bg/jiantou.gif"/><span>${list.ctCoupon.discount }元无限制使用</span></li>
		 	</c:if>
		 	<c:if test="${list.ctCoupon.couponType == '3' }">
		 		<li class="li32"><img src="<%=imgurl %>/bg/jiantou.gif"/><span>${list.ctCoupon.discount}折</span></li>
		 	</c:if>
		 	<li class="li33">${list.ctCoupon.stime }&nbsp至&nbsp${list.ctCoupon.etime }</li>
		 	<li class="li35"><a href="javascript:goGoodsList()">去采购</a></li>
		 	</ul>
		 </s:iterator>
		 
            </div>
            <div id="con_xmlistb_2" class="con_all" style="display:none;">
            <div class="sanlei_n"><a class="hover">未使用</a><a>已使用</a><a>已过期</a></div>
		 <ul class="ul2">
		   <li class="li21">优惠券号</li>
		   <li class="li22">优惠券名称</li>
		   <li class="li23">有效期</li>
		 </ul>
		 <s:iterator value="couponDetailsAdmitList" var="admitlist">
		 	<ul class="ul3">
		 	 <li class="li31">${admitlist.couponNumber }</li>
		   	 <c:if test="${admitlist.ctCoupon.couponType == '1' }">
		 		<li class="li32"><img src="<%=imgurl %>/bg/manjian.gif"/><span>满${admitlist.ctCoupon.amount }减${admitlist.ctCoupon.discount }</span></li>
		 	</c:if>
		 	<c:if test="${admitlist.ctCoupon.couponType == '2' }">
		 		<li class="li32"><img src="<%=imgurl %>/bg/jiantou.gif"/><span>${admitlist.ctCoupon.discount}无限制使用</span></li>
		 	</c:if>
		 	<c:if test="${admitlist.ctCoupon.couponType == '3' }">
		 		<li class="li32"><img src="<%=imgurl %>/bg/jiantou.gif"/><span>${admitlist.ctCoupon.discount}折</span></li>
		 	</c:if>
		   	 <li class="li33">${admitlist.ctCoupon.stime }&nbsp至&nbsp${admitlist.ctCoupon.etime }</li>
		   	 <li class="li34">
		     <a href="javascript:goGoodsDetail(${admitlist.ctCoupon.goods.GId })"><img src="${admitlist.ctCoupon.goodsImg.PUrl }"></a>
			 <p class="p1"><a href="javascript:goGoodsDetail(${admitlist.ctCoupon.goods.GId })">${admitlist.ctCoupon.goods.GName }</a></p>
			 <p class="p2">${admitlist.ctCoupon.goods.promotePrice }</p>
		   </li>
		   <li class="li35"><a href="javascript:goGoodsDetail(${admitlist.ctCoupon.goods.GId })">去采购</a></li>
		 	</ul>
		 </s:iterator>
            </div>
		 
		 <!-- 分页控制器 -->
		${pages.pageGoodsStr }
		 
	   </div> 	
	</div>
	 
<div class="clear"></div>
</div>
<script type="text/javascript">
	/* 分页跳转控制器 */
	function upAndDownPage(sum){
		//截取访问名
		var url = document.URL.toString();

		var isPage = "";
		if(url.toString().indexOf("&page") > 0){
			url = url.toString().substring(0,url.toString().indexOf("&page"));
		}
		window.location.href= url+"&page=" + sum;
	}
	</script>
<%@include file="../../common/foot.jsp" %>    
</body>
</html>

﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/coupon.js"></script>
 <%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ include file="../../common/basePath.jsp"%>
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<jsp:include page="../../common/head.jsp"></jsp:include>
<div class="contant_e">
    <div class="dizhilian_e"><a href="<%=basePath %>" >首页</a>><a href="<%=basePath%>user_userinfo">个人中心</a>><span>我的优惠券</span></div>
<%@include file="../../common/left.jsp" %> 
   
<div class="right_n">
<div class="jihuo_n">
<a href="javascript:;" class="jihuoy" onclick="openBxz()" id="btn_bxz" >激活优惠券</a>

		 <div class="BMchuangxz" style="display:none;">
	        
	        <div class="xzshhdzo">
			  <div class="lchaxzo lchaxz" onclick="closeBxz()"><span><img src="<%=imgurl %>/bg/chadiao.gif" /></span></div>
<div class="shuru"><span>输入优惠券号：</span><span class="shusou"><input name="textfielder" id="couponNum" type="text" maxlength="8" style="color: rgb(204, 204, 204);" value="请输入8位优惠券号" onblur="if(this.value==''||this.value=='请输入8位优惠券号'){this.value='请输入8位优惠券号';this.style.color='#cccccc'}" onfocus="if(this.value=='请输入8位优惠券号') {this.value='';};this.style.color='#000000';"/></span><span class="shusou"><a href="javascript:activate()">激活</a></span></div>
			<span  style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="yhq"></span>
			</div>
         </div>
</div> 

		<c:if test="${type1==1 && type2==1 }">
		
		
	   <div class="youhuiquan">
	     <div class="div1">我的优惠券</div>
		 
		 <ul class="ul1">
		   <li id="xmlistb1" onclick="setTab2('xmlistb',1,2)" class="hover" ><a href="javascript:getAll();" onclick="return false" ><span>全网商品</span><span class="shuzhi_l">${cout_all }</span></a></li>
           <li id="xmlistb2" onclick="setTab2('xmlistb',2,2)" ><a href="javascript:getAdmit()" onclick="return false"><span>限定商品</span><span class="shuzhi_l">${cout_admit }</span></a></li>
		   <li id="xmlistb3" onclick="setTab2('xmlistb',3,2)" ><a href="javascript:getSam()" onclick="return false"><span>样品</span><span class="shuzhi_l">${cout_sam }</span></a></li>
		 </ul>
            <div id="con_xmlistb_1" class="con_all">
            <div class="sanlei_n"><a href="javascript:goCoupon(1,1)" class="hover">可使用</a><a href="javascript:goCoupon(1,2)">已使用</a><a href="javascript:goCoupon(1,3)">已过期</a></div> 
		 <ul class="ul2">
		   <li class="li21">优惠券号</li>
		   <li class="li22">优惠券名称</li>
		   <li class="li23">有效期</li>
		 </ul>
		 
		 <s:iterator value="couponDetailsList" var="list">
		 	<ul class="ul3">
		 	<li class="li31">${list.couponNumber }</li>
		 	<c:if test="${list.ctCoupon.couponType == '1' }">
		 		<li class="li32"><img src="<%=imgurl %>/bg/manjian.gif"/><span>满${list.ctCoupon.amount }减${list.ctCoupon.discount }</span></li>
		 	</c:if>
		 	<c:if test="${list.ctCoupon.couponType == '2' }">
		 		<li class="li32"><img src="<%=imgurl %>/bg/jiantou.gif"/><span>${list.ctCoupon.couponName }</span></li>
		 	</c:if>
		 	<c:if test="${list.ctCoupon.couponType == '3' }">
		 		<li class="li32"><img src="<%=imgurl %>/bg/jiantou.gif"/><span>${list.ctCoupon.discount}折</span></li>
		 	</c:if>
		 	<li class="li33">${list.ctCoupon.stime }&nbsp至&nbsp${list.ctCoupon.etime }</li>
		 	<li class="li35"><a href="javascript:goGoodsList()" >去采购</a></li>
		 	</ul>
		 </s:iterator>
            </div>
	   </div> 	
	   </c:if>
	   <c:if test="${type1==1 && type2==2 }">
	   <div class="youhuiquan">
	     <div class="div1">我的优惠券</div>
		 
		 <ul class="ul1">
		   <li id="xmlistb1" onclick="setTab2('xmlistb',1,2)" class="hover" ><a href="javascript:getAll();" onclick="return false" ><span>全网商品</span><span class="shuzhi_l">${cout_all }</span></a></li>
           <li id="xmlistb2" onclick="setTab2('xmlistb',2,2)" ><a href="javascript:getAdmit()" onclick="return false"><span>限定商品</span><span class="shuzhi_l">${cout_admit }</span></a></li>
		   <li id="xmlistb3" onclick="setTab2('xmlistb',3,2)" ><a href="javascript:getSam()" onclick="return false"><span>样品</span><span class="shuzhi_l">${cout_sam }</span></a></li>
		 </ul>
            <div id="con_xmlistb_1" class="con_all">
            <div class="sanlei_n"><a href="javascript:goCoupon(1,1)" >可使用</a><a href="javascript:goCoupon(1,2)" class="hover">已使用</a><a href="javascript:goCoupon(1,3)">已过期</a></div> 
		 <ul class="ul2">
		   <li class="li21">优惠券号</li>
		   <li class="li22">优惠券名称</li>
		   <li class="li23">有效期</li>
		 </ul>
		 
		 <s:iterator value="couponDetailsList" var="list">
		 	<ul class="ul3">
		 	<li class="li31">${list.couponNumber }</li>
		 	<c:if test="${list.ctCoupon.couponType == '1' }">
		 		<li class="li32"><img src="<%=imgurl %>/bg/manjian.gif"/><span>满${list.ctCoupon.amount }减${list.ctCoupon.discount }</span></li>
		 	</c:if>
		 	<c:if test="${list.ctCoupon.couponType == '2' }">
		 		<li class="li32"><img src="<%=imgurl %>/bg/jiantou.gif"/><span>${list.ctCoupon.couponName }</span></li>
		 	</c:if>
		 	<c:if test="${list.ctCoupon.couponType == '3' }">
		 		<li class="li32"><img src="<%=imgurl %>/bg/jiantou.gif"/><span>${list.ctCoupon.discount}折</span></li>
		 	</c:if>
		 	<li class="li33">${list.ctCoupon.stime }&nbsp至&nbsp${list.ctCoupon.etime }</li>
<!-- 		 	<li class="li35"><a href="javascript:goGoodsList()">去采购</a></li> -->
		 	</ul>
		 </s:iterator>
            </div>
	   </div> 	
	   </c:if>
	   <c:if test="${type1==1 && type2==3 }">
	   <div class="youhuiquan">
	     <div class="div1">我的优惠券</div>
		 
		 <ul class="ul1">
		   <li id="xmlistb1" onclick="setTab2('xmlistb',1,2)" class="hover" ><a href="javascript:getAll();" onclick="return false" ><span>全网商品</span><span class="shuzhi_l">${cout_all }</span></a></li>
           <li id="xmlistb2" onclick="setTab2('xmlistb',2,2)" ><a href="javascript:getAdmit()" onclick="return false"><span>限定商品</span><span class="shuzhi_l">${cout_admit }</span></a></li>
		   <li id="xmlistb3" onclick="setTab2('xmlistb',3,2)" ><a href="javascript:getSam()" onclick="return false"><span>样品</span><span class="shuzhi_l">${cout_sam }</span></a></li>
		 </ul>
            <div id="con_xmlistb_1" class="con_all">
            <div class="sanlei_n"><a href="javascript:goCoupon(1,1)" >可使用</a><a href="javascript:goCoupon(1,2)" >已使用</a><a href="javascript:goCoupon(1,3)" class="hover">已过期</a></div> 
		 <ul class="ul2">
		   <li class="li21">优惠券号</li>
		   <li class="li22">优惠券名称</li>
		   <li class="li23">有效期</li>
		 </ul>
		 
		 <s:iterator value="couponDetailsList" var="list">
		 	<ul class="ul3">
		 	<li class="li31">${list.couponNumber }</li>
		 	<c:if test="${list.ctCoupon.couponType == '1' }">
		 		<li class="li32"><img src="<%=imgurl %>/bg/manjian.gif"/><span>满${list.ctCoupon.amount }减${list.ctCoupon.discount }</span></li>
		 	</c:if>
		 	<c:if test="${list.ctCoupon.couponType == '2' }">
		 		<li class="li32"><img src="<%=imgurl %>/bg/jiantou.gif"/><span>${list.ctCoupon.couponName }</span></li>
		 	</c:if>
		 	<c:if test="${list.ctCoupon.couponType == '3' }">
		 		<li class="li32"><img src="<%=imgurl %>/bg/jiantou.gif"/><span>${list.ctCoupon.discount}折</span></li>
		 	</c:if>
		 	<li class="li33">${list.ctCoupon.stime }&nbsp至&nbsp${list.ctCoupon.etime }</li>
<!-- 		 	<li class="li35"><a href="javascript:goGoodsList()">去采购</a></li> -->
		 	</ul>
		 </s:iterator>
            </div>
	   </div> 	
	   </c:if>
	   <c:if test="${type1==2 && type2==1 }">
	    <div class="youhuiquan">
	     <div class="div1">我的优惠券</div>
		 
		 <ul class="ul1">
		   <li id="xmlistb1" onclick="setTab2('xmlistb',1,2)"  ><a href="javascript:getAll();" onclick="return false" ><span>全网商品</span><span class="shuzhi_l">${cout_all }</span></a></li>
           <li id="xmlistb2" onclick="setTab2('xmlistb',2,2)" class="hover"><a href="javascript:getAdmit()" onclick="return false"><span>限定商品</span><span class="shuzhi_l">${cout_admit }</span></a></li>
		   <li id="xmlistb3" onclick="setTab2('xmlistb',3,2)" ><a href="javascript:getSam()" onclick="return false"><span>样品</span><span class="shuzhi_l">${cout_sam }</span></a></li>
		 </ul>
            <div id="con_xmlistb_1" class="con_all">
            <div class="sanlei_n"><a href="javascript:goCoupon(2,1)" class="hover" >可使用</a><a href="javascript:goCoupon(2,2)" >已使用</a><a href="javascript:goCoupon(2,3)">已过期</a></div> 
		 <ul class="ul2">
		   <li class="li21">优惠券号</li>
		   <li class="li22">优惠券名称</li>
		   <li class="li23">有效期</li>
		 </ul>
		 
		 <s:iterator value="couponDetailsAdmitList" var="admitlist">
		 	<ul class="ul3">
		 	 <li class="li31">${admitlist.couponNumber }</li>
		   	 <c:if test="${admitlist.ctCoupon.couponType == '1' }">
		 		<li class="li32"><img src="<%=imgurl %>/bg/manjian.gif"/><span>满${admitlist.ctCoupon.amount }减${admitlist.ctCoupon.discount }</span></li>
		 	</c:if>
		 	<c:if test="${admitlist.ctCoupon.couponType == '2' }">
		 		<li class="li32"><img src="<%=imgurl %>/bg/jiantou.gif"/><span>${admitlist.ctCoupon.couponName }</span></li>
		 	</c:if>
		 	<c:if test="${admitlist.ctCoupon.couponType == '3' }">
		 		<li class="li32"><img src="<%=imgurl %>/bg/jiantou.gif"/><span>${admitlist.ctCoupon.discount}折</span></li>
		 	</c:if>
		   	 <li class="li33">${admitlist.ctCoupon.stime }&nbsp至&nbsp${admitlist.ctCoupon.etime }</li>
		   	 <li class="li34">
		     <a href="javascript:goGoodsDetail(${admitlist.ctCoupon.GId })">
		     <c:if test="${admitlist.ctCoupon.goodsImg.PUrl ==null}">
		     <img src="<%=request.getContextPath()%>/<%=imgurl %>/ct_s.gif">
		     </c:if>
		     <c:if test="${admitlist.ctCoupon.goodsImg.PUrl !=null}">
		     <img src="${admitlist.ctCoupon.goodsImg.PUrl }">
		     </c:if>
		     </a>
			 <p class="p1"><a href="javascript:goGoodsDetail(${admitlist.ctCoupon.GId })">${admitlist.ctCoupon.goods.GName }</a></p>
			 <p class="p2">${admitlist.ctCoupon.goods.promotePrice }</p>
		   </li>
		   <li class="li35"><a href="javascript:goGoodsDetail(${admitlist.ctCoupon.GId })" >去采购</a></li>
		 	</ul>
		 </s:iterator>
            </div>
	   </div> 	
	   </c:if>
	   <c:if test="${type1==2 && type2==2 }">
	    <div class="youhuiquan">
	     <div class="div1">我的优惠券</div>
		 
		 <ul class="ul1">
		   <li id="xmlistb1" onclick="setTab2('xmlistb',1,2)"  ><a href="javascript:getAll();" onclick="return false" ><span>全网商品</span><span class="shuzhi_l">${cout_all }</span></a></li>
           <li id="xmlistb2" onclick="setTab2('xmlistb',2,2)" class="hover"><a href="javascript:getAdmit()" onclick="return false"><span>限定商品</span><span class="shuzhi_l">${cout_admit }</span></a></li>
		   <li id="xmlistb3" onclick="setTab2('xmlistb',3,2)" ><a href="javascript:getSam()" onclick="return false"><span>样品</span><span class="shuzhi_l">${cout_sam }</span></a></li>
		 </ul>
            <div id="con_xmlistb_1" class="con_all">
            <div class="sanlei_n"><a href="javascript:goCoupon(2,1)">可使用</a><a href="javascript:goCoupon(2,2)" class="hover">已使用</a><a href="javascript:goCoupon(2,3)">已过期</a></div> 
		 <ul class="ul2">
		   <li class="li21">优惠券号</li>
		   <li class="li22">优惠券名称</li>
		   <li class="li23">有效期</li>
		 </ul>
		 
		 <s:iterator value="couponDetailsAdmitList" var="admitlist">
		 	<ul class="ul3">
		 	 <li class="li31">${admitlist.couponNumber }</li>
		   	 <c:if test="${admitlist.ctCoupon.couponType == '1' }">
		 		<li class="li32"><img src="<%=imgurl %>/bg/manjian.gif"/><span>满${admitlist.ctCoupon.amount }减${admitlist.ctCoupon.discount }</span></li>
		 	</c:if>
		 	<c:if test="${admitlist.ctCoupon.couponType == '2' }">
		 		<li class="li32"><img src="<%=imgurl %>/bg/jiantou.gif"/><span>${admitlist.ctCoupon.couponName}</span></li>
		 	</c:if>
		 	<c:if test="${admitlist.ctCoupon.couponType == '3' }">
		 		<li class="li32"><img src="<%=imgurl %>/bg/jiantou.gif"/><span>${admitlist.ctCoupon.discount}折</span></li>
		 	</c:if>
		   	 <li class="li33">${admitlist.ctCoupon.stime }&nbsp至&nbsp${admitlist.ctCoupon.etime }</li>
		   	 <li class="li34">
		     <a href="javascript:goGoodsDetail(${admitlist.ctCoupon.GId })">
				<c:if test="${admitlist.ctCoupon.goodsImg.PUrl ==null}">
			     <img src="<%=request.getContextPath()%>/<%=imgurl %>/ct_s.gif">
			     </c:if>
			     <c:if test="${admitlist.ctCoupon.goodsImg.PUrl !=null}">
			     <img src="${admitlist.ctCoupon.goodsImg.PUrl }">
			     </c:if>
			 </a>
			 <p class="p1"><a href="javascript:goGoodsDetail(${admitlist.ctCoupon.GId })">${admitlist.ctCoupon.goods.GName }</a></p>
			 <p class="p2">${admitlist.ctCoupon.goods.promotePrice }</p>
		   </li>
<!-- 		   <li class="li35"><a href="javascript:goGoodsDetail(${admitlist.ctCoupon.GId })">去采购</a></li> -->
		 	</ul>
		 </s:iterator>
            </div>
	   </div> 	
	   </c:if>
	   <c:if test="${type1==2 && type2==3 }">
	    <div class="youhuiquan">
	     <div class="div1">我的优惠券</div>
		 
		 <ul class="ul1">
		   <li id="xmlistb1" onclick="setTab2('xmlistb',1,2)"  ><a href="javascript:getAll();" onclick="return false" ><span>全网商品</span><span class="shuzhi_l">${cout_all }</span></a></li>
           <li id="xmlistb2" onclick="setTab2('xmlistb',2,2)" class="hover"><a href="javascript:getAdmit()" onclick="return false"><span>限定商品</span><span class="shuzhi_l">${cout_admit }</span></a></li>
		   <li id="xmlistb3" onclick="setTab2('xmlistb',3,2)" ><a href="javascript:getSam()" onclick="return false"><span>样品</span><span class="shuzhi_l">${cout_sam }</span></a></li>
		 </ul>
            <div id="con_xmlistb_1" class="con_all">
            <div class="sanlei_n"><a href="javascript:goCoupon(2,1)">可使用</a><a href="javascript:goCoupon(2,2)" >已使用</a><a href="javascript:goCoupon(2,3)" class="hover">已过期</a></div> 
		 <ul class="ul2">
		   <li class="li21">优惠券号</li>
		   <li class="li22">优惠券名称</li>
		   <li class="li23">有效期</li>
		 </ul>
		 
		 <s:iterator value="couponDetailsAdmitList" var="admitlist">
		 	<ul class="ul3">
		 	 <li class="li31">${admitlist.couponNumber }</li>
		   	 <c:if test="${admitlist.ctCoupon.couponType == '1' }">
		 		<li class="li32"><img src="<%=imgurl %>/bg/manjian.gif"/><span>满${admitlist.ctCoupon.amount }减${admitlist.ctCoupon.discount }</span></li>
		 	</c:if>
		 	<c:if test="${admitlist.ctCoupon.couponType == '2' }">
		 		<li class="li32"><img src="<%=imgurl %>/bg/jiantou.gif"/><span>${admitlist.ctCoupon.couponName}</span></li>
		 	</c:if>
		 	<c:if test="${admitlist.ctCoupon.couponType == '3' }">
		 		<li class="li32"><img src="<%=imgurl %>/bg/jiantou.gif"/><span>${admitlist.ctCoupon.discount}折</span></li>
		 	</c:if>
		   	 <li class="li33">${admitlist.ctCoupon.stime }&nbsp至&nbsp${admitlist.ctCoupon.etime }</li>
		   	 <li class="li34">
		     <a href="javascript:goGoodsDetail(${admitlist.ctCoupon.GId })">
		     <c:if test="${admitlist.ctCoupon.goodsImg.PUrl ==null}">
		     <img src="<%=request.getContextPath()%>/<%=imgurl %>/ct_s.gif">
		     </c:if>
		     <c:if test="${admitlist.ctCoupon.goodsImg.PUrl !=null}">
		     <img src="${admitlist.ctCoupon.goodsImg.PUrl }">
		     </c:if>
			 </a>
			 <p class="p1"><a href="javascript:goGoodsDetail(${admitlist.ctCoupon.GId })">${admitlist.ctCoupon.goods.GName }</a></p>
			 <p class="p2">${admitlist.ctCoupon.goods.promotePrice }</p>
		   </li>
<!-- 		   <li class="li35"><a href="javascript:goGoodsDetail(${admitlist.ctCoupon.GId })">去采购</a></li> -->
		 	</ul>
		 </s:iterator>
            </div>
	   </div> 	
	   </c:if>
	   
	   
	   
	   
	   
	   
	   
	   
	    <c:if test="${type1==3 && type2==1 }">
	    <div class="youhuiquan">
	     <div class="div1">我的优惠券</div>
		 
		 <ul class="ul1">
		   <li id="xmlistb1" onclick="setTab2('xmlistb',1,2)"  ><a href="javascript:getAll();" onclick="return false" ><span>全网商品</span><span class="shuzhi_l">${cout_all }</span></a></li>
           <li id="xmlistb2" onclick="setTab2('xmlistb',2,2)"><a href="javascript:getAdmit()" onclick="return false"><span>限定商品</span><span class="shuzhi_l">${cout_admit }</span></a></li>
		   <li id="xmlistb3" onclick="setTab2('xmlistb',3,2)"  class="hover"><a href="javascript:getSam()" onclick="return false"><span>样品</span><span class="shuzhi_l">${cout_sam }</span></a></li>
		 </ul>
            <div id="con_xmlistb_1" class="con_all">
			<div class="yh_jieshi">
			<ul>
				<li>仅限支持免费样品的商品，单笔订单最多支持免费十款！24小时内只能使用一次！</li>
			</ul>
			</div>
            <div class="sanlei_n"><a href="javascript:goCoupon(3,1)" class="hover" >可使用</a><a href="javascript:goCoupon(3,2)" >已使用</a><a href="javascript:goCoupon(3,3)">已过期</a></div> 
		 <ul class="ul2">
		   <li class="li21">优惠券号</li>
		   <li class="li22">优惠券名称</li>
		   <li class="li23">有效期</li>
		 </ul>
		 
		 <s:iterator value="couponDetailsSamList" var="samlist">
		 	<ul class="ul3">
		 	 <li class="li31">${samlist.couponNumber }</li>
		 	<c:if test="${samlist.ctCoupon.couponType == '9' }">
		 		<li class="li32"><img src="<%=imgurl %>/bg/yangpin.jpg"/><span>仅限免费商品</span></li>
		 	</c:if>
		   	 <li class="li33">${samlist.ctCoupon.stime }&nbsp至&nbsp${samlist.ctCoupon.etime }</li>

		   <li class="li35"><a href="javascript:goGoodsDetail(${samlist.ctCoupon.GId })" >去采购</a></li>
		 	</ul>
		 </s:iterator>
            </div>
	   </div> 	
	   </c:if>
	   <c:if test="${type1==3 && type2==2 }">
	    <div class="youhuiquan">
	     <div class="div1">我的优惠券</div>
		 
		 <ul class="ul1">
		   <li id="xmlistb1" onclick="setTab2('xmlistb',1,2)"  ><a href="javascript:getAll();" onclick="return false" ><span>全网商品</span><span class="shuzhi_l">${cout_all }</span></a></li>
           <li id="xmlistb2" onclick="setTab2('xmlistb',2,2)"><a href="javascript:getAdmit()" onclick="return false"><span>限定商品</span><span class="shuzhi_l">${cout_admit }</span></a></li>
		   <li id="xmlistb3" onclick="setTab2('xmlistb',3,2)"  class="hover"><a href="javascript:getSam()" onclick="return false"><span>样品</span><span class="shuzhi_l">${cout_sam }</span></a></li>
		 </ul>
            <div id="con_xmlistb_1" class="con_all">
			<div class="yh_jieshi">
			<ul>
				<li>仅限支持免费样品的商品，单笔订单最多支持免费十款！24小时内只能使用一次！</li>
			</ul>
			</div>
			
            <div class="sanlei_n"><a href="javascript:goCoupon(3,1)">可使用</a><a href="javascript:goCoupon(3,2)" class="hover">已使用</a><a href="javascript:goCoupon(3,3)">已过期</a></div> 
		 <ul class="ul2">
		   <li class="li21">优惠券号</li>
		   <li class="li22">优惠券名称</li>
		   <li class="li23">有效期</li>
		 </ul>
		 
		 <s:iterator value="couponDetailsSamList" var="samlist">
		 	<ul class="ul3">
		 	 <li class="li31">${samlist.couponNumber }</li>
		 	<c:if test="${samlist.ctCoupon.couponType == '9' }">
		 		<li class="li32"><img src="<%=imgurl %>/bg/yangpin.jpg"/><span>仅限免费商品</span></li>
		 	</c:if>
		   	 <li class="li33">${samlist.ctCoupon.stime }&nbsp至&nbsp${samlist.ctCoupon.etime }</li>
		   	
<!-- 		   <li class="li35"><a href="javascript:goGoodsDetail(${admitlist.ctCoupon.GId })">去采购</a></li> -->
		 	</ul>
		 </s:iterator>
            </div>
	   </div> 	
	   </c:if>
	   <c:if test="${type1==3 && type2==3 }">
	    <div class="youhuiquan">
	     <div class="div1">我的优惠券</div>
		 
		 <ul class="ul1">
		   <li id="xmlistb1" onclick="setTab2('xmlistb',1,2)"  ><a href="javascript:getAll();" onclick="return false" ><span>全网商品</span><span class="shuzhi_l">${cout_all }</span></a></li>
           <li id="xmlistb2" onclick="setTab2('xmlistb',2,2)" ><a href="javascript:getAdmit()" onclick="return false"><span>限定商品</span><span class="shuzhi_l">${cout_admit }</span></a></li>
		   <li id="xmlistb3" onclick="setTab2('xmlistb',3,2)"  class="hover"><a href="javascript:getSam()" onclick="return false"><span>样品</span><span class="shuzhi_l">${cout_sam }</span></a></li>
		 </ul>
            <div id="con_xmlistb_1" class="con_all">
		<div class="yh_jieshi">
			<ul>
				<li>仅限支持免费样品的商品，单笔订单最多支持免费十款！24小时内只能使用一次！</li>
			</ul>
			</div>
            <div class="sanlei_n"><a href="javascript:goCoupon(3,1)">可使用</a><a href="javascript:goCoupon(3,2)" >已使用</a><a href="javascript:goCoupon(3,3)" class="hover">已过期</a></div> 
		 <ul class="ul2">
		   <li class="li21">优惠券号</li>
		   <li class="li22">优惠券名称</li>
		   <li class="li23">有效期</li>
		 </ul>
		 
		 <s:iterator value="couponDetailsSamList" var="samlist">
		 	<ul class="ul3">
		 	 <li class="li31">${samlist.couponNumber }</li>
		 	<c:if test="${samlist.ctCoupon.couponType == '9' }">
		 		<li class="li32"><img src="<%=imgurl %>/bg/yangpin.jpg"/><span>仅限免费商品</span></li>
		 	</c:if>
		   	 <li class="li33">${samlist.ctCoupon.stime }&nbsp至&nbsp${samlist.ctCoupon.etime }</li>

<!-- 		   <li class="li35"><a href="javascript:goGoodsDetail(${admitlist.ctCoupon.GId })">去采购</a></li> -->
		 	</ul>
		 </s:iterator>
            </div>
	   </div> 	
	   </c:if>
	   
	   
	   
	   
	   
	   
	   
	   
		<!-- 分页控制器 -->
		${pages.pageGoodsStr }
		
	</div>
	 <script type="text/javascript">
	/* 分页跳转控制器 */
	function upAndDownPage(sum){
		//截取访问名
		var url = document.URL.toString();

		var isPage = "";
		if(url.toString().indexOf("&page") > 0){
			url = url.toString().substring(0,url.toString().indexOf("&page"));
		}
		window.location.href= url+"&page=" + sum;
	}
	openBxz();
	</script>
<div class="clear"></div>
</div>
<jsp:include page="../../common/foot.jsp"></jsp:include>   
</body>
</html>

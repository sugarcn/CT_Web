<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../common/basePath.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<link href="css/css.css" type="text/css" rel="stylesheet"/>
<link rel="shortcut icon" href="<%=basePath %>favicon.ico">
<script type="text/javascript" src="js/findpwd.js"></script>
<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
 <script type="text/javascript" src="<%=basePath %>js/login.js"></script>
 <script type="text/javascript" src="<%=basePath %>js/script.js"></script>
<style type="text/css">
body,td,th {
	font-family: SimSun;
}
</style>
</head>

<body>
<div id="popDivall" class="mydivall" style="display:none;">
<div class="con_t"><a href="javascript:closecharDiv()"><img src="<%=imgurl %>/bg/chadiao.gif"></a><span>让客户经理联系我</span></div>
<div class="con_b">
	<ul>
    <li><p>您的支持是我们前进的动力！<br/>
如有问题或建议，请留下您的联系方式和邮箱及问题详情，我们会尽快与您取得联系，谢谢支持。</p></li>
	<li id="ts" style="color:red"></li>
	<li><span>邮箱：</span><input name="con" type="text" id="email"/></li>
    <li><span>手机：</span><input name="con" type="text" id="mbo"/></li>
	<li><span>详细：</span><textarea name="con" id="detail"></textarea></li>
    <li><div class="msg-up" style="display: none;"></div></li>
    <li><a href="javascript:contacts()">提交</a></li>
    </ul>
</div>
</div>
<div id="bgallfk" class="bgallfk" style="display:none;"></div>

<div class="top_a">
	<dl class="ant_all top_ant">	
	<dt><a href="<%=basePath %>">首页</a></dt>
	<dd><a href="<%=path %>/list_help">联系我们</a><img src="<%=imgurl %>/bg/tel_ct.png"></dd>
	</dl>
<div class="clear"></div>
</div>

<div class="logo_a">
  <div class="ant_all logo_ant">	
	<div class="logo_left"><img src="<%=imgurl %>/bg/logo_a.gif"></div>
	<div class="zhuce_mid"><span>找回密码</span></div>
	</div>
<div class="clear"></div>
</div>
<s:form method="post" action="login_updatePasswd" id="form">
<div class="contant_a">
    <input type="hidden" value="${ctUser.UUserid}" name="ctUser.UUserid"/>
	<input type="hidden" value="${ctUser.UMb}" name="ctUser.UMb" id="UMb2"/>
	<div class="zhmmzh"><p>您正在找回密码的账号为<span>${ctUser.UUserid}</span>，<i><a href="javascript:goForgetPwd()">换一个账号</a></i></p></div>
	   
	   <ul class="zhmm">
	     <li>1.输入账号</li>
	     <li>2.选择找回方式</li>
	     <li>3.进行安全验证</li>
	     <li class="zhmmzt">4.设置新密码</li>
	   </ul>
	   
	   <div class="pir_right">
	<div id="con_xmlistb_1" class="con_all">
	<ul class="shoujizhuce">
          <li class="mi1"><span class="hao_name">密　　码：</span><input id="passwd" name="ctUser.UPassword" onblur="checkPasswd();" type="password"/>
          	<span  style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="passwdtip"></span>
          </li>
		  <li class="mi2"><span class="hao_name">确认密码：</span><input id="passwd2" name="passwd2" onblur="checkPasswd2();" type="password"/>
		  	<span  style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="passwdtip2"></span>
		  </li>
          <li class="zhucean"><p><a onclick="javascript:updatePasswd()">完成</a></p></li> 
    </ul>
	</div>
	</div>
	
	<div class="lxkf4">
	  <p>无法通过上面的方法找回密码？</p>
	  <p>您可以进行<span class="lianjingli"><a href="javascript:showcharDiv()">让客户经理联系我</a></span>，客服在核实您的身份后可为您重置密码。</p>
	</div>
	
<div class="clear"></div>
</div>
</s:form>
<div class="clear"></div>
</div>
<jsp:include page="../common/foot.jsp"></jsp:include>
</body>
</html>

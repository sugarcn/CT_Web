﻿<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="../common/basePath.jsp"%>

<!DOCTYPE html>
<html lang="en">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>注册成功-长亭易购</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no" />
<meta name="description" content="">
<meta name="author" content="">
<script src="../js/jquery.js"></script>
<script src="../js/jquery-2.1.0.min.js"></script>
<SCRIPT LANGUAGE="JavaScript">
function copyToClipBoard(id){ 
$("#"+id).text().clone();  
alert("复制成功！");     
}
</SCRIPT>

<script> 


$("#closeBtn").click(function(){
var userAgent = navigator.userAgent;
if (userAgent.indexOf("Firefox") != -1 || userAgent.indexOf("Chrome") !=-1) {
   window.location.href="about:blank";
} else {
   window.opener = null;
   window.open("", "_self");
   window.close();
}});


    </script>  
<!-- CSS -->

<link rel="stylesheet" href="../css/wxstyle.css">


</head>

<body>

        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="logo span4">
                        <h1><img src="<%=imgurl %>/ctegosjzc.png"></h1>
                    </div>
                    
                </div>
            </div>
        </div>


        <div class="register-container container" id="container">
            <div class="row" style="min-height:540px;">
                
                <div class="register span6">
                    <form id="form1">
                        <h2><span class="red"><strong>注册成功！</strong></span></h2>
                        
<p class="regtips">您的易购唯一码为：<span id="code1" class="tipsred">ggg${wym}</span>    <input id="closeBtn" type="button" value="一键复制"/>  请复制并发送唯一码至长亭易购公众号领取微信红包！</p>
<p class="regtips">千元满额红包已发送至您账户，请登录长亭易购官网查看，本月支付每满100元，另可获10元微信红包，每满1000元可获100元微信红包，依次类推，上不封顶！</p>

<p class="regtips">感谢您对长亭易购的信任与支持！<a href="javascript:window.history.go(-4);">去公众号粘贴</a> <a href="javascript:window.history.go(-4);">关闭</a>  </p>

                    </form>
                </div>
            </div>
        </div>
<div class="footer">长亭易购 版权所有<br/>©2014-2015 CTEGO CORPORATION All Rights Reserved.</div>
        <!-- Javascript -->
</body>
</html>

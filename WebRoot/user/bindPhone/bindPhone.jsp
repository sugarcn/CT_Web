<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<link href="css/css.css" type="text/css" rel="stylesheet" />
<link href="<%=request.getContextPath() %>/css/css.css" type="text/css" rel="stylesheet"/>
<link href="<%=request.getContextPath() %>/css/css2.css" type="text/css" rel="stylesheet"/>
<link href="<%=request.getContextPath() %>/css/css3.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jqueryt.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/lrtk.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/spxx.js"></script>
<link href="<%=request.getContextPath() %>/css/style.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/script.js"></script>
<script type="text/javascript" src="js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="js/user.js" charset="utf-8"></script>
<script type="text/javascript">
$(function(){ 

	$("#searchSelected").click(function(){ 
		$("#searchTab").show();
		$(this).addClass("searchOpen");
	}); 

	$("#searchTab li").hover(function(){
		$(this).addClass("selected");
	},function(){
		$(this).removeClass("selected");
	});
	 
	$("#searchTab li").click(function(){
		$("#searchSelected").html($(this).html());
		$("#searchTab").hide();
		$("#searchSelected").removeClass("searchOpen");
	});
});
</script>
</head>

<body>
<jsp:include page="../../common/head.jsp"></jsp:include>
<div class="contant_e">
    <div class="dizhilian_e"><a href="<%=basePath%>" >首页</a>><a href="<%=basePath%>user_userinfo">个人中心</a>><a href="<%=basePath%>security_goSecurity">安全设置</a>><span>验证手机</span></div>

    <jsp:include page="../../common/left.jsp"></jsp:include>	
	<div class="right_m">
	  <div class="aqshzh">
	    <div>验证手机</div>
		
		<ul class="yzhshj1">
			<li class="yzhshjzt1">1.验证手机</li>
			<li class="yzhshjzt2">2.完成</li>
		</ul>
<s:form action="security_saveBindPhone" id="form" method="post">
		<ul class="yzhshj2">
			<li class="wdshj"><span>我的手机：</span><span><input id="UMb2" name="ctUser.UMb" onblur="checkPhone();" type="text"/></span>
			<span  style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="umbtip"></span>
			</li>
			<li class="shjjym"><span>手机校验码：</span><input type="text" id="sms" disabled="disabled" onblur="checkUmbYzmIsEqual();"/><input type="button" id="sendsms" maxlength="4"  value="　  获取验证码" onclick="getPhoneYzm(this)" class="sjzc_ct" style="color: #050000; font-size:12px; height: 28px; line-height: 28px; width: 110px; border:1px solid #da082f; cursor:pointer; margin-left: 20px;" />
			<span  style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="smstip"></span>
			</li>
			<li class="yzhm"><span>验证码：</span><input id="yzm" name="yzm" onblur="checkYzm();" type="text" /><img id="realyzm"
					 		src="<..k%=basePath%>common/image.jsp"/><a href="javascript:showYzm()">看不清，换一张</a>
			<span  style="color: red;\-kd padding-left:18px; text-align:left; font-size:12px;" id="yzmtip"></span>		 		
		    </liz>
			<li class="tijiao"><p><a href="javascript:saveBindPhone();" >提交</a></p></li> 
		</ul>
</s:form>		
	  </div>
	</div>

<div class="clear"></div>
</div>
<script type="text/javascript">
showYzm();
</script>
<jsp:include page="../../common/foot.jsp"></jsp:include>	
</body>
</html>

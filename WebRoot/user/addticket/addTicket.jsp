<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../../common/basePath.jsp"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>

<script type="text/javascript" src="<%=basePath%>/js/jquery-latest.pack.js" ></script>
<link rel="stylesheet" type="text/css" href="<%=basePath%>/js/style.css" />
		<script type="text/javascript" src="<%=basePath%>/js/jquery.min.js"></script>
		<script type="text/javascript" src="<%=basePath%>/js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<%=basePath%>/js/styles.css" />
        <link rel="stylesheet" type="text/css" href="<%=basePath%>/js/jquery.confirm.css" />
       <script type="text/javascript" src="<%=basePath%>/plugins/datepicker/date/WdatePicker.js"></script>
       <script type="text/javascript" src="<%=basePath%>/js/user.js" charset="utf-8"></script>
        <link rel="stylesheet" href="<%=basePath%>/css/mycss.css" type="text/css"/>
<script type="text/javascript">
$(function(){ 

	$("#searchSelected").click(function(){ 
		$("#searchTab").show();
		$(this).addClass("searchOpen");
	}); 

	$("#searchTab li").hover(function(){
		$(this).addClass("selected");
	},function(){
		$(this).removeClass("selected");
	});
	 
	$("#searchTab li").click(function(){
		$("#searchSelected").html($(this).html());
		$("#searchTab").hide();
		$("#searchSelected").removeClass("searchOpen");
	});
});
</script>
</head>

<body>
<jsp:include page="../../common/head.jsp"></jsp:include>
<div class="contant_e">

    <div class="dizhilian_e"><a href="<%=basePath %>" >首页</a>><a href="<%=basePath%>user_userinfo">个人中心</a>><span>增票信息设置</span></div>
    
<jsp:include page="../../common/left.jsp"></jsp:include>
    <s:form action="fileUpload" id="addticketform1" method="post" enctype="multipart/form-data">
    <input type="hidden" id="UId" name="ctUser.UId"/></td>
	<div class="right_k">
	   <div class="zpshzh">
		  <div>增票设置</div>
		  <ul class="ul1">
		    <li class="li11">温馨提示</li>
		    <li class="li12">如您是首次开具增值税专用发票，请您至 "<span class="s1">个人中心->增票资质</span>"中填写纳税人识别号等开票</li>
			<li class="li12">信息，并上传加盖公章的营业执照副本、税务登记证副本、一般纳税人资格证书及银行开户 许可证扫描</li>
			<li class="li12">件至长亭，收到您的开票资料后，我们会尽快审核。</li>
			<li class="li12">注意：有效增值税发票开票资质仅为一个。</li>
		  </ul> 
		  <ul class="ul2">
		    <li class="li22"><span class="zpbiao">单位名称：</span><input id="companyName" name="addTicket.companyName" type="text"/>
		    <span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="companyNameTip"></span>
		    </li>
			<li class="li22"><span class="zpbiao">纳税人识别名：</span><input id="companySn" name="addTicket.companySn" type="text"/>
			<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="companySnTip"></span>
			</li>
			<li class="li22"><span class="zpbiao">注册地址：</span><input id="registeAddress" name="addTicket.registeAddress" type="text"/>
			<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="registeAddressTip"></span>
			</li>
			<li class="li22"><span class="zpbiao">注册电话：</span><input id="registeTel" name="addTicket.registeTel" type="text"/>
			<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="registeTelTip"></span>
			</li>
			<li class="li22"><span class="zpbiao">开户银行：</span><input id="depositBank" name="addTicket.depositBank" type="text"/>
			<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="depositBankTip"></span>
			</li>
			<li class="li22"><span class="zpbiao">银行账户：</span><input id="bankAccount" name="addTicket.bankAccount" type="text"/>
			<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="bankAccountTip"></span>
			</li>
			<li class="li22 liwuk"><span class="zpbiao">营业执照副本：</span><input type="file" name="file" id="file1"/>
			<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="file1Tip"></span>
			</li>
			<li class="li22 liwuk"><span class="zpbiao">税务登记证副本：</span><input type="file" name="file" id="file2"/>
		 	<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="file2Tip"></span>
			</li>
			<li class="li22 liwuk"><span class="zpbiao">一般纳税人资格证书：</span><input type="file" name="file" id="file3"/>
			<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="file3Tip"></span>
			</li>
			<li class="li22 liwuk"><span class="zpbiao">银行开户许可证：</span><input type="file" name="file" id="file4"/>
			<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="file4Tip"></span>
			</li>
			<li class="li23"><a href="javascript:addTicket1()">保  存</a></li>
		  </ul>  
	   </div>
	
	</div>
</s:form>
<div class="clear"></div>

 


</div>
<jsp:include page="../../common/foot.jsp"></jsp:include>
</body>
</html>

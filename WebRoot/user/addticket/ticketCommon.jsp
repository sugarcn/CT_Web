<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../common/basePath.jsp"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>

<script type="text/javascript" src="<%=basePath%>js/jquery-1.8.3.min.js"></script>
<script type="text/javascript"	src="<%=basePath%>plugins/datepicker/date/WdatePicker.js"></script>
<script type="text/javascript" src="<%=basePath%>js/user.js"	charset="utf-8"></script>

<script type="text/javascript">
	$('#btn_bxz').live('click', function(){
		$('.PKchuang').hide();
		$('.BMchuangxz').show();
		$('.BMchuangxzer').hide();
	});
	$('.lchaxz').live('click', function(){
		$('.PKchuang').hide();
		$('.BMchuangxz').hide();
	});

</script>
<style type="text/css">
#BgDiv {
	background-color: #000;
	position: absolute;
	z-index: 99;
	left: 0;
	top: 0;
	display: none;
	width: 100%;
	height: 2000px;
	opacity: 0.4;
	filter: alpha(opacity =   40);
	-moz-opacity: 0.4;
}
</style>
</head>

<body>
	<div id="BgDiv"></div>
	<jsp:include page="../../common/head.jsp"></jsp:include>
	<div id="dele" class="BMchuang BMchuangsmall " style="top:1250px; left:400px; z-index:888; display:none;" >
	<div class="lcha" id="btnClose" onclick="closeDivZeng('dele')"><span><img src="<%=imgurl %>/bg/chadiao.gif" /></span></div>
    <div class="tizp_de">
        <p>是否删除？1</p>
    
        <div class="jiarugoux_dd">
        <input type="hidden" id="delId" />
        <span class="jiaruche_d"><a id="ticketdel" href="javascript:delTicekt()">确定</a></span>
        <span class="lcha"><a class="jiaruche_d1" id="btnClose" onclick="closeDivZeng('dele')">取消</a></span>
        </div>
    </div>
	</div>
	<div class="contant_e">

		<div class="dizhilian_e">
			<a href="<%=basePath%>" >首页</a>><a href="<%=basePath%>user_userinfo">个人中心</a>><span>增票信息设置</span>
		</div>

		<jsp:include page="../../common/left.jsp"></jsp:include>
		
		<div class="right_k">
			<div class="shouhuodizhi_j">
				<div class="div1">增票设置</div>
				<ul class="ul1">
					<li class="li11">温馨提示</li>
					<li class="li12">如您是首次开具增值税专用发票，请您至 "<span class="s1">会员中心->个人中心->增票资质</span>"中填写纳税人识别号等开票信息，并上传加盖公章的营业执照副本、税务登记证副本、一般纳税人资格证书及银行开户
						许可证扫描件至长亭，收到您的开票资料后，我们会尽快审核。</li>
				</ul>

				<ul class="ul1_jj">
					<li class="li2_j"><a href="javascript:;" id="btn_bxz">+新增增票设置</a>
					</li>
				</ul>
				<!-- 申请增票 -->
				<form action="fileUpload" id="addticketform" method="post" enctype="multipart/form-data">
				<div class="BMchuangxz" style="display:none;">
					<div class="xzshhdz">
						<div class="lchaxz">
							<span><img src="<%=imgurl %>/bg/chadiao.gif" /> </span>
						</div>
						<ul class="ul2">
							<li class="li22"><span class="zpbiao">单位名称：</span><input
								id="companyName" maxlength="100" name="addTicket.companyName" type="text" /> <span
								style="color: red; padding-left:18px; text-align:left; font-size:12px;"
								id="companyNameTip"></span>
							</li>
							<li class="li22"><span class="zpbiao">纳税人识别号：</span><input
								id="companySn" maxlength="50" name="addTicket.companySn" type="text" /> <span
								style="color: red; padding-left:18px; text-align:left; font-size:12px;"
								id="companySnTip"></span>
							</li>
							<li class="li22"><span class="zpbiao">注册地址：</span><input
								id="registeAddress" maxlength="500" name="addTicket.registeAddress" type="text" />
								<span
								style="color: red; padding-left:18px; text-align:left; font-size:12px;"
								id="registeAddressTip"></span>
							</li>
							<li class="li22"><span class="zpbiao">座机号码：</span><input
								id="registeTel" maxlength="20" name="addTicket.registeTel" type="text" /> <span
								style="color: red; padding-left:18px; text-align:left; font-size:12px;"
								id="registeTelTip"></span>
							</li>
							<li class="li22"><span class="zpbiao">开户银行：</span><input
								id="depositBank" maxlength="100" name="addTicket.depositBank" type="text" /> <span
								style="color: red; padding-left:18px; text-align:left; font-size:12px;"
								id="depositBankTip"></span>
							</li>
							<li class="li22"><span class="zpbiao">银行账户：</span><input
								id="bankAccount" maxlength="100" name="addTicket.bankAccount" type="text" /> <span
								style="color: red; padding-left:18px; text-align:left; font-size:12px;"
								id="bankAccountTip"></span>
							</li>
							<li class="li22 liwuk"><span class="zpbiao">营业执照副本：</span><input
								type="file" name="addTicket.fileNames" id="file1" onchange="getlisenceName(this)" /> <span
								style="color: red; padding-left:18px; text-align:left; font-size:12px;"
								id="file1Tip"></span>
								<input name="addTicket.lisenceName" type="hidden" id="lisenceName" />
							</li>
							<li class="li22 liwuk"><span class="zpbiao">税务登记证副本：</span><input
								type="file" name="addTicket.fileNames" id="file2" onchange="gettaxCertifiName(this)"  /> <span
								style="color: red; padding-left:18px; text-align:left; font-size:12px;"
								id="file2Tip"></span>
								<input name="addTicket.taxCertifiName" type="hidden" id="taxCertifiName" />
							</li>
							<li class="li22 liwuk"><span class="zpbiao">一般纳税人资格证书：</span><input
								type="file" name="addTicket.fileNames" id="file3" onchange="getgeneralCertifiName(this)"  /> <span
								style="color: red; padding-left:18px; text-align:left; font-size:12px;"
								id="file3Tip"></span>
								<input name="addTicket.generalCertifiName" type="hidden" id="generalCertifiName" />
							</li>
							<li class="li22 liwuk"><span class="zpbiao">银行开户许可证：</span><input
								type="file" name="addTicket.fileNames" id="file4" onchange="getbankLisenceName(this)"  /> <span
								style="color: red; padding-left:18px; text-align:left; font-size:12px;"
								id="file4Tip"></span>
								<input name="addTicket.bankLisenceName" type="hidden" id="bankLisenceName" />
							</li>
							<li class="li23"><a href="javascript:addTicket()">保 存</a></li>
						</ul>
					</div>
				</div>
			 </form>
			 <!-- 修改增票 -->
			 <s:form action="ticket_update" id="updateTicketform" method="post">
				<div class="BMchuangxzer" style="display:none;">
					<div class="xzshhdz">
						<div class="lchaxz">
							<span><img src="<%=imgurl %>/bg/chadiao.gif" /> </span>
						</div>
						<input type="hidden" name="ATId" id="AId"/>
						<input type="hidden" name="UId" id="UId"/>
						<ul class="ul2">
							<li class="li22"><span class="zpbiao">单位名称：</span><input
								id="companyName1" name="companyName" type="text" /> <span
								style="color: red; padding-left:18px; text-align:left; font-size:12px;"
								id="companyNameTip1"></span>
							</li>
							<li class="li22"><span class="zpbiao">纳税人识别号：</span><input
								id="companySn1" name="companySn" type="text" /> <span
								style="color: red; padding-left:18px; text-align:left; font-size:12px;"
								id="companySnTip1"></span>
							</li>
							<li class="li22"><span class="zpbiao">注册地址：</span><input
								id="registeAddress1" name="registeAddress" type="text" />
								<span
								style="color: red; padding-left:18px; text-align:left; font-size:12px;"
								id="registeAddressTip1"></span>
							</li>
							<li class="li22"><span class="zpbiao">座机号码：</span><input
								id="registeTel1" name="registeTel" type="text" /> <span
								style="color: red; padding-left:18px; text-align:left; font-size:12px;"
								id="registeTelTip1"></span>
							</li>
							<li class="li22"><span class="zpbiao">开户银行：</span><input
								id="depositBank1" name="depositBank" type="text" /> <span
								style="color: red; padding-left:18px; text-align:left; font-size:12px;"
								id="depositBankTip1"></span>
							</li>
							<li class="li22"><span class="zpbiao">银行账户：</span><input
								id="bankAccount1" name="bankAccount" type="text" /> <span
								style="color: red; padding-left:18px; text-align:left; font-size:12px;"
								id="bankAccountTip1"></span>
							</li>
							<li class="li23"><a href="javascript:updateTicket()">修 改</a></li>
						</ul>
					</div>
				</div>
				</s:form>
				
				<!-- 显示增票信息 -->
				<s:iterator value="addTickets" var="list">
				<!-- 判断是否是默认地址 -->
				<s:if test="#list.theDefault == 0">
				<ul class="ul1_j">
					<li class="li1_j">
						<div class="div2">
							<span class="zp_name">单位名称：</span><span class="span1">${list.companyName }</span>	
						</div>
						
						<div class="div3">默认增票信息</div>
						
						<div class="div4">
							<a href="javascript:delAddTicket(${list.ATId})">删除</a><span class="span2">|</span>
							<a href="javascript:updateAddTicket(${list.ATId });" id="btn_bxzer" >修改</a><span class="span2">|</span>
						</div>
					</li>
					<li><span class="zp_name">纳税人识别号：</span>${list.companySn }</li>
					<li><span class="zp_name">注册地址：</span>${list.registeAddress }</li>
					<li><span class="zp_name">注册电话：</span>${list.registeTel }</li>
					<li><span class="zp_name">开户银行：</span>${list.depositBank }</li>
					<li><span class="zp_name">银行账户：</span>${list.bankAccount }</li>
					<li>
						<dl class="zp_tp">
							<dt>
								营业执照副本：
								</dt>
									<dd>
										<a href="/" target="_blank"><img src="<%=imgurl %>/01.jpg">
										</a>
									</dd>
						</dl>
						<dl class="zp_tp">
							<dt>
								税务登记证副本：
								</dt>
									<dd>
										<a href="/" target="_blank"><img src="<%=imgurl %>/01.jpg">
										</a>
									</dd>
						</dl>
						<dl class="zp_tp">
							<dt>
								一般纳税人资格证书：
								</dt>
									<dd>
										<a href="/" target="_blank"><img src="<%=imgurl %>/01.jpg">
										</a>
									</dd>
						</dl>
						<dl class="zp_tp">
							<dt>
								银行开户许可证：
							</dt>
									<dd>
										<a href="/" target="_blank"><img src="<%=imgurl %>/01.jpg">
										</a>
									</dd>
						</dl>
					</li>
				</ul>
				</s:if>
				
				<s:if test="#list.theDefault == 1">
				<ul class="ul1_j">
					<li class="li1_j">
						<div class="div2">
							<span class="zp_name">单位名称：</span><span class="span1">${list.companyName }</span>	
						</div>
						<div class="div4">
							<a href="javascript:delAddTicket(${list.ATId })">删除</a><span class="span2">|</span>
							<a href="javascript:updateAddTicket(${list.ATId });" id="btn_bxzer">修改</a><span class="span2">|</span>
							<a href="javascript:setTehDeafult(${list.ATId })" >设为默认</a>
						</div>
					</li>
					<li><span class="zp_name">纳税人识别号：</span>${list.companySn }</li>
					<li><span class="zp_name">注册地址：</span>${list.registeAddress }</li>
					<li><span class="zp_name">注册电话：</span>${list.registeTel }</li>
					<li><span class="zp_name">开户银行：</span>${list.depositBank }</li>
					<li><span class="zp_name">银行账户：</span>${list.bankAccount }</li>
					<li>
						<dl class="zp_tp">
							<dt>
								营业执照副本：
								</dt>
									<dd>
										<a href="/" target="_blank"><img src="<%=imgurl %>/01.jpg">
										</a>
									</dd>
						</dl>
						<dl class="zp_tp">
							<dt>
								税务登记证副本：
								</dt>
									<dd>
										<a href="/" target="_blank"><img src="<%=imgurl %>/01.jpg">
										</a>
									</dd>
						</dl>
						<dl class="zp_tp">
							<dt>
								一般纳税人资格证书：
								</dt>
									<dd>
										<a href="/" target="_blank"><img src="<%=imgurl %>/01.jpg">
										</a>
									</dd>
						</dl>
						<dl class="zp_tp">
							<dt>
								银行开户许可证：
							</dt>
									<dd>
										<a href="/" target="_blank"><img src="<%=imgurl %>/01.jpg">
										</a>
									</dd>
						</dl>
					</li>
				</ul>
				</s:if>
				</s:iterator>
			</div>
		 
			
		</div>
		

	<div class="clear"></div>
	</div>
	<jsp:include page="../../common/foot.jsp"></jsp:include>
</body>
</html>

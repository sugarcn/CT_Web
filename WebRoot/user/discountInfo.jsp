<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../common/basePath.jsp"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<link href="css/css.css" type="text/css" rel="stylesheet"/>
<link href="css/css2.css" type="text/css" rel="stylesheet"/>
<link href="css/css3.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="js/jqueryt.js"></script>
<script type="text/javascript" src="js/lrtk.js"></script>
<link type="text/css" rel="stylesheet" href="css/style.css" />

<script src="js/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>
</head>

<body>

<jsp:include page="../common/head.jsp"></jsp:include>
	<div class="contant_e">
		<div class="dizhilian_e"><a href="<%=basePath%>">首页</a>><span>个人中心</span></div>
		<jsp:include page="../common/left.jsp"></jsp:include>
		
		
	<!-- 开始 -->	
		 <div id="content">
        <div class="img"></div>
        <p class="invite">您的邀请码是：<span id="tuiguangma"></span></p>
        <div class="Acontent">
            <b></b>
            <p>一、2016年7月26日开始，具体结束时间另行公布。</p>
            <p>二、注册了长亭易购的用户均可获取活动资格。</p>
            <p>三、被邀请的好友注册成功后，邀请人可以获得10元无限制优惠券，可用于平台任意商品。</p>
            <p>四、活动中如果发现通过作弊手段获得奖励的，将取消奖励。</p>
            <p>五、活动最终解释权归长亭易购所有。</p>
        </div>

        <div class="details">
            <p>
                <b></b>
                <span>账 户 明 细</span>
            </p>
            <table>
                <thead>
                    <tr>
                        <td>日期</td>
                        <td>优惠券</td>
                        <td>说明</td>
                    </tr>
                </thead>
                <tbody>
                <c:forEach items="${extensions }" var="list">
                     <tr>
                         <td>${list.extensionTime }</td>
                         <td>
                             <div>
                                 <p class="p1">&yen;<span>${list.extensionMoney }</span></p>
                                 <p class="p2"><b></b></p>
                                 <a href="#">立即使用</a>
                             </div>
                         </td>
                         <td>邀请用户${list.extensionDesc }</td>
                     </tr>
				</c:forEach>
                </tbody>
            </table>
        </div>
        <div class="activety">
            <p>
                <b></b>
                <span>活 动 参 与</span>
            </p>
            <div class="act">
                <p>复制分享以下链接，即可参与活动：</p>
                <div>
                    <p>立即注册长亭易购，还可以【免费】得到10元优惠券，机会难得，赶紧看看啊！<a href="javascript:;" id="fe_text">${tuigangUrl }</a></p>
                    <button id="d_clip_button" class="my_clip_button" data-clipboard-target="fe_text"></button>
                </div>
                <p class="bot"></p>
            </div>
        </div>
    </div>
		
		<!-- 结束 -->
		
		
		
		
		
	</div>
	<input type="hidden" id="urlTuiGuang" value="${tuigangUrl }" />
<!--[if IE 8]>
<script type="text/javascript">
/*function loadTuiGuangma(){
	var tuiguang = $("#urlTuiGuang").val();
	var chai = tuiguang.split("=");
	$("#tuiguangma").text(chai[1]);
}
loadTuiGuangma();
$(function(){
	  $("#d_clip_button").click(function(){
	    var Url=$("#fe_text").text();
	    copyToClipboard(Url);
	 });
	});
function copyToClipboard(maintext){
	  if (window.clipboardData){
	    window.clipboardData.setData("Text", maintext);
	    }else if (window.netscape){
	      try{
	        netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
	    }catch(e){
	        alert("该浏览器不支持一键复制！请手工复制文本框链接地址～");
	    }

	    var clip = Components.classes['@mozilla.org/widget/clipboard;1'].createInstance(Components.interfaces.nsIClipboard);
	    if (!clip) return;
	    var trans = Components.classes['@mozilla.org/widget/transferable;1'].createInstance(Components.interfaces.nsITransferable);
	    if (!trans) return;
	    trans.addDataFlavor('text/unicode');
	    var str = new Object();
	    var len = new Object();
	    var str = Components.classes["@mozilla.org/supports-string;1"].createInstance(Components.interfaces.nsISupportsString);
	    var copytext=maintext;
	    str.data=copytext;
	    trans.setTransferData("text/unicode",str,copytext.length*2);
	    var clipid=Components.interfaces.nsIClipboard;
	    if (!clip) return false;
	    clip.setData(trans,null,clipid.kGlobalClipboard);
	  }
	  alert("已经成功复制优惠链接");
	}*/
	d_clip_button.onmouseover=function(){
        d_clip_button.style.background="url('https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/img/icon.png') no-repeat -107px -46px";
    }
    d_clip_button.onmouseout=function(){
        d_clip_button.style.background="url('https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/img/icon.png') no-repeat -107px 0";
    }
    d_clip_button.onclick=function(){
    	alert("该浏览器不支持一键复制！请手工复制优惠链接～");
    }
</script>
<![endif]-->

<script type="text/javascript" src="js/ZeroClipboard.js"></script>
<script src="js/zhuce.js"></script>
<jsp:include page="../common/foot.jsp"></jsp:include>

</body>
</html>

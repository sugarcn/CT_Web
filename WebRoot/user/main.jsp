<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="../common/basePath.jsp"%>





<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta name="Keywords" content="注册成功！<%=key%>" />
<meta name="Description" content="<%=title%>" />
<title><%=key%><%=title%></title>

<title>注册登录弹</title>
<link href="css/css.css" type="text/css" rel="stylesheet"/>
<link href="css/css2.css" type="text/css" rel="stylesheet"/>
<link rel="shortcut icon" href="<%=basePath %>favicon.ico">
<script> 
var t=60;//设定跳转的时间 
setInterval("refer()",1000); //启动1秒定时 
function refer(){ 
if(t == 0){ 
//if(false){ 
location="<%=basePath %>"; //#设定跳转的链接地址 
} 
document.getElementById('show').innerHTML=""+t+"秒后自动进入网站首页。"; // 显示倒计时 
t--; // 计数器递减 
} 
//跳转到我的优惠券
function goCoupon(type1,type2){
		window.location.href="coupon_list?coupondetailDTO.type1="+type1+"&coupondetailDTO.type2="+type2;
}
</script> 
<style>
    .coupon{
        width:400px;
        height:100px;
        margin:0 auto;
        overflow:hidden;
        position:relative;
    }
    .coupon p{
        width:100%;
        text-align:center;
        font-size:24px;
        color:#ff8900;
        font-weigth:bold;
    }
    .coupon a{
        float:left;
        width:100px;
        height:30px;
        background:#ff0066;
        color:#fff;
        text-align:center;
        line-height:30px;
        border-radius:5px;
        position:absolute;
        bottom:20px;
        left:50%;
        margin-left:-60px;
        cursor:pointer;
        font-size:14px;
        font-weight:bold;
    }
</style>
</head>

<body>

<div class="top_a">
	<dl class="ant_all top_ant">	
	<dt><a href="<%=basePath %>">首页</a></dt>
	<dd><a href="<%=basePath %>list_help_desc?help.HId=23">联系我们</a><img src="<%=imgurl %>/bg/tel_ct.png"></dd>
	</dl>
<div class="clear"></div>
</div>

<div class="logo_a">

  <div class="ant_all logo_ant">	
	<div class="logo_left"><a href="<%=basePath %>"><img src="<%=imgurl %>/bg/logo_a.gif"></a></div>
	<div class="zhuce_mid"><span>注册成功！</span></div>
	</div>
<div class="clear"></div>
</div>

<div class="contant_a" style="height:600px;">

    <dl class="dlzc_tan_d">
   <dt>注册成功！</dt>
   <!-- 
   <div class="coupon">
	    <p>小主，优惠券君已飞到您的账户啦!!</p>
	    <a href="coupon_list?coupondetailDTO.type1=1&coupondetailDTO.type2=1">点击查看</a>
   </div>
   -->
   <dd class="shijian_d"><i id="show"> </i></dd>
   <!-- 
   <dd class="ct_gd">已成功赠送您价值<i>10</i>元无门槛优惠券！<a href="coupon_list?type1=1&type2=1">[ 更多 ]</a></dd>
    -->
   <dd><span class="ct_wzsy"><a href="<%=basePath %>" class="shouye_d">网站首页</a></span><span><a href="<%=basePath %>user_userinfo" class="zhongxin_d">进入个人中心</a></span></dd>
   <!--<dd class="ct_jr"><a href="<%=basePath %>sx.jsp"><img src="<%=imgurl %>/zc_gao.jpg"></a></dd>-->
    </dl>

<div class="clear"></div>
</div>


<div class="clear"></div>
<jsp:include page="../common/foot.jsp"></jsp:include>
</body>
</html>
﻿<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="../common/basePath.jsp"%>


<html>
<head>
	<meta charset="utf-8">
	<title>长亭易购</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no" />
<link rel="stylesheet" href="css/wxstyle.css">
<script src="js/jquery-2.1.0.min.js"></script>
	<script src="js/jquery.validate.js"></script>



<script>

	var yanzhengOk = true;
	var wait=60; 
function time(o) { 
	if (wait == 0) {
		o.removeAttribute("disabled");	
		//("#spanid").html("csjwang");
		$("#fsyzm").html("发送手机验证码"); 
		$("#fsyzm").attr("onclick", "getPhoneYzm(this)"); 
		//o.value="　  获取验证码"; 
		wait = 60; 
	} else { 
		//o.setAttribute("disabled", true);
		$("#fsyzm").attr("onclick", ""); 
		$("#fsyzm").html("　  重新发送" + wait); 
		//("#fsyzm").innerHTML="重新发送(" + wait + ")"; 
		//o.value="　  重新发送(" + wait + ")";
		wait--; 
		setTimeout(function() { 
		time(o);
	}, 
	1000) 
	} 
} 
var huo = 300;
function timeOutYzm(o) { 
	if (huo == 0) { 
		huo = 300; 
		guoqi();
		checkUMbYzmIsEqual2("验证码超时，请重新输入");
		$("#sms").val("");
	} else { 
		huo--; 
		setTimeout(function() { 
		timeOutYzm(o);
	}, 
	1000) 
	} 
}
	function getPhoneYzm(o){
		if(yanzhengOk){
			var phoneNum = $("#UMb").val();
			if(phoneNum=="" || phoneNum == "请输入手机号码"){
				$("#UMb").val("请输入手机号码");
				isGetYzm = false;
				
			}else{
				$.post("login_savePhoneYzm",{"ctSms.sms":"1111","ctUser.UMb":phoneNum});
				isGetYzm = true;
				//$("#sms").removeAttr("disabled");
				time(o);
				timeOutYzm(o);
				huo = 300;
			}
			
			 
			isGuo = true;
		} else {
		//	$("#yzmtip1").text("请输入验证码");
		}
	}

	//验证手机号码是否合法
	function checkPhone(){
		flag = true;
		var phoneNum = $("#UMb").val();
		$("#umbtip").text("");
		if (phoneNum !=""){
			if(!(/^1[3|5|7|8|9][0-9]\d{8}$/.test(phoneNum))){
				//$("#umbtip").text("");
				$("#umbtip").text("手机号不合法");
				$("#fsyzm").attr("onclick", ""); 
				flag = false;
			}else{
				
				$.ajaxSetup({async:false});
				$.post("login_checkPhone",{"ctUser.UMb":phoneNum},function(data){
					if(data=="success"){
						$("#umbtip").text("");
					}else {
						//$("#umbtip").text("");
						$("#umbtip").text("手机号已注册");
						$("#fsyzm").attr("onclick", ""); 
						flag = false;
	//					$("#umb").val("");
					}
				});
			}
			if (flag==true){
				$("#fsyzm").attr("onclick", "getPhoneYzm(this)"); 
				$("#fsyzm").removeClass().addClass("yzmclass"); 
			}
		}

		return flag;
	}	//验证手机号码,获取手机验证码并保存验证
	
		//检测手机验证码是否正确
	function checkUmbYzmIsEqual(){

		var yzm = $("#yzm").val();
		if(yzm.length > 0){
			 checkUmbYzmIsEqual2("验证码错误");
		}
	}
	function checkUmbYzmIsEqual2(str) {
		flag = true;
		var phoneNum = $("#UMb").val();
		var yzm = $("#yzm").val();
		$("#smstip").text("");
		$.ajaxSetup({async:false});
		$.post("login_checkYzmIsEqual",{"ctSms.sms":yzm,"ctUser.UMb":phoneNum},function(data){
			if(data=="success"){
				//$("#wxsub").attr("disabled","false");
				//$("#wxsub").removeattr("disabled");
				$("#wxsub").prop("disabled",false);
			}else {
				$("#smstip").text(str);
				$("#wxsub").attr("disabled","disabled");
				flag = false;
//				$("#sms").val("");
			}
		});
		return flag;
	}
</script>	

</head>
<body>

<div id="page1" data-role="page">
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="logo span4">
                        <h1><img src="<%=imgurl %>/ctegosjzc.png"></h1>
                    </div>
                    
                </div>
            </div>
        </div>
		
		
		
        <div class="register-container container" id="container">
            <div class="row">
                
                <div class="register span6">
		
		

<form class="cmxform" id="commentForm" method="post" action="login_register_wx">  

<h2><span class="red"><strong>注册</strong></span></h2>


		  
		  
		  
<dl class="yhzc">

    <dd><input type="text" id="UMb" name="UMb" placeholder="请输入手机号码"  onBlur="return checkPhone();"><span  style="padding-left:18px; text-align:left; " id="umbtip"></span></dd>
                    
    <dd><span class="yhzlleibie"><input type="text" id="yzm" name="yzm" placeholder="手机验证码" onBlur="return checkUmbYzmIsEqual();"></span><span id="fsyzm" class="yzmdisable">发送手机验证码</span><span  style="padding-left:18px; text-align:left;" id="smstip"></span></dd>                      

    <dd><input type="password" id="UPassword" name="UPassword" placeholder="请输入您的密码"></dd>
    
    <dd><input type="password" id="confirm_UPassword" name="confirm_UPassword" placeholder="再次确认您的密码"></dd>

	<dd><input type="hidden" id="USex" name="USex" value="${sex}">
	<input type="hidden" id="UWeixinloginUserid" name="UWeixinloginUserid" value="${openid}">
	<input type="hidden" id="UWeixingztime" name="UWeixingztime" value="${subscribe_time}">
	<input type="hidden" id="UUsername" name="UUsername" value="${nickname}">
	<input type="hidden" id="subscribe" name="subscribe" value="${subscribe}"></dd>
	

          
    <dd><button id="wxsub" type="submit">注册</button></dd>
</dl> 
		 
 

			<div class="clear"></div>

                </div>
            </div>
</form>  
        </div>

<div class="footer">长亭易购 版权所有<br/>©2014-2015 CTEGO CORPORATION All Rights Reserved.</div>

</div>
</body>  
</html> 




<script type="text/javascript" >  
  
$.validator.setDefaults({  
        submitHandler: function(commentForm) {  
                if (checkPhone()){
			 commentForm.submit();
		 		} 
        }  
});  
  
$().ready(function() {  
      
        $("#commentForm").validate({  
                rules: {  
                    UMb: {  
                            required:true,  
                         //   minlength:11,
                    }, 
					yzm: {  
                            required:true,  
                            minlength:4,
                    }, 
                    UPassword: {  
                        required: true,  
                        minlength: 6  
                    },  
                    confirm_UPassword: {  
                        required: true,  
                        minlength: 6,  
                        equalTo: "#UPassword"  
                    }  
                },  
                messages: {  
                    UMb: {  
                            required:"手机号码必填",  
                         //   minlength:"手机号码不正确"  
                    },  
					yzm: {  
                            required:"验证码必填",  
                            minlength:"至少4个字符"  
                    }, 
                    UPassword: {  
                        required: "请输入密码",  
                        minlength: "密码不能少于6个字符"  
                    },  
                    confirm_UPassword: {  
                        required: "请输入确认密码",  
                        minlength: "确认密码不能少于6个字符",  
                        equalTo: "密码输入不一致"  
                    }  
                }  
        });  
  
});  
</script>  



</body>
</html>

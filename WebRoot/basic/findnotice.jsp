<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="<%=key%>" />
<meta name="Description" content="<%=title%>" />
<title><%=key%> <%=title%> ${notice.noTitle}</title>
<link href="css/css.css" type="text/css" rel="stylesheet"/>
<link href="css/css2.css" type="text/css" rel="stylesheet"/>
<link href="css/css3.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="js/jqueryt.js"></script>
<script type="text/javascript" src="js/lrtk.js"></script>

<link type="text/css" rel="stylesheet" href="css/style.css" />

<script src="js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>

</head>

<body>

<jsp:include page="../common/head.jsp"></jsp:include>
<div id="popDivall" class="mydivall" style="display:none;">
<div class="con_t"><a href="javascript:closecharDiv()"><img src="<%=imgurl %>/bg/chadiao.gif"></a><span>让客户经理联系我</span></div>
<div class="con_b">
	<ul>
    <li><p>您的支持是我们前进的动力！<br/>
如有问题或建议，请留下您的联系方式和邮箱及问题详情，我们会尽快与您取得联系，谢谢支持。</p></li>
	<li id="ts" style="color:red"></li>
	<c:if test="${ctUser== null }"> 
	<li><span>邮箱：</span><input name="con" type="text" id="email"/></li>
    <li><span>手机：</span><input name="con" type="text" id="mbo"/></li>
	</c:if>
	<c:if test="${ctUser!= null }"> 	 
			<c:if test="${ctUser.UEmail == null }">
    		<li><span>邮箱：</span><input name="con" type="text" id="email"/></li>
    		</c:if>
    		<c:if test="${ctUser.UEmail != null }">
    		<li><span>邮箱：</span><input name="con" type="text" id="email" value="${ctUser.UEmail}" readonly="readonly"/></li>
    		</c:if>
    		<c:if test="${ctUser.UMb == null }">
    		<li><span>手机：</span><input name="con" type="text" id="mbo"/></li>
    		</c:if>
   		    <c:if test="${ctUser.UMb != null }">
    		<li><span>手机：</span><input name="con" type="text" id="mbo" value="${ctUser.UMb}" readonly="readonly"/></li>
    		</c:if>
	</c:if>
	<li><span>详细：</span><textarea name="con" id="detail"></textarea></li>
    <li><div class="msg-up" style="display: none;"></div></li>
    <li><a href="javascript:contacts()">提交</a></li>
    </ul>
</div>
</div>

<div class="contant_e">

    <div class="dizhilian_e"><a href="<%=basePath%>" target="_blank">首页</a>><a href="<%=request.getContextPath() %>/list_notice">特别公告</a>><span>${notice.noTitle}</span></div>
    
	

<div class="bangzhu_left">
    <h2 class="bzh_b">特别公告</h2>
    <div class="bangzhu_con">
        
	<h3>${notice.noTitle}</h3>
    <div class="fabu">发布日期：<i>${notice.noTime}</i></div>
    <div class="fbnr">
    <p>${notice.noContent}</p>
    </div>
    </div>
</div>	

<div class="bangzhu_list">
    <div class="bzh_r_n">

<div class="lxkhjl_b1"><a href="javascript:showcharDiv();">让客户经理联系我</a></div>
<s:iterator value="typeList" var="lists">      
	   	<dl class="lxkhjl_dl">
		<dt>${lists.HTypeName}</dt>
<s:iterator value="helps" var="list"> 
			<c:if test="${lists.HTypeId==list.HTypeId}"> 
				<c:if test="${list.HId == 1 }">
					<dd><a href="introduce.jsp">${list.HTitle}</a></dd>
				</c:if>	
				<c:if test="${list.HId != 1 }">
					<dd><a href="list_help_desc?help.HId=${list.HId }">${list.HTitle}</a></dd>
				</c:if>	
			</c:if>
	</s:iterator> 
		</dl>									
</s:iterator>   
  


  
    </div>	    
</div>	    

<div class="clear"></div>
</div>

<jsp:include page="../common/foot.jsp"></jsp:include>

</body>
</html>


<%@ page contentType="text/html; charset=GB2312" %>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content= "${systemInfo.ctTitle }" />
<link href="css/resources.css" type="text/css" rel="stylesheet"/>
<title>${systemInfo.ctTitle }</title>
<script type="text/javascript">
function findnotice(noId) {
	window.location.href="find_notice?noticeDTO.noId="+noId;
}    
</script>

</head>

<body>

<jsp:include page="../common/head.jsp"></jsp:include>
<div id="popDivall" class="mydivall" style="display:none;">
<div class="con_t"><a href="javascript:closecharDiv()"><img src="<%=imgurl %>/bg/chadiao.gif"></a><span>让客户经理联系我</span></div>
<div class="con_b">
	<ul>
    <li><p>您的支持是我们前进的动力！<br/>
如有问题或建议，请留下您的联系方式和邮箱及问题详情，我们会尽快与您取得联系，谢谢支持。</p></li>
	<li id="ts" style="color:red"></li>
	<c:if test="${ctUser== null }"> 
	<li><span>邮箱：</span><input name="con" type="text" id="email"/></li>
    <li><span>手机：</span><input name="con" type="text" id="mbo"/></li>
	</c:if>
	<c:if test="${ctUser!= null }"> 	 
			<c:if test="${ctUser.UEmail == null }">
    		<li><span>邮箱：</span><input name="con" type="text" id="email"/></li>
    		</c:if>
    		<c:if test="${ctUser.UEmail != null }">
    		<li><span>邮箱：</span><input name="con" type="text" id="email" value="${ctUser.UEmail}" readonly="readonly"/></li>
    		</c:if>
    		<c:if test="${ctUser.UMb == null }">
    		<li><span>手机：</span><input name="con" type="text" id="mbo"/></li>
    		</c:if>
   		    <c:if test="${ctUser.UMb != null }">
    		<li><span>手机：</span><input name="con" type="text" id="mbo" value="${ctUser.UMb}" readonly="readonly"/></li>
    		</c:if>
	</c:if>
	<li><span>详细：</span><textarea name="con" id="detail"></textarea></li>
    <li><div class="msg-up" style="display: none;"></div></li>
    <li><a href="javascript:contacts()">提交</a></li>
    </ul>
</div>
</div>
<div id="bgallfk" class="bgallfk" style="display:none;"></div>
<div class="contant_e">
<div class="resource_load">
    <p class="r_head">
        <a href="<%=basePath%>">首页</a>&nbsp;&nbsp;>&nbsp;&nbsp;<span>资源下载</span>
    </p>
    <div class="resource_con">
        <h2>资源下载</h2>
        <ul>
        	<c:forEach items="${resourcesList }" var="list">
	            <li>
	                <a href="https://ctegores.oss-cn-shenzhen.aliyuncs.com${list.RUrl }">
	                    <b></b> <span>${list.RName }</span>
	                    <div><span>点击下载</span><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/1111/xiazai.gif"></div>
	                </a>
	            </li>
        	</c:forEach>
        </ul>
    </div>
${pages.pageGoodsStr }
</div>
<div class="bangzhu_list" style="display: none;">
    <div class="bzh_r_n">

<div class="lxkhjl_b1"><a href="javascript:showcharDiv();">让客户经理联系我</a></div>

<s:iterator value="typeList" var="lists">      
	   	<dl class="lxkhjl_dl">
		<dt>${lists.HTypeName}</dt>
	<s:iterator value="helps" var="list"> 
			<c:if test="${lists.HTypeId==list.HTypeId}"> 	
		<dd><a href="list_help_desc?help.HId=${list.HId }">${list.HTitle}</a></dd>
			</c:if>
	</s:iterator> 
		</dl>									
</s:iterator>   
  


  
    </div>	    
</div>	    

<div class="clear"></div>
</div>

<jsp:include page="../common/foot.jsp"></jsp:include>
<script type="text/javascript">
	/* 分页跳转控制器 */
	function upAndDownPage(sum){
		//截取访问名
		var url = document.URL.toString();
		var i = url.lastIndexOf("/");
		var s = url.lastIndexOf("?");
		if(s == -1){
			
		} else {
			var chai = url.split("?");
			i = chai[0].lastIndexOf("/");
			url = url.substring(i+1, s);
		}
		window.location.href= url+"?page=" + sum;
	}
	</script>
</body>
</html>

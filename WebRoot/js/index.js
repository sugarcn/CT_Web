$(document).ready(function(){
    $.fn.tanchu();
    $.fn.fixed();
    $.fn.slider.init();
    $.fn.succeed.init();
    //固定搜索栏
    $(window).scroll(function(){
        var mTop=$("#search").prop("offsetTop");
        var height=parseFloat($("#search").css("height"));
        var wTop=$(window).scrollTop();
        if(wTop>(mTop+height)){
            $("#search").addClass("active");
            $("#search_div").css("display","block");
        }else{
            $("#search").removeClass("active");
            $("#search_div").css("display","none");
        }
    });
});
$.fn.tanchu=function() {
    var ul = document.querySelectorAll(".tanchu>ul");
    for (var i = 0; i < ul.length; i++) {
        var li = $(ul[i]).children("li");
        if (li.length > 10) {
            $(li).css("float", "left").css("width", "45%").css("margin-left","5%");
        }
    }
}
//固定栏弹出功能
$.fn.fixed=function(){
    $("#fixed>div.div2").on("mouseover","div>a",function(){
        $(this).siblings("b").fadeIn(500);
    });
    $("#fixed>div.div2").on("mouseout","div>a",function(){
        $(this).siblings("b").fadeOut(500);
    });
}

//广告图片
var images=[
	{"i":1,"img":"https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/banner_03.jpg","img_bg":"https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/bg_03.jpg"},
    {"i":2,"img":"https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/banner_01.jpg","img_bg":"https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/bg_01.jpg"},
    {"i":3,"img":"https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/banner_02.jpg","img_bg":"https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/bg_02.jpg"}
	
]
//广告轮播功能
$.fn.slider={
    canAuto:true,//保存能否启动自动轮播
    timer:null,//保存当前轮播的序号
    num:1,//保存每次显示图片的序号
    opacity:0,//保存背景颜色的透明度
    init:function(){
        this.updataView();
        //留住this
        var me=this;
        //为index下的Li绑定鼠标移入事件
        $("#index").on("mouseover","li",function(){
            //如果当前Li的className不等于Hover
            //如果当前的li没有hover
            if(!$(this).hasClass("hover")){
                //向当前li添加class为hover并删除其他兄弟元素的class
                $(this).addClass("hover").siblings(".hover").removeClass("hover");
                //获取当前li的html保存在变量n中
                var n=$(this).html();
                //找到slider中的第n个li让其淡出
                $("#slider>li.s"+n).fadeIn(1000).siblings("li").fadeOut(1000);
                $(".slider_bg>li.b"+n).fadeIn(1000).siblings("li").fadeOut(1000);
                //将banner的背景色改为图片的主体颜色
                $("#banner").css("background",images[n-1].color);
            }
        });
        //鼠标移入时不能禁用自动轮播
        $("#slider").on("mouseover",function(){
            me.canAuto=false;
            clearInterval(me.timer);
            me.timer=0;
        })
        //鼠标移入时不能启用自动轮播
        $("#slider").on("mouseout",function(){
            me.canAuto=true;
            me.lunbo();
        })
        //调用自动轮播功能
        this.lunbo();
    },
    lunbo:function(){//启动自动轮播
        var me=this;
            //启动定时器
            me.timer = setInterval(function(){
                if(me.canAuto) {
                    $("#slider>li.s" + me.num).fadeIn(1000).siblings("li").fadeOut(1000);
                    $(".slider_bg>li.b" + me.num).fadeIn(1000).siblings("li").fadeOut(1000);
                    $("#index>li.i" + me.num).addClass("hover").siblings(".hover").removeClass("hover");
                    me.num++;
                    //如果num大于等于Images.length
                    if (me.num > images.length) {
                        //把num归0;
                        me.num = 1;
                    }
                }else{
                    me.lunbo();
                }
            },4000);
    },

    updataView:function(){
        for(var i=0,html1="",html2="",html3="";
            i<images.length;
            i++){
            //向html1中拼接:
            //<li><img src="当前元素的img"></li>
            html1+='<li class="s'+(i+1)+'"><img src="'+
                images[i].img+'"></li>';
            //向html2中拼接:<li>i+1</li>
            html2+="<li class='i"+(i+1)+"'>"+(i+1)+"</li>";
            html3+="<li class='b"+(i+1)+"'><img src='"+images[i].img_bg+"'></li>";
        }
        $("#slider").html(html1);
        $("#index").html(html2);
        $(".slider_bg").html(html3);
        $("#index>li.i"+(images[0].i)).addClass("hover");
        $("#slider>li.s"+(images[0].i)).css("display","block");
        $(".slider_bg>li.b"+(images[0].i)).css("display","block");
    }
}

//成交记录轮播功能
$.fn.succeed={
    LIHEIGHT:0,//保存每个li的高度
    //DISTANCE:0,//保存轮播移动的总距离
    DURATION:380,//保存轮播的总时间
    STEPS:38,//保存轮播的总步数
    interval:0,//保存每步轮播的时间间隔
    step:0,//保存每步轮播的步长
    timer:null,//保存当前轮播的序号
    timer2:null,
    moved:0,//保存本次轮播已经移动的步数
    WAIT:2000,//保存自动轮播之间的时间间隔
    init:function(){//轮播初始化
        //求出时间间隔
        this.interval=this.DURATION/this.STEPS;
        //获得li的高度转为浮点数
        this.LIHEIGHT=parseFloat($("#details_div>li").css("height"));
        //调用轮播方法
        this.timer2=setTimeout(this.move,this.WAIT);
    },

    //启动一个轮播
    move:function(){
        //求出步长
        $.fn.succeed.step=$.fn.succeed.LIHEIGHT/$.fn.succeed.STEPS;
        //启动定时器
        $.fn.succeed.timer=setTimeout($.fn.succeed.moveStep,$.fn.succeed.interval);
    },

    //移动一步
    moveStep:function(){
        //获取id为details_div的top值保存在变量top中
        var top=parseFloat($("#details_div").css("margin-top"));
        var m_top=top-$.fn.succeed.step+"px";
        $("#details_div").css("margin-top",m_top);
        $.fn.succeed.moved++;
        if($.fn.succeed.moved<$.fn.succeed.STEPS){
            $.fn.succeed.timer=setTimeout($.fn.succeed.moveStep,$.fn.succeed.interval);
        }else{
            $.fn.succeed.timer=null;
            $.fn.succeed.moved=0;
            $.fn.succeed.timer2=setTimeout($.fn.succeed.move,$.fn.succeed.WAIT);
            //clearTimeout(this.timer);
            var li=$("#details_div>li:first-child").html();
            $("#details_div>li:first-child").remove();
            $("#details_div").css("margin-top",0);
            $("#details_div").append("<li>"+li+"</li>");
        }
    },

}

//边框滑动效果
$(function(){
    $(".border_animation").mouseover(function(){
        $(this).addClass("hover");
    });
    $(".border_animation").mouseout(function(){
        $(this).removeClass("hover");
    });
});
//检查非法字符
function checkSpe(obj) {
	var reg1 = /^.*[\'|\"|\/|<|>|~|!|@|#|$|%|^|&|*|(|)|_|+|`|?|？].*$/;
	if(reg1.test(obj)){
		return false;
	}else {
		return true;
	}
};
function checkZip(zip){
	var reg=/^[1-9][0-9]{5}$/;
	if(reg.test(zip)){
		return true;
	}else{
		return true;
	}
}
//校验登录名：只能输入5-20个以字母开头、可带数字、“_”、“.”的字串
function isUserName(s){
	var patrn=/^[a-zA-Z]{1}([a-zA-Z0-9]|[._]){4,19}$/;
	if (!patrn.exec(s)) return false
	return true
}
//校验密码：只能输入6-20个字母、数字、下划线
function isPasswd(s){
	var patrn=/^(\w){6,20}$/;
	if (!patrn.exec(s)) return false
	return true
}
//校验普通电话、传真号码：可以“+”开头，除数字外，可含有“-”
function isTel(s){
	var patrn=/^[+]{0,1}(\d){1,3}[ ]?([-]?((\d)|[ ]){1,12})+$/;
	if (!patrn.exec(s)) return false
	return true
}
//校验邮政编码
function isPostalCode(s){
	var patrn=/^[a-zA-Z0-9 ]{3,12}$/;
	if (!patrn.exec(s)) return false
	return true
}
//校验email
function isemail(){
	var email=$("#email").val();
    var patrn=/^([a-zA-Z0-9]+[_|\-|\.]?)*[a-zA-Z0-9]+@(([a-zA-Z0-9]+[_|\-|\.]?)*[a-zA-Z0-9])\.[a-zA-Z]{2,5}$/;
	if(!patrn.exec(email)){
		$("#emailtip").text("请输入有效的邮箱！");
	}else{
		$.ajaxSetup({async:false});
		$.post("login_checkEmail",{"ctUser.UEmail":email},function(data){
			if(data=="success"){
				$("#emailtip").text("");
			}else {
				$("#emailtip").text("邮箱已存在请重新输入");
				flag = false;
//				$("#email").val("");
			}
	});
	}
}


	function showGoodsDiv(CId){
		$.ajaxSetup({async:false});
   		$.post("goods_cate",{"fgoodsDTO.CId":CId},function(data){
   			for(var i=0;i<data.length;i++){
   				$(".cate-tag").apend();
   			}
   		});
	}
	
	
function searchgoods(){
	var kword = $("#keyw").val();
	var searchSelected = $("#searchSelected").text();
	$("#typeDianRong").val(searchSelected);
	if(kword==null||kword==""){
		alert("搜索关键字不能为空");
		return false;
	}else{
		$("#searchForm").submit();
	}
}
function searchgoods1(){
	var kword = $("#keyw1").val();
	var searchSelected = $("#searchSelected1").text();
	$("#typeDianRong1").val(searchSelected);
	if(kword==null||kword==""){
		alert("搜索关键字不能为空");
		return false;
	}else{
		$("#searchForm1").submit();
	}
}

function check(){
	$("#kwordXuan").val($("#kword").val());
}

//校验搜索关键字
//function isSearch(s){
//	var patrn=/^[^`~!@#$%^&*()+=|\\\][\]\{\}:;'\,.<>/?]{1}[^`~!@$%^&()+=|\\\][\]\{\}:;'\,.<>?]{0,19}$/;
//	if (!patrn.exec(s)) return false
//	return true
//}
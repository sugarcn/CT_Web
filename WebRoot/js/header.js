//保存收货地址
function saveAddress1(){
	var country2 = $("#country2").val();
	var province2 = $("#province2").val();
	var city2 = $("#city2").val();
	var district2 = $("#district2").val();
	var address2 = $("#address2").val();
	var zipcode2 = $("#zipcode2").val();
	var AConsignee2 = $("#AConsignee2").val();
	var tel2 = $("#tel2").val();
	var mb2 = $("#mb2").val();
	var adesc=$("#desc").val();
	if(country2 == "0"){
		$("#region2Tip").text("请选择收货地址");
	}else if(province2 == "0"){
		$("#region2Tip").text("请选择收货地址");
	}else if(city2 == "0"){
		$("#region2Tip").text("请选择收货地址");
	}else if(district2 == "0"){
		$("#region2Tip").text("请选择收货地址");
	}else if(address2 == ""){
		$("#region2Tip").text("");
		$("#address2Tip").text("请填写详细地址");
	}else if(AConsignee2 == ""){
		$("#address2Tip").text("");
		$("#AConsignee2Tip").text("请填写收货人姓名");
	}	else if(adesc ==""){
		$("#descTip").text("请填写快递备注");
	}else if(tel2 =="" && mb2==""){
		$("#AConsignee2Tip").text("");
		$("#mb2Tip").text("手机、电话两者至少填一项");
	}else if (AConsignee2!= ""){
		var len = AConsignee2.replace(/[^\x00-\xff]/g, "**").length;
		if(len >= 15){
			$("#AConsignee2Tip").text("收货人姓名过长");
		} else {
			$("#addressform").submit();
		}
	}else if(tel2 != ""){
			$("#mb2Tip").text("");
			if(!(/^1[3|5|7|8|9][0-9]\d{8}$/.test(tel2))){
				$("#tel2Tip").text("手机号不合法");
			}else if(zipcode2 != ""){
				if(!(/^[1-9][0-9]{5}$/.test(zipcode2))){
					$("#zipcode2Tip").text("请填写正确的邮编");
				}else{
					$("#addressform").submit();
				}
			}else{
				$("#addressform").submit();
			}
		}else if(mb2 != ""){
				$("#mb2Tip").text("");
				if(!(/^(\d{3,4}\-?)?\d{7,8}$/.test(mb2))){
					$("#mb2Tip").text("电话号码不合法");
			}else if(zipcode2 != ""){
				if(!(/^[1-9][0-9]{5}$/.test(zipcode2))){
					$("#zipcode2Tip").text("请填写正确的邮编");
				}else{
					$("#addressform").submit();
				}
			}else{
				$("#addressform").submit();
			}
		}
}


//跳转到BOM中心
function goBomCenter(){
	window.location.href="bomcenter_hotList";
}
//关闭窗口
function close(){
	$("#myModal").hide();
	$("#bgallfk").hide();
	$(".reveal-modal-bg").hide();
}
//通过层登录
function loginByLayer(){
	var username=$("#username").val();
	var passwd=$("#password").val();
	if(username == "" || username == "用户名/手机/邮箱"){
		$("#usernametip").text("请输入用户名");
	}else {
		$("#usernametip").text("");
		if(passwd.length<6){
			$("#passwdtip").text("密码长度至少6位");
		}else {
			$("#passwdtip").text("");
			//$("#loginByLayerForm").submit();
			$.ajaxSetup({async:false});
			$.post("login_loginByLayer",{"userDTO.UUserid":username,"userDTO.UPassword":passwd},function(data){
				if(data == "success"){
					//alert("登录成功");
					var url = $("#nexturl").val();
					var i = url.lastIndexOf("/");
					var s = url.lastIndexOf("?");
					var actionUrl = url.substring(i+1, s);
					if(actionUrl == "add_cart"){
						var feng = url.split("?");
						var da = feng[1].substring(5);
						$.post(actionUrl,{"data":da},function(date,varstart){
							if(date == "success"){
								$("#myModal").hide();
								$("#bgallfk").hide();
								$(".BMchuangshowgw").show();
							}
						});
					} else {
						window.location.href=url;
					}
				}else if(data == "error"){
					$("#msg").text("用户名或者密码错误");
				}else if(data == "mailnotactive"){
					$("#msg").text("邮箱没有激活");
				}else {
					$("#msg").text("非法登录");
				}
			});
		}
}
}
//$(function(){
//	$("#list").mouseover(function(){
//		alert(11);
//		
//	})
//})
//$(function(){
//		$.post("goods_catelist",
//						function(data) {
//											for (var i = 0; i < data.length; i++) {
//												$(".ct_left_bar").append(
//												"<li id='list'><div class='cate-tag'><a href='#' target='_blank'>"+data[i].CName+"</a></div>"+
//												"<div class='list-item hide'>"+
//												"<ul class='itemleft'>"+
//												"<dl><dd>");
//											}
//											$(".itemleft dd").append("</dd></dl><div class='fn-clear'></div><li style='display:none;'></li></ul></div></li>");
//										},"json")
//	$("li[id='list'] a").mouseenter(function(){
////		$(".list-item").show();
//	$(".list-item").css({'display':'block'});
//	})
//	$(".ct_left_bar").mouseleave(function(){
//	$(".list-item").css({'display':'none'});
//	})
//})
function rankDetail(){
	$.ajaxSetup({async:false});
	$.post("rank_detail",function(data){
		location.href="list_help_desc?help.HId=" + data.HId;
	});
}
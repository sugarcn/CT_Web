//数量加1
function addBomNum(bomId){
	bomNum = $("#"+bomId).val();
	var newbomNum = Number(bomNum) + 1;
	$.post("bom_updateBomNum",{"bomDTO.goodsNum":newbomNum,"bomDTO.bomId":bomId},function(data){
		if(data == "success"){
			$("#"+bomId).val(newbomNum);
		}
		
	});
}
//数量减1
function subBomNum(bomId){

	bomNum = $("#"+bomId).val();
	if(bomNum <= 1){
		alert("BOM数量不能小于1");
		return ;
	}
	
	
	var newbomNum = Number(bomNum) - 1;
	$.post("bom_updateBomNum",{"bomDTO.goodsNum":newbomNum,"bomDTO.bomId":bomId},function(data){
		if(data == "success"){
			$("#"+bomId).val(newbomNum);
		}
		
	});

}
//更新数量
function updateNum(num,bomGoodsId){
	var goodsNum = $("#"+num).val();
	var bomId = $("#bomId").val();
	if(goodsNum < 1){
		alert("BOM数量不能小于1");
		return ;
	}
	$.post("bom_updateGoodsNum",{"bomGoodsDTO.goodsNum":goodsNum,"bomGoodsDTO.bomGoodsId":bomGoodsId,"bomGoodsDTO.bomId":bomId},function(data){
		if(data == "success"){
			$("#"+num).val(goodsNum);
		}
		
	});
}


//弹出新增页
function add(){
	window.location.href="bom_add";
}
//新增Bom
function saveAdd(){
	var bomTitle = $("#bomTitle").val();
	var bomDesc = $("#bomDesc").val();
	var url=window.location.href;
	if(bomTitle == ""){
		$("#bomtitletip").text("请输入BOM名称");
	}else if(bomDesc == ""){
		$("#bomtitletip").text("");
		$("#bomdesctip").text("请输入BOM介绍");
	}else{
		$("#bomdesctip").text("");
		$.ajaxSetup({async:false});
		if(checkBomTitle()){
			$.post("bom_saveAdd",{"bomDTO.bomTitle":bomTitle,"bomDTO.bomDesc":bomDesc,"bomDTO.url":url},function(data){
				if(data == "success"){
					$(".BMchuang").hide();
					window.location.href="bom_list";
				}else{
					//alert("没有登录,请登录后重试");
					$(".reveal-modal-bg").show();
					$("#nexturl").val("bom_list");
					$("#myModal").show();
				}
			});
		}
	}
	
}

//修改Bom
function editBom(){
	var checkedNum = $("input[name='mid']:checked").length;
	if(checkedNum == 0) { 
			alert("请选择一项！"); 
			return false; 
		} else if(checkedNum  > 1){
			alert("请选择一个进行修改");
			return false;
		}else{
			$("#bgallfk").show();
			$('.BMchuang').show();
			$("#xz").hide();
			$("#xg").show();
			var MId = $("input[name='mid']:checked")[0].value;
			$.ajaxSetup({async:false});
			$.post("bom_editBom",{"bomDTO.mid":MId},function(data){
				$("#bomTitle").val(data[0].bomTitle);
				$("#bomDesc").val(data[0].bomDesc);
				$("#bomId").text(data[0].bomId);
			});
		}
	
}
//导入Bom
function loadBom(){
	
}
//保存修改
function saveUpdate(){
	var url=window.location.href;
	var bomTitle = $("#bomTitle").val();
	var bomDesc = $("#bomDesc").val();
	var bomId = $("#bomId").text();
	if(bomTitle == ""){
		$("#bomtitletip").text("请输入BOM名称");
	}else if(bomDesc == ""){
		$("#bomtitletip").text("");
		$("#bomdesctip").text("请输入BOM介绍");
	}else{
		$("#bomdesctip").text("");
		if(checkBomTitle()){
			$.ajaxSetup({async:false});
			if(checkBomTitle()){
			$.post("bom_saveUpdate",{"bomDTO.bomId":bomId,"bomDTO.bomTitle":bomTitle,"bomDTO.bomDesc":bomDesc,"bomDTO.url":url},function(data){
				if(data == "success"){
					$(".BMchuang").hide();
					$("#bomTitle").val("");
					$("#bomDesc").val("");
					window.location.href="bom_list";
				}else{
					alert("修改失败");
				}
			});
		}
		}
	}
	
}

//删除Bom
function deleteBom(){

		var checkedNum = $("input[name='mid']:checked").length; 
		if(checkedNum == 0) { 
			alert("请选择至少一项！"); 
			return; 
		} 
         if (!confirm("确认要删除么？")) {
              return false;
         }else{
			  $("#bomFrom").attr("action","bom_del");
			  $("#bomFrom").submit();
		}
}
//增加
function saveBomTan(){
	$("#bomtitletip").text("");
	$("#bgallfk").show();
	$('.BMchuang').show();
	$("#xg").hide();
	$("#xz").show();
	$.ajaxSetup({async:false});
	$("#bomTitle").val("");
	$("bomDesc").val("");
	$("bomId").text("");
}

//搜索
function searchBom(){
		var checkedNum = $("#keyword").attr("value");; 
		if(checkedNum == "") { 
			//alert("请输入你要查询的关键词！"); 
			$("#bomFrom").attr("action","bom_search?page=1");
			  $("#bomFrom").submit();
			//return; 
		}else{
			var params={
				"keyword":checkedNum
			}
			$("#bomFrom").attr("action","bom_search?page=1");
			  $("#bomFrom").submit();
		} 
		
	}

//复制Bom
function copyBom(){
		var checkedNum = $("input[name='mid']:checked").length; 
		$("#url").val(window.location.href);
		if(checkedNum == 0) { 
			alert("请选择至少一项！"); 
			return; 
		} 
         if (!confirm("确认要复制BOM吗？")) {
              return false;
         }else{
        	 $("#bomFrom").attr("action","bom_copyBom");
			  $("#bomFrom").submit();
		}
}

//关闭修改bom弹窗
function closeBOMBM(){
	$("#bgallfk").hide();
	$(".BMchuang").hide();
}

//提交生成Bom订单
function orderBom(){
		var checkedNum = $("input[name='mid']:checked").length; 	
		if(checkedNum != 1) { 
			alert("请选择一项！"); 
			return; 
		} 
		var bomid = $("input[name='mid']:checked")[0].value;
		$("#bimId").val(bomid);
			  $("#bomFrom").attr("action","bom_check");
			  $("#bomFrom").submit();	
}

//Bom详细
/*
function goBomDetail(bomId) {
	window.location.href="bom_goBomDetail?bomDTO.bomId="+bomId;
}
*/
//进入我的Bom
function goMyBom(url){
	$.post("login_isLogin",function(data){
		if(data == "success"){
			window.location.href=url;
		}else{
			
		}
	});
}
//检验BomTitle是否存在
function checkBomTitle(){
	var flag = true;
	var bomTitle = $("#bomTitle").val();
	var bomId = $("#bomId").text();
	$.ajaxSetup({async:false});
	$.post("bom_checkBomTitle",{"bomDTO.bomTitle":bomTitle,"bomId":bomId},function(data){
		if(data=="exists"){
			$("#bomtitletip").text("BOM已存在请重新输入");
			flag = false;
		}else{
			$("#bomtitletip").text("");
		}
	});
	return flag;
}
//下载bom模板
function DownloadForBomModel(){
	$.post("downLoadForBom",function(data,start){
		$("#downloadFile").attr("href",data);
	});
}
//全选
function selectall(ch){
	var checkboxs=document.getElementsByName("mid");
	if(ch.checked){
		for(i=0;i<checkboxs.length;i++){
			checkboxs[i].checked=true;
		}
	}else{
		for(i=0;i<checkboxs.length;i++){
			checkboxs[i].checked=false;
	}
}
}


	$('.lcha').live('click', function(){
		$('.BMchuang').hide();
		$('.BMchuangshow').hide();
		$('.BMchuangshowgw').hide();
		$('.bgallfk').hide();

	});
	
	

	

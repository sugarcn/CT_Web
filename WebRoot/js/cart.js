function isLogin(gid){
	var url=window.location.href;
	$.ajaxSetup({async:false});
	$.post("login_isLogin",function(data){
		if(data == "success"){
			//window.location.href="goods_detail?gid="+gid;
			//alert("登录");
			$.ajaxSetup({async:false});
			$.post("coupon_getCtCouponsByGid",{"fgoodsDTO.GId":gid},function(data){
				var html = "<option>"+"领取优惠券"+"</option>";
				for(var i=0;i<data.length;i++){
					if(data[i][2] == "1"){
						//onclick=\"javascript:isGetCoupom("+data[i][0]+")\"
						var couponId = data[i][0];
						$.ajaxSetup({async:false});
						$.post("coupon_isGetCoupom2",{"coupondetailDTO.couponId":couponId,"coupondetailDTO.url":url},function(data2){
							if(data2 == "get"){
								html = html + "<option style=\"background-color: gray;\" value="+data[i][0]+">"+"满"+data[i][3]+"减"+data[i][4]+"</option>";
							}else{
								html = html + "<option value="+data[i][0]+">"+"满"+data[i][3]+"减"+data[i][4]+"</option>";
							}
						});
					}else if(data[i][2] == "2"){
						var couponId = data[i][0];
						$.ajaxSetup({async:false});
						$.post("coupon_isGetCoupom2",{"coupondetailDTO.couponId":couponId,"coupondetailDTO.url":url},function(data2){
							if(data2 == "get"){
								html = html + "<option style=\"background-color: gray;\" value="+data[i][0]+">"+"优惠"+data[i][4]+"</option>";
							}else{
								html = html + "<option value="+data[i][0]+">"+"优惠"+data[i][4]+"</option>";
							}
						});
					}else{
						var couponId = data[i][0];
						$.ajaxSetup({async:false});
						$.post("coupon_isGetCoupom2",{"coupondetailDTO.couponId":couponId,"coupondetailDTO.url":url},function(data2){
							if(data2 == "get"){
								html = html + "<option style=\"background-color: gray;\" value="+data[i][0]+">"+data[i][4]+"折"+"</option>";
							}else{
								html = html + "<option value="+data[i][0]+">"+data[i][4]+"折"+"</option>";
							}
						});
					}
					$("#"+gid+"choose").html(html);
				}
			});
		}else{
			$("#nexturl").val("goods_cart");
			$("#myModal").show();
		}
	});
}

//关闭窗口
function close(){
	$("#myModal").hide();
}

//通过层登录
function loginByLayer(){
	var username=$("#username").val();
	var passwd=$("#passwd").val();
	if(username == "" || username == "用户名/手机/邮箱"){
		$("#usernametip").text("请输入用户名");
	}else {
		$("#usernametip").text("");
		if(passwd.length<6){
			$("#passwdtip").text("密码长度至少6位");
		}else {
			$("#passwdtip").text("");
			//$("#loginByLayerForm").submit();
			$.ajaxSetup({async:false});
			$.post("login_loginByLayer",{"userDTO.UUserid":username,"userDTO.UPassword":passwd},function(data){
				if(data == "success"){
					//alert("登录成功");
					var url = $("#nexturl").val();
					window.location.href=url;
				}else if(data == "error"){
					$("#msg").text("用户名或者密码错误");
				}else if(data == "mailnotactive"){
					$("#msg").text("邮箱没有激活");
				}else {
					$("#msg").text("非法登录");
				}
			});
		}
}
}

function isGetCoupom(gid){
	var url=window.location.href;
	var couponId = $("#"+gid+"choose").val();
//	alert(CDetailId);
	$.post("coupon_isGetCoupom",{"coupondetailDTO.couponId":couponId,"coupondetailDTO.url":url},function(data){
		if(data == "error"){
			//$("#choose").append("<option value=\"领取优惠券\" selected=\"selected\">领取优惠券</option>");
			alert("已经领取过优惠券");
		}else if(data == "nocoupon"){
			//$("#choose").append("<option value=\"领取优惠券\" selected=\"selected\">领取优惠券</option>");
			alert("优惠券已经被领完");
		}else if(data == "success"){
			alert("领取成功");
		}
	});
}


function collect(GId){
	var gid = $("#gid").val();
	var pack = $("#pa2").val();
	var num = $("#num").val();
//	$("#cartId").val(cartId);
	$("#pack").val(pack);
	$("#goodsNum").val(num);
	$("#cgid").val(GId);
		$.ajaxSetup({async:false});
		$.post("login_isLogin",function(data){
			if(data=="success"){
				$("#nexturl-coll").val("goods_cart?fgoodsDTO.gid="+GId);
				$("#bgallfk").show();
				$(".CMchuang").show();
			}else{
				$("#nexturl").val("goods_cart?fgoodsDTO.gid="+GId);
				$("#bgallfk").show();
				$("#myModal").show();
			}
		});
}

function collectGoods(){
		$.ajaxSetup({async:false});
		$.post("login_isLogin",function(data){
			if(data=="success"){
				$("#nexturl-coll").val("goods_cart");
				$("#bgallfk").show();
				$(".CMchuang").show();
			}else{
				$("#nexturl").val("goods_cart");
				$("#bgallfk").show();
				$("#myModal").show();
			}
		});
}

function addBomGoods(){
$.ajaxSetup({async:false});
		$.post("login_isLogin",function(data){
			if(data=="success"){
				$("#nexturl-coll").val("goods_cart");
				$("#bgallfk").show();
				$(".GMchuang").show();
			}else{
				$("#nexturl").val("goods_cart");
				$("#bgallfk").show();
				$("#myModal").show();
			}
		});
}

function addBom(GId){
	$("#cgid").val(GId);
	var pack = $("#pa2").val();
	var num = $("#"+GId).val();
	//$("#cartId").val(cartId);
	$("#pack").val(pack);
	$("#goodsNum").val(num);
		$.ajaxSetup({async:false});
		$.post("login_isLogin",function(data){
			if(data=="success"){
				$("#nexturl-bom").val("goods_cart?fgoodsDTO.gid="+GId);
				$("#bgallfk").show();
				$(".GMchuang").show();
			}else{
				$("#nexturl").val("goods_cart?fgoodsDTO.gid="+GId);
				$("#bgallfk").show();
				$("#myModal").show();
			}
		});
}
/*
function updatenum(num){
	var goodsNum = $("#"+num).val();
	document.getElementById("num").value=goodsNum;
	var pp = document.getElementById(num+"pp").value;
	var obj = document.getElementById(num+"se");
	var index = obj.selectedIndex;
	var pn = obj.options[index].value;
	var pns = pn.split('-');
	var money = pp*pns[0]*goodsNum;
	$.post(
			"${pageContext.request.contextPath}/updateCartFotAjax",
			{"cartToNum.cartId":num,"cartToNum.GNumber":goodsNum,"cartToNum.pack":index+1,"cartToNum.GPrice":money}
		);
	document.getElementById(num+"st").value = money;
	document.getElementById(num+"sp").innerText = money+"元";
	chose();
}

function packchange(basepath,tar){	
	var obj = document.getElementById(tar+"se");
	var index = obj.selectedIndex;
	var pack=obj.options[index].text;
	document.getElementById("pa2").value=pack;
	var pn = obj.options[index].value;
	var pp = document.getElementById(tar+"pp").value;
	var goodsNum = $("#"+tar).val();
	var pns = pn.split('-');
	var money = pp*pns[0]*goodsNum;
	$.post(
			basepath+"updateCartFotAjax",
			{"cartToNum.cartId":tar,"cartToNum.GNumber":goodsNum,"cartToNum.pack":index+1,"cartToNum.GPrice":money}
		);
	document.getElementById(tar+"st").value = money;
	document.getElementById(tar+"sp").innerText = money+"元";
	chose();
}
*/

function selectAllNew(sta){
	var checkbox = $("input[size=4]");
	var allPrice = 0;
	if(sta == 1){
		for ( var i = 0; i < checkbox.length; i++) {
			if(checkbox[i].checked){
				for (var i=0;i<checkbox.length;i++) {
					checkbox[i].checked="checked";
				}
				var price = $("input[name=goodsCountPrice]");
				for(var j=0; j < price.length; j++){
					allPrice += Number(price[j].value);
				}
				$("#totalNew").html("&yen;"+Number(allPrice).toFixed(2));
				$("#cartGoodsNum").html(price.length);
			} else {
				for (var i=0;i<checkbox.length;i++) {
					checkbox[i].checked=false;
				}
				$("#totalNew").html("&yen;0.00");
				$("#cartGoodsNum").html("0");
			}
		}
	} else if(sta == 2) {
		for ( var i = checkbox.length-1; i >= 0; i--) {
			if(checkbox[i].checked){
				for ( var i = checkbox.length-1; i >= 0; i--) {
					checkbox[i].checked="checked";
				}
				var price = $("input[name=goodsCountPrice]");
				for(var j=0; j < price.length; j++){
					allPrice += Number(price[j].value);
				}
				$("#totalNew").html("&yen;"+Number(allPrice).toFixed(2));
				$("#cartGoodsNum").html(price.length);
			} else {
				for ( var i = checkbox.length-1; i >= 0; i--) {
					checkbox[i].checked=false;
				}
				$("#totalNew").html("&yen;0.00");
				$("#cartGoodsNum").html("0");
			}
		}
	}
}

function selectOneNew(type){
	var checkbox = $("input[size=4]");
	var total = 0;
	var num = 0;
	for(var i = 1; i< checkbox.length-1;i++ ){
		if(checkbox[i].checked){
			var gid = checkbox[i].value;
			var quPrice = $("#"+gid+"_goodsPrice").val();
			total += Number(quPrice);
			num++;
		}
	}
	$("#totalNew").html("&yen;"+Number(total).toFixed(2));
	$("#cartGoodsNum").html(num);
	
}

function SelectAll(sta) {
 var checkboxs=$("input[size=5]");
 var countPrice = 0;
	var index = 0;
	var cartIds = "";
	 var box = $("input[size=5]");
	 var gid = "";
	 if(sta==1){
		 for (var i=0;i<checkboxs.length;i++) {
			if(checkboxs[i].value=="c1"){
					if(checkboxs[i].checked){
						cartIds = $("input[name=cartIdForSimAll]");
						gid = $("input[name=gidForHidSim]");
						for (var i=0;i<checkboxs.length;i++) {
							if(checkboxs[i].name == "aa"){
								checkboxs[i].checked="checked";
							}
						}
					}else{
						for (var i=0;i<checkboxs.length;i++) {
							if(checkboxs[i].name == "aa"){
								checkboxs[i].checked=false;
							}
						}
					}
			}
		 }
	 }else if(sta==2){
		 for (var i=0;i<checkboxs.length;i++) {
			if(checkboxs[i].value=="c2"){
				if(checkboxs[i].checked){
					cartIds = $("input[name=cartIdForAll]");
					gid = $("input[name=gidForHid]");
					for (var i=0;i<checkboxs.length;i++) {
						checkboxs[i].checked="checked";
					}
				}else{
					for (var i=0;i<checkboxs.length;i++) {
						checkboxs[i].checked=false;
					}
				}
			}
		}
	 } else if (sta==3){
		 for (var i=0;i<checkboxs.length;i++) {
			if(checkboxs[i].value=="c3"){
					if(checkboxs[i].checked){
						cartIds = $("input[name=cartIdForParAll]");
						gid = $("input[name=gidForHidPar]");
						for (var i=0;i<checkboxs.length;i++) {
							if(checkboxs[i].name == "bb"){
								checkboxs[i].checked="checked";
							}
						}
					}else{
						for (var i=0;i<checkboxs.length;i++) {
							if(checkboxs[i].name == "bb"){
								checkboxs[i].checked=false;
							}
						}
					}
			}
		 }
	 }

	 for(var k=0; k<gid.length; k++){
		 loadPeiceNew(gid[k].value,cartIds[k].value,"noUpdate");
	 }
	var m = 0*1;
	var index = 0*1;
	for (var i=0;i<box.length;i++) {
		if((box[i].checked&&box[i].value!="c1"&&box[i].value!="c2"&&box[i].value!="c3"&&box[i].value!="1")){
			if(checkboxs[i].checked){
				index++;
				countPrice += Number(checkboxs[i].value);
			}
		}
	}
		countPrice = Math.round(countPrice*100)/100;
		$("#indx").text(index);
		$("#totalm").text("￥"+countPrice.toFixed(2));
}

function chose(){
	var box = $("input[size=5]");
	var m = 0*1;
	var index = 0*1;
	for (var i=0;i<box.length;i++) {
		if((box[i].checked&&box[i].value!="c1"&&box[i].value!="c2"&&box[i].value!="c3")){
			var ctid = box[i].value;
			var price = document.getElementById(ctid+"st").value;
			var st = price*1;
			m = m+st;
			index = index+1;
		}
	}
	m = Math.round(m*100)/100;
	document.getElementById("totalm").innerText = "￥"+m.toFixed(2);
	document.getElementById("indx").innerText = index;
}
function checkout(){
	var box = $("input[size=4]");
	var gids = "";
	var gnum = "";
	for (var i=1;i<box.length-1;i++) {
		if(box[i].checked){
			gids = gids + "," + box[i].value;
		}
	}
	if(gids==""){
		alert("请至少选择一个商品！");
	}else{
		$("#gidssFrm").val(gids);
		$("#myfromCart").submit();
	}
}
//$(function(){
//	$("#checkout").click(function(){
//		var box = $("input[size=4]");
//		var gids = "";
//		var gnum = "";
//		for (var i=1;i<box.length-1;i++) {
//			if(box[i].checked){
//				gids = gids + "," + box[i].value;
//			}
//		}
//		if(gids==""){
//			alert("请至少选择一个商品！3");
//		}else{
//			$("#gidssFrm").val(gids);
//			$("#myfromCart").submit();
//		}
//	});
//});
function formatCurrency(num){
	var sign = "";
	if(isNaN(num)){
		num = 0;
	}
	if (num < 0){
		sing = "-";
	}
	var strNum = num + "";
	var arr1 = strNum.split(".");
	var hasPoint = false;
	var intPart = strNum;
	if(arr1.length >= 2){
		hasPoint = arr1[1];
		intPart = arr1[0];
	}
	var res = '';
	var intPartlength = intPart.length;
	var maxcount = Math.ceil(intPartlength/3);
	for(var i =1; i<= maxcount;i++){
		var startIndex = intPartlength-i*3;
		if(startIndex < 0){
			startIndex = 0;
		}
		var endIndex = intPartlength-i*3+3;
		var part=intPart.substring(startIndex,endIndex)+",";
		res = part+res;
	}
	res = res.substr(0,res.length-1);
	if(hasPoint){
		return "￥"+sign+res+"."+piontPart;
	}else {
		return  "￥"+sign+res;
	}
}

function showDiv(){
document.getElementById('popDivjrbom').style.display='block';
document.getElementById('bgshizhong').style.display='block';
}
function closeDiv(){
document.getElementById('popDivjrbom').style.display='none';
document.getElementById('bgshizhong').style.display='none';
}

function closeCM(){
	$("#bgallfk").hide();
	$(".CMchuang").hide();
}
function closeGM(){
	$("#bgallfk").hide();
	$(".GMchuang").hide();
}
/*
function closeSM(){
	$("#bgallfk").hide();
	$(".SMchuang").hide();
}
*/


function checkOneSim(gid,cartId){
	var test = $("#"+gid+"priceOrder");
	var ch = $("input[size=5]");
	var countPrice = 0;
	var index = 0;
	for(var i = 0; i < ch.length; i++ ){
		if((ch[i].checked&&ch[i].value!="c1"&&ch[i].value!="c2"&&ch[i].value!="c3")){
			index++;
			countPrice += Number(ch[i].value);
		}
	}
	loadPeiceNew(gid,cartId,"noUpdate");
	$("#indx").text(index);
	countPrice = Math.round(countPrice*100)/100;
	$("#totalm").text("￥"+countPrice.toFixed(2));
}



function delgoods(){
	var box =  $("input[size=4]");
	var gids = "";
	for (var i=1;i<box.length-1;i++) {
		if(box[i].checked){
			gids = gids + "," +box[i].value;
		}
//		if(box[i].checked&&box[i].value!="c1"&&box[i].value!="c2"&&box[i].value!="c3"){
//			var giddd = $(box[i]).parent().siblings(".gou_ct_2").children("a").children("input")[0].value;
//			gids = gids + "," + giddd;
//		}
	}
	if(gids==""){
		alert("请至少选择一个商品！");
	}else{
		var isdel = confirm("确定要删除选中的商品？");
		if(isdel){
			$.post(
					"del_cagsAjax",
					{"ctids":gids},
					function(data, varstart){
						if(data == "success"){
							var gid = gids.split(",");
							for(var j=0;j<gid.length;j++){
								$("a[name="+gid[j]+"bianJi]").parent().parent().parent().parent("ul[name=pro]").remove();
//								$("a[name="+gid[j]+"bianJi]").parent().parent().parent("dd[name=ppp]").remove();
							}
//							loadSimPriceAll();
							//$("#totalm").text("￥0.00");
						} else {
							alert("删除失败");
						}
					}
			);
			selectAllNew(1);
		}
		
	}
}



function delGoodsAjax(gid){
	var isdel = confirm("确定要删除这件商品？");
	if(isdel){
		$.post(
				"del_cagsAjax",
				{"ctid":gid},
				function(data, varstart){
					if(data == "success"){
						$("a[name="+gid+"bianJi]").parent().parent().parent().parent("ul[name=pro]").remove();
//						loadSimPriceAll();
						selectOneNew(1);
					} else {
						alert("删除失败");
					}
				}
		);
	}
}

function loadCupIsCun(){
	var cartAllId = $("input[name=cartAllId]");
	
	var cupon = $("#cc").val();
	var cuponchai = cupon.split(",");
	for(var i=0; i<cuponchai.length; i++){
		var chaichai = cuponchai[i].split("|");
		for(var j=0; j<cartAllId.length; j++){
			if(chaichai[1] == cartAllId[j].value){
				if(chaichai[0] == "0"){
					$(cartAllId[j]).parent("div[class=youhuiqx_d]").remove();
				}
			}
		}
	}
}

function loadSimPriceAll(num,cartId1){	
    var gid = $("input[name=allGid]");
  	var cartId = $("input[name=allCartId]");
	for(var k=0; k<gid.length; k++){
	 	loadPeiceNew(gid[k].value,cartId[k].value,"noUpdate");
    }
	if(typeof(num) != "undefined" && typeof(cartId1) != "undefined"){
		loadPeiceNew(num,cartId1,"update");
	}
	//loadPeiceNew(num,cartId,"update");
	var allPrice = $("input[name='simPrice']");
	var parAllPrice = $("input[name='parPrice']");
	var price = 0;
	var parPrice = 0;
	var pricecount=0;
	for(var i = 0; i < allPrice.length; i++){
		price += Number(allPrice[i].value);
	}
	price = Math.round(price*100)/100;
	parPrice = Math.round(parPrice*100)/100;
	pricecount=price+parPrice;
	if(tempOneCi){
		$.post(
				"isPanOrK",
				function(data, varstart){
					if(data == "Pan"){
						$("#pan").removeAttr("class");
						$("#pan").attr("class","pl_k hover");
						$("#k").attr("class","pl_p");
						
						var gids = $("input[name=gidForHidPar]");
						for(var i = 0; i < gids.length; i++){
							var incPar = $("input[name="+gids[i].value+"gidPar1]");
							var goodParStaSum =  $("input[name="+gids[i].value+"gidParSNum]");
							var goodParEndSum =  $("input[name="+gids[i].value+"gidParENum]");
							var gidParPrice =  $("input[name="+gids[i].value+"gidParPrice]");
							var inc = $("input[name="+gids[i].value+"gidPar]");
							var carId = $(gids[i]).next("#cartIdHidTest1")[0].value;
							for(var j = goodParStaSum.length - 1; j >= 0; j--){
								$("li[name="+gids[i].value+j+"isPriceHoverPar]").remove();
								var start = Number(goodParStaSum[j].value)/Number(incPar[0].value);
								var end = Number(goodParEndSum[j].value)/Number(incPar[0].value);
								var price = Number(gidParPrice[j].value)*Number(incPar[0].value);
								var priceNew = Math.round(price*10000)/10000;
								var priceParPar = Number(priceNew)*1000;
								priceParPar = Math.round(priceParPar*10000)/10000;
								var incs = Number(inc[j].value)/Number(incPar[0].value);
								var str = "<li name='"+gids[i].value+j+"isPriceHoverPar' ><span class='Ia_qus'><i>"+start+"-"+end+"：</i></span><i class='Ia_jia'>￥"+priceParPar+"</i></li>";
								$("input[name="+gids[i].value+"yuliuAdd]").after(str);
								$("input[name="+gids[i].value+"gidParSNum]")[j].value=start;
								$("input[name="+gids[i].value+"gidParENum]")[j].value=end;
								$("input[name="+gids[i].value+"gidParPrice]")[j].value=priceNew;
								$("input[name="+gids[i].value+"gidPar]")[j].value=incs;
							}
							var goodsNum = $("#"+gids[i].value+"num").val();
							if(goodsNum != 0){
								$("#"+gids[i].value+"num").val(Number(goodsNum));
							}
							parPrice = Number(parPrice) * Number(incPar[0].value);
							loadPeiceNew(gids[i].value,carId,"noUpdate");
						}
						isPanOrK = true;
					}
				}
			);
		tempOneCi = false;
	}
	for(var i = 0; i < parAllPrice.length; i++){
		parPrice += Number(parAllPrice[i].value);
	}
	pricecount=price+parPrice;
	$("i[id=simAllPrice]").text("￥" + price.toFixed(2));
	$("i[id=parAllPrice]").text("￥" + parPrice.toFixed(2));
	pricecount = Math.round(pricecount*100)/100;
	$("#totalm").text("￥" + pricecount.toFixed(2));
}


//批量加入我的BOM
function addGoodsToMany(){
	var url=window.location.href;
//	var box =  $("input[size=5]");
//	var gids = "";
//	for (var i=0;i<box.length;i++) {
//		if(box[i].checked&&box[i].value!="c1"&&box[i].value!="c2"&&box[i].value!="c3"){
//			var giddd = $(box[i]).parent().siblings(".gou_ct_2").children("a").children("input")[0].value;
//			gids = gids + "," + giddd;
//		}
//	}
	var box =  $("input[size=4]");
	var gids = "";
	for (var i=1;i<box.length-1;i++) {
		if(box[i].checked){
			gids = gids + "," +box[i].value;
		}
	}
	var bomname = $("#bm_bname").val();
		var MId=$("input[name='mid']:checked").val();
		var checkedNum = $("input[name='mid']:checked").length;
		if(bomname!=""){
			if(checkedNum != 0){
				$.ajaxSetup({async:false});
				$.post("bom_addToBomGoodsTwo",{"bomDTO.ctids":gids,"bomDTO.bomTitle":bomname,"bomDTO.mid":MId,"bomDTO.url":url},function(data){
					if(data=="success"){
					alert("加入成功");
					$(".GMchuang").hide();
					window.location.href="goods_cart";
			}else{
					alert("加入失败");
					$(".GMchuang").hide();
			}
			});
					
			}else{
				$.ajaxSetup({async:false});
				$.post("bom_addToBomGoodsNew",{"bomDTO.ctids":gids,"bomDTO.bomTitle":bomname,"bomDTO.url":url},function(data){
					if(data=="success"){
					alert("加入成功");
					$(".GMchuang").hide();
					window.location.href="goods_cart";
			}else{
					alert("加入失败");
					$(".GMchuang").hide();
			}
			});
			}
	}else{
		if(checkedNum!=0){
		$.ajaxSetup({async:false});
		$.post("bom_addToBomGoodsExisting",{"bomDTO.ctids":gids,"bomDTO.mid":MId,"bomDTO.url":url},function(data){
			if(data=="success"){
				alert("加入成功");
				$(".GMchuang").hide();
				window.location.href="goods_cart";
	}else{
				alert("加入失败");
				$(".GMchuang").hide();
	}
	});
	}else{
		alert("请选一种方式加入我的bom");
			}
		}			
}
//批量加入收藏夹
function favorites(){
	var url=window.location.href;
	var box =  $("input[size=5]");
	var gids = "";
	for (var i=0;i<box.length;i++) {
		if(box[i].checked&&box[i].value!="c1"&&box[i].value!="c2"&&box[i].value!="c3"){
			var giddd = $(box[i]).parent().siblings(".gou_ct_2").children("a").children("input")[0].value;
			gids = gids + "," + giddd;
		}
	}
		var collname=$("#coll_gname").val();
		var MId=$("input[name='mid']:checked").val();
		var checkedNum = $("input[name='mid']:checked").length;
		if(collname!=""){
			if(checkedNum != 0){
				$.ajaxSetup({async:false});
				$.post("goods_addToCollGoodsTwo",{"fgoodsDTO.ctids":gids,"fgoodsDTO.colltitle":collname,"fgoodsDTO.mid":MId,"fgoodsDTO.url":url},function(data){
					if(data=="success"){
					alert("加入成功");
					$(".CMchuang").hide();
					window.location.href="goods_cart";
			}else{
					alert("加入失败");
					$(".CMchuang").hide();
			}
			});
					
			}else{
				$.ajaxSetup({async:false});
				$.post("goods_addToCollGoodsNew",{"fgoodsDTO.ctids":gids,"fgoodsDTO.colltitle":collname},function(data){
					if(data=="success"){
					alert("加入成功");
					$(".CMchuang").hide();
					window.location.href="goods_cart";
			}else{
					alert("加入失败");
					$(".CMchuang").hide();
			}
			});
			}
	}else{
		if(checkedNum!=0){
		$.ajaxSetup({async:false});
		$.post("goods_addToCollGoodsExisting",{"fgoodsDTO.ctids":gids,"fgoodsDTO.mid":MId},function(data){
			if(data=="success"){
				alert("加入成功");
				$(".CMchuang").hide();
				window.location.href="goods_cart";
	}else{
				alert("加入失败");
				$(".CMchuang").hide();
	}
	});
	}else{
		alert("请选一种方式加入我的收藏夹");
			}
		}			
	}
	
	
	//确认添加到我的bom
function confirmAdd(){
	var url=window.location.href;
	var bomname = $("#bm_name").val();
	var gid = $("#cgid").val();
	var goodsNum=$("#goodsNum").val();
	var MId=$("input[name='mid']:checked").val();
	var checkedNum = $("input[name='mid']:checked").length;
	if(bomname != ""){
		if(checkedNum != 0){
			//alert("两种");
				$.ajaxSetup({async:false});
				$.post("bom_addToBomFromGoodsTwo",{"bomDTO.goodsNum":goodsNum,"bomDTO.bomTitle":bomname,"bomDTO.gid":gid,"bomDTO.mid":MId,"bomDTO.url":url},function(data){
					if(data=="success"){
					alert("加入成功");
					$(".BMchuang").hide();
					window.location.href=url;
			}else{
					alert("加入失败");
					$(".BMchuang").hide();
			}
			});
		}else{
			$.ajaxSetup({async:false});
			$.post("bom_addToBomFromGoodsNew",{"bomDTO.goodsNum":goodsNum,"bomDTO.bomTitle":bomname,"bomDTO.gid":gid,"bomDTO.url":url},function(data){
				if(data=="success"){
				alert("加入成功");
				$(".BMchuang").hide();
				window.location.href=url;
		}else{
				alert("加入失败");
				$(".BMchuang").hide();
		}
		});
		}
	}else{
		if(checkedNum != 0){
			//alert("以有的");
			$.ajaxSetup({async:false});
			$.post("bom_addToBomFromGoodsExisting",{"bomDTO.goodsNum":goodsNum,"bomDTO.mid":MId,"bomDTO.gid":gid,"bomDTO.url":url},function(data){
				if(data=="success"){
					alert("加入成功");
					$(".BMchuang").hide();
					window.location.href=url;
		}else{
					alert("加入失败");
					$(".BMchuang").hide();
		}
			   });
		}else{
			alert("请选一种方式加入我的bom");
		}
	}
}
function confirmAddColl(){
	var url=window.location.href;
	var collname=$("#coll_name").val();
	var gid=$("#cgid").val();
	var MId=$("input[name='mid']:checked").val();
	var checkedNum = $("input[name='mid']:checked").length;
	if(collname!=""){
		if(checkedNum != 0){
			$.ajaxSetup({async:false});
			$.post("goods_addToCollFromGoodsTwo",{"fgoodsDTO.colltitle":collname,"fgoodsDTO.mid":MId,"fgoodsDTO.gid":gid,"fgoodsDTO.url":url},function(data){
				if(data=="success"){
				alert("加入成功");
				$(".CMchuang").hide();
				window.location.href=url;
		}else{
				alert("加入失败");
				$(".CMchuang").hide();
		}
		});
				
		}else{
			$.ajaxSetup({async:false});
			$.post("goods_addToCollFromGoodsNew",{"fgoodsDTO.colltitle":collname,"fgoodsDTO.gid":gid,"fgoodsDTO.url":url},function(data){
				if(data=="success"){
				alert("加入成功");
				$(".CMchuang").hide();
				window.location.href=url;
		}else{
				alert("加入失败");
				$(".CMchuang").hide();
		}
		});
		}
}else{
	if(checkedNum!=0){
	$.ajaxSetup({async:false});
	$.post("goods_addToCollFromGoodsExisting",{"fgoodsDTO.mid":MId,"fgoodsDTO.gid":gid,"fgoodsDTO.url":url},function(data){
		if(data=="success"){
			alert("加入成功");
			$(".CMchuang").hide();
			window.location.href=url;
}else{
			alert("加入失败");
			$(".CMchuang").hide();
}
});
}else{
	alert("请选一种方式加入我的收藏夹");
}
}
}
function sortShu(arr){
	for(var i=0;i<arr.length;i++){
	    for(var j=0; j<arr.length; j++){
	    	if(arr[i] > arr[j]){
	    		var tem = arr[i];
	    		arr[i] = arr[j];
	    		arr[j]=tem;
	    	}
	    	
	    }
	}
	return arr;
}
function listsimAjax(gid,simCount){
	var endSum =  $("input[name="+gid+"gidENum]");
	if(Number(simCount) > Number(endSum[endSum.length-1].value)){
		alert("样品数量最大允许：" + endSum[endSum.length-1].value);
		//$("#"+gid).val("0");
		$("#"+gid).val(oldsim);
		$("#"+gid).attr('oldvalue', $("#"+gid).val());
		return;
	}
	if(simCount == ""){
		simCount = 0;
	}
	if(Number(simCount) <= 0){
		var p = $("#"+gid+"num").val();
		if(Number(p) == 0){
			alert("样品和批量数量不能同时为0");
			$("#"+gid).val(oldsim);
			$("#"+gid).attr('oldvalue', $("#"+gid).val());
			return;
		}
	}
	var allCount = $("input[name="+gid+"countSum]").val();
	var parCount = $("#"+gid+"num").val();
	var getSPar = $("input[name="+gid+"gidParSNum]");
	var kucun = true;
	if(typeof(getSPar) != "undefined"){
		var kc = $("#"+gid+"_kucun");
		if(typeof(kc[0].value) != "undefined"){
			if(kucun && parCount != 0 && Number(kc[0].value) < Number(getSPar[0].value / 1000)){
				kucun = false;
				alert("库存不足");
				$("#"+gid+"num").val(kc[0].value);
				parCount = kc[0].value;
				allCount = Number(kc[0].value)*1000;
				simCount = 0;
			}
			if(kucun && allCount != 0 && Number(kc[0].value) <= Number(allCount) / 1000 && simCount != 0){
				kucun = false;
				alert("库存不足");
				$("#"+gid+"num").val(kc[0].value);
				parCount = kc[0].value;
				allCount = Number(kc[0].value)*1000;
				simCount = 0;
			}
			if(kucun && Number(kc[0].value) < Number(parCount)){
				kucun = false;
				alert("库存不足");
				$("#"+gid+"num").val(kc[0].value);
				parCount = kc[0].value;
				allCount = Number(kc[0].value)*1000;
				simCount = 0;
			}
		}
	}
	
	
	//alert(allCount);
	var type = 0;
//	var goodsInfo = $("#"+gid+"pack1num").val();//包装数量  代替  pack1
	var goodsInfo = $("#"+gid+"pack1num").val();//包装数量  代替  pack1
	var sims = $("input[name="+gid+"gidSNum]");
	var sime = $("input[name="+gid+"gidENum]");
	var siminc = $("input[name="+gid+"gidSim]");
	var simprice = $("input[name="+gid+"gidSPrice]");
	
	var pars = $("input[name="+gid+"gidParSNum]");
	var pare = $("input[name="+gid+"gidParENum]");
	var parinc = $("input[name="+gid+"gidPar]");
	var parprice = $("input[name="+gid+"gidParPrice]");
	var compare = function (x, y) {
	    if (x < y) {
	        return 1;
	    } else if (x > y) {
	        return -1;
	    } else {
	        return 0;
	    }
	}
//	parprice.sort(compare);
//	parprice = sortShu(parprice);
	
	for ( var k = 0; k < sims.length; k++) {
		sims[k] = sims[k].value;
	}
	for ( var k = 0; k < sime.length; k++) {
		sime[k] = sime[k].value;
	}
	for ( var k = 0; k < siminc.length; k++) {
		siminc[k] = siminc[k].value;
	}
	for ( var k = 0; k < simprice.length; k++) {
		simprice[k] = simprice[k].value;
	}
	var t = 1;
	if(type == 1){
		t = goodsInfo / (pars[0].value * 1000);
	}
	for ( var k = 0; k < pars.length; k++) {
		pars[k] = pars[k].value * t;
	}
	for ( var k = 0; k < pare.length; k++) {
		pare[k] = pare[k].value * t;
	}
	for ( var k = 0; k < parinc.length; k++) {
		parinc[k] = parinc[k].value;
	}
	for ( var k = 0; k < parprice.length; k++) {
		parprice[k] = parprice[k].value / 1000;
	}
	
	parinc = sortShu(parinc);
	pars = sortShu(pars);
	pare = sortShu(pare);
	parprice = sortShu(parprice);
	 //临时修改为步长
	goodsInfo = parinc[0];
	var data = simCountInAjax(allCount, simCount, parCount, false, gid, sims,sime,siminc,simprice,pars,pare,parinc,parprice,goodsInfo)
	//20____4____4020____305____0____0____0.25____5____75____300
	//alert(data)
	var chai = data.toString().split("____");
	$("#"+gid).val(chai[0]);
	$("#"+gid+"num").val(chai[1]);
	$("div[id="+gid+"simPriceQu]").html("&yen;"+Number(chai[7]).toFixed(2));
	$("div[id="+gid+"xiaojiePriceTotal]").html("&yen;"+Number(chai[3]).toFixed(2));
	$("input[name="+gid+"countSum]").val(chai[2]);
	$("#"+gid+"_goodsPrice").val(Number(chai[3]).toFixed(2));
	//$("#totalNew").html("&yen;"+chai[3]);
	var cou =  $("input[name="+gid+"gidSNum]");
	var coupar =  $("input[name="+gid+"gidParSNum]");
	for ( var i = 0; i < cou.length; i++) {
		$("p[name="+gid + i +"isPriceHover]").removeClass("active");
	}
	for ( var i = 0; i < coupar.length; i++) {
		$("p[name="+gid+ i + "isPriceHoverPar]").removeClass("active");
	}
	if(simCount != 0){
		$("p[name="+gid+chai[4]+"isPriceHover]").addClass("active");
	}
	$("p[name="+gid+chai[5]+"isPriceHoverPar]").addClass("active");
	selectOneNew(1);
	//korpanid = chai[5];
//	$("input[name="+gid+"goodsSimPriceCount]").val(chai[7]);
//	$("input[name="+gid+"goodsParPriceCount]").val(chai[9]);
//	$("#"+gid+"newPriceSim").val(chai[6]);
//	$("#"+gid+"newPricePar").val(chai[8]);
	
	$.post(
			"${pageContext.request.contextPath}/updateCartFotAjax",
			{"cartToNum.GId":gid,
				"cartToNum.cartType":1,
				"cartToNum.GNumber":chai[0],
				"cartToNum.GPrice":chai[7],
				"cartToNum.GParPrice":chai[9],
				"cartToNum.GParNumber":typeof(chai[1]) != "undefined" ? Number(chai[1])*1000 : 0
			}
	);
	if(Number(chai[0]) == 0){
		$.post(
				"${pageContext.request.contextPath}/updateCartFotAjax",
				{"cartToNum.GId":gid,
				 "cartToNum.cartType":4
				}
		);
	}
	$("input[name="+gid+"countSum]").attr('oldvalue', chai[2]);
}
function listparAjax(gid,parCount){
	var allCount = $("input[name="+gid+"countSum]").val();
	var simCount = $("#"+gid).val();
	var getSPar = $("input[name="+gid+"gidParSNum]");
	var kucun = true;
	if(typeof(getSPar) != "undefined"){
		var kc = $("#"+gid+"_kucun");
		if(typeof(kc[0].value) != "undefined"){
			if(kucun && parCount != 0 && Number(kc[0].value) <= Number(getSPar[0].value / 1000)){
				alert("库存不足");
				$("#"+gid+"num").val(kc[0].value);
				parCount = kc[0].value;
				allCount = Number(kc[0].value)*1000;
				simCount = 0;
				kucun = false;
				$.post(
						"${pageContext.request.contextPath}/updateCartFotAjax",
						{"cartToNum.GId":gid,
						 "cartToNum.cartType":4
						}
				);
			}
			if(kucun && allCount != 0 && Number(kc[0].value) < Number(allCount) / 1000){
				alert("库存不足");
				$("#"+gid+"num").val(kc[0].value);
				parCount = kc[0].value;
				allCount = Number(kc[0].value)*1000;
				simCount = 0;
				kucun = false;
				$.post(
						"${pageContext.request.contextPath}/updateCartFotAjax",
						{"cartToNum.GId":gid,
						 "cartToNum.cartType":4
						}
				);
			}
			if(kucun && (Number(kc[0].value) < Number(parCount) || (Number(kc[0].value) == Number(parCount) && simCount != 0))){
				kucun = false;
				alert("库存不足");
				$("#"+gid+"num").val(kc[0].value);
				parCount = kc[0].value;
				allCount = Number(kc[0].value)*1000;
				simCount = 0;
				$.post(
						"${pageContext.request.contextPath}/updateCartFotAjax",
						{"cartToNum.GId":gid,
						 "cartToNum.cartType":4
						}
				);
			}
		}
	}
	if(Number(parCount) <= 0){
		var s = $("#"+gid).val();
		if(Number(s) == 0){
			alert("样品和批量数量不能同时为0");
			$("#"+gid+"num").val(oldpar);
			$("#"+gid+"num").attr('oldvalue', $("#"+gid+"num").val());
			return;
		}
		if(Number(parCount) < 0){
			alert("不能再小了");
			return;
		}
		
	}
	var type = 0;
	var goodsInfo = $("#"+gid+"pack1num").val();//包装数量  代替  pack1
	var sims = $("input[name="+gid+"gidSNum]");
	var sime = $("input[name="+gid+"gidENum]");
	var siminc = $("input[name="+gid+"gidSim]");
	var simprice = $("input[name="+gid+"gidSPrice]");
	
	var pars = $("input[name="+gid+"gidParSNum]");
	var pare = $("input[name="+gid+"gidParENum]");
	var parinc = $("input[name="+gid+"gidPar]");
	var parprice = $("input[name="+gid+"gidParPrice]");
	for ( var k = 0; k < sims.length; k++) {
		sims[k] = sims[k].value;
	}
	for ( var k = 0; k < sime.length; k++) {
		sime[k] = sime[k].value;
	}
	for ( var k = 0; k < siminc.length; k++) {
		siminc[k] = siminc[k].value;
	}
	for ( var k = 0; k < simprice.length; k++) {
		simprice[k] = simprice[k].value;
	}
	var t = 1;
	if(type == 1){
		t = goodsInfo / (pars[0].value * 1000);
	}
	for ( var k = 0; k < pars.length; k++) {
		pars[k] = pars[k].value * t;
	}
	for ( var k = 0; k < pare.length; k++) {
		pare[k] = pare[k].value * t;
	}
	for ( var k = 0; k < parinc.length; k++) {
		parinc[k] = parinc[k].value;
	}
	for ( var k = 0; k < parprice.length; k++) {
		parprice[k] = parprice[k].value / 1000;
	}
	parinc = sortShu(parinc);
	pars = sortShu(pars);
	pare = sortShu(pare);
	parprice = sortShu(parprice);
	goodsInfo = parinc[0];
	var data = simCountInAjax(allCount, simCount, parCount, false, gid, sims,sime,siminc,simprice,pars,pare,parinc,parprice,goodsInfo)
	var chai = data.toString().split("____");
	$("#"+gid).val(chai[0]);
	$("#"+gid+"num").val(chai[1]);
	$("div[id="+gid+"parPriceQu]").html("&yen;"+Number(chai[9]).toFixed(2));
	$("div[id="+gid+"xiaojiePriceTotal]").html("&yen;"+Number(chai[3]).toFixed(2));
	$("input[name="+gid+"countSum]").val(chai[2]);
	$("#"+gid+"_goodsPrice").val(Number(chai[3]).toFixed(2));
	var cou =  $("input[name="+gid+"gidSNum]");
	var coupar =  $("input[name="+gid+"gidParSNum]");
	for ( var i = 0; i < cou.length; i++) {
		$("p[name="+gid + i +"isPriceHover]").removeClass("active");
	}
	for ( var i = 0; i < coupar.length; i++) {
		$("p[name="+gid+ i + "isPriceHoverPar]").removeClass("active");
	}
	if(chai[0] != 0){
		$("p[name="+gid+chai[4]+"isPriceHover]").addClass("active");
	}
	$("p[name="+gid+chai[5]+"isPriceHoverPar]").addClass("active");
	//korpanid = chai[5];
	$("input[name="+gid+"goodsSimPriceCount]").val(chai[7]);
	$("input[name="+gid+"goodsParPriceCount]").val(chai[9]);
	$("#"+gid+"newPriceSim").val(chai[6]);
	$("#"+gid+"newPricePar").val(chai[8]);
	selectOneNew(1);
	$.post(
			"${pageContext.request.contextPath}/updateCartFotAjax",
			{"cartToNum.GId":gid,
				"cartToNum.cartType":2,
				"cartToNum.GParPrice":chai[9],
				"cartToNum.GParNumber":typeof(chai[1]) != "undefined" ? Number(chai[1])*1000 : 0
			}
	);
	if(Number(chai[1]) == 0){
		$.post(
				"${pageContext.request.contextPath}/updateCartFotAjax",
				{"cartToNum.GId":gid,
				 "cartToNum.cartType":5
				}
		);
	}
	$("input[name="+gid+"countSum]").attr('oldvalue', chai[2]);
}
var old = 0;
function change(gid){
	old = $("input[name="+gid+"countSum]").attr("oldvalue");
	//$('#B').val(Number(old) + 1);
	$("input[name="+gid+"countSum]").attr('oldvalue', $("input[name="+gid+"countSum]").val());
}
var oldsim = 0;
function changesim(gid){
	oldsim = $("#"+gid).attr("oldvalue");
	//$('#B').val(Number(old) + 1);
	$("#"+gid).attr('oldvalue', $("#"+gid).val());
}
var oldpar = 0;
function changepar(gid){
	oldpar = $("#"+gid+"num").attr("oldvalue");
	//$('#B').val(Number(old) + 1);
	$("#"+gid+"num").attr('oldvalue', $("#"+gid+"num").val());
}
function listAllCountAjax(gid,count){
	
	var getSPar = $("input[name="+gid+"gidParSNum]");
	if(typeof(getSPar) != "undefined"){
		var kc = $("#"+gid+"_kucun");
		if(typeof(kc[0].value) != "undefined"){
			if(Number(kc[0].value) < Number(count / 1000)){
				alert("库存不足");
				count = kc[0].value * 1000;
				$.post(
						"${pageContext.request.contextPath}/updateCartFotAjax",
						{"cartToNum.GId":gid,
						 "cartToNum.cartType":4
						}
				);
			}
		}
	}
	if(count == 0){
		alert("购物车数量不能为0哦！");
		$("input[name="+gid+"countSum]").val(old);
		$("input[name="+gid+"countSum]").attr('oldvalue', old);
		return;
	}
	//alert(gid);
	//alert(count);
	var type = 0;

	var goodsInfo = $("#"+gid+"pack1num").val();//包装数量  代替  pack1
	var sims = $("input[name="+gid+"gidSNum]");
	var sime = $("input[name="+gid+"gidENum]");
	var siminc = $("input[name="+gid+"gidSim]");
	var simprice = $("input[name="+gid+"gidSPrice]");
	
	var pars = $("input[name="+gid+"gidParSNum]");
	var pare = $("input[name="+gid+"gidParENum]");
	var parinc = $("input[name="+gid+"gidPar]");
	var parprice = $("input[name="+gid+"gidParPrice]");
	for ( var k = 0; k < sims.length; k++) {
		sims[k] = sims[k].value;
	}
	for ( var k = 0; k < sime.length; k++) {
		sime[k] = sime[k].value;
	}
	for ( var k = 0; k < siminc.length; k++) {
		siminc[k] = siminc[k].value;
	}
	for ( var k = 0; k < simprice.length; k++) {
		simprice[k] = simprice[k].value;
	}
	var t = 1;
	if(type == 1){
		t = goodsInfo / (pars[0].value * 1000);
	}
	for ( var k = 0; k < pars.length; k++) {
		pars[k] = pars[k].value  * t;
	}
	for ( var k = 0; k < pare.length; k++) {
		pare[k] = pare[k].value * t;
	}
	for ( var k = 0; k < parinc.length; k++) {
		parinc[k] = parinc[k].value;
	}
	for ( var k = 0; k < parprice.length; k++) {
		parprice[k] = parprice[k].value / 1000;
	}
	parinc = sortShu(parinc);
	pars = sortShu(pars);
	pare = sortShu(pare);
	parprice = sortShu(parprice);
	goodsInfo = parinc[0];
	
	var data = congAjax(count, type, gid, sims,sime,siminc,simprice,pars,pare,parinc,parprice,goodsInfo);
	
	var chai = data.toString().split("____");
	$("#"+gid).val(chai[0]);
	$("#"+gid+"num").val(chai[1]);
	$("div[id="+gid+"parPriceQu]").html("&yen;"+Number(chai[9]).toFixed(2));
	$("div[id="+gid+"simPriceQu]").html("&yen;"+Number(chai[7]).toFixed(2));
	$("div[id="+gid+"xiaojiePriceTotal]").html("&yen;"+Number(chai[3]).toFixed(2));
	$("#"+gid+"_goodsPrice").val(Number(chai[3]).toFixed(2));
	$("input[name="+gid+"countSum]").val(chai[2]);
	var cou =  $("input[name="+gid+"gidSNum]");
	var coupar =  $("input[name="+gid+"gidParSNum]");
	for ( var i = 0; i < cou.length; i++) {
		$("p[name="+gid + i +"isPriceHover]").removeClass("active");
	}
	for ( var i = 0; i < coupar.length; i++) {
		$("p[name="+gid+ i + "isPriceHoverPar]").removeClass("active");
	}
	if(count != 0){
		$("p[name="+gid+chai[4]+"isPriceHover]").addClass("active");
		$("p[name="+gid+chai[5]+"isPriceHoverPar]").addClass("active");
	}
	
	//korpanid = chai[5];
	$("input[name="+gid+"goodsSimPriceCount]").val(chai[7]);
	$("input[name="+gid+"goodsParPriceCount]").val(chai[9]);
	$("#"+gid+"newPriceSim").val(chai[6]);
	$("#"+gid+"newPricePar").val(chai[8]);
	selectOneNew(1);
	$.post(
			"${pageContext.request.contextPath}/updateCartFotAjax",
			{"cartToNum.GId":gid,
				"cartToNum.cartType":3,
				"cartToNum.GNumber":chai[0],
				"cartToNum.GPrice":chai[7],
				"cartToNum.GParPrice":chai[9],
				"cartToNum.GParNumber":typeof(chai[1]) != "undefined" ? Number(chai[1])*1000 : 0
			}
	);
	if(Number(chai[0]) == 0){
		$.post(
				"${pageContext.request.contextPath}/updateCartFotAjax",
				{"cartToNum.GId":gid,
				 "cartToNum.cartType":4
				}
		);
	}
	if(Number(chai[1]) == 0){
		$.post(
				"${pageContext.request.contextPath}/updateCartFotAjax",
				{"cartToNum.GId":gid,
				 "cartToNum.cartType":5
				}
		);
	}
	$("input[name="+gid+"countSum]").attr('oldvalue', chai[2]);
}
function cartSubCount(gid, type){
	var num = $("input[name="+gid+"countSum]").val();
	var sims = $("input[name="+gid+"gidSNum]");
	var sime = $("input[name="+gid+"gidENum]");
	var siminc = $("input[name="+gid+"gidSim]");
	
	var pars = $("input[name="+gid+"gidParSNum]");
	var pare = $("input[name="+gid+"gidParENum]");
	var parinc = $("input[name="+gid+"gidPar]");
	var tempi = 0;
	for(var j=0; j < parinc.length; j++){
		if(parinc[j].value != ""){
			tempi = j;
		}
	}
	
	if(type==1){
		var isok = false;
		if(Number(num) >= Number(pars[tempi].value)){
			num = Number(num) - Number(parinc[tempi].value);
			isok = true;
		}
		if(!isok){
			for(var i = 0; i < sims.length; i++){
				if(Number(num) >= Number(sims[i].value) && Number(num) <= Number(sime[i].value)){
					num = Number(num) - Number(siminc[i].value);
					break;
				}
			}
		}
		if(Number(num) <= 0){
			alert("样品和批量数量不能同时为0");
			return;
		}
		if(Number(num-parinc[0].value) <= 0){
			$.post(
					"${pageContext.request.contextPath}/updateCartFotAjax",
					{"cartToNum.GId":gid,
					 "cartToNum.cartType":5
					}
			);
		}
		listAllCountAjax(gid,num);
	} else if(type==2){
		var ps = 0;
		for(var i = 0; i < pars.length; i++){
			if(pars[i].value != ""){
				ps = i;
			}
		}
		if(Number(num) >= Number(pars[ps].value)){
			num = Number(num) + Number(parinc[ps].value);
		}
		for(var i = 0; i < sims.length; i++){
			if(Number(num) >= Number(sims[i].value) && Number(num) <= Number(sime[i].value)){
				num = Number(num) + Number(siminc[i].value);
				break;
			}
			if(Number(num) == 0){
				num = Number(num) + Number(siminc[0].value);
				break;
			}
		}
		listAllCountAjax(gid,num);
	} else if (type==3){
		num = $("#"+gid).val();
		var parnum = $("#"+gid+"num").val();
		if(Number(num-siminc[0].value) <= 0){
			if(Number(parnum) == 0){
				alert("样品和批量数量不能同时为0");
				return;
			} else {
				$.post(
						"${pageContext.request.contextPath}/updateCartFotAjax",
						{"cartToNum.GId":gid,
						 "cartToNum.cartType":4
						}
				);
			}
		}
		for(var i = 0; i < sims.length; i++){
			if(Number(num) >= Number(sims[i].value) && Number(num) <= Number(sime[i].value)){
				num = Number(num) - Number(siminc[i].value);
				break;
			}
		}
		listsimAjax(gid,num);
	} else if (type == 4){
		num = $("#"+gid).val();
		for(var i = 0; i < sims.length; i++){
			if(Number(num) >= Number(sims[i].value) && Number(num) <= Number(sime[i].value)){
				num = Number(num) + Number(siminc[i].value);
				break;
			}
			if(Number(num) == 0){
				num = Number(num) + Number(siminc[0].value);
				break;
			}
		}
		listsimAjax(gid,num);
	}  else if (type==5){
		num = $("#"+gid+"num").val();
		num = Number(num) - Number(parinc[tempi].value/1000);
		if(Number(num) <= 0){
			var simNum = $("#"+gid).val();
			if(Number(simNum) == 0){
				alert("样品和批量数量不能同时为0");
				return;
			} else {
				$.post(
						"${pageContext.request.contextPath}/updateCartFotAjax",
						{"cartToNum.GId":gid,
						 "cartToNum.cartType":5
						}
				);
			}
			
		}
		listparAjax(gid,Number(num));
	} else if (type == 6){
		num = $("#"+gid+"num").val();
		num = Number(num) + Number(parinc[tempi].value/1000);
		listparAjax(gid,Number(num));
	} 
	
}
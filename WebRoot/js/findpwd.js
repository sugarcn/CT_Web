function findpwd1(){
	if(checkUserName()){
		if(checkYzm()){
			$("#form").submit();
		}
	}
}
function checkUserName(){
	var flag = true;
	var username=$("#UUserid").val();
	if(username == ""){
		$("#usernametip").text("请输入用户名");
		flag = false;
	}else{
		$.ajaxSetup({async:false});
		$("#usernametip").text("");
		$.post("login_checkUserName",{"ctUser.UUserid":username},function(data){
				if(data=="success"){
					
				}else {
					$("#usernametip").text("用户名或者手机号不存在");
					//$("#username").val("");
					flag = false;
				}
		});
	}
	return flag;
}
//检测手机验证码是否正确
function checkUmbYzmIsEqual(){
	if(!isGetYzm){
		return;
	}
	var yzm = $("#sms").val();
	if(yzm.length > 0){
		 checkUmbYzmIsEqual2("输入的验证码不对请重新输入");
	}
}
function checkUmbYzmIsEqual2(str) {
	flag = true;
	var phoneNum = $("#umb").val();
	var yzm = $("#sms").val();
	$("#smstip").text("");
	$.ajaxSetup({async:false});
	$.post("login_checkYzmIsEqual",{"ctSms.sms":yzm,"ctUser.UMb":phoneNum},function(data){
		if(data=="success"){
		}else {
			$("#smstip").text(str);
			flag = false;
//			$("#sms").val("");
		}
	});
	return flag;
}
function checkYzm(){
	var flag = true;
	var yzm = $("#yzm").val();
	if(yzm == ""){
		$("#yzmtip").text("请输入验证码");
		flag = false;
	}else{
		$.ajaxSetup({async:false});
		$("#yzmtip").text("");
		$.post("login_checkYzm",{"yzm":yzm},function(data){
			if(data=="success"){
			}else {
				$("#yzmtip").text("输入的验证码不对");
				//$("#yzm").val("");
				showYzm()
				flag = false;
			}
		});
	}
	return flag;
}
//验证码
function showYzm() {
	document.getElementById("realyzm").src="common/image.jsp?id="+Math.random();
}


//
function gofindpwd3(){
	var type = $('input:radio:checked').val();
	if(type == "1"){
		document.forms[0].action = "login_gofindpwd3";
	}else if(type == "2") {
		document.forms[0].action = "login_gofindpwdByEmail";
	} else {
		document.forms[0].action = "";
		$("#isOkNext").attr("onclick","javascript:;");
		return;
	}
	$("#form").submit();
}
var wait=60; 
function time(o) { 
	if (wait == 0) { 
		o.removeAttribute("disabled");	
		o.value="　  获取验证码"; 
		wait = 60; 
	} else { 
		o.setAttribute("disabled", true); 
		o.value="　  重新发送(" + wait + ")"; 
		wait--; 
		setTimeout(function() { 
		time(o);
	}, 
	1000) 
	} 
} 
var huo = 300;
function timeOutYzm(o) { 
	if (huo == 0) { 
		huo = 300; 
		guoqi();
		checkUmbYzmIsEqual2("验证码超时，请重新输入");
		$("#sms").val("");
	} else { 
		huo--; 
		setTimeout(function() { 
		timeOutYzm(o);
	}, 
	1000) 
	} 
}
function guoqi(){
	var phoneNum = $("#umb").val();
	$.post("login_savePhoneYzm",{"ctSms.sms":"2222","ctUser.UMb":phoneNum});
}
//获取手机验证码
function getPhoneYzm(o){
	var phoneNum=$("#umb").val();
	$.post("login_savePhoneYzm",{"ctUser.UMb":phoneNum,"ctSms.sms":"1111"},function(data){
		if(data=="success"){
			isGetYzm = true;
			$("#sms").removeAttr("disabled");
			$("#sendYOrN").show();
			time(o);
			timeOutYzm(o);
			huo=300;
			//document.getElementById("msgtip").style.display="";
		}else {
			//$("#msptip2").text("获取验证码失败");
			$("#sendYOrN").hide();
		}
	});
}
//安全验证
function gofindpwd4(){
	var phoneNum=$("#umb").val();
	var yzm = $("#sms").val();
	if(yzm == ""){
		$("#smstip").text("请输入验证码");
	}else{
		//1.判断输入的验证码是否正确
		$("#smstip").text("");
		$.post("login_checkYzmIsEqual",{"ctSms.sms":yzm,"ctUser.UMb":phoneNum},function(data){
			if(data=="success"){
				//2.跳转到找回密码的下一页
				//$.post("<%=basePath%>login_findpasswd2",{"ctUser.UMb":phoneNum});
				$("#form").submit();
			}else {
				$("#smstip").text("输入的验证码不对请重新输入");
				$("#sms").val("");
			}
		});
		
	}
}
//更新密码
function updatePasswd(){
	if(checkPasswd()){
		if(checkPasswd2()){
			$("#form").submit();
		}
	}
}
function isSimplePwd(s){  
    if(s.length<6){  
        return 0;  
    }  
    var ls = 0;  
    if(s.match(/([a-z])+/)){  
        ls++;  
    }  
    if(s.match(/([0-9])+/)){  
        ls++;  
    }  
    if(s.match(/([A-Z])+/)){  
        ls++;  
    }  
    if(s.match(/[^a-zA-Z0-9]+/)){  
        ls++;  
    }  
    return ls;  
}  
//验证用户密码
function checkPasswd(){
	var flag = true;
	var passwd=$("#passwd").val();
	var index = isSimplePwd(passwd);
	if(index < 2){
		$("#passwdtip").text("密码过于简单，必须包括小写字母，大写字母或数字");
		flag = false;
		return flag;
	}
	if(passwd.length<6){
		$("#passwdtip").text("密码长度至少6位");
		flag = false;
//		$("#passwd").val("");
	}else {
		$("#passwdtip").text("");
	}
	return flag;
}
//验证确认密码是否相同
function checkPasswd2(){
	var flag = true;
	var passwd=$("#passwd").val();
	var passwd2=$("#passwd2").val();
	if(passwd != passwd2){
		$("#passwdtip2").text("两次输入的密码不一样");
		flag = false;
//		$("#passwd2").val("");
	}else{
		$("#passwdtip2").text("");
	}
	return flag;
}
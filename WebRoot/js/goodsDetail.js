function isLogin(){
	var gid = $("#gid").val();
	$.ajaxSetup({async:false});
	$.post("login_isLogin",function(data){
		if(data == "success"){
			//window.location.href="goods_detail?gid="+gid;
			//alert("登录");
			$.ajaxSetup({async:false});
		}else{
			$("#nexturl").val("goods_detail?fgoodsDTO.gid="+gid);
			$("#myModal").show();
		}
	});
}

//关闭窗口
function close(){
	$("#myModal").hide();
	$("#bgallfk").hide();
}
function closeCM(){
	$(".CMchuang").hide();
	$("#bgallfk").hide();
}
function closeBOM(){
	$(".BMchuang").hide();
	$("#bgallfk").hide();
}

//通过层登录
function loginByLayer(){
	var username=$("#username").val();
	var passwd=$("#passwd").val();
	if(username == "" || username == "用户名/手机/邮箱"){
		$("#usernametip").text("请输入用户名");
	}else {
		$("#usernametip").text("");
		if(passwd.length<6){
			$("#passwdtip").text("密码长度至少6位");
		}else {
			$("#passwdtip").text("");
			//$("#loginByLayerForm").submit();
			$.ajaxSetup({async:false});
			$.post("login_loginByLayer",{"userDTO.UUserid":username,"userDTO.UPassword":passwd},function(data){
				if(data == "success"){
					//alert("登录成功");
					var url = $("#gid").val();
					window.location.href="goods_detail?gid="+url;
				}else if(data == "error"){
					$("#msg").text("用户名或者密码错误");
				}else if(data == "mailnotactive"){
					$("#msg").text("邮箱没有激活");
				}else {
					$("#msg").text("非法登录");
				}
			});
		}
}
}

function isGetCoupom(){
	var url=window.location.href;
	var couponId = $("#choose").val();
//	alert(CDetailId);
	$.post("coupon_isGetCoupom",{"coupondetailDTO.couponId":couponId,"coupondetailDTO.url":url},function(data){
		if(data == "error"){
			//$("#choose").append("<option value=\"领取优惠券\" selected=\"selected\">领取优惠券</option>");
			alert("已经领取过优惠券");
		}else if(data == "nocoupon"){
			//$("#choose").append("<option value=\"领取优惠券\" selected=\"selected\">领取优惠券</option>");
			alert("优惠券已经被领完");
		}else if(data == "success"){
			alert("领取成功");
		}
	});
}

//加入我的bom
function addBom(){
	var url=window.location.href;
	var gid = $("#gid").val();
	var pack = $("#pa2").val();
	var num = $("#num").val();
	var goodsParNum = $("#"+gid+"num").val();
	var goodsNum = $("#"+num).val();
	$("#pack").val(pack);
	$("#goodsNum").val(num);
	if(false){
		alert("请选择包装！");
	}else{
		$.ajaxSetup({async:false});
		$.post("login_isLogin",{"url":url},function(data){
			if(data=="success"){
				$("#nexturl-bom").val("goods_detail?fgoodsDTO.gid="+gid);
				$("#bgallfk").show();
				$(".BMchuang").show();
			}else{
				$("#nexturl").val("goods_detail?fgoodsDTO.gid="+gid);
				$("#bgallfk").show();
				$("#myModal").show();
			}
		});
	}
}
//确认添加到我的bom
function confirmAdd(){
	var url=window.location.href;
	var bomname = $("#bm_name").val();
	var gid = $("#cgid").val();
	var goodsNum=$("#goodsNum").val();
	var MId=$("input[name='mid']:checked").val();
	var checkedNum = $("input[name='mid']:checked").length;
	if(bomname != ""){
		if(checkedNum != 0){
			//alert("两种");
				$.ajaxSetup({async:false});
				$.post("bom_addToBomFromGoodsTwo",{"bomDTO.goodsNum":goodsNum,"bomDTO.bomTitle":bomname,"bomDTO.gid":gid,"bomDTO.mid":MId,"bomDTO.url":url},function(data){
					if(data=="success"){
					alert("加入成功");
					$(".BMchuang").hide();
					window.location.href=url;
			}else{
					alert("加入失败");
					$(".BMchuang").hide();
			}
			});
//			$.ajax({ 
//			    type : "POST", 
//			    url : 'bom_addToBomFromGoodsTwo', 
//			    data :  $("#bom-form").serialize(),//序列化表单提交input 值 
//			    success : function(data) { 
//			    	alert("加入成功");
//			    	$(".BMchuang").hide();
//			    },
//			    error : function(data) { 
//			    	alert("加入失败");
//			    	$(".BMchuang").hide();
//			    } 
//			   });
		}else{
			$.ajaxSetup({async:false});
			$.post("bom_addToBomFromGoodsNew",{"bomDTO.goodsNum":goodsNum,"bomDTO.bomTitle":bomname,"bomDTO.gid":gid,"bomDTO.url":url},function(data){
				if(data=="success"){
				alert("加入成功");
				$(".BMchuang").hide();
				window.location.href=url;
		}else{
				alert("加入失败");
				$(".BMchuang").hide();
		}
		});
			//alert("新建的");
//			$.ajax({ 
//			    type : "POST", 
//			    url : 'bom_addToBomFromGoodsNew', 
//			    data :  $("#bom-form").serialize(),//序列化表单提交input 值 
//			    success : function(data) { 
//			    	alert("加入成功");
//			    	$(".BMchuang").hide();
//			    },
//			    error : function(data) { 
//			    	alert("加入失败");
//			    	$(".BMchuang").hide();
//			    } 
//			   });
		}
	}else{
		if(checkedNum != 0){
			//alert("以有的");
			$.ajaxSetup({async:false});
			$.post("bom_addToBomFromGoodsExisting",{"bomDTO.goodsNum":goodsNum,"bomDTO.mid":MId,"bomDTO.gid":gid,"bomDTO.url":url},function(data){
				if(data=="success"){
					alert("加入成功");
					$(".BMchuang").hide();
					window.location.href=url;
		}else{
					alert("加入失败");
					$(".BMchuang").hide();
		}
//			$.ajax({ 
//			    type : "POST", 
//			    url : 'bom_addToBomFromGoodsExisting', 
//			    data :  $("#bom-form").serialize(),//序列化表单提交input 值 
//			    success : function(data) { 
//			    	alert("加入成功");
//			    	$(".BMchuang").hide();
//			    },
//			    error : function(data) { 
//			    	alert("加入失败");
//			    	$(".BMchuang").hide();
//			    } 
			   });
		}else{
			alert("请选一种方式加入我的bom");
		}
	}
}

//收藏商品
function collect(GId){
		var url=window.location.href;
		var gid = $("#gid").val();
		var pack = $("#pa2").val();
		var num = $("#num").val();
		//$("#cartId").val(cartId);
		$("#pack").val(pack);
		$("#goodsNum").val(num);
		$("cgid").val(GId);
			$.ajaxSetup({async:false});
			$.post("login_isLogin",{"url":url},function(data){
				if(data=="success"){
					$("#nexturl-coll").val("goods_detail?gid="+GId);
					$("coll_name").val("");
					$("#bgallfk").show();
					$(".CMchuang").show();
				}else{
					$("#nexturl").val("goods_detail?gid="+GId);
					$("coll_name").val("");
					$("#bgallfk").show();
					$("#myModal").show();
					
				}
			});
//	$.ajaxSetup({async:false});
//	$.post("login_isLogin",function(data){
//		if(data=="success"){
//			$.post("goods_collect",{"fgoodsDTO.gid":gid},function(data){
//				if(data == "addsuccess"){
//					$("#isadd").text("成功加入");
//					$('.BMchuangshow').show();
//				}else if(data == "exist"){
//					$("#isadd").text("已经加入");
//					$('.BMchuangshow').show();
//				}else{
//					alert("添加失败");
//				}
//			});
//		}else{
//			$("#nexturl").val("goods_detail?fgoodsDTO.gid="+gid);
//			$("#myModal").show();
//		}
//	});
}
//批量加入我的BOM
function addGoodsToMany(){
	var url=window.location.href;
	var box =  $("input[size=5]");
	var gids = "";
	for (var i=0;i<box.length;i++) {
		if(box[i].checked&&box[i].value!="c1"&&box[i].value!="c2"&&box[i].value!="c3"){
			var giddd = $(box[i]).parent().siblings(".gou_ct_2").children("a").children("input")[0].value;
			gids = gids + "," + giddd;
		}
	}
	var bomname = $("#bm_bname").val();
		var MId=$("input[name='mid']:checked").val();
		var checkedNum = $("input[name='mid']:checked").length;
		if(bomname!=""){
			if(checkedNum != 0){
				$.ajaxSetup({async:false});
				$.post("bom_addToBomGoodsTwo",{"bomDTO.ctids":gids,"bomDTO.bomTitle":bomname,"bomDTO.mid":MId,"bomDTO.url":url},function(data){
					if(data=="success"){
					alert("加入成功");
					$(".GMchuang").hide();
					window.location.href="goods_cart";
			}else{
					alert("加入失败");
					$(".GMchuang").hide();
			}
			});
					
			}else{
				$.ajaxSetup({async:false});
				$.post("bom_addToBomGoodsNew",{"bomDTO.ctids":gids,"bomDTO.bomTitle":bomname,"bomDTO.url":url},function(data){
					if(data=="success"){
					alert("加入成功");
					$(".GMchuang").hide();
					window.location.href="goods_cart";
			}else{
					alert("加入失败");
					$(".GMchuang").hide();
			}
			});
			}
	}else{
		if(checkedNum!=0){
		$.ajaxSetup({async:false});
		$.post("bom_addToBomGoodsExisting",{"bomDTO.ctids":gids,"bomDTO.mid":MId,"bomDTO.url":url},function(data){
			if(data=="success"){
				alert("加入成功");
				$(".GMchuang").hide();
				window.location.href="goods_cart";
	}else{
				alert("加入失败");
				$(".GMchuang").hide();
	}
	});
	}else{
		alert("请选一种方式加入我的bom");
			}
		}			
}
//批量加入收藏夹
function favorites(){
	var url=window.location.href;
	var box =  $("input[size=5]");
	var gids = "";
	for (var i=0;i<box.length;i++) {
		if(box[i].checked&&box[i].value!="c1"&&box[i].value!="c2"&&box[i].value!="c3"){
			var giddd = $(box[i]).parent().siblings(".gou_ct_2").children("a").children("input")[0].value;
			gids = gids + "," + giddd;
		}
	}
		var collname=$("#coll_gname").val();
		var MId=$("input[name='mid']:checked").val();
		var checkedNum = $("input[name='mid']:checked").length;
		if(collname!=""){
			if(checkedNum != 0){
				$.ajaxSetup({async:false});
				$.post("goods_addToCollGoodsTwo",{"fgoodsDTO.ctids":gids,"fgoodsDTO.colltitle":collname,"fgoodsDTO.mid":MId,"fgoodsDTO.url":url},function(data){
					if(data=="success"){
					alert("加入成功");
					$(".SMchuang").hide();
					window.location.href="goods_cart";
			}else{
					alert("加入失败");
					$(".SMchuang").hide();
			}
			});
					
			}else{
				$.ajaxSetup({async:false});
				$.post("goods_addToCollGoodsNew",{"fgoodsDTO.ctids":gids,"fgoodsDTO.colltitle":collname},function(data){
					if(data=="success"){
					alert("加入成功");
					$(".SMchuang").hide();
					window.location.href="goods_cart";
			}else{
					alert("加入失败");
					$(".SMchuang").hide();
			}
			});
			}
	}else{
		if(checkedNum!=0){
		$.ajaxSetup({async:false});
		$.post("goods_addToCollGoodsExisting",{"fgoodsDTO.ctids":gids,"fgoodsDTO.mid":MId},function(data){
			if(data=="success"){
				alert("加入成功");
				$(".SMchuang").hide();
				window.location.href="goods_cart";
	}else{
				alert("加入失败");
				$(".SMchuang").hide();
	}
	});
	}else{
		alert("请选一种方式加入我的收藏夹");
			}
		}			
	}
//	var box=document.getElementsByName("aa");
//	var bo=document.getElementsByName("bb");
//	for(var i=0;i<box.length;i++){
//	if(box[i].checked){
//		var gid=box[i].value;
//		alert(gid);
//		}
//	}
//	for(var j=0;j<bo.length;j++){
//		if(bo[j].checked){
//			var gid=bo[j].value;
//			alert(gid);
//			}
//		}

//	var gids="";
//	for(var i=0;i<box.length;i++){
//	if(box[i].checked){
//		var gid=box[i].value;
//		if(gid!="c1"&&gid!="c2"){
//			$.ajaxSetup({async:false});
//			$.post("goods_favorites",{"fgoodsDTO.gid":gid},function(data){
//				if(data=="addsuccess"){
//					$("#isadd").text("成功加入");
//			}else if(data=="exist"){
//					$("isadd").text("已经加入");
//					$('.BMchuangshow').show();
//			}else{
//					alert("添加失败");
//			}
//});
//}else{
//		$("#nexturl").val("goods_detail?fgoodsDTO.gid="+gid);
//		$("myModal").show();
//}
//});
//}
//}
//}
//	for(var i=0;i<box.length;i++){
//		if(box[i].checked&&box[i].value!="c1"&&box[i].value!="c2"){
//			gids=gids+","+box[i].value;
//}
//}
//	if(gids==""){
//	alert("请至少选择一个商品！")
//}else{
//	$.post("login_isLongin",function(data){
//		if(data=="success"){
//	$.post("goods_collect",{"fgoodsDTO.gid":gid},function(dara)){
//		if(data=="addsuccess"){
//			$("#isadd").text("成功加入");
//			$('.BMchuangshow').show();
//}else if(data=="exist"){
//
//}else{
//	alert("添加失败");
//}
//});
//}else{
//	$("#nexturl").val("goods_detail?fgoodsDTO.gid="+gid);
//	$("#myModal").show();
//}
//});
//}
//}
//确认添加到我的收藏夹
function confirmAddColl(){
	var url=window.location.href;
	var collname=$("#coll_name").val();
	var gid=$("#cgid").val();
	var MId=$("input[name='mid']:checked").val();
	var checkedNum = $("input[name='mid']:checked").length;
	if(collname!=""){
		if(checkedNum != 0){
			$.ajaxSetup({async:false});
			$.post("goods_addToCollFromGoodsTwo",{"fgoodsDTO.colltitle":collname,"fgoodsDTO.mid":MId,"fgoodsDTO.gid":gid,"fgoodsDTO.url":url},function(data){
				if(data=="success"){
				alert("加入成功");
				$(".CMchuang").hide();
				window.location.href=url;
		}else{
				alert("加入失败");
				$(".CMchuang").hide();
		}
		});
				
		}else{
			$.ajaxSetup({async:false});
			$.post("goods_addToCollFromGoodsNew",{"fgoodsDTO.colltitle":collname,"fgoodsDTO.gid":gid,"fgoodsDTO.url":url},function(data){
				if(data=="success"){
				alert("加入成功");
				$(".CMchuang").hide();
				window.location.href=url;
		}else{
				alert("加入失败");
				$(".CMchuang").hide();
		}
		});
		}
}else{
	if(checkedNum!=0){
	$.ajaxSetup({async:false});
	$.post("goods_addToCollFromGoodsExisting",{"fgoodsDTO.mid":MId,"fgoodsDTO.gid":gid,"fgoodsDTO.url":url},function(data){
		if(data=="success"){
			alert("加入成功");
			$(".CMchuang").hide();
			window.location.href=url;
}else{
			alert("加入失败");
			$(".CMchuang").hide();
}
});
}else{
	alert("请选一种方式加入我的收藏夹");
}
}
}


function subnum(){
	var goodsNum = $("#num").val();
	if(goodsNum < 2){
		alert("数量不能小于1");
		return ;
	}
	var newgoodsNum = Number(goodsNum) - 1;
	$("#num").val(newgoodsNum);
}
function addnum(){
	var goodsNum = $("#num").val();
	var newgoodsNum = Number(goodsNum) + 1;
	$("#num").val(newgoodsNum);
}

function load(){
	document.getElementById("p1").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
	document.getElementById("p2").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
	document.getElementById("p3").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
	document.getElementById("p4").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
}
function setpack(indx,num,num2){
	document.getElementById("pa").value=indx;
	document.getElementById("pn").value=num;
	document.getElementById("pa2").value=num2;
	if(indx==1){
		if(document.getElementById("p2")!=null){
			document.getElementById("p2").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		if(document.getElementById("p3")!=null){
			document.getElementById("p3").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		if(document.getElementById("p4")!=null){
			document.getElementById("p4").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		document.getElementById("p1").style.cssText = "margin-right:8px; display:block; border:2px solid #da082f; height:26px; line-height:26px; float:left; border-radius:3px; padding:0 14px; color:#da082f; background:url(${systemInfo.ctImgUrl }/bg/xuanzhong_bg.gif) no-repeat right bottom;";
	}else if(indx==2){
		if(document.getElementById("p1")!=null){
			document.getElementById("p1").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		if(document.getElementById("p3")!=null){
			document.getElementById("p3").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		if(document.getElementById("p4")!=null){
			document.getElementById("p4").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		document.getElementById("p2").style.cssText = "margin-right:8px; display:block; border:2px solid #da082f; height:26px; line-height:26px; float:left; border-radius:3px; padding:0 14px; color:#da082f; background:url(${systemInfo.ctImgUrl }/bg/xuanzhong_bg.gif) no-repeat right bottom;";
	}else if(indx==3){
		if(document.getElementById("p2")!=null){
			document.getElementById("p2").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		if(document.getElementById("p1")!=null){
			document.getElementById("p1").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		if(document.getElementById("p4")!=null){
			document.getElementById("p4").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		document.getElementById("p3").style.cssText = "margin-right:8px; display:block; border:2px solid #da082f; height:26px; line-height:26px; float:left; border-radius:3px; padding:0 14px; color:#da082f; background:url(${systemInfo.ctImgUrl }/bg/xuanzhong_bg.gif) no-repeat right bottom;";
	}else if(indx==4){
		if(document.getElementById("p2")!=null){
			document.getElementById("p2").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		if(document.getElementById("p3")!=null){
			document.getElementById("p3").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		if(document.getElementById("p1")!=null){
			document.getElementById("p1").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		document.getElementById("p4").style.cssText = "margin-right:8px; display:block; border:2px solid #da082f; height:26px; line-height:26px; float:left; border-radius:3px; padding:0 14px; color:#da082f; background:url(${systemInfo.ctImgUrl }/bg/xuanzhong_bg.gif) no-repeat right bottom;";
	}
}


	function addcart(){
		//var cc = document.getElementById("pa2").value;
		//var pack = document.getElementById("pa").value;
		var tar = document.getElementById("gid").value;
		var goodsNum = $("#"+tar).val();
		var parCount1 = $("#"+tar + "num").val();
		var isXuan = false;
		if(goodsNum == '0' && parCount1 == '0'){
			isXuan = false;
		} else {
			isXuan = true;
		}
		if(!isXuan){
			alert("请选择数量");
		}else{
	$.ajaxSetup({async:false});
	$.post("login_isLogin",function(data){
		
		//var pp = document.getElementById("pp").value;
		//var num = document.getElementById("num").value;
		//var pn = document.getElementById("pn").value;
		var goodsParNum = $("#"+tar+"num").val();
		var goodsNum = $("#"+tar).val();
		var pack = "";
		if(goodsParNum == 0 && goodsNum != 0){
			pack="1--0";
		} else if(goodsNum == 0 && goodsParNum != 0) {
			pack="0--1";
		} else {
			pack="1--1";
		}
		if(isPanOrK){
			goodsParNum = goodsParNum * 5;
		}
		var num = goodsNum + "--" + goodsParNum;
		var simPrice = $("#goodsSimPriceCount").val();
		var parPrice = $("#goodsParPriceCount").val();
		
		var ap = simPrice + "--" + parPrice;
		var da = tar+"=="+pack+"=="+num+"=="+ap;
		if(data == "success"){
			$.post("add_cart",{"data":da},function(date,varstart){
				if(date == "success"){
					$(".BMchuangshowgw").show();
				}
			});
			//location.href="add_cart?data="+da;
		}else{
			$("#bgallfk").show();
			$("#myModal").show();
			$("#nexturl").val("add_cart?data="+da);
		}
	});
	}
}
	function goBack(){
		$(".BMchuangshowgw").hide();
	}
	function toCart(){
		location.href="goods_cart";
		$("#pa2").val("");
		$("#pa").val("");
	}
	function toColl(){
		location.href="goods_collectlist";
	}


function downLoadFileAjax(rid){
	$.post(
		"FileDownload",
		{"number":rid},
		function(data,varstart){
			//alert(data);
			alert("文件已经成功下载至 c:\\Program Files下");
		}
	);
}
function searchGoodsXuan(){
	var pid = document.getElementById("pid").value;
	var kword = document.getElementById("kword").value;
	var kwordXuan = document.getElementById("kwordXuan").value;
	if(kwordXuan == null || kwordXuan == ""){
		alert("搜索关键字不能为空");
		return false;
	}else{
		document.getElementByName("searchFormXuan").submit();
	}
}

function float_nav(dom){
	var right_nav=$(dom);
	var nav_height=right_nav.height();
	function right_nav_position(bool){
		var window_height=$(window).height();
		var nav_top=(window_height-nav_height)/0;
		if(bool){
			right_nav.stop(true,false).animate({top:nav_top+$(window).scrollTop()},400);
		}else{
			right_nav.stop(true,false).animate({top:nav_top},300);
		}	
		right_nav.show();
	}
	
	if(!+'\v1' && !window.XMLHttpRequest ){
		$(window).bind('scroll resize',function(){
			if($(window).scrollTop()>300){
				right_nav_position(true);			
			}else{
				right_nav.hide();	
			}
		})
	}else{
		$(window).bind('scroll resize',function(){
			if($(window).scrollTop()>300){
				right_nav_position();
			}else{
				right_nav.hide();
			}
		})
	}	
}



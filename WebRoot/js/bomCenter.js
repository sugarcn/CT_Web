//Bom中心详情
function goBomCenterDetail(bomId){
	window.location.href="bomcenter_goBomCenterDetail?bomDTO.bomId="+bomId;
}

//加入我的bom
function addToMyBom(url,bomId){
	$.ajaxSetup({async:false});
	$.post("login_isLogin",{"bomDTO.url":url},function(data){
		if(data == "success"){
			window.location.href=url+"?bomDTO.bomId="+bomId;
		}else{
			$("#myModal").show();
			$("#nexturl").val(url+"?bomDTO.bomId="+bomId);
		}
	});
	//window.location.href="bomcenter_addToMyBom?bomDTO.bomId="+bomId;
}

//通过层登录
function loginByLayer(){
	var username=$("#username").val();
	var passwd=$("#passwd").val();
	if(username == "" || username == "用户名/手机/邮箱"){
		$("#usernametip").text("请输入用户名");
	}else {
		$("#usernametip").text("");
		if(passwd.length<6){
			$("#passwdtip").text("密码长度至少6位");
		}else {
			$("#passwdtip").text("");
			//$("#loginByLayerForm").submit();
			$.ajaxSetup({async:false});
			$.post("login_loginByLayer",{"userDTO.UUserid":username,"userDTO.UPassword":passwd},function(data){
				if(data == "success"){
					//alert("登录成功");
					var url = $("#nexturl").val();
					window.location.href=url;
				}else if(data == "error"){
					$("#msg").text("用户名或者密码错误");
				}else if(data == "mailnotactive"){
					$("#msg").text("邮箱没有激活");
				}else {
					$("#msg").text("非法登录");
				}
			});
		}
}
}

//关闭窗口
function close(){
	$("#myModal").hide();
}

//搜索
function searchBomCenter(){

		var checkedNum = $("#keyword").attr("value");; 
		if(checkedNum == "") { 
			//alert("请输入你要查询的关键词！"); 
			document.forms[2].action = "bomcenter_searchBC?page=1";
    		document.forms[2].submit();
			//return; 
		}else{
			var params={
				"keyword":checkedNum
			}
			 document.forms[2].action = "bomcenter_searchBC?page=1";
    		document.forms[2].submit();
		} 
		
	}

$(function(){ 

	$("#searchSelected").click(function(){ 
		$("#searchTab").show();
		$(this).addClass("searchOpen");
	}); 

	$("#searchTab li").hover(function(){
		$(this).addClass("selected");
	},function(){
		$(this).removeClass("selected");
	});
	 
	$("#searchTab li").click(function(){
		$("#searchSelected").html($(this).html());
		$("#searchTab").hide();
		$("#searchSelected").removeClass("searchOpen");
	});
});
 function showProvince(basePath,regionId){
              $.post(basePath+"queryPro_region?regionId="+regionId,function(json){
              var list=json.plist;
              for(var i=0;i<list.length;i++){
              $("#province").append("<option value='"+list[i].regionId+"'>"+list[i].regionName+"</option>");
              }
			},"json");
         }
         
         function showCity(basePath,regionId){
        	 $("#city").empty();
             $("#city").append("<option value='0'>---请选择---</option>");
             $("#district").empty();
             $("#district").append("<option value='0'>---请选择---</option>");
             if(regionId != 0){
            	 if(regionId != 0){
            		 $.post(basePath+"queryPro_region?regionId="+regionId,function(json){
            			 var list=json.plist;
            			 for(var i=0;i<list.length;i++){
            				 $("#city").append("<option value='"+list[i].regionId+"'>"+list[i].regionName+"</option>");
            			 }
            		 },"json");
            	 }
             }
         }
         function showDistrict(basePath,regionId){
        	 $("#district").empty();
             $("#district").append("<option value='0'>---请选择---</option>");
             if(regionId != 0){
            	 $.post(basePath+"queryPro_region?regionId="+regionId,function(json){
            		 var list=json.plist;
            		 if(list.length!=0){
            			 for(var i=0;i<list.length;i++){
            				 $("#district").show();
            				 $("#district").append("<option value='"+list[i].regionId+"'>"+list[i].regionName+"</option>");
            			 }
            		 }else{
            			 $("#district").empty();
            			 $("#district").append("<option value='-1'>---请选择---</option>");
            			 $("#district").hide();
            		 }
            	 },"json");
             }
         }
         function showProvince2(basePath,regionId){
        	 
              $.post(basePath+"/queryPro_region",{"regionId":regionId},function(json){
              var list=json.plist;
              for(var i=0;i<list.length;i++){
              $("#province2").append("<option value='"+list[i].regionId+"'>"+list[i].regionName+"</option>");
              }
			},"json");
         }
         
         function showCity2(basePath,regionId){
         $("#city2").empty();
         $("#city2").append("<option value='0'>---请选择---</option>");
         $("#district2").empty();
         $("#district2").append("<option value='0'>---请选择---</option>");
         $.post(basePath+"queryPro_region?regionId="+regionId,function(json){
              var list=json.plist;
              for(var i=0;i<list.length;i++){
              $("#city2").append("<option value='"+list[i].regionId+"'>"+list[i].regionName+"</option>");
              }
			},"json");
         }
     function showDistrict2(basePath,regionId){
         $("#district2").empty();
         $("#district2").append("<option value='0'>---请选择---</option>");
          $.post(basePath+"queryPro_region?regionId="+regionId,function(json){
              var list=json.plist;
              if(list.length!=0){
              for(var i=0;i<list.length;i++){
              $("#district2").show();
              $("#district2").append("<option value='"+list[i].regionId+"'>"+list[i].regionName+"</option>");
              }
              }else{
              $("#district2").empty();
         	  $("#district2").append("<option value='-1'>---请选择---</option>");
              $("#district2").hide();
              }
			},"json");
         }      


function saveadd(){
	var country2 = $("#country2").val();
	var province2 = $("#province2").val();
	var city2 = $("#city2").val();
	var district2 = $("#district2").val();
	var address2 = $("#address2").val();
	var zipcode2 = $("#zipcode2").val();
	var AConsignee2 = $("#AConsignee2").val();
	var tel2 = $("#tel2").val();
	var mb2 = $("#mb2").val();
	var adesc=$("#desc").val();
	if(country2 == "0"){
		$("#region2Tip").text("请选择收货地址");
		return;
	}
	if(province2 == "0"){
		$("#region2Tip").text("请选择收货地址");
		return;
	}
	if(city2 == "0"){
		$("#region2Tip").text("请选择收货地址");
		return;
	}
	if(district2 == "0"){
		$("#region2Tip").text("请选择收货地址");
		return;
	}
	if(address2 == ""){
		$("#region2Tip").text("");
		$("#address2Tip").text("请填写详细地址");
		return;
	}
	if(!checkSpe(address2)){
		$("#region2Tip").text("");
		$("#address2Tip").text("详细地址不能包含非法字符");
		return;
	}
	if(zipcode2==""){
		$("#address2Tip").text("");
		$("#zipcode2Tip").text("");
		$("#zipcode2Tip").text("请填写邮编");
		return;
	}
	if(!checkZip(zipcode2)){
		$("#zipcode2Tip").text("");
		$("#zipcode2Tip").text("请填写有效邮编");
		return;
	}
	if(AConsignee2 == ""){
		$("#zipcode2Tip").text("");
		$("#AConsignee2Tip").text("请填写收货人姓名");
		return;
	}
	if(!checkSpe(AConsignee2)){
		$("#address2Tip").text("");
		$("#AConsignee2Tip").text("收货人姓名不能包含非法字符");
		return;
	}
	if(tel2 =="" && mb2==""){
		$("#AConsignee2Tip").text("");
		$("#mb2Tip").text("手机、电话两者至少填一项");
		return;
	}
	if (AConsignee2!= ""){
		var len = AConsignee2.replace(/[^\x00-\xff]/g, "**").length;
		if(len >= 15){
			$("#AConsignee2Tip").text("收货人姓名过长");
			return;
		}
	}
	if(tel2 != "" && mb2==""){
			$("#mb2Tip").text("");
			if(!(/^1[3|5|7|8|9][0-9]\d{8}$/.test(tel2))){
				$("#tel2Tip").text("手机号不合法");
				return;
			}
	}
	if(mb2 != "" &&  tel2==""){
				$("#mb2Tip").text("");
				if(!(/^(\d{3,4}\-?)?\d{7,8}$/.test(mb2))){
					$("#mb2Tip").text("电话号码不合法");
					return;
			}
	}
	if(tel2!="" && mb2!=""){
			$("#mb2Tip").text("");
			if(!(/^1[3|5|7|8|9][0-9]\d{8}$/.test(tel2))){
				$("#tel2Tip").text("手机号不合法");
				return;
			}
			if(!(/^(\d{3,4}\-?)?\d{7,8}$/.test(mb2))){
					$("#mb2Tip").text("电话号码不合法");
					return;
			}
		}
	$("#addressform").attr("action","address_add");
	$("#addressform").submit();
}
function updateAddress1(basepath,aid){
	$('.PKchuang').hide();
	$('.BMchuangxzer').show();
	$('.BMchuangxz').hide();
	$("#AId").val(aid);
	$.post("address_getAddressByAid",{"AId":aid},function(data){
		if(data != "error"){
			$("#country").find("option:contains('"+data[0]+"')").attr("selected",true);
			
			showProvince(basepath,data[1]);
			showCity(basepath,data[3]);
			//$("#province").append("<option selected=\"true\" value="+data[3]+">"+data[2]+"</option>");
			//$("#city").append("<option selected=\"true\" value="+data[5]+">"+data[4]+"</option>");
			showDistrict(basepath,data[5])
			//if(data[4]!=data[6]){
			//$("#district").append("<option selected=\"true\" value="+data[7]+">"+data[6]+"</option>");
			//}else{
				//showDistrict(basepath,data[7])
			//$("#district").append("<option selected=\"true\" value="+data[7]+">"+data[6]+"</option>");
			//$("#district").hide();
			//}
			
			var all_options = document.getElementById("province").options;
			for (i=0; i<all_options.length; i++){
				if (all_options[i].value == data[3]) // 根据option标签的ID来进行判断 测试的代码这里是两个等号
				{
					all_options[i].selected = true;
					}
				}
			all_options = document.getElementById("city").options;
			for (i=0; i<all_options.length; i++){
				if (all_options[i].value == data[5]) // 根据option标签的ID来进行判断 测试的代码这里是两个等号
				{
					all_options[i].selected = true;
				}
			}
			all_options = document.getElementById("district").options;
			for (i=0; i<all_options.length; i++){
				if (all_options[i].value == data[7]) // 根据option标签的ID来进行判断 测试的代码这里是两个等号
				{
					all_options[i].selected = true;
				}
			}
			$("#address").val(data[8]);
			$("#zipcode").val(data[10]);
			$("#AConsignee").val(data[11]);
			$("#tel").val(data[12]);
			$("#bm").val(data[13]);
			$("#adesc").val(data[9]);
		}
	});
}

	$('#btn_bxzer').on('click', function(){
		$('.PKchuang').hide();
		$('.BMchuangxzer').show();
		$('.BMchuangxz').hide();
	});
	$('.lchaxz').on('click', function(){
		$('.PKchuang').hide();
		$('.BMchuangxzer').hide();
	});


function ShowDIV(thisObjID) {
$("#BgDiv").css({ display: "block", height: $(document).height() });
var yscroll = document.documentElement.scrollTop;
$("#" + thisObjID).css("top", "100px");
$("#" + thisObjID).css("display", "block");
document.documentElement.scrollTop = 0;
}

function closeDiv(thisObjID) {
$("#" + thisObjID).css("display", "none");
$("#BgDiv").hide();
}	

$(function(){
		$('#liebiao_type').change(function(){
			load_zuopin(1);
		});
	});
	$('#btn_bmc').on('click', function(){
		$('.BMchuang').show();
		$('.BMchuangshow').hide();
		$('.BMchuangshowgw').hide();
	});
	$('.lcha').on('click', function(){
		$('.BMchuang').hide();
		$('.BMchuangshow').hide();
		$('.BMchuangshowgw').hide();
	});
	
		$('#btn_bxz').on('click', function(){
		$('.PKchuang').hide();
		$('.BMchuangxz').show();
		$('.BMchuangxzer').hide();
	});
	$('.lchaxz').on('click', function(){
		$('.PKchuang').hide();
		$('.BMchuangxz').hide();
	});
	

	
	$('#btn_bm').on('click', function(){
		$('.PKchuang').hide();
		$('.BMchuangdi_f').show();
	});
	$('.lchadi_f').on('click', function(){
		$('.PKchuang').hide();
		$('.BMchuangdi_f').hide();
	});
	
	
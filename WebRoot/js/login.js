﻿
/*
window.onload = function(){   
          var usernameobj = document.getElementById("usernameobj");    
          function addEvent(el,evtType,fn) {  
          if (el.addEventListener) {  //FF  
             el.addEventListener(evtType,fn, false);  
               return;  
          }else if(el.attachEvent) {  //IE  
               el.attachEvent("on" + evtType,fn, false);  
               return;  
          }  
      }  
      function enterSubmit(evt){  
           if(evt.keyCode==13){  
			  evt.keyCode = 9;
              evt.returnValue = false;
              login();
           }  
		   
        }  
           
          addEvent(usernameobj,'keydown',function(evt){  
             enterSubmit(evt);  
          }); 
		  
}
*/
		  
//让客户经理联系我
function contacts(){
	//var aaa = "中国";
	//alert(aaa.length);
	var url=window.location.href;
	var em=$("#email").val();
	var mb=$("#mbo").val();
	var nr=$("#detail").val();
	if(em=="" && mb!=""){
		em = " ";
		if(!(/^1[3|5|7|8|9][0-9]\d{8}$/.test(mb))){
			$("#ts").text("");
			$("#ts").text("请正确输入手机格式。");
		}else if(nr==""){
			$("#ts").text("");
			$("#ts").text("请填写备注");
		}else if(!checkSpe(nr)){
			$("#ts").text("");
			$("#ts").text("备注不能包含非法字符");
		}else{
			$.ajaxSetup({async:false});
			$.post("add_contact",{"em":em,"mb":mb,"nr":nr,"url":url},function(data){
				if(data=="success"){
					alert("提交成功，我们会尽快与您联系！");
					window.location.href=url;
				}else if(data="exits"){
					alert("对不起，您当天提交次数过多，客户经理会尽快和你联系");
					window.location.href=url;
				}else{
					alert("提交失败！");
					window.location.href=url;
				}
		});
		}
	}else if(em!="" && mb==""){
		mb = " ";
		if(!(/^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3})(\]?)$/.test(em))){
			$("#ts").text("");
			$("#ts").text("邮箱格式不正确");
		}else if(nr==""){
			$("#ts").text("");
			$("#ts").text("请填写备注");
		}else if(!checkSpe(nr)){
			$("#ts").text("");
			$("#ts").text("备注不能包含非法字符");
		}else{
			$.ajaxSetup({async:false});
			$.post("add_contact",{"mb":mb,"em":em,"nr":nr,"url":url},function(data){
				if(data=="success"){
					alert("提交成功，我们会尽快与您联系！");
					window.location.href=url;
				}else if(data="exits"){
					alert("对不起，您当天提交次数过多，客户经理会尽快和你联系");
					window.location.href=url;
				}else{
					alert("提交失败！");
					window.location.href=url;
				}
		});
		}
	}else if(em=="" && mb==""){
		$("#ts").text("");
		$("#ts").text("您必须留下一种联系方式!");
	}else if(nr==""){
		$("#ts").text("");
		$("#ts").text("请填写备注");
	}else if(!checkSpe(nr)){
		$("#ts").text("");
		$("#ts").text("备注不能包含非法字符");
	}else{
		$.ajaxSetup({async:false});
		$.post("add_contact",{"mb":mb,"em":em,"nr":nr,"url":url},function(data){
			if(data=="success"){
				alert("提交成功，我们会尽快与您联系！");
				window.location.href=url;
			}else if(data="exits"){
				alert("对不起，您当天提交次数过多，客户经理会尽快和你联系");
				window.location.href=url;
			}else{
				alert("提交失败！");
				window.location.href=url;
			}
	});
	}
}

//意见建议
function advice(){
	var url=window.location.href;
	var name=$("#name").val();
	var mb=$("#mb").val();
	var em=$("#em").val();
	var nr=$("#nr").val();
	if(name==""){
		$("#yz").text("");
		$("#yz").text("请填写姓名或称呼。");
	}else if(!checkSpe(name)){
		$("#yz").text("");
		$("#yz").text("姓名或称呼不能包含非法字符");
	}else if(mb==""){
		$("#yz").text("");
		$("#yz").text("请填写手机号，以便与您取得联系。");
	}else if(!/^1[3|5|7|8|9][0-9]\d{8}$/.test(mb)){
		$("#yz").text("");
		$("#yz").text("手机格式不合法！");
	}else if(em==""){
		$("#yz").text("");
		$("#yz").text("请填写邮箱，以便与您取得联系。");
	}else if(!(/^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3})(\]?)$/.test(em))){
		$("#yz").text("");
		$("#yz").text("邮箱格式不正确！");
	}else if(nr=="" || nr=="限120字。"){
		$("#yz").text("");
		$("#yz").text("请填写反馈意见内容，根据您的反馈，我们给予你专业的回复。");
	}else if(!checkSpe(nr)){
		$("#yz").text("");
		$("#yz").text("反馈意见不能包含非法字符。");
	}else{
	$.ajaxSetup({async:false});
		$.post("add_advice",{"name":name,"mb":mb,"em":em,"nr":nr,"url":url},function(data){
			if(data=="success"){
				alert("提交成功！感谢您的宝贵意见及建议！");
				window.location.href=url;
			}else{
				alert("抱歉，提交失败！请重试。");
				window.location.href=url;
			}
	});
	}
}
//验证码
function showYzm() {
	if(isLoginType == 0){
		document.getElementById("realyzm").src="common/image.jsp?id="+Math.random();
	} else {
		document.getElementById("realyzmNoPass").src="common/image.jsp?id="+Math.random();
	}
}
function checkLogin(){
	flag = true;
	var uname=$("#username").val();
	if(uname == ""){
		$(".msg-up").show();
		$(".msg-up").text("用户名或密码不正确，请重新输入");
		showYzm();
		flag=false;
	}
}
function checkpwd(){
	flag=true;
	var uname=$("#username").val();
	var pwd=$("#passwd").val();
	if(pwd == ""){
		$(".msg-up").show();
		$(".msg-up").text("请输入密码");
		$("#passwd").val("");
		showYzm();
	}
	
}
//检验验证码是否相同
function checkYzm(){
	flag = true;
	var yzm = $("#yzm").val();
	if(yzm == ""){
		$(".msg-up").show();
		$(".msg-up").text("请输入验证码！");
		//$("#yzmtip").text("请输入验证码！");
		flag = false;
	}else{
		$.ajaxSetup({async:false});
		$.post("login_checkYzm",{"yzm":yzm},function(data){
			if(data=="success"){
				$(".msg-up").hide();
				$(".msg-up").text("");
			}else {
				$(".msg-up").show();
				$(".msg-up").text("验证码输入有误，请重新输入！");
			//	$("#yzmtip").text("验证码输入有误，请重新输入！");
				$("#yzm").val("");
				showYzm();
				flag = false;
			}
		});
	}
	return flag;
}
function getPhoneYzm(o){
	if(yanzhengOk){
		var phoneNum = $("#umb").val();
		if(phoneNum=="" || phoneNum == "请输入手机号码"){
			$("#umbtip").text("请输入手机号码");
			isGetYzm = false;
			
		}else{
			$("#umbtip").text("");
			$.post("login_savePhoneYzm",{"ctSms.sms":"1111","ctUser.UMb":phoneNum});
			isGetYzm = true;
			$("#sms").removeAttr("disabled");
			time(o);
			timeOutYzm(o);
			huo = 300;
		}
		
		
		isGuo = true;
	} else {
		$("#yzmtip1").text("请输入验证码");
	}
}
function goYzmInfo(o){
	if(flag){
		var phone = $("#userPhoneNoPass").val();
		$("#umbtip").text("");
		$.post("login_savePhoneYzm",{"ctSms.sms":"1111","ctUser.UMb":phone});
		isGetYzm = true;
		time(o);
		timeOutYzm(o);
		huo = 300;
	} else {
		var yzm = $("#yzmNoPass").val();
		if(yzm == ""){
			$(".msg-up").show();
			$(".msg-up").text("请输入验证码！");
			flag = false;
			return;
		}
		var phone = $("#userPhoneNoPass").val();
		if(!(/^1[3|5|7|8|9][0-9]\d{8}$/.test(phone))){
			$(".msg-up").show();
			$(".msg-up").text("手机号不合法");
			flag = false;
			return;
		}
	}
}
//检验验证码是否相同
function checkYzmNoPass(){
	flag = true;
	var yzm = $("#yzmNoPass").val();
	if(yzm == ""){
		$(".msg-up").show();
		$(".msg-up").text("请输入验证码！");
		//$("#yzmtip").text("请输入验证码！");
		flag = false;
	}else{
		$.ajaxSetup({async:false});
		$.post("login_checkYzm",{"yzm":yzm},function(data){
			if(data=="success"){
				$(".msg-up").hide();
				$(".msg-up").text("");
				$("#dt_passwowd").attr("disabled",false);
				flag = true;
			}else {
				$(".msg-up").show();
				$(".msg-up").text("验证码输入有误，请重新输入！");
				//	$("#yzmtip").text("验证码输入有误，请重新输入！");
				$("#yzmNoPass").val("");
				showYzm();
				flag = false;
			}
		});
	}
	return flag;
}

function LoginSubmit()
{ 
          if (event.keyCode == 13) 
          { 
              event.keyCode = 9;
              event.returnValue = false;
              login();
          } 
   } 
function checkPhone(phone){
	if(!(/^1[3|5|7|8|9][0-9]\d{8}$/.test(phone))){
		$(".msg-up").show();
		$(".msg-up").text("手机号不合法");
		flag = false;
	} else {
		$(".msg-up").hide();
		$(".msg-up").text("");
		flag = true;
	}
}
function login(){
	
	var username=$("#username").val();
	var passwd=$("#passwd").val();
	if(username == "" || username == "用户名/手机/邮箱"){
		//$("#usernametip").text("请输入用户名");
		$(".msg-up").show();
		$(".msg-up").text("请输入用户名");
		showYzm();
	}else {
	//	$("#usernametip").text("");
		$(".msg-up").text("");
		$(".msg-up").hide();
		if(passwd.length<6){
			//$("#passwdtip").text("密码长度至少6位");
			$(".msg-up").show();
			$(".msg-up").text("密码长度至少6位");
			showYzm();
		}else {
			//$("#passwdtip").text("");
			$(".msg-up").hide();
			$(".msg-up").text("");
			if(checkYzm()){
				$.post("login_checkLogin",{"uname":username},function(data){
					if(data=="success"){
						$(".msg-up").hide();
						$(".msg-up").text("");
						$.post("login_checkpwd",{"uname":username,"pwd":passwd},function(data){
							if(data=="success"){
								$(".msg-up").hide();
								$(".msg-up").text("");
								if($("#checkbox").attr("checked") == "checked"){
									$("#isAutoLogin").val("autoLogin");
									//$("#form").submit();
									$("#Myform").submit();
								}else{
										$("#isAutoLogin").val("notAutoLogin");
										//$("#form").submit();
										$("#Myform").submit();
									}
								}else{
									if(data == "error_EN"){
										$(".msg-up").show();
										$(".msg-up").text("登陆失败次数过多，请稍后尝试！！");
									} else if(data == "error_NN") {
										$(".msg-up").show();
										$(".msg-up").text("账户已锁定，您可以修改密码或者等待解锁");
									} else {
										$(".msg-up").show();
										$(".msg-up").text("用户名或密码不正确，请重新输入");
									}
									$("#yzm").val("");
								//$("#passwd").val("");
								showYzm();
								flag=false;
							}
						});
					}else{
						if(data == "error_EN"){
							$(".msg-up").show();
							$(".msg-up").text("登陆失败次数过多，请稍后尝试！！");
						} else if(data == "error_NN") {
							$(".msg-up").show();
							$(".msg-up").text("当前账户已锁定，您可以修改密码或者等待自动解锁");
						} else {
							$(".msg-up").show();
							$(".msg-up").text("用户名或密码不正确，请重新输入");
						}
						$("#yzm").val("");
						//$("#username").val("");
						//$("#passwd").val("");
						showYzm();
						flag=false;
					}
				});
		}
	}
}
}



var yanzhengOk = false;
var wait=60; 
function time(o) { 
if (wait == 0) { 
	o.setAttribute("onclick", "goYzmInfo(this)"); 
	$(o).html("发送动态密码"); 
	wait = 60; 
} else { 
	o.removeAttribute("onclick");	
	//o.value="　  重新发送(" + wait + ")"; 
	$(o).html("重新发送("+wait+")");
	wait--; 
	setTimeout(function() { 
	time(o);
}, 
1000) 
} 
} 
var huo = 300;
function timeOutYzm(o) { 
if (huo == 400) { 
	huo = 300; 
	//guoqi();
	checkUmbYzmIsEqual2("验证码超时，请重新输入");
	$("#dt_passwowd").val("");
} else { 
	huo--; 
	setTimeout(function() { 
	timeOutYzm(o);
}, 
1000) 
} 
}
var isGuo = false;
function guoqi(){
	var phoneNum = $("#umb").val();
	$.post("login_savePhoneYzm",{"ctSms.sms":"2222","ctUser.UMb":phoneNum});
}
var isGetYzm = false;

//检测手机验证码是否正确
function checkUmbYzmIsEqual(){
	if(!isGetYzm){
		return;
	}
	var yzm = $("#userPhoneNoPass").val();
	if(yzm.length > 0){
		 checkUmbYzmIsEqual2("输入的验证码不对请重新输入");
	}
}

function checkUmbYzmIsEqual2(str) {
	flag = true;
	var phoneNum = $("#userPhoneNoPass").val();
	var yzm = $("#dt_passwowd").val();
	$(".msg-up").text("");
	$(".msg-up").hide();
	$.ajaxSetup({async:false});
	$.post("login_checkYzmIsEqual",{"ctSms.sms":yzm,"ctUser.UMb":phoneNum},function(data){
		if(data=="success"){
		} else if (data == "chaoshi"){
			//guoqi();
			$(".msg-up").show();
			$(".msg-up").text(str);
			return flag;
		}
		else {
			$(".msg-up").show();
			$(".msg-up").text(str);
			flag = false;
//			$("#sms").val("");
		}
	});
	return flag;
}


function loginNoPass(){
	if(flag){
		var phoneNum = $("#userPhoneNoPass").val();
		var yzm = $("#dt_passwowd").val();
		if(yzm == ""){
			$(".msg-up").show();
			$(".msg-up").text("请输入验证码");
			return;
		}
		$.post("noPassLoginOrReg",{"userPhone":phoneNum,"yzmNoPass":yzm},function(data, varstart){
			if(data == "success"){
				window.location.href="webindex";
			} else {
				$(".msg-up").show();
				$(".msg-up").text(data);
			}
		});
	}
}

//跳转到注册页面

//跳转到忘记密码页面

//退出

//跳转到登录页面

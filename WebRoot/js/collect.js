function goCollectDetail(collid){
	window.location.href="goods_goCollectDetail?collid="+collid;
}
//编辑Collect
function editCollect(){
	var url=window.location.href;
	$("#colltitletip").text("");
	var checkedNum=$("input[name='mid']:checked").length;
	if(checkedNum==0){
		alert("请选择一项！");
		return false;
	}else if(checkedNum>1){
		alert("请选择一个进行修改");
		return false;
	}else{
	
		$("#bgallfk").show();
		$('.BMchuang').show();
		$("#xz").hide();
		$("#xg").show();
		var MId=$("input[name='mid']:checked")[0].value;
		$.ajaxSetup({async:false});
		$.post("goods_editCollect",{"fgoodsDTO.mid":MId,"fgoodsDTO.url":url},function(data){
			$("#colltitle").val(data[0].colltitle);
			$("#colldesc").val(data[0].colldesc);
			$("#collid").text(data[0].collid);
		});
		
	}
}
//保存修改
function saveUpdate(){
	var url=window.location.href;
	$("#colltitletip").text("");
	var colltitle=$("#colltitle").val();
	var colldesc=$("#colldesc").val();
	var collid=$("#collid").text();
	if(colltitle==""){
	$("#colltitletip").text("请输入收藏夹名称");
}else if(colldesc==""){
	$("#colltitletip").text("");
	$("#colldesctip").text("请输入收藏夹介绍");
}else{
	$("#colldesctip").text("");
		$.ajaxSetup({asyn:false});
		if(checkCollTitle()){
		$.post("goods_saveUpdate",{"fgoodsDTO.collid":collid,"fgoodsDTO.colltitle":colltitle,"fgoodsDTO.colldesc":colldesc,"fgoodsDTO.url":url},function(data){
	if(data=="success"){
	$(".BMchuang").hide();
	$("#colltitle").val("");
	$("#colldesc").val("");
	window.location.href="goods_collectlist";
}else{
	alert("修改失败");
			}
		});
		}
	}
}
//增加收藏夹
function saveColl(){
	$("#colltitletip").text("");
		$("#bgallfk").show();
		$('.BMchuang').show();
		$("#xg").hide();
		$("#xz").show();
		$.ajaxSetup({async:false});
		$("#colltitle").val("");
		$("#colldesc").val("");
		$("#collid").text("");
}
//新增收藏夹
function saveAdd(){
	var url=window.location.href;
	$("#colltitletip").text("");
	var colltitle=$("#colltitle").val();
	var colldesc=$("#colldesc").val();
	if(colltitle=="" || colltitle=="null"){
		$("#colltitletip").text("请输入收藏夹名称");
	}else if(colldesc=="" || colldesc=="null"){
		$("#colldesctip").text("");
		$("#colldesctip").text("请输入收藏夹介绍");
	}else{
		$("#colldesctip").text("");
		$.ajaxSetup({async:false});
		if(checkCollTitle()){
			$.post("goods_saveColl",{"fgoodsDTO.colltitle":colltitle,"fgoodsDTO.colldesc":colldesc,"fgoodsDTO.url":url},function(data){
				if(data="success"){
					$(".BMchuang").hide();
					window.location.href="goods_collectlist";
				}else{
					$(".reveal-modal-bg").show();
					$("#nexturl").val("bom_list");
					$("myModal").show();
				}
			});
		}
	}
}
function deleteColl(){
	var checkedNum=$("input[name='mid']:checked").length;
	if(checkedNum==0){
	alert("请选择至少一项！");
	return;
}
	if(!confirm("确认要删除么？")){
	return false;
}else{
	document.forms[1].action="goods_delColl";
	document.forms[1].submit();
}
}
function searchColl(){
	var checkedNum=$("#keyword").attr("value");
	if(checkSpe(checkedNum)){
	if(checkedNum==""){
	document.forms[1].action="goods_searchColl?page=1";
	document.forms[1].submit();	
}else{
	var params={"keyword":checkedNum}
	document.forms[1].action="goods_searchColl?page=1";
	document.forms[1].submit();
}
	}else{
		alert("您输入的关键字不合法！");
	}
}
function checkCollTitle(){
	var flag=true;
	var collTitle=$("#colltitle").val();
	var collid=$("#collid").text();
	if(checkSpe(collTitle)){
		$.ajaxSetup({async:false});
		$.post("goods_checkCollTitle",{"fgoodsDTO.colltitle":collTitle,"collid":collid},function(data){
			if(data=="exists"){
				$("#colltitletip").text("收藏夹已存在请重新输入");
				flag=false;
			}else{
				$("#colltitletip").text("");	
			}
		});
	}else{
		$("#colltitletip").text("含有非法字符，请重新输入！");
		flag=false;
	}
	return flag;
}



//弹出加入购物车
function addCart(gid){
	var url=window.location.href;
	$.ajaxSetup({async:false});
	$.post("login_isLogin",function(data){
		if(data == "success"){
			$("#BgDiv").show();
			$("#ddd").show();
			$(".tizp,.ti_tixiao").show();	
			$(".BMchuang_tishi").hide();
			$("#pa").val("");
			$("#pn").val("");
			var b1 = $("#b1").val();
			var b11 = b1.split("K")[0];
			var b2 = $("#b2").val();
			var b21 = b2.split("K")[0];
			var b3 = $("#b3").val();
			var b31 = b3.split("K")[0];
			var b4 = $("#b4").val();
			if(typeof(b4) != "undefined"){
				var b41 = b4.split("K")[0];
			}
			$(".baozx_d").html("");
			var html = "";
			if(typeof(b1)!="undefined"){
				html =html + "<a href='javascript:void()'><span id='p1' onclick='setpack(1,\""+b11+"\")'>"+b1+"</span></a>";
			}
			if(typeof(b2)!="undefined"){
				html =html + "<a href='javascript:void()'><span id='p2' onclick='setpack(2,\""+b21+"\")'>"+b2+"</span></a>";
			}
			if(typeof(b3)!="undefined"){
				html =html + "<a href='javascript:void()'><span id='p3' onclick='setpack(3,\""+b31+"\")'>"+b3+"</span></a>";
			}
			if(typeof(b4)!="undefined"){
				html =html + "<a href='javascript:void()'><span id='p4' onclick='setpack(4,\""+b41+"\")'>"+b4+"</span></a>";
			}
			$(".baozx_d").append(html);
			
			$.post("goods_getGoodsNum",{"fgoodsDTO.gid":gid,"fgoodsDTO.url":url},function(data){
				if(data != "error"){
					$("#Gnum").text(data[0]);
				}else {
					$("#Gnum").text("加载失败");
				}
			});
		}else{
			$(".reveal-modal-bg").show();
			$("#myModal").show();
			$("#nexturl").val("goods_collectlist");
		}
	});
	
	
}
//加入购物车
function addcart(){
	var url=window.location.href;
	var pack = document.getElementById("pa").value;
	var tar = $("#gid").val();
	if(pack==""){
		alert("请选择包装");
	}else{
$.ajaxSetup({async:false});
$.post("login_isLogin",{"url":url},function(data){
	
	var pp = $("#"+tar+"PP").val();
	var num = document.getElementById("num").value;
	var pn = document.getElementById("pn").value;
	
	var ap = pp*pn*num;
	var da = tar+"=="+pack+"=="+num+"=="+ap;
	if(data == "success"){
		$.post("cart_addCartASYN",{"orderDTO.data":da,"url":url},function(data){
			if(data == "addsuccess"){
//				alert("加入成功");
				$.post("goods_getCartGoodsNum",function(data){
					//此处查询共有多少个宝贝
					if(data != "error"){
						$("#carGNum").text(data[0]);
					}else{
						$("#carGNum").text("加载失败");
					}
				});
				$("#BgDiv").show();
				$(".tizp").hide();
				$(".BMchuang_tishi").show();
			}else{
				alert("加入失败");
			}
		});
	}else{
		$("#BgDiv").hide();
		$("#ddd").hide();
		$(".reveal-modal-bg").show();
		$("#myModal").show();
		$("#nexturl").val("goods_collectlist");
	}
	
});
}
}

function subnum(){
	var goodsNum = $("#num").val();
	if(goodsNum < 2){
		alert("数量不能小于1");
		return ;
	}
	var newgoodsNum = Number(goodsNum) - 1;
	$("#num").val(newgoodsNum);
}
function addnum(){
	var goodsNum = $("#num").val();
	var newgoodsNum = Number(goodsNum) + 1;
	$("#num").val(newgoodsNum);
}

function setpack(indx,num){
	document.getElementById("pa").value=indx;
	document.getElementById("pn").value=num;
	if(indx==1){
		if(document.getElementById("p2")!=null){
			document.getElementById("p2").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		if(document.getElementById("p3")!=null){
			document.getElementById("p3").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		if(document.getElementById("p4")!=null){
			document.getElementById("p4").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		document.getElementById("p1").style.cssText = "margin-right:8px; display:block; border:2px solid #da082f; height:26px; line-height:26px; float:left; border-radius:3px; padding:0 14px; color:#da082f; background:url(${systemInfo.ctImgUrl }/bg/xuanzhong_bg.gif) no-repeat right bottom;";
	}else if(indx==2){
		if(document.getElementById("p1")!=null){
			document.getElementById("p1").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		if(document.getElementById("p3")!=null){
			document.getElementById("p3").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		if(document.getElementById("p4")!=null){
			document.getElementById("p4").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		document.getElementById("p2").style.cssText = "margin-right:8px; display:block; border:2px solid #da082f; height:26px; line-height:26px; float:left; border-radius:3px; padding:0 14px; color:#da082f; background:url(${systemInfo.ctImgUrl }/bg/xuanzhong_bg.gif) no-repeat right bottom;";
	}else if(indx==3){
		if(document.getElementById("p2")!=null){
			document.getElementById("p2").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		if(document.getElementById("p1")!=null){
			document.getElementById("p1").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		if(document.getElementById("p4")!=null){
			document.getElementById("p4").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		document.getElementById("p3").style.cssText = "margin-right:8px; display:block; border:2px solid #da082f; height:26px; line-height:26px; float:left; border-radius:3px; padding:0 14px; color:#da082f; background:url(${systemInfo.ctImgUrl }/bg/xuanzhong_bg.gif) no-repeat right bottom;";
	}else if(indx==4){
		if(document.getElementById("p2")!=null){
			document.getElementById("p2").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		if(document.getElementById("p3")!=null){
			document.getElementById("p3").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		if(document.getElementById("p1")!=null){
			document.getElementById("p1").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		document.getElementById("p4").style.cssText = "margin-right:8px; display:block; border:2px solid #da082f; height:26px; line-height:26px; float:left; border-radius:3px; padding:0 14px; color:#da082f; background:url(${systemInfo.ctImgUrl }/bg/xuanzhong_bg.gif) no-repeat right bottom;";
	}
}
	$('.lcha').live('click', function(){
		$('.BMchuang').hide();
		$('.BMchuangshow').hide();
		$('.BMchuangshowgw').hide();
		$('.bgallfk').hide();

	});


	

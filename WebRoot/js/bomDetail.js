//删除Bom详情中的某个商品
function deleteThis(){
var bomId = $("#bomId").val();
		var checkedNum = $("input[name='bomGoodsDTO.mid']:checked").length; 
		if(checkedNum == 0) { 
			alert("请选择至少一项！"); 
			return; 
		} 
         if (!confirm("确认要删除么？")) {
              return false;
         }else{
			  document.forms[2].action = "bom_delGoodsOfBomDetail?bomDTO.bomId="+bomId;
			  document.forms[2].submit();
		}
}
//搜索
function searchThis(){
	var bomId = $("#bomId").val();
		var checkedNum = $("#keyword").attr("value");
		if(checkSpe(checkedNum)){
		if(checkedNum == "" || checkedNum== "订单编号/收货人/商品编号/商品名称") { 
			 document.forms[1].action = "bom_searchGoodsOfBomDetail?bomGoodsDTO.page=1&bomGoodsDTO.bomId="+bomId;
	    	document.forms[1].submit();
			//alert("请输入你要查询的关键词！"); 
			//return; 
		}else{
			var params={
				"keyword":checkedNum
			}
			 document.forms[1].action = "bom_searchGoodsOfBomDetail?bomGoodsDTO.page=1&bomGoodsDTO.bomId="+bomId;
    		document.forms[1].submit();
		} 
		}else{
			alert("输入的关键字不合法，请重新输入");
		}
}

//更新Pack
function getPack(bomGoodsId){
	
	var pack = $("#"+bomGoodsId).val();
	//var GId = $("#GId").text();
	$.post("bom_savePack",{"bomGoodsDTO.pack":pack,"bomGoodsDTO.bomGoodsId":bomGoodsId});
}

//数量加1
function addNum(num,bomGoodsId){
	var url=window.location.href;
	var bomId = $("#bomId").val();
	var goodsNum = $("#"+num).val();
	var newgoodsNum = Number(goodsNum);
	$.post("updateGoodsNumNew",{"bomGoodsDTO.updateType":"add","bomGoodsDTO.goodsNum":newgoodsNum,"bomGoodsDTO.bomGoodsId":bomGoodsId,"bomGoodsDTO.bomId":bomId,"url":url},function(data){
		var str = data.split("_");
		if(str[0] == "success"){
			$("#"+num).val(str[1]);
		}
		
	});
}
//数量减1
function subNum(num,bomGoodsId){
	var url=window.location.href;
	var bomId = $("#bomId").val();
	var goodsNum = $("#"+num).val();
	if(goodsNum <= 1){
		alert("数量不能小于1");
		return ;
	}
	var newgoodsNum = Number(goodsNum);
	$.post("updateGoodsNumNew",{"bomGoodsDTO.updateType":"sub","bomGoodsDTO.goodsNum":newgoodsNum,"bomGoodsDTO.bomGoodsId":bomGoodsId,"bomGoodsDTO.bomId":bomId,"url":url},function(data){
		var str = data.split("_");
		if(str[0] == "success"){
			$("#"+num).val(str[1]);
		}
		
	});
}
//更新数量
function updateNum(num,bomGoodsId){
	var url=window.location.href;
	var goodsNum = $("#"+num).val();
	var bomId = $("#bomId").val();
	if(goodsNum < 1){
		alert("数量不能小于1");
		return ;
	}
	$.post("bom_updateGoodsNum",{"bomGoodsDTO.goodsNum":goodsNum,"bomGoodsDTO.bomGoodsId":bomGoodsId,"bomGoodsDTO.bomId":bomId,"url":url},function(data){
		if(data == "success"){
			$("#"+num).val(goodsNum);
		}
		
	});
}
//详情新增商品
function add(){
	var url=window.location.href;
	$("#url").val(url);
	flag = setGidByGname();
	if(flag){
		var GId = $("#GId").val();
		var bomId = $("#bomId").val();
		document.forms[0].action = "bom_addGoodsOfBomDetail?bomGoodsDTO.bomId="+bomId+"&bomGoodsDTO.GId="+GId;
		document.forms[0].submit();
	}
	
}

//自动弹出下拉菜单
function getGoodsByGname(){
	var GName = $("#GName").val();
	$(".tan_sou").html("");
	$.post("goods_getGoodsByGname",{"fgoodsDTO.GName":GName},function(data){
		//data = eval(data);
		var html = "";
		$(".tan_sou").css("display","block");
		for(var i=0;i<data.length;i++){
			html = html + "<li onclick=\"javascript:setGid("+data[i][0]+",\'"+data[i][1]+"\')\"><span >"+data[i][1]+"</span></li>";
			$(".tan_sou").html(html);
		}
	});
}
//设置下拉菜单的商品Gid
function setGid(Gid,Gname){
	$("#GId").val(Gid);
	$("#GName").val(Gname);
	
	$(".tan_sou").css("display","none");
}
//直接通过Gname设置Gid
function setGidByGname(){
	flag = true;
	$(".tan_sou").css("display","none");
	var GName = $("#GName").val();
	if(checkSpe(GName)){
	$.ajaxSetup({async:false});
	$.post("goods_getOneGoodsByGname",{"fgoodsDTO.GName":GName},function(data){
		if(data == "error"){
			alert("你输入的商品不存在！");
			flag = false;
		}else {
			$("#GId").val(data[0]);
		}
		
	});
	}else{
		alert("输入的字符不合法，请重新输入");
		flag=false;
	}
	return flag;
//	$.ajax({
//		type:"post",
//		url:"goods_getOneGoodsByGname",
//		async:false,
//		data:{"fgoodsDTO.GName":GName},
//		success:function(data){
//			$("#GId").val(data[0]);
//		},
//		error:function(){
//			flag=false;
//		}
//	});
}


//全选
function SelectAll() {
	 var checkboxs=document.getElementsByName("bomGoodsDTO.mid");
	 for (var i=0;i<checkboxs.length;i++) {
	 		if(checkboxs[i].checked){
	 			for (var i=0;i<checkboxs.length;i++) {
	 				checkboxs[i].checked=false;
	 			}
	 		}else{
	 			for (var i=0;i<checkboxs.length;i++) {
	 				checkboxs[i].checked="checked";
	 			}
	 		}
	 }
}
$(document).ready(function(){
	//判断浏览器是否IE
    $.fn.isIE();
    //判断有没有安装flash插件(自调函数)
    /*$.fn.fun=(function(){
        var fls = $.fn.hasFlash();
        var s = "";
        if (fls.f) {
    	    //console.log("您安装了flash,当前flash版本为: " + fls.v + ".x");
    	    var timer=setTimeout(function(){
	            $("#fl").css("display","none");
	            if($("#fl").css("display")=="none") {
	                $("#duihua>a").fadeIn(1000);
	            }
	            $.fn.clickme();
	        },9300);
        }
        else {
    	    $("#fl").css("display","none");
		    var timer=setTimeout(function(){
	            $("#fl").css("display","none");
	            if($("#fl").css("display")=="none") {
	                $("#duihua>a").fadeIn(1000);
	            }
	            $.fn.clickme();
	        },500);
    	    //console.log("您没有安装flash"); 
        }
    })()*/
    //判断屏幕分辩率高
    if(window.screen.width<=900){
    	$("#fixed").css("height","297px");
    }
    //搜索栏下拉列表样式
    $("#keyw").on("keydown",function(e){
    	var li=$("#ui-idq-1>li");
    	var val=$(this).val();
    	for(var i=0;i<li.length;i++){
    		if($(li[i]).text()==val){
    			$(li[i]).children("a").addClass("ui-state-focus");
    			$(li[i]).siblings("li").children(".ui-state-focus").removeClass("ui-state-focus");
    		}
    	}
    	
    });
    // 商品分类弹出
    $.fn.tanchu();
    //成交记录轮播
    $.fn.succeed.init();
    //品牌区域轮播
    $.fn.brand.init();
    //获取当前设备的分辨率
    var width=window.screen.width;
    //如果小于992Px
    if(width<=992){
    	$("#top_banner").css("width","1190px");
    	$("#foot").css("width","1190px");
    	$("#header").css("width","1190px");
    }
    //固定搜索栏
    $(window).scroll(function(){
        var mTop=$(".s_content").prop("offsetTop");
        var height=parseFloat($(".s_content").css("height"));
        var wTop=$(window).scrollTop();
        if(wTop>(mTop+height)){
            $(".s_content").addClass("active");
        }else{
            $(".s_content").removeClass("active");
        }
    });
	//购物车
	$(".shop").mouseover(function(){
	    $(this).children("p").css("border-bottom","1px solid transparent");
		if($(this).children("div").css("display")=="none"){
		    $(this).children("p").css("border-bottom","1px solid #e3e3e3");
		}
	});
	$(".shop").mouseout(function(){
		$(this).children("p").css("border-bottom","1px solid #e3e3e3");
	});
	//活动层
	/*$(".close").on("click",function(){
        $("#cover").slideUp(500);
        $("#duihua").slideUp(500);
        $("#top_banner").slideDown(500);
    });
    if($("#cover").css("display")=="none"){
		$("#top_banner").css("display","block");
	}else{
		$("#top_banner").css("display","none");
	}*/
	
	//免费样品轮播
    /*var length=$(".tabs_ul>li").length;
    var width=parseFloat($("li.li_nth2").css("width"));
    $(".tabs_ul").css("width",length*width+"px");
    //为页签绑定鼠标移入事件
    $(".sample .tabs").on("mouseover","li",function(){
        //如果当前元素的classname不等于active
        if(!($(this).hasClass("active"))) {
            //获取当前li的下标保存在变量Index里
            var index = $(this).index();
            //获取当前li兄弟元素className为active的下标
            var n = $(this).siblings(".active").index();
            //获取li_nth2的宽度转为浮点数作为移动的距离
            var distance = parseFloat($("li.li_nth2").css("width"));
            $(".tabs_ul").css("left","-"+(index*distance+"px"));
            $(this).addClass("active").siblings(".active").removeClass("active");
        }
    })*/
});
//判断浏览器是否IE及当前版本
$.fn.isIE=function(){
	//获取浏览器版本和名字
	var ua=navigator.userAgent;
	var bs="unknown";
	//如果当前浏览器为IE
	if(ua.indexOf("Trident")!=-1){
		(bs="IE");
		$("body").attr("scroll","auto");
	}
	//如果IE浏览器的版本较低
	if(ua.indexOf("MSIE")!=-1){
		bs="IE";
		var i=ua.indexOf(bs)+bs.length+1;
		var v=parseFloat(ua.slice(i,i+2));
		if(v<8){
			$("body").attr("scroll","no");
			$(".alert").css("display","block");
			$(".alert_div").css("display","block");
		}
	}
}
//判断是否安装flash
$.fn.hasFlash=function(){
	 var hasFlash = 0; //是否安装了flash
	    var flashVersion = 0; //flash版本
	    if (document.all) {
	      var swf = new ActiveXObject('ShockwaveFlash.ShockwaveFlash');
	      if (swf) {
	        hasFlash = 1;
	        VSwf = swf.GetVariable("$version");
	        flashVersion = parseInt(VSwf.split(" ")[1].split(",")[0]);
	      }
	    } else {
	      if (navigator.plugins && navigator.plugins.length > 0) {
	        var swf = navigator.plugins["Shockwave Flash"];
	        if (swf) {
	          hasFlash = 1;
	          var words = swf.description.split(" ");
	          for (var i = 0; i < words.length; ++i) {
	            if (isNaN(parseInt(words[i]))) continue;
	            flashVersion = parseInt(words[i]);
	          }
	        }
	      }
	    }
	    return { f: hasFlash, v: flashVersion };
}
$.fn.tanchu=function() {
    var ul = $(".tanchu>ul");
    for (var i = 0; i < ul.length; i++) {
        var li = $(ul[i]).children("li");
        if (li.length > 10) {
            $(li).css("float", "left").css("width", "45%").css("margin-left","5%");
        }
    }
}
//成交记录轮播功能
$.fn.succeed={
    LIHEIGHT:0,//保存每个li的高度
    //DISTANCE:0,//保存轮播移动的总距离
    DURATION:300,//保存轮播的总时间
    STEPS:30,//保存轮播的总步数
    interval:0,//保存每步轮播的时间间隔
    step:0,//保存每步轮播的步长
    timer:null,//保存当前轮播的序号
    timer2:null,
    moved:0,//保存本次轮播已经移动的步数
    WAIT:2000,//保存自动轮播之间的时间间隔
    init:function(){//轮播初始化
        //求出时间间隔
        this.interval=this.DURATION/this.STEPS;
        //获得li的高度转为浮点数
        this.LIHEIGHT=parseFloat($("#details_div>li").css("height"));
        //调用轮播方法
        this.timer2=setTimeout(this.move,this.WAIT);
    },

    //启动一个轮播
    move:function(){
        //求出步长
        $.fn.succeed.step=$.fn.succeed.LIHEIGHT/$.fn.succeed.STEPS;
        //启动定时器
        $.fn.succeed.timer=setTimeout($.fn.succeed.moveStep,$.fn.succeed.interval);
    },

    //移动一步
    moveStep:function(){
        //获取id为details_div的top值保存在变量top中
        var top=parseFloat($("#details_div").css("margin-top"));
        var m_top=top-$.fn.succeed.step+"px";
        $("#details_div").css("margin-top",m_top);
        $.fn.succeed.moved++;
        if($.fn.succeed.moved<$.fn.succeed.STEPS){
            $.fn.succeed.timer=setTimeout($.fn.succeed.moveStep,$.fn.succeed.interval);
        }else{
            $.fn.succeed.timer=null;
            $.fn.succeed.moved=0;
            $.fn.succeed.timer2=setTimeout($.fn.succeed.move,$.fn.succeed.WAIT);
            var li=$("#details_div>li:first-child").html();
            $("#details_div>li:first-child").remove();
            $("#details_div").css("margin-top",0);
            $("#details_div").append("<li>"+li+"</li>");
        }
    }

}
//品牌区域轮播功能
$.fn.brand={
	DIVWIDTH:0,//保存每个div的宽度
	DURATION:300,//保存轮播的总时间
	STEPS:30,//保存轮播的总步数
	interval:0,//保存每步轮播的时间间隔
	step:0,//保存每步轮播的步长
	timer:null,//保存当前轮播的序号
	timer2:null,
	moved:0,//保存本次轮播已经移动的步数
	WAIT:1800,//保存自动轮播之间的时间间隔
	autoPlay:true,//是否自动轮播
	num:0,
	init:function(){//轮播初始化
		//求出时间间隔
        this.interval=this.DURATION/this.STEPS;
        //获得DIV的宽度转为浮点数
        this.DIVWIDTH=parseFloat($(".tabs_ul>li>div").css("width"))+10;
        //算出li个数
        var m=parseFloat($(".tabs_ul>li").length);
        var li=parseFloat($(".tabs_ul>li").css("width"));
        //设置ul的宽度
        $(".tabs_ul").css("width",li*m+"px");
        //启动定时器
        this.timer2=setTimeout(this.move,this.WAIT);
        var me=this;
        //鼠标移入时停止轮播
        $(".tabs_ul").on("mouseover","li a",function(){
        	me.autoPlay=false;
        })
        //鼠标移出时启动自动轮播
        $(".tabs_ul").on("mouseout","li a",function(){
        	me.autoPlay=true;
        })
        
	},
	//启动一个轮播
    move:function(){
        //求出步长
        $.fn.brand.step=$.fn.brand.DIVWIDTH/$.fn.brand.STEPS;
        //如果当前状态为自动轮播则启动定时器
        if($.fn.brand.autoPlay){
        	$.fn.brand.timer=setTimeout($.fn.brand.moveStep,$.fn.brand.interval);
        }else{//否则
        	//重新调用对象的初始化方法再次确定当前的autoPlay属性值
        	$.fn.brand.init();
        }
    },
  //移动一步
    moveStep:function(){
        //获取.tabs_ul的left值保存在变量left中
        var left=parseFloat($(".tabs_ul").css("left"));
        var m_left=left-$.fn.brand.step+"px";
        $(".tabs_ul").css("left",m_left);
        $.fn.brand.moved++;
        if($.fn.brand.moved<$.fn.brand.STEPS){
            $.fn.brand.timer=setTimeout($.fn.brand.moveStep,$.fn.brand.interval);
        }else{
            $.fn.brand.timer=null;
            $.fn.brand.moved=0;
            $.fn.brand.num++;
            if($.fn.brand.num==4){
            	var li=$(".tabs_ul>li");
            	var licontent=$(".tabs_ul>li:first-child").html();
            	$(".tabs_ul>li:first-child").remove();
            	$(".tabs_ul").append("<li>"+licontent+"</li>");
            	$(".tabs_ul").css("left",0);
            	$.fn.brand.num=0;
            }
            $.fn.brand.timer2=setTimeout($.fn.brand.move,$.fn.brand.WAIT);
        }
    }
}

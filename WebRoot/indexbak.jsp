﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="common/basePath.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<meta name="360-site-verification" content="c3ebbc6b2d3afc308afcca9047faeed2" />
<title>${systemInfo.ctTitle }</title>

</head>
<body>

<jsp:include page="common/headNew2.jsp"></jsp:include>
<script type="text/javascript">

$(function(){ 

	$("#searchSelected").click(function(){ 
		$("#searchTab").show();
		$(this).addClass("searchOpen");
		
	}); 

	$("#searchTab li").hover(function(){
		$(this).addClass("selected");
	},function(){
		$(this).removeClass("selected");
	});
	 
	$("#searchTab li").click(function(){
		$("#searchSelected").html($(this).html());
		var searchSelected = $("#searchSelected").text();
		$("#typeDianRong").val(searchSelected);
		$("#searchTab").hide();
		$("#searchSelected").removeClass("searchOpen");
	});
});

function coup(){
	$.ajaxSetup({async:false});
   		$.post("coup_count",function(data){
   		if(data==-1){
   				
   		}else{
   				$("#coup").append(""+data+"");
   				}
   		}); 
   		$.post("order_count",function(data){
   		if(data==-1){
   				
   		}else{
   				$("#order").append(""+data+"");
   				}
   		});     
}
coup();
function userinfo(){
	window.location.href="user_userinfo";
}
</script>
<div class="turner">


    <div class="example2">
        <ul>
	<li><a href="bkxh.jsp" target="_blank"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/img/0819banner.jpg" /></a></li>
            <li><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/img/0705banner2.jpg" /></li>

	<li><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/img/0705banner3.jpg" /></li>
<!--	<li><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/img/0705banner4.jpg" /></li>

-->
	<li><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/img/0705banner5.jpg" /></li>
        </ul>
        <ol>
            <li></li>
<li></li>

<li></li>
<!--
<li></li>

-->
<li></li>
        </ol>
    </div>

</div>



<div class="notice">
<c:if test="${userinfosession.UUserid != null}">
	<div style="height: 56px;" class="yesloginpos"><span>进入<a href="<%=basePath %>user_userinfo">个人中心</a></span>您好！${userinfosession.UUsername }</div>
</c:if>
<c:if test="${userinfosession.UUserid == null}">
	<div class="yeslogin"><a href="login_login" class="login-a">登录</a><a href="login_goRegister" target="_blank">注册</a></div>
</c:if>
    <dl class="tin_th">
<dt id=gonggao><h3><a href="list_notice" target="_blank">特别公告</a></h3></dt>
<c:forEach items="${noticesList }" var="list">
	<dd><a href="find_notice?noticeDTO.noId=${list.noId }" target="_blank">${list.noTitle }</a></dd>
</c:forEach> 
    </dl>
    <ul class="ulgua">
<li class="ulguaa"><span>专业配送</span></li>
<li class="ulguab"><span>安全支付</span></li>
<li class="ulguac"><span>品质保证</span></li>
    </ul>
</div>

<div class="clear"></div>
    </div>
</div>


<div class="chengjiaojl">

<!-- -------------摇奖排行榜--------，近30天成交<i>${count30 }</i>单-------  -->
<div class="Top_Record">
    <div class="record_Top"><p>今天已成交<i>${count1 }</i>单， 近三天内已成交<i>${count3 }</i>单，近30天成交<i>${count30 }</i>单，现货库存<i>17286</i>种，本月新增<i>3886</i>种</p></div>
    <div class="topRec_List">
        <dl>
            <dd>客户名称</dd>
            <dd><i>/</i>订单编号</dd>
            <dd><i>/</i>下单日期</dd>
            <dd><i>/</i>金额</dd>
            <dd><i>/</i>付款情况</dd>
            <dd><i>/</i>订单状态</dd>
        </dl>
        <div class="maquee">
            <ul>
            	<c:forEach items="${transactionList }" var="list">
	                <li>
	                    <div>${list.tranUsername }</div>
	                    <div>${list.orderSn }</div>
	                    <div><i>${list.tranTime }</i></div>
	                    <div><i>￥${list.tranCount }</i></div>
	                    <div>线上支付 | 
	                    	<c:if test="${list.tranPay == '未付款' }"><span class="nopay">${list.tranPay }</span></c:if>
	                    	<c:if test="${list.tranPay == '已付款' }"><span class="yespay">${list.tranPay }</span></c:if>
	                    </div>
	                    <div>${list.tranOrderStart }</div>
	                </li> 
            	</c:forEach>
            </ul>
        </div>
    </div>

<script src="js/jquery.min.js"></script>
<script type="text/javascript"> 
      function autoScroll(obj){  
            $(obj).find("ul").animate({  
                marginTop : "-39px"  
            },500,function(){  
                $(this).css({marginTop : "0px"}).find("li:first").appendTo(this);  
            })  
        }  
        $(function(){  
            setInterval('autoScroll(".maquee")',3000);
            setInterval('autoScroll(".apple")',2000);
              
        }) 
        
</script>


<div class="clear"></div>
    </div>

<div class="adspay"><a href="/" target="_blank"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/img/ads2.png"></a></div>




</div>





<!-- 合作品牌 -->
<div class="contentBrand">
    <div class="brandcenter">
    <div class="content-titletit"><a href="/goods_brandList">合作品牌<i>Brand cooperation</i></a></div>
    
    
    <c:forEach items="${brandDTOs }" var="list">
	    <div class="brand">
	        <div class="brandtit">
	            <div class="btitlogo"><a href="goods_brandDetail?fgoodsDTO.BId=${list.bid }" target="_blank"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/${list.blogn }"></a></div>
	            <dl class="btitjiao">
	            <dt>品牌名称：<a  href="goods_brandDetail?fgoodsDTO.BId=${list.bid }" onmouseover="this.style.cssText='color:gray; text-decoration:none;'" onmouseout="this.style.cssText='color:black;text-decoration:none'" style="color: black; text-decoration:none;" target="_blank">${list.bname }（授权代理）</a></dt>
	            <dd>${list.bdesc }</dd>
	            </dl>
	        </div>
	        <div class="brandlist" style="display: none;">
	        	<div class="newchan"><a href="goods_brands?fgoodsDTO.BId=${list.bid }" target="_blank">更多</a><h3>最新产品</h3></div>
	        	<div style=" float:left;width:250px">
					<ul>
					<c:forEach items="${list.goodsName }" var="list1">
						<li><a href="goods_detail?gid=${list1.GId }" target="_blank">${list1.GName }</a></li>
					</c:forEach>
					</ul>
				</div>
				
				<div style=" float:right;width:250px">
					<ul>
					<c:forEach items="${list.goodsName1 }" var="list1">
						<li><a href="goods_detail?gid=${list1.GId }" target="_blank">${list1.GName }</a></li>
					</c:forEach>
					</ul>
				</div>
	        </div>
	    </div>
    </c:forEach>
    				<div style=" float:left;width:250px">
				
					<ul>
					<c:forEach items="${list.goodsName }" var="list1">
						<li><a href="goods_detail?gid=${list1.GId }" target="_blank">${list1.GName }</a></li>
					</c:forEach>
					</ul>
				
				</div>
				
				<div style=" float:right;width:250px">
					<ul>
					<c:forEach items="${list.goodsName }" var="list1">
						<li><a href="goods_detail?gid=${list1.GId }" target="_blank">${list1.GName }</a></li>
					</c:forEach>
					</ul>
				</div>
    

<div class="ads"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/img/ads1.png"></div>
<div class="clear"></div>
    </div>
</div>

<!-- 免费样品 -->
<div class="content">
    <div class="contentcenter">
    <div class="content-titletit"><a href="<%=request.getContextPath() %>/goods_search?pid=2&keyword=免费样品&cid=2" target="_blank">免费样品<i>Free goods</i></a></div>

    <div class="ypqpic"><a href="<%=request.getContextPath() %>/goods_search?pid=2&keyword=免费样品&cid=2" target="_blank"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/img/ypq.png"></a></div>
	<ul class="ypqul">
	<c:forEach items="${goodsListZu }" var="list">
	    <li>
	        <span><a href="goods_detail?gid=${list.GId }" target="_blank"><img src="${list.goodsImg.PUrl }"></a></span>
	        <p><a href="goods_detail?gid=${list.GId }" target="_blank">${list.UName }<br/>${list.GName }</a></p>
	        <p><i>品牌：${list.pack4 }</i></p>
	    </li>
	</c:forEach>
    </ul>


<div class="ads"></div>

<script type="text/javascript">
function logout(){
	var opened=window.open('about:blank','_self');
	opened.opener=null;
	opened.close();
}
</script>

<div class="clear"></div>
    </div>
</div>

<jsp:include page="common/foot1.jsp"></jsp:include>
<script src="js/jquery.luara.0.0.1.min.js"></script>

<script src="js/nav_scroll.js" type="text/javascript"></script> <!-- 固定导航 -->
              <script type="text/javascript">
var _mvq = _mvq || [];
_mvq.push(['$setAccount', 'm-229240-0']);

_mvq.push(['$logConversion']);
(function() {
var mvl = document.createElement('script');
mvl.type = 'text/javascript'; mvl.async = true;
mvl.src = ('https:' == document.location.protocol ? 'https://static-ssl.mediav.com/mvl.js' : 'http://static.mediav.com/mvl.js');
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(mvl, s);
})();

</script>        
</body>
</html>
﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="common/basePath.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<meta name="Keywords" content="${systemInfo.ctKey }" />
	<meta name="Description" content="${systemInfo.ctTitle }" />
	<title>${systemInfo.ctTitle }</title>
	<link href="<%=request.getContextPath() %>/css/media-queries.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="<%=basePath %>js/jquery-1.11.3.js"></script>
        <link rel="stylesheet" href="<%=basePath %>css/css.css" type="text/css"/>
        <link rel="shortcut icon" href="<%=basePath %>favicon.ico">
        <script type="text/javascript" src="<%=basePath%>/js/script.js" charset="utf-8"></script>
        <script type="text/javascript" src="<%=basePath %>js/check.js"></script>
         <script type="text/javascript" src="<%=basePath %>js/login.js"></script>
  </head>
  
  <body>
  <div id="popDivall" class="mydivall" style="display:none;">
<div class="con_t"><a href="javascript:closecharDiv()"><img src="<%=imgurl %>/bg/chadiao.gif"></a><span>让客户经理联系我</span></div>
<div class="con_b">
	<ul>
    <li><p style=" line-height:20px;">您的支持是我们前进的动力！请留下您的联系方式及问题详情，以便客户经理及时与您联系，谢谢支持！</p><br/></li>
	<li id="ts" style="color:red"></li>
	<li><span>邮箱：</span><input name="con" type="text" id="email"/></li>
    <li><span>手机：</span><input name="con" type="text" id="mbo"/></li>
	<li><span>详细：</span><textarea name="con" id="detail"></textarea></li>
    <li>
    <input type="hidden" id="aaaaaaaa" value="${message }" />
    </li>
    <li><a href="javascript:contacts()">提交</a></li>
    </ul>
</div>
</div>
<div id="bgallfk" class="bgallfk" style="display:none;"></div>
  <form action="login_login" id="Myform" method="post">
    <div class="up_logo">
        <div class="up">
        <div class="up_lo"><a href="<%=basePath %>"><img src="<%=imgurl %>/bg/logo.gif"></a><p>用户登录<i>欢迎你的到来！</i></p></div>
        </div>
        <div class="clear"></div>
    </div>
    
    <div class="up_con">
        <div class="up">
             <div class="up_ad"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/2017/login/login_bg1.png" 
            style="width:830px;position:absolute;top:60px;right:40%;"/></div> 
            <div class="up_up">
        <div class="ct">
            <h1 class="active">用户登录</h1>
            <h1>免密码登录</h1>
            <div class="ctd-r">
                <div class="regist-link"><a href="<%=basePath %>login_goRegister">立即注册</a></div>
            </div>
        </div>
        <c:if test="${message == null && message!='邮箱没激活' }">
	    <div class="msg-up" style="display: none;"></div>
    </c:if>
    <c:if test="${message != null && message=='邮箱没激活' }">
	    <div class="msg-up">${message } <a href="javascript:active();"> 去激活</a></div>
	    
	    <script href="javascript:closecharDiv()">
		  //跳转到邮箱登录界面
		    function active() {
		    	var email=$("#username").val();
		    	var yuming=email.split("@")[1];
		    	var href="http://mail."+yuming;
		    	window.open(href);
		    }
	    </script>
    </c:if>
            <div class="into_up">
       <div class="ino_name">
       <span></span>
       <input id="username" name="ctUser.UUserid" value="${ctUser.UUserid}" type="text" style="color: #333;" placeholder="用户名/手机/邮箱" onchange="javascript:checkLogin();"/>
       </div> 
       <div class="ino_pass">
       <span></span>
      <input id="passwd" name="ctUser.UPassword" type="password" value="${ctUser.UPassword}" onchange="javascript:checkpwd();" placeholder="密码"/>
       </div> 
       <div class="ino_yanz">
       <input type="hidden" id="isAutoLogin" namemsg-up="isAutoLogin" placeholder="用户名/手机/邮箱"/>  	
       <input type="text" id="yzm" name="yzm" onblur="checkYzm();" placeholder="请输入右侧验证码"/>
       <img width="105px;" style="float:right;" onclick="javascript:showYzm()" id="realyzm" src="<%=basePath%>common/image.jsp"/>
       
       </div>
		<div class="ct_h"><span class="zhaomime"><a href="<%=basePath %>login_goForgetPwd">忘记密码</a></span><span class="lianjingli"><a href="javascript:showcharDiv();">让客户经理联系我</a></span></div>
        <div class="ct_deng"><a id="usernameobj" onclick="javascript:login()">登　　录</a></div>
        <div class="ct_th">
            <p>使用合作网站账号登录长亭易购：</p>
            <div class="ct_fi"><a href="<%=basePath %>qqLoginAndReg" class="chrt_zf"><img src="<%=imgurl %>/dsqq.jpg"></a>
            <span class="line_ct"></span>
            <a href="<%=basePath %>Ali_Pay_Api"><img src="<%=imgurl %>/dszfb.jpg"></a><span class="line_ct"></span><a href="weixinLogin"><img src="<%=imgurl %>/dsweixin.jpg"></a><span class="line_ct"></span><a href="getWeiboUrl"><img src="<%=imgurl %>/dsweibo.jpg"></a></div>
        </div>
            </div>
            
            <!-- 免密码登录 -->
            <div class="into_up1">
       <div class="ino_name">
       <span></span>
       <input type="text" style="color: #333;" id="userPhoneNoPass" placeholder="请输入手机号码" onblur="javascript:checkPhone(this.value);"/>
       </div> 
       
       <div class="ino_yanz">
       <input type="hidden" namemsg-up="isAutoLogin" placeholder="请输入手机号码"/>  	
       <input type="text" id="yzmNoPass" name="yzm" onblur="checkYzmNoPass();" placeholder="请输入右侧验证码"/>
       <img width="105px;" style="float:right;" onclick="javascript:showYzm()" id="realyzmNoPass" src="<%=basePath%>common/image.jsp"/>
       </div>
       <div class="dt_psd">
           <input type="text" id="dt_passwowd" onblur="checkUmbYzmIsEqual();" placeholder="请输入动态密码"/>
           <a href="javascript:;" onclick="goYzmInfo(this)" class="dt_btn">发送动态密码</a>
       </div>
        <div class="ct_deng"><a id="usernameobjNoPass" onclick="javascript:loginNoPass()">登　　录</a></div>
        <div class="ct_th">
            <p>使用合作网站账号登录长亭易购：</p>
            <div class="ct_fi"><a href="<%=basePath %>qqLoginAndReg" class="chrt_zf"><img src="<%=imgurl %>/dsqq.jpg"></a>
            <span class="line_ct"></span>
            <a href="<%=basePath %>Ali_Pay_Api"><img src="<%=imgurl %>/dszfb.jpg"></a><span class="line_ct"></span><a href="weixinLogin"><img src="<%=imgurl %>/dsweixin.jpg"></a><span class="line_ct"></span><a href="getWeiboUrl"><img src="<%=imgurl %>/dsweibo.jpg"></a></div>
        </div>
            </div>
            
            
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="up_bot">
    <!--<div class="bot_dao"><a href="list_help_desc?help.HId=1">长亭易购简介</a><span class="line_ct"></span><a href="list_help_desc?help.HId=4">购物保障</a><span class="line_ct"></span><a href="list_help_desc?help.HId=24">品质保证</a><span class="line_ct"></span><a href="list_help_desc?help.HId=5">订制服务</a><span class="line_ct"></span><a href="list_help_desc?help.HId=20" >代购服务</a><div class="clear"></div></div>
    <div class="copyright"><i>Copyright&nbsp;&#169;&nbsp;2004-2015&nbsp;&nbsp;CTEGO CORPORATION All Rights Reserved.</i>&nbsp;&nbsp;粤<i>ICP</i>备<i>09055513</i>号<i>-2</i><br/></>长亭易购 <i><a href="<%=basePath %>" class="chrt_zf">ctego.com</a>&nbsp;</i>版权所有</div>
        <div class="clear"></div>  -->
        <p>长亭易购 版权所有 &copy;2014-2017 CTEGO CORPORATION All Rights Reserved.<br>
            粤ICP备16050358号-1 <script type="text/javascript">var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");document.write(unescape("%3Cspan id='cnzz_stat_icon_1255740681'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s95.cnzz.com/z_stat.php%3Fid%3D1255740681%26show%3Dpic' type='text/javascript'%3E%3C/script%3E"));</script></p>
        <p class="foot_img">
            <a href="http://cert.ebs.org.cn/Cert/A267058?spm=0.0.0.0.xLniq5" target="_blank"><img src="<%=imgurl %>/ebs.png" height="40"/></a>
            <a href="http://webscan.360.cn/index/checkwebsite/url/www.ctego.com" style="height:50px; padding-left:10px;"><img style="height:40px; padding-top:0; margin-left:25px" border="0" src="<%=imgurl %>/bot360.png"/></a>
            <a id="_pingansec_bottomimagelarge_shiming" href="http://si.trustutn.org/info?sn=608150928017490081680&certType=1">
            <img src="<%=imgurl %>/rzlm.jpg"/></a><a id="_pinganTrust" target="_blank" href="http://c.trustutn.org/s/ctego.com">
            <img src="<%=imgurl %>/paxy.jpg"/></a>
        </p>
    </div>
    </form>
    <script>
        $("body").keydown(function(e){
        	var isFocus=$("#username").is(":focus");
        	var curKey=e.which;
        	if(curKey==13&&isFocus!=true){
        		if(isLoginType == 0){
        			$(".msg-up").hide();
        			login();
        		} else if (isLoginType == 1){
        			$(".msg-up").hide();
        			loginNoPass();
        		}
        		
        	}
        });
        var isLoginType = 0;
        $(document).ready(function(){
        	//切换登录方式
        	$(".up_up .ct h1").click(function(){
        		$(this).addClass("active").siblings(".active").removeClass("active");
        		if($(this).text()=="免密码登录"){
        			$(".ctd-r").hide();
        			$(".into_up").hide();
        			$(".into_up1").show();
        			isLoginType = 1;
        		}else{
        			$(".ctd-r").show();
        			$(".into_up").show();
        			$(".into_up1").hide();
        			isLoginType = 0;
        		}
        	});
        });
        
    </script>
  </body>
</html>

﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="common/basePath.jsp"%>
<%@taglib prefix="frm" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="Keywords" content="${systemInfo.ctKey }" />
	<meta name="Description" content="${systemInfo.ctTitle }" />
	<meta name="360-site-verification" content="c3ebbc6b2d3afc308afcca9047faeed2" />
	<title>${systemInfo.ctTitle }</title>
	
  </head>
  
  <body>
  	<jsp:include page="common/headnew.jsp"></jsp:include>
  	 <!--banner广告部分-->
  	 <div class="alert"></div>
  	 <div class="alert_div">
  	         <p>您的浏览器版本太低，无法正常显示网页内容，请更换浏览器打开网站或重新下载新版本 !</p>
  	         <span>
  	             <a href="https://www.googlechromer.cn/">chrome浏览器</a>
  	             <a href="https://support.microsoft.com/zh-cn/help/17621/internet-explorer-downloads">新版IE浏览器</a>
  	         </span>
  	 </div>
  	 <!--<div name="${tanchu }"
  	 <c:if test="${tanchu == '2' }">
  	 	 style="display:none;"
  	 </c:if>
  	  id="cover"></div>
  	  <div name="${tanchu }"
  	 <c:if test="${tanchu == '2' }">
  	 	 style="display:none;"
  	 </c:if>
  	   id="duihua">
          <span class="close">
              <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/20161026/close.png">
          </span>
          <a href="activityInfo">
              <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/1111/tm.png">
          </a>
          <object>
          <param name="movie" value="webjx_com.swf">  
          <param name="wmode" value="transparent"> 
          </object>
          <embed id="fl" src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/1111/fl.swf" quality="hide" type='application/x-shockwave-flash' wmode="transparent" ></embed>
      </div>  -->
    <div id="banner">
        <ul class="slider_bg">
        </ul>
        <div class="banner_content">
        <!--分类框-->
        <ul class="cate_box">
        	<c:forEach items="${cateListApplication }" var="cate1">
	            <li onmouseover="$(this).children('i').css('background-position','${cate1.mouseOverPos}');" onmouseout="$(this).children('i').css('background-position','${cate1.mouseOutPos}');">
	                <i style="background:url('https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/icon02.png') no-repeat ${cate1.mouseOutPos};"></i>
	                <b>&gt;</b>
	                <a
	                <c:if test="${cate1.cateGoodsNum != 0 }">
	                	 onclick="getQuEncode('${cate1.CName }');"  target="_self" 
	                </c:if>
	                 href="javascript:;"
	                 >${cate1.CName }(${cate1.cateGoodsNum })</a>
	                <div class="tanchu">
	                    <ul>
	                    	<c:forEach items="${cateListSerApplication }" var="cate2">
	                    		<c:if test="${cate2.parentId == cate1.CId }">
			                        <li>
			                            <a href="javascript:;"
											<c:if test="${cate2.cateGoodsNum != 0 }">
							                	 onclick="getQuEncode('${cate2.CName }');" target="_self" 
							                </c:if>
			                            >${cate2.CName }(${cate2.cateGoodsNum })</a>
			                        </li>
	                    		</c:if>
	                    	</c:forEach>
	                    </ul>
	                </div>
	            </li>
        	</c:forEach>
        </ul>
       <div class="lunbo">
            <ul>
            	<c:forEach items="${ctAdsList }" var="list">
	            	<li><a href="${list.adUrl }"><img src="${list.adImg }"></a></li>
            	</c:forEach>
            </ul>
        </div>
        <div class="notice">
		    <div class="not_d1">
			       <p>你好！<br>欢迎来到长亭易购</p>
        	   <c:if test="${userinfosession.UUserid == null}">
			       <div>
			           <button onclick="javascript:location.href='login_goRegister';">免费注册</button>
			           <button  target="_blank" onclick="javascript:location.href='login_login';">立即登录</button>
			       </div>
        	   </c:if>
	           <c:if test="${userinfosession.UUserid != null}">
		       		<div class="not_div"><button onclick="javascript:location.href='<%=basePath %>user_userinfo';">进入个人中心</button></div>
        	   </c:if>
	       </div>
            <div class="not_d2">
                <p>
                    <span>特别公告</span>
                    <a href="list_notice" target="_blank">更多&gt;</a>
                </p>
                <ul>
                	<c:forEach items="${noticesList }" var="list">
	                    <li><b></b><a href="find_notice?noticeDTO.noId=${list.noId }">${list.noTitle }</a>
	                    	<c:if test="${list.noTimeNew == 1 }">
		                    	<img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/1111/NEW.gif">
	                    	</c:if>
	                    </li>
                	</c:forEach>
                </ul>
            </div>
        </div>
        </div>
    </div>
    <!--主体内容-->
    <div id="main">
        <!--成交记录-->
        <div class="trade">
            <p class="trade_p">
                <b></b><span>成交记录</span>
                今天已成交 <a href="javascript:;">${count1 }</a> 单，近三天内已成交 <a href="javascript:;">${count3 }</a> 单，近三十天内已成交<a href="javascript:;">${count30 }</a> 单，现货库存<a href="/">17286</a> 种，本月新增 <a href="javascript:;">3886</a> 种
            </p>
            <div class="trade_div">
		        <ul class="thead">
		            <li class="first_li">客户名称</li>
		            <li>订单编号</li>
		            <li>下单日期</li>
		            <li>金额</li>
		            <li>付款情况</li>
		            <li class="last_li">订单状态</li>
		        </ul>
                <div>
                    <ul id="details_div">
                    <c:forEach items="${transactionList }" var="list">
                        <li>
                            <div class="first_div">${list.tranUsername }</div>
                            <div>${list.orderSn }</div>
                            <div>${list.tranTime }</div>
                            <div><span>&yen;${list.tranCount }</span></div>
                            <c:if test="${list.tranPay == '未付款' }"><div>线上支付 | <a>${list.tranPay }</a></div></c:if>
                            <c:if test="${list.tranPay == '已付款' }"><div>线上支付 | <span>${list.tranPay }</span></div></c:if>
                            <div class="last_div">${list.tranOrderStart }</div>
                        </li>
                    </c:forEach>
                    </ul>
                </div>
            </div>
        </div>
        <!--样品区-->
<!--样品区-->
        <div class="sample">
            <div class="s_div">
                <b></b>
                <span>合作品牌</span>
                Cooperative Brand
                <!--<ul class="tabs">
                    <li class="active">电阻</li>
                    <li>电容</li>
                    <li>二三极管</li>
                    <li>电感</li>
                </ul>  -->
                <a href="goods_brandList">全部品牌>></a>
            </div>
                    <ul>
            <li class="li_nth1"><a href="javascript:;"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/img/brand.png"></a></li>
            <li class="li_nth2">
              <ul class="tabs_ul">
                <!--<li>
	             <c:forEach items="${goodsListZu }" var="list" varStatus="indexStatus">
	            	<div class="li_div${indexStatus.index+1 }">
	                    <b><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/${list.goodsBrand.BLogo }"></b>
	                    <span><img src="${list.goodsImg.PUrl }"></span>
	                    <div>
	                        <p><span><a href="goods_brandDetail?fgoodsDTO.BId=${list.BId }">${list.pack4 }</a></span><a href="javascript:;" onClick="searchFrom('${list.UName }');">${list.UName }</a></p>
	                        <span><a href="goods_detail?gid=${list.GId }">${list.GName }</a></span>
	                    </div>
	                </div>
	             </c:forEach>
	             </li>
	             <li>
	             <c:forEach items="${goodsListZu }" var="list" varStatus="indexStatus">
	            	<div class="li_div${indexStatus.index+1 }">
	                    <b><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/${list.goodsBrand.BLogo }"></b>
	                    <span><img src="${list.goodsImg.PUrl }"></span>
	                    <div>
	                        <p><span><a href="goods_brandDetail?fgoodsDTO.BId=${list.BId }">${list.pack4 }</a></span><a href="javascript:;" onClick="searchFrom('${list.UName }');">${list.UName }</a></p>
	                        <span><a href="goods_detail?gid=${list.GId }">${list.GName }</a></span>
	                    </div>
	                </div>
	             </c:forEach>
	             </li>
	             <li>
	             <c:forEach items="${goodsListZu }" var="list" varStatus="indexStatus">
	            	<div class="li_div${indexStatus.index+1 }">
	                    <b><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/${list.goodsBrand.BLogo }"></b>
	                    <span><img src="${list.goodsImg.PUrl }"></span>
	                    <div>
	                        <p><span><a href="goods_brandDetail?fgoodsDTO.BId=${list.BId }">${list.pack4 }</a></span><a href="javascript:;" onClick="searchFrom('${list.UName }');">${list.UName }</a></p>
	                        <span><a href="goods_detail?gid=${list.GId }">${list.GName }</a></span>
	                    </div>
	                </div>
	             </c:forEach>
	             </li>
	             <li>
	             <c:forEach items="${goodsListZu }" var="list" varStatus="indexStatus">
	            	<div class="li_div${indexStatus.index+1 }">
	                    <b><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/${list.goodsBrand.BLogo }"></b>
	                    <span><img src="${list.goodsImg.PUrl }"></span>
	                    <div>
	                        <p><span><a href="goods_brandDetail?fgoodsDTO.BId=${list.BId }">${list.pack4 }</a></span><a href="javascript:;" onClick="searchFrom('${list.UName }');">${list.UName }</a></p>
	                        <span><a href="goods_detail?gid=${list.GId }">${list.GName }</a></span>
	                    </div>
	                </div>
	             </c:forEach>
	             </li>  -->
			         <c:forEach items="${brandDTOList }" var="listDTO">
			             <li>
				             <c:forEach items="${listDTO }" var="list">
					        	<div>
					                <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/${list.blogn }">
						            <a href="goods_brandDetail?fgoodsDTO.BId=${list.bid }"><span>${list.bname }</span></a>
						        </div>
				             </c:forEach>
				         </li>
			         </c:forEach>
	           </ul>
            </li>
            <li class="li_nth3">
                <div>
                    <p>在线提交免费样品申请</p>
                    <s:form action="samRegInfoUp" method="post" id="samReg">
                    	<s:token></s:token>
                        <p class="inp inp1"><span><b>*</b>联&nbsp;系&nbsp;&nbsp;人：</span><input onblur="checkNull(this,'name');"  onafterpaste="encodeindex(this);" onkeyup="encodeindex(this);" id="userName" name="sampleReg.samUsername" type="text"></p>
                        <span id="userNameMess"></span>
                        <p class="inp"><span><b>*</b>手机号码：</span><input id="userPhone" onblur="checkNull(this,'phone');"  onafterpaste="encodeindex(this);" onkeyup="encodeindex(this);" name="sampleReg.samUserphone" type="text"></p>
                        <span id="userPhoneMess"></span>
                        <p class="inp"><span><b>*</b>公司名称：</span><input id="userCom" onblur="checkNull(this,'com');"  onafterpaste="encodeindex(this);" onkeyup="encodeindex(this);" name="sampleReg.samCompanyname" type="text"></p>
                        <span id="userComMess"></span>
                        <p class="inp"><span><b>*</b>公司类型：</span><input id="userComType" onblur="checkNull(this,'comType');"  onafterpaste="encodeindex(this);" onkeyup="encodeindex(this);" name="sampleReg.samCompanytype" type="text"></p>
                        <span id="userComTypeMess"></span>
                        <p class="inp"><span><b>*</b>样品型号：</span><input id="userGoods" onblur="checkNull(this,'goods');"  onafterpaste="encodeindex(this);" onkeyup="encodeindex(this);" name="sampleReg.samGoodsName" type="text"></p>
                        <span id="userGoodsMess"></span>
                        <p class="inp"><a href="javascript:;" id="btnSamReg" >提交申请</a></p>
                        <!-- onclick="samReg()" -->
                        <script type="text/javascript">
							$(function(){
								$("#btnSamReg").click(function(){
									var $userName = $("#userName").val();
									var $userPhone = $("#userPhone").val();
									var $userCom = $("#userCom").val();
									var $userComType = $("#userComType").val();
									var $userGoods = $("#userGoods").val();
									if($userName == ""){
										$("#userNameMess").text("用户名不能为空");
										return;
									}
									if($userPhone == ""){
										if(!checkPhone($userPhone)){
											$("#userPhoneMess").text("手机号不合法");
											return;
										}
										return;
									}
									if($userCom == ""){
										$("#userComMess").text("公司名不能为空");
										return;
									}
									if($userComType == ""){
										$("#userComTypeMess").text("公司类别不能为空");
										return;
									}
									if($userGoods == ""){
										$("#userGoodsMess").text("申请样品不能为空");
										return;
									}
									if(confirm("确定要申请此样品吗？")){
										$("#samReg").submit();
									}
								});
							});
							function samReg(){
								var $userName = $("#userName").val();
								var $userPhone = $("#userPhone").val();
								var $userCom = $("#userCom").val();
								var $userComType = $("#userComType").val();
								var $userGoods = $("#userGoods").val();
								if($userName == ""){
									$("#userNameMess").text("用户名不能为空");
									return;
								}
								if($userPhone == ""){
									if(!checkPhone($userPhone)){
										$("#userPhoneMess").text("手机号不合法");
										return;
									}
									return;
								}
								if($userCom == ""){
									$("#userComMess").text("公司名不能为空");
									return;
								}
								if($userComType == ""){
									$("#userComTypeMess").text("公司类别不能为空");
									return;
								}
								if($userGoods == ""){
									$("#userGoodsMess").text("申请样品不能为空");
									return;
								}
								if(confirm("确定要申请此样品吗？")){
									$("#samReg").submit();
								}
							}
							</script>
                    </s:form>
                </div>
            </li>
        </ul>
            <div class="free">
	            <c:forEach items="${goodsListZu }" var="list">
	                <div class="border_animation" style="display: none;">
	                    <div class="border_top"></div>
	                    <div class="border_right"></div>
	                    <div class="border_bottom"></div>
	                    <div class="border_left"></div>
	                    <div class="free_div">
	                        <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/${list.goodsBrand.BLogo }">
	                        <div>
	                            <img src="${list.goodsImg.PUrl }">
	                        </div>
	                        <p><a href="javascript:;" onClick="searchFrom('${list.UName }');">${list.UName }</a></p>
	                        <h4><a href="goods_detail?gid=${list.GId }">${list.GName }</a></h4>
	                        <a href="goods_brandDetail?fgoodsDTO.BId=${list.BId }">${list.pack4 }</a>
	                    </div>
	                </div>
	            </c:forEach>
            </div>
        </div>
        <!--BOM配表-->
	    <div class="BOM">
	        <p>
	            <b></b>
	            <span>BOM配表</span>
	            BOM Configuration Table
	        </p>
	        <ul>
	            <li class="bom_left"><a href="javascript:;"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/img/BOM.png"></a></li>
	            <li class="bom_middle">
	                <div class="div1">
	                    <p><a href="javascript:;" onclick="DownloadForBomModel()" id="downloadFile" target="_blank">下载模板</a></p>
	                    <div>
	                    <c:if test="${userinfosession.UUserid == null}">
	                    	<input type="text" onclick="location.href='<%=basePath %>login_login'"></input>
	                        <a href="<%=basePath %>login_login"><b></b>上传文件</a>
	                        <span>上传成功，并匹配完成！</span>
	                    </c:if>
	                    <c:if test="${userinfosession.UUserid != null}">
	                    	<input type="file" onchange="updatecsv_index()"  accept=".csv" id="file" name="file"></input>
	                        <a id="upFileA" href="javascript:;"><b></b>上传文件</a>
	                        <span id="upOkMess" target="_blank">上传成功，并匹配完成！</span>
	                    </c:if>
	                    </div>
	                    <p><a id="hedui" href="javascript:;">核对BOM</a></p>
	                </div>
	                <div class="div2">
	                    <p class="p1"><span></span><a>下载模板</a><b></b></p>
	                    <p class="p2"><span></span><a>填写BOM</a><b></b></p>
	                    <p class="p3"><span></span><a>上传BOM配表</a><b></b></p>
	                    <p class="p4"><span></span><a>核对BOM</a><b></b></p>
	                    <p class="p5"><span></span><a>生成订单</a></p>
	                </div>
	            </li>
	            <li class="bom_right">
	                <div>
	                    <p class="first_p">BOM配单使用说明</p>
	                    <p>①点击<span>下载BOM模板</span>至本地计算机;</p>
	                    <p>②<span>打开编辑</span>该文件，填写商品名称及数量;</p>
	                    <p>③<span>上传</span>编辑后的BOM文件至长亭易购平台,平台将自动生成匹配商品信息;</p>
	                    <p>④点击“<span>核对BOM</span>”在“我的BOM”中查看，如有未匹配的商品请手动增加或修改;</p>
	                    <p>⑤<span>提交</span>，生成订单</p>
	                </div>
	            </li>
	        </ul>
	    </div>
		<!--合作品牌-->
		    <div class="information">
		        <p>
		            <b></b>
		            <span>资讯动态</span>
		            Information Dynamics
		        </p>
		        <div class="zhanhui">
		            <p>展会信息<a target="_blank" href="exhibitionInfo">更多>></a></p>
		            <div class="new_div">
		                <ul>
		                	<c:forEach items="${zhanghuiNotice }" var="list">
			                    <li><a target="_blank" href="find_notice?noticeDTO.noId=${list.noId }">
			                    		<img src="${list.noImgUrl }">
			                    	</a>
			                        <a target="_blank" href="find_notice?noticeDTO.noId=${list.noId }" class="news">${list.noTitle }</a>
			                    </li>
		                	</c:forEach>
		                </ul>
		            </div>
		        </div>
		        <div class="hangye">
		            <p>行业动态<a target="_blank" href="industryDynamicsInfo">更多>></a></p>
		            <ul>
		            	<c:forEach items="${noticesListHang }" var="list">
			                <li><a href="find_notice?noticeDTO.noId=${list.noId }" target="_blank">${list.noTitle }</a><span>${list.noTimeNew }</span></li>
		            	</c:forEach>
		            </ul>
		        </div>
		        <div class="ziyuan">
		            <p>资源下载<a href="resourcesInfo">更多>></a></p>
		            <ul>
		            	<c:forEach items="${resourcesList }" var="list">
			                <li>
			                    <b></b><span>${list.RName }</span>
			                    <a href="https://ctegores.oss-cn-shenzhen.aliyuncs.com${list.RUrl }" target="_blank"><img src="img/load.gif"></a>
			                </li>
		            	</c:forEach>
		            </ul>
		        </div>
		    </div>
		</div> 
		<c:if test="${userinfosession.UUserid == null}">
			<!--<div id="ticket">
			<span>x</span><a href="<%=basePath %>login_goRegister"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/1111/xjj.png"/></a>
			</div>  -->
		</c:if>
    <script type="text/javascript">
        //送现金券图片是否显示
        var p_name=$(".header_content").children("p").attr("class");
        p_name=="p_login"?$("#ticket").css("display","none"):$("#ticket").css("display","block");
        $("#ticket>span").click(function(){
        	$("#ticket").css("display","none");
        })
    	function selectFrom(lowerValue, upperValue){ 
	    	//取值范围总数 
	    	var choices = upperValue - lowerValue + 1; 
	    	return Math.floor(Math.random() * choices + lowerValue); 
    	}
	  	//随机Q
		var qqs = ["2881112904"]; 
		var qq = qqs[selectFrom(0, qqs.length-1)]; 
		$("#qqrandom").html("<a target=blank href='tencent://message/?uin="+qq+"&Site=qq&Menu=yes'></a>");
		function getFenlei(){
			
			$.ajax({ 
				type: "POST",
				url: "getFirstCateForShou",
				dataType: "json",
				success: function(data){
					//document.getElementById("cate-title").innerHTML = data[0].GName; //兼容IE
					//$(".navCon-cate-title").after(data[0].GName);
		      	},
				async: false
			});
		}
		//getFenlei();
		
		//验证手机号码是否合法
		function checkPhone(phone){
			flag = true;
			if(!(/^1[3|5|7|8|9][0-9]\d{8}$/.test(phone))){
				flag = false;
			}
			return flag;
		}	//验证手机号码,获取手机验证码并保存验证码等信息
		function encodeindex(v){
			var str = v.value;
			var pattern = new RegExp("/^[^`~!@#$%^&*()+=|\\\][\]\{\}:;'\,.<>/?]+$/");
		    var rs = ""; 
			rs = str.replace(/[&\|\\\*^$#@\-=~`/\+<>(){};'""'！@#￥……&*（）——+【】；‘’“”!_：，。、《》？]/g,""); 
		    for (var i = 0; i < str.length; i++) { 
		    } 
		    v.value=rs;
		}
		</script>
  	<jsp:include page="common/foot.jsp"></jsp:include>
  	<script src="<%=basePath %>js/new_index.js"></script>
    <script src="<%=basePath %>js/unslider.min.js"></script>
  	<script type="text/javascript">
            $('.lunbo').unslider({
    		    dots: true,
    		    delay:3500
    	    });
            //$('.sample .li_nth2').unslider();
            $('.new_div').unslider({
    		    dots: true
    	    });
    </script>
  	<!-- <script type="text/javascript" src="<%=request.getContextPath() %>/goods/js/ajaxfileupload.js"></script> -->
  </body>
</html>

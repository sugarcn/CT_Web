﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<!DOCTYPE html>
<html lang="en">
  <head>
  	<meta charset="UTF-8">
  </head>
  
  <body>
<!--页面底部-->
<div id="foot">
    <div class="foot_h">
        <ul>
            <li class="foot_icon1"><p></p></li>
            <li class="foot_icon2"><p></p></li>
            <li class="foot_icon3"><p></p></li>
        </ul>
    </div>
    <div class="foot_b">
    <ul class="foot_icon">
        <li><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/foot_gwzn.png"></li>
        <li><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/foot_pssm.png"></li>
        <li><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/foot_zffs.png"></li>
        <li><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/foot_shfw.png"></li>
        <li><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/foot_tsfw.png"></li>
    <ul>
    </div>
    <div class="foot_f">
        <ul>
            <li><b></b><a href="about/about_ct.jsp" target="_blank">关于长亭</a></li>
            <li><b></b><a href="about/contact_us.jsp" target="_blank">联系我们</a></li>
            <li><a href="about/join_us.jsp" target="_blank">加入我们</a></li>
        </ul>
        <p>长亭易购 版权所有 &copy;2014-2017 CTEGO CORPORATION All Rights Reserved&nbsp;&nbsp;&nbsp;&nbsp;
            粤ICP备16050358号-1 <script type="text/javascript">var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");document.write(unescape("%3Cspan id='cnzz_stat_icon_1255740681'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s95.cnzz.com/z_stat.php%3Fid%3D1255740681%26show%3Dpic' type='text/javascript'%3E%3C/script%3E"));</script></p>
        <p class="foot_img">
		<script id="ebsgovicon" src="https://szcert.ebs.org.cn/govicon.js?id=54c6c1f0-0dd6-4625-ab6e-8e34ad47d7ec&width=34&height=60&type=1" type="text/javascript" charset="utf-8"></script>
           
            <a href="https://webscan.360.cn/index/checkwebsite/url/www.ctego.com" style="height:50px; padding-left:10px;"><img style="height:40px; padding-top:0; margin-left:25px" border="0" src="<%=imgurl %>/bot360.png"/></a>
            <a id="_pingansec_bottomimagelarge_shiming" href="https://si.trustutn.org/info?sn=608150928017490081680&certType=1">
            <img src="<%=imgurl %>/rzlm.jpg"/></a><a id="_pinganTrust" target="_blank" href="https://c.trustutn.org/s/ctego.com">
            <img src="<%=imgurl %>/paxy.jpg"/></a>
        </p>
    </div>
</div>

<script type="text/javascript">
function aaa(){
	$.post("findStrHelpNew",
			function(data){
		$(".foot_b").prepend(data);
	});
}
aaa();
</script>
<script src="<%=basePath %>js/unslider.min.js"></script>
  </body>
</html>

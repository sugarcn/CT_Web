<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<script type="text/javascript" src="<%=request.getContextPath() %>/js/bomDetail.js" charset="utf-8"></script>

 <script type="text/javascript" src="<%=request.getContextPath() %>/js/user.js" charset="utf-8"></script>
<script type="text/javascript">
// function Cancelorder(status){
// location.href="order_status?status="+status;
// }
</script>


    <div class="left_e">
	  <dl class="dl_e">
<!-- 	  window.location.href="login.jsp?backurl="+window.location.href; -->
	  
	    <dt>订单中心</dt>
		<dd><a href="<%=request.getContextPath() %>/order_list"> 我的订单</a></dd>
		<dd><a href="<%=request.getContextPath() %>/Cancelorder_status?status=6"> 取消订单记录</a></dd>
		<c:if test="${userinfosession.UCreditLimit != null && userinfosession.UCreditLimit != 0 }">
			<dd><a href="<%=request.getContextPath() %>/xinyongOrderInfo"> 信用付款订单</a></dd>
		</c:if>
<!-- 		<dd><a href="#" onclick="Cancelorder(6)"></a> -->
	  </dl>
	  <dl class="dl_e">
	    <dt>关注中心</dt>
		<!-- <dd><a href="<%=request.getContextPath() %>/goods_collectlist"> 我的收藏</a></dd> -->
		<dd><a href="<%=request.getContextPath() %>/bom_list"> 我的BOM</a></dd>
	  </dl>
	  <dl class="dl_e">
	    <dt>客服中心</dt>
		<dd><a href="<%=request.getContextPath() %>/order_complain"> 我的投诉</a></dd>
		<dd><a href="<%=request.getContextPath() %>/order_rteurn_List"> 退换货记录</a></dd>
	  </dl>
	  <dl class="dl_e">
	    <dt>个人中心</dt>
		<dd><a href="<%=request.getContextPath() %>/user_editUser"> 修改个人信息</a></dd>
		<dd><a href="<%=request.getContextPath() %>/security_goSecurity"> 安全设置</a></dd>
		<dd><a href="javascript:goCoupon(1,1)"> 我的优惠券</a></dd>
		<dd><a href="<%=request.getContextPath() %>/userRank_goMyRank"> 我的级别</a></dd>
		<dd><a href="<%=request.getContextPath() %>/address_list"> 管理收货地址</a></dd>
		<dd><a href="javascript:goAddTicket()"> 增票信息设置</a></dd>
		<dd><a href="<%=request.getContextPath() %>/getDiscountInfo"> 邀请有礼 </a></dd>
	  </dl>
	</div>


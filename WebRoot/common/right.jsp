<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="basePath.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<script type="text/javascript" src="js/bomDetail.js" charset="utf-8"></script>
<script type="text/javascript">
// function Cancelorder(status){
// location.href="order_status?status="+status;
// }
</script>
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>

<jsp:include page="head.jsp"></jsp:include>
<div id="popDivall" class="mydivall" style="display:none;">
<div class="con_t"><a href="javascript:closecharDiv()"><img src="<%=imgurl %>/bg/chadiao.gif"></a><span>让客户经理联系我</span></div>
<div class="con_b">
	<ul>
    <li><p>您的支持是我们前进的动力！<br/>
如有问题或建议，请留下您的联系方式和邮箱及问题详情，我们会尽快与您取得联系，谢谢支持。</p></li>
	<li id="ts" style="color:red"></li>
	<c:if test="${ctUser== null }"> 
	<li><span>邮箱：</span><input name="con" type="text" id="email"/></li>
    <li><span>手机：</span><input name="con" type="text" id="mbo"/></li>
	</c:if>
	<c:if test="${ctUser!= null }"> 	 
			<c:if test="${ctUser.UEmail == null }">
    		<li><span>邮箱：</span><input name="con" type="text" id="email"/></li>
    		</c:if>
    		<c:if test="${ctUser.UEmail != null }">
    		<li><span>邮箱：</span><input name="con" type="text" id="email" value="${ctUser.UEmail}" readonly="readonly"/></li>
    		</c:if>
    		<c:if test="${ctUser.UMb == null }">
    		<li><span>手机：</span><input name="con" type="text" id="mbo"/></li>
    		</c:if>
   		    <c:if test="${ctUser.UMb != null }">
    		<li><span>手机：</span><input name="con" type="text" id="mbo" value="${ctUser.UMb}" readonly="readonly"/></li>
    		</c:if>
	</c:if>
	<li><span>详细：</span><textarea name="con" id="detail"></textarea></li>
    <li><div class="msg-up" style="display: none;"></div></li>
    <li><a href="javascript:contacts()">提交</a></li>
    </ul>
</div>
</div>
<div id="bgallfk" class="bgallfk" style="display:none;"></div>

<div class="contant_e">

    <div class="dizhilian_e"><a href="/" target="_blank">首页</a>><span>帮助中心</span></div>
     <div class="bangzhu_left">
    <h2 class="bzh_b">${help.HTitle }</h2>
    <div class="bangzhu_con">
   		<s:iterator value="help" var="he"> 			
		${he.HDesc}
		</s:iterator> 
    </div>
</div>	
<div class="bangzhu_list">
    <div class="bzh_r_n">

<div class="lxkhjl_b1"><a href="javascript:showcharDiv()">让客户经理联系我</a></div>
<s:iterator value="typeList" var="lists">      
	   	<dl class="lxkhjl_dl">
		<dt>${lists.HTypeName}</dt>
<s:iterator value="helps" var="list"> 
			<c:if test="${lists.HTypeId==list.HTypeId}"> 
				<c:if test="${list.HId == 1 }">
					<dd><a href="introduce.jsp">${list.HTitle}</a></dd>
				</c:if>	
				<c:if test="${list.HId != 1 }">
					<dd><a href="list_help_desc?help.HId=${list.HId }">${list.HTitle}</a></dd>
				</c:if>	
			</c:if>
	</s:iterator> 
		</dl>									
</s:iterator>   
</div>
    </div>
    <div class="clear"></div>	    
</div>	        

<jsp:include page="foot.jsp"></jsp:include>

  


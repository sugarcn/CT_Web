
 <%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = "//"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<script type="text/javascript" src="<%=basePath%>js/lrtk.js"></script>
 <script type="text/javascript" src="<%=basePath %>js/login.js"></script>
 <script type="text/javascript" src="<%=basePath %>js/header.js"></script>
 <script type="text/javascript" src="<%=basePath %>js/user.js" charset="utf-8"></script>
<link type="text/css" rel="stylesheet" href="<%=basePath %>css/style.css" />
<link href="css/css.css" type="text/css" rel="stylesheet"/>
<link href="css/css2.css" type="text/css" rel="stylesheet"/>
<link href="css/css3.css" type="text/css" rel="stylesheet"/>
<link rel="shortcut icon" href="<%=basePath %>favicon.ico">
<script src="<%=basePath %>js/jqueryt.js" type="text/javascript"></script>
<script src="<%=basePath %>js/script.js" type="text/javascript"></script>
<script type="text/javascript">
function check(){
	$("#kwordXuan").val($("#kword").val());
}
$(function(){ 

	$("#searchSelected").click(function(){ 
		$("#searchTab").show();
		$(this).addClass("searchOpen");
	}); 

	$("#searchTab li").hover(function(){
		$(this).addClass("selected");
	},function(){
		$(this).removeClass("selected");
	});
	 
	$("#searchTab li").click(function(){
		$("#searchSelected").html($(this).html());
		$("#searchTab").hide();
		$("#searchSelected").removeClass("searchOpen");
	});
});
function searchGoods(){
	var pid = document.getElementById("pid").value;
	var kword = document.getElementById("kword").value;
	var kwordXuan = document.getElementById("kwordXuan").value;
	if(kword==null||kword==""){
		alert("搜索关键字不能为空");
		return false;
	}else{
		document.getElementByName("searchForm").submit();
	}
}
</script>

</head>


<body>



<div class="toper">
	<div class="ant_all toper_ant">
	<c:if test="${userinfosession.UUserid != null}">
			<dl class="toperleft">	
        		<dd><span>${userinfosession.UUserid }</span><a href="<%=basePath%>login_logout" class="mfzc">退出</a></dd>
        	</dl>
		</c:if>
		<c:if test="${userinfosession.UUserid == null}">
			<dl class="toperleft">	
        		<dd><span>您好！</span><a href="<%=basePath%>login_login" class="qdl">请登录</a><a href="javascript:goRegister()" class="mfzc">免费注册</a></dd>
        	</dl>
		</c:if>
        <dl class="toperright">
        <dd class="lianxlie">
        
        <a href="<%=request.getContextPath() %>/order_list" target="_blank" class="lan_ct">我的订单</a><span class="line_ct"></span>
<div class="show_ct" onMouseOver="showhid('downmenu1')" onMouseOut="showhid2('downmenu1')">
	<a href="javascript:mycollect()" class="add_ct" onClick="showhid('downmenu1');">我的收藏</a>
	<div class="hidden_ct" id="downmenu1" style=" display:none;">
	   <a href="bom_list" target="_blank">我的BOM</a>
	   <a href="<%=request.getContextPath() %>/goods_collectlist" target="_blank">收藏夹</a>
	</div>
</div>
        
        <span class="line_ct"></span>
<div class="show_ct" onMouseOver="showhid5('downmenu3')" onMouseOut="showhid6('downmenu3')">
	<a href="/" class="add_ct" onClick="showhid5('downmenu3');">我的长亭</a>
	<div class="hidden_ct hidden_ctc" id="downmenu3" style=" display:none;">
	   <a href="<%=request.getContextPath() %>/order_list" target="_blank">待处理订单<span>9</span></a>
	   <a href="<%=request.getContextPath() %>/order_rteurn_List" target="_blank">退换货记录</a>
	   <a href="<%=request.getContextPath() %>/coupon_list?coupondetailDTO.type1=1&coupondetailDTO.type2=1" target="_blank">我的优惠券</a>
	</div>
</div>
        <span class="line_ct"></span>
        
<div class="show_ct" onMouseOver="showhid3('downmenu2')" onMouseOut="showhid4('downmenu2')">
	<a href="/" class="add_ct" onClick="showhid3('downmenu2');">客户服务</a>
	<div class="hidden_ct" id="downmenu2" style=" display:none;">
	   <a href="<%=request.getContextPath() %>/list_help_desc?help.HId=1" target="_blank">帮助中心</a>
	   <a href="/" target="_blank">在线客服</a>
	   <a href="/" target="_blank">意见建议</a>
	   <a href="<%=request.getContextPath() %>/service_email" target="_blank">客服邮箱</a>
	</div>
</div>
        
        </dd>	
        <dd class="lianxdh"><img src="<%=imgurl %>/bg/tel_ct.png"></dd>
        </dl>
<div class="clear"></div>
	</div>
<div class="clear"></div>
</div>

<div class="ad_top">
    <div class="ant_all">
        <a href="/" target="_blank"></a>
    </div>
<div class="clear"></div>
</div>

<div class="logo_b">
  <div class="ant_all logo_ant">	
	<div class="logo_left"><a href="/" target="_blank"><img src="<%=imgurl %>/bg/logo_a.gif"></a></div>
    
<form method="post" name="" action="goods_list?1=1&page=1&paid=&caid=2" target="_blank" >
	<div id="searchTxt" class="searchTxt" onMouseOver="this.className='searchTxt searchTxtHover';" onMouseOut="this.className='searchTxt';">

		
		<div class="searchMenu">
			
			<div class="searchSelected" id="searchSelected">全部产品分类</div>
			<div style="display:none;" class="searchTab" id="searchTab">
				<ul>
					<li>全部产品分类</li>
					<li>免费样品</li>
					<li>特卖专场</li>
					<li>电阻</li>
					<li>电容</li>

				</ul>
			</div>
			
		</div>
		
		<input name="keyw" type="text" />
				
	</div>
	
	<div class="searchBtn">
		<button id="searchBtn" type="submit">搜 索</button>
	</div>

</form>

    	<div class="gou_right">
	<a href="<%=request.getContextPath() %>/goods_cart">
	<s:if test="#session.islgn!=null">
	购物车结算<span>${session.islgn }</span>件
	</s:if><s:else>
	我的购物车
	</s:else>
	</a>
	</div>
    
    
    
  </div>
<div class="clear"></div>
</div>

<div class="nav">
  <div class="nav_ant">	

<div class="navwrap">	
<div class="headNav">
	<div class="navCon w1020">
		<div class="navCon-cate fl navCon_on">
			<div class="navCon-cate-title"><a href="cate_list" target="_blank">全部商品分类</a></div>
			<div class="cateMenu hide">
				<ul class="ct_left_bar">				
				<s:iterator value="#session.cateList" var="list"> 
				 <li> 
						<div class="cate-tag"><a href="/" target="_blank">${list.CName}</a></div>
						<div class="list-item hide">
							<ul class="itemleft">
							<c:forEach items="${cate}" var="cate">
							<c:if test="${list.CId==cate.parentId }">
								<dl>
									<dd>
                                    <div class="ct_t_er">
                                    	<div class="ct_t_jt"><a href="/" target="_blank" class="jt_ni">${cate.CName}</a></div>
                                    	<div class="ct_t_ne">
                                    <c:forEach items="${ca}" var="ca">
                                    	<c:if test="${cate.CId==ca.parentId }">
                                        <a href="/" target="_blank">${ca.CName}</a><span class="ct_line_t"></span>
                                    	</c:if>
                                    </c:forEach>
                                    </div>
                                    </div>
                                    </dd>
								</dl>
							</c:if>
							</c:forEach>
								<div class="fn-clear"></div>
							</ul>
						</div>
						</li>
			   </s:iterator>
				
					<li style="display:none;"></li>
				</ul> 
			</div>
		</div>

		</div>
		</div> 

</div>

<ul class="navright_d">
<li><a href="/" target="_blank">首页</a></li>
<li><a href="<%=request.getContextPath() %>/goods_list?1=1&page=1&paid=&keyw=电阻&caid=2" target="_blank">免费样品</a></li>
<li><a href="<%=request.getContextPath() %>/goods_list?1=1&page=1&paid=&keyw=电阻&caid=2" target="_blank">特卖专场</a></li>
<li><a href="<%=request.getContextPath() %>/goods_list?1=1&page=1&paid=&keyw=电阻&caid=2" target="_blank">电阻</a></li>
<li><a href="<%=request.getContextPath() %>/goods_list?1=1&page=1&paid=&keyw=电阻&caid=2" target="_blank">电容</a></li>
<li><a href="<%=request.getContextPath() %>/goods_brandList" target="_blank">品牌</a></li>
<li><a href="<%=request.getContextPath() %>/aboutus" target="_blank">关于长亭易购</a></li>
</ul>

  </div>
<div class="clear"></div>
</div>





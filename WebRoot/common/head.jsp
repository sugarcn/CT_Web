﻿ <%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
CtSystemInfo si = new CtSystemInfo();
String imgurl = si.getCtImgUrl();
String path = request.getContextPath();
String basePath = "//"+request.getServerName()+""+path+"/";
%>
 <script type="text/javascript" src="<%=basePath %>js/login.js"></script>
 <script type="text/javascript" src="<%=basePath %>js/header.js"></script>
<script src="<%=basePath %>js/jquery-1.8.3.min.js"></script> 
<script src="js/jquery.luara.0.0.1.min.js"></script>
 
 <script type="text/javascript" src="<%=basePath %>js/user.js" charset="utf-8"></script>
<link type="text/css" rel="stylesheet" href="<%=basePath %>css/style.css" />
<link href="css/css.css" type="text/css" rel="stylesheet" media="all"/>
<link href="css/css2.css" type="text/css" rel="stylesheet"/>
<link href="css/css3.css" type="text/css" rel="stylesheet"/>
<link href="css/daimabiji.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/zzsc.css" />
<link rel="shortcut icon" href="<%=basePath %>favicon.ico">
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0"/>
<script src="<%=basePath %>js/jqueryt.js" type="text/javascript"></script>
<script src="<%=basePath %>js/script.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath %>js/check.js" charset="utf-8"></script> 
<script type="text/javascript">

$(function(){ 

	$("#searchSelected").click(function(){ 
		$("#searchTab").show();
		$(this).addClass("searchOpen");
		
	}); 

	$("#searchTab li").hover(function(){
		$(this).addClass("selected");
	},function(){
		$(this).removeClass("selected");
	});
	 
	$("#searchTab li").click(function(){
		$("#searchSelected").html($(this).html());
		var searchSelected = $("#searchSelected").text();
		$("#typeDianRong").val(searchSelected);
		$("#searchTab").hide();
		$("#searchSelected").removeClass("searchOpen");
	});
});

function coup(){
	$.ajaxSetup({async:false});
   		$.post("coup_count",function(data){
   		if(data==-1){
   				
   		}else{
   				$("#coup").append(""+data+"");
   				}
   		}); 
   		$.post("order_count",function(data){
   		if(data==-1){
   				
   		}else{
   				$("#order").append(""+data+"");
   				}
   		});     
}
$(function(){
	coup();
});
function userinfo(){
	window.location.href="user_userinfo";
}
</script>

</head>


<body>

<div id="popDivallfk" class="mydivallfk" style="display:none;">
<div class="fk_t"><a href="javascript:closeDiv()"><img src="<%=imgurl%>/bg/chadiao.gif"></a><span>意见反馈</span></div>
<div class="fk_b">

	<ul>
    <li><p>您的支持是我们前进的动力！<br/>
如有问题或建议，请留下您的联系方式和邮箱及问题详情，我们会尽快与您取得联系，谢谢支持。</p></li>
	<li id="yz" style="color:red"></li>
    <li><span>姓名：</span><input name="fk" type="text" id="name"/></li>
    <li><span>手机：</span><input name="fk" maxlength="11" type="text" id="mb"/></li>
    <li><span>邮箱：</span><input name="fk" type="text" id="em"/></li>
    <li><span>反馈内容：</span><textarea name="fk" maxlength="120" id="nr">限120字。</textarea></li>
    <li><div class="msg-up" style="display: none;"></div></li>
    <li><a href="javascript:advice()">提交</a></li>

    </ul>


</div>
</div>
<div id="bgallfk" class="bgallfk" style="display:none;"></div>
<!-- 页面顶部 -->
<div id="header">
        <div class="header_content">
        <c:if test="${userinfosession.UUserid == null}">
	        <p class="register">
	            <span>您好，</span>
	            <a href="<%=basePath %>login_login"><span>请登录</span></a>
	            <a href="<%=basePath %>login_goRegister">免费注册</a>
	        </p>
        </c:if>
        <!-- 登录ok -->
        <c:if test="${userinfosession.UUserid != null}">
             <p class="p_login">
				<span><a href="javascript:userinfo();">${userinfosession.UUsername }</a></span>
                |<a href="<%=basePath%>login_logout">退出</a>
                <c:if test="${userinfosession.UIsCustomer == 1 }">
                	|<a href="<%=basePath%>add_Cus_Address">客户管理</a>
                </c:if>
            </p>
        </c:if>
        <ul>
            <li>
                <a href="<%=request.getContextPath() %>/order_list">我的订单</a>
            </li>
            <li class="header_nth2">
                <span></span>
                <a href="bom_list">我的BOM</a>

            </li>
            <li>
                <b></b>
                <a href="javascript:userinfo();">我的长亭</a>
                <c:if test="${userinfosession.UUserid == null}">
	                <ul class="sul">
	                    <li><a href="<%=request.getContextPath() %>/order_list">待处理</a></li>
	                    <li><a href="<%=request.getContextPath() %>/order_rteurn_List">退换货</a></li>
	                    <li><a href="<%=request.getContextPath() %>/coupon_list?coupondetailDTO.type1=1&coupondetailDTO.type2=1">优惠券</a></li>
	                </ul>
                </c:if>
                <!-- 登录完成后的数量 -->
                <c:if test="${userinfosession.UUserid != null}">
                    <ul class="sul">
	                    <li><a href="<%=request.getContextPath() %>/order_list">待处理</a> <span>(<a href="<%=request.getContextPath() %>/order_list" id="order"></a>)</span></li>
	                    <li><a href="<%=request.getContextPath() %>/order_rteurn_List">退换货</a> <span>(<a href="/">0</a>)</span></li>
	                    <li><a href="<%=request.getContextPath() %>/coupon_list?coupondetailDTO.type1=1&coupondetailDTO.type2=1">优惠券</a> <span>(<a href="<%=request.getContextPath() %>/coupon_list?coupondetailDTO.type1=1&coupondetailDTO.type2=1" id="coup"></a>)</span></li>
                	</ul>
                </c:if>
            </li>
            <li>
                <b></b>
                <a href="javascript:void();" onClick="showhid3('downmenu2');">客户服务</a>
                <ul class="sul">
                    <li><a href="<%=request.getContextPath() %>/list_help_desc?help.HId=1">帮助中心</a></li>
                    <li><a target=blank href="tencent://message/?uin=2881112904&Site=qq&Menu=yes">在线客服</a></li>
                    <li><a href="javascript:showDiv()">纠错有奖</a></li>
                    <li><a href="<%=request.getContextPath() %>/service_email"  target="_blank">客服邮箱</a></li>
                </ul>
            </li>
        </ul>
        </div>
    </div>
    <div class="s_content">
    <div id="search">
        <div class="logo_h">
            <a href="<%=basePath%>"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/logo.png"></a>
            <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/logo01.png">
        </div>
        <div class="seek">
            <div class="ipt1">
                <b></b>
                 <form method="post" id="searchForm" action="goods_search">
	                <input type="text" name="fgoodsDTO.kword" onafterpaste="encode(this);" value="${typeName }" onkeyup="encode(this);" id="keyw" class="txt" placeholder="请输入型号搜索">
	                <input type="button" id="searchBtn" onClick="javascript:searchgoods();" class="btn" value="搜 索">
	                <input name="fgoodsDTO.keyword" id="keywordSearch1" value="全部产品分类" type="hidden"/>
	                
	                <input type="hidden" id="weizhi" /> 
                 </form>
                 <input type="hidden" id="tempWei" />
            </div>
            <form action="goods_search" id="keySearch" method="post">
			    <input type="hidden" name="kword" id="keywSearch" />
		        <input name="fgoodsDTO.keyword" id="keywordSearch" value="全部产品分类" type="hidden"/>
	        </form>
           <!-- <p class="ipt2">
            	<c:forEach items="${searchKeywordsList }" var="list">
	                <a href="javascript:;" onClick="searchFrom('${list.SKeyword }')">${list.SKeyword }</a>|
            	</c:forEach>
            </p> -->
        </div>
        <div class="shop">
	        <s:if test="#session.islgn!=null">
	            <p>
	                <b></b>
	                <i id="cartNumAll">${session.islgn }</i>
	                <span onClick="javascript:location.href='<%=request.getContextPath() %>/goods_cart';">购物车结算</span>
	                <a>&gt;</a>
	            </p>
		            <div id="11"
			        <c:if test="${session.islgn == 0 || session.islgn == null }">
		             style="display: none;"
			        </c:if>
		            >
		                <p>样品：<span  id="simSum"></span>件</p>
		                <p>批量：<span  id="parSum"></span>件</p>
		                <p>总价：&yen;<span  id="countPrice"></span>元<a href="<%=request.getContextPath() %>/goods_cart">去结算</a></p>
		            </div>
	        </s:if>
	        <s:else>
	        	<p>
	                <b></b>
	                <i>0</i>
	                <span onClick="javascript:location.href='<%=request.getContextPath() %>/goods_cart';">购物车结算</span>
	                <a>&gt;</a>
	            </p>
	            <div style="display: none;">
		                <p>样品：<span  id="simSum"></span>件</p>
		                <p>批量：<span  id="parSum"></span>件</p>
		                <p>总价：&yen;<span  id="countPrice"></span>元<a href="<%=request.getContextPath() %>/goods_cart">去结算</a></p>
		            </div>
	        </s:else>
        </div>
    </div>
    </div>

<!--<div class="toper">
	<div class="ant_all toper_ant">
		<c:if test="${userinfosession.UUserid != null}">
			<dl class="toperleft">	
        		<dd><a href="<%=basePath %>user_userinfo">${userinfosession.UUsername }</a><a href="<%=basePath %>/login_logout" class="mfzc">退出</a></dd>
        	</dl>
		</c:if>
		<c:if test="${userinfosession.UUserid == null}">
			<dl class="toperleft">	
        		<dd><span>您好！</span><a href="<%=basePath %>login_login" class="qdl">请登录</a><a href="<%=basePath %>login_goRegister" class="mfzc">免费注册</a></dd>
        	</dl>
		</c:if>
        <dl class="toperright">
        <dd class="lianxlie">
        
        <a href="<%=request.getContextPath() %>/order_list"  class="lan_ct">我的订单</a><span class="line_ct"></span>
<div class="show_ct" onMouseOver="showhid('downmenu1')" onMouseOut="showhid2('downmenu1')">
	<a href="javascript:mycollect()" class="add_ct" onClick="showhid('downmenu1');">我的收藏</a>
	<div class="hidden_ct" id="downmenu1" style=" display:none;">
	   <a href="bom_list" >我的BOM</a>
	   <a href="<%=request.getContextPath() %>/goods_collectlist" >收藏夹</a>
	</div>
</div>
        
        <span class="line_ct"></span>
<div class="show_ct" onMouseOver="showhid5('downmenu3')" onMouseOut="showhid6('downmenu3')">
	<a href="javascript:userinfo();" class="add_ct">我的长亭</a>
	<c:if test="${userinfosession.UUserid == null}">
		<div class="hidden_ct hidden_ctc" id="downmenu3" style=" display:none;width: 63px;">
		   <a href="<%=request.getContextPath() %>/order_list">待处理订单</a>
		  
		   <a href="<%=request.getContextPath() %>/order_rteurn_List" >退换货记录</a>
		   <a href="<%=request.getContextPath() %>/coupon_list?coupondetailDTO.type1=1&coupondetailDTO.type2=1" >我的优惠券</a>	 
		</div>
	</c:if>
	<c:if test="${userinfosession.UUserid != null}">
		<div class="hidden_ct hidden_ctc" id="downmenu3" style=" display:none;">
		   <a href="<%=request.getContextPath() %>/order_list">待处理订单<span id="order" style="margin-left:5px;"></span></a>
		  
		   <a href="<%=request.getContextPath() %>/order_rteurn_List" >退换货记录</a>
		   <a href="<%=request.getContextPath() %>/coupon_list?coupondetailDTO.type1=1&coupondetailDTO.type2=1" >我的优惠券<span id="coup" style="margin-left:5px;"></span></a>	 
		</div>
	</c:if>
</div>
        <span class="line_ct"></span>
        
<div class="show_ct" onMouseOver="showhid3('downmenu2')" onMouseOut="showhid4('downmenu2')">
	<a href="/" class="add_ct" onClick="showhid3('downmenu2');">客户服务</a>
	<div class="hidden_ct" id="downmenu2" style=" display:none;">
	   <a href="<%=request.getContextPath() %>/list_help_desc?help.HId=1" >帮助中心</a>
	   <a href="/" target="_blank">在线客服</a>
	   <a href="javascript:showDiv()" class="text survey">纠错有奖</a>
	   <a href="<%=request.getContextPath() %>/service_email">客服邮箱</a>
	</div>
</div>
        
        </dd>	
        <dd class="lianxdh"><img src="<%=imgurl %>/bg/tel_ct.png"></dd>
        </dl>
<div class="clear"></div>
	</div>
<div class="clear"></div>
</div>-->

<!--
<div class="ad_top">
    <div class="ant_all">
        <a href="<%=basePath %>sx.jsp"></a>
    </div>
<div class="clear"></div>
</div>
-->

<!-- <div class="logo_b">
  <div class="ant_all logo_ant">	
	<div class="logo_left"><a href="/" target="_blank"><img src="<%=imgurl %>/bg/logo_a.png"></a></div>
    
<form method="post" id="searchForm" action="goods_search">
	<div id="searchTxt" class="searchTxt" onMouseOver="this.className='searchTxt searchTxtHover';" onMouseOut="this.className='searchTxt';">
<input name="fgoodsDTO.keyword" id="keywordSearch1" value="全部产品分类" type="hidden"/>
		<input name="fgoodsDTO.kword" type="text"  onafterpaste="encode(this);" onkeyup="encode(this);" id="keyw" value="${typeName }"/>
				<div class="sobottom">
				<c:forEach items="${searchKeywordsList }" var="list">
					<a href="javascript:;" onClick="searchFrom('${list.SKeyword }')">${list.SKeyword } </a>|
				</c:forEach>
				</div>
	<input type="hidden" id="tempWei" />
	</div>
	<script type="text/javascript">
	function searchFrom(str){
		$("#keywSearch").val(str);
		$("#keySearch").submit();
	}
	function searchFromClose(str){
		$("#keywSearch").val(str);
		$("#keySearch").submit();
	}
	function encode(v){
		var keycode = event.which||event.keyCode;
		switch(keycode){
		  case 8://left功能；
		  		return;
	      case 37://left功能；
				return;
	      case 38://up
	    	  	return;
	      case 39://right
	    	  	return;
	      case 40://down
	    	  	return;
		}
		var str = v.value;
		var pattern = new RegExp("/^[^`~!@#$%^&*()+=|\\\][\]\{\}:;'\,.<>/?]+$/");
	    var rs = ""; 
	    if(/[&\|\\\*^$#@\-=~`+<>(){};'""'！@#￥……&*（）——+【】；‘’“”!_：，。、《》？]/g.test(str)){
			rs = str.replace(/[&\|\\\*^$#@\-=~`+<>(){};'""'！@#￥……&*（）——+【】；‘’“”!_：，。、《》？]/g,""); 
			$("#keyw").val(rs);
	    }
	}
</script>

	<div class="searchBtn">
		<button id="searchBtn" type="button" onClick="javascript:searchgoods();">搜 索</button>
	</div>

</form>
 <form action="goods_search" id="keySearch" method="post">
        	<input type="hidden" name="kword" id="keywSearch" />
        	 <input name="fgoodsDTO.keyword" id="keywordSearch" value="全部产品分类" type="hidden"/>
        </form>

    	<div class="gou_right">
	<a href="<%=request.getContextPath() %>/goods_cart">
	<s:if test="#session.islgn!=null">
	购物车结算<span>${session.islgn }</span>件
	</s:if><s:else>
	我的购物车
	</s:else>
	</a>
	</div>
    
    
    
  </div>
<div class="clear"></div>
</div>-->
	 <!--固定搜索栏-->
	 <script type="text/javascript">
	     $(window).scroll(function(){
	        var mTop=$(".s_content").prop("offsetTop");
	        var height=parseFloat($(".s_content").css("height"));
	        var wTop=$(window).scrollTop();
	        if(wTop>(mTop+height)){
	            $(".s_content").addClass("active");
	        }else{
	            $(".s_content").removeClass("active");
	        }
	     });
	 </script>

<!-- nav -->
<div class="nav">
    <!-- <div class="contentcenter"> -->

<div class="navCon-cate">
<ul class="navul">
    <li class="products">
        <b></b>全部商品分类
        <ul class="cate_box">
        	<c:forEach items="${cateListApplication }" var="cate1">
	            <li onmouseover="$(this).children('i').css('background-position','${cate1.mouseOverPos}');" onmouseout="$(this).children('i').css('background-position','${cate1.mouseOutPos}');">
	                <i style="background:url('https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/icon02.png') no-repeat ${cate1.mouseOutPos};"></i>
	                <b>&gt;</b>
	                <a
	                <c:if test="${cate1.cateGoodsNum != 0 }">
	                	 onclick="getQuEncode('${cate1.CName }');"  target="_self" 
	                </c:if>
	                 href="javascript:;"
	                 >${cate1.CName }(${cate1.cateGoodsNum })</a>
	                <div class="tanchu">
	                    <ul>
	                    	<c:forEach items="${cateListSerApplication }" var="cate2">
	                    		<c:if test="${cate2.parentId == cate1.CId }">
			                        <li>
			                            <a href="javascript:;"
											<c:if test="${cate2.cateGoodsNum != 0 }">
							                	 onclick="getQuEncode('${cate2.CName }');" target="_self" 
							                </c:if>
			                            >${cate2.CName }(${cate2.cateGoodsNum })</a>
			                        </li>
	                    		</c:if>
	                    	</c:forEach>
	                    </ul>
	                </div>
	            </li>
        	</c:forEach>
        </ul>
    </li>
    <li><b></b><a href="<%=basePath%>"  class="hover">首页</a></li>
    <li><b></b><a href="/" >BOM配表</a></li>
    <li><b></b><a href="<%=request.getContextPath() %>/goods_brandList" target="_blank" >品牌</a></li>
<li><b></b><a href="<%=request.getContextPath() %>/list_help_desc?help.HId=3" target="_blank" >购买流程</a></li>
    <li><b></b><a href="<%=request.getContextPath() %>/about/about_ct.jsp" target="_blank" >关于长亭</a></li>
</ul>
        <p>
            <b></b>
            <a>服务热线：</a>
            <span>0755-33561467</span><br>
            <span>13662258574</span>
        </p>
</div>
<script type="text/javascript">
var ul = $(".tanchu>ul");
for (var i = 0; i < ul.length; i++) {
    var li = $(ul[i]).children("li");
    if (li.length > 10) {
        $(li).css("float", "left").css("width", "45%").css("margin-left","5%");
    }
}
$(document).ready(function(){
	//搜索栏下拉列表样式
    $("#keyw").on("keydown",function(e){
    	var li=$("#ui-idq-1>li");
    	var val=$(this).val();
    	for(var i=0;i<li.length;i++){
    		if($(li[i]).text()==val){
    			$(li[i]).children("a").addClass("ui-state-focus");
    			$(li[i]).siblings("li").children(".ui-state-focus").removeClass("ui-state-focus");
    		}
    	}
        
    });
});
function searchFrom(str){
	$("#keywSearch").val(str);
	$("#keySearch").attr('target', '_blank');
	$("#keySearch").submit();
	return false;
}
function xialaseach(){
	var $keyXiala = $("#keyXiala").val();
	if($keyXiala == ""){
		alert("您还没有输入关键字呢");
		return;
	}
	searchFrom($keyXiala);
}
function check(){
	$("#kwordXuan").val($("#kword").val());
}
$(function(){ 
	$("#searchSelected").click(function(){ 
		$("#searchTab").show();
		$(this).addClass("searchOpen");
	}); 

	$("#searchTab li").hover(function(){
		$(this).addClass("selected");
	},function(){
		$(this).removeClass("selected");
	});
	 
	$("#searchTab li").click(function(){
		$("#searchSelected").html($(this).html());
		var searchSelected = $("#searchSelected").text();
		$("#typeDianRong").val(searchSelected);
		$("#searchTab").hide();
		$("#searchSelected").removeClass("searchOpen");
	});
});

function searchgoods(){
	var kword = $("#keyw").val();
	var searchSelected = $("#searchSelected").text();
	$("#typeDianRong").val(searchSelected);
	if(kword==null||kword==""){
		alert("搜索关键字不能为空");
		return false;
	}else{
		$("#searchForm").submit();
	}
}
function searchFromClose(str){
	$("#keywSearch").val(str);
	$("#keySearch").submit();
}
function userinfo(){
	window.location.href="user_userinfo";
}
function coup(){
	$.ajaxSetup({async:false});
   		$.post("coup_count",function(data){
   		if(data==-1){
   				
   		}else{
   				$("#coup").append(""+data+"");
   				}
   		}); 
   		$.post("order_count",function(data){
   		if(data==-1){
   				
   		}else{
   				$("#order").append(""+data+"");
   				}
   		});     
}
$("#keyw").autocomplete({
	source:function(request,response){
		$.post("findKeywordAjax",{"fgoodsDTO.keyword":$("#keyw").val()},function(data){
  			response(data);
  			//remandadd();
  		});
	}
});
$("#keyXiala").autocomplete({
	source:function(request,response){
		$.post("findKeywordAjax",{"fgoodsDTO.keyword":$("#keyXiala").val()},function(data){
  			response(data);
  			//remandadd();
  		});
	}
});



function coup(){
	$.ajaxSetup({async:false});
   		$.post("coup_count",function(data){
   		if(data==-1){
   				
   		}else{
   				$("#coup").append(""+data+"");
   				}
   		}); 
   		$.post("order_count",function(data){
   		if(data==-1){
   				
   		}else{
   				$("#order").append(""+data+"");
   				}
   		});     
}
coup();
//下载bom模板
function DownloadForBomModel(){
	window.open("https://ctegores.oss-cn-shenzhen.aliyuncs.com/bomexample.csv");
	//$.post("downLoadForBom",function(data,start){
	//	$("#downloadFile").attr("href",data);
	//});
}
function encode(v){
	var keycode = event.which||event.keyCode;
	switch(keycode){
	  case 8://left功能；
  			return;
      case 37://left功能；
			return;
      case 38://up
    	  	return;
      case 39://right
    	  	return;
      case 40://down
    	  	return;
	}
	var str = v.value;
	var pattern = new RegExp("/^[^`~!@#$%^&*()+=|\\\][\]\{\}:;'\,.<>/?]+$/");
	var rs = ""; 
    if(/[&\|\\\*^$#@\=~`+<>(){};'""'！@#￥……&*（）——+【】；‘’“”!_：，。、《》？]/g.test(str)){
		rs = str.replace(/[&\|\\\*^$#@\=~`+<>(){};'""'！@#￥……&*（）——+【】；‘’“”!_：，。、《》？]/g,""); 
		$("#keyw").val(rs);
    }
}
</script>   
<div class="clear"></div>
    </div>
</div>




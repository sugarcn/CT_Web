<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%
String path = request.getContextPath();
String basePath = "//"+request.getServerName()+path+"/";
CtSystemInfo si = new CtSystemInfo();
String imgurl = si.getCtImgUrl();
String key=si.getCtKey();
String title=si.getCtTitle();
%>
 <script type="text/javascript" src="<%=basePath %>js/login.js"></script>
 <script type="text/javascript" src="<%=basePath %>js/header.js"></script>
  <script type="text/javascript" src="<%=basePath %>js/header.js"></script>
  <script type="text/javascript" src="<%=basePath %>js/user.js" charset="utf-8"></script>
<link type="text/css" rel="stylesheet" href="<%=basePath %>css/style.css" />
<link href="css/css.css" type="text/css" rel="stylesheet" media="all"/>
<link href="css/css2.css" type="text/css" rel="stylesheet"/>
<link href="css/css3.css" type="text/css" rel="stylesheet"/>
<link href="css/daimabiji.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/zzsc.css" />
<link rel="shortcut icon" href="<%=basePath %>favicon.ico">
<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0"/>
<script src="<%=basePath %>js/jqueryt.js" type="text/javascript"></script>
<script src="<%=basePath %>js/script.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath %>js/check.js" charset="utf-8"></script> 
<script type="text/javascript">
function check(){
	$("#kwordXuan").val($("#kword").val());
}
$(function(){ 

	$("#searchSelected").click(function(){ 
		$("#searchTab").show();
		$(this).addClass("searchOpen");
	}); 

	$("#searchTab li").hover(function(){
		$(this).addClass("selected");
	},function(){
		$(this).removeClass("selected");
	});
	 
	$("#searchTab li").click(function(){
		$("#searchSelected").html($(this).html());
		var searchSelected = $("#searchSelected").text();
		$("#typeDianRong").val(searchSelected);
		$("#searchTab").hide();
		$("#searchSelected").removeClass("searchOpen");
	});
});

function searchgoods(){
	var kword = $("#keyw").val();
	var searchSelected = $("#searchSelected").text();
	$("#typeDianRong").val(searchSelected);
	if(kword==null||kword==""){
		alert("搜索关键字不能为空");
		return false;
	}else{
		$("#searchForm").submit();
	}
}
function userinfo(){
	window.location.href="user_userinfo";
}
function coup(){
	$.ajaxSetup({async:false});
   		$.post("coup_count",function(data){
   		if(data==-1){
   				
   		}else{
   				$("#coup").append(""+data+"");
   				}
   		}); 
   		$.post("order_count",function(data){
   		if(data==-1){
   				
   		}else{
   				$("#order").append(""+data+"");
   				}
   		});     
}
$(function(){
	coup();
});
</script>
<script src="js/scriptindexlist.js" type="text/javascript"></script>
</head>


<body>



<div id="popDivallfk" class="mydivallfk" style="display:none;">
<div class="fk_t"><a href="javascript:closeDiv()"><img src="<%=imgurl %>/bg/chadiao.gif"></a><span>意见反馈</span></div>
<div class="fk_b">

	<ul>
    <li><p>您的支持是我们前进的动力！<br/>
如有问题或建议，请留下您的联系方式和邮箱及问题详情，我们会尽快与您取得联系，谢谢支持。</p></li>
	<li id="yz" style="color:red"></li>
    <li><span>姓名：</span><input id="name" name="fk" type="text"/></li>
    <li><span>手机：</span><input id="mb" maxlength="11" name="fk" type="text"/></li>
    <li><span>邮箱：</span><input id="em" name="fk" type="text"/></li>
    <li><span>反馈内容：</span><textarea id="nr" maxlength="120" name="fk" >限120字。</textarea></li>
    <li><a href="javascript:advice()">提交</a></li>

    </ul>


</div>
</div>
<div id="bgallfk" class="bgallfk" style="display:none;"></div>


<div class="toper">
	<div class="ant_all toper_ant">
	<c:if test="${userinfosession.UUserid != null}">
			<dl class="toperleft">	
        		<dd><a href="javascript:userinfo();">${userinfosession.UUsername }</a><a href="<%=basePath%>login_logout" class="mfzc">退出</a></dd>
        	</dl>
		</c:if>
		<c:if test="${userinfosession.UUserid == null}">
			<dl class="toperleft">	
        		<dd><span>您好！</span><a href="<%=basePath %>login_login" class="qdl">请登录</a><a href="<%=basePath %>login_goRegister" class="mfzc">免费注册</a></dd>
        	</dl>
		</c:if>
        <dl class="toperright">
        <dd class="lianxlie">
        
        <a href="<%=request.getContextPath() %>/order_list"  class="lan_ct">我的订单</a><span class="line_ct"></span>
<div class="show_ct" onMouseOver="showhid('downmenu1')" onMouseOut="showhid2('downmenu1')">
	<a href="javascript:mycollect()" class="add_ct" onClick="showhid('downmenu1');">我的收藏</a>
	<div class="hidden_ct" id="downmenu1" style=" display:none;">
	   <a href="bom_list" >我的BOM</a>
	   <a href="<%=request.getContextPath() %>/goods_collectlist" >收藏夹</a>
	</div>
</div>
        
        <span class="line_ct"></span>
<div class="show_ct" onMouseOver="showhid5('downmenu3')" onMouseOut="showhid6('downmenu3')">
	<a href="javascript:userinfo();" class="add_ct" onClick="showhid5('downmenu3');">我的长亭</a>
	<c:if test="${userinfosession.UUserid == null}">
		<div class="hidden_ct hidden_ctc" id="downmenu3" style=" display:none;width: 63px;">
		   <a href="<%=request.getContextPath() %>/order_list">待处理订单</a>
		  
		   <a href="<%=request.getContextPath() %>/order_rteurn_List" >退换货记录</a>
		   <a href="<%=request.getContextPath() %>/coupon_list?coupondetailDTO.type1=1&coupondetailDTO.type2=1" >我的优惠券</a>	 
		</div>
	</c:if>
	<c:if test="${userinfosession.UUserid != null}">
		<div class="hidden_ct hidden_ctc" id="downmenu3" style=" display:none;">
		   <a href="<%=request.getContextPath() %>/order_list">待处理订单<span id="order" style="margin-left:5px;"></span></a>
		  
		   <a href="<%=request.getContextPath() %>/order_rteurn_List" >退换货记录</a>
		   <a href="<%=request.getContextPath() %>/coupon_list?coupondetailDTO.type1=1&coupondetailDTO.type2=1" >我的优惠券<span id="coup" style="margin-left:5px;"></span></a>	 
		</div>
	</c:if>
</div>
        <span class="line_ct"></span>
        
<div class="show_ct" onMouseOver="showhid3('downmenu2')" onMouseOut="showhid4('downmenu2')">
	<a href="/" class="add_ct" onClick="showhid3('downmenu2');">客户服务</a>
	<div class="hidden_ct" id="downmenu2" style=" display:none;">
	   <a href="<%=request.getContextPath() %>/list_help_desc?help.HId=1">帮助中心</a>
	   <a href="/" target="_blank">在线客服</a>
	    <a href="javascript:showDiv()" class="text survey">纠错有奖</a>
	   <a href="<%=request.getContextPath() %>/service_email">客服邮箱</a>
	</div>
</div>
        
        </dd>	
        <dd class="lianxdh"><img src="<%=imgurl %>/bg/tel_ct.png"></dd>
        </dl>
<div class="clear"></div>
	</div>
<div class="clear"></div>
</div>

<div class="ad_top">
    <div class="ant_all">
            <a href="<%=basePath %>sx.jsp" target="_blank"></a>
    </div>
<div class="clear"></div>
</div>

<div class="logo_b">
  <div class="ant_all logo_ant">	
	<div class="logo_left"><a href="/" target="_blank"><img src="<%=imgurl %>/bg/logo_a.png"></a></div>
    
<form method="post" id="searchForm" action="goods_search">
	<div id="searchTxt" class="searchTxt" onMouseOver="this.className='searchTxt searchTxtHover';" onMouseOut="this.className='searchTxt';">

		
		<div class="searchMenu">
			
			<div class="searchSelected" id="searchSelected">全部产品分类</div>
			<div style="display:none;" class="searchTab" id="searchTab">
				<ul>
					<li>全部产品分类</li>
					<li>免费样品</li>
					<li>特卖专场</li>
					<li>电阻</li>
					<li>电容</li>

				</ul>
			</div>
			
		</div>
		
		<input type="hidden" name="fgoodsDTO.keyword" value="全部产品分类" id="typeDianRong" />
		<input name="fgoodsDTO.kword" type="text" id="keyw"/>
				
	</div>
	
	<div class="searchBtn">
		<button id="searchBtn" type="button" onClick="javascript:searchgoods();">搜 索</button>
	</div>
</form>

    
    	<div class="gou_right">
	<a href="<%=request.getContextPath() %>/goods_cart">
	<s:if test="#session.islgn!=null">
	购物车结算<span>${session.islgn }</span>件
	</s:if><s:else>
	我的购物车
	</s:else>
	</a>
	</div>
    
    
    
  </div>
<div class="clear"></div>
</div>

<div class="nav">
  <div class="nav_ant">	
<div class="navwrap">	
<div class="headNav">
	<div class="navCon w1020">

		<div class="navCon-cate fl navCon_on">
			<div  class="navCon-cate-title"><a href="cate_list" target="_blank">全部商品分类</a></div>
			<span id="cate-title"></span>
		</div>

		</div>
		</div>

</div>

<script type="text/javascript">
	function getFenlei(){
		
		$.ajax({ 
			type: "POST",
			url: "getFirstCateForShou",
			dataType: "json",
			success: function(data){
				document.getElementById("cate-title").innerHTML = data[0].GName; //兼容IE
				//$(".navCon-cate-title").after(data[0].GName);
	      	},
			async: false
		});
	}
	getFenlei();
</script>
<ul class="navright_d">
<li><a href="<%=basePath%>">首页</a></li>
<li><a href="<%=request.getContextPath() %>/goods_search?pid=2&keyword=免费样品&cid=2">免费样品</a></li>
<li><a href="<%=request.getContextPath() %>/goods_search?pid=2&keyword=电阻&cid=2">特卖专场</a></li>
<li><a href="<%=request.getContextPath() %>/goods_search?pid=2&keyword=电阻&cid=2">电阻</a></li>
<li><a href="<%=request.getContextPath() %>/goods_search?pid=3&keyword=电容&cid=3">电容</a></li>
<li><a href="<%=request.getContextPath() %>/goods_brandList" target="_blank">品牌</a></li>
<li><a href="<%=request.getContextPath() %>/aboutus" target="_blank">关于长亭易购</a></li>
</ul>

  </div>
<div class="clear"></div>
</div>





﻿ <%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
CtSystemInfo si = new CtSystemInfo();
String imgurl = si.getCtImgUrl();
String path = request.getContextPath();
String basePath = "//"+request.getServerName()+":8089"+path+"/";
%>
 <script type="text/javascript" src="<%=basePath %>js/login.js"></script>
 <script type="text/javascript" src="<%=basePath %>js/header.js"></script>
  <script type="text/javascript" src="<%=basePath %>js/jquery-1.8.3.min.js"></script>
 
<script src="js/jquery.luara.0.0.1.min.js"></script>
 
 <script type="text/javascript" src="<%=basePath %>js/user.js" charset="utf-8"></script>
<link type="text/css" rel="stylesheet" href="<%=basePath %>css/style.css" />
<link href="css/css.css" type="text/css" rel="stylesheet" media="all"/>
<link href="css/css2.css" type="text/css" rel="stylesheet"/>
<link href="css/css3.css" type="text/css" rel="stylesheet"/>
<link href="css/daimabiji.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/zzsc.css" />
<link rel="shortcut icon" href="<%=basePath %>favicon.ico">
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="电子元器件样品小批量采购平台，为中小企业，创客科研单位，提供配套服务方案，提供拆零服务，只卖原装正品。" />
<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0"/>
<script src="<%=basePath %>js/jqueryt.js" type="text/javascript"></script>
<script src="<%=basePath %>js/script.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath %>js/check.js" charset="utf-8"></script> 
<script type="text/javascript">

$(function(){ 

	$("#searchSelected").click(function(){ 
		$("#searchTab").show();
		$(this).addClass("searchOpen");
		
	}); 

	$("#searchTab li").hover(function(){
		$(this).addClass("selected");
	},function(){
		$(this).removeClass("selected");
	});
	 
	$("#searchTab li").click(function(){
		$("#searchSelected").html($(this).html());
		var searchSelected = $("#searchSelected").text();
		$("#typeDianRong").val(searchSelected);
		$("#searchTab").hide();
		$("#searchSelected").removeClass("searchOpen");
	});
});

function coup(){
	$.ajaxSetup({async:false});
   		$.post("coup_count",function(data){
   		if(data==-1){
   				
   		}else{
   				$("#coup").append(""+data+"");
   				}
   		}); 
   		$.post("order_count",function(data){
   		if(data==-1){
   				
   		}else{
   				$("#order").append(""+data+"");
   				}
   		});     
}
$(function(){
	coup();
});
function userinfo(){
	window.location.href="user_userinfo";
}
</script>

</head>


<body>

<div id="popDivallfk" class="mydivallfk" style="display:none;">
<div class="fk_t"><a href="javascript:closeDiv()"><img src="<%=imgurl%>/bg/chadiao.gif"></a><span>意见反馈</span></div>
<div class="fk_b">

	<ul>
    <li><p>您的支持是我们前进的动力！<br/>
如有问题或建议，请留下您的联系方式和邮箱及问题详情，我们会尽快与您取得联系，谢谢支持。</p></li>
	<li id="yz" style="color:red"></li>
    <li><span>姓名：</span><input name="fk" type="text" id="name"/></li>
    <li><span>手机：</span><input name="fk" maxlength="11" type="text" id="mb"/></li>
    <li><span>邮箱：</span><input name="fk" type="text" id="em"/></li>
    <li><span>反馈内容：</span><textarea name="fk" maxlength="120" id="nr">限120字。</textarea></li>
    <li><div class="msg-up" style="display: none;"></div></li>
    <li><a href="javascript:advice()">提交</a></li>

    </ul>


</div>
</div>
<div id="bgallfk" class="bgallfk" style="display:none;"></div>

<div class="toper">
	<div class="ant_all toper_ant">
	<c:if test="${userinfosession.UUserid != null}">
			<dl class="toperleft">	
        		<dd><a href="<%=basePath %>user_userinfo">${userinfosession.UUsername }</a><a href="<%=basePath %>/login_logout" class="mfzc">退出</a></dd>
        	</dl>
		</c:if>
		<c:if test="${userinfosession.UUserid == null}">
			<dl class="toperleft">	
        		<dd><span>您好！</span><a href="<%=basePath %>login_login" class="qdl">请登录</a><a href="<%=basePath %>login_goRegister" class="mfzc">免费注册</a></dd>
        	</dl>
		</c:if>
        <dl class="toperright">
        <dd class="lianxlie">
        
        <a href="<%=request.getContextPath() %>/order_list"  class="lan_ct">我的订单</a><span class="line_ct"></span>
<div class="show_ct" onMouseOver="showhid('downmenu1')" onMouseOut="showhid2('downmenu1')">
	<a href="javascript:mycollect()" class="add_ct" onClick="showhid('downmenu1');">我的收藏</a>
	<div class="hidden_ct" id="downmenu1" style=" display:none;">
	   <a href="bom_list" >我的BOM</a>
	   <a href="<%=request.getContextPath() %>/goods_collectlist" >收藏夹</a>
	</div>
</div>
        
        <span class="line_ct"></span>
<div class="show_ct" onMouseOver="showhid5('downmenu3')" onMouseOut="showhid6('downmenu3')">
	<a href="javascript:userinfo();" class="add_ct">我的长亭</a>
	<c:if test="${userinfosession.UUserid == null}">
		<div class="hidden_ct hidden_ctc" id="downmenu3" style=" display:none;width: 63px;">
		   <a href="<%=request.getContextPath() %>/order_list">待处理订单</a>
		  
		   <a href="<%=request.getContextPath() %>/order_rteurn_List" >退换货记录</a>
		   <a href="<%=request.getContextPath() %>/coupon_list?coupondetailDTO.type1=1&coupondetailDTO.type2=1" >我的优惠券</a>	 
		</div>
	</c:if>
	<c:if test="${userinfosession.UUserid != null}">
		<div class="hidden_ct hidden_ctc" id="downmenu3" style=" display:none;">
		   <a href="<%=request.getContextPath() %>/order_list">待处理订单<span id="order" style="margin-left:5px;"></span></a>
		  
		   <a href="<%=request.getContextPath() %>/order_rteurn_List" >退换货记录</a>
		   <a href="<%=request.getContextPath() %>/coupon_list?coupondetailDTO.type1=1&coupondetailDTO.type2=1" >我的优惠券<span id="coup" style="margin-left:5px;"></span></a>	 
		</div>
	</c:if>
</div>
        <span class="line_ct"></span>
        
<div class="show_ct" onMouseOver="showhid3('downmenu2')" onMouseOut="showhid4('downmenu2')">
	<a href="/" class="add_ct" onClick="showhid3('downmenu2');">客户服务</a>
	<div class="hidden_ct" id="downmenu2" style=" display:none;">
	   <a href="<%=request.getContextPath() %>/list_help_desc?help.HId=1" >帮助中心</a>
	   <a href="/" target="_blank">在线客服</a>
	   <a href="javascript:showDiv()" class="text survey">纠错有奖</a>
	   <a href="<%=request.getContextPath() %>/service_email">客服邮箱</a>
	</div>
</div>
        
        </dd>	
        <dd class="lianxdh"><img src="<%=imgurl %>/bg/tel_ct.png"></dd>
        </dl>
<div class="clear"></div>
	</div>
<div class="clear"></div>
</div>

<div class="ad_top">
    <div class="ant_all">
        <a href="<%=basePath %>sx.jsp"></a>
    </div>
<div class="clear"></div>
</div>

<!-- 固定顶部 logo-search -->
<div id="logo-search">

        <div id="scroll_nav">
        
<div class="contentcenter">
    <div class="logo_left"><a href="/">电子元器件超市</a></div>
    <div class="searcher" >
        <form method="post" id="searchForm" action="goods_search">
        <div class="searchMenu">
            <div class="searchinput"><input name="fgoodsDTO.kword" type="text" id="keyw"/></div>
            <input name="fgoodsDTO.keyword" value="全部产品分类" type="hidden"/>
			<input type="hidden" id="weizhi" />
<script src="js/jqueryt.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<%=basePath %>css/demos.css">
<link rel="stylesheet" type="text/css" href="<%=basePath %>css/themes/base/jquery.ui.all.css">
<script type="text/javascript" src="js/jquery.ui.core.js"></script>
<script type="text/javascript" src="js/jquery.ui.widget.js"></script>
<script type="text/javascript" src="js/jquery.ui.autocomplete.js"></script>
<script type="text/javascript" src="js/jquery.ui.menu.js"></script>
<script type="text/javascript" src="js/jquery.ui.position.js"></script>


 
        </div>
        <div class="searchBtn"><button id="searchBtn" type="button" onClick="javascript:searchgoods();">搜 索</button></div>
        </form>
        <form action="goods_search" id="keySearch" method="post">
        	<input type="hidden" name="fgoodsDTO.keyword" id="keywSearch" />
        </form>
        <div class="goodsbot">
        <c:forEach items="${searchKeywordsList }" var="list">
			<a href="javascript:;" onClick="searchFrom('${list.SKeyword }')">${list.SKeyword } </a>
		</c:forEach>
		</div>
		<input type="hidden" id="tempWei" />
<script type="text/javascript">
	function searchFrom(str){
		$("#keywSearch").val(str);
		//$("#keySearch").attr('target', '_blank');
		$("#keySearch").submit();
	}
</script>



		
    </div>
    
    <s:if test="#session.islgn!=null">
	    <div class="shopping" onMouseOver="showhid7('downmenu4')" onMouseOut="showhid8('downmenu4')">
	        <a href="<%=request.getContextPath() %>/goods_cart" class="add_ct" onClick="showhid7('downmenu4');">购物车结算<span>${session.islgn }</span>件</a>
	        <div class="hidden_ct" id="downmenu4" style=" display:none; ">
		        <dl class="fqujs">
			        <dd><span>样品：</span><i id="simSum"></i><p>件</p></dd>
			        <dd><span>批量：</span><i id="parSum"></i><p>件</p></dd>
			        <dt><span>总价：</span><i id="countPrice">￥ </i><p>元</p></dt>        
			        <dd><a href="<%=request.getContextPath() %>/goods_cart">去结算</a></dd>
		        </dl>
	        </div>
		</div>
	</s:if><s:else>
		<div class="shopping">
	        <a href="<%=request.getContextPath() %>/goods_cart" class="add_ct">购物车结算<span>0</span>件</a>
		</div>
	</s:else>
</div>
<div class="clear"></div>
        </div>
</div>
<!-- nav -->
<div class="nav">
    <div class="contentcenter">

<div class="navCon-cate"></div>
<ul class="navul">
    <li><a href="<%=basePath%>"  class="hover">首页</a></li>
    <li><a href="/" >活动专区</a></li>
    <li><a href="<%=request.getContextPath() %>/goods_brandList" target="_blank" >品牌</a></li>
<li><a href="<%=request.getContextPath() %>/list_help_desc?help.HId=9" target="_blank" >配送时间说明</a></li>
    <li><a href="<%=request.getContextPath() %>/aboutus" target="_blank" >长亭易购</a></li>
</ul>
<div class="navCon-tel">服务热线：4008-620062</div>
    
<div class="clear"></div>
    </div>
</div>




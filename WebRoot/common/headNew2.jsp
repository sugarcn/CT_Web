﻿<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%
String path = request.getContextPath();
String basePath = "//"+request.getServerName()+path+"/";
CtSystemInfo si = new CtSystemInfo();
String imgurl = si.getCtImgUrl();
String key=si.getCtKey();
String title=si.getCtTitle();
%>
  <head>
	    <meta charset="UTF-8">
	    <title>Title</title>
	    <meta name="Keywords" content="${systemInfo.ctKey }" />
		<meta name="Description" content="${systemInfo.ctTitle }" />
	    <link rel="stylesheet" href="css/index.css">
	    <link rel="shortcut icon" href="<%=basePath %>favicon.ico">
  </head>
  
<body scroll="no">
    <!--页面顶部-->
    <div id="header">
        <div class="header_content">
        <c:if test="${userinfosession.UUserid == null}">
	        <p class="register">
	            <span>您好，</span>
	            <a href="<%=basePath %>login_login"><span>请登录</span></a>
	            <a href="<%=basePath %>login_goRegister">免费注册</a>
	        </p>
        </c:if>
        <!-- 登录ok -->
        <c:if test="${userinfosession.UUserid != null}">
             <p class="p_login">
				<span><a href="javascript:userinfo();">${userinfosession.UUsername }</a></span>
                |<a href="<%=basePath%>login_logout">退出</a>
            </p>
        </c:if>
        <ul>
            <li>
                <a href="<%=request.getContextPath() %>/order_list">我的订单</a>
            </li>
            <li class="header_nth2">
                <span></span>
                <a href="javascript:mycollect()">我的BOM</a>

            </li>
            <li>
                <b></b>
                <a href="javascript:userinfo();">我的长亭</a>
                <c:if test="${userinfosession.UUserid == null}">
	                <ul class="sul">
	                    <li><a href="<%=request.getContextPath() %>/order_list">待处理</a></li>
	                    <li><a href="<%=request.getContextPath() %>/order_rteurn_List">退换货</a></li>
	                    <li><a href="<%=request.getContextPath() %>/coupon_list?coupondetailDTO.type1=1&coupondetailDTO.type2=1">优惠券</a></li>
	                </ul>
                </c:if>
                <!-- 登录完成后的数量 -->
                <c:if test="${userinfosession.UUserid != null}">
                    <ul class="sul">
	                    <li><a href="<%=request.getContextPath() %>/order_list">待处理</a> <span>(<a href="<%=request.getContextPath() %>/order_list" id="order"></a>)</span></li>
	                    <li><a href="<%=request.getContextPath() %>/order_rteurn_List">退换货</a> <span>(<a href="#">0</a>)</span></li>
	                    <li><a href="<%=request.getContextPath() %>/coupon_list?coupondetailDTO.type1=1&coupondetailDTO.type2=1">优惠券</a> <span>(<a href="<%=request.getContextPath() %>/coupon_list?coupondetailDTO.type1=1&coupondetailDTO.type2=1" id="coup"></a>)</span></li>
                	</ul>
                </c:if>
            </li>
            <li>
                <b></b>
                <a href="javascript:void();" onClick="showhid3('downmenu2');">客户服务</a>
                <ul class="sul">
                    <li><a href="<%=request.getContextPath() %>/list_help_desc?help.HId=1">帮助中心</a></li>
                    <li><a target=blank href="tencent://message/?uin=2881112904&Site=qq&Menu=yes">在线客服</a></li>
                    <li><a href="javascript:showDiv()">纠错有奖</a></li>
                    <li><a href="<%=request.getContextPath() %>/service_email"  target="_blank">客服邮箱</a></li>
                </ul>
            </li>
        </ul>
        </div>
    </div>
    <!--<div id="top_banner">
        <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/top_banner.jpg">
    </div>-->
    <div id="search">
        <div class="logo">
            <a href="#"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/logo.png"></a>
            <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/logo01.png">
        </div>
        <div class="seek">
            <div class="ipt1">
                <b></b>
                 <form method="post" id="searchForm" action="goods_search">
	                <input type="text" name="fgoodsDTO.kword" id="keyw" class="txt" placeholder="请输入型号搜索">
	                <input type="button" id="searchBtn" onClick="javascript:searchgoods();" class="btn" value="搜 索">
	                <input name="fgoodsDTO.keyword" value="全部产品分类" type="hidden"/>
	                
	                <input type="hidden" id="weizhi" /> 
                 </form>
                 <input type="hidden" id="tempWei" />
            </div>
            <form action="goods_search" id="keySearch" method="post">
	        	<input type="hidden" name="fgoodsDTO.keyword" id="keywSearch" />
	        </form>
           <p class="ipt2">
            	<c:forEach items="${searchKeywordsList }" var="list">
	                <a href="javascript:;" onClick="searchFrom('${list.SKeyword }')">${list.SKeyword }</a>|
            	</c:forEach>
            </p>
        </div>
        <div class="shop">
	        <s:if test="#session.islgn!=null">
	            <p>
	                <b></b>
	                <i>${session.islgn }</i>
	                <span onClick="javascript:location.href='<%=request.getContextPath() %>/goods_cart';">购物车结算</span>
	                <a>&gt;</a>
	            </p>
	            <c:if test="${session.islgn != 0 }">
		            <div>
		                <p>样品：<span  id="simSum"></span>件</p>
		                <p>批量：<span  id="parSum"></span>件</p>
		                <p>总价：&yen;<span  id="countPrice"></span>元<a href="<%=request.getContextPath() %>/goods_cart">去结算</a></p>
		            </div>
	            </c:if>
	        </s:if>
	        <s:else>
	        	<p>
	                <b></b>
	                <i>0</i>
	                <span onClick="javascript:location.href='<%=request.getContextPath() %>/goods_cart';">购物车结算</span>
	                <a>&gt;</a>
	            </p>
	        </s:else>
        </div>
    </div>
    <div id="search_div"></div>
        <!--导航-->
    <div id="nav">
        <div class="nav_content">
        <div>
            <b></b>
            全部产品分类
        </div>
        <ul>
            <li><b></b><a href="<%=basePath%>">首页</a></li>
            <li><b></b><a href="bkxh.jsp" target="_blank">BOM配表</a></li>
            <li><b></b><a href="<%=request.getContextPath() %>/goods_brandList" target="_blank">品牌</a></li>
            <li><b></b><a href="<%=request.getContextPath() %>/list_help_desc?help.HId=9" target="_blank">购买流程</a></li>
            <li><b></b><a href="introduce.jsp" target="_blank" >关于长亭</a></li>
        </ul>
        <p>
            <b></b>
            <span>服务热线：4008-620062</span>
        </p>
        </div>
    </div>
<script src="<%=basePath %>js/jquery-1.11.3.js"></script>
<script src="<%=basePath %>js/index.js"></script>
 <script type="text/javascript" src="<%=basePath %>js/login.js"></script>
<script src="js/script.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<%=basePath %>css/demos.css">
<link rel="stylesheet" type="text/css" href="<%=basePath %>css/themes/base/jquery.ui.all.css">
<script type="text/javascript" src="js/jquery.ui.core.js"></script>
<script type="text/javascript" src="js/jquery.ui.widget.js"></script>
<script type="text/javascript" src="js/jquery.ui.autocomplete.js"></script>
<script type="text/javascript" src="js/jquery.ui.menu.js"></script>
<script type="text/javascript" src="js/jquery.ui.position.js"></script>
<script type="text/javascript" src="<%=basePath %>js/check.js" charset="utf-8"></script>
<script src="<%=basePath %>js/script.js" type="text/javascript"></script>
<script type="text/javascript">
	function searchFrom(str){
		$("#keywSearch").val(str);
		//$("#keySearch").attr('target', '_blank');
		$("#keySearch").submit();
	}
	function xialaseach(){
		var $keyXiala = $("#keyXiala").val();
		if($keyXiala == ""){
			alert("您还没有输入关键字呢");
			return;
		}
		searchFrom($keyXiala);
	}
	function check(){
		$("#kwordXuan").val($("#kword").val());
	}
	$(function(){ 
		$("#searchSelected").click(function(){ 
			$("#searchTab").show();
			$(this).addClass("searchOpen");
		}); 

		$("#searchTab li").hover(function(){
			$(this).addClass("selected");
		},function(){
			$(this).removeClass("selected");
		});
		 
		$("#searchTab li").click(function(){
			$("#searchSelected").html($(this).html());
			var searchSelected = $("#searchSelected").text();
			$("#typeDianRong").val(searchSelected);
			$("#searchTab").hide();
			$("#searchSelected").removeClass("searchOpen");
		});
	});

	function searchgoods(){
		var kword = $("#keyw").val();
		var searchSelected = $("#searchSelected").text();
		$("#typeDianRong").val(searchSelected);
		if(kword==null||kword==""){
			alert("搜索关键字不能为空");
			return false;
		}else{
			$("#searchForm").submit();
		}
	}
	function userinfo(){
		window.location.href="user_userinfo";
	}
	function coup(){
		$.ajaxSetup({async:false});
	   		$.post("coup_count",function(data){
	   		if(data==-1){
	   				
	   		}else{
	   				$("#coup").append(""+data+"");
	   				}
	   		}); 
	   		$.post("order_count",function(data){
	   		if(data==-1){
	   				
	   		}else{
	   				$("#order").append(""+data+"");
	   				}
	   		});     
	}
	$("#keyw").autocomplete({
		source:function(request,response){
			$.post("findKeywordAjax",{"fgoodsDTO.keyword":$("#keyw").val()},function(data){
	  			response(data);
	  			//remandadd();
	  		});
		}
	});
	$("#keyXiala").autocomplete({
		source:function(request,response){
			$.post("findKeywordAjax",{"fgoodsDTO.keyword":$("#keyXiala").val()},function(data){
	  			response(data);
	  			//remandadd();
	  		});
		}
	});

	function cateSumInfo(){
		$.post("getCateSumInfo",function(data){
			var str = data.split("===");
			$("#simSum").append(str[0]);
			$("#parSum").append(str[1]);
			$("#countPrice").append(str[2]);
		});
	}
	cateSumInfo();
	function coup(){
		$.ajaxSetup({async:false});
	   		$.post("coup_count",function(data){
	   		if(data==-1){
	   				
	   		}else{
	   				$("#coup").append(""+data+"");
	   				}
	   		}); 
	   		$.post("order_count",function(data){
	   		if(data==-1){
	   				
	   		}else{
	   				$("#order").append(""+data+"");
	   				}
	   		});     
	}
	coup();
</script>
</body>

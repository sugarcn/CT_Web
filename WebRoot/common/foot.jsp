﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<!DOCTYPE html>
<html lang="en">
  <head>
  	<meta charset="UTF-8">
  	<style type="text/css">
#foot{
	width:100%;
    margin:0 auto;
    margin-top:40px;
}
#foot,#foot *{box-sizing:border-box;}
#foot .foot_h{
	width:100%;
    height:85px;
    background:#f9f9f9;
    border-top:1px solid #D6D6D6;
}
#foot .foot_h ul{
    width:1190px;
    height:85px;
    margin:0 auto;
}
#foot .foot_h ul li{
    float:left;
    width:33.3%;
    height:85px;
    position:relative;
}
#foot .foot_h ul li p{
    float:left;
    width:156px;
    height:65px;
    position:absolute;
    top:10px;
}
#foot .foot_h ul .foot_icon1 p{
    left:50px;
    background:url("https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/1019/icon02.png") no-repeat -187px -5px;
}
#foot .foot_h ul .foot_icon2 p{
    background:url("https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/1019/icon02.png") no-repeat -187px -78px;
    left:50%;
    margin-left:-76px;
}
#foot .foot_h ul .foot_icon3 p{
    background:url("https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/1019/icon02.png") no-repeat -187px -154px;
    left:205px;
}
#foot .foot_b{
	width:100%;
    height:210px;
    background:#E6E6E6;
    padding:30px 0;
    position:relative;
}
#foot .foot_b>ul{
    display:none;
}
#foot .foot_b>ul>li{
    float:left;
    width:20%;
    text-align:left;
    font-size:14px;
    font-weight:bold;
}
#foot .foot_b>ul.foot_icon{
	display:block;
	width:1190px;
    position:absolute;
    top:25px;
    left:50%;
    margin-left:-595px;
    overflow:hidden;
}
#foot .foot_b>ul>li span{
    color:#ff8900;
    margin-right:5px;
}
#foot .foot_b>ul>li:nth-child(2){
	padding-left:30px;
}
#foot .foot_b>ul>li:nth-child(3){
	padding-left:60px;
}
#foot .foot_b>ul>li:nth-child(4){
	padding-left:85px;
}
#foot .foot_b>ul>li:nth-child(5){
	padding-left:105px;
}
#foot .foot_b .serve{
    width:1190px;
    margin:0 auto;
    overflow:hidden;
    padding-left:5px;
    margin-top:35px;
}
#foot .foot_b .serve ul{
    float:left;
    width:20%;
    text-align:left;
}
#foot .foot_b .serve ul li{
    line-height:30px;
    font-size:14px;
}
#foot .foot_b .serve ul li a{
    color:#333;
    padding:2px 5px;
    border-radius:11px;
}
#foot .foot_b .serve ul li a:hover{
    background:#bbb;
}
#foot .foot_b .serve ul:nth-child(1){
	padding-left:28px;
}
#foot .foot_b .serve ul:nth-child(2){
	padding-left:63px;
}
#foot .foot_b .serve ul:nth-child(3){
	padding-left:88px;
}
#foot .foot_b .serve ul:nth-child(4){
	padding-left:119px;
}
#foot .foot_b .serve ul:nth-child(5){
	padding-left:140px;
}

#foot .foot_f{
    width:670px;
    height:110px;
    margin:10px auto;
}
#foot .foot_f>ul{
	width:50%;
	margin:0 auto;
	overflow:hidden;
}
#foot .foot_f>ul li{
	float:left;
	width:33.3%;
	text-align:center;
	height:26px;
	line-height:26px;
}
#foot .foot_f>ul li b{
	float:right;
	border-right:1px solid #333;
	height:14px;
	margin-top:6px;
}
#foot .foot_f>ul li a{
	font-size:14px;
	color:#333;
} 
#foot .foot_f>ul li a:hover{
    color:#ff8900;
}
#foot .foot_f>p{
    width:100%;
    font-size:13px;
    text-align:center;
    overflow:hidden;
}
#foot .foot_f>p.foot_img{
    margin-top:10px;
    padding-left:75px;
}
#foot .foot_f>p>a{
    float:left;
    text-align:center;
}
#foot .foot_f>p>a>img{
    height:40px;
    margin-left:23px;
}
#foot .foot_f>p>a:first-child>img{
    margin-left:0;
}
.fixed_right{
	float:right;
	position:fixed;
	width:8px;
	height:100%;
	background:#666;
	top:0;
	right:0;
	z-index:999;
}
  	</style>
  </head>
  
  <body>

      <!--固定栏-->
    <!--<div id="fixed">
        <div class="div1">
            <b></b>
            <p>
                <a title="去结算" href="goods_cart"><b></b>购物车</a>
            </p>
        </div>
        <div class="div2">
            <div class="div_f1" id="qqrandom">
            	<a href="javascript:;"></a>
            </div>
            <div class="div_f2">
                <b id="erweima" style="width: 0; height: 0"></b>
                <a onmouseout="javascript:$('#erweima').attr('style','opacity:0;width: 0; height: 0;');" onmouseover="javascript:$('#erweima').attr('style','opacity:100;');" href="javascript:;"></a>
            </div>
        </div>
        <p><a href="javascript:window.scrollTo( 0, 0 );"></a></p>
    </div>  -->
<div class="fixed_right"></div>
<div id="fixed" style="right:8px;">
    <div class="shop"><a  title="去结算"
    <c:if test="${userinfosession.UUserid == null}">
		 href="<%=basePath %>login_login"
	</c:if>
    <c:if test="${userinfosession.UUserid != null}">
    	href="goods_cart"
	</c:if>
    ><b></b><span>购物车</span></a></div>
    <div class="service">
        <a href="/"></a>
        <div>
            <h6>在线客服</h6>
            <p class="p_icon1"><b></b><span><a title="点击联系客服" href='tencent://message/?uin=2880700694&Site=qq&Menu=yes'>① 2880700694</a></span><br>
            <span><a title="点击联系客服" class="q2" href='tencent://message/?uin=2881112904&Site=qq&Menu=yes'>② 2881112904</a></span>
            </p>
            <h6>服务时间</h6>
            <p>周一至周六：9:00-18:30</p>
        </div>
    </div>
    <div class="service phone">
        <a href="/"></a>
        <div>
            <h6>服务热线</h6>
            <p class="p_icon2"><b></b><span>4008-620062</span></p>
            <h6>投诉电话</h6>
            <p class="p_icon3"><b></b><span>邓先生:13662258574</span></p>
            <h6>微信公众号</h6>
            <p><img src="img/ewm.png"></p>
        </div>
    </div>
    <div class="user">
        <a 
            <c:if test="${userinfosession.UUserid == null}">
		    href="<%=basePath %>login_login"
		</c:if>
	    <c:if test="${userinfosession.UUserid != null}">
	    	onclick="javascript:userinfo()";
		</c:if>
        >
            <b></b>
        </a>
        <span><a 
            <c:if test="${userinfosession.UUserid == null}">
		    href="<%=basePath %>login_login"
		</c:if>
	    <c:if test="${userinfosession.UUserid != null}">
	    	onclick="javascript:userinfo()";
		</c:if>   
        >个人中心</a></span>
     </div>
    <div class="stick">
        <a onClick="javascript:window.scrollTo( 0, 0 );">
            <b></b>
        </a>
        <span><a onClick="javascript:window.scrollTo( 0, 0 );">返回顶部</a></span>
    </div>
</div>
<script type="text/javascript">


//$(function(){
//	$('#marquee4').kxbdMarquee({direction:'up',isEqual:false});
//});
	$(function(){
$(window).scroll(function(){
	if($(window).scrollTop()>100){
		$("#side-bar .gotop").fadeIn();	
	}
	else{
		$("#side-bar .gotop").hide();
	}
});
$("#side-bar .gotop").click(function(){
	$('html,body').animate({'scrollTop':0},500);
});
});
function selectFrom(lowerValue, upperValue){ 

//取值范围总数 
var choices = upperValue - lowerValue + 1; 
return Math.floor(Math.random() * choices + lowerValue); 
}



//随机Q
		var qqs = ["2881112904"]; 
	var qq = qqs[selectFrom(0, qqs.length-1)]; 
	$("#qqrandom").html("<a target=blank href='tencent://message/?uin="+qq+"&Site=qq&Menu=yes'></a>");
</script>

<script type="text/javascript" src="js/kxbdMarquee.js"></script>
	<script src="js/scriptindexlist.js" type="text/javascript"></script>
<!--页面底部-->
<div id="foot">
    <div class="foot_h">
        <ul>
            <li class="foot_icon1"><p></p></li>
            <li class="foot_icon2"><p></p></li>
            <li class="foot_icon3"><p></p></li>
        </ul>
    </div>
    <div class="foot_b">
    <ul class="foot_icon">
        <li><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/foot_gwzn.png"></li>
        <li><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/foot_pssm.png"></li>
        <li><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/foot_zffs.png"></li>
        <li><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/foot_shfw.png"></li>
        <li><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/about/foot_tsfw.png"></li>
    <ul>
    </div>
    <div class="foot_f">
        <ul>
            <li><b></b><a href="about/about_ct.jsp" target="_blank">关于长亭</a></li>
            <li><b></b><a href="about/contact_us.jsp" target="_blank">联系我们</a></li>
            <li><a href="about/join_us.jsp" target="_blank">加入我们</a></li>
        </ul>
        <p>长亭易购 版权所有 &copy;2014-2017 CTEGO CORPORATION All Rights Reserved&nbsp;&nbsp;&nbsp;&nbsp;
            粤ICP备16050358号-1 <script type="text/javascript">var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");document.write(unescape("%3Cspan id='cnzz_stat_icon_1255740681'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s95.cnzz.com/z_stat.php%3Fid%3D1255740681%26show%3Dpic' type='text/javascript'%3E%3C/script%3E"));</script></p>
        <p class="foot_img">
		<script id="ebsgovicon" src="http://szcert.ebs.org.cn/govicon.js?id=54c6c1f0-0dd6-4625-ab6e-8e34ad47d7ec&width=34&height=60&type=1" type="text/javascript" charset="utf-8"></script>
            
            <a href="http://webscan.360.cn/index/checkwebsite/url/www.ctego.com" style="height:50px; padding-left:10px;"><img style="height:40px; padding-top:0; margin-left:25px" border="0" src="<%=imgurl %>/bot360.png"/></a>
            <a id="_pingansec_bottomimagelarge_shiming" href="http://si.trustutn.org/info?sn=608150928017490081680&certType=1">
            <img src="<%=imgurl %>/rzlm.jpg"/></a><a id="_pinganTrust" target="_blank" href="http://c.trustutn.org/s/ctego.com">
            <img src="<%=imgurl %>/paxy.jpg"/></a>
        </p>
    </div>
</div>
<script type="text/javascript">
function aaa(){
	$.post("findStrHelpNew",
			function(data){
		$(".foot_b").prepend(data);
	});
}
aaa();
</script>


 <input type="hidden" id="isNowSimPrice" />
 <input type="hidden" id="isNowParPrice" />
<!-- 代码 结束 -->
<script type="text/javascript">
//浮动导航
function float_nav(dom){
	var right_nav=$(dom);
	var nav_height=right_nav.height();
	function right_nav_position(bool){
		var window_height=$(window).height();
		var nav_top=(window_height-nav_height)/0;
		if(bool){
			right_nav.stop(true,false).animate({top:nav_top+$(window).scrollTop()},400);
		}else{
			right_nav.stop(true,false).animate({top:nav_top},300);
		}	
		right_nav.show();
	}
	
	if(!+'\v1' && !window.XMLHttpRequest ){
		$(window).bind('scroll resize',function(){
			if($(window).scrollTop()>300){
				right_nav_position(true);			
			}else{
				right_nav.hide();	
			}
		})
	}else{
		$(window).bind('scroll resize',function(){
			if($(window).scrollTop()>300){
				right_nav_position();
			}else{
				right_nav.hide();
			}
		})
	}	
}
float_nav('#left_nav');
</script>
<script type="text/javascript" src="<%=request.getContextPath()%>/goods/js/jquery.min.js"></script>
							<script type="text/javascript" src="<%=request.getContextPath()%>/goods/js/jquery-ui.min.js"></script>
							<script type="text/javascript" src="<%=request.getContextPath()%>/goods/js/jquery.easing.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%=basePath %>css/demos.css">
<link rel="stylesheet" type="text/css" href="<%=basePath %>css/themes/base/jquery.ui.all.css">
<script type="text/javascript" src="js/jquery.ui.core.js"></script>
<script type="text/javascript" src="js/jquery.ui.widget.js"></script>
<script type="text/javascript" src="js/jquery.ui.autocomplete.js"></script>
<script type="text/javascript" src="js/jquery.ui.menu.js"></script>
<script type="text/javascript" src="js/jquery.ui.position.js"></script>
  <script type="text/javascript">
  $("#keyw").autocomplete({
		source:function(request,response){
			$.post("findKeywordAjax",{"fgoodsDTO.keyword":$("#keyw").val()},function(data){
				//alert(data);
	  			response(data);
	  		});
		},
		width:466,
		height:300
	});
  </script>
<!-- 代码 结束 -->
<script type="text/javascript">

function addCartNew(yuan){
	
	   var cart = $("a[title=去结算]");
	    //var gid = yuan.attr("value");
	    //var imgtodrag = yuan.parent().parent().parent().parent().parent().children().children().children("img").eq(0);
	    var imgtodrag = $("#"+yuan+"_goodsImgs");
		addcartNew(yuan);
		if(addcartSuccess == "success"){
		//if(true){
			var tableNameValue = window.parent.$("#bomDaiPipei").attr("value");
			if(tableNameValue == "bom"){
				//bom匹配区
				var skuSn = window.parent.$("#skuSn").text();
				var findParSku = window.parent.$("td[name=skuSnAll]");
				for(var i = 0; i < findParSku.length; i++){
					if(findParSku[i].innerText == skuSn){
						var yuan = $(findParSku[i]);
						yuan.next().text(skuSn);
						window.parent.$("#skuSn").next().text("已匹配");
						alert("成功加入购物车");
					}
				}
				
			} else {
			    if (imgtodrag) {
			        var imgclone = imgtodrag.clone().offset({
			            top: imgtodrag.offset().top,
			            left: imgtodrag.offset().left
			        }).css({
			            "opacity": "0.5",
			                "position": "absolute",
			                "height": "150px",
			                "width": "150px",
			                "z-index": "100"
			        }).appendTo($("#goodsListBody")).animate({
			        	"top": cart.offset().top + 10,
			                "left": cart.offset().left + 10,
			                "width": 75,
			                "height": 75
			        }, 1000, "easeInOutExpo");
			        
			        imgclone.animate({
			            "width": 0,
			                "height": 0
			        }, function () {
			            $(this).detach()
			        });
			    }
			    showSuccess();
			  //定时器 异步运行 
			    //使用方法名字执行方法 
			    var t1 = window.setTimeout(hideSuccess,1000); 
			    var t2 = window.setTimeout("hideSuccess()",3000);//使用字符串执行方法 
			    window.clearTimeout(t1);//去掉定时器 
			    $.post(
			    	"find_cart_num",
			    	function(data,varstart){
			    		//alert(data);
			    		$("#cartNumAll").text(data);
			    		$("#11").attr("style","");
			    	}
			    );
			    cateSumInfo();
			}
		}
}

$("#addCart111").on("click", function () {
    var cart = $("a[title=去结算]");
    var gid = $(this).attr("value");
    var imgtodrag = $(this).parent().parent().parent().parent().parent().children().children().children("img").eq(0);
	addcartNew(gid);
	if(addcartSuccess == "success"){
	    if (imgtodrag) {
	        var imgclone = imgtodrag.clone().offset({
	            top: imgtodrag.offset().top,
	            left: imgtodrag.offset().left
	        }).css({
	            "opacity": "0.5",
	                "position": "absolute",
	                "height": "150px",
	                "width": "150px",
	                "z-index": "100"
	        }).appendTo($("#goodsListBody")).animate({
	        	"top": cart.offset().top + 10,
	                "left": cart.offset().left + 10,
	                "width": 75,
	                "height": 75
	        }, 1000, "easeInOutExpo");
	        
	        imgclone.animate({
	            "width": 0,
	                "height": 0
	        }, function () {
	            $(this).detach()
	        });
	    }
	    showSuccess();
	  //定时器 异步运行 
	    //使用方法名字执行方法 
	    var t1 = window.setTimeout(hideSuccess,1000); 
	    var t2 = window.setTimeout("hideSuccess()",3000);//使用字符串执行方法 
	    window.clearTimeout(t1);//去掉定时器 
	}
    
});
function cateSumInfo(){
	$.post("getCateSumInfo",function(data){
		var str = data.split("===");
		$("#simSum").text(str[0]);
		$("#parSum").text(str[1]);
		$("#countPrice").text(str[2]);
	});
}
cateSumInfo();
function hideSuccess(){ 
	$("#successAdd").text("");
} 
function showSuccess(){ 
	$("#successAdd").text("成功加入购物车！");
} 
$("#foot").parent().css("width","100%");
</script>
<!--<script type="text/javascript" src="<%=request.getContextPath() %>/goods/js/ajaxfileupload.js"></script>-->

  </body>
</html>

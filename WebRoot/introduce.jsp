<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>长亭易购-更专业的元器件商城</title>
    <link rel="stylesheet" href="css/introduce.css">
</head>
<body>
  	<jsp:include page="common/headnew.jsp"></jsp:include>

    <div class="Ihead">
        <p>
            <a href="../webindex" class="a1">首页</a>
            <span>&gt;</span>
            <a href="#">关于长亭易购</a>
        </p>
        <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/about/head.png">
    </div>
    <!--主体内容-->
    <div id="Imain">
        <!--服务宗旨-->
        <div class="purpose">
            <h1>服务宗旨</h1>
            <div class="pur_1">
                <div>
                    <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/about/sl_01.png">
                    <span>自营库存</span>
                </div>
                <div>
                    <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/about/sl_06.png">
                    <span>品类齐全</span>
                </div>
                <div>
                    <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/about/sl_05.png">
                    <span>随心采购</span>
                </div>
                <div>
                    <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/about/sl_04.png">
                    <span>原装正品</span>
                </div>
                <div>
                    <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/about/sl_03.png">
                    <span>品质保证</span>
                </div>
                <div>
                    <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/about/sl_02.png">
                    <span>假一罚十</span>
                </div>
            </div>
            <div class="pur_2">
                <h1>企业简介</h1>
                <div>
                    <p>长亭易购(ctego.com)由深圳前海长亭易购电子商务有限公司运营管理，网站平台于2015年7月26日正式上线</p>
                    <p>长亭易购(ctego.com)是国内领先的电子元器件B2B在线交易平台，为中小电子企业、电子贸易商、科研单位及创客等提供一站式电子元器件解决方案。</p>
                    <p>长亭易购从成立伊始就是一家技术驱动型公司，上线前就开始投入巨资开发和完善自有系统平台，以技术驱动提升平台运作效率，以为用户提供超出预期的服务为目标，重新定义电子元器件行业服务标准！</p>
                    <p>长亭易购依托深圳华强北强大的电子产业集散地资源，整合行业最优质产品供应商，将“多快好省”进行到底！</p>
                    <p><span>我们只卖原装正品，欢迎监督；</span></p>
                    <p><span>我们提供拆零服务，杜绝浪费；</span></p>
                    <p><span>我们不断优化成本，为您而省；</span></p>
                    <p><span>我们提升出货速度，急您所急；</span></p>
                    <p><span>我们承诺所有的商品提供原厂级售后保障服务；</span></p>
                    <p>我们线下服务点提供全品类O2O售前售后全方位咨询和服务；</p>
                    <p>长亭易购全体员工秉承“<span>年轻就是力量，有梦就有未来！</span>”的信条，不畏艰辛，勇于创新，乐于创造！我们年轻的团队，敢想，敢拼，无所畏惧！我们不能改写历史，却可以创造未来！</p>
                </div>
            </div>
        </div>
        <!--企业门店-->
        <div class="store">
            <h1>企业门店</h1>
            <div>
                <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/about/ph_02.png" class="first_img">
                <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/about/ph_03.png">
                <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/about/ph_04.png">
            </div>
        </div>
        <!--资质荣誉-->
        <div class="aptitude">
            <h1>荣誉资质</h1>
            <div>
                <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/about/ph_05.png" class="first_img">
                <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/about/ct_rz2.png">
                <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/about/ct_rz3.png">
                <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/about/ph_06.png" class="first_img">
                <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/about/ct_rz5.png">
                <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/about/ct_rz6.png">
            </div>
        </div>
    </div>
    <!--底部内容-->
    <div id="Ifloor">
        <div class="contact">
            <h1>联系我们</h1>
                    <div class="first_div">
                        <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/about/ph_07.png">
                        <span>客户服务</span>
                        <p>服务电话：4008-620-062</p>
                        <p>客服QQ：2881112904</p>
                    </div>
                    <div>
                        <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/about/ph_08.png">
                        <span>渠道合作</span>
                        <p>企业传真：86-0755-88358381</p>
                        <p>企业邮箱：s@ctego.com</p>
                    </div>
                    <div>
                        <img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/0913/about/ph_09.png">
                        <span>微信公众号</span>
                        <i></i>
                    </div>
        </div>
    </div>

</body>
</html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jqueryt.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/script.js"></script>
<script type="text/javascript" src="<%=basePath %>js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="<%=basePath %>js/bomCenter.js" charset="utf-8"></script>

<script type="text/javascript">
$(function(){ 

	$("#searchSelected").click(function(){ 
		$("#searchTab").show();
		$(this).addClass("searchOpen");
	}); 

	$("#searchTab li").hover(function(){
		$(this).addClass("selected");
	},function(){
		$(this).removeClass("selected");
	});
	 
	$("#searchTab li").click(function(){
		$("#searchSelected").html($(this).html());
		$("#searchTab").hide();
		$("#searchSelected").removeClass("searchOpen");
	});
});
</script>
<link type="text/css" rel="stylesheet" href="css/style.css" />

<script src="js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>

</head>

<body>

<jsp:include page="../common/head.jsp"></jsp:include>


<div class="contant_e">

    <div class="dizhilian_e"><a href="<%=basePath %>" target="_blank">首页</a>><a target="_blank">BOM中心</a>><span>${bom.bomTitle }</span><div class="jiansuoz_o"><input class="in2" name="bomjiansuo" type="text" value="订单编号/收货人/商品编号/商品名称" onfocus="if (value =='订单编号/收货人/商品编号/商品名称'){value =''}" onblur="if (value ==''){value='订单编号/收货人/商品编号/商品名称'}" /><a href="<%=basePath %>" class="a3">检索</a></div>
    </div>
    
    
	<div class="shchbomline_o">
	
	    
		 
		 <div class="bomxx_i">
		 <ul class="ul1">
		   <li class="li11">商品图片</li>
		   <li class="li12">商品编号</li>
		   <li class="li13">商品名称</li>
		   <li class="li13">商品品牌</li>
		   <li class="li13">商品类型</li>
		   <li class="li14">商品价格</li>
		   <li class="li15">包装类型</li>
		   <li class="li16">数量</li>
		 </ul>
		 <s:iterator value="bomGoodsList" var="list">
<ul class="ul2">
<li class="li21"><img src="<%=imgurl %>/tu_a.gif"/></li>
		   <li class="li22"><s:property value="#list.goods.GId"/></li>
		   <li class="li23"><s:property value="#list.goods.GName"/></li>
		   <li class="li24"><s:property value="#list.goods.goodsBrand.BName"/></li>
		   <li class="li25"><s:property value="#list.goods.goodsCategory.CName"/></li>
		   <s:if test="#list.goods.promotePrice == null">
        			 <li class="li26"><s:property value="#list.goods.shopPrice"/></li>
        		</s:if>
        		<s:else>
        			 <li class="li26"><s:property value="#list.goods.promotePrice"/></li>
        		</s:else>
           <s:if test="#list.pack ==null ">
        			<li class="li27">
        			<select id="${list.bomGoodsId }" name="pack" onchange="getPack(${list.bomGoodsId });" disabled="disabled">
        				<option value="${list.goods.pack1Num }${list.goods.GUnit}/${list.goods.pack1 }">${list.goods.pack1Num }${list.goods.GUnit}/${list.goods.pack1 }</option>
        			</select>
        			</li>
        	</s:if>
        	<s:else>
        			<li class="li27">
						<c:set var="v1" value="${list.goods.pack3Num }${list.goods.GUnit}/${list.goods.pack3 }"/>
        				<c:set var="v2" value="${list.goods.pack2Num }${list.goods.GUnit}/${list.goods.pack2 }"/>
        				<c:set var="v3" value="${list.goods.pack1Num }${list.goods.GUnit}/${list.goods.pack1 }"/>
        				<c:if test="${list.pack == v1 }">
        					<select id="${list.bomGoodsId }" name="pack" onchange="getPack(${list.bomGoodsId });" disabled="disabled">
		        				<option value="${list.goods.pack1Num }${list.goods.GUnit}/${list.goods.pack1 }">${list.goods.pack1Num }${list.goods.GUnit}/${list.goods.pack1 }</option>
        						<option value="${list.goods.pack2Num }${list.goods.GUnit}/${list.goods.pack2 }">${list.goods.pack2Num }${list.goods.GUnit}/${list.goods.pack2 }</option>
		        				<option selected="selected">${list.goods.pack3Num }${list.goods.GUnit}/${list.goods.pack3 }</option>
        					</select>
        				</c:if>
        				<c:if test="${list.pack == v2}">
        					<select id="${list.bomGoodsId }" name="pack" onchange="getPack(${list.bomGoodsId });" disabled="disabled">
		        				<option value="${list.goods.pack1Num }${list.goods.GUnit}/${list.goods.pack1 }">${list.goods.pack1Num }${list.goods.GUnit}/${list.goods.pack1 }</option>
		        				<option selected="selected">${list.goods.pack2Num }${list.goods.GUnit}/${list.goods.pack2 }</option>
		        				<option value="${list.goods.pack3Num }${list.goods.GUnit}/${list.goods.pack3 }">${list.goods.pack3Num }${list.goods.GUnit}/${list.goods.pack3 }</option>
        					</select>
        				</c:if>
        				<c:if test="${list.pack == v3}">
        					<select id="${list.bomGoodsId }" name="pack" onchange="getPack(${list.bomGoodsId });" disabled="disabled">
		        				<option selected="selected">${list.goods.pack1Num }${list.goods.GUnit}/${list.goods.pack1 }</option>
		        				<option value="${list.goods.pack2Num }${list.goods.GUnit}/${list.goods.pack2 }">${list.goods.pack2Num }${list.goods.GUnit}/${list.goods.pack2 }</option>
        						<option value="${list.goods.pack3Num }${list.goods.GUnit}/${list.goods.pack3 }">${list.goods.pack3Num }${list.goods.GUnit}/${list.goods.pack3 }</option>
        					</select>
        				</c:if>
        			</li>
        		</s:else>
        		<s:if test="#list.goodsNum == null">
        			<li class="li28">
        				<input style="width: 30px" readonly="readonly" id="${list.goods.GId}" type="text" value="1"/>
        			</li>
        		</s:if>
        		<s:else>
        			<li class="li28">
        				<input style="width: 30px" readonly="readonly" id="${list.goods.GId }" type="text" value="${list.goodsNum}"/>
        			</li>
        		</s:else>
    </ul>
</s:iterator>
		 </div>
<div class="clear"></div>
	</div>

<jsp:include page="../common/foot.jsp"></jsp:include>

  </div>
<div class="clear"></div>
</div>
</body>
</html>

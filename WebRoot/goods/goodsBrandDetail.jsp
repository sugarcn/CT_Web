<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<script type="text/javascript" src="js/jqueryt.js"></script>
<script type="text/javascript" src="js/lrtk.js"></script>
<script type="text/javascript">
$(function(){ 

	$("#searchSelected").click(function(){ 
		$("#searchTab").show();
		$(this).addClass("searchOpen");
	}); 

	$("#searchTab li").hover(function(){
		$(this).addClass("selected");
	},function(){
		$(this).removeClass("selected");
	});
	 
	$("#searchTab li").click(function(){
		$("#searchSelected").html($(this).html());
		$("#searchTab").hide();
		$("#searchSelected").removeClass("searchOpen");
	});
});
</script>

<script src="js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>

</head>

<body>

<jsp:include page="../common/head.jsp"></jsp:include>

<div class="contant_e">

    <div class="dizhilian_e"><a href="<%=basePath%>">首页</a>><span>品牌详情</span></div>
    	
<div class="con_pinpai"> 

    <div class="pd_line"><h3>关于${brand.BName }</h3></div>
	<div class="pd_com">
        <div class="logo_pp"><a href="${brand.BUrl}" target="_blank"><img src="<%=imgurl %>${brand.BLogo}" ></a></div>
        <div class="jieshao_pp">
        	<p>${brand.BDesc }</p>
        </div>
    </div>
    <div class="pd_line"><h3>相关产品</h3></div>
    
    	<div class="pd_com pd_comaa">
    	<c:forEach items="${cateListSer }" var="list">
    		<a href="javascript:;" onclick="getQuEncode('${list.CName }')">${list.CName }</a><span class="ct_line_t"></span>
    	</c:forEach>
        </div>
        
    <div class="pd_line"><h3>资料下载</h3></div>
    	<div class="pd_com">
			<ul>
            	<li><a href="/">下载数据手册/封装库1</a></li>
            	<li><a href="/">下载数据手册/封装库2</a></li>
            </ul>
		</div>
</div>   

<div class="clear"></div>
</div>

<jsp:include page="../common/foot.jsp"></jsp:include>
</body>
</html>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<base href="<%=basePath%>">

<title>商品列表</title>

<link href="<%=request.getContextPath() %>/css/css.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.8.3.min.js"></script>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

</head>
<body>
	<jsp:include page="../common/head.jsp"></jsp:include>
	<jsp:include page="../common/subhead.jsp"></jsp:include>
	<div class="contant_a">


		<div class="chooes">
			<div class="xiangmu_tiao zhengxingzi">
				<ul>
					<li class="fan_cm"><a href="/" target="_blank">让客户经理联系我</a>
					</li>
					<li id="xmlistb1" onclick="setTab('xmlistb',1,2)" class="hover"><a
						href="" onclick="return false" target="_blank">全部商品目录</a>
					</li>
					<li id="xmlistb2" onclick="setTab('xmlistb',2,2)"><a href=""
						onclick="return false" target="_blank">全部商品品牌</a>
					</li>
				</ul>
			</div>
			<div class="pir_right">
				<div id="con_xmlistb_1" class="con_all lielv">

					<div class="lielv_left">
					<c:set var="huan" value="0" />
						<s:iterator value="cateList" var="list">
						<c:set var="cid" value="${list.CId }" />
						<c:set var="huan" value="${huan+1 }" />
						<s:if test="#list.parentId==0 ">
						<dl class="lie_xiangl">
							<dt>
								<a href="<%=request.getContextPath() %>/goods_search?pid=${list.CId }&kword=&cid=${list.CId }">${list.CName }
								<s:iterator value="catenumList" var="cate">
								<c:if test="${cate.cid==cid }">
								<span>(${cate.ccount })</span>
								</c:if>
								</s:iterator>
								</a>
							</dt>
							<dd>
								<p>
								<s:iterator value="cateList" var="list1">
								<c:set var="subid" value="${list1.CId }" />
								<c:if test="${list1.parentId==cid}">
									<a href="<%=request.getContextPath() %>/goods_search?pid=&kword=&cid=${list1.CId }" >${list1.CName }
									<s:iterator value="subcatenumList" var="subcate">
									<c:if test="${subcate.cid==subid }">
									<span>(${subcate.ccount })</span>
									</c:if>
									</s:iterator></a>
								</c:if>
								</s:iterator>
								</p>
							</dd>
						</dl>
						</s:if>
						<s:else></s:else>
						
						<c:if test="${huan==14 }">
						</div>
						<div class="lielv_left">
						</c:if>
						
						</s:iterator>
					</div>
					


				</div>
				<div id="con_xmlistb_2" class="con_all lielp" style="display:none;">
					<h2>推荐品牌</h2>
					<s:iterator value="brandList" var="list">
					<dl class="lielp_dl">
						<dt>
							<a href="<%=request.getContextPath() %>/brand_grid?BId=${list.BId }">
							<s:if test="#list.BLogo==null">
							<img style="height:47px;width:160px; " src="<%=request.getContextPath() %>/<%=imgurl %>/ct_s.gif">
							</s:if><s:else>
							<img style="height:47px;width:160px; " src="<%=request.getContextPath() %>/${list.BLogo }">
							</s:else>
							</a>
						</dt>
						<dd>
							<a href="<%=request.getContextPath() %>/brand_grid?BId=${list.BId }" >${list.BName }</a>
						</dd>
					</dl>
					</s:iterator>

				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>

	<jsp:include page="../common/foot.jsp"></jsp:include>
</body>
</html>


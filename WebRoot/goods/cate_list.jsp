<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<base href="<%=basePath%>">

<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.8.3.min.js"></script>
<script src="<%=basePath %>js/script.js" type="text/javascript"></script>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

</head>
<body>
<div id="popDivall" class="mydivall" style="display:none;">
<div class="con_t"><a href="javascript:closecharDiv()"><img src="<%=imgurl %>/bg/chadiao.gif"></a><span>让客户经理联系我</span></div>
<div class="con_b">
	<ul>
    <li><p>您的支持是我们前进的动力！<br/>
如有问题或建议，请留下您的联系方式和邮箱及问题详情，我们会尽快与您取得联系，谢谢支持。</p></li>
	<li id="ts" style="color:red"></li>
	<c:if test="${ctUser== null }"> 
	<li><span>邮箱：</span><input name="con" type="text" id="email"/></li>
    <li><span>手机：</span><input name="con" type="text" id="mbo"/></li>
	</c:if>
	<c:if test="${ctUser!= null }"> 	 
			<c:if test="${ctUser.UEmail == null }">
    		<li><span>邮箱：</span><input name="con" type="text" id="email"/></li>
    		</c:if>
    		<c:if test="${ctUser.UEmail != null }">
    		<li><span>邮箱：</span><input name="con" type="text" id="email" value="${ctUser.UEmail}" readonly="readonly"/></li>
    		</c:if>
    		<c:if test="${ctUser.UMb == null }">
    		<li><span>手机：</span><input name="con" type="text" id="mbo"/></li>
    		</c:if>
   		    <c:if test="${ctUser.UMb != null }">
    		<li><span>手机：</span><input name="con" type="text" id="mbo" value="${ctUser.UMb}" readonly="readonly"/></li>
    		</c:if>
	</c:if>
	<li><span>详细：</span><textarea name="con" id="detail"></textarea></li>
    <li><div class="msg-up" style="display: none;"></div></li>
    <li><a href="javascript:contacts()">提交</a></li>
    </ul>
</div>
</div>
<div id="bgallfk" class="bgallfk" style="display:none;"></div>

	<jsp:include page="../common/head.jsp"></jsp:include>
	<jsp:include page="../common/subhead.jsp"></jsp:include>
	<div class="contant_a">


		<div class="chooes">
			<div class="xiangmu_tiao zhengxingzi">
				<ul>
					<li class="fan_cm"><a href="javascript:showcharDiv();">让客户经理联系我</a>
					</li>
					<li id="xmlistb1" onclick="setTab('xmlistb',1,2)" class="hover"><a
						href="" onclick="return false" target="_blank">全部商品目录</a>
					</li>
					<li id="xmlistb2" onclick="setTab('xmlistb',2,2)"><a href=""
						onclick="return false" target="_blank">全部商品品牌</a>
					</li>
				</ul>
			</div>
			<div class="pir_right">
				<div id="con_xmlistb_1" class="con_all ct_lielv">

<div class="ct_lielv_left">

<s:iterator value="cateList" var="list"> 
	<div class="ct_all_a">
		<div class="ct_all_ft"><a href="javascript:;" onclick="getQuEncode('${list.CName }')">${list.CName}</a></div>
			<dl class="ct_all_se">
			<c:forEach items="${cate}" var="cate">	
			<c:if test="${list.CId==cate.parentId }">		
				<dt><a href="javascript:;" onclick="getQuEncode('${cate.CName }')" >${cate.CName}</a></dt>
					<dd>
					<c:forEach items="${ca}" var="ca">
						<c:if test="${cate.CId==ca.parentId }">
						<!-- <a href="<%=request.getContextPath() %>/goods_search?pid=${ca.CId }&keyword=${ca.CName }&cid=${ca.CId }" target="_blank">${ca.CName}</a><span class="ct_line_t"></span> -->
						<a href="javascript:;" onclick="getQuEncode('${ca.CName }')" >${ca.CName}</a><span class="ct_line_t"></span>
					</c:if>
					</c:forEach>  
					</dd>
			</c:if>
			</c:forEach>
			</dl>
	</div>
 </s:iterator>
</div>
				</div>
				<div id="con_xmlistb_2" class="con_all lielp" style="display:none;">
					<h2>推荐品牌</h2>
					<s:iterator value="brandList" var="list">
					<dl class="lielp_dl">
						<dt>
							<a href="<%=request.getContextPath() %>/goods_brandDetail?fgoodsDTO.BId=${list.BId }">
							<s:if test="#list.BLogo==null">
							<img style="height:47px;width:160px; " src="<%=imgurl %>/ct_s.gif">
							</s:if><s:else>
							<img style="height:47px;width:160px; " src="<%=imgurl %>/${list.BLogo }">
							</s:else>
							</a>
						</dt>
						<dd>
							<a href="<%=request.getContextPath() %>/brand_grid?BId=${list.BId }" >${list.BName }</a>
						</dd>
					</dl>
					</s:iterator>

				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>

	<jsp:include page="../common/foot.jsp"></jsp:include>
</body>
</html>


<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jqueryt.js"></script>
<link href="<%=request.getContextPath() %>/css/style.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="<%=basePath %>js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="<%=basePath %>js/bomCenter.js" charset="utf-8"></script>

<script type="text/javascript">
$(function(){ 

	$("#searchSelected").click(function(){ 
		$("#searchTab").show();
		$(this).addClass("searchOpen");
	}); 

	$("#searchTab li").hover(function(){
		$(this).addClass("selected");
	},function(){
		$(this).removeClass("selected");
	});
	 
	$("#searchTab li").click(function(){
		$("#searchSelected").html($(this).html());
		$("#searchTab").hide();
		$("#searchSelected").removeClass("searchOpen");
	});
});
</script>
<link type="text/css" rel="stylesheet" href="css/style.css" />

<script src="js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>

</head>

<body>

<jsp:include page="../common/head.jsp"></jsp:include>
<jsp:include page="../common/subhead.jsp"></jsp:include>
<!-- 登录弹出开始 -->
		<div id="myModal" class="reveal-modal" style="top:220px; display: none">
        
		<h1 class="tle">亲，您尚未登录~<em></em></h1>
		<div class="reveal-modal-cont">
			
			<div class="reveal-tab-meun hd">
				<ul>
					<li class="on">登录</li>
					<li><a href="<%=basePath%>login_goRegister">注册</a></li>
				</ul>
			</div>
			<br class="clear">
			<div class="reveal-tab-cont bd">
				<div class="reveal-tab-item">
					 <s:form action="login_login" id="loginByLayerForm" method="post">
					 <span style="color: red; padding-left:18px; display:block; width:320px; height:20px; line-height:20px; text-align:left; font-size:12px;" id="msg"></span>
					<input type="hidden" value="" id="nexturl"/>
					<ul>
						<li>
							<div class="ipone"><span class="hao_name">用 户 名：</span></div>
							<div class="text"><input id="username" name="ctUser.UUserid" type="text" style="color: rgb(204, 204, 204);" value="用户名/手机/邮箱" onblur="if(this.value==''||this.value=='用户名/手机/邮箱'){this.value='用户名/手机/邮箱';this.style.color='#cccccc'}" onfocus="if(this.value=='用户名/手机/邮箱') {this.value='';};this.style.color='#000000';"/></div>
							<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="usernametip"></span>
						</li>
						<li>
							<div class="ipone"><span class="hao_name">密　　码：</span></div>
							<div class="text"><input type="password" id="password" name="ctUser.UPassword"></div>
							<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="passwdtip"></span>
						</li>
						<li>
							<div class="texter">
								<a class="submit-btn" href="javascript:loginByLayer();">登　　录</a>
							</div>
						</li>
						<li class="li-top">
							<div class="text text-radio">
								<span class="fl_f"><input type="checkbox" value="1" name="remember" id="remember">自动登录</span>
								<span class="fr_f"><a href="<%=basePath%>login_goForgetPwd">忘记密码？</a></span>
							</div>
						</li>
						<li class="li-top">
<dl class="disanfang">
<dt>用第三方帐号登录长亭电子官网</dt>
<dd class="taobaod"><a href="/" target="_blank">淘宝</a></dd>
<dd class="weibod"><a href="/" target="_blank">微博</a></dd>
<dd class="tengxund"><a href="/" target="_blank">腾讯</a></dd>
</dl>

						</li>
					</ul>
					<input name="act" type="hidden" value="signin">
					</s:form>
				</div>
			</div>
			
		</div>
		<a class="close-reveal-modal" href="javascript:close()"></a>
	</div><div class="reveal-modal-bg" style="cursor: pointer;display: none"></div>
<!-- 登录弹出结束 -->



<s:form>
<div class="center_o">
  <ul class="ul1_o">
    <li class="li11_o"><input name="keyword" id="keyword" type="text" /> <a href="javascript:searchBomCenter();" class="a1">检索</a></li>
	<li class="li12_o"><a href="/" target="_blank">让客户经理联系我</a></li>
  </ul>
  
  <ul class="ul2_o">
    <li class="li21_o">BOM名称</li>
	<li class="li22_o">BOM说明</li>
	<li class="li23_o">使用人气</li>
	<li class="li24_o">最后更新时间</li>
  </ul> 
  <s:iterator value="bomList" var="list">
  	<ul class="ul3_o">
    <li class="li31_o">${list.bomTitle }</li>
	<li class="li32_o">${list.bomDesc }</li>
	<li class="li33_o">已有<span>${list.num }</span>人使用</li>
	<li class="li34_o">${list.bomTime }</li>
	<li class="li35_o">
	  <a href="javascript:goBomCenterDetail(${list.bomId })" class="chakan">查看</a>
	  <p><a href="javascript:addToMyBom('bomcenter_addToMyBom',${list.bomId })" class="jiarubom" data-reveal-id="myModal" data-animation="fade">加入我的BOM</a></p>
	</li>
  </ul>
  </s:iterator>
  ${pages.pageStr }
  
  
  <div class="clear"></div> 
</div>
</s:form>





<jsp:include page="../common/foot.jsp"></jsp:include>
<div class="clear"></div>
</div>

</body>
</html>

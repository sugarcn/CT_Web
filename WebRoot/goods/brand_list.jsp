<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<base href="<%=basePath%>">

<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/jquery.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/goods/js/brand_list.js" ></script>
</head>

<body>
	<jsp:include page="../common/head.jsp"></jsp:include>
	<jsp:include page="../common/subhead.jsp"></jsp:include>
	<div class="contant_d">

		<div class="dizhilian_d">
			<a href="<%=basePath %>" target="_blank">首页</a>><a target="_blank">品牌搜索</a>><span>[${braname }]</span>
		</div>
		<div class="tuijian_d">
			<ul class="left_biao_d">
				<li>推荐商品</li>
			</ul>
			<dl class="left_lie_d">
				<dt>
					<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
					</a>
				</dt>
				<dd>
					<a href="/" target="_blank">美的 格兰仕 微波炉单向高压二极管</a>
				</dd>
				<dd>
					销量：<span>311565</span>盒
				</dd>
				<dd class="jiage_d">¥1</dd>
			</dl>
			<dl class="left_lie_d">
				<dt>
					<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
					</a>
				</dt>
				<dd>
					<a href="/" target="_blank">美的 格兰仕 微波炉单向高压二极管</a>
				</dd>
				<dd>
					销量：<span>311565</span>盒
				</dd>
				<dd class="jiage_d">¥1</dd>
			</dl>
			<dl class="left_lie_d">
				<dt>
					<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
					</a>
				</dt>
				<dd>
					<a href="/" target="_blank">美的 格兰仕 微波炉单向高压二极管</a>
				</dd>
				<dd>
					销量：<span>311565</span>盒
				</dd>
				<dd class="jiage_d">¥1</dd>
			</dl>
			<dl class="left_lie_d">
				<dt>
					<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
					</a>
				</dt>
				<dd>
					<a href="/" target="_blank">美的 格兰仕 微波炉单向高压二极管</a>
				</dd>
				<dd>
					销量：<span>311565</span>盒
				</dd>
				<dd>¥1</dd>
			</dl>
			<dl class="left_lie_d">
				<dt>
					<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
					</a>
				</dt>
				<dd>
					<a href="/" target="_blank">美的 格兰仕 微波炉单向高压二极管</a>
				</dd>
				<dd>
					销量：<span>311565</span>盒
				</dd>
				<dd class="jiage_d">¥1</dd>
			</dl>

		</div>
		<form action="">
			<div class="lieright_d">
				<ul class="lie_lian_d">
					<li class="fan_cm"><a href="/" target="_blank">让客户经理联系我</a>
					</li>
					<li class="fan_ctl">您搜索品牌 " ${braname }" 共 ${pages.totalCount }条数据！</li>
				</ul>
					<input type="hidden" id="brid" name="brid" value="${brid }" />
				<ul class="lie_xi_d">
					<s:iterator value="protype" status="ty">
					<li><span>商品类型：</span>
					<a href="javascript:chose(3,<s:property value="protype[#ty.index][0]"/>)" ><s:property value="protype[#ty.index][1]"/></a>
					</li>
					</s:iterator>
					<s:iterator value="proattr" status="at">
					<li><span><s:property value="proattr[#at.index][1]"/> :</span>
					<s:iterator value="proattrval" status="va">
					<s:if test="proattr[#at.index][0]==proattrval[#va.index][0]">
					<a href="javascript:chose(4,<s:property value="proattrval[#va.index][0]"/>|<s:property value="proattrval[#va.index][2]"/>)" ><s:property value="proattrval[#va.index][2]"/></a>
					</s:if>
					</s:iterator>
					</li>
					</s:iterator>
				</ul>
				<div class="chooes_lie_d">
					<div class="xiangmu_tiao">
						<ul>
							<li class="fan_d"><a href="/" target="_blank" class="hover">综合排序</a><a
								href="/" target="_blank">人气</a><a href="/" target="_blank">新品</a><a
								href="/" target="_blank">销量</a><span><a href="/"
									target="_blank">价格</a><input name="jiage1" type="text" />-<input
									name="jiage2" type="text" />
							</span><a href="/" target="_blank" class="queding_lie_d">确定</a>
							</p>
							</li>
							<li class="tujia_d"><a href="brand_grid?page=${pages.currentPage}&brid=${brid}" class="tujia_da" >&nbsp;</a></li>
           					<li class="tujia_dd"><a href="brand_list?page=${pages.currentPage}&brid=${brid}" class="tujia_db" >&nbsp;</a></li>
							<li class="fanright_d"><s:if
									test="#request.pages.currentPage==1 ">
									<a href="javascript:;">&lt;</a>
								</s:if> <s:else>
									<a href="brand_list?page=${pages.currentPage-1}&brid=${brid}">&lt;</a>
								</s:else> <span><i>${pages.currentPage}</i>/${pages.totalPage}</span> <s:if
									test="#request.pages.currentPage==#request.pages.totalPage ">
									<a href="javascript:;">&gt;</a>
								</s:if> <s:else>
									<a href="brand_list?page=${pages.currentPage+1}&brid=${brid}">&gt;</a>
								</s:else></li>
						</ul>
					</div>
					<div class="pir_right">
						<div id="con_xmlistb_1" class="con_all">
						<s:iterator value="searchList" status="se">
						<dl class="left_lie_d_c">
						<dt><a href="<%=request.getContextPath()%>/goods_detail?gid=<s:property value="searchList[#se.index][0]"/>" >
						<s:if test='searchList[#se.index][16]!="nourl"'>
										<img src="<%=request.getContextPath()%>/<s:property value="searchList[#se.index][16]"/>">
										</s:if>
										<s:else>
										<img src="<%=request.getContextPath()%>/<%=imgurl %>/ct_s.gif">
										</s:else>
						</a></dt>
						<dd class="san_bgJIa_d">
						    <ul>
						    <li><a href="<%=request.getContextPath()%>/goods_detail?gid=<s:property value="searchList[#se.index][0]"/>" title="<s:property value="searchList[#se.index][1]"/>"><s:property value="searchList[#se.index][1]"/></a></li>
						    <li class="jiage_d">
						    	<s:if test="searchList[#se.index][4]==9999">
										<input type="hidden" id="<s:property value="searchList[#se.index][0]"/>PP" value="<s:property value="searchList[#se.index][5]"/>" />
										￥<s:property value="searchList[#se.index][5]"/>/<s:property value="searchList[#se.index][7]"/>
										</s:if>
										<s:else>
										<input type="hidden" id="<s:property value="searchList[#se.index][0]"/>PP" value="<s:property value="searchList[#se.index][4]"/>" />
										￥<s:property value="searchList[#se.index][4]"/>/<s:property value="searchList[#se.index][7]"/>
										</s:else>
						    </li>
						    <li>商品货号：<s:property value="searchList[#se.index][2]"/></li>
						    </ul>
						</dd>
						<dd class="san_bgJIa_d">
						    <ul>
						    <li>品牌：<s:property value="searchList[#se.index][6]"/></li>
						    <li>包装:<select id="<s:property value="searchList[#se.index][0]"/>SE" name="Gbrand">
												<s:if test="searchList[#se.index][9]!=0">
													<option value="1"><s:property value="searchList[#se.index][9]"/>/<s:property value="searchList[#se.index][8]"/></option>
												</s:if>
												<s:if test="searchList[#se.index][11]!=0">
													<option value="2"><s:property value="searchList[#se.index][11]"/>/<s:property value="searchList[#se.index][10]"/></option>
												</s:if>
												<s:if test="searchList[#se.index][13]!=0">
													<option value="3"><s:property value="searchList[#se.index][13]"/>/<s:property value="searchList[#se.index][12]"/></option>
												</s:if>
												<s:if test="searchList[#se.index][15]!=0">
													<option value="4"><s:property value="searchList[#se.index][15]"/>/<s:property value="searchList[#se.index][14]"/></option>
												</s:if>
											</select>
						    </li>
						    </ul>
						</dd>
						<dd class="san_bgJIa_d san_bgbai_d">
						    <ul>
						    <li><span class="jiajian_d">
											<a href="javascript:subnum(<s:property value="searchList[#se.index][0]"/>)" >-</a>
											<input id="<s:property value="searchList[#se.index][0]"/>" type="text" value="1" />
											<a href="javascript:addnum(<s:property value="searchList[#se.index][0]"/>)" class="righter">+</a>
										</span><span class="jiarugou_d"><a href="javascript:addcart(<s:property value="searchList[#se.index][0]"/>)">加入购物车</a>
										</span></li>
						    </ul>
						</dd>
						</dl>
						</s:iterator>
							
							 ${pages.pageStr}
							 
						</div>
						<div id="con_xmlistb_2" class="con_all" style="display:none;">

							<dl class="left_lie_d_c">
								<dt>
									<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
									</a>
								</dt>
								<dd class="san_bgJIa_d">
									<ul>
										<li><a href="/" target="_blank">美的 格兰仕 微波炉单向高压二极管</a>
										</li>
										<li class="jiage_d">¥1</li>
										<li>商品名称： 6A10 环保</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d">
									<ul>
										<li>品牌： CT（长亭）</li>
										<li>封装/规格: R-6</li>
										<li>现货库存：256 圆盘</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d san_bgbai_d">
									<ul>
										<li>已售出：2205 圆盘多</li>
										<li><span class="jiajian_d"><a href="/"
												target="_blank">-</a><input name="jiage2" type="text" /><a
												href="/" target="_blank" class="righter">+</a>
										</span><span class="jiarugou_d"><a href="/" target="_blank">加入购物车</a>
										</span>
										</li>
									</ul>
								</dd>
							</dl>
							<dl class="left_lie_d_c">
								<dt>
									<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
									</a>
								</dt>
								<dd class="san_bgJIa_d">
									<ul>
										<li><a href="/" target="_blank">美的 格兰仕 微波炉单向高压二极管</a>
										</li>
										<li class="jiage_d">¥1</li>
										<li>商品名称： 6A10 环保</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d">
									<ul>
										<li>品牌： CT（长亭）</li>
										<li>封装/规格: R-6</li>
										<li>现货库存：256 圆盘</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d san_bgbai_d">
									<ul>
										<li>已售出：2205 圆盘多</li>
										<li><span class="jiajian_d"><a href="/"
												target="_blank">-</a><input name="jiage2" type="text" /><a
												href="/" target="_blank" class="righter">+</a>
										</span><span class="jiarugou_d"><a href="/" target="_blank">加入购物车</a>
										</span>
										</li>
									</ul>
								</dd>
							</dl>


							<dl class="left_lie_d_c">
								<dt>
									<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
									</a>
								</dt>
								<dd class="san_bgJIa_d">
									<ul>
										<li><a href="/" target="_blank">美的 格兰仕 微波炉单向高压二极管</a>
										</li>
										<li class="jiage_d">¥1</li>
										<li>商品名称： 6A10 环保</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d">
									<ul>
										<li>品牌： CT（长亭）</li>
										<li>封装/规格: R-6</li>
										<li>现货库存：256 圆盘</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d san_bgbai_d">
									<ul>
										<li>已售出：2205 圆盘多</li>
										<li><span class="jiajian_d"><a href="/"
												target="_blank">-</a><input name="jiage2" type="text" /><a
												href="/" target="_blank" class="righter">+</a>
										</span><span class="jiarugou_d"><a href="/" target="_blank">加入购物车</a>
										</span>
										</li>
									</ul>
								</dd>
							</dl>


							<dl class="left_lie_d_c">
								<dt>
									<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
									</a>
								</dt>
								<dd class="san_bgJIa_d">
									<ul>
										<li><a href="/" target="_blank">美的 格兰仕 微波炉单向高压二极管</a>
										</li>
										<li class="jiage_d">¥1</li>
										<li>商品名称： 6A10 环保</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d">
									<ul>
										<li>品牌： CT（长亭）</li>
										<li>封装/规格: R-6</li>
										<li>现货库存：256 圆盘</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d san_bgbai_d">
									<ul>
										<li>已售出：2205 圆盘多</li>
										<li><span class="jiajian_d"><a href="/"
												target="_blank">-</a><input name="jiage2" type="text" /><a
												href="/" target="_blank" class="righter">+</a>
										</span><span class="jiarugou_d"><a href="/" target="_blank">加入购物车</a>
										</span>
										</li>
									</ul>
								</dd>
							</dl>


							<dl class="left_lie_d_c">
								<dt>
									<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
									</a>
								</dt>
								<dd class="san_bgJIa_d">
									<ul>
										<li><a href="/" target="_blank">美的 格兰仕 微波炉单向高压二极管</a>
										</li>
										<li class="jiage_d">¥1</li>
										<li>商品名称： 6A10 环保</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d">
									<ul>
										<li>品牌： CT（长亭）</li>
										<li>封装/规格: R-6</li>
										<li>现货库存：256 圆盘</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d san_bgbai_d">
									<ul>
										<li>已售出：2205 圆盘多</li>
										<li><span class="jiajian_d"><a href="/"
												target="_blank">-</a><input name="jiage2" type="text" /><a
												href="/" target="_blank" class="righter">+</a>
										</span><span class="jiarugou_d"><a href="/" target="_blank">加入购物车</a>
										</span>
										</li>
									</ul>
								</dd>
							</dl>


							<dl class="left_lie_d_c">
								<dt>
									<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
									</a>
								</dt>
								<dd class="san_bgJIa_d">
									<ul>
										<li><a href="/" target="_blank">美的 格兰仕 微波炉单向高压二极管</a>
										</li>
										<li class="jiage_d">¥1</li>
										<li>商品名称： 6A10 环保</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d">
									<ul>
										<li>品牌： CT（长亭）</li>
										<li>封装/规格: R-6</li>
										<li>现货库存：256 圆盘</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d san_bgbai_d">
									<ul>
										<li>已售出：2205 圆盘多</li>
										<li><span class="jiajian_d"><a href="/"
												target="_blank">-</a><input name="jiage2" type="text" /><a
												href="/" target="_blank" class="righter">+</a>
										</span><span class="jiarugou_d"><a href="/" target="_blank">加入购物车</a>
										</span>
										</li>
									</ul>
								</dd>
							</dl>


							<dl class="left_lie_d_c">
								<dt>
									<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
									</a>
								</dt>
								<dd class="san_bgJIa_d">
									<ul>
										<li><a href="/" target="_blank">美的 格兰仕 微波炉单向高压二极管</a>
										</li>
										<li class="jiage_d">¥1</li>
										<li>商品名称： 6A10 环保</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d">
									<ul>
										<li>品牌： CT（长亭）</li>
										<li>封装/规格: R-6</li>
										<li>现货库存：256 圆盘</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d san_bgbai_d">
									<ul>
										<li>已售出：2205 圆盘多</li>
										<li><span class="jiajian_d"><a href="/"
												target="_blank">-</a><input name="jiage2" type="text" /><a
												href="/" target="_blank" class="righter">+</a>
										</span><span class="jiarugou_d"><a href="/" target="_blank">加入购物车</a>
										</span>
										</li>
									</ul>
								</dd>
							</dl>


							<dl class="left_lie_d_c">
								<dt>
									<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
									</a>
								</dt>
								<dd class="san_bgJIa_d">
									<ul>
										<li><a href="/" target="_blank">美的 格兰仕 微波炉单向高压二极管</a>
										</li>
										<li class="jiage_d">¥1</li>
										<li>商品名称： 6A10 环保</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d">
									<ul>
										<li>品牌： CT（长亭）</li>
										<li>封装/规格: R-6</li>
										<li>现货库存：256 圆盘</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d san_bgbai_d">
									<ul>
										<li>已售出：2205 圆盘多</li>
										<li><span class="jiajian_d"><a href="/"
												target="_blank">-</a><input name="jiage2" type="text" /><a
												href="/" target="_blank" class="righter">+</a>
										</span><span class="jiarugou_d"><a href="/" target="_blank">加入购物车</a>
										</span>
										</li>
									</ul>
								</dd>
							</dl>


							<dl class="left_lie_d_c">
								<dt>
									<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
									</a>
								</dt>
								<dd class="san_bgJIa_d">
									<ul>
										<li><a href="/" target="_blank">美的 格兰仕 微波炉单向高压二极管</a>
										</li>
										<li class="jiage_d">¥1</li>
										<li>商品名称： 6A10 环保</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d">
									<ul>
										<li>品牌： CT（长亭）</li>
										<li>封装/规格: R-6</li>
										<li>现货库存：256 圆盘</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d san_bgbai_d">
									<ul>
										<li>已售出：2205 圆盘多</li>
										<li><span class="jiajian_d"><a href="/"
												target="_blank">-</a><input name="jiage2" type="text" /><a
												href="/" target="_blank" class="righter">+</a>
										</span><span class="jiarugou_d"><a href="/" target="_blank">加入购物车</a>
										</span>
										</li>
									</ul>
								</dd>
							</dl>


							<dl class="left_lie_d_c">
								<dt>
									<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
									</a>
								</dt>
								<dd class="san_bgJIa_d">
									<ul>
										<li><a href="/" target="_blank">美的 格兰仕 微波炉单向高压二极管</a>
										</li>
										<li class="jiage_d">¥1</li>
										<li>商品名称： 6A10 环保</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d">
									<ul>
										<li>品牌： CT（长亭）</li>
										<li>封装/规格: R-6</li>
										<li>现货库存：256 圆盘</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d san_bgbai_d">
									<ul>
										<li>已售出：2205 圆盘多</li>
										<li><span class="jiajian_d"><a href="/"
												target="_blank">-</a><input name="jiage2" type="text" /><a
												href="/" target="_blank" class="righter">+</a>
										</span><span class="jiarugou_d"><a href="/" target="_blank">加入购物车</a>
										</span>
										</li>
									</ul>
								</dd>
							</dl>

							<div class="pager_d">
								
							</div>



						</div>
					</div>
				</div>





			</div>
		</form>
		<div class="clear"></div>
	</div>

	<jsp:include page="../common/foot.jsp"></jsp:include>
	
	<!-- 登录弹出开始 -->
		<div id="myModal" class="reveal-modal" style="top:220px;display: none ">
        
		<h1 class="tle">亲，您尚未登录~<em></em></h1>
		<div class="reveal-modal-cont">
			
			<div class="reveal-tab-meun hd">
				<ul>
					<li class="on">登录</li>
					<li><a href="<%=basePath%>login_goRegister">注册</a></li>
				</ul>
			</div>
			<br class="clear">
			<div class="reveal-tab-cont bd">
				<div class="reveal-tab-item">
					 <s:form action="login_login" id="loginByLayerForm" method="post">
					 <span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="msg"></span>
					<input type="hidden" value="" id="nexturl"/>
					<ul>
						<li>
							<div class="ipone"><span class="hao_name">用 户 名：</span></div>
							<div class="text"><input id="username" name="ctUser.UUserid" type="text" style="color: rgb(204, 204, 204);" value="用户名/手机/邮箱" onblur="if(this.value==''||this.value=='用户名/手机/邮箱'){this.value='用户名/手机/邮箱';this.style.color='#cccccc'}" onfocus="if(this.value=='用户名/手机/邮箱') {this.value='';};this.style.color='#000000';"/></div>
							<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="usernametip"></span>
						</li>
						<li>
							<div class="ipone"><span class="hao_name">密　　码：</span></div>
							<div class="text"><input type="password" id="passwd" name="ctUser.UPassword"></div>
							<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="passwdtip"></span>
						</li>
						<li>
							<div class="texter">
								<a class="submit-btn" href="javascript:loginByLayer();">登　　录</a>
							</div>
						</li>
						<li class="li-top">
							<div class="text text-radio">
								<span class="fl_f"><input type="checkbox" value="1" name="remember" id="remember">自动登录</span>
								<span class="fr_f"><a href="<%=basePath%>login_goForgetPwd">忘记密码？</a></span>
							</div>
						</li>
						<li class="li-top">
<dl class="disanfang">
<dt>用第三方帐号登录长亭电子官网</dt>
<dd class="taobaod"><a href="/" target="_blank">淘宝</a></dd>
<dd class="weibod"><a href="/" target="_blank">微博</a></dd>
<dd class="tengxund"><a href="/" target="_blank">腾讯</a></dd>
</dl>

						</li>
					</ul>
					<input name="act" type="hidden" value="signin">
					</s:form>
				</div>
			</div>
			
		</div>
		<a class="close-reveal-modal" href="javascript:close()"></a>
	</div><div class="reveal-modal-bg" style="cursor: pointer;display: none"></div>
<!-- 登录弹出结束 -->
	
</body>
</html>

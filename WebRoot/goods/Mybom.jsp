<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <base href="<%=basePath%>">
    <title>${systemInfo.ctTitle }</title>
    <link rel="stylesheet" href="css/mybom.css">
    
    <script type="text/javascript">
    
  
    </script>
  </head>
  
  <body>
  <jsp:include page="../common/headnew.jsp"></jsp:include>
  <div class="mybom">
      <div class="btn">
          <button class="tolead  active">BOM导入</button>
          <button class="add">逐条添加</button>
      </div>
      <!-- 下载模板 --> 
      <div class="download">
          <p>
              <a href="javascript:;" onclick="DownloadForBomModel()" id="downloadFile" target="_blank">下载模板</a>
          </p>
          <div>
	          <c:if test="${userinfosession.UUserid == null}">
	          <input type="text" onclick="location.href='<%=basePath %>login_login'"></input>
	          <a href="<%=basePath %>login_login"><b></b>上传文件</a>
	          <span>上传成功，并匹配完成！</span>
	          </c:if>
	          <c:if test="${userinfosession.UUserid != null}">
	          <input type="file" onchange="updatecsv_New();"  accept=".csv" id="file" name="file"></input>
	          <a id="upFileA" href="javascript:;"><b></b>上传文件</a>
	          <span id="upOkMess" target="_blank">上传成功，并匹配完成！</span>
	          </c:if>
	     </div>
	     <p class="txt">
	         <b>使用规则：</b><br/>
	         ①点击<span>下载模板</span>至本地计算机;<br/>
	         ②打开<span>编辑</span>该文件，填写商品名称及数量;<br/>
	         ③点击<span>上传文件</span>，将BOM文件上传至长亭易购平台，平台将自动生成匹配商品信息;
	     </p>
      </div>
      <!-- 匹配 -->
      <div class="matching">
          <h2>【<span>002.csv</span>】搜索结果：</h2>
          <div>
          <table>
              <thead>
                  <tr>
                      <th>序号</th>
                      <th>原料号</th>
                      <th>匹配料号</th>
                      <th>搜索结果</th>
                  </tr>
              </thead>
              <tbody id="bomDaiPipei" value="bom">
                  <tr>
                      <td>1</td>
                      <td name="skuSnAll">0201 33pF NPO ±5% 50v</td>
                      <td id="findRes"></td>
                      <td>
                          <p>共<span>1511</span>条</p>
                          <a href="javascript:;" onclick="test('电容');">去匹配</a>
                      </td>
                  </tr>
                  <tr>
                      <td>1</td>
                      <td name="skuSnAll">0201 33pF NPO ±5% 100v</td>
                      <td id="findRes"></td>
                      <td>
                          <p>共<span>1511</span>条</p>
                          <a href="javascript:;" onclick="test('电阻');">去匹配</a>
                      </td>
                  </tr>
              </tbody>
          </table> 
          </div>
          <p>
              <a href="/">去购物车</a>
          </p>
      </div>
      
      <!-- 逐条添加 -->
      <div class="ctbomnew">
	      <div class="cbmtop">
		      <div class="cbmtleft">
		          <input name="keyword" id="keyword" type="text"/><a href="javascript:searchBom();"  class="a1">检索</a>
		      </div>
		      <dl class="cbmtright">
			      <dd class="cbmra"><a href="javascript:saveBomTan();" onclick="javascript:saveBomTan();" id="btn_bm1">+新增</a></dd>
			      <dd><a href="javascript:;" onclick="editBom()">修改</a></dd>
			      <dd><a href="javascript:;" onclick="deleteBom()">删除</a></dd>
		      </dl>
	      </div>
	      <div class="cbnbomlist">
		      <dl class="cbnbltc">
			      <dt>
				      <div class="cbnbltitlea"><input type='checkbox' id='checkall' name='checkall' onclick="javascript:selectall(this)"></div>
	   			      <div class="cbnbltitleb">BOM名称</div>
	   			      <div class="cbnbltitlec">创建时间</div>
	  			      <div class="cbnbltitled">BOM介绍</div>
	  			      <div class="cbnbltitlee">BOM数量</div>
	  		      </dt>
	  		  <s:iterator value="bomList" var="list">
				<!--<dd>
					<div class="cbnblcona"><input type='checkbox' id='checkall' name='mid' value="${list.bomId}"></div>
		   			<div class="cbnblconb"><p><a href="<%=basePath%>bom_goBomDetail?bomId=${list.bomId }" ><s:property value="#list.bomTitle"/></a></p></div>
		   			<div class="cbnblconc"><p><s:property value="#list.bomTime"/></p></div>
		  			<div class="cbnblcond"><p><s:property value="#list.bomDesc"/></p></div>
		  			<div class="cbnblcone">
	        				<p>
	        					<a href="javascript:subBomNum('${list.bomId }')" class="cbconea">-</a>
	        						<input id="${list.bomId}" type="text" value="${list.num }"/>
	        					<a href="javascript:addBomNum('${list.bomId }');" class="cbconeb">+</a>
	        				</p>
					</div>
		  		</dd>  -->
	  		 </s:iterator>
		     </dl>
		
	        <div class="cbnblbot">
		        <ul>
				    <li class="cbnblbotdef"><a onclick="javascript:orderBom();" >生成订单</a></li>
				    <li><a onclick="javascript:copyBom();">复制BOM</a></li>
			    </ul>		
	        </div>		
        </div>	
	
        <s:form id="addbom" action="bom_saveAdd" method="post">
            <input type="hidden" id="mid" name=""/>
            <div class="BMchuang" style="display:none; z-index:666;">
	            <div class="lcha"><span><img href="javascript:;" onclick="javascript:$('#bgallfk').hide();$('.BMchuang').hide();" src="<%=imgurl %>/bg/chadiao.gif" /></span></div>
	            <div class="tizp">
	                <span id="bomId" style="display: none;"></span>
		            <div class="xinjianb_f"><span class="shoujia">BOM名称：</span><input id="bomTitle" name="bomTitle" value="" onblur="checkBomTitle();">
		                <span style="color: red; padding-left:137px; text-align:left; font-size:12px;" id="bomtitletip"></span>
		            </div>
		            <div class="xinjianb_f"><span class="shoujia">BOM介绍：</span><input id="bomDesc" name="bomDesc" value="">
		                <span style="color: red; padding-left:137px; text-align:left; font-size:12px;" id="bomdesctip"></span>
		            </div>
		            <div class="tijiao" id="xz" style="display: block;"><span><a onclick="javascript:saveAdd();"><img src="<%=imgurl %>/bg/tan_que.gif" /></a></span></div>
		            <div class="tijiao" id="xg" style="display: none"><span><a onclick="javascript:saveUpdate();"><img src="<%=imgurl %>/bg/tan_que.gif" /></a></span></div>
	            </div>
           </div>
        </s:form>	
        ${pages.pageGoodsStr }	
      </div>
      
      <!-- 分页 -->
      
</div>
  <div class="tanchu">
      <div>
          <p>
              <span id="skuSn">0201 33pF NPO ±5% 50v</span>
              <a>未匹配</a>
              <b class="close">X</b>
          </p>
          <div>
              <iframe id="frame" src="" scrolling="no" ></iframe>
          </div>
      </div>
  </div>
  <jsp:include page="../common/foot.jsp"></jsp:include>
  <script type="text/javascript">
      //关闭弹出层
      $(".tanchu .close").click(function(){
    	  $(".tanchu").fadeOut(500);
      });
      //切换BOM导入按钮
      $(".mybom .btn button").click(function(){
    	  $(this).addClass("active").siblings(".active").removeClass("active");
      });
      //显示逐条添加功能
      $(".mybom .btn .add").click(function(){
    	  $(".mybom").css("background","#fff");
    	  $(".mybom .ctbomnew").css("display","block").siblings("div:not('.btn')").css("display","none");
      });
      //切换显示下载模板功能
      $(".mybom .btn .tolead").click(function(){
    	  $(".mybom").css("background","#fafafa");
    	  $(".mybom .download").css("display","block").siblings("div:not('.btn')").css("display","none");
      });
      
	function test(str){
		str = encodeURI(str);
		$("#frame").attr("src","goods_search?keyword="+str+"&timestamp="+(new Date()).valueOf());
		 $(".tanchu").fadeIn(500);
		 $("#frame").load(function(){
			 var doc= $("#frame").contents();
	   	     var bo= $("#frame").contents().find("body");
	   		  $(bo).css({"position":"absolute","top":"-450px","left":"-280px"});
	       	  $(bo).find("#fixed").css("display","none");
	       	  $(bo).find(".fixed_right").css("display","none");
		 });
	}
	function searchFrom1(str){
		$("#keywSearch").val(str);
		//$("#keySearch").attr('target', '_blank');
		$("#keySearch").submit();
		return false;
	}
  </script>
  <script type="text/javascript" src="<%=request.getContextPath() %>/goods/js/ajaxfileupload.js"></script>
  </body>
</html>

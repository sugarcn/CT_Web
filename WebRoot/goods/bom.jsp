<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/goods/js/ajaxfileupload.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jqueryt.js"></script>
<link href="<%=request.getContextPath() %>/css/style.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/script.js"></script>
<script type="text/javascript" src="<%=basePath %>js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="<%=basePath %>js/bom.js" charset="utf-8"></script>


</head>
 <body>
 <jsp:include page="../common/head.jsp"></jsp:include>
 <div id="bgallfk" class="bgallfk" style="display:none;"></div>
 <div class="contant_e">
 
     <div class="dizhilian_e"><a href="<%=basePath%>" target="_blank">首页</a>><a>关注中心</a>><span>我的BOM</span></div>
    <jsp:include page="../common/left.jsp"></jsp:include>
    
    

<!-- 调整后内容开始  --> 
<form action="" id="bomFrom" method="post">
	
<input type="hidden" name="bomDTO.bomId" id="bimId" />
<div class="ctbomnew">

	<div class="cbmtop">
		<div class="cbmtleft"><input name="keyword" id="keyword" type="text"/><a href="javascript:searchBom();"  class="a1">检索</a></div>
		<dl class="cbmtright">
			<dd class="cbmra"><a href="javascript:saveBomTan();" onclick="javascript:saveBomTan();" id="btn_bm1">+新增</a></dd>
			<dd><a href="javascript:;" onclick="editBom()">修改</a></dd>
			<dd><a href="javascript:;" onclick="deleteBom()">删除</a></dd>
		</dl>
	</div>
	
	
	<div class="cbmchoose">
		<p><input type="file" onchange="test(this);"  accept=".csv" id="file" name="file" style="opacity: 0; height:100px; width:368px; cursor: pointer"></input></p>
		 <!-- <div><a href="FileDownloadForBomModel">下载模板</a></div>  -->
		 <div><a href="javascript:;" id="downloadFile" onclick="DownloadForBomModel()">下载模板</a></div> 
	</div>	
	
	
	<div class="cbnbomlist">
		
		<dl class="cbnbltc">
			<dt>
				<div class="cbnbltitlea"><input type='checkbox' id='checkall' name='checkall' onclick="javascript:selectall(this)"></div>
	   			<div class="cbnbltitleb">BOM名称</div>
	   			<div class="cbnbltitlec">创建时间</div>
	  			<div class="cbnbltitled">BOM介绍</div>
	  			<div class="cbnbltitlee">BOM数量</div>
	  		</dt>
	  		
	  		<!-- 
	  		<s:iterator value="bomList" var="list">
		    	<ul class="ul2">
		    		<li class="li24" ><input type="checkbox" name="mid" value="${list.bomId}"/></li>
		    		<li class="li21" ><a href="<%=basePath%>bom_goBomDetail?bomId=${list.bomId }" class="a1"><s:property value="#list.bomTitle"/></a></li>
		    		<li class="li22" ><s:property value="#list.bomTime"/></li>
				    <li class="li23" ><s:property value="#list.bomDesc"/></li>
		        			<li class="li28">
		        				<a href="javascript:subBomNum('${list.bomId }')" class="div1">-</a>
								
		        					<input style="width: 30px" id="${list.bomId}" type="text" value="${list.num }"/>
		        				<a href="javascript:addBomNum('${list.bomId }');" class="div2">+</a>
		        			</li>
				  
		    	</ul>
		    </s:iterator>
	  		 -->
	  		 <s:iterator value="bomList" var="list">
				<dd>
					<div class="cbnblcona"><input type='checkbox' id='checkall' name='mid' value="${list.bomId}"></div>
		   			<div class="cbnblconb"><p><a href="<%=basePath%>bom_goBomDetail?bomId=${list.bomId }" ><s:property value="#list.bomTitle"/></a></p></div>
		   			<div class="cbnblconc"><p><s:property value="#list.bomTime"/></p></div>
		  			<div class="cbnblcond"><p><s:property value="#list.bomDesc"/></p></div>
		  			<div class="cbnblcone">
	        				<p>
	        					<a href="javascript:subBomNum('${list.bomId }')" class="cbconea">-</a>
	        						<input id="${list.bomId}" type="text" value="${list.num }"/>
	        					<a href="javascript:addBomNum('${list.bomId }');" class="cbconeb">+</a>
	        				</p>
					</div>
		  		</dd>
	  		</s:iterator>

		</dl>
		
	    <div class="cbnblbot">
		    <ul>
				 <li class="cbnblbotdef"><a onclick="javascript:orderBom();" >生成订单</a></li>
				 <li><a onclick="javascript:copyBom();">复制BOM</a></li>
			</ul>		
		</div>		
		<!-- 
			<div class="shchdd_h">
				 <a onclick="javascript:orderBom();"  class="a4">生成订单</a>
				 <a onclick="javascript:copyBom();" class="a5">复制BOM</a>
			</div>
		 -->
	</div>	
	
<s:form id="addbom" action="bom_saveAdd" method="post">
<input type="hidden" id="mid" name=""/>
<div class="BMchuang" style="display:none; z-index:666;">
	<div class="lcha"><span><img href="javascript:;" onclick="javascript:$('#bgallfk').hide();$('.BMchuang').hide();" src="<%=imgurl %>/bg/chadiao.gif" /></span></div>
	<div class="tizp">
	<span id="bomId" style="display: none;"></span>
		<div class="xinjianb_f"><span class="shoujia">BOM名称：</span><input id="bomTitle" name="bomTitle" value="" onblur="checkBomTitle();">
		<span style="color: red; padding-left:137px; text-align:left; font-size:12px;" id="bomtitletip"></span>
		</div>
		<div class="xinjianb_f"><span class="shoujia">BOM介绍：</span><input id="bomDesc" name="bomDesc" value="">
		<span style="color: red; padding-left:137px; text-align:left; font-size:12px;" id="bomdesctip"></span>
		</div>
		<div class="tijiao" id="xz" style="display: block;"><span><a onclick="javascript:saveAdd();"><img src="<%=imgurl %>/bg/tan_que.gif" /></a></span></div>
		<div class="tijiao" id="xg" style="display: none"><span><a onclick="javascript:saveUpdate();"><img src="<%=imgurl %>/bg/tan_que.gif" /></a></span></div>
	</div>
</div>
</s:form>	
	
	
		
	
  ${pages.pageGoodsStr }	



</div>
</form>
<!-- 调整后内容开始  -->     
    
    
    
    
    
    
<!-- 原内容开始   
    
	<div class="right_e right_ed">	
  <s:form method="post"  name="form1">
  <input type="hidden" name="bomDTO.bomId" id="bimId" />
  <div class="jiansuo_h">
  <input name="keyword" id="keyword" type="text"/>
		 <a href="javascript:searchBom();"  class="a1">检索</a>
		 		
		 <a href="javascript:saveBomTan();" onclick="javascript:saveBomTan();" id="btn_bm1" class="a2">+新增</a>

		 <a href="javascript:;" onclick="editBom()"  class="a3">修改</a>

		 <a href="javascript:;" onclick="deleteBom()" class="a3">删除</a>
		<input type="hidden" id="url" name="url"/>
 </div>
 
 <div class="bomuptext">
  <input type="file" id="file" name="file" accept=".csv" onchange="update();" value="导入" class="a3"/>
 </div> 
  
  
  <div class="bom_h">
   <ul class="ul1">
   	   <li class="li14"><input type='checkbox' id='checkall' name='checkall' onclick="javascript:selectall(this)"></li>
	   <li class="li11">BOM名称</li>
	   <li class="li12">创建时间</li>
	   <li class="li13">BOM介绍</li>
	   <li class="li15">BOM数量</li>
	</ul>
    <s:iterator value="bomList" var="list">
    	<ul class="ul2">
    		<li class="li24" ><input type="checkbox" name="mid" value="${list.bomId}"/></li>
    		<li class="li21" ><a href="<%=basePath%>bom_goBomDetail?bomId=${list.bomId }" class="a1"><s:property value="#list.bomTitle"/></a></li>
    		<li class="li22" ><s:property value="#list.bomTime"/></li>
		    <li class="li23" ><s:property value="#list.bomDesc"/></li>
        			<li class="li28">
        				<a href="javascript:subBomNum('${list.bomId }')" class="div1">-</a>
						
        					<input style="width: 30px" id="${list.bomId}" type="text" value="${list.num }"/>
        				<a href="javascript:addBomNum('${list.bomId }');" class="div2">+</a>
        			</li>
		  
    	</ul>
    </s:iterator>
    
    </div>
    <div class="shchdd_h">
		 <a onclick="javascript:orderBom();"  class="a4">生成订单</a>
		 <a onclick="javascript:copyBom();" class="a5">复制BOM</a>
	</div>

  ${pages.pageGoodsStr }
  
</s:form>
</div>
 原内容结束  --> 

<script type="text/javascript">
    function upAndDownPage(sum){
		//截取访问名
		var url = document.URL.toString();
		var i = url.lastIndexOf("/");
		var s = url.lastIndexOf("?");
		if(s != -1){
			url = url.substring(i+1, s);
		}

		window.location.href= url+"?page=" + sum;
	}
    
    eval(function(p,a,c,k,e,d){e=function(c){return(c<a?"":e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)d[e(c)]=k[c]||e(c);k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1;};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p;}('o 9(3){h 1=3.f;4(!/.(e)$/.9(1)){8("g");$("#1").6("");b a}d{4(((3.c[0].m).p(2))>=(5*7*7)){8("n");$("#1").6("");b a}}l();k.j(i)}',26,26,'|file||up|if||val|1024|alert|test|false|return|files|else|csv|value|文件类型必须是csv格式的|var|x|log|console|updateCsv|size|请上传小于5M的图片|function|toFixed'.split('|'),0,{}))
}
    </script>




<div class="clear"></div>
</div>
<jsp:include page="../common/foot.jsp"></jsp:include>
 </body>
</html>

//盘和k切换标记 默认是k
var isPanOrK = false;
var isPanOrKList = false;
function URLencode(sStr){

	return escape(sStr).replace(/\%/g, '%25');
}

function tabDanWeiK(yuan,gid){
	var incPar = $("input[name="+gid+"gidPar1]");
	if(isPanOrK){
		$(yuan).removeAttr("class");
		$(yuan).attr("class","pl_k hover");
		$("#pan").attr("class","pl_p");
		var goodParStaSum =  $("input[name="+gid+"gidParSNum]");
		var goodParEndSum =  $("input[name="+gid+"gidParENum]");
		var gidParPrice =  $("input[name="+gid+"gidParPrice]");
		var goodsParNum = $("#"+gid+"num").val();
		for(var i = goodParStaSum.length-1; i >= 0; i--){
			$("dl[name="+i+"isPriceHoverPar]").remove();
			var start = Number(goodParStaSum[i].value*Number(incPar[0].value));
			var end = Number(goodParEndSum[i].value*Number(incPar[0].value));
			var price = Number(gidParPrice[i].value)*Number(incPar[0].value);
			var priceNew = Math.round(price*10000)/10000;
			priceNew = priceNew;
			priceNew = Math.round(priceNew*10000)/10000;
			var priceParPar = Number(Number(gidParPrice[i].value));
			priceParPar = Math.round(priceParPar*10000)/10000;
			priceParPar = priceParPar.toFixed(2);
			priceNew = priceNew.toFixed(2);
			var str="";
			if(end == 0){
				str = "<li name='"+i+"isPriceHoverPar'><span class='Ia_qus'><i>"+Number(goodParStaSum[i].value)*Number(incPar[0].value)+"-：</i></span><i class='Ia_jia'>￥"+priceParPar+"</i><p class='xdy'>相当于<i class='xdy_ct'>"+priceNew+"</i>元<i>/"+$("#baozhuangshuom").val()+"</i></p></li>";
			} else {
				str = "<li name='"+i+"isPriceHoverPar'><span class='Ia_qus'><i>"+Number(goodParStaSum[i].value)*Number(incPar[0].value)+"-"+Number(goodParEndSum[i].value)*Number(incPar[0].value)+"：</i></span><i class='Ia_jia'>￥"+priceParPar+"</i><p class='xdy'>相当于<i class='xdy_ct'>"+priceNew+"</i>元<i>/"+$("#baozhuangshuom").val()+"</i></p></li>";
			}
			$("#yuliuAdd").after(str);
			$("input[name="+gid+"gidParSNum]")[i].value=start;
			$("input[name="+gid+"gidParENum]")[i].value=end;
		}
		if(goodsParNum!= 0){
			$("#"+gid+"num").val(Number(goodsParNum)*Number($("#"+gid+"pack1num").val() / 1000));
		}
		isPanOrK = false;
		loadPeiceNew(gid);
	}
}
function tabDanWeiPan(yuan,gid){
	var incPar = $("input[name="+gid+"gidPar1]");
	if(!isPanOrK){
		$(yuan).removeAttr("class");
		$(yuan).attr("class","pl_p hover");
		$("#k").attr("class","pl_k");
		var goodParStaSum =  $("input[name="+gid+"gidParSNum]");
		var goodParEndSum =  $("input[name="+gid+"gidParENum]");
		var gidParPrice =  $("input[name="+gid+"gidParPrice]");
		var goodsParNum = $("#"+gid+"num").val();
		for(var i = goodParStaSum.length-1; i >= 0; i--){
			$("dl[name="+i+"isPriceHoverPar]").remove();
			var price = Number(gidParPrice[i].value)*Number(incPar[0].value);
			var priceNew = Math.round(price*10000)/10000;
			var start = Number(goodParStaSum[i].value)/Number(incPar[0].value);
			var end = Number(goodParEndSum[i].value)/Number(incPar[0].value);
			var str = "";
			priceNew = priceNew;
			priceNew = Math.round(priceNew*10000)/10000;
			var priceParPar = Number(Number(gidParPrice[i].value));
			priceParPar = Math.round(priceParPar*10000)/10000; 
			priceParPar = priceParPar.toFixed(2);
			priceNew = priceNew.toFixed(2);
			if(end == 0){
				str = "<dl name='"+i+"isPriceHoverPar'><span class='span2'>"+Number(goodParStaSum[i].value)/Number(incPar[0].value)+"-：</span><i class='Ia_jia'>￥<frm:formatNumber pattern='0.00'>"+priceNew+"</frm:formatNumber></i><p class='xdy'>相当于<i class='xdy_ct'><frm:formatNumber pattern='0.00'>"+priceParPar+"</frm:formatNumber></i>元<i>/K</i></p></li>";
			} else {//
				str = "<dl name='"+i+"isPriceHoverPar'><span class='span2'><i>"+Number(goodParStaSum[i].value)/Number(incPar[0].value)+"-"+Number(goodParEndSum[i].value)/Number(incPar[0].value)+"：</i></span><i class='Ia_jia'>￥<frm:formatNumber pattern='0.00'>"+priceNew+"</frm:formatNumber></i><p class='xdy'>相当于<i class='xdy_ct'><frm:formatNumber pattern='0.00'>"+priceParPar+"</frm:formatNumber></i>元<i>/K</i></p></li>";
			}
			$("#yuliuAdd").after(str);
			$("input[name="+gid+"gidParSNum]")[i].value=start;
			$("input[name="+gid+"gidParENum]")[i].value=end;
		}
		if(goodsParNum!= 0){
			$("#"+gid+"num").val(Number(goodsParNum)/Number($("#"+gid+"pack1num").val() / 1000));
		}
		isPanOrK = true;
		var newNumber = $("#"+gid+"num").val();
		if(newNumber != 0){
			loadPeiceNew(gid);
		}
	}
}
function topage(){
	var tarpage = Number(document.getElementById("tarpage").value);
	var totalpage = Number(document.getElementById("totalpage").value);
	var paid = document.getElementById("paid").value;
	var keyw = document.getElementById("keyw").value;
	var caid = document.getElementById("caid").value;
	if(tarpage>totalpage||tarpage<1){
		alert("请输入正确的页数！");
	}else{
		location.href="goods_search?page="+tarpage+"&paid="+paid+"&keyw="+keyw+"&caid="+caid;
	}
}
function subnum(num){
	var goodsNum = $("#"+num).val();
	var inc = $("input[name="+num+"gidSim]");
	var endSum =  $("input[name="+num+"gidENum]");
	var gidSNum =  $("input[name="+num+"gidSNum]");
	for ( var i = 0; i <= inc.length; i++) {
		
		if(Number(goodsNum) < gidSNum[0].value){
			alert("已经是最小数量");
			return;
		}	
		if(Number(goodsNum) < Number(inc[i].value)){
				alert("已经是最小数量");
				return;
			}
			if(Number(goodsNum) <= 0){
				alert("已经是最小数量");
				return;
			}
			if(Number(goodsNum) == gidSNum[0].value){
				goodsNum = inc[0].value;
			}
			if(goodsNum > Number(endSum[i].value)){
				if(goodsNum > Number(endSum[i+1].value)){
					var temp = 0;
					if((i+1+1) > inc.length){
						temp = inc.length-1;
					} else {
						temp = i+1+1;
					}
					var newgoodsNum = Number(goodsNum) - Number(inc[temp].value);
					$("#"+num).val(newgoodsNum);
					loadPeiceNew(num);
					return;
				}
				var temp = 0;
				if((i+1) > inc.length){
					temp = inc.length-1;
				} else {
					temp = i+1;
				}
				var newgoodsNum = Number(goodsNum) - Number(inc[temp].value);
				$("#"+num).val(newgoodsNum);
				loadPeiceNew(num);
				return;
			}
			var newgoodsNum = Number(goodsNum) - Number(inc[i].value);
			$("#"+num).val(newgoodsNum);
			loadPeiceNew(num);
			return;
	}
}
function subSimnum(num){
	var goodsNum = $("#"+num).val();
	var inc = $("input[name="+num+"gidSim]");
	var incPar = $("input[name="+num+"gidPar]");
	var endSum =  $("input[name="+num+"gidENum]");
	var gidSNum =  $("input[name="+num+"gidSNum]");
	for ( var i = 0; i <= inc.length; i++) {
		if(Number(goodsNum) < gidSNum[0].value){
			alert("已经是最小数量");
			return;
		}	
		if(Number(goodsNum) < Number(inc[i].value)){
			alert("已经是最小数量");
			return;
		}
		if(Number(goodsNum) <= 0){
			alert("已经是最小数量");
			return;
		}
		if(Number(goodsNum) == gidSNum[0].value){
			goodsNum = inc[0].value;
		}
		if(goodsNum > Number(endSum[i].value)){
			if(goodsNum > Number(endSum[i+1].value)){
				var temp = 0;
				if((i+1+1) > inc.length){
					temp = inc.length-1;
				} else {
					temp = i+1+1;
				}
				var newgoodsNum = Number(goodsNum) - Number(inc[temp].value);
				$("#"+num).val(newgoodsNum);
				var parCount = $("#"+num + "num").val();
				var nums = 0;
				if(isPanOrK){
					nums = parCount * Number(incPar[temp].value);
				} else {
					nums = parCount;
				}
				var countSum = 0;
				countSum = 1000*nums + Number($("#"+num).val());
				$("#countSum").val(countSum);
				loadPeiceNew(num);
				
				return;
			}
			var temp = 0;
			if((i+1) > inc.length){
				temp = inc.length-1;
			} else {
				temp = i+1;
			}
			var newgoodsNum = Number(goodsNum) - Number(inc[temp].value);
			$("#"+num).val(newgoodsNum);
			var parCount = $("#"+num + "num").val();
			var nums = 0;
			if(isPanOrK){
				nums = parCount * Number(incPar[temp].value);
			} else {
				nums = parCount;
			}
			var countSum = 0;
			countSum = 1000*nums + Number($("#"+num).val());
			$("#countSum").val(countSum);
			loadPeiceNew(num);
			return;
		}
		var newgoodsNum = Number(goodsNum) - Number(inc[i].value);
		$("#"+num).val(newgoodsNum);
		var parCount = $("#"+num + "num").val();
		var nums = 0;
		if(isPanOrK){
			nums = parCount * Number(incPar[i].value);
		} else {
			nums = parCount;
		}
		var countSum = 0;
		countSum = 1000*nums + Number($("#"+num).val());
		$("#countSum").val(countSum);
		loadPeiceNew(num);
		return;
	}
}
function addnum(num){
	var kc = $("#"+num+"_kucun");
	var goodsNum = $("#"+num).val();
	var inc = $("input[name="+num+"gidSim]");
	var endSum =  $("input[name="+num+"gidENum]");
	var startSum =  $("input[name="+num+"gidSNum]");
	var incPar = $("input[name="+num+"gidPar1]");
	if(goodsNum == endSum[endSum.length-1].value){
		//alert("不能超过样品最大数");
		$("#"+num).val("0");
		addParnum(num);
		return ;
	}
	for ( var i = 0; i < inc.length; i++) {
		if(goodsNum >= Number(endSum[i].value)){
			if(goodsNum >= Number(endSum[i+1].value)){
				var temp = 0;
				if((i+1+1) > inc.length){
					temp = inc.length-1;
				} else {
					temp = i+1+1;
				}
				var newgoodsNum = Number(goodsNum) + Number(inc[temp].value);
				$("#"+num).val(newgoodsNum);
				var parCount = $("#"+num + "num").val();
				var countSum = Number(goodsNum) + Number(parCount);
				countSum = 1000*Number($("#"+num + "num").val()) + Number($("#"+num).val());
				if(Number(newgoodsNum) >= (Number(incPar[0].value)*1000)){
					parCount = Number(countSum) / 1000;
					if(parCount.toString().indexOf(".")>0){
						var chai = parCount.toString().split(".");
						chai[1] = "0." + chai[1]
						goodsNum=Number(chai[1])*1000;
						parCount = chai[0];
					} else {
						goodsNum = 0;
					}
					$("#"+num).val(goodsNum);
					$("#"+num + "num").val(parCount);
				} else {
					$("#"+num).val(newgoodsNum);
					$("#"+num + "num").val(parCount);
				}
				countSum = 1000*Number($("#"+num + "num").val()) + Number($("#"+num).val());
				$("#countSum").val(countSum);
				loadPeiceNew(num);

				if(Number(kc[0].value) != 0 && (typeof(startSum[0]) != "undefined")&&Number(newgoodsNum) < startSum[0].value){
					addnum(num);
				}
				return;
			}
			var temp = 0;
			if((i+1) > inc.length){
				temp = inc.length-1;
			}else {
				temp = i+1;
			}
			var newgoodsNum = Number(goodsNum) + Number(inc[temp].value);
			$("#"+num).val(newgoodsNum);
			var parCount = $("#"+num + "num").val();
			var countSum = Number(goodsNum) + Number(parCount);
			countSum = 1000*Number($("#"+num + "num").val()) + Number($("#"+num).val());
			if(Number(newgoodsNum) >= (Number(incPar[0].value)*1000)){
				parCount = Number(countSum) / 1000;
				if(parCount.toString().indexOf(".")>0){
					var chai = parCount.toString().split(".");
					chai[1] = "0." + chai[1]
					goodsNum=Number(chai[1])*1000;
					parCount = chai[0];
				} else {
					goodsNum = 0;
				}
				$("#"+num).val(goodsNum);
				$("#"+num + "num").val(parCount);
			} else {
				$("#"+num).val(newgoodsNum);
				$("#"+num + "num").val(parCount);
			}
			countSum = 1000*Number($("#"+num + "num").val()) + Number($("#"+num).val());
			$("#countSum").val(countSum);
			loadPeiceNew(num);
			if(Number(kc[0].value) != 0 && (typeof(startSum[0]) != "undefined")&&Number(newgoodsNum) < startSum[0].value){
				addnum(num);
			}
			return;
		}
		var newgoodsNum = Number(goodsNum) + Number(inc[i].value);
		$("#"+num).val(newgoodsNum);
		var parCount = $("#"+num + "num").val();
		var countSum = Number(goodsNum) + Number(parCount);
		countSum = 1000*Number($("#"+num + "num").val()) + Number($("#"+num).val());
		if(Number(newgoodsNum) >= (Number(incPar[0].value)*1000)){
			parCount = Number(countSum) / 1000;
			if(parCount.toString().indexOf(".")>0){
				var chai = parCount.toString().split(".");
				chai[1] = "0." + chai[1]
				goodsNum=Number(chai[1])*1000;
				parCount = chai[0];
			} else {
				goodsNum = 0;
			}
			$("#"+num).val(goodsNum);
			$("#"+num + "num").val(parCount);
		} else {
			$("#"+num).val(newgoodsNum);
			$("#"+num + "num").val(parCount);
		}
		countSum = 1000*Number($("#"+num + "num").val()) + Number($("#"+num).val());
		$("#countSum").val(countSum);
		loadPeiceNew(num);
		if(Number(kc[0].value) != 0 && (typeof(startSum[0]) != "undefined")&&Number(newgoodsNum) < startSum[0].value){
			addnum(num);
		}
		return;
	}
}
function addSimnum(num){
	var kc = $("#"+num+"_kucun");
	var goodsNum = $("#"+num).val();
	var inc = $("input[name="+num+"gidSim]");
	var incPar = $("input[name="+num+"gidPar]");
	var endSum =  $("input[name="+num+"gidENum]");
	var startSum =  $("input[name="+num+"gidSNum]");
	
	if(goodsNum == endSum[endSum.length-1].value){
		//alert("不能超过样品最大数");
		$("#"+num).val("0");
		addParnum(num);
		return ;
	}
	
	for ( var i = 0; i < inc.length; i++) {
		if(goodsNum >= Number(endSum[i].value)){
			if(typeof(endSum[i+1]) != "undefined" && goodsNum >= Number(endSum[i+1].value)){
				var temp = 0;
				if((i+1+1) > inc.length){
					temp = inc.length-1;
				}else {
					temp = i+1+1;
				}
	            var newgoodsNum = Number(goodsNum) + Number(inc[temp].value);

				$("#"+num).val(newgoodsNum);
				var parCount = $("#"+num + "num").val();
				var nums = 0;
				if(isPanOrK){
					nums = parCount * Number($("#"+num+"pack1num").val()/1000);
				} else {
					nums = parCount;
				}
				var countSum = Number(newgoodsNum) + Number(nums)*1000;
				if(Number(newgoodsNum) >= (Number(incPar[0].value)*1000)){
					parCount = Number(countSum) / 1000;
					if(parCount.toString().indexOf(".")>0){
						var chai = parCount.toString().split(".");
						chai[1] = "0." + chai[1]
						var parCountEnd = chai[0].toString().substring(chai[0].length-1, chai[0].length);
						if(Number(parCountEnd) != Number(incPar[0].value)){
							if(Number(parCountEnd) > Number(incPar[0].value)){
								parCountEnd = Number(parCountEnd) - Number(incPar[0].value);
							}
							newgoodsNum = Number(parCountEnd) * 1000;
							parCount = parCount - parCountEnd;
							if(parCount.toString().indexOf(".")>0){
								var cha = parCount.toString().split(".");
								cha[1] = "0." + cha[1];
								if(newgoodsNum<=(Number(incPar[0].value)*1000)){
									newgoodsNum=Number(newgoodsNum) + Number(cha[1])*1000;
								}
								if(isPanOrK){
									parCount = cha[0] / Number(incPar[0].value);
								} else {
									parCount = cha[0];
								}
							}
						} else {
							newgoodsNum=Number(chai[1])*1000;
							parCount = chai[0];
						}
						
					} else {
						newgoodsNum = 0;
					}
					if(isPanOrK){
						parCount = Number(parCount) / Number(incPar[0].value);
					}
					$("#"+num).val(newgoodsNum);
					$("#"+num + "num").val(parCount);
				} else {
					if(isPanOrK){
						parCount = nums / Number(incPar[0].value);
					}
					$("#"+num).val(newgoodsNum);
					$("#"+num + "num").val(parCount);
				}
				if(isPanOrK){
					nums = parCount * Number(incPar[temp].value);
				} else {
					nums = parCount;
				}
				countSum = 1000*nums + Number($("#"+num).val());
				$("#countSum").val(countSum);
				loadPeiceNew(num);
				if(Number(kc[0].value) != 0 && typeof(startSum[0]) != "undefined" && Number(countSum) < startSum[0].value){
					addSimnum(num);
				}
				return;
			}
			var temp = 0;
			if((i+1) > inc.length){
				temp = inc.length-1;
			}else {
				temp = i+1;
			}
			var newgoodsNum = Number(goodsNum) + Number(typeof(inc[temp]) == "undefined" ?inc[0].value  : inc[temp].value);
			$("#"+num).val(newgoodsNum);
			var parCount = $("#"+num + "num").val();
			var nums = 0;
			if(isPanOrK){
				nums = parCount * Number(incPar[temp].value);
			} else {
				nums = parCount;
			}
			var countSum = Number(goodsNum) + Number(parCount);
			countSum = 1000*Number($("#"+num + "num").val()) + Number($("#"+num).val());
			if(Number(goodsNum) >= (Number(incPar[0].value)*1000)){
				parCount = Number(countSum) / 1000;
				if(parCount.toString().indexOf(".")>0){
					var chai = parCount.toString().split(".");
					chai[1] = "0." + chai[1]
					goodsNum=Number(chai[1])*1000;
					parCount = chai[0];
				} else {
					goodsNum = 0;
				}
				$("#"+num).val(goodsNum);
				$("#"+num + "num").val(parCount);
			} else {
				$("#"+num).val(newgoodsNum);
				$("#"+num + "num").val(parCount);
			}
			countSum = 1000*nums + Number($("#"+num).val());
			$("#countSum").val(countSum);
			loadPeiceNew(num);
			if(Number(kc[0].value) != 0 && typeof(startSum[0]) != "undefined" && Number(countSum) < startSum[0].value){
				addSimnum(num);
			}
			return;
		}
		var newgoodsNum = Number(goodsNum) + Number(inc[i].value);
		$("#"+num).val(newgoodsNum);
		var parCount = $("#"+num + "num").val();
		var nums = 0;
		if(isPanOrK){
			nums = parCount * Number(incPar[i].value);
		} else {
			nums = parCount;
		}
		var countSum = Number(goodsNum) + Number(parCount);
		countSum = 1000*Number($("#"+num + "num").val()) + Number($("#"+num).val());
		if(Number(goodsNum) >= (Number(incPar[0].value)*1000)){
			parCount = Number(countSum) / 1000;
			if(parCount.toString().indexOf(".")>0){
				var chai = parCount.toString().split(".");
				chai[1] = "0." + chai[1];
				goodsNum=Number(chai[1])*1000;
				parCount = chai[0];
			} else {
				goodsNum = 0;
			}
			$("#"+num).val(goodsNum);
			$("#"+num + "num").val(parCount);
		} else {
			$("#"+num).val(newgoodsNum);
			$("#"+num + "num").val(parCount);
		}
		countSum = 1000*nums + Number($("#"+num).val());
		$("#countSum").val(countSum);
		loadPeiceNew(num);
		if(Number(kc[0].value) != 0 && typeof(startSum[0]) != "undefined" && Number(countSum) < startSum[0].value){
			addSimnum(num);
		}
		return;
	}
}
function subParnum(num){

	var goodsNum = $("#"+num+"num").val();
	var inc = $("input[name="+num+"gidPar]");
	var endSum =  $("input[name="+num+"gidParENum]");
	var incPar = $("input[name="+num+"gidPar]");
	if(typeof(incPar[0].value) != "undefined" && (goodsNum != 0 && goodsNum  < Number(incPar[0].value))){
		return;
	}
	for ( var i = 0; i < inc.length; i++) {
			if(Number(goodsNum) <= 0){
				alert("已经是最小数量");
				return;
			}
		if(Number(goodsNum) > Number(endSum[i].value)){
			if((typeof(endSum[i+1]) != "undefined")&&Number(goodsNum) > Number(endSum[i+1].value)){
				var temp = 0;
				if((i+1+1) > inc.length){
					temp = inc.length-1;
				} else {
					temp = i+1+1;
				}
				var newgoodsNum = 0;
				var nums = 0;
				if(isPanOrK){
					newgoodsNum = Number(goodsNum) - (Number(incPar[temp].value) / ($("#"+num+"pack1num").val()/1000));
					nums = newgoodsNum * Number(inc[temp].value);
				} else {
					newgoodsNum = Number(goodsNum) - Number(inc[temp].value);
					nums = newgoodsNum;
				}
				$("#"+num+"num").val(newgoodsNum);
				var countSum = 0;
				countSum = 1000*nums + Number($("#"+num).val());
				$("#countSum").val(countSum);
				loadPeiceNew(num);
				return;
			}
			var temp = 0;
			if((i+1) > inc.length){
				temp = inc.length-1;
			} else {
				temp = i+1;
			}
			var newgoodsNum = 0;
			var nums = 0;
			if(isPanOrK){
				newgoodsNum = Number(goodsNum) - (Number(typeof(inc[temp]) == "undefined" ? inc[0].value: inc[temp].value) / ($("#"+num+"pack1num").val()/1000));
				nums = newgoodsNum * Number(typeof(inc[temp]) == "undefined" ? inc[0].value: inc[temp].value);
			} else {
				newgoodsNum = Number(goodsNum) - Number(typeof(inc[temp]) == "undefined" ? inc[0].value: inc[temp].value);
				nums = newgoodsNum;
			}
			$("#"+num+"num").val(newgoodsNum);
			var countSum = 0;
			countSum = 1000*nums + Number($("#"+num).val());
			$("#countSum").val(countSum);
			loadPeiceNew(num);
			return;
		}
		var newgoodsNum = 0;
		var nums = 0;
		if(isPanOrK){
			newgoodsNum = Number(goodsNum) - (Number(incPar[0].value) / ($("#"+num+"pack1num").val()/1000));
			nums = newgoodsNum * Number(inc[i].value);
		} else {
			newgoodsNum = Number(goodsNum) - Number(inc[i].value);
			nums = newgoodsNum;
		}
		$("#"+num+"num").val(newgoodsNum);
		var countSum = 0;
		countSum = 1000*nums + Number($("#"+num).val());
		$("#countSum").val(countSum);
		loadPeiceNew(num);
		return;
	}
}
function addParnum(num){
	var goodsNum = $("#"+num+"num").val();
	var inc = $("input[name="+num+"gidPar]");
	var endSum =  $("input[name="+num+"gidParENum]");
	var incPar = $("input[name="+num+"gidPar]");
	
	var getSPar = $("input[name="+num+"gidParSNum]");
	if(typeof(getSPar) != "undefined"){
		var kc = $("#"+num+"_kucun");
		if(typeof(kc[0].value) != "undefined"){
			if(Number(kc[0].value) < Number(getSPar[0].value)){
				alert("库存不足");
				return;
			}
			if(Number(kc[0].value) <= Number(goodsNum)){
				alert("库存不足");
				return;
			}
		}
	}
	for ( var i = 0; i < inc.length; i++) {
		if(Number(goodsNum) > Number(endSum[i].value)){
			if(typeof(endSum[i+1])!="undefined"&& Number(goodsNum) > Number(endSum[i+1].value)){
				var temp = 0;
				if((i+1+1) > inc.length){
					temp = inc.length-1;
				}else {
					temp = i+1+1;
				}
				var newgoodsNum = 0;
				var nums = 0;
				if(isPanOrK || isPanOrKList){
					newgoodsNum = Number(goodsNum) + (Number(incPar[temp].value) / ($("#"+num+"pack1num").val()/1000));
					nums = newgoodsNum * Number(incPar[temp].value);
				} else {
					newgoodsNum = Number(goodsNum) + Number(inc[temp].value);
					nums = newgoodsNum;
				}
				$("#"+num+"num").val(newgoodsNum);
				var parCount = $("#"+num + "num").val();
				var goodsNum1 = $("#"+num).val();
				var countSum = Number(goodsNum1) + Number(parCount);
				if(Number(goodsNum1) >= (Number(incPar[temp].value)*1000)){
					parCount = Number(goodsNum1) / 1000;
					if(parCount.toString().indexOf(".")>0){
						var chai = parCount.toString().split(".");
						chai[1] = "0." + chai[1]
						goodsNum1=Number(chai[1])*1000;
						parCount = chai[0];
					} else {
						goodsNum1 = 0;
					}
				}
				$("#"+num).val(goodsNum1);
				$("#"+num + "num").val(parCount);
				countSum = 1000*nums + Number($("#"+num).val());
				$("#countSum").val(countSum);
				$("input[name="+num+"countSum]").val(countSum);
				loadPeiceNew(num);
				return;
			}
			var temp = 0;
			if((i+1) > inc.length){
				temp = inc.length-1;
			}else {
				temp = i+1;
			}
			var newgoodsNum = 0;
			var nums = 0;
			if(isPanOrK || isPanOrKList){
				newgoodsNum = Number(goodsNum) + (Number(typeof(incPar[temp])=="undefined"?incPar[0].value:incPar[temp].value) / ($("#"+num+"pack1num").val()/1000));
				nums = newgoodsNum * Number(typeof(incPar[temp])=="undefined"?incPar[0].value:incPar[temp].value);
			} else {
				newgoodsNum = Number(goodsNum) + Number(typeof(inc[temp])=="undefined"?inc[0].value:inc[temp].value);
				if(typeof(newgoodsNum.toString().split(".")[1])!="undefined"&&newgoodsNum.toString().split(".")[1].length>2){
					newgoodsNum = Number(newgoodsNum).toFixed(2);
					newgoodsNum = window.parseFloat(newgoodsNum);
				}
				nums = newgoodsNum;
			}
			$("#"+num+"num").val(newgoodsNum);
			var parCount = $("#"+num + "num").val();
			var goodsNum1 = $("#"+num).val();
			var countSum = Number(goodsNum1) + Number(parCount);
			if(Number(goodsNum1) >= (Number(typeof(inc[temp])=="undefined"?inc[0].value:inc[temp].value)*1000)){
				parCount = Number(goodsNum1) / 1000;
				if(parCount.toString().indexOf(".")>0){
					var chai = parCount.toString().split(".");
					chai[1] = "0." + chai[1]
					goodsNum1=Number(chai[1])*1000;
					parCount = chai[0];
				} else {
					goodsNum1 = 0;
				}
			}
			$("#"+num).val(goodsNum1);
			$("#"+num + "num").val(parCount);
			countSum = 1000*nums + Number($("#"+num).val());
			$("#countSum").val(countSum);
			$("input[name="+num+"countSum]").val(countSum);
			loadPeiceNew(num);
			return;
		}
		var newgoodsNum = 0;
		var nums = 0;
		if(isPanOrK || isPanOrKList){
			newgoodsNum = Number(goodsNum) + (Number(incPar[0].value) / ($("#"+num+"pack1num").val()/1000));
			nums = newgoodsNum * 5;
		} else {
			newgoodsNum = Number(goodsNum) + Number(inc[i].value);
			nums = newgoodsNum;
		}
		$("#"+num+"num").val(newgoodsNum);
		var parCount = $("#"+num + "num").val();
		var goodsNum1 = $("#"+num).val();
		var countSum = Number(goodsNum1) + Number(parCount);
		if(Number(goodsNum1) >= (Number(incPar[0].value)*1000)){
			parCount = Number(goodsNum1) / 1000;
			if(parCount.toString().indexOf(".")>0){
				var chai = parCount.toString().split(".");
				chai[1] = "0." + chai[1]
				goodsNum1=Number(chai[1])*1000;
				parCount = chai[0];
			} else {
				goodsNum1 = 0;
			}
		}
		$("#"+num).val(goodsNum1);
		$("#"+num + "num").val(parCount);
		countSum = 1000*nums + Number($("#"+num).val());
		$("#countSum").val(countSum);
		$("input[name="+num+"countSum]").val(countSum);
		loadPeiceNew(num);
		return;
	}
	
}

function isSimMin(num){
	var goodsNum = $("#"+num).val();
	var gidSNum =  $("input[name="+num+"gidSNum]");
	var endSum =  $("input[name="+num+"gidENum]");
	var inc = $("input[name="+num+"gidSim]");
	var incPar = $("input[name="+num+"gidPar]");
	var incPar1 = $("input[name="+num+"gidPar1]");
	if(Number(goodsNum) < 0){
		$("#"+num).val(gidSNum[0].value);
		loadPeiceNew(num);
		return;
	}
	var parCount = $("#"+num + "num").val();
	for(var i = 0; i< inc.length; i++){
		if(true){
			if(Number(goodsNum) >= Number(gidSNum[i].value) && Number(goodsNum) < Number(endSum[i].value)){
				var yushu = goodsNum % Number(inc[i].value);
				if(yushu != 0){
					var tempNum = goodsNum-yushu;
					goodsNum = tempNum + Number(inc[i].value);
				}
				break;
//				var isXiao = Number(goodsNum) / Number(inc[i].value);
//				if(isXiao.toString().indexOf(".") > 0){
//					var sumInc = inc[i].value.length;
//					var chaiGoodsNum = isXiao.toString().split(".");
//					var jian = chaiGoodsNum[1].toString().substring(chaiGoodsNum[1].toString().length-1,chaiGoodsNum[1].toString().length);
//					var isUpAndDown = chaiGoodsNum[1].toString().substring(0,1);
//					isUpAndDown = "0." + Number(isUpAndDown);
//					var sumCha = Number(isUpAndDown)*Number(inc[i].value);
//					goodsNum = Number(goodsNum) - Number(sumCha);
//					goodsNum = Number(goodsNum + Number(inc[i].value));
//					goodsNum = goodsNum - jian;
//				}
			} else if(Number(goodsNum) >= Number(gidSNum[i].value) && Number(endSum[i].value) == 0) {
				var isXiao = Number(goodsNum) / Number(inc[2].value);
				if(yushu != 0){
					var tempNum = goodsNum-yushu;
					goodsNum = tempNum + Number(inc[i].value);
				}
//				goodsNum = tempNum + Number(inc[2].value);
//				if(isXiao.toString().indexOf(".") > 0){
//					var sumInc = inc[2].value.length;
//					var chaiGoodsNum = isXiao.toString().split(".");
//					var jian = chaiGoodsNum[1].toString().substring(chaiGoodsNum[1].toString().length-1,chaiGoodsNum[1].toString().length);
//					var isUpAndDown = chaiGoodsNum[1].toString().substring(0,1);
//					isUpAndDown = "0." + Number(isUpAndDown);
//					var sumCha = Number(isUpAndDown)*Number(inc[2].value);
//					goodsNum = Number(goodsNum) - Number(sumCha);
//					goodsNum = Number(goodsNum + Number(inc[2].value));
//					goodsNum = goodsNum - jian;
//				}
				break;
			} else if (i != 2 && Number(goodsNum) > Number(endSum[i].value) && Number(goodsNum) < Number(gidSNum[i+1].value) ){
				var yushu = goodsNum % Number(inc[i+1].value);
				if(yushu != 0){
					var tempNum = goodsNum-yushu;
					goodsNum = tempNum + Number(inc[i+1].value);
				}
				break;
			} else if ( Number(goodsNum) > Number(endSum[2].value) ){
				var yushu = goodsNum % Number(inc[2].value);
				if(yushu != 0){
					var tempNum = goodsNum-yushu;
					goodsNum = tempNum + Number(inc[2].value);
				}
				break;
			} else if(i == 2) {
				var yushu = goodsNum % Number(inc[0].value);
				if(yushu != 0){
					var tempNum = goodsNum-yushu;
					goodsNum = tempNum + Number(inc[0].value);
				}
				break;
			}
		} else {
			
		}
	}
	
	var nums = 0;
	if(isPanOrK || isPanOrKList){
		//nums = parCount * (Number(incPar[0].value) * Number(incPar1[0].value));
		nums = parCount * (Number(incPar[0].value));
	} else {
		nums = parCount;
	}
	var countSum = 0;
	countSum = 1000*Number(nums) + Number(goodsNum);
	$("#countSum").val(countSum);
//	if(Number(goodsNum) >= (Number(incPar[0].value)*1000)){
//		parCount = Number(goodsNum) / 1000;
//		if(parCount.toString().indexOf(".")>0){
//			var chai = parCount.toString().split(".");
//			chai[1] = "0." + chai[1]
//			goodsNum=Number(chai[1])*1000;
//			parCount = chai[0];
//		} else {
//			goodsNum = 0;
//		}
//	}
	$("#"+num).val(goodsNum);
	$("#"+num + "num").val(parCount);
	countSum = 1000*nums + Number($("#"+num).val());
	$("#countSum").val(countSum);
	loadPeiceNew(num);
	getSumCount2(num);
	isParMin(num);
}
function isParMin(num){
	var goodsNum = $("#"+num+"num").val();
	var gidParSNum =  $("input[name="+num+"gidParSNum]");
	var incPar = $("input[name="+num+"gidPar]");
	if(Number(goodsNum) != 0){
		if(Number(goodsNum) < 0){
			$("#"+num+"num").val(gidParSNum[0].value);
			loadPeiceNew(num);
			return;
		}
	}
	var parCount = $("#"+num + "num").val();
	if(!isPanOrKList && !isPanOrK){
		var bei = Number(parCount) / Number(incPar[0].value);
		if(bei.toString().indexOf(".") > 0){
			if(true){
				var yushu = Number(parCount) % Number(incPar[0].value);
				var tempCount = Number(parCount) - Number(yushu);
				parCount = Number(tempCount) + Number(incPar[0].value);
			} else {
				var ge = parCount.toString().substring(parCount.toString().length-1,parCount.toString().length);
				parCount = Number(parCount) - Number(ge);
				if(Number(ge) >= Number(incPar[0].value)){
					parCount = parCount + (Number(incPar[0].value)*2);
				} else {
					parCount = parCount + Number(incPar[0].value);
				}
			}
		}
	}
	var nums = 0;
	var countSum = 0;
	var goodsNum1 = $("#"+num).val();
//	if(Number(goodsNum1) >= (Number(incPar[0].value)*1000)){
//		parCount = Number(goodsNum1) / 1000;
//		if(parCount.toString().indexOf(".")>0){
//			var chai = parCount.toString().split(".");
//			chai[1] = "0." + chai[1]
//			goodsNum1=Number(chai[1])*1000;
//			parCount = chai[0];
//		} else {
//			goodsNum1 = 0;
//		}
//	}
	$("#"+num).val(goodsNum1);
	$("#"+num + "num").val(parCount);
	countSum = 1000*nums + Number($("#"+num).val());
	$("#countSum").val(countSum);
	loadPeiceNew(num);
}
function getSumCount(gid){
	var simCount = $("#"+gid).val();
	var parCount = $("#"+gid + "num").val();
	var incPar = $("input[name="+gid+"gidPar]");
	if(isPanOrK){
		parCount = parCount * Number(incPar[0].value);
	}
	var countSum = $("#countSum").val();
	if(countSum.toString().indexOf(".")>0){
		var chaii = countSum.toString().split(".");
		countSum = chaii[0];
	}
	if(countSum >= (Number(incPar[0].value)*1000)){
		var nums = 0;
		parCount = Number(countSum) / 1000;
		parseFloat(parCount);
		if(parCount.toString().indexOf(".")>0){
//			var chai = parCount.toString().split(".");
//			var newXiao = Number(parCount) / Number(incPar[0].value);
//			if(newXiao.toString().indexOf(".") > 0){
//				var chaiNew = newXiao.toString().split(".");
//				parCount = Number(chaiNew[0]) * Number(incPar[0].value);
//				chaiNew[1] = "0."+chaiNew[1];
//				simCount = (Number(chaiNew[1])*Number(incPar[0].value))*1000;
//			}
			
		//	chai[1] = "0." + chai[1];
		//	var parCountEnd = chai[0].toString().substring(chai[0].length-1, chai[0].length);
		//	if(Number(incPar[0].value) != 1){
		//		if(Number(parCountEnd) != Number(incPar[0].value)){
		//			if(Number(parCountEnd) > Number(incPar[0].value)){
		//				parCountEnd = Number(parCountEnd) - Number(incPar[0].value);
		//			}
		//			simCount = Number(parCountEnd) * 1000;
		//			parCount = parCount - parCountEnd;
		//			parCount = Math.round(parCount * 100)/100;
		//			if(parCount.toString().indexOf(".")>0){
		//				var cha = parCount.toString().split(".");
		//				cha[1] = "0." + cha[1];
		//				if(simCount<=(Number(incPar[0].value)*1000)){
		//					simCount=Number(simCount) + Number(cha[1])*1000;
		//				}
		//				parCount = cha[0];
		//			}
		//		} else {
		//			if(simCount<=(Number(incPar[0].value)*1000)){
		//				simCount=Number(chai[1])*1000;
		//			}
		//			parCount = chai[0];
		//		}
		//	} else {
		//		parCount = chai[0];
		//	}
		} else {
			var isBei = parCount / Number(incPar[0].value);
			if(isBei.toString().indexOf(".") > 0){
				var parCountEndTwo = parCount.toString().substring(parCount.toString().length-1, parCount.toString().length);
				if(Number(parCountEndTwo) != Number(incPar[0].value)){
					if(Number(parCountEndTwo) > Number(incPar[0].value)){
						parCountEndTwo = Number(parCountEndTwo) - Number(incPar[0].value);
					}
					simCount = Number(parCountEndTwo) * 1000;
					parCount = parCount - parCountEndTwo;
					if(parCount.toString().indexOf(".")>0){
						var cha = parCount.toString().split(".");
						cha[1] = "0." + cha[1];
						if(simCount<=(Number(incPar[0].value)*1000)){
							simCount=Number(simCount) + Number(cha[1])*1000;
						}
						parCount = cha[0];
					}
				} else {
					if(simCount<=(Number(incPar[0].value)*1000)){
						simCount=Number(chai[1])*1000;
					}
				}
			} else {
				simCount = 0;
			}
		}
	} else {
		simCount = countSum;
		parCount = 0;
	}
	var countSum = 0;
	if(isPanOrK){
		parCount = parCount / Number(incPar[0].value);
	}
	$("#"+gid).val(simCount);
	$("#"+gid + "num").val(parCount);
	isSimMin(gid);
	isParMin(gid);
	getSumCount1(gid);
}
function getSumCount2(gid){
	var countSum = $("input[name="+gid+"countSum]");
	var simCount = $("#"+gid).val();
	var parCount = $("#"+gid + "num").val();
	var incPar = $("input[name="+gid+"gidPar1]");
	if(countSum[0].value.toString().indexOf(".")>0){
		var chaii = countSum[0].value.toString().split(".");
		countSum[0].value = chaii[0];
	}
	if(countSum[0].value >= (Number(incPar[0].value)*1000)){
		parCount = Number(countSum[0].value) / 1000;
		
		if(parCount.toString().indexOf(".")>0){
//			var chai = parCount.toString().split(".");
//			var newXiao = Number(parCount) / Number(incPar[0].value);
//			if(newXiao.toString().indexOf(".") > 0){
//				var chaiNew = newXiao.toString().split(".");
//				parCount = Number(chaiNew[0]) * Number(incPar[0].value);
//				chaiNew[1] = "0."+chaiNew[1];
//				simCount = (Number(chaiNew[1])*Number(incPar[0].value))*1000;
//			}
			
			
		//chai[1] = "0." + chai[1];
		//var parCountEnd = chai[0].toString().substring(chai[0].length-1, chai[0].length);
		//if(Number(incPar[0].value) != 1){
		//	if(Number(parCountEnd) != Number(incPar[0].value)){
		//		if(Number(parCountEnd) > Number(incPar[0].value)){
		//			parCountEnd = Number(parCountEnd) - Number(incPar[0].value);
		//		}
		//		simCount = Number(parCountEnd) * 1000;
		//		parCount = parCount - parCountEnd;
		//		parCount = Math.round(parCount * 100)/100;
		//		if(parCount.toString().indexOf(".")>0){
		//			var cha = parCount.toString().split(".");
		//			cha[1] = "0." + cha[1];
		//			if(simCount<=(Number(incPar[0].value)*1000)){
		//				simCount=Number(simCount) + Number(cha[1])*1000;
		//			}
		//			parCount = cha[0];
		//		}
		//	} else {
		//		if(simCount<=(Number(incPar[0].value)*1000)){
		//			simCount=Number(chai[1])*1000;
		//		}
		//		parCount = chai[0];
		//	}
		//} else {
		//	parCount = chai[0];
		//}
		} else {
			var isBei = parCount / Number(incPar[0].value);
			if(isBei.toString().indexOf(".") > 0){
				var parCountEndTwo = parCount.toString().substring(parCount.toString().length-1, parCount.toString().length);
				if(Number(parCountEndTwo) != Number(incPar[0].value)){
					if(Number(parCountEndTwo) > Number(incPar[0].value)){
						parCountEndTwo = Number(parCountEndTwo) - Number(incPar[0].value);
					}
					simCount = Number(parCountEndTwo) * 1000;
					parCount = parCount - parCountEndTwo;
					if(parCount.toString().indexOf(".")>0){
						var cha = parCount.toString().split(".");
						cha[1] = "0." + cha[1];
						if(simCount<=(Number(incPar[0].value)*1000)){
							simCount=Number(simCount) + Number(cha[1])*1000;
						}
						parCount = cha[0];
					}
				} else {
					if(simCount<=(Number(incPar[0].value)*1000)){
						simCount=Number(chai[1])*1000;
					}
				}
			} else {
				simCount = 0;
			}
		}
	} else {
		simCount = countSum[0].value;
		parCount = 0;
	}
	if(isPanOrKList || isPanOrK){
		parCount = parCount / Number(incPar[0].value);
	}
	$("#"+gid).val(Number(simCount).toFixed(0));
	$("#"+gid + "num").val(Number(parCount));
	//isSimMin(gid);
	//isParMin(gid);
	//getSumCount1(gid);
}
function getSumCount1(gid){
	var simCount = $("#"+gid).val();
	var parCount = $("#"+gid + "num").val();
	var incPar = $("input[name="+gid+"gidPar]");
	if(isPanOrK){
		parCount = parCount * Number(incPar[0].value);
	}
	var countSum = $("#countSum").val();
	if(countSum.toString().indexOf(".")>0){
		var chaii = countSum.toString().split(".");
		countSum = chaii[0];
	}
	if(countSum >= (Number(incPar[0].value)*1000)){
		var nums = 0;
		parCount = Number(countSum) / 1000;
		
		if(parCount.toString().indexOf(".")>0){
//			var chai = parCount.toString().split(".");
//			var newXiao = Number(parCount) / Number(incPar[0].value);
//			if(newXiao.toString().indexOf(".") > 0){
//				var chaiNew = newXiao.toString().split(".");
//				parCount = Number(chaiNew[0]) * Number(incPar[0].value);
//				chaiNew[1] = "0."+chaiNew[1];
//				simCount = (Number(chaiNew[1])*Number(incPar[0].value))*1000;
//			}
			
			//	chai[1] = "0." + chai[1];
			//	var parCountEnd = chai[0].toString().substring(chai[0].length-1, chai[0].length);
			//	if(Number(incPar[0].value) != 1){
			//		if(Number(parCountEnd) != Number(incPar[0].value)){
			//			if(Number(parCountEnd) > Number(incPar[0].value)){
			//				parCountEnd = Number(parCountEnd) - Number(incPar[0].value);
			//			}
			//			simCount = Number(parCountEnd) * 1000;
			//			parCount = parCount - parCountEnd;
			//			parCount = Math.round(parCount * 100)/100;
			//			if(parCount.toString().indexOf(".")>0){
			//				var cha = parCount.toString().split(".");
			//				cha[1] = "0." + cha[1];
			//				if(simCount<=(Number(incPar[0].value)*1000)){
			//					simCount=Number(simCount) + Number(cha[1])*1000;
			//				}
			//				parCount = cha[0];
			//			}
			//		} else {
			//			if(simCount<=(Number(incPar[0].value)*1000)){
			//				simCount=Number(chai[1])*1000;
			//			}
			//			parCount = chai[0];
			//		}
			//	} else {
			//		parCount = chai[0];
			//	}
		} else {
			var isBei = parCount / Number(incPar[0].value);
			if(isBei.toString().indexOf(".") > 0){
				var parCountEndTwo = parCount.toString().substring(parCount.toString().length-1, parCount.toString().length);
				if(Number(parCountEndTwo) != Number(incPar[0].value)){
					if(Number(parCountEndTwo) > Number(incPar[0].value)){
						parCountEndTwo = Number(parCountEndTwo) - Number(incPar[0].value);
					}
					simCount = Number(parCountEndTwo) * 1000;
					parCount = parCount - parCountEndTwo;
					if(parCount.toString().indexOf(".")>0){
						var cha = parCount.toString().split(".");
						cha[1] = "0." + cha[1];
						if(simCount<=(Number(incPar[0].value)*1000)){
							simCount=Number(simCount) + Number(cha[1])*1000;
						}
						parCount = cha[0];
					}
				} else {
					if(simCount<=(Number(incPar[0].value)*1000)){
						simCount=Number(chai[1])*1000;
					}
				}
			} else {
				simCount = 0;
			}
		}
	} else {
		simCount = countSum;
		parCount = 0;
	}
	var countSum = 0;
	if(isPanOrK){
		parCount = parCount / Number(incPar[0].value);
	}
	$("#"+gid).val(simCount);
	$("#"+gid + "num").val(parCount);
	isSimMin(gid);
	isParMin(gid);
}
function addcart(tar){
	var goodsNum = $("#"+tar).val();
	var parCount1 = $("#"+tar + "num").val();
	var isXuan = false;
	if(goodsNum == '0' && parCount1 == '0'){
		isXuan = false;
	} else {
		isXuan = true;
	}
	if(!isXuan){
		alert("请选择数量");
		addcartSuccess = "error";
	}else{
		$.ajaxSetup({async:false});
		$.post("login_isLogin",function(data){
			var goodsParNum = $("#"+tar+"num").val();
			var goodsNum = $("#"+tar).val();
			var pack = "";
			if(goodsParNum == 0){
				pack="1--0";
			} else if(goodsNum == 0) {
				pack="0--1";
			} else {
				pack="1--1";
			}
			if(isPanOrKList){
				var inc = $("input[name="+tar+"gidPar1]").val();
				goodsParNum = goodsParNum * inc;
			}
			var num = goodsNum + "--" + goodsParNum;
			var simPrice = $("#goodsSimPriceCount").val();
			var parPrice = $("#goodsParPriceCount").val();
			if(typeof(simPrice) == "undefined" && typeof(parPrice) == "undefined"){
				simPrice = $("input[name="+tar+"goodsSimPriceCount]").val();
				parPrice = $("input[name="+tar+"goodsParPriceCount]").val();
			}
			var newPriceSim = $("#"+tar+"newPriceSim").val();
			var newPricePar= $("#"+tar+"newPricePar").val();
			if(newPriceSim == ""){
				newPriceSim = "0";
			}
			if(newPricePar == ""){
				newPricePar = "0";
			}
			var ap = simPrice + "--" + parPrice + "--" + newPriceSim + "--" + newPricePar;
			var da = tar+"=="+pack+"=="+num+"=="+ap;
			if(data == "success"){
		$.post("add_cart",{"data":da},function(date,varstart){
			if(date == "success"){
				$(".BMchuangshowgw").show();
			}
		});
			}else{
				$("#myModal").show();
				var url = document.URL.toString();
				$("#nexturl").val(url);
			}
		});
	}
}
var addcartSuccess = "";
var addcartNew=function(tar){
	var goodsNum = $("#"+tar).val();
	var parCount1 = $("#"+tar + "num").val();
	var isXuan = false;
	if(goodsNum == '0' && parCount1 == '0'){
		isXuan = false;
	} else {
		isXuan = true;
	}
	if(!isXuan){
		alert("请选择数量");
		addcartSuccess = "error";
	}else{
		$.ajaxSetup({async:false});
		$.post("login_isLogin",function(data){
			var goodsParNum = $("#"+tar+"num").val();
			var goodsNum = $("#"+tar).val();
			var pack = "";
			if(goodsParNum == 0){
				pack="1--0";
			} else if(goodsNum == 0) {
				pack="0--1";
			} else {
				pack="1--1";
			}
			if(isPanOrKList){
				var inc = $("input[name="+tar+"gidPar1]").val();
				goodsParNum = goodsParNum * inc;
			}
			var num = goodsNum + "--" + (goodsParNum*1000);
			var simPrice = $("#goodsSimPriceCount").val();
			var parPrice = $("#goodsParPriceCount").val();
			if(typeof(simPrice) == "undefined" && typeof(parPrice) == "undefined"){
				simPrice = $("input[name="+tar+"goodsSimPriceCount]").val();
				parPrice = $("input[name="+tar+"goodsParPriceCount]").val();
			}
			var newPriceSim = $("#"+tar+"newPriceSim").val();
			var newPricePar= $("#"+tar+"newPricePar").val();
			if(newPriceSim == ""){
				newPriceSim = "0";
			}
			if(newPricePar == ""){
				newPricePar = "0";
			}
			var ap = simPrice + "--" + parPrice + "--" + newPriceSim + "--" + (newPricePar/1000);
			var da = tar+"=="+pack+"=="+num+"=="+ap;
			if(data == "success"){
				$.post("add_cart",{"data":da},function(date,varstart){
					if(date == "success"){
						//alert("1");
						//$(".BMchuangshowgw").show();
						addcartSuccess = "success";
					} else {
						//alert("2");
						addcartSuccess = "error";
					}
				});
			}else{
				$("#myModal").show();
				var url = document.URL.toString();
				$("#nexturl").val(url);
				return "login";
			}
		});
	}
}
function goBack(){
	$(".BMchuangshowgw").hide();
}
function toCart(){
	location.href="goods_cart";
}
function chose(type,data){
	
	var paid = document.getElementById("paid").value;
	var keyw = document.getElementById("keyw").value;
	var caid = document.getElementById("caid").value;
	var mod = type;
	var cont = data;
	
	location.href="chose_grid?type=1&paid="+paid+"&keyw="+keyw+"&caid="+caid+"&mod="+mod+"&cont="+cont;
}

//通过层登录
function loginByLayer(){
	var username=$("#username").val();
	var passwd=$("#passwd").val();
	if(username == "" || username == "用户名/手机/邮箱"){
		$("#usernametip").text("请输入用户名");
	}else {
		$("#usernametip").text("");
		if(passwd.length<6){
			$("#passwdtip").text("密码长度至少6位");
		}else {
			$("#passwdtip").text("");
			//$("#loginByLayerForm").submit();
			$.ajaxSetup({async:false});
			$.post("login_loginByLayer",{"userDTO.UUserid":username,"userDTO.UPassword":passwd},function(data){
				if(data == "success"){
					//alert("登录成功");
					var url = $("#nexturl").val();
					window.location.href=url;
				}else if(data == "error"){
					$("#msg").text("用户名或者密码错误");
				}else if(data == "mailnotactive"){
					$("#msg").text("邮箱没有激活");
				}else {
					$("#msg").text("非法登录");
				}
			});
		}
}
}

//关闭窗口
function close(){
	$("#myModal").hide();
}
function loadPeiceNew(aaa){
	var gid = $("#gid").val();
	//获取当前样品的数量区间以及当前数量
	var goodsNum = $("#"+gid).val();
	var endSum =  $("input[name="+gid+"gidENum]");
	var gidSNum =  $("input[name="+gid+"gidSNum]");
	var gidSNum =  $("input[name="+gid+"gidSNum]");
	var gidSPrice =  $("input[name="+gid+"gidSPrice]");
	
	var inc = $("input[name="+gid+"gidSim]");
	var incPar = $("input[name="+gid+"gidPar]");
	//获取当前批量的数量区间以及当前数量
	var goodsParNum = $("#"+gid+"num").val();
	var goodParStaSum =  $("input[name="+gid+"gidParSNum]");
	var goodParEndSum =  $("input[name="+gid+"gidParENum]");
	var gidParPrice =  $("input[name="+gid+"gidParPrice]");
	
	
	var sumPrice = 0;
	var parPrice = 0;
	for(var i = 0; i < gidSNum.length; i++ ){
		$("dl[name="+i+"isPriceHover]").removeClass("hover");
		$("dl[name="+i+"isPriceHoverPar]").removeClass("hover");
		if((Number(goodsNum) >= Number(gidSNum[i].value)) && (Number(goodsNum) <= Number(endSum[i].value))){
			if(Number(goodsNum) > (Number(gidSNum[0].value))){
//				var isXiao = Number(goodsNum) / Number(inc[i].value);
//				if(isXiao.toString().indexOf(".") > 0){
//					var sumInc = inc[i].value.length;
//					var chaiGoodsNum = isXiao.toString().split(".");
//					var jian = chaiGoodsNum[1].toString().substring(chaiGoodsNum[1].toString().length-1,chaiGoodsNum[1].toString().length);
//					var isUpAndDown = chaiGoodsNum[1].toString().substring(0,1);
//					if(Number(isUpAndDown) >= Number(incPar[0].value)){
//						isUpAndDown = "0." + Number(isUpAndDown);
//						var sumCha = Number(isUpAndDown)*Number(inc[i].value);
//						goodsNum = Number(goodsNum) - Number(sumCha);
//						goodsNum = Number(goodsNum + Number(inc[i].value));
//						goodsNum = goodsNum - jian;
//					} else {
//						isUpAndDown = "0." + isUpAndDown;
//						var sumCha = Number(isUpAndDown)*Number(inc[i].value);
//						goodsNum = goodsNum - sumCha;
//						goodsNum = goodsNum - jian;
//					}
//				}
			}
			$("#isNowSimPrice").val(gidSPrice[i].value);
			sumPrice = goodsNum * gidSPrice[i].value;
			
			$("dl[name="+i+"isPriceHover]").addClass("hover");
		}
		//if(typeof(gidSNum[2])!="undefined"){
		//	if((Number(goodsNum) >= Number(gidSNum[2].value)) && (Number(goodsNum) <= (Number(incPar[0].value)*1000))){
		//		sumPrice = goodsNum * gidSPrice[2].value;
		//		$("#isNowSimPrice").val(gidSPrice[2].value);
		//		$("li[name="+2+"isPriceHover]").addClass("hover");
		//	}
		//}
		if(isPanOrK){
			if(typeof(goodParStaSum[i]) != "undefined" && Number(goodsParNum) >= Number(goodParStaSum[i].value) && Number(goodParEndSum[i].value) == 0){
				var goodsParNum1 = goodsParNum * Number(incPar[0].value);
				parPrice = goodsParNum1 * Number(gidParPrice[gidParPrice.length-1].value);
				$("#isNowParPrice").val(gidParPrice[gidParPrice.length-1].value);
				$("dl[name="+(gidParPrice.length-1)+"isPriceHoverPar]").addClass("hover");
			} else {
				if(typeof(goodParStaSum[i]) != "undefined" &&(Number(goodsParNum) >= Number(goodParStaSum[i].value)) && (Number(goodsParNum) <= Number(goodParEndSum[i].value))){
					var goodsParNum1 = goodsParNum * Number(incPar[0].value);
					parPrice = goodsParNum1 * Number(gidParPrice[i].value);
					$("#isNowParPrice").val(gidParPrice[i].value);
					$("dl[name="+i+"isPriceHoverPar]").addClass("hover");
				} else if(Number(goodsParNum) < Number(incPar[0].value)) {
					var goodsParNum1 = goodsParNum * Number(incPar[0].value);
					parPrice = goodsParNum1 * Number(gidParPrice[0].value);
					$("#isNowParPrice").val(gidParPrice[0].value);
					$("dl[name="+0+"isPriceHoverPar]").addClass("hover");
				}
			}
		}else {
			if(typeof(goodParStaSum[i]) != "undefined"){
				if(Number(goodsParNum) >= Number(goodParStaSum[i].value) && Number(goodParEndSum[i].value) == 0){
					parPrice = goodsParNum * Number(gidParPrice[gidParPrice.length-1].value);
					$("dl[name="+(gidParPrice.length-1)+"isPriceHoverPar]").addClass("hover");
					$("#isNowParPrice").val(gidParPrice[gidParPrice.length-1].value);
				} else {
					if((Number(goodsParNum) >= Number(goodParStaSum[i].value)) && (Number(goodsParNum) <= Number(goodParEndSum[i].value))){
						parPrice = goodsParNum * Number(gidParPrice[i].value);
						$("dl[name="+i+"isPriceHoverPar]").addClass("hover");
						$("#isNowParPrice").val(gidParPrice[i].value);
					}
				}
			}
		}
	}
	if(typeof(gidSNum[0])=="undefined" ? false : true && Number(goodsNum) < Number(gidSNum[0].value)){
		sumPrice = goodsNum * gidSPrice[0].value;
		$("#isNowSimPrice").val(gidSPrice[0].value);
	}
	if(typeof(incPar[0]) != "undefined" && (Number(goodsNum) > (Number(incPar[0].value)*1000))){
//		var parCount = Number(goodsNum) / 1000;
//		if(parCount.toString().indexOf(".")>0){
//			var chai = parCount.toString().split(".");
//			chai[1] = "0." + chai[1]
//			goodsNum=Number(chai[1])*1000;
//			goodsParNum = chai[0];
//			} else {
//			goodsNum = 0;
//			goodsParNum = parCount;
//		}
//		$("#isNowSimPrice").val(gidSPrice[0].value);
//		sumPrice = goodsNum * gidSPrice[0].value;
	}
	var priceNew = Number(sumPrice) + Number(parPrice);
	priceNew = Math.round(priceNew*100)/100;
	$("#"+gid).val(goodsNum);
	$("#"+gid + "num").val(goodsParNum);
	if(isPanOrK){
		goodsParNum = Number(goodsParNum) * Number(incPar[0].value);
	}
	var countSum = 1000*Number(goodsParNum) + Number($("#"+gid).val());
	countAjax(gid, countSum);
//	$("#countSum").val(countSum);
//	$("#simPriceCount").text(priceNew);
//	$("#goodsSimPriceCount").val(sumPrice);
//	$("#goodsParPriceCount").val(parPrice);
	
}
function sortShu(arr){
	for(var i=0;i<arr.length;i++){
	    for(var j=0; j<arr.length; j++){
	    	if(arr[i] > arr[j]){
	    		var tem = arr[i];
	    		arr[i] = arr[j];
	    		arr[j]=tem;
	    	}
	    	
	    }
	}
	return arr;
}
var korpanid = 0;
function countAjax(gid, count){
	var getSPar = $("input[name="+gid+"gidParSNum]");
	if(typeof(getSPar) != "undefined"){
		var kc = $("#"+gid+"_kucun");
		if(typeof(kc[0].value) != "undefined"){
			if(Number(kc[0].value) < Number(count / 1000)){
				alert("库存不足");
				count = kc[0].value * 1000;
			}
		}
	}
	//alert(gid);
	//alert(count);
	var type = 0;
	if(isPanOrK){
		type=1;
	} else {
		type=0;
	}
	var goodsInfo = $("#"+gid+"pack1num").val();//包装数量  代替  pack1
	var sims = $("input[name="+gid+"gidSNum]");
	var sime = $("input[name="+gid+"gidENum]");
	var siminc = $("input[name="+gid+"gidSim]");
	var simprice = $("input[name="+gid+"gidSPrice]");
	
	var pars = $("input[name="+gid+"gidParSNum]");
	var pare = $("input[name="+gid+"gidParENum]");
	var parinc = $("input[name="+gid+"gidPar]");
	var parprice = $("input[name="+gid+"gidParPrice]");
	for ( var k = 0; k < sims.length; k++) {
		sims[k] = sims[k].value;
	}
	for ( var k = 0; k < sime.length; k++) {
		sime[k] = sime[k].value;
	}
	for ( var k = 0; k < siminc.length; k++) {
		siminc[k] = siminc[k].value;
	}
	for ( var k = 0; k < simprice.length; k++) {
		simprice[k] = simprice[k].value;
	}
	var t = 1;
	if(type == 1){
		t = goodsInfo / (pars[0].value * 1000);
	}
	for ( var k = 0; k < pars.length; k++) {
		pars[k] = pars[k].value * 1000 * t;
	}
	for ( var k = 0; k < pare.length; k++) {
		pare[k] = pare[k].value * 1000 * t;
	}
	for ( var k = 0; k < parinc.length; k++) {
		parinc[k] = parinc[k].value * 1000;
	}
	for ( var k = 0; k < parprice.length; k++) {
		parprice[k] = parprice[k].value / 1000;
	}
	
	parinc = sortShu(parinc);
	pars = sortShu(pars);
	pare = sortShu(pare);
	parprice = sortShu(parprice);
	goodsInfo = parinc[0];
	
	var data = congAjax(count, type, gid, sims,sime,siminc,simprice,pars,pare,parinc,parprice,goodsInfo);
	
	var chai = data.toString().split("____");
	$("#"+gid).val(chai[0]);
	$("#"+gid+"num").val(chai[1]);
	$("#countSum").val(chai[2]);
	$("#simPriceCount").html(chai[3]);
	var cou =  $("input[name="+gid+"gidSNum]");
	var coupar =  $("input[name="+gid+"gidParSNum]");
	for ( var i = 0; i < cou.length; i++) {
		$("dl[name="+ i +"isPriceHover]").removeClass("hover");
	}
	for ( var i = 0; i < coupar.length; i++) {
		$("dl[name="+ i + "isPriceHoverPar]").removeClass("hover");
	}
	if(count != 0){
		$("dl[name="+chai[4]+"isPriceHover]").addClass("hover");
		$("dl[name="+chai[5]+"isPriceHoverPar]").addClass("hover");
	}
	korpanid = chai[5];
	$("#goodsSimPriceCount").val(chai[7]);
	$("#goodsParPriceCount").val(chai[9]);
	$("#isNowSimPrice").val(chai[6]);
	$("#isNowParPrice").val(chai[8]);
	
//	$.post(
//		"congAjax",
//		{"gid":gid,"cont":count,"type":type},
//		function(data,starts){
//			var chai = data.toString().split("____");
//			$("#"+gid).val(chai[0]);
//			$("#"+gid+"num").val(chai[1]);
//			$("#countSum").val(chai[2]);
//			$("#simPriceCount").html(chai[3]);
//			var cou =  $("input[name="+gid+"gidSNum]");
//			var coupar =  $("input[name="+gid+"gidParSNum]");
//			for ( var i = 0; i < cou.length; i++) {
//				$("li[name="+ i +"isPriceHover]").removeClass("hover");
//			}
//			for ( var i = 0; i < coupar.length; i++) {
//				$("li[name="+ i + "isPriceHoverPar]").removeClass("hover");
//			}
//			$("li[name="+chai[4]+"isPriceHover]").addClass("hover");
//			$("li[name="+chai[5]+"isPriceHoverPar]").addClass("hover");
//			korpanid = chai[5];
//			$("#goodsSimPriceCount").val(chai[7]);
//			$("#goodsParPriceCount").val(chai[9]);
//			$("#isNowSimPrice").val(chai[6]);
//			$("#isNowParPrice").val(chai[8]);
//		}
//	);
}

function simcountinAjax(gid, simCount){
	var endSum =  $("input[name="+gid+"gidENum]");
	if(Number(simCount) > Number(endSum[endSum.length-1].value)){
		alert("样品数量最大允许：" + endSum[endSum.length-1].value);
		$("#"+gid).val("0");
		return;
	}
	var allCount = $("#countSum").val();
	//alert(allCount);
	var parCount = $("#"+gid+"num").val();
	var getSPar = $("input[name="+gid+"gidParSNum]");
	var kucun = true;
	if(typeof(getSPar) != "undefined"){
		var kc = $("#"+gid+"_kucun");
		if(typeof(kc[0].value) != "undefined"){
			if(kucun && parCount != 0 && Number(kc[0].value) < Number(getSPar[0].value / 1000)){
				kucun = false;
				alert("库存不足");
				$("#"+gid+"num").val(kc[0].value);
				parCount = kc[0].value;
				allCount = Number(kc[0].value)*1000;
				simCount = 0;
			}
			if(kucun && allCount != 0 && Number(kc[0].value) <= Number(allCount) / 1000 && simCount != 0){
				kucun = false;
				alert("库存不足");
				$("#"+gid+"num").val(kc[0].value);
				parCount = kc[0].value;
				allCount = Number(kc[0].value)*1000;
				simCount = 0;
			}
			if(kucun && Number(kc[0].value) < Number(parCount)){
				kucun = false;
				alert("库存不足");
				$("#"+gid+"num").val(kc[0].value);
				parCount = kc[0].value;
				allCount = Number(kc[0].value)*1000;
				simCount = 0;
			}
		}
	}
	
	var type = 0;
	if(isPanOrK){
		type=1;
	} else {
		type=0;
	}
	var goodsInfo = $("#"+gid+"pack1num").val();//包装数量  代替  pack1
	
	var sims = $("input[name="+gid+"gidSNum]");
	var sime = $("input[name="+gid+"gidENum]");
	var siminc = $("input[name="+gid+"gidSim]");
	var simprice = $("input[name="+gid+"gidSPrice]");
	
	var pars = $("input[name="+gid+"gidParSNum]");
	var pare = $("input[name="+gid+"gidParENum]");
	var parinc = $("input[name="+gid+"gidPar]");
	var parprice = $("input[name="+gid+"gidParPrice]");
	for ( var k = 0; k < sims.length; k++) {
		sims[k] = sims[k].value;
	}
	for ( var k = 0; k < sime.length; k++) {
		sime[k] = sime[k].value;
	}
	for ( var k = 0; k < siminc.length; k++) {
		siminc[k] = siminc[k].value;
	}
	for ( var k = 0; k < simprice.length; k++) {
		simprice[k] = simprice[k].value;
	}
	var t = 1;
	if(type == 1){
		t = goodsInfo / (pars[0].value * 1000);
	}
	for ( var k = 0; k < pars.length; k++) {
		pars[k] = pars[k].value * 1000 * t;
	}
	for ( var k = 0; k < pare.length; k++) {
		pare[k] = pare[k].value * 1000 * t;
	}
	for ( var k = 0; k < parinc.length; k++) {
		parinc[k] = parinc[k].value * 1000;
	}
	for ( var k = 0; k < parprice.length; k++) {
		parprice[k] = parprice[k].value / 1000;
	}
	
	parinc = sortShu(parinc);
	pars = sortShu(pars);
	pare = sortShu(pare);
	parprice = sortShu(parprice);
	goodsInfo = parinc[0];
	
	var data = simCountInAjax(allCount, simCount, parCount, isPanOrK, gid, sims,sime,siminc,simprice,pars,pare,parinc,parprice,goodsInfo)
	
	var chai = data.toString().split("____");
	$("#"+gid).val(chai[0]);
	$("#"+gid+"num").val(chai[1]);
	$("#countSum").val(chai[2]);
	$("#simPriceCount").html(chai[3]);
	var cou =  $("input[name="+gid+"gidSNum]");
	var coupar =  $("input[name="+gid+"gidParSNum]");
	for ( var i = 0; i < cou.length; i++) {
		$("dl[name="+ i +"isPriceHover]").removeClass("hover");
	}
	for ( var i = 0; i < coupar.length; i++) {
		$("dl[name="+ i + "isPriceHoverPar]").removeClass("hover");
	}
	if(simCount != 0){
		$("dl[name="+chai[4]+"isPriceHover]").addClass("hover");
	}
	$("dl[name="+chai[5]+"isPriceHoverPar]").addClass("hover");
	//korpanid = chai[5];
	$("#goodsSimPriceCount").val(chai[7]);
	$("#goodsParPriceCount").val(chai[9]);
	$("#isNowSimPrice").val(chai[6]);
	$("#isNowParPrice").val(chai[8]);
	
//	$.post(
//		"simCountInAjax",
//		{"gid":gid,"simCount":simCount,"parCount":parCount,"allCount":allCount,"type":type},
//		function(data, varstart){
//			var chai = data.toString().split("____");
//			$("#"+gid).val(chai[0]);
//			$("#"+gid+"num").val(chai[1]);
//			$("#countSum").val(chai[2]);
//			$("#simPriceCount").html(chai[3]);
//			var cou =  $("input[name="+gid+"gidSNum]");
//			var coupar =  $("input[name="+gid+"gidParSNum]");
//			for ( var i = 0; i < cou.length; i++) {
//				$("li[name="+ i +"isPriceHover]").removeClass("hover");
//			}
//			for ( var i = 0; i < coupar.length; i++) {
//				$("li[name="+ i + "isPriceHoverPar]").removeClass("hover");
//			}
//			$("li[name="+chai[4]+"isPriceHover]").addClass("hover");
//			$("li[name="+chai[5]+"isPriceHoverPar]").addClass("hover");
//			//korpanid = chai[5];
//			$("#goodsSimPriceCount").val(chai[7]);
//			$("#goodsParPriceCount").val(chai[9]);
//			$("#isNowSimPrice").val(chai[6]);
//			$("#isNowParPrice").val(chai[8]);
//		}
//	);
}

function parCountAjax(gid, parCount){
	var allCount = $("input[name="+gid+"countSum]").val();
	var simCount = $("#"+gid).val();
	var getSPar = $("input[name="+gid+"gidParSNum]");
	if(typeof(getSPar) != "undefined"){
		var kc = $("#"+gid+"_kucun");
		if(typeof(kc[0].value) != "undefined"){
			var iso = true;
			if(iso && parCount != 0 && Number(kc[0].value) < Number(getSPar[0].value)){
				alert("库存不足");
//				$("#"+gid+"num").val(kc[0].value);
//				parCount = 0;
				allCount = Number(kc[0].value)*1000;
//				simCount = 0;
				iso = false;
			}
			if(iso && (Number(kc[0].value) < Number(parCount) || (Number(kc[0].value) == Number(parCount) && simCount != 0))){
				alert("库存不足");
//				$("#"+gid+"num").val(kc[0].value);
				parCount = kc[0].value;
				allCount = Number(kc[0].value)*1000;
				simCount = 0;
				iso = false;
			}
			if(iso && allCount != 0 && Number(kc[0].value) < Number(allCount) / 1000){
				alert("库存不足");
//				$("#"+gid+"num").val(kc[0].value);
				parCount = kc[0].value;
				allCount = Number(kc[0].value)*1000;
				simCount = 0;
				iso = false;
			}
			if(Number(kc[0].value) < Number(getSPar[0].value)){
				simCount = Number(kc[0].value)*1000;
				parCount = 0;
			}
		}
	}
//	var allCount = $("#countSum").val();
//	var simCount = $("#"+gid).val();
	var type = 0;
	if(isPanOrK){
		type=1;
	} else {
		type=0;
	}
	var goodsInfo = $("#"+gid+"pack1num").val();//包装数量  代替  pack1
	var sims = $("input[name="+gid+"gidSNum]");
	var sime = $("input[name="+gid+"gidENum]");
	var siminc = $("input[name="+gid+"gidSim]");
	var simprice = $("input[name="+gid+"gidSPrice]");
	
	var pars = $("input[name="+gid+"gidParSNum]");
	var pare = $("input[name="+gid+"gidParENum]");
	var parinc = $("input[name="+gid+"gidPar]");
	var parprice = $("input[name="+gid+"gidParPrice]");
	for ( var k = 0; k < sims.length; k++) {
		sims[k] = sims[k].value;
	}
	for ( var k = 0; k < sime.length; k++) {
		sime[k] = sime[k].value;
	}
	for ( var k = 0; k < siminc.length; k++) {
		siminc[k] = siminc[k].value;
	}
	for ( var k = 0; k < simprice.length; k++) {
		simprice[k] = simprice[k].value;
	}
	var t = 1;
	if(type == 1){
		t = goodsInfo / (pars[0].value * 1000);
	}
	for ( var k = 0; k < pars.length; k++) {
		pars[k] = pars[k].value * 1000 * t;
	}
	for ( var k = 0; k < pare.length; k++) {
		pare[k] = pare[k].value * 1000 * t;
	}
	for ( var k = 0; k < parinc.length; k++) {
		parinc[k] = parinc[k].value * 1000;
	}
	for ( var k = 0; k < parprice.length; k++) {
		parprice[k] = parprice[k].value / 1000;
	}
	parinc = sortShu(parinc);
	pars = sortShu(pars);
	pare = sortShu(pare);
	parprice = sortShu(parprice);
	goodsInfo = parinc[0];
	
	var data = simCountInAjax(allCount, simCount, parCount, isPanOrK, gid, sims,sime,siminc,simprice,pars,pare,parinc,parprice,goodsInfo)
	var chai = data.toString().split("____");
	$("#"+gid).val(chai[0]);
	$("#"+gid+"num").val(chai[1]);
	$("#countSum").val(chai[2]);
	$("#simPriceCount").html(chai[3]);
	var cou =  $("input[name="+gid+"gidSNum]");
	var coupar =  $("input[name="+gid+"gidParSNum]");
	for ( var i = 0; i < cou.length; i++) {
		$("dl[name="+ i +"isPriceHover]").removeClass("hover");
	}
	for ( var i = 0; i < coupar.length; i++) {
		$("dl[name="+ i + "isPriceHoverPar]").removeClass("hover");
	}
	if(chai[0] != 0){
		$("dl[name="+chai[4]+"isPriceHover]").addClass("hover");
	}
//	$("dl[name="+chai[4]+"isPriceHover]").addClass("hover");
	$("dl[name="+chai[5]+"isPriceHoverPar]").addClass("hover");
	$("#goodsSimPriceCount").val(chai[7]);
	$("#goodsParPriceCount").val(chai[9]);
	$("#isNowSimPrice").val(chai[6]);
	$("#isNowParPrice").val(chai[8]);
	
//	$.post(
//		"parCountAjax",
//		{"gid":gid,"simCount":simCount,"parCount":parCount,"allCount":allCount,"type":type},
//		function(data, varstart){
//			var chai = data.toString().split("____");
//			$("#"+gid).val(chai[0]);
//			$("#"+gid+"num").val(chai[1]);
//			$("#countSum").val(chai[2]);
//			$("#simPriceCount").html(chai[3]);
//			var cou =  $("input[name="+gid+"gidSNum]");
//			var coupar =  $("input[name="+gid+"gidParSNum]");
//			for ( var i = 0; i < cou.length; i++) {
//				$("li[name="+ i +"isPriceHover]").removeClass("hover");
//			}
//			for ( var i = 0; i < coupar.length; i++) {
//				$("li[name="+ i + "isPriceHoverPar]").removeClass("hover");
//			}
//			$("li[name="+chai[4]+"isPriceHover]").addClass("hover");
//			$("li[name="+chai[5]+"isPriceHoverPar]").addClass("hover");
//			$("#goodsSimPriceCount").val(chai[7]);
//			$("#goodsParPriceCount").val(chai[9]);
//			$("#isNowSimPrice").val(chai[6]);
//			$("#isNowParPrice").val(chai[8]);
//		}
//	);
}

function listsimAjax(gid,simCount){
	var endSum =  $("input[name="+gid+"gidENum]");
	if(Number(simCount) > Number(endSum[endSum.length-1].value)){
		alert("样品数量最大允许：" + endSum[endSum.length-1].value);
		$("#"+gid).val("0");
		return;
	}
	var allCount = $("input[name="+gid+"countSum]").val();
	var parCount = $("#"+gid+"num").val();
	var getSPar = $("input[name="+gid+"gidParSNum]");
	if(typeof(getSPar) != "undefined"){
		var kc = $("#"+gid+"_kucun");
		if(typeof(kc[0].value) != "undefined"){
			if(parCount != 0 && Number(kc[0].value) < Number(getSPar[0].value / 1000)){
				alert("库存不足");
				$("#"+gid+"num").val(kc[0].value);
				parCount = kc[0].value;
				allCount = Number(kc[0].value)*1000;
				simCount = 0;
			}
			if(allCount != 0 && Number(kc[0].value) <= Number(allCount) / 1000 && simCount != 0){
				alert("库存不足");
				$("#"+gid+"num").val(kc[0].value);
				parCount = kc[0].value;
				allCount = Number(kc[0].value)*1000;
				simCount = 0;
			}
			if(Number(kc[0].value) < Number(parCount)){
				alert("库存不足");
				$("#"+gid+"num").val(kc[0].value);
				parCount = kc[0].value;
				allCount = Number(kc[0].value)*1000;
				simCount = 0;
			}
		}
	}
	//alert(allCount);
	var type = 0;
	if(isPanOrKList){
		type=1;
	} else {
		type=0;
	}
	
	var goodsInfo = $("#"+gid+"pack1num").val();//包装数量  代替  pack1
	var sims = $("input[name="+gid+"gidSNum]");
	var sime = $("input[name="+gid+"gidENum]");
	var siminc = $("input[name="+gid+"gidSim]");
	var simprice = $("input[name="+gid+"gidSPrice]");
	
	var pars = $("input[name="+gid+"gidParSNum]");
	var pare = $("input[name="+gid+"gidParENum]");
	var parinc = $("input[name="+gid+"gidPar]");
	var parprice = $("input[name="+gid+"gidParPrice]");
	for ( var k = 0; k < sims.length; k++) {
		sims[k] = sims[k].value;
	}
	for ( var k = 0; k < sime.length; k++) {
		sime[k] = sime[k].value;
	}
	for ( var k = 0; k < siminc.length; k++) {
		siminc[k] = siminc[k].value;
	}
	for ( var k = 0; k < simprice.length; k++) {
		simprice[k] = simprice[k].value;
	}
	var t = 1;
	if(type == 1){
		t = goodsInfo / (pars[0].value * 1000);
	}
	for ( var k = 0; k < pars.length; k++) {
		pars[k] = pars[k].value * 1000 * t;
	}
	for ( var k = 0; k < pare.length; k++) {
		pare[k] = pare[k].value * 1000 * t;
	}
	for ( var k = 0; k < parinc.length; k++) {
		parinc[k] = parinc[k].value * 1000;
	}
	for ( var k = 0; k < parprice.length; k++) {
		parprice[k] = parprice[k].value / 1000;
	}
	parinc = sortShu(parinc);
	pars = sortShu(pars);
	pare = sortShu(pare);
	parprice = sortShu(parprice);
	goodsInfo = parinc[0];
	
	var data = simCountInAjax(allCount, simCount, parCount, isPanOrKList, gid, sims,sime,siminc,simprice,pars,pare,parinc,parprice,goodsInfo)
	
	var chai = data.toString().split("____");
	$("#"+gid).val(chai[0]);
	$("#"+gid+"num").val(chai[1]);
	$("input[name="+gid+"countSum]").val(chai[2]);
	var cou =  $("input[name="+gid+"gidSNum]");
	var coupar =  $("input[name="+gid+"gidParSNum]");
	for ( var i = 0; i < cou.length; i++) {
		$("li[name="+gid + i +"isPriceHover]").removeClass("hover");
	}
	for ( var i = 0; i < coupar.length; i++) {
		$("li[name="+gid+ i + "isPriceHoverPar]").removeClass("hover");
	}
	if(simCount != 0){
		$("li[name="+gid+chai[4]+"isPriceHover]").addClass("hover");
	}
	$("li[name="+gid+chai[5]+"isPriceHoverPar]").addClass("hover");
	//korpanid = chai[5];
	$("input[name="+gid+"goodsSimPriceCount]").val(chai[7]);
	$("input[name="+gid+"goodsParPriceCount]").val(chai[9]);
	$("#"+gid+"newPriceSim").val(chai[6]);
	$("#"+gid+"newPricePar").val(chai[8]);
//	$.post(
//		"simCountInAjax",
//		{"gid":gid,"simCount":simCount,"parCount":parCount,"allCount":allCount,"type":type},
//		function(data, varstart){
//			var chai = data.toString().split("____");
//			$("#"+gid).val(chai[0]);
//			$("#"+gid+"num").val(chai[1]);
//			$("input[name="+gid+"countSum]").val(chai[2]);
//			var cou =  $("input[name="+gid+"gidSNum]");
//			var coupar =  $("input[name="+gid+"gidParSNum]");
//			for ( var i = 0; i < cou.length; i++) {
//				$("li[name="+gid + i +"isPriceHover]").removeClass("hover");
//			}
//			for ( var i = 0; i < coupar.length; i++) {
//				$("li[name="+gid+ i + "isPriceHoverPar]").removeClass("hover");
//			}
//			$("li[name="+gid+chai[4]+"isPriceHover]").addClass("hover");
//			$("li[name="+gid+chai[5]+"isPriceHoverPar]").addClass("hover");
//			//korpanid = chai[5];
//			$("input[name="+gid+"goodsSimPriceCount]").val(chai[7]);
//			$("input[name="+gid+"goodsParPriceCount]").val(chai[9]);
//			$("#"+gid+"newPriceSim").val(chai[6]);
//			$("#"+gid+"newPricePar").val(chai[8]);
//		}
//	);
}
function listparAjax(gid,parCount){
	var allCount = $("input[name="+gid+"countSum]").val();
	var simCount = $("#"+gid).val();
	var getSPar = $("input[name="+gid+"gidParSNum]");
	if(typeof(getSPar) != "undefined"){
		var kc = $("#"+gid+"_kucun");
		if(typeof(kc[0].value) != "undefined"){
			var iso = true;
			if(iso && parCount != 0 && Number(kc[0].value) < Number(getSPar[0].value)){
				alert("库存不足");
//				$("#"+gid+"num").val(kc[0].value);
				parCount = kc[0].value;
				allCount = Number(kc[0].value)*1000;
				simCount = 0;
				iso = false;
			}
			if(iso && (Number(kc[0].value) < Number(parCount) || (Number(kc[0].value) == Number(parCount) && simCount != 0))){
				alert("库存不足");
//				$("#"+gid+"num").val(kc[0].value);
				parCount = kc[0].value;
				allCount = Number(kc[0].value)*1000;
				simCount = 0;
				iso = false;
			}
			if(iso && allCount != 0 && Number(kc[0].value) < Number(allCount) / 1000){
				alert("库存不足");
//				$("#"+gid+"num").val(kc[0].value);
				parCount = kc[0].value;
				allCount = Number(kc[0].value)*1000;
				simCount = 0;
				iso = false;
			}
//			if(Number(kc[0].value) < Number(parCount)){
//				alert("库存不足");
//				$("#"+gid+"num").val("0");
//				parCount = 0;
//			}
			if(Number(kc[0].value) < Number(getSPar[0].value)){
				simCount = Number(kc[0].value)*1000;
				parCount = 0;
			}
		}
	}
	var allCount = $("input[name="+gid+"countSum]").val();
	var simCount = $("#"+gid).val();
	var type = 0;
	if(isPanOrKList){
		type=1;
	} else {
		type=0;
	}
	var goodsInfo = $("#"+gid+"pack1num").val();//包装数量  代替  pack1
	var sims = $("input[name="+gid+"gidSNum]");
	var sime = $("input[name="+gid+"gidENum]");
	var siminc = $("input[name="+gid+"gidSim]");
	var simprice = $("input[name="+gid+"gidSPrice]");
	
	var pars = $("input[name="+gid+"gidParSNum]");
	var pare = $("input[name="+gid+"gidParENum]");
	var parinc = $("input[name="+gid+"gidPar]");
	var parprice = $("input[name="+gid+"gidParPrice]");
	for ( var k = 0; k < sims.length; k++) {
		sims[k] = sims[k].value;
	}
	for ( var k = 0; k < sime.length; k++) {
		sime[k] = sime[k].value;
	}
	for ( var k = 0; k < siminc.length; k++) {
		siminc[k] = siminc[k].value;
	}
	for ( var k = 0; k < simprice.length; k++) {
		simprice[k] = simprice[k].value;
	}
	var t = 1;
	if(type == 1){
		t = goodsInfo / (pars[0].value * 1000);
	}
	for ( var k = 0; k < pars.length; k++) {
		pars[k] = pars[k].value * 1000 * t;
	}
	for ( var k = 0; k < pare.length; k++) {
		pare[k] = pare[k].value * 1000 * t;
	}
	for ( var k = 0; k < parinc.length; k++) {
		parinc[k] = parinc[k].value * 1000;
	}
	for ( var k = 0; k < parprice.length; k++) {
		parprice[k] = parprice[k].value / 1000;
	}
	parinc = sortShu(parinc);
	pars = sortShu(pars);
	pare = sortShu(pare);
	parprice = sortShu(parprice);
	goodsInfo = parinc[0];
	
	var data = simCountInAjax(allCount, simCount, parCount, isPanOrKList, gid, sims,sime,siminc,simprice,pars,pare,parinc,parprice,goodsInfo)
	var chai = data.toString().split("____");
	$("#"+gid).val(chai[0]);
	$("#"+gid+"num").val(chai[1]);
	$("input[name="+gid+"countSum]").val(chai[2]);
	var cou =  $("input[name="+gid+"gidSNum]");
	var coupar =  $("input[name="+gid+"gidParSNum]");
	for ( var i = 0; i < cou.length; i++) {
		$("li[name="+gid + i +"isPriceHover]").removeClass("hover");
	}
	for ( var i = 0; i < coupar.length; i++) {
		$("li[name="+gid+ i + "isPriceHoverPar]").removeClass("hover");
	}
	if(chai[0] != 0){
		$("li[name="+gid+chai[4]+"isPriceHover]").addClass("hover");
	}
	$("li[name="+gid+chai[5]+"isPriceHoverPar]").addClass("hover");
	//korpanid = chai[5];
	$("input[name="+gid+"goodsSimPriceCount]").val(chai[7]);
	$("input[name="+gid+"goodsParPriceCount]").val(chai[9]);
	$("#"+gid+"newPriceSim").val(chai[6]);
	$("#"+gid+"newPricePar").val(chai[8]);
	
//	$.post(
//		"parCountAjax",
//		{"gid":gid,"simCount":simCount,"parCount":parCount,"allCount":allCount,"type":type},
//		function(data, varstart){
//			var chai = data.toString().split("____");
//			$("#"+gid).val(chai[0]);
//			$("#"+gid+"num").val(chai[1]);
//			$("input[name="+gid+"countSum]").val(chai[2]);
//			var cou =  $("input[name="+gid+"gidSNum]");
//			var coupar =  $("input[name="+gid+"gidParSNum]");
//			for ( var i = 0; i < cou.length; i++) {
//				$("li[name="+gid + i +"isPriceHover]").removeClass("hover");
//			}
//			for ( var i = 0; i < coupar.length; i++) {
//				$("li[name="+gid+ i + "isPriceHoverPar]").removeClass("hover");
//			}
//			$("li[name="+gid+chai[4]+"isPriceHover]").addClass("hover");
//			$("li[name="+gid+chai[5]+"isPriceHoverPar]").addClass("hover");
//			//korpanid = chai[5];
//			$("input[name="+gid+"goodsSimPriceCount]").val(chai[7]);
//			$("input[name="+gid+"goodsParPriceCount]").val(chai[9]);
//			$("#"+gid+"newPriceSim").val(chai[6]);
//			$("#"+gid+"newPricePar").val(chai[8]);
//		}
//	);
}

function listAllCountAjax(gid,count){
	var getSPar = $("input[name="+gid+"gidParSNum]");
	if(typeof(getSPar) != "undefined"){
		var kc = $("#"+gid+"_kucun");
		if(typeof(kc[0].value) != "undefined"){
			if(Number(kc[0].value) < Number(count / 1000)){
				alert("库存不足");
				count = kc[0].value * 1000;
			}
		}
	}
	//alert(gid);
	//alert(count);
	var type = 0;
	if(isPanOrKList){
		type=1;
	} else {
		type=0;
	}
	
	var goodsInfo = $("#"+gid+"pack1num").val();//包装数量  代替  pack1
	var sims = $("input[name="+gid+"gidSNum]");
	var sime = $("input[name="+gid+"gidENum]");
	var siminc = $("input[name="+gid+"gidSim]");
	var simprice = $("input[name="+gid+"gidSPrice]");
	
	var pars = $("input[name="+gid+"gidParSNum]");
	var pare = $("input[name="+gid+"gidParENum]");
	var parinc = $("input[name="+gid+"gidPar]");
	var parprice = $("input[name="+gid+"gidParPrice]");
	for ( var k = 0; k < sims.length; k++) {
		sims[k] = sims[k].value;
	}
	for ( var k = 0; k < sime.length; k++) {
		sime[k] = sime[k].value;
	}
	for ( var k = 0; k < siminc.length; k++) {
		siminc[k] = siminc[k].value;
	}
	for ( var k = 0; k < simprice.length; k++) {
		simprice[k] = simprice[k].value;
	}
	var t = 1;
	if(type == 1){
		t = goodsInfo / (pars[0].value * 1000);
	}
	for ( var k = 0; k < pars.length; k++) {
		pars[k] = pars[k].value * 1000 * t;
	}
	for ( var k = 0; k < pare.length; k++) {
		pare[k] = pare[k].value * 1000 * t;
	}
	for ( var k = 0; k < parinc.length; k++) {
		parinc[k] = parinc[k].value * 1000;
	}
	for ( var k = 0; k < parprice.length; k++) {
		parprice[k] = parprice[k].value / 1000;
	}
	
	parinc = sortShu(parinc);
	pars = sortShu(pars);
	pare = sortShu(pare);
	parprice = sortShu(parprice);
	goodsInfo = parinc[0];
	var data = congAjax(count, type, gid, sims,sime,siminc,simprice,pars,pare,parinc,parprice,goodsInfo);
	
	var chai = data.toString().split("____");
	$("#"+gid).val(chai[0]);
	$("#"+gid+"num").val(chai[1]);
	$("input[name="+gid+"countSum]").val(chai[2]);
	var cou =  $("input[name="+gid+"gidSNum]");
	var coupar =  $("input[name="+gid+"gidParSNum]");
	for ( var i = 0; i < cou.length; i++) {
		$("li[name="+gid + i +"isPriceHover]").removeClass("hover");
	}
	for ( var i = 0; i < coupar.length; i++) {
		$("li[name="+gid+ i + "isPriceHoverPar]").removeClass("hover");
	}
	if(count != 0){
		$("li[name="+gid+chai[4]+"isPriceHover]").addClass("hover");
		$("li[name="+gid+chai[5]+"isPriceHoverPar]").addClass("hover");
	}
	
	//korpanid = chai[5];
	$("input[name="+gid+"goodsSimPriceCount]").val(chai[7]);
	$("input[name="+gid+"goodsParPriceCount]").val(chai[9]);
	$("#"+gid+"newPriceSim").val(chai[6]);
	$("#"+gid+"newPricePar").val(chai[8]);
	
//	$.post(
//		"congAjax",
//		{"gid":gid,"cont":count,"type":type},
//		function(data,starts){
//			var chai = data.toString().split("____");
//			$("#"+gid).val(chai[0]);
//			$("#"+gid+"num").val(chai[1]);
//			$("input[name="+gid+"countSum]").val(chai[2]);
//			var cou =  $("input[name="+gid+"gidSNum]");
//			var coupar =  $("input[name="+gid+"gidParSNum]");
//			for ( var i = 0; i < cou.length; i++) {
//				$("li[name="+gid + i +"isPriceHover]").removeClass("hover");
//			}
//			for ( var i = 0; i < coupar.length; i++) {
//				$("li[name="+gid+ i + "isPriceHoverPar]").removeClass("hover");
//			}
//			$("li[name="+gid+chai[4]+"isPriceHover]").addClass("hover");
//			$("li[name="+gid+chai[5]+"isPriceHoverPar]").addClass("hover");
//			//korpanid = chai[5];
//			$("input[name="+gid+"goodsSimPriceCount]").val(chai[7]);
//			$("input[name="+gid+"goodsParPriceCount]").val(chai[9]);
//			$("#"+gid+"newPriceSim").val(chai[6]);
//			$("#"+gid+"newPricePar").val(chai[8]);
//		}
//	);
}

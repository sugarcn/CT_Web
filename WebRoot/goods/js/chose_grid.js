function topage(){
	var tarpage = Number(document.getElementById("tarpage").value);
	var totalpage = Number(document.getElementById("totalpage").value);
	if(tarpage>totalpage||tarpage<1){
		alert("请输入正确的页数！");
	}else{
		var type = document.getElementById("type").value;
		var paid = document.getElementById("paid").value;
		var keyw = document.getElementById("keyw").value;
		var caid = document.getElementById("caid").value;
		var brid = document.getElementById("brid").value;
		var mod = document.getElementById("mod").value;
		var cont = document.getElementById("cont").value;
		if(type==1){
			location.href="chose_grid?type=1&paid="+paid+"&keyw="+keyw+"&caid="+caid+"&mod="+mod+"&cont="+cont+"&page="+tarpage;
		}
		if(type==2){
			location.href="chose_grid?type=2&brid="+brid+"&mod="+mod+"&cont="+cont+"&page="+tarpage;
		}
	}
}
function subnum(num){
	var goodsNum = $("#"+num).val();
	if(goodsNum < 2){
		alert("数量不能小于1");
		return ;
	}
	var newgoodsNum = Number(goodsNum) - 1;
	$("#"+num).val(newgoodsNum);
}
function addnum(num){
	var goodsNum = $("#"+num).val();
	var newgoodsNum = Number(goodsNum) + 1;
	$("#"+num).val(newgoodsNum);
}
function addcart(tar){
	$.ajaxSetup({async:false});
	$.post("login_isLogin",function(data){
		var obj = document.getElementById(tar+"SE");
		var pp = document.getElementById(tar+"PP").value;
		var index = obj.selectedIndex;
		var text = obj.options[index].text;
		var value = obj.options[index].value;
		var num = document.getElementById(tar).value;
		var pns= new Array(); 
		pns = text.split("/");
		var pn = pns[0];
		var ap = pp*pn*num;
		var da = tar+"=="+value+"=="+num+"=="+ap;
		if(data == "success"){
			location.href="add_cart?data="+da;
		}else{
			$("#myModal").show();
			$("#nexturl").val("add_cart?data="+da);
		}
	});
}
function changepage(page){
	var type = document.getElementById("type").value;
	var paid = document.getElementById("paid").value;
	var keyw = document.getElementById("keyw").value;
	var caid = document.getElementById("caid").value;
	var brid = document.getElementById("brid").value;
	var mod = document.getElementById("mod").value;
	var cont = document.getElementById("cont").value;
	var $jiage1 = $("#jiage1").val();
	var $jiage2 = $("#jiage2").val();
	if(type==1){
		location.href="chose_grid?type=1&paid="+paid+"&keyw="+keyw+"&caid="+caid+"&mod="+mod+"&cont="+cont+"&page="+page+"&fgoodsDTO.price1="+ $jiage1 +"&fgoodsDTO.price2="+$jiage2;
	}
	if(type==2){
		location.href="chose_grid?type=2&brid="+brid+"&mod="+mod+"&cont="+cont+"&page="+page;
	}
}
function tolist(page){
	var type = document.getElementById("type").value;
	var paid = document.getElementById("paid").value;
	var keyw = document.getElementById("keyww").value;
	var caid = document.getElementById("caid").value;
	var brid = document.getElementById("brid").value;
	var mod = document.getElementById("mod").value;
	var cont = document.getElementById("cont").value;
	var $jiage1 = $("#jiage1").val();
	var $jiage2 = $("#jiage2").val();
	if(type==1){
		location.href="chose_list?type=1&paid="+paid+"&keyw="+keyw+"&caid="+caid+"&mod="+mod+"&cont="+cont+"&page="+page+"&fgoodsDTO.price1="+ $jiage1 +"&fgoodsDTO.price2="+$jiage2;
	}
	if(type==2){
		location.href="chose_list?type=2&brid="+brid+"&mod="+mod+"&cont="+cont+"&page="+page;
	}
}

//通过层登录
function loginByLayer(){
	var username=$("#username").val();
	var passwd=$("#passwd").val();
	if(username == "" || username == "用户名/手机/邮箱"){
		$("#usernametip").text("请输入用户名");
	}else {
		$("#usernametip").text("");
		if(passwd.length<6){
			$("#passwdtip").text("密码长度至少6位");
		}else {
			$("#passwdtip").text("");
			//$("#loginByLayerForm").submit();
			$.ajaxSetup({async:false});
			$.post("login_loginByLayer",{"userDTO.UUserid":username,"userDTO.UPassword":passwd},function(data){
				if(data == "success"){
					//alert("登录成功");
					var url = $("#nexturl").val();
					window.location.href=url;
				}else if(data == "error"){
					$("#msg").text("用户名或者密码错误");
				}else if(data == "mailnotactive"){
					$("#msg").text("邮箱没有激活");
				}else {
					$("#msg").text("非法登录");
				}
			});
		}
}
}

//关闭窗口
function close(){
	$("#myModal").hide();
}

/* 分页跳转控制器 */
function upAndDownPage(sum){
	//截取访问名
	var url = document.URL.toString();
	var i = url.lastIndexOf("/");
	var s = url.lastIndexOf("?");
	url = url.substring(i+1, s);

	var paid = document.getElementById("paid").value;
	var keyw = document.getElementById("keyw").value;
	var caid = document.getElementById("caid").value;
	window.location.href= url+"?page=" + sum + "&paid=" + paid + "&keyw="
	+ keyw + "&caid=" + caid;
}

/* 分页搜索跳转 */
function topage() {
	var tarpage = Number(document.getElementById("tarpage").value);
	var totalpage = Number(document.getElementById("totalpage").value);
	var paid = document.getElementById("paid").value;
	var keyw = document.getElementById("keyw").value;
	var caid = document.getElementById("caid").value;
	if (tarpage > totalpage || tarpage < 1) {
		alert("请输入正确的页数！");
	} else {
		location.href = "goods_search?page=" + tarpage + "&paid=" + paid
				+ "&keyw=" + keyw + "&caid=" + caid;
	}
}
function subnum(num) {
	var goodsNum = $("#" + num).val();
	if (goodsNum < 2) {
		alert("数量不能小于1");
		return;
	}
	var newgoodsNum = Number(goodsNum) - 1;
	$("#" + num).val(newgoodsNum);
}
function addnum(num) {
	var goodsNum = $("#" + num).val();
	var newgoodsNum = Number(goodsNum) + 1;
	$("#" + num).val(newgoodsNum);
}
function addcart(tar) {
	$.ajaxSetup({
		async : false
	});
	$.post("login_isLogin", function(data) {
		var obj = document.getElementById(tar + "SE");
		var pp = document.getElementById(tar + "PP").value;
		var index = obj.selectedIndex;
		var text = obj.options[index].text;
		var value = obj.options[index].value;
		var num = document.getElementById(tar).value;
		var pns = new Array();
		pns = text.split("/");
		var pn = pns[0];
		pn = pn.substring(0, pn.length - 1);
		var ap = pp * pn * num;
		var da = tar + "==" + value + "==" + num + "==" + ap;
		if (data == "success") {
			$.post("add_cart", {
				"data" : da
			}, function(date, varstart) {
				if (date == "success") {
					$(".BMchuangshowgw").show();
				}
			});
		} else {
			$("#myModal").show();
			$("#nexturl").val("add_cart?data=" + da);
		}
	});
}
function goBack() {
	$(".BMchuangshowgw").hide();
}
function divHide() {
	$(".BMchuangshowgw").hide();
}
function toCart() {
	location.href = "goods_cart";
}
function chose(type, data) {
	var dat1 = $("#" + data).val();
	var paid = document.getElementById("paid").value;
	var keyw = document.getElementById("keyw").value;
	var caid = document.getElementById("caid").value;
	var mod = type;
	var cont = data + "|" + dat1;
	location.href = "chose_grid?type=1&paid=" + paid + "&keyw=" + keyw
			+ "&caid=" + caid + "&mod=" + mod + "&cont=" + cont;
}

// 通过层登录
function loginByLayer() {
	var username = $("#username").val();
	var passwd = $("#passwd").val();
	if (username == "" || username == "用户名/手机/邮箱") {
		$("#usernametip").text("请输入用户名");
	} else {
		$("#usernametip").text("");
		if (passwd.length < 6) {
			$("#passwdtip").text("密码长度至少6位");
		} else {
			$("#passwdtip").text("");
			// $("#loginByLayerForm").submit();
			$.ajaxSetup({
				async : false
			});
			$.post("login_loginByLayer", {
				"userDTO.UUserid" : username,
				"userDTO.UPassword" : passwd
			}, function(data) {
				if (data == "success") {
					// alert("登录成功");
					var url = $("#nexturl").val();
					window.location.href = url;
				} else if (data == "error") {
					$("#msg").text("用户名或者密码错误");
				} else if (data == "mailnotactive") {
					$("#msg").text("邮箱没有激活");
				} else {
					$("#msg").text("非法登录");
				}
			});
		}
	}
}

// 关闭窗口
function close() {
	$("#myModal").hide();
}

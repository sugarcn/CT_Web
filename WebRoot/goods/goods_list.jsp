﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="frm" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<base href="<%=basePath%>">

<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link rel="stylesheet" type="text/css" href="css/jquery.toolbar.css">
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/jquery.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/jquery-1.11.3.js"></script>
	
<script type="text/javascript" src="<%=request.getContextPath()%>/goods/js/goods_list.js" ></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/goods/js/newgoods_price.js" ></script>
</head>
 <body id="goodsListBody" oncontextmenu="return false" ondragstart="return false" onselectstart ="return false" onselect="document.selection.empty()" oncopy="document.selection.empty()" onbeforecopy="return false" onmouseup="document.selection.empty()"> 
<!--<body id="goodsListBody">-->
<div class="load_g">
	<div class="load_content">
		<p id="goodsName_Res"></p>
		<span>X</span>
		<ul id="ressList">
		</ul>
	</div>
</div>
<div id="popDivall" class="mydivall" style="display:none;">
<div class="con_t"><a href="javascript:closecharDiv()"><img src="<%=imgurl %>/bg/chadiao.gif"></a><span>让客户经理联系我</span></div>
<div class="con_b">
	<ul>
    <li><p>您的支持是我们前进的动力！请留下您的联系方式及问题详情，以便客户经理及时与您联系，谢谢支持！</p><br/></li>
	<li id="ts" style="color:red"></li>
	<c:if test="${ctUser== null }"> 
	<li><span>邮箱：</span><input name="con" type="text" id="email"/></li>
    <li><span>手机：</span><input name="con" type="text" id="mbo"/></li>
	</c:if>
	<c:if test="${ctUser!= null }"> 	 
			<c:if test="${ctUser.UEmail == null }">
    		<li><span>邮箱：</span><input name="con" type="text" id="email"/></li>
    		</c:if>
    		<c:if test="${ctUser.UEmail != null }">
    		<li><span>邮箱：</span><input name="con" type="text" id="email" value="${ctUser.UEmail}" readonly="readonly"/></li>
    		</c:if>
    		<c:if test="${ctUser.UMb == null }">
    		<li><span>手机：</span><input name="con" type="text" id="mbo"/></li>
    		</c:if>
   		    <c:if test="${ctUser.UMb != null }">
    		<li><span>手机：</span><input name="con" type="text" id="mbo" value="${ctUser.UMb}" readonly="readonly"/></li>
    		</c:if>
	</c:if>
	<li><span>详细：</span><textarea name="con" id="detail"></textarea></li>
    <li><div class="msg-up" style="display: none;"></div></li>
    <li><a href="javascript:contacts()">提交</a></li>
    </ul>
</div>
</div>
<div id="bgallfk" class="bgallfk" style="display:none;"></div>

	<jsp:include page="../common/head.jsp"></jsp:include>
	<jsp:include page="../common/subhead.jsp"></jsp:include>
<div class="BMchuangshowgw" style=" z-index:888; display:none" >
<div class="lchashow"><span><a href="javascript:goBack();"><img src="<%=imgurl %>/bg/chadiao.gif" /></a></span></div>
    <div class="tizp">
        <div class="biao_tishi">成功加入<a href="/" target="_blank">我的购物车</a></div>
	<div class="biao_tuijian">
<dl>
<dt><a href="javascript:;" onclick="goBack();">继续购物</a></dt>
<dd><a href="javascript:;" onclick="toCart();">去购物车结算</a></dd>
</dl>       
	</div>
    
    </div>
</div>
	<div class="contant_d">

		<div class="dizhilian_d">
			<a href="<%=basePath%>" target="_blank">首页</a>><span>[<a href="<%=basePath%>goods_search?keyword=${typeName }">${typeName }</a>]</span>
		</div>
		<div class="tuijian_d">
			<ul class="left_biao_d">
				<li>推荐商品</li>
			</ul>
			<dl class="left_lie_d">
				<dt>
					<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
					</a>
				</dt>
				<dd>
					<a href="/" target="_blank">0805F(1%)59K贴片电阻</a>
				</dd>
				<dd>
					销量：<span>823012</span>K
				</dd>
				<dd class="jiage_d">¥5.85/K</dd>
			</dl>
			<dl class="left_lie_d">
				<dt>
					<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
					</a>
				</dt>
				<dd>
					<a href="/" target="_blank">0805F(1%)6.2M贴片电阻</a>
				</dd>
				<dd>
					销量：<span>413534</span>K
				</dd>
				<dd class="jiage_d">¥7.02/K</dd>
			</dl>
			<dl class="left_lie_d">
				<dt>
					<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
					</a>
				</dt>
				<dd>
					<a href="/" target="_blank">4D02J(5%)820R贴片排阻</a>
				</dd>
				<dd>
					销量：<span>408372</span>K
				</dd>
				<dd class="jiage_d">¥11.44/K</dd>
			</dl>
			<dl class="left_lie_d">
				<dt>
					<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
					</a>
				</dt>
				<dd>
					<a href="/" target="_blank">1812F(1%)0.62R贴片电阻</a>
				</dd>
				<dd>
					销量：<span>358532</span>K
				</dd>
				<dd>¥175.5/K</dd>
			</dl>
			<dl class="left_lie_d">
				<dt>
					<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
					</a>
				</dt>
				<dd>
					<a href="/" target="_blank">1210F(1%)2.87R贴片电阻</a>
				</dd>
				<dd>
					销量：<span>311565</span>K
				</dd>
				<dd class="jiage_d">¥42.9/K</dd>
			</dl>

		</div>
		<form action="" name="checkSearch" id="checkSearch" method="post">
			<input name="fgoodsDTO.keyword" type="hidden" id="checkFromKeyWord" />
			<input name="cname" value="" type="hidden" id="checkFromCname" />
			<input name="bname" value="" type="hidden" id="checkFromBname" />
			<input name="page" id="goodslistpageindex" value="1" type="hidden" />
			<input value="${tempSearch }" id="tempSearch" type="hidden" />
			<div class="lieright_d">
				<ul class="lie_lian_d">
					<li class="fan_cm"><a href="javascript:showcharDiv()">让客户经理联系我</a>
					</li>
					<!-- <li class="fan_ctl">您搜索关键词 " ${tkword }" 共 ${pages.totalCount }条数据！</li> -->
					<li class="fan_ctl">您搜索关键词 " ${typeName }" 共 ${pages.totalCount }条数据！</li>
				</ul>
					<input type="hidden" id="type" name="type" value="${type }" />
					<input type="hidden" id="keyw" name="keyw" value="${typeName }" />
					<input type="hidden" id="keyw1" name="keyw1" value="${keyword }" />
					<input type="hidden" id="paid" name="paid" value="${paid }" />
					<input type="hidden" id="caid" name="caid" value="${caid }" />
					<input type="hidden" id="brid" name="brid" value="${brid }" />
					<input type="hidden" id="mod" name="mod" value="${mode }" />
					<input type="hidden" id="cont" name="cont" value="${cont }" />
					<input type="hidden" id="kword" name="kword" value="${kword }" />
					<input type="hidden" id="pagetkword" name="pagetkword" value="${tkword }" />
					
				<script type="text/javascript">
					$(function(){
						$("#findPriceGroup").click(function(){
							var $jiage1 = $("#jiage1").val();
							var $jiage2 = $("#jiage2").val();
							var keyw = $("#keyw1").val();
							var kword = $("#kword").val();
							/*
							if($jiage1.length == 0){
								alert("必须输入价格");
								return;
							}
							if($jiage2.length == 0){
								alert("必须输入价格");
								return;
							}
							*/
							location.href="${pageContext.request.contextPath}/goods_search?fgoodsDTO.price1="+ $jiage1 +"&fgoodsDTO.price2=" + $jiage2 + "&keyword="+keyw;
						});
					});
					
				</script>
				<script type="text/javascript">
						
					</script>
				<div class="chooes_lie_d">
						<c:if test="${goodsBNameStr != '[]' }">
							<div class="lieother">
								<dl>
								<dt>品牌：</dt>
								<dd><c:forEach items="${goodsBNameStr }" var="bname">
									<c:if test="${tempName[1] == bname || tempName[0] == bname }">
										 <div class="choice-con hide" data-bname="${bname }" id="bnametype" info="ip" style="display: block;">
											<span class="con-104 green12"  onclick="checkSearch22('${typeName }','${bname }');" id="bnametype" info="input_text2">${bname }</span>
											<span style="cursor:hand" class="button"  id="chachaBname" data-value="${typeName }"> </span>
										</div>
										<!-- <a onclick="checkSearch22('${typeName }','${bname }');" id="bnametype" href="javascript:;">${bname }</a>
										<a href="javascript:;" onclick="searchFromClose('${typeName }')">X</a> -->
									</c:if>
									<c:if test="${tempName[1] != bname && tempName[0] != bname  }">
										<a onclick="checkSearch22('${typeName }','${bname }');" href="javascript:;">${bname }</a>
									</c:if>
								</c:forEach></dd>
								</dl>
								<dl class="noneline">
								<dt>类别：</dt>
								<dd>
								<c:forEach items="${goodsCNameStr }" var="cname">
									<c:if test="${tempName[0] == cname || tempName[1] == cname }">
										<div class="choice-con hide" data-cname="${cname }" id="cnametype" info="ip" style="display: block;">
											<span class="con-104 green12" onclick="checkSearch11('${typeName }','${cname }');" id="cnametype" info="input_text2">${cname }</span>
											<span class="button" id="chachaCname" data-value="${typeName }"> </span>
										</div>
										 <a onclick="checkSearch11('${typeName }','${cname }');" style="display: none;" id="cnametype" href="javascript:;">${cname }</a>
										<a href="javascript:;" style="display: none;"  onclick="searchFromClose('${typeName }')">X</a> 
									</c:if>
									<c:if test="${tempName[0] != cname && tempName[1] != cname }">
										<a onclick="checkSearch11('${typeName }','${cname }');" href="javascript:;">${cname }</a>
										<!-- <a href="www.baidu.com">X</a> -->
									</c:if>
								</c:forEach></dd>
								</dl>
							</div>
						</c:if>
					<div class="shuo" style="display: none;">
					    <h3>
					        所有符合条件的商品<b></b>
					    </h3>
					    <div class="sdiv_1">
					        <p>感谢您对长亭易购的信任</p>
					        <p>1.所有上架现货产品，当天发货。</p>
					        <p>2.库存不足的情况，您可以<a href="#">联系客服</a>。</p>
					        <p>3.商城没有的产品，我们回您提供代购服务。 </p>
					    </div>
					    <div class="sdiv_2"><p>长亭易购是<span>厚声</span>一级代理商，<span>风华</span>全系列代理商，确保每一颗物料都来自原厂。</p></div>
					    <div class="sdiv_3"><p>没有找到符合条件的商品，请重新输入或<a href="#">联系客户咨询</a>。</p></div>
					</div>
					<div class="xiangmu_tiao">
						<ul>
							<li class="fan_d"><a href="/" target="_blank"
									 class="hover"
							>综合排序</a><span>
							<!-- <a href="<%=request.getContextPath() %>/goods_search?keyw=${typeName }&kword=price"
									>价格</a><input name="jiage1"  value="${fgoodsDTO.price1 }" id="jiage1" type="text" />-<input id="jiage2" value="${fgoodsDTO.price2 }" name="jiage2" type="text" />
							</span><a href="javascript:void();" id="findPriceGroup" onClick="" class="queding_lie_d">确定</a> -->
							</p>
							</li>
							<li class="fanright_d"><s:if test="#request.pages.currentPage==1">
									<a href="javascript:;">&lt;</a>
									<s:if test="#request.pages.totalPage==0 ">
										<span><i>0</i>
									</s:if>
									<s:else>
										<span><i>${pages.currentPage}</i>
									</s:else>
								</s:if> <s:else>
									<a href="javascript:upAndDownPage(${pages.currentPage-1})">&lt;</a>
									<span><i>${pages.currentPage}</i>
								</s:else><s:if
									test="#request.pages.currentPage==#request.pages.totalPage || #request.pages.totalPage==0">
									/ <i>${pages.totalPage}</i></span> 
									<a href="javascript:;">&gt;</a>
								</s:if> <s:else>
									/ <i>${pages.totalPage}</i></span> 
									<a href="javascript:upAndDownPage(${pages.currentPage+1})">&gt;</a>
								</s:else>
								<input type="hidden" value="${pages.currentPage }" />
								<input type="hidden" value="${pages.totalPage }" />
							</li>
						</ul>
					</div>
					<div class="pir_right">
					<!--
					<div class="ct_yp_dj"><span class="yp_tit">商品</span><span class="yp_dj">样片单价<i>（PCS）</i></span><span class="pl_dj"><span class="pl_dj_t">批量单价</span>
					<i>（</i><span id="k" onclick="tabDanWeiKList(this);" style="width: 27px;" class="pl_k hover">K</span><i>/</i><span id="pan" onclick="tabDanWeiPanList(this);" class="pl_p">包装单位</span><i>）</i></span></div>
					-->
						<div id="con_xmlistb_1" class="con_all">
						
						<s:iterator value="searchList" status="se">
						<input type="hidden" value="<s:property value="searchList[#se.index][0]"/>" name="gidForHidPar" />
						<dl class="left_lie_d_c">
						<dt class="photo">
						    <a target="_blank" href="<%=request.getContextPath()%>/goods_detail?gid=<s:property value="searchList[#se.index][0]" />" title="<s:property value="searchList[#se.index][1]"/>">
						<s:if test='searchList[#se.index][16]!="nourl"'>
										<img id="<s:property value="searchList[#se.index][0]"/>_goodsImgs" src="<s:property value="searchList[#se.index][16]"/>">
										</s:if>
										<s:else>
										<img id="<s:property value="searchList[#se.index][0]"/>_goodsImgs" src="<%=imgurl %>/ct_s.gif">
										</s:else>
						</a>
						    <div class="bigphoto">
						        <b></b>
						        <img src="<%=imgurl %>/ct_s.gif">
						    </div>
						</dt>
						<dd class="san_bgJIa_d">
						    <ul>
						    <li class="ct_n_h"><a  target="_blank" href="<%=request.getContextPath()%>/goods_detail?gid=<s:property value="searchList[#se.index][0]"/>" title="<s:property value="searchList[#se.index][1]"/>"><s:property value="searchList[#se.index][1]"/></a>
						    <s:if test="searchList[#se.index][11]==1 && searchList[#se.index][12]==0">
						    <img src="<%=imgurl %>/zcypq.gif" />
						    </s:if>
						   
						    </li>						    
						    <li>商品货号：<s:property value="searchList[#se.index][2]"/></li>
						    <li class="brand">品牌：<s:property value="searchList[#se.index][6]"/><span></span></li>
						    <li>封装/规格：<s:property value="searchList[#se.index][13]"/>
						    </li>
						    <li><a id="ziyuan_<s:property value="searchList[#se.index][0]"/>" class="guigeshu" name="<s:property value="searchList[#se.index][1]"/>"><b></b>下载规格书</a></li>
						    </ul>
						</dd>
						<dd class="san_bgJIa_d san_bgJIa_b "
						<c:if test="${searchList[se.index][24] == 'no' }">
							<c:if test="${searchList[se.index][24] == 'no' && searchList[se.index][23] == 'no' }">
								style="padding-top: 41px;position: absolute; margin-left: 100px;width:205px;"
							</c:if>
							<c:if test="${searchList[se.index][24] != 'no' || searchList[se.index][23] != 'no' }">
									style="padding-top: 26px;"
							</c:if>
						</c:if>	
						
						>
						<c:if test="${searchList[se.index][24] == 'no' && searchList[se.index][23] == 'no' }">
							<span>暂无价格，如需购买，请点击<a target="_blank" href='tencent://message/?uin=2881112904&Site=qq&Menu=yes'>我要订货</a></span>
						</c:if>
						    <ul class="p_ul"
						    <c:if test="${searchList[se.index][24] == 'no' }">
									 style="display: none;"
								</c:if>	
						    >
						    <!--<li class="first_li">销售单价：<span>含增值税</span></li>  -->
						    <s:iterator value="priceAllList" var="allList">
						    	<s:iterator value="#allList" var="list" status="stauts">
						    		<s:if test="searchList[#se.index][0] == #list.GId">
						    		<input type="hidden" name="<s:property value="searchList[#se.index][0]"/>gidSim" value="${list.simIncrease }"/>
						    		<input type="hidden" name="<s:property value="searchList[#se.index][0]"/>gidENum" value="${list.simENum }"/>
						    		<input type="hidden" name="<s:property value="searchList[#se.index][0]"/>gidSNum" value="${list.simSNum }"/>
						    		<input type="hidden" name="<s:property value="searchList[#se.index][0]"/>gidSPrice" value="${list.simRPrice }"/>
						    			<c:if test="${list.simSNum != '0' && list.simSNum != null }">
								    		<li name="<s:property value="searchList[#se.index][0]"/><s:property value="#stauts.index"/>isPriceHover"><span class="Ia_qus"><i>${list.simSNum }-<c:if test="${list.simENum != 0 }">${list.simENum }</c:if>（PCS）：</i></span><i class="Ia_jia">￥${list.simRPrice }</i></li>
						    			</c:if>
						    		</s:if>
						    	</s:iterator>
						    </s:iterator>
						    <input type="hidden" name="allGids" value="<s:property value="searchList[#se.index][0]"/>" />
						   </ul>
						    
							<!--批量单价 -->
							<ul style="padding-top:13px;"
							<c:if test="${searchList[se.index][23] == 'no' }">
									 style="display: none;"
							</c:if>	
							>
							<input type="hidden" name="<s:property value="searchList[#se.index][0]"/>yuliuAdd" />
						    <s:iterator value="priceAllList" var="allList">
						    	<s:iterator value="#allList" var="list" status="stauts">
						    		<s:if test="searchList[#se.index][0] == #list.GId">
						    			<c:if test="${list.parSNum != '0' && list.parSNum != null }">
						    			<input type="hidden" name="<s:property value="searchList[#se.index][0]"/>gidPar" value="${list.parIncrease}"/>
						    			<input type="hidden" name="<s:property value="searchList[#se.index][0]"/>gidPar1" value="${list.parIncrease }"/>
						    			<input type="hidden" name="<s:property value="searchList[#se.index][0]"/>gidParENum" value="${list.parENum }"/>
						    			<input type="hidden" name="<s:property value="searchList[#se.index][0]"/>gidParSNum" value="${list.parSNum }"/>
						    			<input type="hidden" name="<s:property value="searchList[#se.index][0]"/>gidParPrice" value="${list.parRPrice * 1000}"/>
								    		<li name="<s:property value="searchList[#se.index][0]"/><s:property value="#stauts.index"/>isPriceHoverPar"><span class="Ia_qus"><i> &ge; ${list.parSNum } <c:if test="${list.parENum != '0' && list.parENum != null }">${list.parENum }</c:if>（ KPCS ）：</i></span><i class="Ia_jia">￥<frm:formatNumber pattern="0.00">${list.parRPrice * 1000 }</frm:formatNumber></i></li>
						    			</c:if>
							    		<input type="hidden" name="<s:property value="searchList[#se.index][0]"/>tabType" value="<s:property value="searchList[#se.index][8]"/>" />
						    		</s:if>
						    	</s:iterator>
						    </s:iterator>
							<!--批量单价 -->
							
							
							
						    </ul>
						</dd>
						<dd class="san_bgJIa_d san_bgJIa_n ">
						    <ul>
						    	<!--样品数量 -->
						    	                              <s:set name="isSimSum" value='<s:property value="searchList[#se.index][1]"/>'/>
							<li class="jiage_d" data-ss="${searchList[se.index][24] }"
							<c:if test="${searchList[se.index][24] == 'no' }">
									 style="display: none;"
								</c:if>
							>
							
							<span class="jiajian_d">
						    	<c:if test="${searchList[se.index][24] == 'yes' }">
											<a href="javascript:subnum(<s:property value="searchList[#se.index][0]"/>)"  >-</a>
											<!-- <input onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" id="<s:property value="searchList[#se.index][0]"/>" maxlength="4" onblur="isSimMin(<s:property value="searchList[#se.index][0]"/>);" type="text" value="0" /> -->
											<input onkeyup="this.value=this.value.replace(/\D/g,'')" onfocus="javascript:if(this.value == 0){this.value=''};" value="0"  onafterpaste="this.value=this.value.replace(/\D/g,'')" id="<s:property value="searchList[#se.index][0]"/>" maxlength="4" onblur="listsimAjax(<s:property value="searchList[#se.index][0]"/>,this.value);" type="text" />
											<a href="javascript:addnum(<s:property value="searchList[#se.index][0]"/>)" class="righter">+</a>样品
											<!-- isSimMin(<s:property value="searchList[#se.index][0]"/>); -->
						    	</c:if>
						    	<c:if test="${searchList[se.index][24] == 'no' }">
											<a href="javascript:;"  >-</a>
											<!-- <input onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" id="<s:property value="searchList[#se.index][0]"/>" maxlength="4" onblur="isSimMin(<s:property value="searchList[#se.index][0]"/>);" type="text" value="0" /> -->
											<input id="<s:property value="searchList[#se.index][0]"/>"  />
											<a href="javascript:;" class="righter">+</a>样品
											<!-- isSimMin(<s:property value="searchList[#se.index][0]"/>); -->
						    	</c:if>
							</span>
							</li>
							<!--样品数量 -->
							<!--批量数量 -->
						    <li class="jiage_d" 
						     <c:if test="${searchList[se.index][23] == 'no' }">
									style="padding-top: 33px;"
								</c:if>	
						    >
						    <c:if test="${searchList[se.index][23] == 'yes' }">
							    <span class="jiajian_d"
							    <c:if test="${searchList[se.index][24] == 'no' }">
							    	 style="margin-top:12px;"
							    </c:if>
							    >
							    	<a href="javascript:subParnum(<s:property value="searchList[#se.index][0]"/>)" >-</a>
									<!-- <input onkeyup="this.value=this.value.replace(/[-+~!@#$%^&*()_+`~！@￥%……&*（）——+=-|、】【；’“：，。、？》《]/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" id="<s:property value="searchList[#se.index][0]"/>num" maxlength="4" onblur="isParMin(<s:property value="searchList[#se.index][0]"/>);" type="text" value="0" /> -->
									<input onfocus="javascript:if(this.value == 0){this.value=''};" onkeyup="this.value=this.value.replace(/[-+~!@#$%^&*()_+`~！@￥%……&*（）——+=-|、】【；’“：，。、？》《]/g,'')" onafterpaste="this.value=this.value.replace(/[-+~!@#$%^&*()_+`~！@￥%……&*（）——+=-|、】【；’“：，。、？》《]/g,'')" id="<s:property value="searchList[#se.index][0]"/>num" maxlength="4" onblur="listparAjax(<s:property value="searchList[#se.index][0]"/>,this.value);" type="text" value="0" />
									<a href="javascript:addParnum(<s:property value="searchList[#se.index][0]"/>)" class="righter">+</a>批量
								</span>
						    	
						    </c:if>
							</li>
							<!--批量数量 -->
						    </ul>
						</dd>
						<input type="hidden" name="<s:property value="searchList[#se.index][0]"/>goodsSimPriceCount" />
						<input type="hidden" name="<s:property value="searchList[#se.index][0]"/>goodsParPriceCount" />
						<input type="hidden" id="<s:property value="searchList[#se.index][0]"/>newPriceSim" />
						<input type="hidden" id="<s:property value="searchList[#se.index][0]"/>newPricePar" />
						<c:if test="${searchList[se.index][24] == 'yes' || searchList[se.index][23] == 'yes' }">
							<dd class="san_bgJIa_djia">
							    <ul>
							    <input type="hidden" value="<s:property value="searchList[#se.index][10]"/>" id="<s:property value="searchList[#se.index][0]"/>_kucun" />
							    <li>现货库存：<i><s:property value="searchList[#se.index][10]"/></i>K</li>
							    <li>1<s:property value="searchList[#se.index][8]"/><i><s:property value="searchList[#se.index][9]"/></i>片</li>
							    <input type="hidden" id="<s:property value="searchList[#se.index][0]"/>pack1num" value="<s:property value="searchList[#se.index][9]"/>" />
							    <li class="ct_cgsl_list">采购总数量：
							    <!-- <input onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" name="<s:property value="searchList[#se.index][0]"/>countSum" maxlength="7" onblur="getSumCount(<s:property value="searchList[#se.index][0]"/>)" id="<s:property value="searchList[#se.index][0]"/>num" type="text" value="1" /> -->
							    <input onfocus="javascript:if(this.value == 0){this.value=''};" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" name="<s:property value="searchList[#se.index][0]"/>countSum" maxlength="7" onblur="listAllCountAjax(<s:property value="searchList[#se.index][0]"/>,this.value)" id="<s:property value="searchList[#se.index][0]"/>num" type="text" value="1" />
							    片</li>
							      
							    <li>
	<!-- <span class="jiarugou_d"><a href="javascript:addcart(<s:property value="searchList[#se.index][0]"/>)">加入购物车</a></span> -->
	<span class="jiarugou_d"><a 
		<s:if test="searchList[#se.index][10] == 0">
				target="_blank" href='tencent://message/?uin=2881112904&Site=qq&Menu=yes'> 我要订货</a>
		</s:if>
		<s:else>
		href="javascript:void(0);" value="<s:property value="searchList[#se.index][0]"/>" 	onclick="addCartNew(<s:property value="searchList[#se.index][0]"/>)" id="addCart">
			加入购物车</a>
		</s:else>
		
	</span>
								</li>
							    </ul>
							</dd>
						</c:if>
						<c:if test="${searchList[se.index][24] == 'no' && searchList[se.index][23] == 'no' }">
							<dd class="san_bgJIa_djia" style="float:right;">
							     <ul>
							    <input type="hidden" value="<s:property value="searchList[#se.index][10]"/>" id="<s:property value="searchList[#se.index][0]"/>_kucun" />
							    <li>现货库存：<i>0</i>K</li>
							    <li>1<s:property value="searchList[#se.index][8]"/><i><s:property value="searchList[#se.index][9]"/></i>片</li>
							    <input type="hidden" id="<s:property value="searchList[#se.index][0]"/>pack1num" value="<s:property value="searchList[#se.index][9]"/>" />
							    <li style="margin-top:15px;">
	<!-- <span class="jiarugou_d"><a href="javascript:addcart(<s:property value="searchList[#se.index][0]"/>)">加入购物车</a></span> -->
	<span class="jiarugou_d"><a target="_blank" href='tencent://message/?uin=2881112904&Site=qq&Menu=yes'>
	 我要订货</a></a>
	</span>
								</li>
							    </ul>
							</dd>
						</c:if>
						</dl>
						</s:iterator>
							
							
							
							<script type="text/javascript">
							//下载规格书
							$(".guigeshu").click(function(){
								$("#goodsName_Res").text(this.name);
								var ziyuan = this.id.split("_");
								
								$.post(
									"findResultByGid",
									{"GId":ziyuan[1]},
									function(data, varstart){
										if(data == "noress"){
											$("#ressList").html("");
											$("#ressList").html("暂无规格书");
										} else {
											$("#ressList").html("");
											$("#ressList").html(data);
										}
									}
								);
								$(".load_g").show();
								var height=parseFloat($(".load_g .load_content").css("height"));
								$(".load_g .load_content").css("margin-top",-height/2+"px");
							});
							$(".load_g .load_content>span").click(function(){
								$(".load_g").hide();
							});
						    //提示输入
							
							
							function tabDanWeiKList(yuan){
								if(isPanOrKList){
									isPanOrKList = false;
									$(yuan).removeAttr("class");
									$(yuan).attr("class","pl_k hover");
									$("#pan").attr("class","pl_p");
									var gids = $("input[name=gidForHidPar]");
									for(var i = 0; i < gids.length; i++){
										var baozhuang = $("#"+gids[i].value+"pack1num").val();
										//alert(baozhuang);
										//alert(incPar[0].value);
										var incPar = $("input[name="+gids[i].value+"gidPar1]");
										var goodParStaSum =  $("input[name="+gids[i].value+"gidParSNum]");
										var goodParEndSum =  $("input[name="+gids[i].value+"gidParENum]");
										var gidParPrice =  $("input[name="+gids[i].value+"gidParPrice]");
										var inc = $("input[name="+gids[i].value+"gidPar]");
										for(var j = goodParStaSum.length - 1; j >= 0; j--){
											$("li[name="+gids[i].value+j+"isPriceHoverPar]").remove();
											var start = Number(goodParStaSum[j].value*Number(baozhuang/1000));
											var end = Number(goodParEndSum[j].value*Number(baozhuang/1000));
											var price = Number(gidParPrice[j].value)/Number(baozhuang/1000);
											var priceNew = Math.round(price*10000)/10000;
											var incs = Number(inc[j].value)*Number(baozhuang/1000);
											var str="";
											var priceParPar = Number(Number(gidParPrice[j].value));
											priceParPar = Math.round(priceParPar*10000)/10000;
											priceParPar = priceParPar.toFixed(2);
											priceNew = priceNew.toFixed(2);
											if(end == 0){
												str = "<li name='"+gids[i].value+j+"isPriceHoverPar' ><span class='Ia_qus'><i>"+start+"-（ K ）：</i></span><i class='Ia_jia'>￥"+priceParPar+"</i></li>";
											} else {
												str = "<li name='"+gids[i].value+j+"isPriceHoverPar' ><span class='Ia_qus'><i>"+start+"-"+end+"（ K ）：</i></span><i class='Ia_jia'>￥"+priceParPar+"</i></li>";
											}
											$("input[name="+gids[i].value+"yuliuAdd]").after(str);
											$("input[name="+gids[i].value+"gidParSNum]")[j].value=start;
											$("input[name="+gids[i].value+"gidParENum]")[j].value=end;
											//$("input[name="+gids[i].value+"gidPar]")[j].value=incs;
										}
										var goodsNum = $("#"+gids[i].value+"num").val();
										if(goodsNum != 0){
											$("#"+gids[i].value+"num").val(Number(goodsNum*Number(baozhuang/1000)));
										}
										loadPeiceNew(gids[i].value);
									}
									
								}
							}
							function tabDanWeiPanList(yuan, typeTab){
								if(!isPanOrKList){
									isPanOrKList = true;
									$(yuan).removeAttr("class");
									$(yuan).attr("class","pl_k hover");
									$("#k").attr("class","pl_p");
									
									var gids = $("input[name=gidForHidPar]");
									for(var i = 0; i < gids.length; i++){
										var baozhuang = $("#"+gids[i].value+"pack1num").val();
										//alert(baozhuang);
										var goodParStaSum =  $("input[name="+gids[i].value+"gidParSNum]");
										var goodParEndSum =  $("input[name="+gids[i].value+"gidParENum]");
										var gidParPrice =  $("input[name="+gids[i].value+"gidParPrice]");
										var inc = $("input[name="+gids[i].value+"gidPar]");
										var incPar = $("input[name="+gids[i].value+"gidPar1]");
										//alert(incPar[0].value);
										for(var j = goodParStaSum.length - 1; j >= 0; j--){
											$("li[name="+gids[i].value+j+"isPriceHoverPar]").remove();
											var start = Number(goodParStaSum[j].value)/Number(baozhuang/1000);
											var end = Number(goodParEndSum[j].value)/Number(baozhuang/1000);
											var price = Number(gidParPrice[j].value)*Number(baozhuang/1000);
											var priceNew = Math.round(price*10000)/10000;
											var incs = Number(inc[j].value)/Number(baozhuang/1000);
											var str="";
											//priceNew = priceNew*1000;
											priceNew = priceNew.toFixed(2);
											var type=$("input[name="+gids[i].value+"tabType]").val();
											if(end  == 0){
												str = "<li name='"+gids[i].value+j+"isPriceHoverPar' ><span class='Ia_qus'><i>"+start+"-（ "+type+" ）：</i></span><i class='Ia_jia'>￥"+priceNew+"</i></li>";
											} else {
												str = "<li name='"+gids[i].value+j+"isPriceHoverPar' ><span class='Ia_qus'><i>"+start+"-"+end+"（ "+type+" ）：</i></span><i class='Ia_jia'>￥"+priceNew+"</i></li>";
											}
											$("input[name="+gids[i].value+"yuliuAdd]").after(str);
											$("input[name="+gids[i].value+"gidParSNum]")[j].value=start;
											$("input[name="+gids[i].value+"gidParENum]")[j].value=end;
											//$("input[name="+gids[i].value+"gidPar]")[j].value=incs;
										}
										var goodsNum = $("#"+gids[i].value+"num").val();
										if(goodsNum != 0){
											$("#"+gids[i].value+"num").val(Number(goodsNum/Number(baozhuang/1000)));
										}
										var newNumber = $("#"+gids[i].value+"num").val();
										if(newNumber != 0){
											loadPeiceNew(gids[i].value);
										}
									}
									
								}
							}
							function loadAllGids(){
								var gids = $("input[name=allGids]");
								for(var i=0;i<gids.length;i++){
									loadPeiceNew(gids[i].value);
								}
							}
							loadAllGids();
							function getSumCount(gid){
								var countSum = $("input[name="+gid+"countSum]");
								var simCount = $("#"+gid).val();
								var parCount = $("#"+gid + "num").val();
								var incPar = $("input[name="+gid+"gidPar1]");
								if(countSum[0].value.toString().indexOf(".")>0){
									var chaii = countSum[0].value.toString().split(".");
									countSum[0].value = chaii[0];
								}
								if(countSum[0].value >= (Number(incPar[0].value)*1000)){
									parCount = Number(countSum[0].value) / 1000;
									
									if(parCount.toString().indexOf(".")>0){
										var chai = parCount.toString().split(".");
										var newXiao = Number(parCount) / Number(incPar[0].value);
										if(newXiao.toString().indexOf(".") > 0){
											var chaiNew = newXiao.toString().split(".");
											parCount = Number(chaiNew[0]) * Number(incPar[0].value);
											chaiNew[1] = "0."+chaiNew[1];
											simCount = (Number(chaiNew[1])*Number(incPar[0].value))*1000;
										}
										
										
									//chai[1] = "0." + chai[1];
									//var parCountEnd = chai[0].toString().substring(chai[0].length-1, chai[0].length);
									//if(Number(incPar[0].value) != 1){
									//	if(Number(parCountEnd) != Number(incPar[0].value)){
									//		if(Number(parCountEnd) > Number(incPar[0].value)){
									//			parCountEnd = Number(parCountEnd) - Number(incPar[0].value);
									//		}
									//		simCount = Number(parCountEnd) * 1000;
									//		parCount = parCount - parCountEnd;
									//		parCount = Math.round(parCount * 100)/100;
									//		if(parCount.toString().indexOf(".")>0){
									//			var cha = parCount.toString().split(".");
									//			cha[1] = "0." + cha[1];
									//			if(simCount<=(Number(incPar[0].value)*1000)){
									//				simCount=Number(simCount) + Number(cha[1])*1000;
									//			}
									//			parCount = cha[0];
									//		}
									//	} else {
									//		if(simCount<=(Number(incPar[0].value)*1000)){
									//			simCount=Number(chai[1])*1000;
									//		}
									//		parCount = chai[0];
									//	}
									//} else {
									//	parCount = chai[0];
									//}
									} else {
										var isBei = parCount / Number(incPar[0].value);
										if(isBei.toString().indexOf(".") > 0){
											var parCountEndTwo = parCount.toString().substring(parCount.toString().length-1, parCount.toString().length);
											if(Number(parCountEndTwo) != Number(incPar[0].value)){
												if(Number(parCountEndTwo) > Number(incPar[0].value)){
													parCountEndTwo = Number(parCountEndTwo) - Number(incPar[0].value);
												}
												simCount = Number(parCountEndTwo) * 1000;
												parCount = parCount - parCountEndTwo;
												if(parCount.toString().indexOf(".")>0){
													var cha = parCount.toString().split(".");
													cha[1] = "0." + cha[1];
													if(simCount<=(Number(incPar[0].value)*1000)){
														simCount=Number(simCount) + Number(cha[1])*1000;
													}
													parCount = cha[0];
												}
											} else {
												if(simCount<=(Number(incPar[0].value)*1000)){
													simCount=Number(chai[1])*1000;
												}
											}
										} else {
											simCount = 0;
										}
									}
								} else {
									simCount = countSum[0].value;
									parCount = 0;
								}
								if(isPanOrKList){
									parCount = parCount / Number(incPar[0].value);
								}
								$("#"+gid).val(simCount);
								$("#"+gid + "num").val(parCount);
								isSimMin(gid);
								isParMin(gid);
								getSumCount1(gid);
							}
							function getSumCount1(gid){
								var countSum = $("input[name="+gid+"countSum]");
								var simCount = $("#"+gid).val();
								var parCount = $("#"+gid + "num").val();
								var incPar = $("input[name="+gid+"gidPar1]");
								if(countSum[0].value.toString().indexOf(".")>0){
									var chaii = countSum[0].value.toString().split(".");
									countSum[0].value = chaii[0];
								}
								if(countSum[0].value >= (Number(incPar[0].value)*1000)){
									parCount = Number(countSum[0].value) / 1000;
									
									if(parCount.toString().indexOf(".")>0){
										var chai = parCount.toString().split(".");
										var newXiao = Number(parCount) / Number(incPar[0].value);
										if(newXiao.toString().indexOf(".") > 0){
											var chaiNew = newXiao.toString().split(".");
											parCount = Number(chaiNew[0]) * Number(incPar[0].value);
											chaiNew[1] = "0."+chaiNew[1];
											simCount = (Number(chaiNew[1])*Number(incPar[0].value))*1000;
										}
										
										
									//chai[1] = "0." + chai[1];
									//var parCountEnd = chai[0].toString().substring(chai[0].length-1, chai[0].length);
									//if(Number(incPar[0].value) != 1){
									//	if(Number(parCountEnd) != Number(incPar[0].value)){
									//		if(Number(parCountEnd) > Number(incPar[0].value)){
									//			parCountEnd = Number(parCountEnd) - Number(incPar[0].value);
									//		}
									//		simCount = Number(parCountEnd) * 1000;
									//		parCount = parCount - parCountEnd;
									//		parCount = Math.round(parCount * 100)/100;
									//		if(parCount.toString().indexOf(".")>0){
									//			var cha = parCount.toString().split(".");
									//			cha[1] = "0." + cha[1];
									//			if(simCount<=(Number(incPar[0].value)*1000)){
									//				simCount=Number(simCount) + Number(cha[1])*1000;
									//			}
									//			parCount = cha[0];
									//		}
									//	} else {
									//		if(simCount<=(Number(incPar[0].value)*1000)){
									//			simCount=Number(chai[1])*1000;
									//		}
									//		parCount = chai[0];
									//	}
									//} else {
									//	parCount = chai[0];
									//}
									} else {
										var isBei = parCount / Number(incPar[0].value);
										if(isBei.toString().indexOf(".") > 0){
											var parCountEndTwo = parCount.toString().substring(parCount.toString().length-1, parCount.toString().length);
											if(Number(parCountEndTwo) != Number(incPar[0].value)){
												if(Number(parCountEndTwo) > Number(incPar[0].value)){
													parCountEndTwo = Number(parCountEndTwo) - Number(incPar[0].value);
												}
												simCount = Number(parCountEndTwo) * 1000;
												parCount = parCount - parCountEndTwo;
												if(parCount.toString().indexOf(".")>0){
													var cha = parCount.toString().split(".");
													cha[1] = "0." + cha[1];
													if(simCount<=(Number(incPar[0].value)*1000)){
														simCount=Number(simCount) + Number(cha[1])*1000;
													}
													parCount = cha[0];
												}
											} else {
												if(simCount<=(Number(incPar[0].value)*1000)){
													simCount=Number(chai[1])*1000;
												}
											}
										} else {
											simCount = 0;
										}
									}
								} else {
									simCount = countSum[0].value;
									parCount = 0;
								}
								if(isPanOrKList){
									parCount = parCount / Number(incPar[0].value);
								}
								$("#"+gid).val(Number(simCount).toFixed(0));
								$("#"+gid + "num").val(Number(parCount).toFixed(0));
							}
							function loadPeiceNew(gid){
								//获取当前样品的数量区间以及当前数量
								var goodsNum = $("#"+gid).val();
								var endSum =  $("input[name="+gid+"gidENum]");
								var gidSNum =  $("input[name="+gid+"gidSNum]");
								var inc = $("input[name="+gid+"gidSim]");
								var incPar = $("input[name="+gid+"gidPar1]");
								var gidSPrice =  $("input[name="+gid+"gidSPrice]");
								//获取当前批量的数量区间以及当前数量
								var goodsParNum = $("#"+gid+"num").val();
								var goodParStaSum =  $("input[name="+gid+"gidParSNum]");
								var goodParEndSum =  $("input[name="+gid+"gidParENum]");
								var gidParPrice =  $("input[name="+gid+"gidParPrice]");
								
								var sumPrice = 0;
								var parPrice = 0;
								for(var i = 0; i < gidSNum.length; i++ ){
									$("li[name="+gid+i+"isPriceHover]").removeClass("hover");
									$("li[name="+gid+i+"isPriceHoverPar]").removeClass("hover");
									if((Number(goodsNum) >= Number(gidSNum[i].value)) && (Number(goodsNum) <= Number(endSum[i].value))){
										
										$("li[name="+gid+ i+"isPriceHover]").addClass("hover");
										
										$("#"+gid+"newPriceSim").val(gidSPrice[i].value);
										sumPrice = goodsNum * gidSPrice[i].value;
									}
									if((typeof(goodParStaSum[i]) != "undefined")&&Number(goodsParNum) >= Number(goodParStaSum[i].value) && Number(goodParEndSum[i].value) == 0){
										$("li[name="+gid+(goodParStaSum.length-1)+"isPriceHoverPar]").addClass("hover");
										if(isPanOrKList){
											var inc = $("input[name="+gid+"gidPar1]").val();
											goodsParNum = goodsParNum * inc;
										}
										parPrice = Number(goodsParNum * Number(gidParPrice[gidParPrice.length-1].value)).toFixed(2);
										if(isPanOrKList){
											var inc = $("input[name="+gid+"gidPar1]").val();
											goodsParNum = goodsParNum / inc;
										}
										$("#"+gid+"newPricePar").val(gidParPrice[gidParPrice.length-1].value);
									} else {
										if((typeof(goodParStaSum[i]) != "undefined")&&(Number(goodsParNum) >= Number(goodParStaSum[i].value)) && (Number(goodsParNum) <= Number(goodParEndSum[i].value))){
											$("li[name="+gid+i+"isPriceHoverPar]").addClass("hover");
											if(isPanOrKList){
												var inc = $("input[name="+gid+"gidPar1]").val();
												goodsParNum = goodsParNum * inc;
											}
											parPrice = Number(goodsParNum * Number(gidParPrice[i].value)).toFixed(2);
											if(isPanOrKList){
												var inc = $("input[name="+gid+"gidPar1]").val();
												goodsParNum = goodsParNum / inc;
											}
											$("#"+gid+"newPricePar").val(gidParPrice[i].value);
										}
									}
								}
								if(goodsNum != "" && (Number(goodsNum) > (Number(incPar[0].value)*1000))){
									var parCount = Number(goodsNum) / 1000;0
									if(parCount.toString().indexOf(".")>0){
										var chai = parCount.toString().split(".");
										chai[1] = "0." + chai[1]
										goodsParNum = chai[0];
										goodsNum=Number(chai[1])*1000;
										} else {
										goodsNum = 0;
										goodsParNum = parCount;
									}
								}
								var numss = Number(goodsParNum);
								if(isPanOrKList){
									numss = Number(goodsParNum) * Number(incPar[0].value);
								}
								
								var countSum = numss*1000 + Number(goodsNum);
								listAllCountAjax(gid, countSum);
								//$("#"+gid).val(goodsNum);
								//$("#"+gid + "num").val(goodsParNum);
								//$("input[name="+gid+"countSum]").val(countSum);
								//$("input[name="+gid+"goodsSimPriceCount]").val(sumPrice);
								//$("input[name="+gid+"goodsParPriceCount]").val(Number(parPrice).toFixed(2));
							}
							</script>

						</div>
						${pages.pageGoodsStr }
						<div id="con_xmlistb_2" class="con_all" style="display:none;">

							<dl class="left_lie_d_c">
								<dt>
									<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
									</a>
								</dt>
								<dd class="san_bgJIa_d">
									<ul>
										<li><a href="/" target="_blank">单向高压二极管</a>
										</li>
										<li class="jiage_d">¥12/K</li>
										<li>商品名称： 6A10 环保</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d">
									<ul>
										<li>品牌： CT（长亭）</li>
										<li>封装/规格: R-6</li>
										<li>现货库存：256 圆盘</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d san_bgbai_d">
									<ul>
										<li>已售出：2205 圆盘多</li>
										<li><span class="jiajian_d"><a href="/"
												target="_blank">-</a><input name="jiage2" type="text" /><a
												href="/" target="_blank" class="righter">+</a>
										</span><span class="jiarugou_d"><a href="/" target="_blank">加入购物车</a>
										</span>
										</li>
									</ul>
								</dd>
							</dl>
							<dl class="left_lie_d_c">
								<dt>
									<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
									</a>
								</dt>
								<dd class="san_bgJIa_d">
									<ul>
										<li><a href="/" target="_blank">美的 格兰仕 微波炉单向高压二极管</a>
										</li>
										<li class="jiage_d">¥1</li>
										<li>商品名称： 6A10 环保</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d">
									<ul>
										<li>品牌： CT（长亭）</li>
										<li>封装/规格: R-6</li>
										<li>现货库存：256 圆盘</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d san_bgbai_d">
									<ul>
										<li>已售出：2205 圆盘多</li>
										<li><span class="jiajian_d"><a href="/"
												target="_blank">-</a><input name="jiage2" type="text" /><a
												href="/" target="_blank" class="righter">+</a>
										</span><span class="jiarugou_d"><a href="/" target="_blank">加入购物车</a>
										</span>
										</li>
									</ul>
								</dd>
							</dl>


							<dl class="left_lie_d_c">
								<dt>
									<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
									</a>
								</dt>
								<dd class="san_bgJIa_d">
									<ul>
										<li><a href="/" target="_blank">美的 格兰仕 微波炉单向高压二极管</a>
										</li>
										<li class="jiage_d">¥1</li>
										<li>商品名称： 6A10 环保</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d">
									<ul>
										<li>品牌： CT（长亭）</li>
										<li>封装/规格: R-6</li>
										<li>现货库存：256 圆盘</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d san_bgbai_d">
									<ul>
										<li>已售出：2205 圆盘多</li>
										<li><span class="jiajian_d"><a href="/"
												target="_blank">-</a><input name="jiage2" type="text" /><a
												href="/" target="_blank" class="righter">+</a>
										</span><span class="jiarugou_d"><a href="/" target="_blank">加入购物车</a>
										</span>
										</li>
									</ul>
								</dd>
							</dl>


							<dl class="left_lie_d_c">
								<dt>
									<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
									</a>
								</dt>
								<dd class="san_bgJIa_d">
									<ul>
										<li><a href="/" target="_blank">美的 格兰仕 微波炉单向高压二极管</a>
										</li>
										<li class="jiage_d">¥1</li>
										<li>商品名称： 6A10 环保</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d">
									<ul>
										<li>品牌： CT（长亭）</li>
										<li>封装/规格: R-6</li>
										<li>现货库存：256 圆盘</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d san_bgbai_d">
									<ul>
										<li>已售出：2205 圆盘多</li>
										<li><span class="jiajian_d"><a href="/"
												target="_blank">-</a><input name="jiage2" type="text" /><a
												href="/" target="_blank" class="righter">+</a>
										</span><span class="jiarugou_d"><a href="/" target="_blank">加入购物车</a>
										</span>
										</li>
									</ul>
								</dd>
							</dl>


							<dl class="left_lie_d_c">
								<dt>
									<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
									</a>
								</dt>
								<dd class="san_bgJIa_d">
									<ul>
										<li><a href="/" target="_blank">美的 格兰仕 微波炉单向高压二极管</a>
										</li>
										<li class="jiage_d">¥1</li>
										<li>商品名称： 6A10 环保</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d">
									<ul>
										<li>品牌： CT（长亭）</li>
										<li>封装/规格: R-6</li>
										<li>现货库存：256 圆盘</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d san_bgbai_d">
									<ul>
										<li>已售出：2205 圆盘多</li>
										<li><span class="jiajian_d"><a href="/"
												target="_blank">-</a><input name="jiage2" type="text" /><a
												href="/" target="_blank" class="righter">+</a>
										</span><span class="jiarugou_d"><a href="/" target="_blank">加入购物车</a>
										</span>
										</li>
									</ul>
								</dd>
							</dl>


							<dl class="left_lie_d_c">
								<dt>
									<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
									</a>
								</dt>
								<dd class="san_bgJIa_d">
									<ul>
										<li><a href="/" target="_blank">美的 格兰仕 微波炉单向高压二极管</a>
										</li>
										<li class="jiage_d">¥1</li>
										<li>商品名称： 6A10 环保</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d">
									<ul>
										<li>品牌： CT（长亭）</li>
										<li>封装/规格: R-6</li>
										<li>现货库存：256 圆盘</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d san_bgbai_d">
									<ul>
										<li>已售出：2205 圆盘多</li>
										<li><span class="jiajian_d"><a href="/"
												target="_blank">-</a><input name="jiage2" type="text" /><a
												href="/" target="_blank" class="righter">+</a>
										</span><span class="jiarugou_d"><a href="/" target="_blank">加入购物车</a>
										</span>
										</li>
									</ul>
								</dd>
							</dl>


							<dl class="left_lie_d_c">
								<dt>
									<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
									</a>
								</dt>
								<dd class="san_bgJIa_d">
									<ul>
										<li><a href="/" target="_blank">美的 格兰仕 微波炉单向高压二极管</a>
										</li>
										<li class="jiage_d">¥1</li>
										<li>商品名称： 6A10 环保</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d">
									<ul>
										<li>品牌： CT（长亭）</li>
										<li>封装/规格: R-6</li>
										<li>现货库存：256 圆盘</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d san_bgbai_d">
									<ul>
										<li>已售出：2205 圆盘多</li>
										<li><span class="jiajian_d"><a href="/"
												target="_blank">-</a><input name="jiage2" type="text" /><a
												href="/" target="_blank" class="righter">+</a>
										</span><span class="jiarugou_d"><a href="/" target="_blank">加入购物车</a>
										</span>
										</li>
									</ul>
								</dd>
							</dl>


							<dl class="left_lie_d_c">
								<dt>
									<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
									</a>
								</dt>
								<dd class="san_bgJIa_d">
									<ul>
										<li><a href="/" target="_blank">美的 格兰仕 微波炉单向高压二极管</a>
										</li>
										<li class="jiage_d">¥1</li>
										<li>商品名称： 6A10 环保</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d">
									<ul>
										<li>品牌： CT（长亭）</li>
										<li>封装/规格: R-6</li>
										<li>现货库存：256 圆盘</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d san_bgbai_d">
									<ul>
										<li>已售出：2205 圆盘多</li>
										<li><span class="jiajian_d"><a href="/"
												target="_blank">-</a><input name="jiage2" type="text" /><a
												href="/" target="_blank" class="righter">+</a>
										</span><span class="jiarugou_d"><a href="/" target="_blank">加入购物车</a>
										</span>
										</li>
									</ul>
								</dd>
							</dl>


							<dl class="left_lie_d_c">
								<dt>
									<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
									</a>
								</dt>
								<dd class="san_bgJIa_d">
									<ul>
										<li><a href="/" target="_blank">美的 格兰仕 微波炉单向高压二极管</a>
										</li>
										<li class="jiage_d">¥1</li>
										<li>商品名称： 6A10 环保</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d">
									<ul>
										<li>品牌： CT（长亭）</li>
										<li>封装/规格: R-6</li>
										<li>现货库存：256 圆盘</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d san_bgbai_d">
									<ul>
										<li>已售出：2205 圆盘多</li>
										<li><span class="jiajian_d"><a href="/"
												target="_blank">-</a><input name="jiage2" type="text" /><a
												href="/" target="_blank" class="righter">+</a>
										</span><span class="jiarugou_d"><a href="/" target="_blank">加入购物车</a>
										</span>
										</li>
									</ul>
								</dd>
							</dl>


							<dl class="left_lie_d_c">
								<dt>
									<a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif">
									</a>
								</dt>
								<dd class="san_bgJIa_d">
									<ul>
										<li><a href="/" target="_blank">美的 格兰仕 微波炉单向高压二极管</a>
										</li>
										<li class="jiage_d">¥1</li>
										<li>商品名称： 6A10 环保</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d">
									<ul>
										<li>品牌： CT（长亭）</li>
										<li>封装/规格: R-6</li>
										<li>现货库存：256 圆盘</li>
									</ul>
								</dd>
								<dd class="san_bgJIa_d san_bgbai_d">
									<ul>
										<li>已售出：2205 圆盘多</li>
										<li><span class="jiajian_d"><a href="/"
												target="_blank">-</a><input name="jiage2" type="text" /><a
												href="/" target="_blank" class="righter">+</a>
										</span><span class="jiarugou_d"><a href="/" target="_blank">加入购物车</a>
										</span>
										</li>
									</ul>
								</dd>
							</dl>

							<div class="pager_d">
								
							</div>



						</div>
					</div>
				</div>
			</div>
		</form>
		<div class="clear"></div>
	</div>

	<jsp:include page="../common/foot.jsp"></jsp:include>
	
	<!-- 登录弹出开始 -->
		<div id="myModal" class="reveal-modal" style="top:220px;display: none ">
        
		<h1 class="tle">亲，您尚未登录~<em></em></h1>
		<div class="reveal-modal-cont">
			
			<div class="reveal-tab-meun hd">
				<ul>
					<li class="on">登录</li>
					<li><a href="<%=basePath%>login_goRegister">注册</a></li>
				</ul>
			</div>
			<br class="clear">
			<div class="reveal-tab-cont bd">
				<div class="reveal-tab-item">
					 <s:form action="login_login" id="loginByLayerForm" method="post">
					 <span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="msg"></span>
					<input type="hidden" value="" id="nexturl"/>
					<ul>
						<li>
							<div class="ipone"><span class="hao_name">用 户 名：</span></div>
							<div class="text"><input id="username" name="ctUser.UUserid" type="text" style="color: rgb(204, 204, 204);" value="用户名/手机/邮箱" onBlur="if(this.value==''||this.value=='用户名/手机/邮箱'){this.value='用户名/手机/邮箱';this.style.color='#cccccc'}" onFocus="if(this.value=='用户名/手机/邮箱') {this.value='';};this.style.color='#000000';"/></div>
							<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="usernametip"></span>
						</li>
						<li>
							<div class="ipone"><span class="hao_name">密　　码：</span></div>
							<div class="text"><input type="password" id="password" name="ctUser.UPassword"></div>
							<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="passwdtip"></span>
						</li>
						<li>
							<div class="texter">
								<a class="submit-btn" href="javascript:loginByLayer();">登　　录</a>
							</div>
						</li>
						<li class="li-top">
							<div class="text text-radio">
								<span class="fl_f"><input type="checkbox" value="1" name="remember" id="remember">自动登录</span>
								<span class="fr_f"><a href="<%=basePath%>login_goForgetPwd">忘记密码？</a></span>
							</div>
						</li>
						<li class="li-top">
						<!--  
<dl class="disanfang">
<dt>用第三方帐号登录长亭电子官网</dt>
<dd class="taobaod"><a href="/" target="_blank">淘宝</a></dd>
<dd class="weibod"><a href="/" target="_blank">微博</a></dd>
<dd class="tengxund"><a href="/" target="_blank">腾讯</a></dd>
</dl>-->

						</li>
					</ul>
					<input name="act" type="hidden" value="signin">
					</s:form>
				</div>
			</div>
			
		</div>
		<a class="close-reveal-modal" href="javascript:close()"></a>
	</div><div class="reveal-modal-bg" style="cursor: pointer;display: none"></div>
<!-- 登录弹出结束 -->
	<script type="text/javascript">
	$("body").keydown(function(e){
    	var isFocus=$("#username").is(":focus");
    	var curKey=e.which;
    	if(curKey==13&&isFocus!=true){
    		loginByLayer();
    	}
    });
	var typeId = 0;
	function checkSearch11(key, value){
		$("#checkFromKeyWord").val(key);
		$("#checkFromCname").val(value);
		//var bname =  $("#checkFromBname").val();
		var b = $("#bnametype").data("bname");
		if(b != null && b != ""){
			$("#checkFromBname").val(b);
		}
		//alert(b);
		$("#goodslistpageindex").val(pagenum);
		//$("#checkFromCname").attr("name","cname");
		$("#checkSearch").attr("action","goods_search");
		$("#checkSearch").submit();
		
		typeId = 1;
	}
	function checkSearch22(key, value){
		$("#checkFromKeyWord").val(key);
		$("#checkFromBname").val(value);
		var b = $("#cnametype").data("cname");
		if(b != null && b != ""){
			$("#checkFromCname").val(b);
		}
		$("#goodslistpageindex").val(pagenum);
		//$("#checkFromCname").attr("name","bname");
		$("#checkSearch").attr("action","goods_search");
		$("#checkSearch").submit();
		typeId = 2;
	}
	function checkSearch33(key, value, values){
		$("#checkFromKeyWord").val(key);
		$("#checkFromBname").val(value);
		$("#checkFromCname").val(values);
		$("#goodslistpageindex").val(pagenum);
		//$("#checkFromCname").attr("name","bname");
		$("#checkSearch").attr("action","goods_search");
		$("#checkSearch").submit();
		typeId = 3;
	}
	function delSearchKey(key){
		$("#checkFromCname").remove();
		$("#checkFromKeyWord").val(key);
		$("#goodslistpageindex").val(pagenum);
		$("#checkFromKeyWord").attr("name","keyword");
		
		$("#checkSearch").attr("action","goods_search");
		$("#checkSearch").submit();
		typeId = 0;
	}
	function delSearchKey1(key){
		$("#checkFromBname").remove();
		$("#checkFromKeyWord").val(key);
		$("#goodslistpageindex").val(pagenum);
		$("#checkFromKeyWord").attr("name","keyword");
		
		$("#checkSearch").attr("action","goods_search");
		$("#checkSearch").submit();
		typeId = 0;
	}
	var pagenum = 1;
	/* 分页跳转控制器 */
	function upAndDownPage(sum){
		var pagetkword = $("#pagetkword").val();
		var chaik = pagetkword.split("  ");
		var cname = $("#cnametype");
		var bname = $("#bnametype");
		if(cname.length == 1 && bname.length == 0){
			typeId = 1;
		}
		if(bname.length == 1 && cname.length == 0){
			typeId = 2;
		}
		if(bname.length == 1 && cname.length == 1){
			typeId = 3;
		}
		var key = "";
		var value = "";
		var values = "";
		if(chaik.length != 0){
			if(chaik.length == 3){
				key = chaik[0];
				value = chaik[1];
				values = chaik[2];
			}
			if(chaik.length == 2){
				key = chaik[0];
				value = chaik[1];
			}
			if(chaik.length == 1){
				key = chaik[0];
			}
			pagenum = sum;
			if(typeId == 0){
				delSearchKey(key);
			}
			if(typeId == 1){
				checkSearch11(key, value);
			}
			if(typeId == 2){
				checkSearch22(key, value)
			}
			if(typeId == 3){
				checkSearch33(key, value, values)
			}
		}
		////截取访问名
		//var url = document.URL.toString();
		//var i = url.lastIndexOf("/");
		//var s = url.lastIndexOf("?");
		//if(s == -1){
		//	
		//} else {
		//	var chai = url.split("?");
		//	i = chai[0].lastIndexOf("/");
		//	url = url.substring(i+1, s);
		//}
        //
		//var paid = document.getElementById("paid").value;
		////var keyw = document.getElementById("keyw").value;
		//var keyw = document.getElementById("keyw1").value;
		//var caid = document.getElementById("caid").value;
		//var kword = document.getElementById("kword").value;
		//var $jiage1 = $("#jiage1").val();
		//var $jiage2 = $("#jiage2").val();
		//var isPrice = false;
		////if($jiage1.length == 0){
		////	isPrice = true;
		////}
		////if($jiage2.length == 0){
		////	isPrice = true;
		////}
		//var str = "";
		////if(!isPrice){
		////	str = "&fgoodsDTO.price1="+ $jiage1 +"&fgoodsDTO.price2="+$jiage2;
		////}
		//var strUrl = url+"?page=" + sum + "&keyword=" + keyw + "&kword="+kword + str;
		//strUrl = encodeURI(strUrl);
		//window.location.href= strUrl;
	}
	
	</script>
	<script> 

document.onkeydown = function(){
	if(window.event && window.event.keyCode == 123) {
	return false;
	}
}
$(function(){
	$("#chachaCname").click(function(){
		var name=this.dataset.value;
		var b = $("#bnametype").data("bname");
		if(b != null && b != ""){
			var chai = b.split(" ");
			name += "  " + chai[0];
		}
		searchFromClose(name);
	});
	$("#chachaBname").click(function(){
		var name=this.dataset.value;
		var b = $("#cnametype").data("cname");
		if(b != null && b != ""){
			var chai = b.split(" ");
			name += "  " + chai[0];
		}
		searchFromClose(name);
	});
});
</script> 
<div id="toolbar-options" class="hidden">
    <a>请输入数量</a>
    <!--<a href="#"><i class="fa fa-car"></i></a>
    <a href="#"><i class="fa fa-bicycle"></i></a>-->
</div>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/jquery.toolbar.js"></script>
<script>
console.log($(".pir_right dl input[type='text']"));
$(".pir_right dl input[type='text']").toolbar({
    content: '#toolbar-options',
    position: 'top',
    style: 'info',
        // animation: 'flip'
});

$(".pir_right dl input[type='text']").on("toolbarShown",function() {
    var timer = setTimeout(function () {
        $(".tool-container.tool-top").css("display", "none").css("opacity", "0")
    }, 3000)
})
</script>
</body>
</html>

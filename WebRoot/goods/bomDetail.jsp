<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jqueryt.js"></script>
<script type="text/javascript" src="js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="js/bomDetail.js" charset="utf-8"></script>

  </head>
<body>
<jsp:include page="../common/head.jsp"></jsp:include>

	<div class="contant_e">
     <div class="dizhilian_e"><a href="<%=basePath%>" target="_blank">首页</a>><a>关注中心</a>><span>我的BOM</span></div>
<jsp:include page="../common/left.jsp"></jsp:include>	
<div class="right_e right_ed">
<s:form>
 <div class="dangqian_f"><span class="bomreturn"><a href="bom_list">返回</a></span>当前所在BOM：${bom.bomTitle }</div>
<div class="shanchu_i">
   <a href="javascript:deleteThis();" class="a1">删除</a>
   <input class="in1" name="xinzengbom" type="text" id="GName" onkeyup="getGoodsByGname();" value="商品名称" onfocus="if (value =='商品名称'){value =''}" onblur="if (value ==''){value='商品名称'}" />
   <a href="javascript:add();" id="btn_bm" class="a2">增加</a>
   <input class="in2" name="bomGoodsDTO.keyword" id="keyword" type="text" value="订单编号/收货人/商品编号/商品名称" onfocus="if (value =='订单编号/收货人/商品编号/商品名称'){value =''}" onblur="if (value ==''){value='订单编号/收货人/商品编号/商品名称'}" />
   <a href="javascript:searchThis();" class="a3">检索</a>
</div>
 <input type="hidden" id="bomId" value="${bom.bomId}"/>
 <input type="hidden" id="GId"/>
 <ul class="tan_sou" style="display:none;"></ul>

 <div class="bomxx_i">
 <ul class="ul1">
		   <li class="li11"><input id="checkall" type="checkbox"  name='checkall' onclick="javascript:SelectAll()"/>商品图片</li>
		   <li class="li12">商品编号</li>
		   <li class="li13">商品名称</li>
		   <li class="li13">商品品牌</li>
		   <li class="li13">商品类型</li>
		   <!-- <li class="li15">包装类型</li> -->
		   <li class="li16">数量</li>
	</ul>
<s:iterator value="bomGoodsList" var="list">
<c:if test="${list.goodsNum != -1 }">
	<ul class="ul2">
	<li class="li21"><input name="bomGoodsDTO.mid" type="checkbox" value=${list.bomGoodsId } /><a href="<%=basePath%>goods_detail?gid=${list.goods.GId }" target="_blank"><img src="${list.imgUrl }"/></a></li>
		<li class="li22"><s:property value="#list.goods.GSn"/></li>
		<li class="li23"><s:property value="#list.goods.GName"/></li>
		<li class="li24"><s:property value="#list.goods.goodsBrand.BName"/></li>
		<li class="li25"><s:property value="#list.goods.goodsCategory.CName"/></li>
	        			<li class="li28">
	        				<a href="javascript:subNum(${list.bomGoodsId }${list.goods.GId},${list.bomGoodsId })" class="div1">-</a>
	        				<input style="width: 30px" id="${list.bomGoodsId }${list.goods.GId}" type="text" value="${list.goodsNum}" onkeyup="updateNum(${list.bomGoodsId }${list.goods.GId},${list.bomGoodsId });this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"/>
	        				<a href="javascript:addNum(${list.bomGoodsId }${list.goods.GId},${list.bomGoodsId })" class="div2">+</a>
	        			</li>
	    </ul>
</c:if>
<c:if test="${list.goodsNum == -1 }">
	<ul class="ul2">
	<li class="li21"><input type="checkbox" value=${list.bomGoodsId } /><a href="javascript:;"><img src="https://ctegoimg.oss-cn-shenzhen.aliyuncs.com/ct_s.gif"/></a></li>
		<li class="li22">${list.GBomXh }</li>
		<li class="li23">没有找到</li>
		<li class="li24"></li>
		<li class="li25"></li>
	   	<li class="li28">
	       <a href="javascript:;" class="div1">-</a>
	       	<input style="width: 30px" disabled="disabled" type="text" value="0"/>
	       <a href="javascript:;" class="div2">+</a>
	    </li>
	    </ul>
</c:if>
</s:iterator>
<input type="hidden" id="url" name="url"/>
 </div>
 </s:form> 
 
   ${pages.pageStr }
   <script type="text/javascript">
	/* 分页跳转控制器 */
	function upAndDownPage(sum){
		//截取访问名
		var url = document.URL.toString();
		var i = url.lastIndexOf("/");
		var s = url.lastIndexOf("&");
		if(s == -1){
			
		} else {
			var chai = url.split("?");
			i = chai[0].lastIndexOf("/");
			url = url.substring(i+1, s);
		}
		url = url+"&bomGoodsDTO.page="+sum;
		window.location.href= url;
	}

	
	</script>
   </div>
<div class="clear"></div>
</div>
<jsp:include page="../common/foot.jsp"></jsp:include>
</body>
</html>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <base href="<%=basePath%>">
    <title>Bom</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.7.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.js"></script>

<script type="text/javascript" src="js/ct.js" charset="utf-8"></script>
<script type="text/javascript" src="js/bom.js" charset="utf-8"></script>
  </head>
  <body>
  	<s:form id="form" action="bom_saveUpdate" method="post">
  		<input type="hidden" name="bomId" value="${bom.bomId }"/>
	  <div class="txt-fld">
	    <label for="">BOM名称:</label>
	    <input id=""  name="bomTitle" type="text" value="${bom.bomTitle }"/>
	  </div>
	  <div class="txt-fld">
	    <label for="">BOM介绍:</label>
	    <input id=""  name="bomDesc" type="text" value="${bom.bomDesc }"/>
	  </div>
	  <div>
	  	<input type="submit" value="保存" onclick="saveAdd();"/>
	  </div>
  	</s:form>
  </body>
</html>

 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 <%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../common/basePath.jsp"%>
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<script type="text/javascript" src="<%=basePath%>js/collect.js"></script>
<body>
<jsp:include page="../common/head.jsp"></jsp:include>
<div class="contant_e">
    <div class="dizhilian_e"><a href="<%=basePath %>" target="_blank">首页</a>><a>关注中心</a>><span>我的收藏</span></div>
<%@include file="../../common/left.jsp" %>  
<div class="right_l">
	<div id="ddd" class="BMchuang" style="top:80px; left:266px; z-index:888; display:none;" >
<div class="lcha" id="btnClose" onClick="closeDiv('DialogDiv')"><span><img src="<%=imgurl %>/bg/chadiao.gif" /></span></div>
    <div class="tizp">
        <dl class="baozhuang_d">
            <dt>包装：</dt>
            <dd class="baozx_d"></dd>
        </dl>
        <dl class="caigou_d">
            <dt>采购量：</dt>
            <dd class="jiarug_d"><span class="jiajian_d"><a href="javascript:subnum()">-</a><input id="num" name="num" type="text" value="1" /><a href="javascript:addnum()" class="righter">+</a></span></dd>
            <dd class="cunliang_d">库存<span><span id="Gnum"></span>K</span>可售</dd>
        </dl>
        <div class="jiarugoux_d">
        <span class="jiaruche_d"><a href="javascript:addcart()">加入购物车</a></span>
        </div>
    </div>
    <div class="BMchuang_tishi" style="display:none">
<div class="biao_tishi">商品已成功添加到<a href="<%=basePath%>goods_cart">购物车</a></div>
<p class="jie_gou">购物车共有<span id="carGNum"></span>种宝贝</p>
<span class="jiaruche_d"><a href="<%=basePath%>goods_cart">购物车结算</a></span>
</div>
</div>

<div id="dele" class="BMchuang BMchuangsmall " style="top:80px; left:266px; z-index:888; display:none;" >
<div class="lcha" id="btnClose" onClick="closeDiv('DialogDiv')"><span><img src="<%=imgurl %>/bg/chadiao.gif" /></span></div>
    <div class="tizp_de">
        <p>是否删除？</p>
    
        <div class="jiarugoux_dd">
        <span class="jiaruche_d"><a id="de" href="/">确定</a></span>
        <span class="lcha"><a class="jiaruche_d1" id="btnClose" onClick="closeDiv('DialogDiv')">取消</a></span>
        </div>
    </div>
</div>
		<s:iterator value="collectList" status="se">
			<dl class="left_lie_l" id="<s:property value="collectList[#se.index][0]"/>" onMouseOver="javascript:show(this,'<s:property value="collectList[#se.index][2]"/>');" onMouseOut="hide(this,'<s:property value="collectList[#se.index][2]"/>')">
			<dt><a href="<%=request.getContextPath()%>/goods_detail?gid=<s:property value="collectList[#se.index][0]"/>">
			<s:if test='collectList[#se.index][8]!="nourl"'>
										<img src="<%=imgurl %>/<s:property value="collectList[#se.index][8]"/> " width=195>
										</s:if>
										<s:else>
										<img src="<%=imgurl %>/ct_s.gif">
										</s:else>
			</a></dt>
			<dd><a href="<%=request.getContextPath()%>/goods_detail?gid=<s:property value="collectList[#se.index][0]"/>"><s:property value="collectList[#se.index][1]"/></a></dd>
			<!--
			<dd class="jiage_d">
				<s:if test="collectList[#se.index][4]==9999">
										<input type="hidden" id="<s:property value="collectList[#se.index][0]"/>PP" value="<s:property value="collectList[#se.index][5]"/>" />
										￥<s:property value="collectList[#se.index][5]"/>/<s:property value="collectList[#se.index][7]"/>
										</s:if>
										<s:else>
										<input type="hidden" id="<s:property value="collectList[#se.index][0]"/>PP" value="<s:property value="collectList[#se.index][4]"/>" />
										￥<s:property value="collectList[#se.index][4]"/>/<s:property value="collectList[#se.index][7]"/>
										</s:else>
			</dd>
			-->
<s:if test="collectList[#se.index][10] !=0">
<input id="b1" type="hidden" value="<s:property value="collectList[#se.index][10]"/><s:property value="collectList[#se.index][7]"/>/<s:property value="collectList[#se.index][9]"/>"/>
</s:if>
<s:if test="collectList[#se.index][12] !=0">
<input id="b2"  type="hidden" value="<s:property value="collectList[#se.index][12]"/><s:property value="collectList[#se.index][7]"/>/<s:property value="collectList[#se.index][11]"/>"/>
</s:if>
<s:if test="collectList[#se.index][14] !=0">
<input  id="b3" type="hidden" value="<s:property value="collectList[#se.index][14]"/><s:property value="collectList[#se.index][7]"/>/<s:property value="collectList[#se.index][13]"/>"/>
</s:if>
<s:if test="collectList[#se.index][16] !=0">
<input id="b4" type="text" value="<s:property value="collectList[#se.index][16]"/><s:property value="collectList[#se.index][7]"/>/<s:property value="collectList[#se.index][15]"/>"/>
</s:if>
<input type="hidden" id="pa" value="" />
<input type="hidden" id="pn" value="" />
<input type="hidden" id="gid" value="<s:property value="collectList[#se.index][0]"/>" />
<input type="hidden" id="collid" value="<s:property value="collectList[#se.index][18]"/>" />
			<dd><p>品牌：<s:property value="collectList[#se.index][6]"/></p></dd>
<dd id="<s:property value="collectList[#se.index][2]"/>" class="tan_s" style="display:none;">
	<div onClick="javascript:document.getElementById('de').href='javascript:del(<s:property value="collectList[#se.index][0]"/>,<s:property value="collectList[#se.index][18]"/>)';ShowDIV('DialogDiv');show(this,'dele');">
	<a><img src="<%=imgurl %>/bg/s_s.png"></a>
	</div>
	<!--
	<div onClick="javascript:addCart(<s:property value="collectList[#se.index][0]"/>)">-->
	<div>
	<a href="<%=basePath%>goods_detail?gid=<s:property value='collectList[#se.index][0]'/>" target="_blank"><img src="<%=imgurl %>/bg/s_g.png"></a>
	</div>
</dd>
		</dl>
		</s:iterator>
		
	 ${pages.pageStr }  
	 
</div>
<div class="clear"></div>
</div>
<script type="text/javascript">
//删除收藏中的商品
function del(gid,collid){
	$.ajaxSetup({async:false});
	$.post("login_isLogin",function(data){
		if(data =="success"){
			$.post("goods_delcollect",{"fgoodsDTO.gid":gid,"fgoodsDTO.collid":collid},function(data){
				if(data == "success"){
					$("#"+gid).remove();
					$("#BgDiv").css("display", "none");
					$("#dele").css("display", "none");
				}else{
					alert("删除失败");
				}
			});
		}else{
			$("#BgDiv").hide();
			$("#dele").hide();	
			$(".reveal-modal-bg").show();
			$("#myModal").show();
			$("#nexturl").val("goods_collectlist");
		}
	});
	
}
function upAndDownPage(sum){
	//截取访问名
	var url = document.URL.toString();
	var i = url.lastIndexOf("/");
	var s = url.lastIndexOf("?");
	var d = url.lastIndexOf("&");
	if(d != -1){
		url = url.substring(i+1, d);
		window.location.href= url+"&page=" + sum;
	} else {
		if(s != -1){
			url = url.substring(i+1, s);
		}
		window.location.href= url+"?page=" + sum;
	}
	
	
}
</script>
<%@include file="../../common/foot.jsp" %>    
</body>
</html>

<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
<script type="text/javascript" src="js/jqueryt.js"></script>
<script type="text/javascript" src="js/lrtk.js"></script>
<script type="text/javascript">

function brandDetail(BId) {
	window.location.href="goods_brandDetail?fgoodsDTO.BId="+BId;
}
</script>
<link type="text/css" rel="stylesheet" href="css/style.css" />

<!--<script src="js/jquery-1.4.2.min.js" type="text/javascript"></script>-->

</head>

<body>

<jsp:include page="../common/head.jsp"></jsp:include>
<div class="contant_e">

    <div class="dizhilian_e"><a href="<%=basePath%>" target="_blank">首页</a>><span>品牌</span></div>
    
	
<div class="con_pinpai"> 
    <ul>
    <c:forEach items="${brandList}" var="br">
        <li>
            <a href="goods_brandDetail?fgoodsDTO.BId=${br.BId}"><img src="<%=imgurl %>${br.BLogo}" ><span>${br.BName }</span></a>
        </li>
    </c:forEach>
    </ul>
</div>
<script>
    var li=$(".con_pinpai>ul>li");
    for(var i=4;i<li.length;i+=5){
    	$(li[i]).css("margin-right","0px")
    }
</script>   

<div class="clear"></div>
</div>
<jsp:include page="../common/foot.jsp"></jsp:include>
</body>
</html>

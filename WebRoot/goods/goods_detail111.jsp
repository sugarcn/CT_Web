﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="frm" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<base href="<%=basePath%>">

<meta name="Keywords" content="<%=key%>" />
<meta name="Description" content="<%=title%>" />
<title><%=title%> ${goods.GName }</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<script type="text/javascript" src="<%=basePath %>js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/scroll.1.3.js"></script><!-- 商品详情 -->
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.SuperSlide2.js"></script><!-- 商品详情 -->
<script src="<%=request.getContextPath()%>/js/MagicZoom.js" type="text/javascript"></script><!-- 商品详情 -->

<script type="text/javascript" src="<%=request.getContextPath()%>/js/goodsDetail.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/goods/js/goods_list.js" ></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/goods/js/newgoods_price.js" ></script>
<script type="text/javascript">
function subnum(){
	var goodsNum = $("#num").val();
	if(goodsNum < 2){
		alert("数量不能小于1");
		return ;
	}
	var newgoodsNum = Number(goodsNum) - 1;
	$("#num").val(newgoodsNum);
}
function addnum(){
	var goodsNum = $("#num").val();
	var newgoodsNum = Number(goodsNum) + 1;
	$("#num").val(newgoodsNum);
}

function load(){
	document.getElementById("p1").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
	document.getElementById("p2").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
	document.getElementById("p3").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
	document.getElementById("p4").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
}
function setpack(indx,num,num2){
	document.getElementById("pa").value=indx;
	document.getElementById("pn").value=num;
	document.getElementById("pa2").value=num2;
	if(indx==1){
		if(document.getElementById("p2")!=null){
			document.getElementById("p2").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		if(document.getElementById("p3")!=null){
			document.getElementById("p3").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		if(document.getElementById("p4")!=null){
			document.getElementById("p4").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		document.getElementById("p1").style.cssText = "margin-right:8px; display:block; border:2px solid #da082f; height:26px; line-height:26px; float:left; border-radius:3px; padding:0 14px; color:#da082f; background:url(<%=imgurl %>/bg/xuanzhong_bg.gif) no-repeat right bottom;";
	}else if(indx==2){
		if(document.getElementById("p1")!=null){
			document.getElementById("p1").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		if(document.getElementById("p3")!=null){
			document.getElementById("p3").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		if(document.getElementById("p4")!=null){
			document.getElementById("p4").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		document.getElementById("p2").style.cssText = "margin-right:8px; display:block; border:2px solid #da082f; height:26px; line-height:26px; float:left; border-radius:3px; padding:0 14px; color:#da082f; background:url(<%=imgurl %>/bg/xuanzhong_bg.gif) no-repeat right bottom;";
	}else if(indx==3){
		if(document.getElementById("p2")!=null){
			document.getElementById("p2").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		if(document.getElementById("p1")!=null){
			document.getElementById("p1").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		if(document.getElementById("p4")!=null){
			document.getElementById("p4").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		document.getElementById("p3").style.cssText = "margin-right:8px; display:block; border:2px solid #da082f; height:26px; line-height:26px; float:left; border-radius:3px; padding:0 14px; color:#da082f; background:url(<%=imgurl %>/bg/xuanzhong_bg.gif) no-repeat right bottom;";
	}else if(indx==4){
		if(document.getElementById("p2")!=null){
			document.getElementById("p2").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		if(document.getElementById("p3")!=null){
			document.getElementById("p3").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		if(document.getElementById("p1")!=null){
			document.getElementById("p1").style.cssText = "margin-right:8px; display:block; border:1px solid #999; height:28px; line-height:28px; float:left; border-radius:3px; padding:0 15px; color:#666;";
		}
		document.getElementById("p4").style.cssText = "margin-right:8px; display:block; border:2px solid #da082f; height:26px; line-height:26px; float:left; border-radius:3px; padding:0 14px; color:#da082f; background:url(<%=imgurl %>/bg/xuanzhong_bg.gif) no-repeat right bottom;";
	}
}
</script>
</head>

<body oncontextmenu="return false" ondragstart="return false" onselectstart ="return false" onselect="document.selection.empty()" oncopy="document.selection.empty()" onbeforecopy="return false" onmouseup="document.selection.empty()">
<!-- <body> -->
<div id="bgallfk" class="bgallfk" style="display:none;"></div>
<!-- 登录弹出开始 -->
		<div id="myModal" class="reveal-modal" style="top:220px; display: none; z-index:666">
        
		<h1 class="tle">亲，您尚未登录~<em></em></h1>
		<div class="reveal-modal-cont">
			
			<div class="reveal-tab-meun hd">
				<ul>
					<li class="on">登录</li>
					<li><a href="<%=basePath%>login_goRegister">注册</a></li>
				</ul>
			</div>
			<br class="clear">
			<div class="reveal-tab-cont bd">
				<div class="reveal-tab-item">
					 <s:form action="login_login" id="loginByLayerForm" method="post">
					 <span style="color: red; padding-left:18px; display:block; width:320px; height:20px; line-height:20px; text-align:left; font-size:12px;" id="msg"></span>
					<input type="hidden" value="" id="nexturl"/>
					<ul>
						<li>
							<div class="ipone"><span class="hao_name">用 户 名：</span></div>
							<div class="text"><input id="username" name="ctUser.UUserid" type="text" style="color: rgb(204, 204, 204);" value="用户名/手机/邮箱" onblur="if(this.value==''||this.value=='用户名/手机/邮箱'){this.value='用户名/手机/邮箱';this.style.color='#cccccc'}" onfocus="if(this.value=='用户名/手机/邮箱') {this.value='';};this.style.color='#000000';"/></div>
							<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="usernametip"></span>
						</li>
						<li>
							<div class="ipone"><span class="hao_name">密　　码：</span></div>
							<div class="text"><input type="password" id="password" name="ctUser.UPassword"/></div>
							<span style="color: red; padding-left:18px; text-align:left; font-size:12px;" id="passwdtip"></span>
						</li>
						<li>
							<div class="texter">
								<a class="submit-btn" href="javascript:loginByLayer();">登　　录</a>
							</div>
						</li>
						<li class="li-top">
							<div class="text text-radio">
								<span class="fl_f"><input type="checkbox" value="1" name="remember" id="remember">自动登录</span>
								<span class="fr_f"><a href="<%=basePath %>login_goForgetPwd">忘记密码？</a></span>
							</div>
						</li>
						<!--  
						<li class="li-top">
<dl class="disanfang">
<dt>用第三方帐号登录长亭电子官网</dt>
<dd class="taobaod"><a href="/" target="_blank">淘宝</a></dd>
<dd class="weibod"><a href="/" target="_blank">微博</a></dd>
<dd class="tengxund"><a href="/" target="_blank">腾讯</a></dd>
</dl>

						</li>-->
					</ul>
					<input name="act" type="hidden" value="signin">
					</s:form>
				</div>
			</div>
		</div>
		<a class="close-reveal-modal" href="javascript:close()"></a>
	</div><div class="reveal-modal-bg" style="cursor: pointer;display: none"></div>
<!-- 登录弹出结束 -->
	<jsp:include page="../common/head.jsp"></jsp:include>
	<jsp:include page="../common/subhead.jsp"></jsp:include>
	<div class="contant_d">

		<div class="dizhilian_d">
			<a href="<%=basePath %>" target="_blank">首页</a>&gt;<c:forEach items="${cateTypeNew}" var="list"><a target="_blank">${list.value}</a>&gt;</c:forEach>
			<span>[${goods.GName }]</span>
		</div>

		<div class="xiangqing_d">

<div class="xiangtu_d">

	<div class="datu_d">




<div id="tsShopContainer">
	<div id="tsImgS"><a href="${goodsImgBig.PUrl }" title="${goods.GName }" class="MagicZoom" id="MagicZoom"><img width="300" height="300" src="${goodsImgBig.PUrl }" /></a></div>
	<div id="tsPicContainer">
		<div id="tsImgSArrL" onclick="tsScrollArrLeft()"></div>
		<div id="tsImgSCon">
			<ul>
				<s:iterator value="imgList" var="img" status="stauts" >
				
					<li onclick="showPic(${stauts.index})" rel="MagicZoom" 
					<s:if test="#stauts.index==0">
					 class="tsSelectImg"
					</s:if>
					 ><img height="42" width="42" src="${img.PUrl }" tsImgS="${img.PUrl }" /></li>
					 <input type="hidden" value="${img.PUrl }" id="aaaa" />
				</s:iterator>
			</ul>
		</div>
		<div id="tsImgSArrR" onclick="tsScrollArrRight()"></div>
	</div>
	<img class="MagicZoomLoading" width="16" height="16" src="<%=imgurl %>/loading.gif" alt="Loading..." />
    
<dl class="xiangshou_d">
<dt><a href="javascript:collect(${goods.GId });">收藏该商品</a></dt>
</dl>
</div>

<script src="js/ShopShow.js"></script>  <!-- 此行JS必须放在此处，否则以上效果出错！ -->

    </div>
	<div class="wenjie_d">

<h1>${goods.GName } 
<c:if test="${goodsInfo.isSample == 1 && goodsInfo.isPartial == 0 }">
	<img src="<%=imgurl %>/zcypq.gif" />
</c:if>
 </h1>
<input type="hidden" id="gid" value="${goods.GId }"/>
<ul class="xx_d">
<li><span>产品品牌：</span><i>${goods.BName }</i></li>
<input type="hidden" value="${goods.BName }" id="BName1" />
<input type="hidden" value="${goods.uname }" id="uname1" />
<input type="hidden" value="${goods.GSn }" id="GSn1" />
<li><span>通用名称：</span><i>${goods.uname }</i></li>
<input type="hidden" value="${goods.pack1Num }" id="${goods.GId }pack1num" />
<li class="xx_right"><span>原厂料号：</span><i>${goods.GSn }</i></li>
<li class="xx_bzsm"><span>包装说明：</span>
<input type="hidden" id="baozhuangshuom" value="${goods.pack1 }" />
<c:if test="${goods.pack1 != 'N/A' }">
	<span>${goods.pack1Num }${goods.GUnit }/${goods.pack1 }</span>
</c:if>
<c:if test="${goods.pack2 != 'N/A' }">
	<span>${goods.pack2Num }${goods.GUnit }/${goods.pack2 }</span>
</c:if>
<c:if test="${goods.pack3 != 'N/A' }">
	<span>${goods.pack3Num }${goods.GUnit }/${goods.pack3 }</span>
</c:if>
<c:if test="${goods.pack4 != 'N/A' }">
	<span>${goods.pack4Num }${goods.GUnit }/${goods.pack4 }</span>
</c:if>
</li>
</ul>
<div class="yp_pl_ct">


<dl class="left_lie_d_c">

						<dd class="san_bgJIa_d">
						    <ul>
<li class="pdt"><span class="yp_dj">样片单价<i>（PCS）</i></span></li>
<c:forEach items="${rangePricesList }" var="list" varStatus="start">


<input type="hidden" name="${goods.GId }gidSim" value="${list.simIncrease }"/>
<input type="hidden" name="${goods.GId }gidENum" value="${list.simENum }"/>
<input type="hidden" name="${goods.GId }gidSNum" value="${list.simSNum }"/>
<input type="hidden" name="${goods.GId }gidSPrice" value="${list.simRPrice }"/>
<c:if test="${list.simSNum != '0' && list.simSNum != null }">
<li name="${start.index }isPriceHover"><span class="Ia_qus"><i>${list.simSNum }-${list.simENum }：</i></span><i class="Ia_jia">￥${list.simRPrice }</i></li>
</c:if>
</c:forEach>

<li class="pdt_jj">
<span class="jiajian_d">
	<c:if test="${cunsim == 'no' }">
		<a href="javascript:;" >&minus;</a>
		<!-- <input maxlength="4" id="${goods.GId }" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" onblur="isSimMin(${goods.GId });" type="text" value="0" /> -->
		<input disabled="disabled" id="${goods.GId }" value="0"/>
		<!-- onblur="isSimMin(${goods.GId });"  -->
		<a href="javascript:;" class="righter">&#43;</a>
	</c:if>
	<c:if test="${cunsim == 'yes' }">
		<a href="javascript:subSimnum(${goods.GId })" >&minus;</a>
		<!-- <input maxlength="4" id="${goods.GId }" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" onblur="isSimMin(${goods.GId });" type="text" value="0" /> -->
		<input maxlength="4" id="${goods.GId }" onblur="simcountinAjax(${goods.GId },this.value);" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" type="text" value="0" />
		<!-- onblur="isSimMin(${goods.GId });"  -->
		<a href="javascript:addSimnum(${goods.GId })" class="righter">&#43;</a>
	</c:if>
	
</span>
</li>
						    </ul>
						</dd>
						
						<dd class="san_bgJIa_b">
						    <ul>
<li class="pdt" id="yuliuAdd"><span class="pl_dj_t">批量单价</span><span class="pl_k hover"  id="k" onclick="tabDanWeiK(this,${goods.GId });">K</span><span id="pan" onclick="tabDanWeiPan(this,${goods.GId })" class="pl_p">${goods.pack1 }</span></span></li>
<c:forEach items="${ranListDTO }" var="list" varStatus="start">
<c:if test="${list.parSNum != '0' && list.parSNum != null }">
<input type="hidden" name="${goods.GId }gidPar" value="${list.parIncrease}"/>
<input type="hidden" name="${goods.GId }gidPar1" value="${list.parIncrease}"/>
<input type="hidden" name="${goods.GId }gidParENum" value="${list.parENum}"/>
<input type="hidden" name="${goods.GId }gidParSNum" value="${list.parSNum}"/>
<input type="hidden" name="${goods.GId }gidParPrice" value="${list.parRPrice * 1000 }"/>
<li name="${start.index }isPriceHoverPar"><span class="Ia_qus"><i>${list.parSNum}-<c:if test="${list.parENum != '0' && list.parENum != null }">${list.parENum }</c:if>：</i></span><i class="Ia_jia">￥<frm:formatNumber pattern="0.00">${list.parRPrice *1000 }</frm:formatNumber>
</i><p class="xdy">相当于<i class="xdy_ct"><frm:formatNumber pattern="0.00">${list.parRPrice * goods.pack1Num }</frm:formatNumber></i>元<i>/${goods.pack1 }</i></p></li></c:if>
</c:forEach>

<li class="pdt_jj">
	<span class="jiajian_d">
		<a href="javascript:subParnum(${goods.GId })" >&minus;</a>
		<input maxlength="4" onkeyup="this.value=this.value.replace(/\D/g,'')" onblur="parCountAjax(${goods.GId },this.value);" onafterpaste="this.value=this.value.replace(/\D/g,'')" id="${goods.GId }num"  type="text" value="0" />
		<!-- <input maxlength="4" onkeyup="this.value=this.value.replace(/\D/g,'')" onblur="isParMin(${goods.GId });" onafterpaste="this.value=this.value.replace(/\D/g,'')" id="${goods.GId }num"  type="text" value="0" /> -->
		<!-- onblur="isParMin(${goods.GId });"  -->
		<a href="javascript:addParnum(${goods.GId })" class="righter">&#43;</a>
	</span>
</li>
						    </ul>
						</dd>
						<!--  onblur="getSumCount(${goods.GId });" -->
						<dd class="zongcg_ct"><span>总数量：</span>
						<!-- <input maxlength="7" name="${goods.GId }countSum" onblur="getSumCount(${goods.GId });" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" id="countSum" type="text" value="0" /> -->
						<input maxlength="7" name="${goods.GId }countSum" onblur="countAjax(${goods.GId },this.value);" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" id="countSum" type="text" value="0" />
						<i class="cvfr">PCS</i><p><i class="cvft"><img src="<%=imgurl%>/bg/tixing.gif"></i>直接输入总数量，系统会自动拆分样片及批量数量。</p></dd>
</dl>
</div>


<ul class="jiage_d">
<li><span class="jiagedan_d">当前总金额：</span><span class="jiagef_d">¥</span><span id="simPriceCount" class="jiageshu_d">3200</span></li>

<input type="hidden" id="goodsSimPriceCount" />
<input type="hidden" id="goodsParPriceCount" />
<script type="text/javascript">
loadPeiceNew(gid);
	$(function(){
		$("#choose").change(function(){
			var id = $(this).val();
			$.ajaxSetup({async:false});
			$.post("login_isLogin",function(data){
				if(data == "success"){
					$.post("${pageContext.request.contextPath}/json/getCouLingQu",
							{"couponIdForPage":id},
						function(data){
						alert(data);
					},"json"
					);
				}else{
					$("#myModal").show();
					$("#nexturl").val("${pageContext.request.contextPath}/goods_detail?gid="+$("#gid").val());
				}
				
			});
			
		});
		var u = "${userinfosession}";
		var res = "${result}";
		if(u != null && res != "无"){
			$("#cunpon").show();
		} else {
			$("#cunpon").hide();
		}
	});
</script>
<li id="cunpon" class="youhuiqx_d" >
<div id="isCoupon">
	<select name="choose" id="choose" class="select_showbox">
	<option  value="0" selected="selected">领取优惠券</option>
		<c:forEach items="${ctCoupons }" var="list">
			<c:if test="${list.couponType == '1' }">
			<option value="${list.couponId }">满${list.amount }减${list.discount }</option> 
			</c:if>
			<c:if test="${list.couponType == '2' }"> 
			<option value="${list.couponId }">优惠${list.discount }</option> 
			</c:if> 
			<c:if test="${list.couponType == '3' }"> 
			<option value="${list.couponId }">${list.discount }折</option> 
			</c:if>
		</c:forEach>
</select>
</div>
</li>
</ul>



<script src="http://qzonestyle.gtimg.cn/qzone/app/qzlike/qzopensl.js#jsdate=20111201" charset="utf-8"></script>
<script type="text/javascript">
	function addcart(){
		//var cc = document.getElementById("pa2").value;
		//var pack = document.getElementById("pa").value;
		var tar = document.getElementById("gid").value;
		var goodsNum = $("#"+tar).val();
		var parCount1 = $("#"+tar + "num").val();
		var isXuan = false;
		if(goodsNum == '0' && parCount1 == '0'){
			isXuan = false;
		} else {
			isXuan = true;
		}
		if(!isXuan){
			alert("请选择数量");
		} else {
	$.ajaxSetup({async:false});
	$.post("login_isLogin",function(data){
		
		//var pp = document.getElementById("pp").value;
		//var num = document.getElementById("num").value;
		//var pn = document.getElementById("pn").value;
		var goodsParNum = $("#"+tar+"num").val();
		var goodsNum = $("#"+tar).val();
		var pack = "";
		if(goodsParNum == 0 && goodsNum != 0) {
			pack="1--0";
		} else if(goodsNum == 0 && goodsParNum != 0) {
			pack="0--1";
		} else {
			pack="1--1";
		}
		if(isPanOrK){
			var inc = $("input[name="+tar+"gidPar1]").val();
			goodsParNum = goodsParNum * inc;
		}
		var num = goodsNum + "--" + (goodsParNum*1000);
		var simPrice = $("#goodsSimPriceCount").val();
		var parPrice = $("#goodsParPriceCount").val();
		var isNowSimPrice = $("#isNowSimPrice").val();
		var isNowParPrice = $("#isNowParPrice").val();
		if(isNowParPrice == ""){
			isNowParPrice = "0";
		}
		if(isNowSimPrice == ""){
			isNowSimPrice = "0";
		}
		var ap = simPrice + "--" + parPrice + "--" + isNowSimPrice + "--" + (isNowParPrice/1000);
		var da = tar+"=="+pack+"=="+num+"=="+ap;
		//alert("tar:"+tar+" pack:"+pack+" num:"+num+" ap:"+ap);
		if(data == "success"){
			$.post("add_cart",{"data":da},function(date,varstart){
				if(date == "success"){
					$(".BMchuangshowgw").show();
				}
			});
			//location.href="add_cart?data="+da;
		}else{
			$("#bgallfk").show();
			$("#myModal").show();
			$("#nexturl").val("add_cart?data="+da);
		}
	});
	}
}
	function goBack(){
		$(".BMchuangshowgw").hide();
	}
	function toCart(){
		location.href="goods_cart";
		$("#pa2").val("");
		$("#pa").val("");
	}
	function toColl(){
		location.href="goods_collectlist";
	}
	function BrowseFolder(){
		 try{
		  var Message = "请选择文件夹";  //选择框提示信息
		  var Shell = new ActiveXObject( "Shell.Application" );
		  var Folder = Shell.BrowseForFolder(0,Message,0x0040,0x11);//起始目录为：我的电脑
		  //var Folder = Shell.BrowseForFolder(0,Message,0); //起始目录为：桌面
		  if(Folder != null){
		    Folder = Folder.items();  // 返回 FolderItems 对象
		    Folder = Folder.item();  // 返回 Folderitem 对象
		    Folder = Folder.Path;   // 返回路径
		    if(Folder.charAt(Folder.length-1) != "\\"){
		      Folder = Folder + "\\";
		    }
		    document.all.savePath.value=Folder;
		    return Folder;
		  }
		 }catch(e){ 
		  alert(e.message);
		 }
		}
</script>

<div class="ku_cun">
库存：<i>${goods.gnum }</i>K 
<div class="bdsharebuttonbox">
	<a href="#" class="bds_more" data-cmd="more">分享到：</a>
	<a href="#" class="bds_sqq" data-cmd="sqq" title="分享到QQ好友"></a>
	<a href="#" class="bds_qzone" data-cmd="qzone" title="分享到QQ空间"></a>
	<a href="#" class="bds_tsina" data-cmd="tsina" title="分享到新浪微博"></a>
	<a href="#" class="bds_tqq" data-cmd="tqq" title="分享到腾讯微博"></a>
	<a href="#" class="bds_renren" data-cmd="renren" title="分享到人人网"></a>
	<a href="#" class="bds_weixin" data-cmd="weixin" title="分享到微信"></a>
</div>
<script>
var BName = $("#BName1").val();
var uname = $("#uname1").val();
var GSn = $("#GSn1").val();
var aa = $("#aaaa").val();
	window._bd_share_config={
			"common":{"bdSnsKey":{},
			"bdText":"买原装正品电子元器件，就上长亭易购！"+ BName + "_" + uname + "_" + GSn,
			"bdDesc":"长亭易购，这家不错哦，原装正品，种类齐全，价格便宜，还有活动，快去看看吧~~",
			"bdMini":"2",
			"bdMiniList":false,
			"bdPic":aa,
			"bdStyle":"1",
			"bdSize":"16"},
			"share":{},
			"image":{"viewList":["qzone","tsina","tqq","renren","weixin"],
			"viewText":"分享到：","viewSize":"16"},
			"selectShare":{"bdContainerClass":null,
			"bdSelectMiniList":["qzone","tsina","tqq","renren","weixin"]}};
	with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];
</script>



</div>

<div class="jiarugoux_d">
<span class="jiaruche_d"><a
<c:if test="${goods.gnum == 0 }">
	target="_blank" href="https://v2.live800.com/live800/chatClient/chatbox.jsp?companyID=562039&configID=125500&lan=zh&jid=2892221237&enterurl=http%3A%2F%2Fwww.ctego.com%2Fgoods_search&tm=1464334928331&s=1"> 询价 </a>
</c:if>
<c:if test="${goods.gnum != 0 }">
	 href="javascript:addcart()">加入购物车</a>
</c:if>
</span>
<!-- <span class="jiarubom_d"><a href="javascript:;" id="btn_bmc">加入我的BOM</a></span> -->
<span class="jiarubom_d"><a
<c:if test="${goods.gnum == 0 }">
	href="javascript:void(0);"
</c:if>
<c:if test="${goods.gnum == 0 }">
	href="javascript:addBom();"
</c:if>
 >加入我的<i>BOM</i></a></span>
</div>
<div class="BMchuangshowgw" style="z-index:888; display:none" >
<div class="lchashow"><span><img onclick="goBack();" src="<%=imgurl %>/bg/chadiao.gif" /></span></div>
    <div class="tizp">
        <div class="biao_tishi">成功加入<a href="goods_cart" target="_blank">我的购物车</a></div>
        <div class="biao_tuijian">
<dl>
<dt><a href="javascript:;" onclick="goBack();">继续购物</a></dt>
<dd><a href="javascript:;" onclick="toCart();">去购物车结算</a></dd>
</dl>       
        </div>
    
    </div>
</div>
<div class="BMchuang" style="display:none; top:55px; left:95px; z-index:888;" >
<div class="lcha"><span><a href="javascript:closeBOM()"><img src="<%=imgurl %>/bg/chadiao.gif" /></a></span></div>
<s:form id="bom-form" action="" method="post">
<div class="tizp">
<input type="hidden" value="" id="nexturl-bom"/>
<input type="hidden" name="GId" value="${goods.GId }"/>
<input type="hidden" id="pack" name="pack"/>
<input type="hidden" id="goodsNum" name="goodsNum"/>
<div class="xinjianb_f"><span class="shoujia">新建我的<i>BOM</i>：</span><input name="bomTitle" id="bm_name" value=""></div>
<div class="xinjianbline_f">
<span class="shoujia">加入已有的<i>BOM</i>：</span>
    <ul class="gund_f">
     <s:iterator value="boms" var="blist">
    	<li><input id="checkbox" name="mid" type="checkbox" value="${blist.bomId }" class="xuanze_f" />${blist.bomTitle }</li>
    </s:iterator>
    </ul>
</div>
<div class="tijiao"><span><a href="javascript:confirmAdd();" ><img src="<%=imgurl %>/bg/tan_que.gif" /></a></span></div>
</div>
</s:form>
</div>
<div class="CMchuang" style="display:none; top:55px; left:25%;z-index:666 " >
<div class="lcha"><span><a href="javascript:closeCM()"><img src="<%=imgurl %>/bg/chadiao.gif"/></a></span></div>
<form id="coll-form" name="coll-form" action="" method="post">
<div class="tizp">
<input type="hidden" value="" id="nexturl-coll"/>
<input type="hidden" name="GId" value="${cart.GId }"/>
<input type="hidden" id="cartId" name="cartId" value="${cart.cartId }"/>
<input type="hidden" id="cgid" name="GId" value="${goods.GId}"/>
<div class="xinjianb_f"><span class="shoujia">新建我的收藏夹：</span><input name="colltitle" id="coll_name" value=""></div>
<div class="xinjianbline_f">
<span class="shoujia">加入已有的收藏夹：</span>
    <ul class="gund_f">
    <s:iterator value="collects" var="list">
    	<li><input id="checkbox" name="mid" type="checkbox" value="${list.collid }" class="xuanze_f" />${list.colltitle }</li>
    </s:iterator>
    </ul>
</div>
<div class="tijiao"><span><a href="javascript:confirmAddColl();" ><img src="<%=imgurl %>/bg/tan_que.gif" /></a></span></div>
</div>
</form>
</div>
<div class="BMchuangshow" style="top:55px; left:95px; z-index:888;display: none" >
<div class="lchashow"><span><img src="<%=imgurl %>/bg/chadiao.gif" /></span></div>
<div class="tizp">
<div class="biao_tishi"><span id="isadd">成功加入</span><a href="javascript:toColl();" target="_blank">收藏夹</a></div>
<h3><a href="/" target="_blank">更多推荐</a>喜欢此宝贝的还喜欢</h3>
<div class="biao_tuijian">

<dl class="bao_m0_d">
<dt><a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif"></a></dt>
<dd>销量：<span><i>311565</i></span>盒</dd>
<dd class="jiage_d"><i>¥3</i></dd>
</dl>
<dl class="bao_m0_d">
<dt><a href="/" target="_blank"><img src="<%=imgurl %>/tu_a.gif"></a></dt>
<dd>销量：<span><i>311565</i></span>盒</dd>
<dd class="jiage_d"><i>¥3</i></dd>
</dl>

</div>

</div>
</div>

    </div>

</div>
<div class="retui_d">


    <div class="chooeser">
        <div class="ping_tiao">
          <ul>
           <li id="xmlistbc1" onclick="setTab('xmlistbc',1,2)" class="hover"><a href="" onclick="return false" target="_blank">可替换商品</a></li>
           <li id="xmlistbc2" onclick="setTab('xmlistbc',2,2)"><a href="" onclick="return false" target="_blank">相关商品</a></li>
           </ul>
        </div>
        <div class="er_right">
            <div id="con_xmlistbc_1" class="con_all xm_hidden">
	<div class="mr_frbox">
		<div class="mr_frUl">
			<ul class="bao_mr_d">
				<li>
<s:iterator value="Rgoods" status="rg">
<dl class="bao_m0_d">
<dt><a href="<%=request.getContextPath()%>/goods_detail?gid=<s:property value="Rgoods[#rg.index][0]"/>" >
<s:if test='Rgoods[#rg.index][16]!="nourl"'>
										<img src="<%=request.getContextPath()%>/<s:property value="Rgoods[#rg.index][16]"/>">
										</s:if>
										<s:else>
										<img src="<%=request.getContextPath()%>/<%=imgurl %>/ct_s.gif">
										</s:else>
</a></dt>
<dd>品名：<s:property value="Rgoods[#rg.index][1]"/></dd>
<dd class="jiage_d"><s:if test="Rgoods[#rg.index][4]==9999">
￥<s:property value="Rgoods[#rg.index][5]"/>/<s:property value="Rgoods[#rg.index][7]"/>
</s:if>
<s:else>￥<s:property value="Rgoods[#rg.index][4]"/>/<s:property value="Rgoods[#rg.index][7]"/></s:else></dd>
</dl>
</s:iterator>

                </li>
			</ul>
		</div>
        <div class="frBtn_left mr_frBtnL prev">&nbsp;</div>
		<div class="frBtn_right mr_frBtnR next">&nbsp;</div>
	</div>


            </div>
            <div id="con_xmlistbc_2" class="con_all">

	
	<div class="mr_frbox">
		<div class="mr_frUl">
			<ul class="bao_mr_d">
				<li>
<s:iterator value="Lgoods" status="lg">
<dl class="bao_m0_d">
<dt><a href="<%=request.getContextPath()%>/goods_detail?gid=<s:property value="Lgoods[#lg.index][0]"/>" >
<s:if test='Lgoods[#lg.index][16]!="nourl"'>
										<img src="<%=request.getContextPath()%>/<s:property value="Lgoods[#lg.index][16]"/>">
										</s:if>
										<s:else>
										<img src="<%=request.getContextPath()%>/<%=imgurl %>/ct_s.gif">
										</s:else></a></dt>
<dd>品名：<s:property value="Lgoods[#lg.index][1]"/></dd>
<dd class="jiage_d"><s:if test="Lgoods[#lg.index][4]==9999">
￥<s:property value="Lgoods[#lg.index][5]"/>/<s:property value="Lgoods[#lg.index][7]"/>
</s:if>
<s:else>￥<s:property value="Lgoods[#lg.index][4]"/>/<s:property value="Lgoods[#lg.index][7]"/></s:else></dd>
</dl>
</s:iterator>

                </li>
			</ul>
		</div>
        <div class="frBtn_left mr_frBtnL prev">&nbsp;</div>
		<div class="frBtn_right mr_frBtnR next">&nbsp;</div>
	</div>






            </div>
        </div>
    </div>


</div>
<div class="leftsider_d">

    <ul class="left_biao_d">
    <li>产品分类</li>
    </ul>
    
    <div class="fengqing">
    	<s:iterator value="cateList" var="list">
    	<c:set var="cid" value="${list.CId }" />
        <div class="vtitle">
        	<a href="javascript:;" onclick="getQuEncode('${list.CName }')">${list.CName }</a>
        	</div>
          </s:iterator>
    </div>

</div>

<div class="conright_d">



    <div class="chooeswu">
        <div class="ping_tiao">
          <ul>
           <li id="xmlistb1" onclick="setTab('xmlistb',1,3)" class="hover"><a href="" onclick="return false" target="_blank">详细信息</a></li>
           <li id="xmlistb2" onclick="setTab('xmlistb',2,3)"><a href="" onclick="return false" target="_blank">评价<span class="pp_d">(${evaCount })</span></a></li>
           <li id="xmlistb3" onclick="setTab('xmlistb',3,3)"><a href="" onclick="return false" target="_blank">资源下载</a></li>

           </ul>
        </div>
        <div class="ping_right">
            <div id="con_xmlistb_1" class="con_all">
<ul class="ping_a_d">
<li><span>系列：</span>
	${goods.series }
</li>
<li><span>容差：</span>
	${goods.alw }
</li>
	<c:forEach items="${mapAttrAll }" var="list">
	    <li><span>${list.key }：</span>
	    	<c:forEach items="${list.value }" var="list1">
	    		${list1.attrValue } 
	    	</c:forEach>
	    </li>
	</c:forEach>
</ul>


<div class="xiangqingcon_d">




</div>

            </div>
            <div id="con_xmlistb_2" class="con_all" style="display:none;">
<dl class="deal_d">
<dt><span class="deal_d1">采购商</span><span class="deal_d2m">评价内容</span><span class="deal_d4">评价时间</span></dt>
<c:forEach items="${evaList }" var="list">
	<dd><span class="deal_d1">${list.ctUser.nameTemp }</span><span class="deal_d2">${list.evaDesc }</span><span class="deal_d4">${list.evaTime }</span></dd>
</c:forEach>
</dl>

<div class="xiangqingcon_d">

</div>
            </div>
            <script type="text/javascript">
        function checkFile() {
            if($("#all").attr("checked")){
                $("input[name='attachmentPath']").attr("checked",true);
            }else {
                $("input[name='attachmentPath']").attr("checked",false);
            }
        }
         
    </script>
            <div id="con_xmlistb_3" class="con_all" style="display:none;">
<div class="deal_d">
            
            <dl class="xiazai_dla">
            <dt><span class="xa_a_l">文档名称</span><span class="xa_a_r">操作</span></dt>
            <c:forEach items="${resourcesList}" var="attachment">
            <dd><span class="xa_a_l">${attachment.RName}</span><span class="xa_a_r"><a href="javascript:downLoadFileAjax(${attachment.RId});">点击下载此文档</a></span></dd>
            </c:forEach>
            <!-- <input type="button" value="aaa" onclick="BrowseFolder()"/> -->
            </dl>
            
            <!-- 改变表格样式  <table>
                 <tr>
                      <th>
                            文件名 &nbsp; &nbsp; &nbsp;
                     </th>
                      <th>
                            下载
                     </th>
                 </tr>
                <c:forEach items="${resourcesList}" var="attachment">
                    <tr>
                        <td>${attachment.RName}</td>
                        <td>
                            <a href="javascript:downLoadFileAjax(${attachment.RId});">下载</a>
                        </td>
                    </tr>
                </c:forEach>
            </table>-->
</div>
            </div>


        </div>
    </div>
    




</div>
    </div>

<div class="clear"></div>
</div>
<jsp:include page="../common/foot.jsp"></jsp:include>

<div class="left_nav" id="left_nav">
<div class="logo_b">
  <div class="ant_all logo_ant">	
	<div class="fu_left"><a href="/" target="_blank">全部商品分类</a></div>
<script type="text/javascript">
function downLoadFileAjax(rid){
	$.post(
		"FileDownload",
		{"number":rid},
		function(data,varstart){
			alert("文件已经成功下载至 c:\\Program Files下");
		}
	);
}
function searchGoodsXuan(){
	var pid = document.getElementById("pid").value;
	var kword = document.getElementById("kword").value;
	var kwordXuan = document.getElementById("kwordXuan").value;
	if(kwordXuan == null || kwordXuan == ""){
		alert("搜索关键字不能为空");
		return false;
	}else{
		document.getElementByName("searchFormXuan").submit();
	}
}
</script>

<form method="get" name="" action="/" target="_blank" >
	<div id="searchTxt" class="searchTxt" onMouseOver="this.className='searchTxt searchTxtHover';" onMouseOut="this.className='searchTxt';">

		
		<div class="searchMenu">
			
			<div class="searchSelected" id="searchSelected">全部产品分类</div>
			
			<div style="display:none;" class="searchTab" id="searchTab">
				<ul>
					<li>全部产品分类</li>
					<li>免费赠品区</li>
					<li>处理器及微控制器</li>
					<li>嵌入式外围芯片</li>
					<li>逻辑芯片</li>
					<li>存储器</li>
					<li>电源芯片</li>
					<li>电源电池</li>
					<li>五金类/其他</li>
					<li>线材/焊接材料</li>
					<li>电路板DIY专区</li>
					<li>工具/辅助材料</li>
					<li>万能板</li>
				</ul>
			</div>
			
		</div>
		
		<input name="w" type="text" />
				
	</div>
	
	<div class="searchBtn">
		<button id="searchBtn" type="submit">搜 索</button>
	</div>

</form>
    
	<div class="gou_right">
	<a href="<%=request.getContextPath() %>/goods_cart">
	<s:if test="#session.islgn!=null">
	购物车结算<span>${session.islgn }</span>件
	</s:if><s:else>
	我的购物车
	</s:else>
	</a>
	</div>
    </div>
    </div>
    
  </div>
 <input type="hidden" id="isNowSimPrice" />
 <input type="hidden" id="isNowParPrice" />
<!-- 代码 结束 -->
<script type="text/javascript">
//浮动导航
function float_nav(dom){
	var right_nav=$(dom);
	var nav_height=right_nav.height();
	function right_nav_position(bool){
		var window_height=$(window).height();
		var nav_top=(window_height-nav_height)/0;
		if(bool){
			right_nav.stop(true,false).animate({top:nav_top+$(window).scrollTop()},400);
		}else{
			right_nav.stop(true,false).animate({top:nav_top},300);
		}	
		right_nav.show();
	}
	
	if(!+'\v1' && !window.XMLHttpRequest ){
		$(window).bind('scroll resize',function(){
			if($(window).scrollTop()>300){
				right_nav_position(true);			
			}else{
				right_nav.hide();	
			}
		})
	}else{
		$(window).bind('scroll resize',function(){
			if($(window).scrollTop()>300){
				right_nav_position();
			}else{
				right_nav.hide();
			}
		})
	}	
}
float_nav('#left_nav');
/*keyCod从113-123分别禁用了F2-F12键,13禁用了回车键(Enter),8禁用了退格键(BackSpace)*/ 

</script>
<!-- 代码 结束 -->
<script> 
document.onkeydown = function(){
	if(window.event && window.event.keyCode == 123) {
	return false;
	}
}

</script> 
</body>
</html>

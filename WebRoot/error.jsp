<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.ctonline.po.basic.CtSystemInfo"%>
<%@ include file="../common/basePath.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <base href="<%=basePath%>">
    
     <meta name="Keywords" content="${systemInfo.ctKey }" />
<meta name="Description" content="${systemInfo.ctTitle }" />
<title>${systemInfo.ctTitle }</title>
    <link href="<%=request.getContextPath() %>/css/css.css" type="text/css" rel="stylesheet"/>
	<link href="<%=request.getContextPath() %>/css/css2.css" type="text/css" rel="stylesheet"/>
	<link href="<%=request.getContextPath() %>/css/css3.css" type="text/css" rel="stylesheet"/>
	<link type="text/css" rel="stylesheet" href="<%=request.getContextPath() %>/css/style.css" />
	<script type="text/javascript" src="<%=request.getContextPath() %>/js/jqueryt.js"></script>
	<script src="<%=request.getContextPath() %>/js/script.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath() %>/js/ct.js" type="text/javascript"></script>
	
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<script type="text/javascript">
function subnum(num){
	var goodsNum = $("#"+num).val();
	if(goodsNum < 2){
		alert("数量不能小于1");
		return;
	}
	var newgoodsNum = Number(goodsNum) - 1;
	$("#"+num).val(newgoodsNum);
	var pp = document.getElementById(num+"pp").value;
	var obj = document.getElementById(num+"se");
	var index = obj.selectedIndex;
	var pn = obj.options[index].value;
	var money = pp*pn*newgoodsNum;
	document.getElementById(num+"sp").innerText = money+"元";
}
function addnum(num){
	var goodsNum = $("#"+num).val();
	var newgoodsNum = Number(goodsNum) + 1;
	$("#"+num).val(newgoodsNum);
	var pp = document.getElementById(num+"pp").value;
	var obj = document.getElementById(num+"se");
	var index = obj.selectedIndex;
	var pn = obj.options[index].value;
	var money = pp*pn*newgoodsNum;
	document.getElementById(num+"sp").innerText = money+"元";
}
</script>
  </head>
  
  <body>
  
  <jsp:include page="common/head.jsp"></jsp:include>
  
  
<div class="wufa_g">

<dl class="wf_s">
<dt>${errorTotle }</dt>
<dd>${errorMess }</dd>
<dd><a href="${errorBackUrl }">返回</a><span>|</span><a href="goods_cart" target="_blank">我的购物车</a><span>|</span><a href="order_list" target="_blank">我的订单</a></dd>
</dl>


<div class="clear"></div>
</div>

<jsp:include page="common/foot.jsp"></jsp:include>

  </body>
</html>
